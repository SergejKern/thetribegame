﻿using UnityEditor;
using UnityEditor.AI;
using UnityEngine;
using UnityEngine.AI;

[CustomEditor(typeof(LocalNavMeshBuilder))]
public class LocalNavMeshBuilderEditor : UnityEditor.Editor
{
    SerializedProperty m_AgentTypeID;

    void OnEnable()
    {
        m_AgentTypeID = serializedObject.FindProperty("m_AgentTypeID");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var bs = NavMesh.GetSettingsByID(m_AgentTypeID.intValue);

        if (bs.agentTypeID != -1)
        {
            // Draw image
            const float diagramHeight = 80.0f;
            Rect agentDiagramRect = EditorGUILayout.GetControlRect(false, diagramHeight);
            NavMeshEditorHelpers.DrawAgentDiagram(agentDiagramRect, bs.agentRadius, bs.agentHeight, bs.agentClimb, bs.agentSlope);
        }
        NavMeshComponentsGUIUtility.AgentTypePopup("Agent Type", m_AgentTypeID);
        serializedObject.ApplyModifiedProperties();

    }
}
