﻿// (c) Copyright HutongGames, LLC 2010-2016. All rights reserved.

using GameUtility.Validation;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Animator)]
	[HutongGames.PlayMaker.Tooltip("Plays a state. This could be used to synchronize your animation with audio or synchronize an Animator over the network.")]
	public class AnimatorPlay : FsmStateAction, IAnimatorStateValidation
	{
		[RequiredField]
		[CheckForComponent(typeof(UnityEngine.Animator))]
		[HutongGames.PlayMaker.Tooltip("The target. An Animator component is required")]
		public FsmOwnerDefault gameObject;

        [UIHint(UIHint.Animation)]
        [HutongGames.PlayMaker.Tooltip("The name of the state that will be played.")]
		public FsmString stateName;

		[HutongGames.PlayMaker.Tooltip("The layer where the state is.")]
		public FsmInt layer;

		[HutongGames.PlayMaker.Tooltip("The normalized time at which the state will play")]
		public FsmFloat normalizedTime;

		[HutongGames.PlayMaker.Tooltip("Repeat every frame. Useful when using normalizedTime to manually control the animation.")]
		public bool everyFrame;

		UnityEngine.Animator animator;

		public override void Reset()
		{
			gameObject = null;
			stateName = null;
			layer = new FsmInt(){UseVariable=true};
			normalizedTime = new FsmFloat(){UseVariable=true};
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			// get the animator component
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			
			if (go==null)
			{
				Finish();
				return;
			}
			
			animator = go.GetComponent<UnityEngine.Animator>();
			
			DoAnimatorPlay();
			if (!everyFrame)
			{
				Finish();
			}
		}

		public override void OnUpdate()
		{
			DoAnimatorPlay();
		}

		void DoAnimatorPlay()
		{
            if (animator == null)
                return;
            var _layer = layer.IsNone?-1:layer.Value;
				
            var _normalizedTime = normalizedTime.IsNone?Mathf.NegativeInfinity:normalizedTime.Value;
				
            animator.Play(stateName.Value,_layer,_normalizedTime);

        }

#if UNITY_EDITOR
	    public override string AutoName()
	    {
	        return ActionHelpers.AutoName(this, stateName);
	    }
#endif

        public RuntimeAnimatorController AnimatorController => Fsm.GetOwnerDefaultTarget(gameObject)
            .GetComponent<UnityEngine.Animator>().runtimeAnimatorController;
        public string[] States => new[] {stateName.Value};
    }
}