using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory(ActionCategory.Vector2)]
	[Tooltip("Gets a random input vector (x, y) between (-1 .. +1)")]
	public class GetRandomInputVec : FsmStateAction
    {
        [UIHint(UIHint.Variable)]
        public FsmVector2 RandomInputVec;

        // Code that runs on entering the state.
        public override void OnEnter()
		{
            RandomInputVec.Value = Random.insideUnitCircle;

            Finish();
		}
	}
}