using UnityEditor;
using UnityEngine;

namespace sr
{
    /**
     * Utility methods to find out useful bits about prefabs.
     */
    public class PrefabUtil
    {
        /**
       * Provides a method to determine if the given property on the given prefab instance is a modification to the existing prefab locally (in a scene).
       */
        public static bool isInstanceModification(SerializedProperty prop)
        {
            var obj = prop.serializedObject.targetObject;
            var prefab = PrefabUtility.GetPrefabInstanceHandle(obj);
            if (prefab != null)
            {
                var pms = PrefabUtility.GetPropertyModifications(obj);
#if UNITY_2018_2 || UNITY_2018_3 || UNITY_2018_4
        UnityEngine.Object parent = PrefabUtility.GetCorrespondingObjectFromSource(obj);
#else
                var parent = PrefabUtility.GetCorrespondingObjectFromSource(obj);
#endif

                if (pms.Length > 0)
                    foreach (var pm in pms)
                        // Debug.Log("[PrefabUtil] pm:"+pm.propertyPath);
                        if (pm.target == parent && pm.propertyPath.StartsWith(prop.propertyPath))
                            return true;
            }

            return false;
        }

        public static bool isInPrefabInstance(Object obj)
        {
            // Debug.Log("[PrefabUtil] prefab type:"+PrefabUtility.GetPrefabType(obj)+ " : "+ obj.name + " parent:"+PrefabUtility.GetPrefabObject(obj));
            //return PrefabUtility.GetPrefabType(obj) == PrefabType.PrefabInstance;
            return PrefabUtility.GetPrefabInstanceStatus(obj) != PrefabInstanceStatus.NotAPrefab;
        }


        public static bool isPrefab(Object obj)
        {
            // Debug.Log("[PrefabUtil] prefab type:"+PrefabUtility.GetPrefabType(obj)+ " : "+ obj.name + " parent:"+PrefabUtility.GetPrefabObject(obj));
            //return PrefabUtility.GetPrefabType(obj) == PrefabType.Prefab;
            return PrefabUtility.IsPartOfAnyPrefab(obj)
                   && PrefabUtility.GetPrefabInstanceStatus(obj) == PrefabInstanceStatus.NotAPrefab;
        }

        public static bool isPrefabRoot(Object obj)
        {
            if (obj == null) return false;
            if (obj is GameObject)
            {
                var go = (GameObject) obj;
                if (isPrefab(go)) return PrefabUtility.GetOutermostPrefabInstanceRoot(go) == obj;
            }

            return false;
        }

        public static GameObject getPrefabRoot(Object obj)
        {
            if (obj == null) return null;
            if (obj is GameObject) return getPrefabRoot((GameObject) obj);
            if (obj is Component)
            {
                var c = (Component) obj;
                return getPrefabRoot(c.gameObject);
            }

            return null;
        }

        public static GameObject getPrefabRoot(GameObject go)
        {
            //var type = PrefabUtility.GetPrefabType(go);
            var instanceType =  PrefabUtility.GetPrefabInstanceStatus(go);
            if (instanceType != PrefabInstanceStatus.NotAPrefab)// == PrefabType.PrefabInstance
            {
                go = PrefabUtility.GetOutermostPrefabInstanceRoot(go);
#if UNITY_2018_2 || UNITY_2018_3 || UNITY_2018_4
        go = (GameObject)PrefabUtility.GetCorrespondingObjectFromSource(go);
#else
                go = (GameObject) PrefabUtility.GetPrefabInstanceHandle(go);
#endif
                return go;
            }

            var isPrefab = PrefabUtility.IsPartOfAnyPrefab(go);
            if (isPrefab && instanceType == PrefabInstanceStatus.NotAPrefab)
                return PrefabUtility.GetOutermostPrefabInstanceRoot(go);
            return null;
        }

        public static void SwapPrefab(SearchJob job, SearchResult result, GameObject gameObjToSwap, GameObject prefab,
            bool updateTransform, bool rename)
        {
            var swapParent = gameObjToSwap.transform.parent;
            var index = gameObjToSwap.transform.GetSiblingIndex();

            result.replaceStrRep = prefab.name;
            result.strRep = gameObjToSwap.name;
            // Debug.Log("[ReplaceItemSwapObject] Instantiating:"  +prefab, prefab);
            var newObj = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
            if (newObj != null)
            {
                newObj.transform.parent = swapParent;
                newObj.transform.SetSiblingIndex(index);
                var oldT = gameObjToSwap.transform;
                if (updateTransform)
                {
                    newObj.transform.rotation = oldT.rotation;
                    newObj.transform.localPosition = oldT.localPosition;
                    newObj.transform.localScale = oldT.localScale;
                }

                if (rename) newObj.name = gameObjToSwap.name;
                result.pathInfo = PathInfo.GetPathInfo(newObj, job);

                Object.DestroyImmediate(gameObjToSwap);
            }
            else
            {
                Debug.Log("[Search & Replace] No object instantiated...hrm!");
            }
        }
    }
}