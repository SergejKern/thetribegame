using System;
using UnityEngine;

namespace sr
{
    /**
     * Unlike the search item set, search options are configurable options
     * that the user may choose based upon context. IE: The user may request
     * case sensitive, search vs. replace or other items.
     */
    [Serializable]
    public class SearchOptions
    {
        public SearchType SearchType;

        public SearchOptions Copy()
        {
            var retVal = new SearchOptions { SearchType = SearchType };
            return retVal;
        }

        public static SearchOptions PopulateFromDisk()
        {
            var path = Application.persistentDataPath + "/searchreplace/currentSearchOptions";
            var sis = (SearchOptions) SerializationUtil.Deserialize(path);
            return sis ?? new SearchOptions();
        }
    }
}