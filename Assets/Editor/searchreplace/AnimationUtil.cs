using UnityEditor.Animations;
using UnityEngine;

namespace searchreplace
{
    public class AnimationUtil
    {
        // is the given object an internal animation object?
        public static bool IsInternalAnimationObject(UnityEngine.Object obj)
        {
            return obj is AnimatorStateMachine || obj is AnimatorStateTransition || obj is AnimatorState;
        }
        public static bool IsAnimationObject(UnityEngine.Object obj)
        {
            return obj is AnimationClip || obj is AnimatorController || obj is AnimatorStateMachine || obj is AnimatorStateTransition || obj is AnimatorState;
        }
    }
}