using System;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace sr
{
    /**
     * A SearchResultSet is the results found from a corresponding SearchItemSet.
     * This provides a paginated interface into results so that the UI doesn't get
     * bogged down.
     */
    public class SearchResultSet
    {
        int end;
        string[] pageNames;
        int pageNum;

        public List<SearchResultGroup>
            results = new List<SearchResultGroup>(100); //TODO: make it possible to search for > 100 things?

        public int resultsCount;
        readonly int resultsPerPage = 50;

        Vector2 scrollPosition;
        public int searchedItems = 0;
        int start;

        public SearchStatus status;

        public string statusMsg;

        public SearchResultSet(SearchItemSet searchSet)
        {
            for (var i = 0; i < 10; i++) results.Add(null);
            foreach (var item in searchSet.validatedItems) results[item.sortIndex] = new SearchResultGroup(item);
        }

        public void CopyToClipboard()
        {
            var sb = new StringBuilder();
            sb.Append("Searched " + searchedItems);
            if (results.Count >= 1)
                sb.Append("Found " + resultsCount + " Total Results.\n");
            else
                sb.Append("No results found.");

            foreach (var resultGroup in results)
            {
                sb.Append("\n");
                resultGroup.CopyToClipboard(sb);
            }

            EditorGUIUtility.systemCopyBuffer = sb.ToString();
        }

        public void Add(SearchResult result, SearchItem item)
        {
            var resultGroup = results[item.sortIndex];
            // for(int i = 0; i < 10;i++)
            // {
            resultGroup.AddResult(result);
            resultsCount++;
            // }
        }

        public void OnSearchComplete()
        {
            var foundNull = true;
            while (foundNull) foundNull = results.Remove(null);
            var pages = Mathf.CeilToInt(resultsCount / (float) resultsPerPage);
            pageNames = new string[pages];
            for (var i = 0; i < pages; i++) pageNames[i] = (i + 1).ToString();

            var items = 1;
            for (var i = 0; i < results.Count; i++)
            {
                var result = results[i];
                result.startIndex = items - 1;
                // items += result.results.Count;
                // Debug.Log("[SearchResultSet] result:"+i+" startIndex:"+result.startIndex+" endIndex:"+result.endIndex);
                for (var j = 0; j < result.results.Count; j++)
                {
                    var sr = result.results[j];
                    sr.recordNum = items++;
                }

                result.endIndex = items - 1;
            }
        }

        public void Draw()
        {
            if (status != SearchStatus.Complete)
            {
                GUILayout.Label(statusMsg);
                return;
            }

            if (resultsCount >= 1)
            {
                start = pageNum * resultsPerPage;
                end = Math.Min(start + resultsPerPage, resultsCount);
                if (resultsCount > resultsPerPage)
                    GUILayout.Label(
                        "Showing " + start + "-" + end + " of <b>" + resultsCount + "</b> Total Results. Searched " +
                        searchedItems + " assets.", SRWindow.richTextStyle);
                else
                    GUILayout.Label(
                        "Found <b>" + resultsCount + "</b> Total Results. Searched " + searchedItems + " assets.",
                        SRWindow.richTextStyle);
            }
            else
            {
                GUILayout.Label("No results found, searched " + searchedItems + " assets.", SRWindow.richTextStyle);
            }

            if (resultsCount >= 1) drawPageSelector();
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            drawPage();
            // foreach(SearchResultGroup resultGroup in results)
            // {
            //   resultGroup.Draw();
            // }
            GUILayout.EndScrollView();
            if (SRWindow.Instance.Compact())
                GUILayout.Label(
                    new GUIContent("For more details, make the window wider.",
                        EditorGUIUtility.FindTexture("d_UnityEditor.InspectorWindow")), SRWindow.richTextStyle);
        }

        void drawPageSelector()
        {
            if (resultsCount <= resultsPerPage)
            {
            }
            else
            {
                var xCount = (int) (SRWindow.Instance.position.width / 40.0f);
                pageNum = GUILayout.SelectionGrid(pageNum, pageNames, xCount);
            }
        }

        void drawPage()
        {
            if (resultsCount == 0)
            {
            }
            else
            {
                // Debug.Log("[SearchResultSet] start:"+start+" end:"+end);
                var itemsDrawn = 0;
                var itemsToDraw = end - start;
                var itemsIndex = start;
                var currentResultIndex = getStartGroup(start);
                // Debug.Log("[SearchResultSet] drawing from result: "+currentResultIndex+ " itemsToDraw:"+itemsToDraw+" itemsIndex:"+itemsIndex + " resultsCount:"+resultsCount);
                while (itemsDrawn < itemsToDraw)
                {
                    var currentResult = results[currentResultIndex];
                    // Debug.Log("[SearchResultSet] itemsIndex:"+itemsIndex + " currentResult.startIndex"+ currentResult.startIndex);
                    var groupIndex = itemsIndex - currentResult.startIndex;
                    var itemsToDrawInGroup =
                        Math.Min(itemsToDraw - itemsDrawn, currentResult.results.Count - groupIndex);
                    currentResult.Draw(groupIndex, itemsToDrawInGroup);
                    itemsDrawn += itemsToDrawInGroup;
                    itemsIndex += itemsToDrawInGroup;
                    currentResultIndex++;
                }

                //Draw any empty results until the next record is found.
                var foundResults = false;
                var tries = 0;
                do
                {
                    if (currentResultIndex < results.Count)
                    {
                        var currentResult = results[currentResultIndex];
                        if (currentResult.results.Count == 0)
                        {
                            currentResult.Draw(0, 0);
                            //keep going.
                            currentResultIndex++;
                        }
                        else
                        {
                            //we found a non-empty result, exit.
                            foundResults = true;
                        }
                    }

                    tries++;
                } while (!foundResults && tries < 10);
            }
        }

        int getStartGroup(int start)
        {
            for (var i = 0; i < results.Count; i++)
            {
                var result = results[i];
                if (result.startIndex <= start && result.endIndex >= start) return i;
            }

            return results.Count - 1;
        }

        /**
       * This functions to select active items within the scene and project.
       * It cannot select items when the scene is closed and reopened, since 
       * the instanceIDs go away forEVER.
       */
        public void SelectAll()
        {
            var selections = new HashSet<int>();
            var openScenePath = SceneUtil.GuidPathForActiveScene();
            var unSelectableItems = 0;
            foreach (var srg in results)
            foreach (var sr in srg.results)
                if (sr.pathInfo.objID.isSceneObject)
                {
                    if (sr.pathInfo.objID.guid == openScenePath)
                    {
                        Debug.Log("[SearchResultSet] searching in scene:" + sr.pathInfo.FullPath());
                        var obj = sr.pathInfo.objID.searchForObjectInScene();
                        if (obj != null) selections.Add(obj.GetInstanceID());
                    }
                    else
                    {
                        unSelectableItems++;
                    }
                }
                else
                {
                    selections.Add(sr.pathInfo.gameObjectID);
                }

            if (unSelectableItems > 0)
                Debug.LogWarning("[Search & Replace] " + unSelectableItems +
                                 " items were not selected because they are not within the active scene, have been moved, or deleted.");

            var selectionsArray = new int[selections.Count];
            selections.CopyTo(selectionsArray);
            Selection.instanceIDs = selectionsArray;
        }
    }
}