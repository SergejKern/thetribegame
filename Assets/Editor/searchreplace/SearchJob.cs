#define PSR_FULL
#if DEMO
#undef PSR_FULL
#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using searchreplace;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    /**
     * When a user executes a search, a SearchJob is generated. Execute() is called
     * and SearchSubJobs are created based on the type of data being searched.
     * SearchJob keeps track of searched assets to stop searches from accidentally
     * being executed multiple times (possibly due to Dependencies being checked).
     * This also contains a number of utility functions for subjobs and search items
     * to call during execution. Things such as recursively searching objects or
     * searching more complicated objects like AnimatorControllers.
     */
    public class SearchJob
    {
        //Our currently searched asset.
        public Object asset;

        // The data model for our currently searched asset.
        public SearchAssetData assetData;
        int currentItem;

        readonly List<string> internalPath = new List<string>();

        readonly StringBuilder logBuilder = new StringBuilder();

        readonly int maxItemsAllowed = 10000;

        // How we want to search for it. This is always a copy.
        public SearchOptions options;

        // The results of the search.
        public SearchResultSet result;

        SceneSubJob sceneSubJob;

        // Where we should search.
        public SearchScopeData scope;

        //What we are searching for.
        public SearchItemSet search;

        public Dictionary<string, SearchAssetData> searchAssetsData = new Dictionary<string, SearchAssetData>();
        bool shownMaxItemsWarning;

        // A list of our subjobs.
        readonly List<SearchSubJob> subjobs = new List<SearchSubJob>();
        public HashSet<string> supportedFileTypes = new HashSet<string>();

        //Progress watching
        int totalItems;

        public SearchJob(SearchItemSet s, SearchOptions o, SearchScopeData sc)
        {
            search = s;
            options = o;
            scope = sc;
            Execute();
        }

        public void Log(string message)
        {
            logBuilder.Append(message);
            logBuilder.Append("\n");
        }

        void addIgnorableResources()
        {
            addIgnorableResource("Library/unity default resources");
            addIgnorableResource("Resources/unity_builtin_extra");
        }

        void addIgnorableResource(string path)
        {
            var data = new SearchAssetData(path);
            data.hasBeenSearched = true;
            searchAssetsData[path] = data;
        }

        public void Execute()
        {
            addIgnorableResources();
            var assetPaths = AssetDatabase.GetAllAssetPaths();
            AnimationMode.StopAnimationMode(); // If recording is on, it will hose the animation.

            subjobs.Add(new SearchSubJob(this, AssetScope.Prefabs, assetPaths, new[] {".prefab"}, false));

            //scenes are oh so 'special'.
            sceneSubJob = new SceneSubJob(this, AssetScope.Scenes, assetPaths, new[] {".unity"});
            subjobs.Add(sceneSubJob);
            subjobs.Add(new SearchSubJob(this, AssetScope.Materials, assetPaths, new[] {".mat"}, false));
            subjobs.Add(new SearchSubJob(this, AssetScope.ScriptableObjects, assetPaths, new[] {".asset"}, false));
            subjobs.Add(new SearchSubJob(this, AssetScope.Animations, assetPaths, new[] {".anim"}, false));
            subjobs.Add(new SearchSubJob(this, AssetScope.Animators, assetPaths, new[] {".controller"}, false));
            subjobs.Add(new SearchSubJob(this, AssetScope.AudioClips, assetPaths, new[] {".wav", ".mp3"}, true));
            subjobs.Add(new SearchSubJob(this, AssetScope.Textures, assetPaths,
                new[] {".png", ".psd", ".tiff", ".tif", ".tga", ".gif", ".bmp", ".jpg", ".jpeg", ".iff", ".pict"},
                true));
            subjobs.Add(new SceneObjectSubJob(this, AssetScope.None, assetPaths, new[] {""}));

            foreach (var subjob in subjobs) totalItems += subjob.assetPaths.Count;
            search.OnSearchBegin();
            result = new SearchResultSet(search);
            result.status = SearchStatus.InProgress;

            currentItem = 1;

            if (sceneSubJob.assetPaths.Count > 0 && scope.projectScope != ProjectScope.CurrentScene)
            {
                var shouldContinue = SceneUtil.SaveDirtyScenes();
                if (!shouldContinue)
                {
                    userAbortedSearch();
                    return;
                }
            }

            var cancelled = false;
            try
            {
                foreach (var subjob in subjobs)
                    for (var i = 0; i < 10; i++)
                    {
                        if (!cancelled) cancelled = progress();
                        while (!cancelled && subjob.SearchNextAsset())
                        {
                            if (result.resultsCount > maxItemsAllowed && !shownMaxItemsWarning)
                            {
                                shownMaxItemsWarning = true;
                                var userContinues = EditorUtility.DisplayDialog("Too Many Results",
                                    "The search and replace plugin has found " + result.resultsCount +
                                    " results so far. Do you want to continue searching?", "Continue", "Cancel");
                                if (!userContinues) cancelled = true;
                            }

                            currentItem++;
                        }
                    }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                result.status = SearchStatus.InProgress;
                result.statusMsg = "An exception occurred: " + ex;
                EditorUtility.ClearProgressBar();
            }

            EditorUtility.ClearProgressBar();

            foreach (var subjob in subjobs) subjob.JobDone();

            search.OnSearchEnd(this);

            if (cancelled) userAbortedSearch();

            //calculate searchedItems
            var searchedItems = 0;
            foreach (var kvp in searchAssetsData)
            {
                var data = kvp.Value;
                if (data.searchExecuted) searchedItems++;
            }

            if (result.status == SearchStatus.InProgress)
            {
                //standard termination.
                result.searchedItems = searchedItems;
                result.status = SearchStatus.Complete;
            }

            result.OnSearchComplete();

            var log = logBuilder.ToString();
            if (log.Length > 0) Debug.Log("[Search & Replace] Log:\n" + log);
        }

        void userAbortedSearch()
        {
            result.status = SearchStatus.UserAborted;
            result.statusMsg = "User cancelled search.";
        }


        bool progress()
        {
            if (totalItems > 0)
                return EditorUtility.DisplayCancelableProgressBar("Progress",
                    "Searching " + currentItem + "/" + totalItems, currentItem / (float) totalItems);
            return false;
        }

        //Callback while searching.
        public void MatchFound(SearchResult r, SearchItem s)
        {
            if (r.actionTaken != SearchAction.Ignored) result.Add(r, s);
        }

        public void Draw()
        {
            GUILayout.BeginVertical();

            result.Draw();

            GUILayout.EndVertical();
            if (result.status == SearchStatus.Complete)
            {
                GUILayout.BeginHorizontal();
#if PSR_FULL
                if (GUILayout.Button("Copy To Clipboard")) result.CopyToClipboard();
                if (GUILayout.Button("Select Objects")) result.SelectAll();
#else
        GUILayout.FlexibleSpace();
#endif
                SRWindow.Instance.drawAbout();

                GUILayout.EndHorizontal();
            }
            else
            {
                GUILayout.FlexibleSpace();

                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                SRWindow.Instance.drawAbout();

                GUILayout.EndHorizontal();
            }
        }

        // SUBJOB HELPER FUNCTIONS.

        public void searchObject(Object obj)
        {
            if (obj == null) return;
            var so = new SerializedObject(obj);
            var iterator = so.GetIterator();
            search.SearchProperty(this, iterator);
            search.SearchObject(this, obj);
        }

        public void searchGameObject(GameObject go)
        {
            // search the game object itself.
            searchObject(go);
            if (go == null) return;
            var components = go.GetComponents<Component>();
            search.SearchGameObject(this, go);
            foreach (var c in components)
                if (c != null)
                {
                    var obj = new SerializedObject(c);
                    var iterator = obj.GetIterator();
                    search.SearchProperty(this, iterator);
                }

            if (go != null)
            {
                var children = go.transform.GetEnumerator();
                while (children.MoveNext())
                {
                    var child = (Transform) children.Current;
                    if (child != null)
                        searchGameObject(child.gameObject);
                    else
                        break;
                }
            }
        }

        public void searchAnimatorController(AnimatorController ac)
        {
            var so = new SerializedObject(ac);
            var layers = so.FindProperty("m_AnimatorLayers");

            if (layers != null)
            {
                layers.Next(true);
                layers.Next(true);
                var count = layers.intValue;
                for (var i = 0; i < count; i++)
                {
                    layers.Next(false);
                    // Debug.Log("[SearchJob] found:"+layers.propertyPath);

                    search.SearchProperty(this, layers.Copy());

                    //grab the state machine from this layer.
                    var stateMachine = layers.FindPropertyRelative("m_StateMachine");
                    var stateMachineObj = stateMachine.objectReferenceValue;
                    // Debug.Log("[SearchJob] stateMachineObj:"+stateMachineObj);
                    pushPath(stateMachineObj);
                    var stateMachineSO = new SerializedObject(stateMachineObj);
                    searchObject(stateMachineObj);
                    searchArray(stateMachineSO.FindProperty("m_AnyStateTransitions"));
                    var states = searchArrayRelative(stateMachineSO.FindProperty("m_ChildStates"), "m_State");
                    foreach (var stateSO in states)
                    {
                        pushPath(stateSO.targetObject);
                        searchArray(stateSO.FindProperty("m_Transitions"));
                        popPath();
                    }

                    popPath();
                }
            }
        }

        List<SerializedObject> searchArrayRelative(SerializedProperty arrayProp, string relativeName)
        {
            var retVal = new List<SerializedObject>();
            arrayProp.Next(true);
            arrayProp.Next(true);
            var count = arrayProp.intValue;
            for (var i = 0; i < count; i++)
            {
                arrayProp.Next(false);
                var item = arrayProp.FindPropertyRelative(relativeName);
                pushPath(item.objectReferenceValue);
                searchObject(item.objectReferenceValue);
                retVal.Add(new SerializedObject(item.objectReferenceValue));
                popPath();
            }

            return retVal;
        }

        void searchArray(SerializedProperty arrayProp)
        {
            arrayProp.Next(true);
            arrayProp.Next(true);
            var count = arrayProp.intValue;
            for (var i = 0; i < count; i++)
            {
                arrayProp.Next(false);
                pushPath(arrayProp.objectReferenceValue);
                searchObject(arrayProp.objectReferenceValue);
                popPath();
            }
        }

        public void pushPath(Object obj)
        {
            if (obj == null)
                return;

            internalPath.Add(obj.name);
            assetData.SetInternalAssetPath(internalPath);
        }

        public void popPath()
        {
            internalPath.RemoveAt(internalPath.Count - 1);
        }

        //Instead of maintaining a separate hashset, we just put the internal asset
        //into the dictionary.
        public void addInternalAsset(string path, int instanceID)
        {
            var internalSearchObject = new SearchAssetData(path);
            searchAssetsData[path + instanceID] = internalSearchObject;
        }

        // DEPENDENCY HELPER FUNCTIONS

        /**
       * Called to handle whether we should search the given asset path.
       */
        public bool isSearchable(string assetPath)
        {
            return supportedFileTypes.Contains(Path.GetExtension(assetPath));
        }

        /**
       * Whether or not the currently searched item is of a type that a SearchItem
       * wants to process.
       */
        public bool searchItemCaresAboutAsset()
        {
            return search.searchItemCaresAboutAsset(this);
        }


        public void searchDependency(SearchAssetData data)
        {
            asset = AssetDatabase.LoadAssetAtPath(data.assetPath, typeof(Object));
            assetData = data;
            if (asset is GameObject go)
            {
                searchGameObject(go);
            }
            else
            {
                searchObject(asset);
            }

            if (assetData.assetIsDirty)
            {
                //TODO save asset?
            }

            assetData.hasBeenSearched = true;
        }

        public void searchDependencies(Object rootObject)
        {
            searchDependencies(new[] {rootObject});
        }

        public void searchDependencies(IReadOnlyCollection<Object> rootObjects)
        {
            if (!scope.searchDependencies) return;
            if (scope.assetScope == SearchScope.allAssets && scope.projectScope == ProjectScope.EntireProject) return;
            //search dependencies
            var dependencies = EditorUtility.CollectDependencies(rootObjects.ToArray());

            foreach (var dependency in dependencies)
            {
                var path = AssetDatabase.GetAssetPath(dependency);
                if (searchAssetsData.ContainsKey(path))
                    continue;
                if (path == string.Empty)
                    continue;
                if (!isSearchable(path))
                    continue;

                var data = new SearchAssetData(path);
                searchAssetsData.Add(path, data);
                searchDependency(data);
            }
        }
    }
}