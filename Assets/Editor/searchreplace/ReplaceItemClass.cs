#define PSR_FULL
#if DEMO
#undef PSR_FULL
#endif
using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    [Serializable]
    public class ReplaceItemClass : ReplaceItem<DynamicTypeClass, ObjectID>
    {
        // public ObjectID objID;

        // [System.NonSerialized]
        // public DynamicTypeClass parent;

        [NonSerialized] bool replaceValid;

        public override void Draw()
        {
#if PSR_FULL
            GUILayout.BeginHorizontal();
            var lw = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = SRWindow.compactLabelWidth;
            var newValue = EditorGUILayout.ObjectField(Keys.Replace, replaceValue.obj, typeof(Object), true);

            EditorGUIUtility.labelWidth = lw; // i love stateful gui! :(

            if (replaceValue.obj != newValue)
            {
                var scriptObject = ObjectUtil.getScriptObjectFromObject(newValue);
                if (scriptObject == null && newValue != null)
                    Debug.Log("[Project Search & Replace] " + newValue +
                              " isn't a Component, MonoBehaviour, ScriptableObject or MonoScript.");
                setObject(scriptObject);
                SRWindow.Instance.PersistCurrentSearch();
            }

            if (type != null)
            {
                GUILayout.Label("(" + type.Name + ")");
                drawSwap();
                GUILayout.EndHorizontal();
                if (parent.type != null)
                {
                    var isMB = typeof(MonoBehaviour).IsAssignableFrom(type);
                    var isParentMB = typeof(MonoBehaviour).IsAssignableFrom(parent.type);
                    var isScript = typeof(ScriptableObject).IsAssignableFrom(type);
                    var isParentScript = typeof(ScriptableObject).IsAssignableFrom(parent.type);

                    if (isMB && isParentMB || isScript && isParentScript)
                    {
                        if (parent.type.IsAssignableFrom(type))
                        {
                            replaceValid = true;
                            // EditorGUILayout.HelpBox("Looks good!", MessageType.Info);
                        }
                        else
                        {
                            replaceValid = true;
                            EditorGUILayout.HelpBox(
                                "Inheritance Warning: These classes do not inherit from each other but Unity will attempt to merge data as best as possible.",
                                MessageType.Info);
                        }
                    }
                    else
                    {
                        replaceValid = false;
                        EditorGUILayout.HelpBox(
                            "Invalid or Mixed Parameters. Components can only be searched for, not replaced.",
                            MessageType.Warning);
                    }
                }
            }
            else
            {
                replaceValid = false;
                drawSwap();
                GUILayout.EndHorizontal();
            }
#endif
        }

        public void setObject(Object o)
        {
            replaceValue.SetObject(o);
            type = ObjectUtil.getTypeFromObject(o);
        }

        public override bool IsValid()
        {
            return replaceValid;
        }

        // Fixes nulls in serialization manually...*sigh*.
        public override void OnDeserialization()
        {
            if (replaceValue == null) replaceValue = new ObjectID();
            replaceValue.OnDeserialization();
        }

        protected override void replace(SearchJob job, SerializedProperty prop, SearchResult result)
        {
#if PSR_FULL
            if (prop.name == "m_Script")
            {
                prop.objectReferenceValue = replaceValue.obj;
                result.replaceStrRep = type.Name;
                prop.serializedObject.ApplyModifiedProperties();
                result.actionTaken = SearchAction.Replaced;
            }
            else
            {
                var isScript = typeof(ScriptableObject).IsAssignableFrom(type);
                if (isScript)
                {
                    var obj = prop.objectReferenceValue;
                    var path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
                    if (path == "")
                    {
                        // It looks like this is a scriptable object that is serialized within
                        // another object and doesn't have a specific location on disk.
                        var so = new SerializedObject(obj);
                        var m_Script = so.FindProperty("m_Script");
                        m_Script.objectReferenceValue = replaceValue.obj;
                        result.replaceStrRep = type.Name;
                        so.ApplyModifiedProperties();
                    }
                    else
                    {
                        result.actionTaken = SearchAction.Ignored;
                    }
                }
                else
                {
                    result.actionTaken = SearchAction.InstanceFound;
                }
            }

#endif
        }

        protected override void Swap()
        {
            var tmp = parent.searchValue.obj;
            parent.SetObject(replaceValue.obj);
            setObject(tmp);
            SRWindow.Instance.PersistCurrentSearch();
        }
    } // End Class
} // End Namespace