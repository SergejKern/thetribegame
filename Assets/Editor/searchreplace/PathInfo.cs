using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sr
{
    /**
     * PathInfo is the data object behind a search result. It provides data that
     * can be displayed to the user without requiring that the (potentially large)
     * object is loaded into memory.
     */
    public class PathInfo
    {
        // A human readable compact name that can be displayed.
        public string assetName;

        // The full path to the asset on disk.
        public string assetPath;

        // An object path that is shorter for tight UI constraints.
        public string compactObjectPath;

        // The instance id of the game object that contains the search result.
        public int gameObjectID;

        // The instance id of the search result itself.
        public int objectID;

        // If this path info goes into an object hierarchy, this will contain that 
        // information in the format object:subobject->property
        public string objectPath;

        // An objectID object for even more information!
        public ObjectID objID;

        // What's the prefab type?
        public PrefabAssetType prefabAssetType;
        public PrefabInstanceStatus prefabInstanceStatus;

        public string FullPath()
        {
            return assetPath + objectPath;
        }

        /**
       * Provides functionality to create path information to an object: Folders->Objects->Monobehaviour->Property
       */
        public static PathInfo GetPathInfo(SerializedProperty prop, SearchJob job)
        {
            var obj = prop.serializedObject.targetObject;
            PathInfo pi = null;
            if (obj is Component)
            {
                var c = (Component) obj;
                pi = InitWithComponent(c, c.gameObject, job);

                if (c is MonoBehaviour)
                {
                    //Get the class information and attach it, then the property path
                    var m = (MonoBehaviour) c;
                    var ms = MonoScript.FromMonoBehaviour(m);
                    pi.objectPath = ToPath(m.gameObject, job) + "->" + ms.GetClass() + "." + prop.propertyPath;
                }
                else
                {
                    // Just a component! GetType will do.
                    pi.objectPath = ToPath(c.gameObject, job) + "->" + c.GetType() + "." + prop.propertyPath;
                }

                pi.compactObjectPath = c.gameObject.name + " (" + c.GetType().Name + ")." + prop.propertyPath;
            }
            else if (obj is GameObject)
            {
                pi = GetPathInfo((GameObject) obj, job);
                pi.objectPath += "." + prop.propertyPath;
            }
            else if (obj is Material)
            {
                pi = GetPathInfo(obj, job);
                pi.objectPath = demanglePropertyPathForMaterial(prop);
                pi.compactObjectPath = obj.name + demanglePropertyPathForMaterial(prop);
            }
            else if (obj is Object)
            {
                pi = GetPathInfo(obj, job);
                pi.objectPath = job.assetData.internalAssetPath + "." + prop.propertyPath;
            }
            else
            {
                Debug.Log("[PathInfo] Cannot guess object type:" + obj);
            }

            return pi;
        }

        // Currently used by DynamicTypeClass when matching classes, and also internally.
        // parent is passed in because its possible component can be null!
        public static PathInfo InitWithComponent(Component c, GameObject parent, SearchJob job)
        {
            var pi = new PathInfo();
            if (c != null)
            {
                pi.objID = new ObjectID(c);
                pi.objectID = c.GetInstanceID();
            }
            else
            {
                pi.objID = new ObjectID(parent);
            }

            pi.objID.obj = null;

            pi.gameObjectID = parent.GetInstanceID();
            pi.prefabAssetType = PrefabUtility.GetPrefabAssetType(parent);
            pi.prefabInstanceStatus = PrefabUtility.GetPrefabInstanceStatus(parent);

            pi.assetPath = ToAssetPath(parent);
            pi.assetName = Path.GetFileName(pi.assetPath);
            pi.objectPath = ToPath(parent, job);
            pi.compactObjectPath = parent.name;

            return pi;
        }

        public static PathInfo GetPathInfo(GameObject go, SearchJob job)
        {
            var pi = new PathInfo();
            pi.objectID = go.GetInstanceID();
            pi.objID = new ObjectID(go);
            pi.objID.obj = null;


            pi.gameObjectID = pi.objectID;
            pi.assetPath = ToAssetPath(go);
            pi.assetName = Path.GetFileName(pi.assetPath);
            pi.prefabAssetType = PrefabUtility.GetPrefabAssetType(go);
            pi.prefabInstanceStatus = PrefabUtility.GetPrefabInstanceStatus(go);
            pi.objectPath = ToPath(go, job);
            pi.compactObjectPath = go.name;
            return pi;
        }

        public static PathInfo GetPathInfo(Object obj, SearchJob job)
        {
            var pi = new PathInfo();
            pi.objectID = obj.GetInstanceID();
            pi.objID = new ObjectID(obj);
            pi.objID.obj = null;

            pi.gameObjectID = pi.objectID;
            pi.assetPath = ToAssetPath(obj);
            pi.assetName = Path.GetFileName(pi.assetPath);
            pi.prefabAssetType = PrefabAssetType.NotAPrefab;
            pi.prefabInstanceStatus = PrefabInstanceStatus.NotAPrefab;

            if (obj.name == string.Empty)
            {
                pi.objectPath = obj.GetType().ToString();
                pi.compactObjectPath = pi.objectPath;
            }
            else
            {
                pi.objectPath = obj.name;
                pi.compactObjectPath = pi.objectPath;
            }

            return pi;
        }

        public static PathInfo InitWithScriptableObject(ScriptableObject so, SearchJob job)
        {
            var pi = GetPathInfo(so, job);
            pi.objectPath = "";
            return pi;
        }

        protected static string ToAssetPath(Object o)
        {
            switch (o)
            {
                // Debug.Log("[PathInfo] ToAssetPath:" + o +"  "+o.GetInstanceID() + AssetDatabase.GetAssetPath(o.GetInstanceID()) +  " type:"+PrefabUtility.GetPrefabType(o));
                case GameObject _:
                {
                    var prefabInstanceStatus = PrefabUtility.GetPrefabInstanceStatus(o);
                    var isPartOfPrefab = PrefabUtility.IsPartOfAnyPrefab(o);

                    if (prefabInstanceStatus == PrefabInstanceStatus.NotAPrefab && isPartOfPrefab)
                    {
                        return AssetDatabase.GetAssetPath(o.GetInstanceID());
                    }
                    return SceneManager.GetActiveScene().path;
                }
                case ScriptableObject _:
                {
                    var path = AssetDatabase.GetAssetPath(o.GetInstanceID());
                    return path == "" ? SceneManager.GetActiveScene().path : path;
                }
                default:
                    return AssetDatabase.GetAssetPath(o.GetInstanceID());
            }

            // Debug.Log("[PathInfo] Asset path empty");
        }

        // The paths for materials are kind of intense. Let's make this make 
        // sense!
        public static string demanglePropertyPathForMaterial(SerializedProperty prop)
        {
            //The format of items follows something like this:
            // m_SavedProperties.m_TexEnvs.Array.data[0].second.m_Texture
            // m_SavedProperties.m_TexEnvs.Array.data[0].first.name
            // So we can do some simple string manipulation and get rid of the second.blah and replace it with first.name, find that property, and use that value instead of 
            // the property path.
            var propPath = prop.propertyPath;
            var secondIndex = propPath.LastIndexOf("second");
            if (secondIndex > -1)
            {
                propPath = propPath.Substring(0, secondIndex);
                //Get the name.
                propPath += "first.name";
                var firstProp = prop.serializedObject.FindProperty(propPath);
                if (firstProp != null)
                    return "." + firstProp.stringValue;
                return "." + prop.propertyPath;
            }

            return "." + prop.propertyPath; //fallback.
        }

        protected static string ToPath(GameObject go, SearchJob job)
        {
            var retVal = "";
            var t = go.transform;
            while (t != null)
            {
                retVal = t.gameObject.name + retVal;
                t = t.parent;
                if (t != null) retVal = "::" + retVal;
            }

            return "/" + retVal + job.assetData.internalAssetPath;
        }

        protected static string ToPath(Object o, SearchJob job)
        {
            if (o is Component)
            {
                var c = (Component) o;
                return ToPath(c.gameObject, job) + "." + c.GetType().Name;
            }

            if (o is GameObject)
            {
                var go = (GameObject) o;
                return ToPath(go, job);
            }

            return "";
        }
    }
}