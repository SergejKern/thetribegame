using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    /**
     * Frequently we're given a UnityEngine.Object and we need to glean an object
     * type out of it somehow. These classes help with that.
     */
    public class ObjectUtil
    {
        public static Object getScriptObjectFromObject(Object obj)
        {
            if (obj == null) return null;
            if (obj is MonoBehaviour)
            {
                var mb = (MonoBehaviour) obj;
                return MonoScript.FromMonoBehaviour(mb);
            }

            if (obj is ScriptableObject) return MonoScript.FromScriptableObject((ScriptableObject) obj);
            if (obj is Component) return obj;
            if (obj is MonoScript) return obj;
            return null;
        }

        public static Type getTypeFromObject(Object obj)
        {
            if (obj == null) return null;
            if (obj is MonoScript)
            {
                var ms = (MonoScript) obj;
                return ms.GetClass();
            }

            if (obj is Component) return obj.GetType();
            return null;
        }

        public static bool IsDirectory(Object obj)
        {
            var assetPath = AssetDatabase.GetAssetPath(obj);
            var isDirectory = false;
            if (obj is DefaultAsset)
                isDirectory = Directory.Exists(Application.dataPath.Replace("Assets", "") + assetPath);
            else
                isDirectory = false;
            return isDirectory;
        }

        public static bool ValidateAndAssign(Object newObj, ObjectID objID, PropertyPopupData searchProperty,
            ref InitializationContext initializationContext)
        {
            // First we need to attempt validation, or find the 'real' object we are 
            // interested in.
            var contextObj = newObj;
            var objectValid = true;
            // If the object is a 'scene' object, let's get the prefab object if it
            // exists.
            if (newObj != null)
            {
                if (newObj.GetInstanceID() < 0)
                {
                    // Debug.Log("[SearchItemProperty] finding prefab.");
#if UNITY_2018_2 || UNITY_2018_3 || UNITY_2018_4
          UnityEngine.Object prefab = PrefabUtility.GetCorrespondingObjectFromSource(newObj);
#else
                    var prefab = PrefabUtility.GetCorrespondingObjectFromSource(newObj);
#endif
                    if (prefab != null) newObj = prefab;
                }

                var partOfPrefab = PrefabUtility.IsPartOfAnyPrefab(newObj);
                if (!partOfPrefab)
                    if (newObj is Component)
                        if (newObj is MonoBehaviour)
                        {
                            var script = MonoScript.FromMonoBehaviour((MonoBehaviour) newObj);
                            if (script != null) newObj = script;
                        }
            }

            if (newObj is MonoScript)
            {
                var m = (MonoScript) newObj;
                if (typeof(MonoBehaviour).IsAssignableFrom(m.GetClass()) ||
                    typeof(ScriptableObject).IsAssignableFrom(m.GetClass()))
                {
                    // Debug.Log("[SearchItemProperty] Valid monobehaviour"+m.GetClass());
                }
                else
                {
                    Debug.LogWarning("[Search And Replace] The object given does not contain a script.");
                    objectValid = false;
                }
            }

            //Ok, validation is complete, let's process the new object.
            if (objectValid)
            {
                objID.SetObject(newObj);
                searchProperty.SetType(objID.obj);
                initializationContext = new InitializationContext(searchProperty.fieldData, contextObj);
                initializationContext.forceUpdate = true;
                SRWindow.Instance.PersistCurrentSearch();
                return true;
            }

            return false;
        }


        // utility function to get a game object from this object if possible.
        public static GameObject GetGameObject(Object obj)
        {
            if (obj is GameObject) return (GameObject) obj;
            if (obj is Component) return ((Component) obj).gameObject;
            return null;
        }
    }
}