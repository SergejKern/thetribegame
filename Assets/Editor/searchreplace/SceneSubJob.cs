using System.Collections.Generic;
using System.Linq;
using searchreplace;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace sr
{
    /**
     * The search sub job that interfaces with searching scenes.
     */
    public class SceneSubJob : SearchSubJob
    {
        /*
       * At startup we get the scenes that were in place before the job is run so
       * that we know how to get us back to our initial state.
       */
        readonly Dictionary<string, SceneSetup> loadScenesHash = new Dictionary<string, SceneSetup>();

        /** Don't count on sceneSetups having scenes! User could be in a new unsaved scene. */
        readonly SceneSetup[] sceneSetups;

        public SceneSubJob(SearchJob j, AssetScope sc, string[] allAssets, string[] s) : base(j, sc, allAssets, s,
            false)
        {
            sceneSetups = EditorSceneManager.GetSceneManagerSetup();
            foreach (var sceneSetup in sceneSetups)
                loadScenesHash[sceneSetup.path] = sceneSetup;
            // Debug.Log("[SceneSubJob] scene:"+sceneSetup.path);
        }

        protected override void filterAssetPaths(string[] allAssets)
        {
            base.filterAssetPaths(allAssets);
            if (job.scope.projectScope == ProjectScope.CurrentScene)
                addItems(new[] {SceneUtil.GuidPathForActiveScene()});
        }

        protected override void processAsset(string assetPath)
        {
            Scene? currentScene;

            currentScene = job.scope.projectScope == ProjectScope.CurrentScene 
                ? SceneManager.GetActiveScene() 
                : SceneUtil.LoadScene(assetPath, OpenSceneMode.Single);

            // Debug.Log("[SearchJob] scene:"+currentScene.path+" loaded.");
            if (currentScene == null)
                return;

            var rootObjects = currentScene.Value.GetRootGameObjects();

            foreach (var go in rootObjects)
                // Debug.Log("[SearchJob] root:"+go);
                job.searchGameObject(go);
            if (job.assetData.assetIsDirty) EditorSceneManager.SaveScene(currentScene.Value);

            job.searchDependencies(rootObjects.ToArray());
        }


        public override void JobDone()
        {
            if (job.scope.projectScope == ProjectScope.CurrentScene)
            {
                if (job.assetData.assetRequiresRefresh) EditorSceneManager.RestoreSceneManagerSetup(sceneSetups);
            }
            else
            {
                //scope isn't just the scene...and we're processing stuff.
                if (assetPaths.Count > 0 && sceneSetups.Length > 0)
                    EditorSceneManager.RestoreSceneManagerSetup(sceneSetups);
            }
        }
    }
}