using System;
using System.Collections.Generic;
using System.Linq;
using searchreplace;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    /**
     * The search item for searching globally for a specific object or primitive type.
     */
    [Serializable]
    public class SearchItemGlobal : SearchItem
    {
        static int recursiveDepth;

        public int depth;

        [NonSerialized] InitializationContext initializationContext;

        [NonSerialized] GameObject searchAsGameObject;

        [NonSerialized] Dictionary<string, Type> subTypeHash;

        [NonSerialized] string[] subTypeOptions;

        [NonSerialized] Type type;

        DynamicTypeField typeField;

        [NonSerialized] Dictionary<string, Type> typeHash;

        int typeIndex;

        [NonSerialized] string[] typeOptions;

        public override void Draw(SearchOptions options)
        {
            var boxStyle = depth == 0 ? SRWindow.searchBox : SRWindow.searchInnerDepthBox;
            GUILayout.BeginHorizontal(boxStyle);
            GUILayout.BeginVertical();
            SRWindow.Instance.CVS();
            drawSubsearch();

            GUILayout.BeginHorizontal();


            var lw = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = SRWindow.compactLabelWidth;

            var typeLabel = depth == 0 ? "Type:" : "Subtype:";
            var tOptions = depth == 0 ? typeOptions : subTypeOptions;
            var newIndex = EditorGUILayout.Popup(typeLabel, typeIndex, tOptions,
                GUILayout.MaxWidth(SRWindow.Instance.position.width - 40));
            EditorGUIUtility.labelWidth = lw; // i love stateful gui! :(

            if (newIndex != typeIndex)
            {
                typeIndex = newIndex;
                initializationContext = new InitializationContext(typeHash[tOptions[typeIndex]]);
                typeField.SetType(initializationContext);
                SRWindow.Instance.PersistCurrentSearch();
            }

            if (depth == 0)
                if (typeField.hasAdvancedOptions())
                {
                    var newShowMoreOptions =
                        EditorGUILayout.Toggle(showMoreOptions, SRWindow.optionsToggle, GUILayout.Width(15));
                    if (newShowMoreOptions != showMoreOptions)
                    {
                        showMoreOptions = newShowMoreOptions;
                        typeField.showMoreOptions = showMoreOptions;
                        SRWindow.Instance.PersistCurrentSearch();
                    }
                }

            GUILayout.EndHorizontal();

            typeField.showMoreOptions = showMoreOptions;
            var typeFieldOptions = options.Copy();

            if (subsearch != null) typeFieldOptions.SearchType = SearchType.Search;
            typeField.Draw(typeFieldOptions);

            if (depth == 0)
            {
                if (subsearch != null)
                {
                    var item = (SearchItem) subsearch;
                    item.Draw(options);
                }

                drawAddRemoveButtons();
            }

            GUILayout.EndVertical();

            SRWindow.Instance.CVE();

            GUILayout.EndHorizontal();
        }


        // Fixes nulls in serialization manually...*sigh*.
        public override void OnDeserialization()
        {
            if (typeField == null) typeField = new DynamicTypeField();
            typeHash = new Dictionary<string, Type>();
            typeHash["Object"] = typeof(Object);
            typeHash["Text"] = typeof(string);
            typeHash["Float, Double"] = typeof(float);
            typeHash["Integer, Long"] = typeof(long);
            typeHash["Boolean"] = typeof(bool);
            typeHash["Color"] = typeof(Color);
            typeHash["Vector2"] = typeof(Vector2);
            typeHash["Vector3"] = typeof(Vector3);
            typeHash["Vector4"] = typeof(Vector4);
            typeHash["Quaternion"] = typeof(Quaternion);
            typeHash["Rect"] = typeof(Rect);
            var classSearchLabel = "Scripts";
            typeHash[classSearchLabel] = typeof(Type);
            typeOptions = typeHash.Keys.ToArray();
            subTypeHash = new Dictionary<string, Type>(typeHash);
            subTypeHash.Remove(classSearchLabel);
            subTypeOptions = subTypeHash.Keys.ToArray();

            typeField.OnDeserialization(); // sigh...this is getting old.
            typeField.showMoreOptions = showMoreOptions;

            typeField.depth = depth;
            initializationContext = new InitializationContext(typeHash[typeOptions[typeIndex]]);
            typeField.SetType(initializationContext);
            OnDeserializeSubSearch();
        }

        public void SearchProperty(SearchJob job, SearchItem item, SerializedProperty prop)
        {
            var onlyVisible = !AnimationUtil.IsAnimationObject(prop.serializedObject.targetObject);
            var isScene = job.assetData.assetScope == AssetScope.Scenes;
            var iterator = prop.Copy();
            SerializedProperty endProperty = null;
            if (depth == 0)
            {
            }
            else
            {
                endProperty = iterator.GetEndProperty();
            }

            // if(endProperty != null)
            // {
            //   Debug.Log("[SearchItemGlobal] STARTING on "+iterator.propertyPath + " ENDING on "+endProperty.propertyPath);
            // }

            while (Next(iterator, onlyVisible))
            {
                if (ignorePropertyOfType(iterator)) continue;
                if (endProperty != null && SerializedProperty.EqualContents(iterator, endProperty)) return;


                // Its possible that we may have sub-objects that are serialized 
                // scriptable objects within a scene. These aren't scriptable objects 
                // on disk, and so we need to create a SerializedObject representation
                // and go a bit deeper.
                if (isScene)
                    if (iterator.propertyType == SerializedPropertyType.ObjectReference &&
                        iterator.objectReferenceValue is ScriptableObject)
                    {
                        var path = AssetDatabase.GetAssetPath(iterator.objectReferenceValue.GetInstanceID());
                        if (path == "")
                        {
                            // Debug.Log("[SearchItemGlobal] found scriptable object serialized within a scene."+iterator.propertyPath);
                            var so = new SerializedObject(iterator.objectReferenceValue);
                            recursiveDepth++;
                            if (recursiveDepth < 100)
                            {
                                var internalID = job.assetData.assetPath + so.targetObject.GetInstanceID();
                                SearchAssetData internalObjectData = null;
                                if (!job.searchAssetsData.TryGetValue(internalID, out internalObjectData))
                                {
                                    // Debug.Log("Searching:"+ iterator.propertyPath + " " +so.targetObject.GetInstanceID());
                                    job.addInternalAsset(job.assetData.assetPath, so.targetObject.GetInstanceID());
                                    SearchProperty(job, item, so.GetIterator());
                                }

                                // else{
                                //   Debug.Log("Already searched this internal object:"+so.targetObject.GetInstanceID());
                                // }
                            }
                            else
                            {
                                // hit recursive depth!
                                Debug.Log("[Search & Replace] Recursive depth hit!");
                            }

                            recursiveDepth--;
                        }
                    }

                searchPropertyInternal(job, item, prop, iterator);
            }

            if (typeHidesName(prop.serializedObject.targetObject))
            {
                var nameProp = prop.serializedObject.FindProperty("m_Name");
                searchPropertyInternal(job, item, prop, nameProp);
            }
        }

        // Some object types have the visibility of the name unseen. This function
        // looks at the type and returns true if its of this type.
        bool typeHidesName(Object obj)
        {
            if (
                obj is ScriptableObject ||
                obj is Material ||
                obj is Texture ||
                obj is AudioClip
            )
                return true;
            return false;
        }

        void searchPropertyInternal(SearchJob job, SearchItem item, SerializedProperty prop,
            SerializedProperty iterator)
        {
            if (iterator.propertyType == typeField.PropertyType())
                if (typeField.ValueEqualsWithConditional(iterator))
                {
                    if (item.subsearch != null)
                    {
                        var subItem = (SearchItem) item.subsearch;
                        subItem.SubsearchProperty(job, prop);
                    }
                    else
                    {
                        var result = new SearchResultProperty(iterator, typeField.StringValueFor(iterator), job);
                        // If the prefab type is 'instance' then only add the
                        // search result if it is a modification to the original
                        // prefab.
                        // We iterate over prefab separately and the PrefabType will simply 
                        // be 'Prefab' and we should handle that before we handle scenes.
                        if (result.pathInfo.prefabInstanceStatus != PrefabInstanceStatus.NotAPrefab)
                        {
                            if (job.options.SearchType == SearchType.SearchAndReplace)
                                result.actionTaken = SearchAction.InstanceReplaced;
                            else
                                result.actionTaken = SearchAction.InstanceFound;
                            // if(item.subsearch == null && PrefabUtil.isInstanceModification(iterator))
                            // {
                            //Apply change, only if change already exists!
                            typeField.ReplaceProperty(job, iterator, result);
                            // }
                            job.MatchFound(result, item);
                        }
                        else
                        {
                            if (item.subsearch == null) typeField.ReplaceProperty(job, iterator, result);
                            job.MatchFound(result, item);
                        }
                    }
                }
        }

        bool Next(SerializedProperty prop, bool visible)
        {
            if (visible)
                return prop.NextVisible(true);
            return prop.Next(true);
        }

        public override void SearchProperty(SearchJob job, SerializedProperty prop)
        {
            SearchProperty(job, this, prop);
        }

        public override void OnSearchBegin()
        {
            searchAsGameObject = typeField.GetGameObject();
            typeField.OnSearchBegin();
        }

        public override void OnSearchEnd(SearchJob job)
        {
            typeField.OnSearchEnd(job, this);
        }

        public override void SearchObject(SearchJob job, Object obj)
        {
            job.assetData.searchExecuted = true;
            typeField.SearchObject(job, this, obj);
        }

        public override void SearchGameObject(SearchJob job, GameObject go)
        {
            job.assetData.searchExecuted = true;
            //We only do this search if its a game object AND its a search (NOT a replace).
            if (searchAsGameObject != null && job.options.SearchType == SearchType.Search && subsearch == null)
            {
                // if(searchAsGameObject == go)
                // {
                //   Debug.Log("[SearchItemGlobal] FOUND THING");
                // }
#if UNITY_2018_2 || UNITY_2018_3 || UNITY_2018_4
        UnityEngine.Object goPrefab = PrefabUtility.GetCorrespondingObjectFromSource(go);
#else
                var goPrefab = PrefabUtility.GetCorrespondingObjectFromSource(go);
#endif

                // Debug.Log("[SearchItemGlobal] goPrefab:"+goPrefab);
                if (goPrefab != null && goPrefab == searchAsGameObject)
                {
                    var result = new SearchResult();
                    result.strRep = "";
                    result.pathInfo = PathInfo.GetPathInfo(go, job);
                    result.actionTaken = SearchAction.InstanceFound;
                    job.MatchFound(result, this);
                }
            }

            typeField.SearchGameObject(job, this, go);
        }


        //This seems unnecessary as long as I use 'nextVisible'?
        public bool ignorePropertyOfType(SerializedProperty prop)
        {
            var ignore = false;
            if (prop.serializedObject.targetObject is Material)
            {
                //don't return the transform's internal m_children var.
                ignore = prop.propertyPath.IndexOf("first.name") > -1;
                if (ignore) return true;
            }

            return ignore;
        }

        public override bool IsValid()
        {
            return typeField.IsValid();
        }

        public override bool IsReplaceValid()
        {
            return
                typeField.IsReplaceValid(); //Typefield has implicit checks for search being valid in addition to search and replace
        }

        public override string GetWarning()
        {
            if (depth == 0) return typeField.GetWarning();
            return string.Empty;
        }

        public override string GetDescription()
        {
            return typeField.StringValueWithConditional();
        }

        public override bool hasAdvancedOptions()
        {
            return typeField.hasAdvancedOptions();
        }
    }
}