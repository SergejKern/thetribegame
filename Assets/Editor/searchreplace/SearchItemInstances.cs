#define PSR_FULL
#if DEMO
#undef PSR_FULL
#endif
using System;
using searchreplace;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    /**
     * The search item for searching for instances of a specific prefab.
     */
    [Serializable]
    public class SearchItemInstances : SearchItem
    {
        ObjectID objID;

        ReplaceItemPrefabInstance replaceItem;

        [NonSerialized] bool searchValid;

        public override void Draw(SearchOptions options)
        {
            GUILayout.BeginHorizontal(SRWindow.searchBox);
            SRWindow.Instance.CVS();
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            var lw = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = SRWindow.compactLabelWidth;

            var newObj = EditorGUILayout.ObjectField("Prefab:", objID.obj, typeof(Object), true,
                GUILayout.MaxWidth(SRWindow.Instance.position.width - SRWindow.boxPad));
            EditorGUIUtility.labelWidth = lw; // i love stateful gui! :(

            if (objID.obj != newObj)
            {
                newObj = PrefabUtil.getPrefabRoot(newObj);

                objID.SetObject(newObj);
                searchValid = validateSearch(objID.obj);
                SRWindow.Instance.PersistCurrentSearch();
            }

            GUILayout.EndHorizontal();

            DrawValidSearch(searchValid, objID.obj);
#if PSR_FULL
            if (options.SearchType == SearchType.SearchAndReplace) replaceItem.Draw();
#endif


            drawAddRemoveButtons();
            GUILayout.EndVertical();
            SRWindow.Instance.CVE();

            GUILayout.EndHorizontal();
        }

        bool inSamePrefab(Object objA, Object objB)
        {
            var aPrefab = PrefabUtility.GetPrefabInstanceHandle(objA);
            var bPrefab = PrefabUtility.GetPrefabInstanceHandle(objB);
            return aPrefab == bPrefab;
        }

        public void Swap()
        {
            var obj = objID.obj;
            objID.SetObject(replaceItem.objID.obj);
            replaceItem.objID.SetObject(obj);
            SRWindow.Instance.PersistCurrentSearch();
        }

        public void DrawValidSearch(bool isValid, Object obj)
        {
            if (!isValid)
            {
                var warningInfo = "";
                if (obj != null)
                {
                    var type = PrefabUtility.GetPrefabInstanceStatus(obj);
                    if (type != PrefabInstanceStatus.NotAPrefab)
                        warningInfo = obj.name + " is a prefab instance, not a prefab.";
                    else
                        warningInfo = obj.name + " is not a prefab root.";
                }

                if (warningInfo.Length > 0) EditorGUILayout.HelpBox(warningInfo, MessageType.Warning);
            }
        }

        public bool validateSearch(Object obj)
        {
            return PrefabUtil.isPrefabRoot(obj);
        }

        public void InitWithObject(Object obj)
        {
            if (objID == null) objID = new ObjectID();
            obj = PrefabUtil.getPrefabRoot(obj);
            objID.SetObject(obj);
            OnDeserialization();
        }

        // Fixes nulls in serialization manually...*sigh*.
        public override void OnDeserialization()
        {
            if (objID == null) objID = new ObjectID();
            objID.OnDeserialization();
            searchValid = validateSearch(objID.obj);

            if (replaceItem == null) replaceItem = new ReplaceItemPrefabInstance();
            replaceItem.parentSearchItem = this;
            replaceItem.OnDeserialization();
        }

        public void SearchProperty(SearchJob job, SearchItem item, SerializedProperty prop)
        {
        }

        public override void SearchProperty(SearchJob job, SerializedProperty prop)
        {
            SearchProperty(job, this, prop);
        }

        public override void SearchObject(SearchJob job, Object obj)
        {
        }

        public override void SearchGameObject(SearchJob job, GameObject go)
        {
            job.assetData.searchExecuted = true;
            // Debug.Log("[SearchItemInstances] GO:"+go.name);
            var goType = PrefabUtility.GetPrefabInstanceStatus(go);
            if (goType != PrefabInstanceStatus.NotAPrefab)
            {
#if UNITY_2018_2 || UNITY_2018_3 || UNITY_2018_4
        UnityEngine.Object goPrefabObj = PrefabUtility.GetCorrespondingObjectFromSource(go);
#else
                var goPrefabObj = PrefabUtility.GetCorrespondingObjectFromSource(go);
#endif
                var root = PrefabUtility.GetOutermostPrefabInstanceRoot(go);
                var isRootOfPrefab = root == go;
                if (isRootOfPrefab && goPrefabObj == objID.obj)
                {
                    //Instance found!
                    // Debug.Log("[SearchItemInstances] instance found!"+go.name);
                    var result = new SearchResult();
                    result.strRep = "";
                    result.pathInfo = PathInfo.GetPathInfo(go, job);
                    result.actionTaken = SearchAction.InstanceFound;
                    job.MatchFound(result, this);
                    replaceItem.ReplaceInstance(job, go, result);
                }
            }
        }

        public override bool IsValid()
        {
            return searchValid && !Application.isPlaying;
        }

        public override bool IsReplaceValid()
        {
            return IsValid() && replaceItem.IsValid();
        }

        public override string GetDescription()
        {
            return objID.obj.name;
        }

        public override bool hasAdvancedOptions()
        {
            return false;
        }

        public override bool canSubsearch()
        {
            return false;
        }

        public override bool caresAboutAsset(SearchJob job)
        {
            //we only care about scenes, because that is only where instances can live.
            if ((job.assetData.assetScope & AssetScope.Scenes) == AssetScope.Scenes)
                return job.assetData.assetExtension == ".unity";
            return false;
        }
    }
}