using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    /**
     * Provides a search field for UnityEngine.Object.
     */
    [Serializable]
    public class DynamicTypeObject : DynamicTypeData<ObjectID>
    {
        public static List<Type> replaceOptionTypes = new List<Type>
        {
            typeof(ReplaceItemObject)
        };

        // Whether the currently selected object is a type that allows for subobjects.
        bool hasSubObject;

        bool overrideSafeSearch;

        [NonSerialized] bool searchScopeOK;

        //Whether or not the user has selected they want to search for sub objects of
        // this object.
        bool searchSubObjects = true;

        [NonSerialized] bool searchTypesOK;

        // Whether the type of search we are currently doing allows us to do a search
        // for sub-objects.
        bool shouldSearchSubObjects;

        [NonSerialized] Dictionary<Object, string> subObjects;

        public override void Draw(SearchOptions options)
        {
            drawControlStart();
            var newValue = EditorGUILayout.ObjectField(searchValue.obj, parent.type, true);
            drawControlEnd();
            if (newValue != null && !parent.type.IsInstanceOfType(newValue)) newValue = null;
            if (newValue != searchValue.obj)
            {
                searchValue.SetObject(newValue);
                hashSubObjects();
                replaceItem.OnDTDUpdate();
                SRWindow.Instance.PersistCurrentSearch();
            }

            hasSubObject = hasSubObjects();
            if (shouldSearchSubObjects && hasSubObject)
            {
                var newSearchSubObjects =
                    EditorGUILayout.Toggle("Search for " + subObjectsType() + "?", searchSubObjects);
                if (newSearchSubObjects != searchSubObjects)
                {
                    searchSubObjects = newSearchSubObjects;
                    hashSubObjects();
                    SRWindow.Instance.PersistCurrentSearch();
                }
            }

            if (searchValue.obj is Material && Application.isPlaying)
                EditorGUILayout.HelpBox(
                    "Searching materials while the game is playing may not return results due to Unity copying the material under the hood. It is still possible to search inside materials for textures, colors, and other data.",
                    MessageType.Warning);

            if (searchValue.isSceneObject && !SRWindow.Instance.isSearchingInScene())
            {
                if (SRWindow.Instance.getCurrentScope() == ProjectScope.CurrentSelection)
                {
                    // We've got a correct scope, but the selection is not a 
                    // scene object! Try to help by informing the user that they have to
                    // select something in the scene first.
                    EditorGUILayout.HelpBox(
                        "Cannot search the current selection because scene objects can only be searched for inside scenes.",
                        MessageType.Warning);
                    searchScopeOK = false;
                }
                else
                {
                    // We've got an incorrect scope for the scene object! Try to help by
                    // informing the user this is an invalid search and give them an 
                    // option out of it.
                    EditorGUILayout.HelpBox(
                        "The current selection is a scene object and you aren't searching the current scene.",
                        MessageType.Warning);
                    searchScopeOK = false;

                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Set Scope to the Current Scene"))
                    {
                        SRWindow.Instance.changeScopeTo(ProjectScope.CurrentScene);
                        searchScopeOK = true;
                    }

                    GUILayout.EndHorizontal();
                }

                // Debug.Log("[Search & Replace] Objects within a scene cannot be searched for outside of a scene. ");
            }
            else
            {
                searchScopeOK = true;
            }

            drawReplaceItem(options);
            if (replaceItem is ReplaceItemObject rio)
            {
                var ro = rio.replaceValue.obj;
                if (ro != null &&
                    searchValue.obj != null &&
                    !searchValue.obj.GetType().IsInstanceOfType(ro) &&
                    options.SearchType == SearchType.SearchAndReplace
                )
                {
                    EditorGUILayout.HelpBox("The objects you're trying to search and replace are incompatible.",
                        MessageType.Warning);
                    searchTypesOK = false;
                }
                else
                {
                    searchTypesOK = true;
                }
            }
            else
            {
                searchTypesOK = true;
            }

            if (searchTypesOK)
                return;
            // It is possible that users may want to search for disparate object which
            // is highly unsafe. For example an interface may change from a scriptable
            // object to a monobehaviour or vice versa.
            var newOverride = EditorGUILayout.Toggle("Allow Unsafe Search", overrideSafeSearch);
            if (newOverride == overrideSafeSearch)
                return;

            overrideSafeSearch = newOverride;
            SRWindow.Instance.PersistCurrentSearch();
        }

        public override void OnDeserialization()
        {
            initReplaceOptions(replaceOptionTypes);
            if (searchValue == null) searchValue = new ObjectID();
            searchValue.OnDeserialization();
            hashSubObjects();
        }

        public override bool ValueEquals(SerializedProperty prop)
        {
            var o = prop.objectReferenceValue;
            var isObject = searchValue.obj == o;
            if (isObject) return true;
            if (o == null) return false;
            if (shouldSearchSubObjects && searchSubObjects)
                return subObjects.ContainsKey(prop.objectReferenceValue);
            return false;
        }

        public override bool IsValid()
        {
            var safeSearch = searchTypesOK || overrideSafeSearch;
            return safeSearch && searchScopeOK;
        }

        public override bool IsReplaceValid()
        {
            if (searchSubObjects && hasSubObject) return false;
            return IsValid();
        }


        public override string StringValue()
        {
            if (searchValue.obj == null) return "null";
            var retVal = searchValue.obj.name;
            if (shouldSearchSubObjects && searchSubObjects && hasSubObject) retVal += " (and " + subObjectsType() + ")";
            return retVal;
        }

        public override string StringValueFor(SerializedProperty prop)
        {
            var o = prop.objectReferenceValue;
            if (o == null) return "null";
            if (searchValue.obj == o) return o.name;
            if (shouldSearchSubObjects && searchSubObjects && hasSubObject) return subObjects[o];
            return "";
        }


        public override SerializedPropertyType PropertyType()
        {
            return SerializedPropertyType.ObjectReference;
        }

        public override bool _hasAdvancedOptions()
        {
            return true;
        }

        protected override void initializeDefaultValue(SerializedProperty prop)
        {
            searchValue.SetObject(prop.objectReferenceValue);
        }

        public override string GetWarning()
        {
            return searchValue.obj == null ? 
                "Searching for NULL globally will return a large number of results. Are you sure you want to proceed?" 
                : string.Empty;
        }

        public override void OnSelect(InitializationContext ic)
        {
            shouldSearchSubObjects = ic.fieldData.fieldDataType == FieldData.FieldDataType.Weak;
            base.OnSelect(ic);
        }

        bool hasSubObjects()
        {
            var obj = searchValue.obj;
            if (obj is GameObject)
                return true;
            if (!(obj is Texture2D))
                return false;

            var objects = AssetDatabase.LoadAllAssetsAtPath(searchValue.assetPath);
            return objects.Length > 1;
        }

        string subObjectsType()
        {
            var obj = searchValue.obj;
            if (obj is GameObject) return "children & components";
            if (obj is Texture2D) return "sprites";
            return "";
        }

        // Hashes our object into sub-objects that will also be searched for.
        void hashSubObjects()
        {
            if (subObjects == null)
                subObjects = new Dictionary<Object, string>();
            hasSubObject = hasSubObjects();
            if (!searchSubObjects || !hasSubObject)
            {
                subObjects.Clear();
                return;
            } 

            var obj = searchValue.obj;
            switch (obj)
            {
                case GameObject go:
                {
                    AddGameObjectsAndComponentsRecursively(go);
                    break;
                }
                case Texture2D _:
                {
                    var objects = AssetDatabase.LoadAllAssetsAtPath(searchValue.assetPath);
                    foreach (var o in objects) subObjects[o] = o.name + " (Sprite)";
                    break;
                }
            }
        }

        void AddGameObjectsAndComponentsRecursively(GameObject go)
        {
            var components = go.GetComponents<Component>();
            foreach (var c in components)
                if (c != null)
                    subObjects[c] = c.name + "." + c.GetType();

            var tr = go.transform;
            for (var i =0; i < tr.childCount; ++i)
                AddGameObjectsAndComponentsRecursively(tr.GetChild(i).gameObject);
        }
    }
}