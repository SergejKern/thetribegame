using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace searchreplace
{
    /***
     * Provides a somewhat hacky but workable way to manage search/replace parameters
     * for replacable properties.
     */
    public class AssetImporterMethodUtil
    {
        static readonly Dictionary<string, string> propertyMethodHash = new Dictionary<string, string>
        {
            {"m_TextureSettings.m_FilterMode", "set_filterMode"},
            {"m_TextureSettings.m_WrapMode", "set_wrapMode"},
            {"m_IsReadable", "set_isReadable"},
            {"m_LoadInBackground", "set_loadInBackground"},
            {"m_PreloadAudioData", "set_preloadAudioData"}
        };

        public static bool HasImporterMethod(string propertyPath)
        {
            return propertyMethodHash.ContainsKey(propertyPath);
        }

        public static MethodInfo GetMethodForProperty(AssetImporter importer, SerializedProperty prop)
        {
            var t = importer.GetType();
            if (!propertyMethodHash.TryGetValue(prop.propertyPath, out var methodName))
                return null;

            var m = t.GetMethod(methodName);
            return m;
        }
    }
}