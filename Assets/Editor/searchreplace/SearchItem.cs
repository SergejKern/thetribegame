#define PSR_FULL
#if DEMO
#undef PSR_FULL
#endif
using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace sr
{
    /** 
     * The base class for a search item. There are three types of search items:
     * SearchItemGlobal - Searches based on object type globally.
     * SearchItemProperty - Searches a property of an object.
     * SearchItemInstances - Searches for instances of a prefab.
     * These can be nested in the form of 'subsearches'.
     *
     * In general when a search is performed there are three different paths based
     * on the type of search occurring:
     * SearchProperty() - searching a specific SerializedProperty of an object.
     * SearchGameObject() - searching everything inside a game object.
     * SearchObject() - searching a UnityEngine.Object (material/texture/etc)
     * How these searches occur is dependent upon the sub-classes.
     *
     * When a SearchJob is initialized, it will create SearchSubJobs which then
     * call these functions.
     */
    [Serializable]
    public class SearchItem : INestable
    {
        public static string subScopeRecurseLabel = "Child Objects?";

        [NonSerialized] public SearchItem parent;

        [NonSerialized] public SearchItem root;

        [NonSerialized] public int searchDepth;

        public bool showMoreOptions;

        //HACK! Used for sorting, but also for insertion!
        public int sortIndex;
        public SubsearchScope subScope = SubsearchScope.GameObject;
        public bool subScopeRecurse = true;

        //HACK! Unity throws a big fat warning when you nest in your serialization.
        // I'm using an interface and that seems to do the trick. :(
        public INestable subsearch;

        public virtual string GetDescription()
        {
            return string.Empty;
        }

        public virtual string GetWarning()
        {
            return string.Empty;
        }

        public virtual bool IsValid()
        {
            return false;
        }

        public virtual bool IsReplaceValid()
        {
            return false;
        }

        public virtual void OnDeserialization()
        {
        }

        public virtual bool caresAboutAsset(SearchJob job)
        {
            return true;
        }


        protected void OnDeserializeSubSearch()
        {
            //initialize subsearches
            if (subsearch != null)
            {
                var item = (SearchItem) subsearch;
                item.searchDepth = searchDepth + 1;
                item.parent = this;
                item.root = root;
                item.OnDeserialization();
            }
        }

        public virtual void Draw(SearchOptions options)
        {
        }

        public virtual void DrawResult(SearchResult result)
        {
        }


        protected void drawSubsearch()
        {
            var lw = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = SRWindow.compactLabelWidth;
            if (searchDepth > 0)
            {
                GUILayout.BeginHorizontal();
                var newSubscope = (SubsearchScope) EditorGUILayout.EnumPopup("Scope:", subScope);
                if (newSubscope != subScope)
                {
                    subScope = newSubscope;
                    SRWindow.Instance.PersistCurrentSearch();
                }

                if (subScope == SubsearchScope.SameObject)
                {
                    GUI.enabled = false;
                    EditorGUILayout.ToggleLeft(subScopeRecurseLabel, false);
                    GUI.enabled = true;
                }
                else
                {
                    var newSubScopeRecurse = EditorGUILayout.ToggleLeft(subScopeRecurseLabel, subScopeRecurse);
                    if (newSubScopeRecurse != subScopeRecurse)
                    {
                        subScopeRecurse = newSubScopeRecurse;
                        SRWindow.Instance.PersistCurrentSearch();
                    }
                }

                GUILayout.EndHorizontal();
            }

            EditorGUIUtility.labelWidth = lw; // i love stateful gui! :(
        }


        // Called when the parent has made a match.
        // public virtual
        public virtual void SubsearchProperty(SearchJob job, SerializedProperty prop)
        {
            GameObject go;
            switch (subScope)
            {
                case SubsearchScope.ReturnValue:
                    searchReturnedValue(job, prop);
                    break;
                case SubsearchScope.SameObject:
                    searchSameObject(job, prop);
                    break;
                case SubsearchScope.GameObject:
                    searchGameObject(job, prop, subScopeRecurse);
                    break;
                case SubsearchScope.Children:
                    go = getGameObjectFromProperty(prop);
                    if (go != null)
                        foreach (Transform child in go.transform)
                            doSubSearch(child.gameObject, job, subScopeRecurse);
                    break;
                case SubsearchScope.Parent:
                    go = getGameObjectFromProperty(prop);
                    if (go != null && go.transform.parent != null)
                        doSubSearch(go.transform.parent.gameObject, job, subScopeRecurse);
                    break;
                case SubsearchScope.Prefab:
                    var prefabRoot = PrefabUtil.getPrefabRoot(prop.serializedObject.targetObject);
                    if (prefabRoot != null) doSubSearch(prefabRoot, job, subScopeRecurse);
                    break;
            }
        }

        void searchReturnedValue(SearchJob job, SerializedProperty prop)
        {
            if (prop.propertyType == SerializedPropertyType.ObjectReference)
                if (prop.objectReferenceValue != null)
                {
                    var so = new SerializedObject(prop.objectReferenceValue);
                    var iterator = so.GetIterator();
                    if (subScopeRecurse)
                        searchGameObject(job, iterator, subScopeRecurse);
                    else
                        SearchProperty(job, iterator);
                }
        }

        void searchSameObject(SearchJob job, SerializedProperty prop)
        {
            var iterator = prop.serializedObject.GetIterator();
            SearchProperty(job, iterator);
        }

        void searchGameObject(SearchJob job, SerializedProperty prop, bool recurse)
        {
            var go = getGameObjectFromProperty(prop);
            if (go == null)
                searchSameObject(job, prop);
            else
                doSubSearch(go, job, recurse);
        }

        GameObject getGameObjectFromProperty(SerializedProperty prop)
        {
            var obj = prop.serializedObject.targetObject;
            if (obj is GameObject)
            {
                var go = (GameObject) obj;
                return go;
            }

            if (obj is Component)
            {
                var m = (Component) obj;
                return m.gameObject;
            }

            return null;
        }

        void doSubSearch(GameObject go, SearchJob job, bool recurse)
        {
            var so = new SerializedObject(go);
            var iterator = so.GetIterator();
            SearchProperty(job, iterator);
            var components = go.GetComponents<Component>();
            foreach (var c in components)
                if (c != null)
                {
                    var sc = new SerializedObject(c);
                    var sci = sc.GetIterator();
                    SearchProperty(job, sci);
                }

            if (recurse)
                foreach (Transform child in go.transform)
                    doSubSearch(child.gameObject, job, recurse);
        }

        public virtual void SearchProperty(SearchJob job, SerializedProperty prop)
        {
        }

        public virtual void SearchGameObject(SearchJob job, GameObject obj)
        {
        }

        public virtual void SearchObject(SearchJob job, Object obj)
        {
        }

        public virtual void OnSearchBegin()
        {
        }

        public virtual void OnSearchEnd(SearchJob job)
        {
        }

        public virtual bool hasAdvancedOptions()
        {
            return false;
        }

        public SearchItem Copy()
        {
            return new SearchItem();
        }

        public virtual bool canSubsearch()
        {
            return subsearch == null;
        }

        public void drawAddRemoveButtons()
        {
            GUILayout.Space(2.0f);
            GUILayout.BeginHorizontal(); // 2
            if (searchDepth == 0)
                if (GUILayout.Button(GUIContent.none, SRWindow.olPlusPlus))
                    SRWindow.Instance.duplicateSearchItem(this);
            GUILayout.FlexibleSpace();
#if PSR_FULL
            if (canSubsearch())
            {
                var selectedIndex =
                    EditorGUILayout.Popup("", 0, new[] {"Add Subsearch", Keys.PropertyLabel, Keys.GlobalLabel});
                if (selectedIndex > 0)
                {
                    if (selectedIndex == 1)
                    {
                        if (this is SearchItemProperty)
                            subsearch = (SearchItemProperty) SerializationUtil.Copy(this);
                        else
                            subsearch = new SearchItemProperty();
                    }

                    if (selectedIndex == 2)
                    {
                        if (this is SearchItemGlobal)
                            subsearch = (SearchItemGlobal) SerializationUtil.Copy(this);
                        else
                            subsearch = new SearchItemGlobal();
                    }

                    OnDeserializeSubSearch();
                    SRWindow.Instance.PersistCurrentSearch();
                }
            }

            GUILayout.FlexibleSpace();
#endif
            var showMinus = true;
            if (searchDepth == 0 && sortIndex == 0 && SRWindow.Instance.currentSearch.items.Count == 1)
                showMinus = false;
            if (showMinus)
                if (GUILayout.Button(GUIContent.none, SRWindow.olMinusMinus))
                {
                    if (searchDepth == 0)
                    {
                        SRWindow.Instance.RemoveSearchItem(this);
                    }
                    else
                    {
                        parent.subsearch = null;
                        SRWindow.Instance.PersistCurrentSearch();
                    }
                }

            GUILayout.EndHorizontal(); // 2
        }
    }
}