using System;
using System.Collections.Generic;
using sr;
using UnityEditor;

namespace searchreplace
{
    /**
     * Encapsulates a search field for searching for collection types.
     */
    [Serializable]
    public class DynamicTypeCollection : DynamicTypeData
    {
        [NonSerialized] InitializationContext initializationContext;

        // The type of object that is in the collection.
        DynamicTypeField typeField;

        public override void Draw(SearchOptions options)
        {
            if (isCollection())
            {
                setSubtype();
                // Debug.Log("[DynamicTypeCollection] array of "+subType);
                initTypeField();
                typeField.showMoreOptions = showMoreOptions;
                typeField.SetType(initializationContext);
                typeField.Draw(options);
            }
        }

        void initTypeField()
        {
            if (typeField != null) return;
            typeField = new DynamicTypeField();
            typeField.OnDeserialization();
        }

        void setSubtype()
        {
            var subType = parent.type.IsArray ? 
                parent.type.GetElementType() 
                : parent.type.GetGenericArguments()[0];
            initializationContext = new InitializationContext(subType);
        }

        // This needs to set the subtype correctly, otherwise showAdvancedOptions
        // will return false during layout, then true afterwards causing a 
        // mismatch in layout data.
        // don't use the ic passed in...that's the wrong type!
        public override void OnSelect(InitializationContext ic)
        {
            setSubtype();
            typeField?.SetType(initializationContext);
        }

        public override void OnDeserialization()
        {
            if (typeField == null)
                return;

            typeField.OnDeserialization();
            typeField.showMoreOptions = parent.showMoreOptions;
        }

        bool isCollection()
        {
            if (parent.type.IsArray) return true;
            return parent.type.IsGenericType &&
                   parent.type.GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        public override void SearchProperty(SearchJob job, SearchItem item, SerializedProperty prop)
        {
            if (!isCollection())
                return;

            //Time for some freakish magic!
            var iterator = prop.Copy();
            iterator.Next(true);
            iterator.Next(true);
            var count = iterator.intValue;
            for (var i = 0; i < count; i++)
            {
                iterator.Next(false);
                typeField.SearchProperty(job, item, iterator);
            }
        }

        public override void ReplaceProperty(SearchJob job, SerializedProperty prop, SearchResult result)
        {
            typeField.ReplaceProperty(job, prop, result);
        }

        public override bool IsValid()
        {
            if (!isCollection())
                return false;

            initTypeField();
            return typeField.IsValid();
        }

        public override bool IsReplaceValid()
        {
            if (!isCollection())
                return false;

            initTypeField();
            return typeField.IsReplaceValid();
        }

        public override string StringValue()
        {
            if (!isCollection())
                return "";

            initTypeField();
            return typeField.StringValue();
        }

        public override string StringValueWithConditional()
        {
            if (!isCollection())
                return "";

            initTypeField();
            return typeField.StringValueWithConditional();
        }

        public override bool _hasAdvancedOptions()
        {
            if (!isCollection())
                return false;

            initTypeField();
            return typeField.hasAdvancedOptions();
        }
    }
}