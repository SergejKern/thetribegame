%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: UpperArmMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: LeftElbow/LeftHand
    m_Weight: 1
  - m_Path: LeftElbow/LeftArm
    m_Weight: 1
  - m_Path: RightElbow/Mod_R_Hand
    m_Weight: 1
  - m_Path: RightElbow/RightArm
    m_Weight: 1
  - m_Path: RightElbow
    m_Weight: 1
  - m_Path: RightElbow/RightArm
    m_Weight: 1
  - m_Path: RightElbow/Mod_R_Hand
    m_Weight: 1
  - m_Path: RightElbow/RightHand
    m_Weight: 1
  - m_Path: LeftElbow
    m_Weight: 1
  - m_Path: LeftElbow/LeftArm
    m_Weight: 1
  - m_Path: LeftElbow/Mod_L_Hand
    m_Weight: 1
  - m_Path: LeftElbow/LeftHand
    m_Weight: 1
  - m_Path: LeftElbow/Mod_L_Hand
    m_Weight: 1
  - m_Path: RightElbow/RightHand
    m_Weight: 1