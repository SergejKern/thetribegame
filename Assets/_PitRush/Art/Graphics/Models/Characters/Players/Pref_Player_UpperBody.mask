%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Pref_Player_UpperBody
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 0
  - m_Path: metarig/main
    m_Weight: 0
  - m_Path: metarig/main/footControl_L
    m_Weight: 0
  - m_Path: metarig/main/footControl_L/foot_L
    m_Weight: 1
  - m_Path: metarig/main/footControl_L/foot_L/toe_L
    m_Weight: 1
  - m_Path: metarig/main/footControl_L/foot_roll_L
    m_Weight: 1
  - m_Path: metarig/main/footControl_L/foot_roll_L/foot_handle
    m_Weight: 1
  - m_Path: metarig/main/footControl_L/toe_control_L
    m_Weight: 1
  - m_Path: metarig/main/footControl_R
    m_Weight: 0
  - m_Path: metarig/main/footControl_R/foot_R
    m_Weight: 1
  - m_Path: metarig/main/footControl_R/foot_R/toe_R
    m_Weight: 1
  - m_Path: metarig/main/footControl_R/foot_roll_R
    m_Weight: 1
  - m_Path: metarig/main/footControl_R/foot_roll_R/foot_handle.001
    m_Weight: 1
  - m_Path: metarig/main/footControl_R/toe_control_R
    m_Weight: 1
  - m_Path: metarig/main/hip
    m_Weight: 0
  - m_Path: metarig/main/hip/leg_L
    m_Weight: 0
  - m_Path: metarig/main/hip/leg_L/upleg_L
    m_Weight: 1
  - m_Path: metarig/main/hip/leg_R
    m_Weight: 0
  - m_Path: metarig/main/hip/leg_R/upleg_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/cordel01
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/cordel01/cordel02
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head
    m_Weight: 0
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/hair01_A
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/hair01_A/hair01_B
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/hair02_A
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/hair02_A/nair02_B
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/tail_root
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/tail_root/tail01
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/tail_root/tail01/tail02
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/tail_root/tail01/tail02/tail03
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/tail_root/tail01/tail02/tail03/tail04
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/tail_root/tail01/tail02/tail03/tail04/tail05
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/wushl_L01
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/wushl_L01/wushl_L02
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/wushl_R01
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/head/wushl_R01/wushl_R02
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/fingerControl_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/index01_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/index01_L/f_index.02.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/index01_L/f_index.02.L/f_index.03.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/middle01_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/middle01_L/f_middle.02.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/middle01_L/f_middle.02.L/f_middle.03.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/pinkyo1_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/pinkyo1_L/f_pinky.02.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/pinkyo1_L/f_pinky.02.L/f_pinky.03.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/ring01_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/ring01_L/f_ring.02.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/ring01_L/f_ring.02.L/f_ring.03.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/thumb01_L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/thumb01_L/thumb.02.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/forearm_L/hand_L/thumb01_L/thumb.02.L/thumb.03.L
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/shoulderPlate_L02
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_L/arm_L/shoulderPlate_L02/shoulderPlate_L01
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/fingerControl_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/inder01_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/inder01_R/f_index.02.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/inder01_R/f_index.02.R/f_index.03.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/middle01_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/middle01_R/f_middle.02.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/middle01_R/f_middle.02.R/f_middle.03.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/pinky01_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/pinky01_R/f_pinky.02.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/pinky01_R/f_pinky.02.R/f_pinky.03.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/ring01_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/ring01_R/f_ring.02.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/ring01_R/f_ring.02.R/f_ring.03.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/thumb01_R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/thumb01_R/thumb.02.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/forearm_R/hand_R/thumb01_R/thumb.02.R/thumb.03.R
    m_Weight: 1
  - m_Path: metarig/main/hip/spine01/spine02/neck/shoulder_R/arm_R/shoulderPlate_R
    m_Weight: 1
  - m_Path: metarig/main/pole_L
    m_Weight: 0
  - m_Path: metarig/main/pole_R
    m_Weight: 0
  - m_Path: Player One
    m_Weight: 0
