%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: EnemyDefault_UpperBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: LeftShoulder
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/Finger3
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/Finger2
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/Finger1
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/Thumb
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/Shield
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/EnemyWeapon
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/EnemyWeapon/DamageCollider
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/EnemyWeapon/DamageCollider/EffectPos
    m_Weight: 1
  - m_Path: LeftShoulder/LeftArm/LeftElbow/Puncher/EnemyWeapon/Puncher
    m_Weight: 1
  - m_Path: RightShoulder
    m_Weight: 1
  - m_Path: RightShoulder/RightArm
    m_Weight: 1
  - m_Path: RightShoulder/RightArm/RightArmGun
    m_Weight: 1
  - m_Path: RightShoulder/RightArm/RightArmGun/Model
    m_Weight: 1
  - m_Path: RightShoulder/RightArm/RightArmGun/Muzzle
    m_Weight: 1
  - m_Path: RightShoulder/RightArm/RightArmGun/Muzzle/LaserSwordPreview
    m_Weight: 1
