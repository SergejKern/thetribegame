﻿using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Components;
using GameUtility.Feature.Exploding;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Components.Actions
{
    /// <summary>
    /// Action Component to explode into parts
    /// AC = Action Component
    /// </summary>
    public class ExplodingAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] DamageHandlerComponent m_damageHandlerComponent;
        [SerializeField] GameObject m_prefab;
        [SerializeField] GameObject m_explodingObject;

        [SerializeField] float m_force = 25f;
        [SerializeField] float m_radius = 10f;
        [SerializeField] float m_upwardsMod;

        [SerializeField] Vector3 m_explodingPosRandomization = 0.1f * Vector3.one;

        [SerializeField] RefIAudioAsset m_explosionSound;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [UsedImplicitly]
        public void Explode()
        {
            if (m_prefab != null)
                Pool.Instance.DelayInactivate(m_prefab, 1);
            if (m_explodingObject == null)
            {
                var tr = transform;
                m_explodingObject = m_prefab.GetPooledInstance(tr.position + m_upwardsMod*Vector3.up, tr.rotation);
            }

            if (!gameObject.TryGetComponent(out DecomposeBodyRC decompose))
                decompose = gameObject.AddComponent<DecomposeBodyRC>();
            decompose.DoDecompose();

            ExplodeParts.ApplyExplosion(m_explodingObject,
                m_damageHandlerComponent.LastDamagePos, m_explodingPosRandomization,
                m_force, m_radius, m_upwardsMod);
            m_explosionSound.Result?.Play(m_explodingObject);
        }
    }
}
