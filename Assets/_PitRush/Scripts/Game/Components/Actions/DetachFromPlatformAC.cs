﻿using Game.Arena;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Components.Actions
{
    /// <summary>
    /// Detaches static objects (non-actors) from platforms
    /// AC = Action Component
    /// </summary>
    public class DetachFromPlatformAC : MonoBehaviour
    {
        [UsedImplicitly]
        public void DetachFromPlatformParent() => DetachFromPlatform(transform.parent);

        [UsedImplicitly]
        public void DetachFromPlatform(Transform platformTr)
        {
            platformTr.TryGetComponent(out ArenaPlatform p);
            p.RemoveStaticFromPlatform(transform);
        }

    }
}
