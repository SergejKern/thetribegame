﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Game.Tutorial;
using GameUtility.Events;
using GameUtility.Interface;
using UnityEngine; //using GameUtility.Energy;

namespace Game.Components.Behaviour
{
    public class RevivableTriggerBehaviour : MonoBehaviour, IInteractiveHolding, IHighlightable, IPoolable
    {
        enum ReviveState
        {
            Idle,
            Reviving,
            Revived
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_defaultAnim;
        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_reviveChargeAnim;
        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_doneAnim;
        [SerializeField] PrefabRef m_tutorialButton;

        // todo 4: probably add state-switch functionality for enums and actions that you can link on state switches
        [SerializeField] RefIAudioAsset m_revivingSound;
        [SerializeField] RefIAudioAsset m_revivedSound;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public bool InteractEnabled => gameObject.activeInHierarchy && enabled && m_reviveState != ReviveState.Revived;
        
        float m_holdValue;
        GameObject m_holdingActor;
        IRevivable m_revivableActor;
        // IHealthComponent m_healthComponent;

        GameObject m_revivableGo;
        float m_timeToRevive;
        ReviveState m_reviveState;

        struct TutorialButtonData
        {
            public GameObject Go;
            public TutorialButton Button;
        }
        TutorialButtonData m_tutorialButtonInstance;

        // public void OnEnable() => m_defaultAnim.Animator.Play(m_defaultAnim);
        void Update()
        {
            transform.rotation = Quaternion.identity;

            switch (m_reviveState)
            {
                case ReviveState.Idle:
                    CheckForExternalRevive(); 
                    break;
                case ReviveState.Reviving: 
                    CheckForExternalRevive();
                    UpdateReviving(); 
                    break;
                case ReviveState.Revived: UpdateRevived(); break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        void CheckForExternalRevive()
        {
            m_revivableActor.Get(out var hp);
            if (hp.HP > 0) 
                OnRevive();
        }

        void UpdateRevived()
        {
            if (m_doneAnim.Animator.IsPlaying(m_doneAnim.StateName))
                return;
            DisableHighlight();
            gameObject.TryDespawnOrDestroy();
        }

        void UpdateReviving()
        {
            var prevHold = m_holdValue;
            m_holdValue = Mathf.Clamp01(prevHold + Time.deltaTime / m_timeToRevive);
            if (Math.Abs(prevHold - m_holdValue) < float.Epsilon)
                return;

            if (m_holdValue < 1.0f)
            {
                if (m_tutorialButtonInstance.Button != null)
                    m_tutorialButtonInstance.Button.SetHoldValue(m_holdValue);

                m_reviveChargeAnim.Animator.Play(m_reviveChargeAnim.StateHash, 0, m_holdValue);
                return;
            }
            ReviveEvent.Trigger(m_revivableGo, m_holdingActor);

            OnRevive();
        }

        void OnRevive()
        {
            if (m_tutorialButtonInstance.Go != null)
                m_tutorialButtonInstance.Go.TryDespawnOrDestroy();

            m_holdingActor = null;
            m_doneAnim.Animator.Play(m_doneAnim);
            m_revivableActor.OnRevive();

            m_reviveState = ReviveState.Revived;
            m_revivingSound.Result?.Stop(gameObject);
            m_revivedSound.Result?.Play(gameObject);
        }

        public void Init(IRevivable revivable)
        {
            m_revivableActor = revivable;
            m_timeToRevive = revivable.TimeToRevive;
            // revivable.Get(out m_healthComponent);
            m_revivableGo = null;
            if (m_revivableActor is MonoBehaviour mb)
                m_revivableGo = mb.gameObject;
        }

        public void Interact(GameObject actor)
        {
            if (m_reviveState != ReviveState.Idle)
                return;
            if (m_revivableGo == actor)
                return;

            m_holdingActor = actor;
            m_revivingSound.Result?.Play(gameObject);

            if (m_tutorialButtonInstance.Button != null)
                m_tutorialButtonInstance.Button.SetHoldValue(0f);

            m_reviveState = ReviveState.Reviving;
            m_holdValue = 0f;
        }

        public void OnRelease(GameObject actor)
        {
            if (m_holdingActor != actor)
                return;
            if (m_holdValue >= 1f)
                return;
            m_holdingActor = null;
            m_defaultAnim.Animator.Play(m_defaultAnim);
            m_revivingSound.Result?.Stop(gameObject);

            if (m_tutorialButtonInstance.Button != null)
                m_tutorialButtonInstance.Button.EnableAnimator();

            m_reviveState = ReviveState.Idle;
            m_holdValue = 0;
        }

        public void OnSpawn() => Reset();
        public void OnDespawn()
        {
            //Reset();
        }

        void Reset()
        {
            m_holdValue = 0;
            m_holdingActor = null;
            m_reviveState = ReviveState.Idle;
            m_defaultAnim.Animator.Play(m_defaultAnim);
            m_revivingSound.Result?.Stop(gameObject);
        }

        public void EnableHighlight()
        {
            if (!gameObject.activeInHierarchy || !enabled)
                return;
            
            if (!InteractEnabled)
                return;
            if (m_tutorialButtonInstance.Go != null) 
                return;
            m_tutorialButtonInstance.Go = m_tutorialButton.GetInstance();
            m_tutorialButtonInstance.Go.TryGetComponent(out m_tutorialButtonInstance.Button);
        }

        public void DisableHighlight()
        {
            if (m_tutorialButtonInstance.Go != null)
                m_tutorialButtonInstance.Go.TryDespawnOrDestroy();
            m_tutorialButtonInstance.Go = null;
        }
    }
}
