﻿using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.InteractiveObjects;
using UnityEngine;

namespace Game.Components.Behaviour
{
    public class SpawnBulletBehaviour : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warning
        [SerializeField] Transform m_actor;

        [SerializeField] float m_secondsBetweenBullets;
        [SerializeField] GameObject m_bulletPrefab;
        [SerializeField] PrefabData m_bulletSpawnEffect;
        [SerializeField] RefIAudioAsset m_bulletSpawnSound;
#pragma warning restore 0649 // wrong warning

        float m_time;

        void Update() => UpdateBulletSpawning();

        void UpdateBulletSpawning()
        {
            m_time += Time.deltaTime;
            if (!(m_time > m_secondsBetweenBullets)) return;
            m_time -= m_secondsBetweenBullets;

            SpawnBullet();
        }

        void SpawnBullet()
        {
            var tr = transform;

            var effect = m_bulletSpawnEffect.Prefab;
            if (effect != null)
            {
                var fxGo = effect.GetPooledInstance(tr.position, tr.rotation);
                fxGo.transform.SetParent(tr);
            }

            m_bulletSpawnSound?.Result?.Play(gameObject);

            var instance = m_bulletPrefab.GetPooledInstance(tr.position, tr.rotation);
            instance.TryGetComponent(out Bullet b);
            b.Initialize(m_actor);
        }
    }
}
