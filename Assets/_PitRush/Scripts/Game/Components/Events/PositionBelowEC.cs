﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Components.Events
{
    /// <summary>
    /// UnityEvent when position is below specific value
    /// EC = Event Component
    /// </summary>
    public class PositionBelowEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_yValue;
        [SerializeField] UnityEvent m_onBelow;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void Update()
        {
            if (transform.position.y < m_yValue)
                m_onBelow.Invoke();
        }
    }
}
