﻿using UnityEngine;
using Core.Unity.Types;

namespace Game.Components.Events
{
    /// <summary>
    /// Component to link actions when OnPlatformMoveChangeEvent is fired
    /// EC = Event Component
    /// </summary>
    public class PlatformMoveEC : MonoBehaviour, IPlatformMoveListener
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] TransformEvent m_onPlatformMoveStart;
        [SerializeField] TransformEvent m_onPlatformMoveEnd;

        [SerializeField] TransformEvent m_onPlatformMoveStartUpDown;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void OnPlatformMoveChangeEvent(Transform platformTr, bool isMoving, bool upDown)
        {
            if (isMoving)
            {
                if (upDown)
                    m_onPlatformMoveStartUpDown?.Invoke(platformTr);
                m_onPlatformMoveStart.Invoke(platformTr);
            }   
            else m_onPlatformMoveEnd.Invoke(platformTr);
        }
    }

    public interface IPlatformMoveListener
    {
        //void OnPlatformMoveStartEvent(Transform platformTr);
        //void OnPlatformMoveEndEvent(Transform platformTr);
        void OnPlatformMoveChangeEvent(Transform platformTr, bool isMoving, bool upDown);
    }
}
