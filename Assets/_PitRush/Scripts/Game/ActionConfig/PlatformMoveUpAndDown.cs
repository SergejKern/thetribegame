﻿using System;
using System.Collections.Generic;
using Core.Types.VariableWrapper;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using DG.Tweening;
using Game.Arena;
using Game.Globals;
using GameUtility.Feature.Tween;
using JetBrains.Annotations;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;
using UnityEngine.AI;

namespace Game.ActionConfig
{
    public class PlatformMoveUpAndDown : ScriptableBaseAction, IVerify
#if UNITY_EDITOR
        , IArenaSelectionAction
#endif
    {
        public FloatReference MoveToHeight;
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PlatformTweenConfig m_config;

        [DefaultAssetSync(nameof(NavMeshSync), nameof(DoGetStatic_NavMesh), nameof(DoSetStatic_NavMesh)),
         VarDrawer, SerializeField]
        GameObjectReference m_navMesh;
#pragma warning restore 0649 // wrong warnings for SerializeField

        string HeightLevelString => Math.Abs(MoveToHeight.Value) < float.Epsilon 
            ? "Default Level" : 
            MoveToHeight.Value < 0f 
                ? "Bottom Level" 
                : "Top Level";
        public override string Name => $"MOVE {(MoveToHeight.Scriptable==null? HeightLevelString:MoveToHeight.Scriptable.name)}";

        static Type StaticFactoryType => typeof(PlatformMoveUpAndDownAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            MoveToHeight.Init(ctx);
            m_navMesh.Init(ctx);
            return new PlatformMoveUpAndDownAction(MoveToHeight, m_config, m_navMesh);
        }

        public void Verify(ref VerificationResult result)
        {
            if (MoveToHeight.IsNone())
                result.Error("MoveToHeight variable not set",this);
            if (m_config == null)
                result.Error("config (PlatformTweenConfig) variable not set",this);
            if (m_navMesh.IsNone())
                result.Error("Nav Mesh variable not set",this);
        }

        #region Default Asset Sync
        [UsedImplicitly]
        static object NavMeshSync { get => RebuildNavMesh.NavMeshSync; set => RebuildNavMesh.NavMeshSync = value; }
        bool DoGetStatic_NavMesh => m_navMesh.Scriptable == null;
        bool DoSetStatic_NavMesh => m_navMesh.Scriptable != null;
        #endregion

#if UNITY_EDITOR
        public IArenaSelection Selection { get; set; }
#endif
    }

    public class PlatformMoveUpAndDownAction : IUpdatingAction, IInstantCompletableAction, IStoppableAction, IArenaSelectionAction
    {
        public PlatformMoveUpAndDownAction(FloatVar height, 
            PlatformTweenConfig tweenConfig,
            GameObject navMeshObject)
        {
            MoveHeight = height;
            m_tweenConfig = tweenConfig;

            m_navMeshSurface = null;
            if (navMeshObject != null)
                navMeshObject.TryGetComponent(out m_navMeshSurface);
        }

        public IArenaSelection Selection { get; set; }
        public float MoveHeight { get; }

        readonly PlatformTweenConfig m_tweenConfig;

        readonly NavMeshSurface m_navMeshSurface;

        Sequence m_tween;
        ArenaPlatformGroup m_group;

        bool TweenFinished => m_tween != null
                              && (!m_tween.IsActive() || m_tween.IsComplete());
        public bool IsFinished => TweenFinished;

        public void Init()
        {
            m_tween = DOTween.Sequence();
            m_tween.SetUpdate(UpdateType.Fixed);

            // todo 4: if moving down release parenting of group when down, 
            // if moving up check whether there is a non-default group that wants to move our target platforms, set parent to that group
            // immediately and add to group regularly when done
            var usingGroup = Math.Abs(m_tweenConfig.DurationVariance) < float.Epsilon;
            if (usingGroup)
                InitTweenWithGroup();
            else
                InitTweenWithoutGroup();

            m_tween.AppendCallback(OnDone);
            m_tween.Play();
        }

        void InitTweenWithoutGroup()
        {
            ClearGroup();

            foreach (var platform in Selection.Platforms)
                InitIndividualPlatformTween(platform);
        }

        void InitTweenWithGroup()
        {
            InitMoveGroup(out var groupMove);
            foreach (var platform in Selection.Platforms)
            {
                InitialPlatformActions(platform, out var seq);
                m_tween.Insert(0, seq);
            }

            m_tween.Append(groupMove);
            var allPlatformsFinish = DOTween.Sequence();

            foreach (var platform in Selection.Platforms)
            {
                var seq = DOTween.Sequence();
                FinalizePlatformSequence(platform, seq);
                allPlatformsFinish.Insert(0, seq);
            }

            m_tween.Append(allPlatformsFinish);
        }

        void InitMoveGroup(out Tween moveTween)
        {
            var mGroup = GameGlobals.Prefabs.GetPrefab(Prefab.PlatformMovingGroup).GetPooledInstance();
            mGroup.TryGetComponent(out m_group);
            mGroup.TryGetComponent(out m_group.Body);

            m_group.BuildGroupFromSelection(Selection);
            var duration = Mathf.Abs(m_group.Body.position.y - MoveHeight) * m_tweenConfig.DurationPerUnit;

            moveTween = GetPlatformMoveTween(m_group.Body, m_group.Body.position, duration);
        }

        void ClearGroup()
        {
            m_group = null;
            ArenaPlatformGroup.UpdateMovingGroups(Selection, null);
        }

        void InitialPlatformActions(ArenaPlatform platform, out Sequence seq)
        {
            seq = DOTween.Sequence();

            platform.MovingAction = this;

            if (m_tweenConfig.WiggleConfig != null)
                PlatformWiggleOperation.Wiggle(m_tweenConfig.WiggleConfig, platform, seq);
            seq.AppendCallback(() => m_tweenConfig.MoveSound.Result?.Play(platform.gameObject));
        }

        void InitIndividualPlatformTween(ArenaPlatform platform)
        {
            if (!GetDataForInitPlatform(platform, out var body, out var pos, out var duration))
                return;

            // avoid dragging agents to death
            platform.RemoveAllAgents();
            platform.SetRailingsActive(false);

            InitialPlatformActions(platform, out var platformSeq);

            var moveTween = GetPlatformMoveTween(body, pos, duration);
            platformSeq.Append(moveTween);

            FinalizePlatformSequence(platform, platformSeq);

            m_tween.Insert(0, platformSeq);
        }

        void FinalizePlatformSequence(ArenaPlatform platform, Sequence platformSeq)
        {
            platformSeq.AppendCallback(() =>
            {
                platform.MovingAction = null;
                //platform.SetDirty();
                m_tweenConfig.MoveEndSound.Result?.Play(platform.gameObject);
            });
            if (m_tweenConfig.WiggleConfig != null)
                platformSeq.AppendCallback(() => PlatformWiggleOperation.Revert(m_tweenConfig.WiggleConfig, platform));
        }

        Tween GetPlatformMoveTween(Rigidbody body, Vector3 pos, float duration)
        {
            Tween tween = body.DOMove(new Vector3(pos.x, MoveHeight, pos.z), duration); //.SetRelative(false);

            if (m_tweenConfig.Ease > Ease.Unset && m_tweenConfig.Ease < Ease.INTERNAL_Zero)
                tween.SetEase(m_tweenConfig.Ease);
            else
                tween.SetEase(m_tweenConfig.Curve);
            return tween;
        }

        bool GetDataForInitPlatform(Component platform, out Rigidbody body, out Vector3 pos, out float duration)
        {
            body = null;
            duration = -1;
            pos = default;

            if (platform == null)
                return false;
            body = platform.GetComponent<Rigidbody>();
            if (body == null)
                return false;

            pos = body.position;
            duration = Mathf.Abs(pos.y - MoveHeight) * m_tweenConfig.DurationPerUnit;
            duration += duration * (-0.5f + 2 * UnityEngine.Random.value) * m_tweenConfig.DurationVariance;
            return true;
        }

        public void Update(float dt) { }

        public void InstantComplete()
        {
            m_tween?.Complete();

            InstantComplete(Selection?.Platforms, MoveHeight);
        }

        public static void InstantComplete(IEnumerable<ArenaPlatform> platforms, float h)
        {
            if (platforms == null)
                return;
            foreach (var platform in platforms)
            {
                if (platform == null)
                    continue;
                var tr = platform.transform;
                var pos = tr.position;
                tr.position = new Vector3(pos.x,h,pos.z);
            }
        }

        void OnDone()
        {
            if (m_navMeshSurface != null)
                m_navMeshSurface.BuildNavMesh();

            var putToDefaultGroup = Math.Abs(MoveHeight) < float.Epsilon;

            if (putToDefaultGroup)
                ArenaPlatformGroup.DefaultGroup.AddSelection(Selection);
            else 
                ArenaPlatformGroup.UpdateMovingGroups(Selection, null);

            if (m_group != null)
                m_group.gameObject.TryDespawnOrDestroy();
        }

        public void Stop()
        {
            m_tween?.Kill();
            foreach (var platform in Selection.Platforms)
            {
                if (platform == null)
                    continue;

                platform.MovingAction = null;
                //platform.SetDirty();
                m_tweenConfig.MoveEndSound.Result?.Play(platform.gameObject);
                if (m_tweenConfig.WiggleConfig != null)
                    PlatformWiggleOperation.Revert(m_tweenConfig.WiggleConfig, platform);
            }
        }
    }
}
