﻿using System;
using Core.Extensions;
using Core.Types.VariableWrapper;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using DG.Tweening;
using Game.Arena;
using Game.Arena.Level;
using Game.Globals;
using GameUtility.Feature.Tween;
using JetBrains.Annotations;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;


namespace Game.ActionConfig
{
    public class PlatformRotation : ScriptableBaseAction, IVerify
#if UNITY_EDITOR
    , IArenaSelectionAction
#endif
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        public int Segments;
        [FormerlySerializedAs("m_config")]
        [SerializeField] PlatformTweenConfig m_tweenConfig;

        [SerializeField] PlatformTweenConfig m_repeatedTweenConfig;
        [SerializeField] PlatformTweenConfig m_finalizeTweenConfig;

        [DefaultAssetSync(nameof(NavMeshSync), nameof(DoGetStatic_NavMesh), nameof(DoSetStatic_NavMesh)),
         VarDrawer, SerializeField]
        GameObjectReference m_navMesh;
        [SerializeField] BoolReference m_repeatBool;
        [SerializeField] bool m_invertRepeatBool;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"Rotate {Segments} segments";
        static Type StaticFactoryType => typeof(PlatformRotationAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            m_navMesh.Init(ctx);
            m_repeatBool.Init(ctx);
            return new PlatformRotationAction(Segments, m_tweenConfig, m_repeatedTweenConfig, m_finalizeTweenConfig,
                m_navMesh, m_repeatBool, m_invertRepeatBool);
        }

        public void Verify(ref VerificationResult result)
        {
            if (Segments == 0)
                result.Error("Segments to rotate is 0",this);
            if (m_tweenConfig == null)
                result.Error("tween Config is null",this);

            var nonRepeat= m_repeatBool.ContextType == ReferenceContextType.Intrinsic && m_repeatBool.IntrinsicValue == false 
                           ||m_repeatBool.IsNone();
            if (!nonRepeat)
            {
                if (m_repeatedTweenConfig == null)
                    result.Error("repeated tween Config is null, but repeat bool is set",this);
                if (m_finalizeTweenConfig == null)
                    result.Error("finalize tween Config is null, but repeat bool is set",this);
            }

            if (m_navMesh.IsNone())
                result.Error("No NavMesh set",this);

            if (m_repeatBool.ContextType == ReferenceContextType.Intrinsic && m_repeatBool.IntrinsicValue)
                result.Warning("Repeat Bool is set to true intrinsically on Action, platforms might rotate forever", this);
        }

        #region Default Asset Sync
        [UsedImplicitly]
        static object NavMeshSync { get => RebuildNavMesh.NavMeshSync; set => RebuildNavMesh.NavMeshSync = value; }
        bool DoGetStatic_NavMesh => m_navMesh.Scriptable == null;
        bool DoSetStatic_NavMesh => m_navMesh.Scriptable != null;
        #endregion

#if  UNITY_EDITOR
        public IArenaSelection Selection { get; set; }
#endif
    }

    public class PlatformRotationAction : IUpdatingAction, IInstantCompletableAction, IStoppableAction, IArenaSelectionAction
    {
        enum State
        {
            Default,
            Starting,
            Repeating,
            Stopping
        }

        public PlatformRotationAction(int rotSegments,
            PlatformTweenConfig config, PlatformTweenConfig repeated, PlatformTweenConfig stopping,
            GameObject navMeshObject, BoolVar repeat, bool invert)
        {
            m_tweenConfig = config;
            m_repeatedConfig = repeated;
            m_stoppingConfig = stopping;
            m_rotationSegments = rotSegments;

            m_navMeshSurface = null;
            m_repeat = repeat;
            m_invert = invert;

            if (navMeshObject != null)
                navMeshObject.TryGetComponent(out m_navMeshSurface);
        }

        public IArenaSelection Selection { get; set; }

        readonly int m_rotationSegments;

        readonly PlatformTweenConfig m_tweenConfig;
        readonly PlatformTweenConfig m_repeatedConfig;
        readonly PlatformTweenConfig m_stoppingConfig;

        readonly NavMeshSurface m_navMeshSurface;
        readonly BoolVar m_repeat;
        readonly bool m_invert;

        ArenaPlatformGroup m_group;

        Sequence m_tween;
        State m_state;

        bool DoRepeat => m_invert ? !m_repeat.Value : m_repeat.Value;
        bool TweenFinished => m_tween != null 
                              && (!m_tween.IsActive() || m_tween.IsComplete());
        public bool IsFinished => TweenFinished && (m_state == State.Default || m_state == State.Stopping);

        int m_rotated;
        int m_totalSegments;
        ArenaLevelFlowConfig.ArenaLayoutData m_layout;

        int GetRotationSegments()
        {
            switch (m_state)
            {
                case State.Default: return m_rotationSegments;
                case State.Starting: return 1 * Math.Sign(m_rotationSegments);
                case State.Repeating: return 1 * Math.Sign(m_rotationSegments);
                case State.Stopping: return m_rotationSegments - (m_rotated % m_totalSegments);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Init()
        {
            m_state = DoRepeat ? State.Starting : State.Default;
            m_group = null;
            m_rotated = 0;

            if (!Selection.Platforms.IsNullOrEmpty())
            {
                m_layout = Selection.Platforms[0].ID.LayoutData;
                if (m_layout.Config is ArenaLayoutConfigCircular circ)
                    m_totalSegments = circ.Segments;
            }

            InitTween();
        }

        // ReSharper disable once FlagArgument
        void InitTween()
        {
            m_tween = DOTween.Sequence();
            m_tween.SetUpdate(UpdateType.Fixed);

            InitTweenWithGroup();

            m_tween.AppendCallback(OnDone);
            m_tween.Play();
        }

        void InitTweenWithGroup()
        {
            InitMoveGroup(out var groupMove);
            foreach (var platform in Selection.Platforms)
            {
                InitialPlatformActions(platform, out var seq);
                m_tween.Insert(0, seq);
            }

            m_tween.Append(groupMove);
            var allPlatformsFinish = DOTween.Sequence();
            foreach (var platform in Selection.Platforms)
            {
                var seq = DOTween.Sequence();
                FinalizePlatformSequence(platform, seq, GetTweenConfig());
                allPlatformsFinish.Insert(0, seq);
            }

            m_tween.Append(allPlatformsFinish);
        }

        void InitMoveGroup(out Tween moveTween)
        {
            if (m_group == null)
            {
                var mGroup = GameGlobals.Prefabs.GetPrefab(Prefab.PlatformMovingGroup).GetPooledInstance();
                mGroup.TryGetComponent(out m_group);
                mGroup.TryGetComponent(out m_group.Body);

                m_group.BuildGroupFromSelection(Selection, m_layout.Position);
            }

            var tweenConfig = GetTweenConfig();
            if (tweenConfig == null) Debug.LogError($"Tween Config null State: {m_state}");

            var segments = GetRotationSegments();
            Debug.Log($"InitMoveGroup segments {segments}");
            var vec = RotationVector(Selection.Platforms[0], segments);

            moveTween = GetPlatformRotateTween(m_group.Body, vec, tweenConfig);
        }

        void InitialPlatformActions(ArenaPlatform platform, out Sequence seq)
        {
            seq = DOTween.Sequence();

            platform.MovingAction = this;
            var tweenConfig = GetTweenConfig();
            if (tweenConfig.WiggleConfig != null)
                PlatformWiggleOperation.Wiggle(tweenConfig.WiggleConfig, platform, seq);
            seq.AppendCallback(() => tweenConfig.MoveSound.Result?.Play(platform.gameObject));
        }

        void FinalizePlatformSequence(ArenaPlatform platform, Sequence platformSeq, PlatformTweenConfig tweenConfig)
        {
            platformSeq.AppendCallback(() =>
            {
                platform.MovingAction = null;
                platform.SetDirty();
                tweenConfig.MoveEndSound.Result?.Play(platform.gameObject);
            });

            if (m_state == State.Starting || m_state == State.Repeating)
                return;

            if (tweenConfig.WiggleConfig != null)
                platformSeq.AppendCallback(() => PlatformWiggleOperation.Revert(tweenConfig.WiggleConfig, platform));
        }

        Tween GetPlatformRotateTween(Rigidbody body, Vector3 vec, PlatformTweenConfig config)
        {
            var duration = Mathf.Abs(GetRotationSegments()) * config.DurationPerUnit;
            Tween tween = body.DORotate(vec, duration).SetRelative(true);

            if (config.Ease > Ease.Unset && config.Ease < Ease.INTERNAL_Zero)
                tween.SetEase(config.Ease);
            else
                tween.SetEase(config.Curve);
            return tween;
        }

        static int PositiveModulo(int k, int n) { return ((k %= n) < 0) ? k + n : k; }

        public void Update(float dt)
        {
            if (m_state == State.Default || m_state == State.Stopping)
                return;

            if (!TweenFinished)
                return;
            if (m_state == State.Starting)
                m_state = State.Repeating;

            var keepRepeat = DoRepeat;
            if (m_state == State.Repeating && !keepRepeat)
                m_state = State.Stopping;

            if (TweenFinished)
                InitTween();
        }

        PlatformTweenConfig GetTweenConfig()
        {
            switch (m_state)
            {
                case State.Default: return m_tweenConfig;
                case State.Starting: return m_tweenConfig;
                case State.Repeating: return m_repeatedConfig;
                case State.Stopping: return m_stoppingConfig;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public void InstantComplete()
        {
            if (m_tween != null)
                m_tween.Complete();
            else
                InstantComplete(Selection, m_rotationSegments);
        }

        static Vector3 RotationVector(ArenaPlatform p, int segments)
        {
            var circularLayout = p.ID.LayoutData.Config as ArenaLayoutConfigCircular;
            if (circularLayout == null)
            {
                Debug.LogError("Applying platform rotation on non-circular layout! Unexpected behaviour!");
                return Vector3.zero;
            }   
            
            var anglePerSeg = circularLayout.AnglesPerSegment;
            var vec = Vector3.up * (anglePerSeg * segments);
            return vec;
        }
        public static void InstantComplete(IArenaSelection selectionConfig, int segments)
        {
            var platforms = selectionConfig.Platforms;
            foreach (var platform in platforms)
            {
                if (platform == null)
                    continue;
                var vec = RotationVector(platform, segments);

                var tr = platform.transform;
                tr.rotation = Quaternion.Euler(vec) * tr.rotation;

                ChangeIndex(selectionConfig, segments, platform);
            }
        }

        void OnDone()
        {
            if (m_navMeshSurface != null)
                m_navMeshSurface.BuildNavMesh();

            UpdateIndices();

            if (DoRepeat || m_state == State.Repeating) 
                return;
            ArenaPlatformGroup.DefaultGroup.AddSelection(Selection);
            m_group.gameObject.TryDespawnOrDestroy();
        }

        void UpdateIndices()
        {
            var seg = GetRotationSegments();
            foreach (var platform in Selection.Platforms)
                ChangeIndex(Selection, GetRotationSegments(), platform);
            m_rotated += seg;
        }

        static void ChangeIndex(IArenaSelection selectionConfig, int segments, ArenaPlatform platform)
        {
            var circularLayout = platform.ID.LayoutData.Config as ArenaLayoutConfigCircular;
            if (circularLayout == null)
                return;
            platform.GetIndexCircular(out var segment, out var ringIdx, out var platformIdx);
            var newSeg = PositiveModulo(segment + segments, circularLayout.Segments);
            var newIdx = CircularPlatformExtensions.CreateIndexCircular(newSeg, ringIdx, platformIdx);

            selectionConfig.ChangeIndex(platform, newIdx);
        }

        public void Stop()
        {
            m_tween?.Kill();
            var tweenConfig = GetTweenConfig();
            foreach (var platform in Selection.Platforms)
            {
                if (platform == null)
                    continue;

                platform.MovingAction = null;
                //platform.SetDirty();

                tweenConfig.MoveEndSound.Result?.Play(platform.gameObject);
                if (tweenConfig.WiggleConfig != null)
                    PlatformWiggleOperation.Revert(tweenConfig.WiggleConfig, platform);
            }
        }
    }

}
