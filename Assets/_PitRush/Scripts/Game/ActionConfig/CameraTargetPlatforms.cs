﻿using System;
using Core.Types.VariableWrapper;
using Game.Arena;
using Game.Globals;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables.Reference;

namespace Game.ActionConfig
{
    public class CameraTargetPlatforms : ScriptableBaseAction
    {
        public BoolReference DoTarget;
        public FloatReference Speed;

        public override string Name => $"CAM {(DoTarget ? "target" : "release")} platforms";

        static Type StaticFactoryType => typeof(CameraTargetPlatformsAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            DoTarget.Init(ctx);
            return new CameraTargetPlatformsAction(DoTarget, Speed);
        }
    }

    public class CameraTargetPlatformsAction : IDefaultAction, IInstantCompletableAction, IArenaSelectionAction
    {
        public CameraTargetPlatformsAction(BoolVar target, FloatVar speed)
        {
            m_doTarget = target;
            m_speed = speed;
        }

        public IArenaSelection Selection { get; set; }

        readonly BoolVar m_doTarget;
        readonly FloatVar m_speed;

        public void Invoke()
        {
            var gamCam = GameGlobals.GameCamera;
            if (gamCam == null)
                return;

            foreach (var p in Selection.Platforms)
            {
                if (m_doTarget.Value)
                    gamCam.AddTarget(p.Filter.transform, 0f, m_speed.Value);
                else 
                    gamCam.FadeOutTarget(p.Filter.transform, m_speed.Value);
            }
        }

        public void InstantComplete() => Invoke();
    }
}
