﻿using System;
using Core.Types;
using Core.Types.VariableWrapper;
using Core.Unity.Attribute;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using Game.Arena;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;

using static Core.Unity.Extensions.CoreEnumExtensions;

namespace Game.ActionConfig
{
    public class SpawnPlatform : ScriptableBaseAction, ISerializationCallbackReceiver, IVerify
    #if UNITY_EDITOR
        , IArenaSelectionAction
    #endif
    {
        public bool SpecifyVariant;
        [SerializeField, HideInInspector] 
        string m_variantKey;
        [EnumStringSync(nameof(m_variantKey))]
        public ArenaPrefabVariant Variant;
        public FloatReference Height;

        public override string Name => $"{(SpecifyVariant ? Variant.ToString() : "Preferred")} PLAT_SPWN";

        static Type StaticFactoryType => typeof(SpawnPlatformAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            Height.Init(ctx);
            return new SpawnPlatformAction(Height, SpecifyVariant, Variant);
        }

        public void OnBeforeSerialize() => OnBeforeSerializeEnum(out m_variantKey, Variant);
        public void OnAfterDeserialize() => OnAfterDeserializeEnum(ref Variant, m_variantKey);
        public void Verify(ref VerificationResult result)
        {
            if (Height.IsNone())
                result.Error("No Height specified",this);
        }

#if UNITY_EDITOR
        // for preview
        public IArenaSelection Selection { get; set; }
#endif
    }

    public class SpawnPlatformAction : IDefaultAction, IInstantCompletableAction, IArenaSelectionAction
    {
        public SpawnPlatformAction(FloatVar height, bool specifyVariant, ArenaPrefabVariant variant)
        {
            m_height = height;
            m_specifyVariant = specifyVariant;
            m_variant = variant;
        }

        public IArenaSelection Selection { get; set; }

        readonly float m_height;

        readonly bool m_specifyVariant;
        readonly ArenaPrefabVariant m_variant;

        public void Invoke()
        {
            if (Selection == null)
                return;
            foreach (var id in Selection.SelectionIds)
            {
                if (GetSpawnPlatformData(id, Selection, m_specifyVariant, m_variant, out var data) == OperationResult.Error)
                    continue;

                var platformGo = data.Prefab.GetPooledInstance(data.Pos + Vector3.up * m_height, data.Rot);

                AddToMap(platformGo, id, Selection, data.Variant);
            }

            if (Mathf.Abs(m_height) < float.Epsilon)
                ArenaPlatformGroup.DefaultGroup.AddSelection(Selection);
        }

        public struct SpawnPlatformData
        {
            public ArenaPrefabVariant Variant;
            public GameObject Prefab;
            public Vector3 Pos;
            public Quaternion Rot;
        }
        public static OperationResult GetSpawnPlatformData(ArenaPlatform.PlatformID id, IArenaSelection selection,
            bool specifyVariant, ArenaPrefabVariant variant, out SpawnPlatformData data)
        {
            data = default;
            var exists = selection.GetInstance(id, out var instanceData);
            if (instanceData.PlatformInstance != null)
                return OperationResult.Error;

            data.Variant = specifyVariant
                ? variant
                : (exists
                    ? instanceData.Variant
                    : ArenaPrefabVariant.Default);

            data.Prefab = id.LayoutData.GetPrefab(id.Index, data.Variant);
            id.LayoutData.GetDefaultTransformation(id.Index, data.Prefab, out data.Pos, out data.Rot);
            return OperationResult.OK;
        }

        public static void AddToMap(GameObject platformGo, ArenaPlatform.PlatformID id, IArenaSelection data, ArenaPrefabVariant variant)
        {
            var platform = platformGo.GetComponent<ArenaPlatform>();
            platform.Init(id, variant);

            data.AddPlatform(id, platform);
        }

        public void InstantComplete() => Invoke();
    }
}
