﻿using System;
using System.Linq;
using Game.Arena;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.ActionConfig
{
    // todo 3: TEST this
    public class PlatformSelectionRandomFilter : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] int m_filterPlatforms;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => "Platform Selection Random Filter";

        static Type StaticFactoryType => typeof(PlatformSelectionRandomFilterAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx) 
            => new PlatformSelectionRandomFilterAction(m_filterPlatforms);
    }

    public class PlatformSelectionRandomFilterAction : IDefaultAction, IInstantCompletableAction,IArenaSelectionAction
    {
        public PlatformSelectionRandomFilterAction(int filterPlatforms) => 
            m_filterPlatforms = filterPlatforms;

        public IArenaSelection Selection { get; set; }

        readonly int m_filterPlatforms;

        public void InstantComplete() => Invoke();

        public void Invoke()
        {
            var ids = Selection.SelectionIds.ToList();
            while (ids.Count > m_filterPlatforms) ids.RemoveAt(Random.Range(0, ids.Count));
            Selection.SelectionIds = ids.ToArray();
            Selection.InitConfig(ArenaPlatformMapData.PlatformData);
        }
    }
}
