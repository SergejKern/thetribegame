﻿using System;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types.VariableWrapper;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;

namespace Game.ActionConfig
{
    public class DespawnObject : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObjectReference[] m_spawnedObjects;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => "Despawn";

        static Type StaticFactoryType => typeof(DespawnObjectAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction(IContext ctx)
        {
            var gos = new GameObjectVar[m_spawnedObjects.Length];
            for (var i = 0; i < m_spawnedObjects.Length; i++)
            {
                m_spawnedObjects[i].Init(ctx);
                gos[i] = m_spawnedObjects[i];
            }

            return new DespawnObjectAction(gos);
        }

        public void Verify(ref VerificationResult result)
        {
            if (m_spawnedObjects.IsNullOrEmpty())
            {
                result.Error("m_spawnedObjects is empty", this);
                return;
            }
            for (var index = 0; index < m_spawnedObjects.Length; index++)
            {
                var obj = m_spawnedObjects[index];
                if (obj.IsNone())
                    result.Error($"No spawned Object set at index {index}", this);
            }
        }
    }

    public class DespawnObjectAction : IDefaultAction, IInstantCompletableAction
    {
        public DespawnObjectAction(GameObjectVar[] spawnedObj) => m_spawnedObj = spawnedObj;

        readonly GameObjectVar[] m_spawnedObj;
        public void Invoke()
        {
            //Debug.Log($"despawn {m_spawnedObj.Value}");
            foreach (var obj in m_spawnedObj)
                obj.Value.TryDespawnOrDestroy();
        }

        public void InstantComplete() => Invoke();
    }
}
