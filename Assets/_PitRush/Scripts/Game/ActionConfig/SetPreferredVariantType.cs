﻿using System;
using Core.Unity.Attribute;
using Game.Arena;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;

using static Core.Unity.Extensions.CoreEnumExtensions;

namespace Game.ActionConfig
{
    public class SetPreferredVariantType : ScriptableBaseAction, ISerializationCallbackReceiver
#if UNITY_EDITOR
        , IArenaSelectionAction
#endif
    {
        [SerializeField, HideInInspector] 
        string m_variantKey;
        [EnumStringSync(nameof(m_variantKey))]
        public ArenaPrefabVariant Variant;

        public override string Name => $"SET {Variant} VT";

        static Type StaticFactoryType => typeof(SetPreferredVariantTypeAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            return new SetPreferredVariantTypeAction(Variant);
        }

        public void OnBeforeSerialize() => OnBeforeSerializeEnum(out m_variantKey, Variant);
        public void OnAfterDeserialize() => OnAfterDeserializeEnum(ref Variant, m_variantKey);

#if UNITY_EDITOR
        public IArenaSelection Selection { get; set; }
#endif
    }

    public class SetPreferredVariantTypeAction : IDefaultAction, IInstantCompletableAction, IArenaSelectionAction
    {
        public SetPreferredVariantTypeAction(ArenaPrefabVariant variant) 
            => m_variant = variant;

        public IArenaSelection Selection { get; set; }
        readonly ArenaPrefabVariant m_variant;

        public void Invoke()
        {
            foreach (var idx in Selection.SelectionIds)
                Selection.SetPreferredVariantType(idx, m_variant);
        }

        public void InstantComplete() => Invoke();
    }
}
