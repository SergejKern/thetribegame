﻿using System;
using Game.Globals;
using Game.Utility;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;

namespace Game.ActionConfig
{
    public class ChangeCameraSettings : ScriptableBaseAction
    {
        public GameCameraConfig Config;
        public float TransitionInSeconds;

        string ActionVerb => Config == null? "Revert" : "Set";
        public override string Name => $"CAM {ActionVerb} settings";

        static Type StaticFactoryType => typeof(ChangeCameraSettingsAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx) 
            => new ChangeCameraSettingsAction(Config, TransitionInSeconds);
    }

    public class ChangeCameraSettingsAction : IDefaultAction, IInstantCompletableAction
    {
        public ChangeCameraSettingsAction(GameCameraConfig config, float seconds)
        {
            m_config = config;
            m_seconds = seconds;
        }

        GameCameraConfig m_config;
        readonly float m_seconds;

        public void Invoke()
        {
            var gamCam = GameGlobals.GameCamera;
            if (gamCam == null)
                return;
            if (m_config == null)
                m_config = gamCam.DefaultConfig;

            gamCam.StartCoroutine(gamCam.SwitchCameraSettings(m_config.Settings, m_seconds));
        }

        public void InstantComplete() => Invoke();
    }
}
