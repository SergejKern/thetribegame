﻿using System;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Interface;
using Core.Unity.Types.VariableWrapper;
using Core.Unity.Utility.PoolAttendant;
using Game.Arena;
using GameUtility.Data.TransformAttachment;
using GameUtility.Components.Collision;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables.Reference;
using UnityEngine;

namespace Game.ActionConfig
{
    public class SpawnPrefab : ScriptableBaseAction, IVerify
    {
        public GameObject Prefab;
        public GameObjectReference ParentReference;

        [Serializable]
        public struct LocalTransformData
        {
            public Vector3 LocalPosition;
            public Vector3 LocalRotation;
            public GameObjectReference SpawnedObjectVar;
        }

        public LocalTransformData[] LocalTransformDataList = new LocalTransformData[0];
        public bool AttachToPlatform;

        const float k_castDistance = 200;

        public override string Name => $"{(Prefab!=null?Prefab.name:"Prefab")} PREF_SPWN";

        static Type StaticFactoryType => typeof(SpawnPrefabAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction(IContext ctx)
        {
            ParentReference.Init(ctx);

            var parent = ParentReference.Value == null ? ctx.Transform : ParentReference.Value.transform;

            var l = LocalTransformDataList.Length;
            var data = new TransformData[l];
            var variables = new GameObjectVar[l];

            for (var i = 0; i < l; i++)
            {
                data[i] = new TransformData()
                {
                    LocalPosition = LocalTransformDataList[i].LocalPosition,
                    LocalRotation = LocalTransformDataList[i].LocalRotation,
                    Parent = parent
                };
                LocalTransformDataList[i].SpawnedObjectVar.Init(ctx);
                variables[i] = LocalTransformDataList[i].SpawnedObjectVar;
            }

            var atData = new SpawnPrefabAction.AttachToPlatformData(AttachToPlatform, 
                LayerMask.GetMask("Platform"),
                k_castDistance);

            return new SpawnPrefabAction(Prefab, data, variables, atData);
        }

        public void Verify(ref VerificationResult result)
        {
            if (Prefab == null)
                result.Error("Prefab is not set", this);
            if (LocalTransformDataList.IsNullOrEmpty())
                result.Error("No Transform data set", this);
        }
    }

    public class SpawnPrefabAction : IDefaultAction
    {
        public struct AttachToPlatformData
        {
            public AttachToPlatformData(bool at, LayerMask mask, float dist)
            {
                DoAttach = at;
                PlatformMask = mask;
                MaxCastDistance = dist;
            }

            public readonly bool DoAttach;
            public LayerMask PlatformMask;
            public readonly float MaxCastDistance;
        }

        public SpawnPrefabAction(GameObject spawnPrefab, TransformData[] td, GameObjectVar[] spawnedObj,
            AttachToPlatformData attachData)
        {
            m_prefab = spawnPrefab;

            m_transformData = td;
            m_spawnedObj = spawnedObj;

            m_attachToPlatform = attachData;
        }

        readonly GameObject m_prefab;

        readonly TransformData[] m_transformData;
        readonly GameObjectVar[] m_spawnedObj;

        readonly AttachToPlatformData m_attachToPlatform;


        public void Invoke()
        {
            for (var i = 0; i < m_transformData.Length; i++)
                Spawn(i);
        }

        void Spawn(int idx)
        {
            var pos = m_transformData[idx].Position;
            var spawned = m_prefab.GetPooledInstance(pos, m_transformData[idx].Rotation);
            m_spawnedObj[idx].SetValue(spawned);

            if (m_attachToPlatform.DoAttach)
                AttachToPlatformBelow(spawned, pos);
        }

        void AttachToPlatformBelow(GameObject spawned, Vector3 pos)
        {
            if (!Physics.Raycast(pos + 0.25f * Vector3.up, Vector3.down, out var hit, 
                m_attachToPlatform.MaxCastDistance, m_attachToPlatform.PlatformMask))
            {
                Debug.LogError($"Raycast missed, Not attached {spawned}");
                return;
            }
            var root = RelativeCollider.GetRoot(hit.collider);
            if (!root.TryGetComponent(out ArenaPlatform ap))
            {
                Debug.LogError($"No ArenaPlatform hit, Not attached {spawned}");
                return;
            }

            var spawnedTr = spawned.transform;
            spawnedTr.position = new Vector3(pos.x, hit.point.y, pos.z);

            ap.AddStaticToPlatform(spawnedTr);
        }
    }
}
