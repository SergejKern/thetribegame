﻿using System;
using System.Collections.Generic;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using DG.Tweening;
using Game.Arena;
using Game.Globals;
using GameUtility.Feature.Tween;
using JetBrains.Annotations;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;
using UnityEngine.AI;

namespace Game.ActionConfig
{
    public class PlatformTransformation : ScriptableBaseAction, IVerify
#if UNITY_EDITOR
    , IArenaSelectionAction
#endif
    {
        public Vector3Reference TransformationVector;
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PlatformTweenConfig m_config;

        [DefaultAssetSync(nameof(NavMeshSync), nameof(DoGetStatic_NavMesh), nameof(DoSetStatic_NavMesh)),
         VarDrawer, SerializeField]
        GameObjectReference m_navMesh;

        [SerializeField] bool m_absoluteFromIndexPos;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"Transform {TransformationVector}";

        static Type StaticFactoryType => typeof(PlatformTransformationAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            TransformationVector.Init(ctx);
            m_navMesh.Init(ctx);
            return new PlatformTransformationAction(TransformationVector,
                m_absoluteFromIndexPos,
                m_config, 
                m_navMesh);
        }

        public void Verify(ref VerificationResult result)
        {
            if (TransformationVector.IsNone())
                result.Error("TransformationVector not set",this);
            if (m_config == null)
                result.Error("config (PlatformTweenConfig) not set",this);
            if (m_navMesh.IsNone())
                result.Error("Nav Mesh not set",this);
        }

        #region Default Asset Sync
        [UsedImplicitly]
        static object NavMeshSync { get => RebuildNavMesh.NavMeshSync; set => RebuildNavMesh.NavMeshSync = value; }
        bool DoGetStatic_NavMesh => m_navMesh.Scriptable == null;
        bool DoSetStatic_NavMesh => m_navMesh.Scriptable != null;
        #endregion

#if UNITY_EDITOR
        public IArenaSelection Selection { get; set; }
#endif
    }

    public class PlatformTransformationAction : IUpdatingAction, IInstantCompletableAction, IStoppableAction, IArenaSelectionAction
    {
        public PlatformTransformationAction(Vector3 vec, bool absoluteFromIndexPos, 
            PlatformTweenConfig config,
            GameObject navMeshObject)
        {
            m_vector = vec;
            m_absoluteFromIndexPos = absoluteFromIndexPos;

            m_tweenConfig = config;
            m_duration = vec.magnitude * config.DurationPerUnit;

            m_navMeshSurface = null;
            if (navMeshObject != null)
                navMeshObject.TryGetComponent(out m_navMeshSurface);
        }

        public IArenaSelection Selection { get; set; }

        readonly Vector3 m_vector;
        readonly bool m_absoluteFromIndexPos;

        readonly float m_duration;
        readonly PlatformTweenConfig m_tweenConfig;

        readonly NavMeshSurface m_navMeshSurface;
        ArenaPlatformGroup m_group;

        Sequence m_tween;

        bool TweenFinished => m_tween != null
                              && (!m_tween.IsActive() || m_tween.IsComplete());
        public bool IsFinished => TweenFinished;

        public void Init()
        {
            m_tween = DOTween.Sequence();
            m_tween.SetUpdate(UpdateType.Fixed);

            InitTweenWithGroup();

            m_tween.AppendCallback(OnDone);
            m_tween.Play();
        }

        void InitTweenWithGroup()
        {
            InitMoveGroup(out var groupMove);
            foreach (var platform in Selection.Platforms)
            {
                InitialPlatformActions(platform, out var seq);
                m_tween.Insert(0, seq);
            }

            m_tween.Append(groupMove);
            var allPlatformsFinish = DOTween.Sequence();
            foreach (var platform in Selection.Platforms)
            {
                var seq = DOTween.Sequence();
                FinalizePlatformSequence(platform, seq);
                allPlatformsFinish.Insert(0, seq);
            }

            m_tween.Append(allPlatformsFinish);
        }

        void InitMoveGroup(out Tween moveTween)
        {
            var mGroup = GameGlobals.Prefabs.GetPrefab(Prefab.PlatformMovingGroup).GetPooledInstance();
            mGroup.TryGetComponent(out m_group);
            mGroup.TryGetComponent(out m_group.Body);

            m_group.BuildGroupFromSelection(Selection);

            var platform = Selection.Platforms[0];
            platform.TryGetComponent(out Rigidbody platformBody);
            var trVector = m_absoluteFromIndexPos
                ? GetAbsolutePos(platform)
                : m_vector;
            var duration = m_absoluteFromIndexPos
                ? GetDurationForAbsolutePos(platformBody, trVector)
                : m_duration;

            moveTween = GetPlatformMoveTween(m_group.Body, trVector, duration);
        }

        void InitialPlatformActions(ArenaPlatform platform, out Sequence seq)
        {
            seq = DOTween.Sequence();

            platform.MovingAction = this;
            if (m_tweenConfig.WiggleConfig != null)
                PlatformWiggleOperation.Wiggle(m_tweenConfig.WiggleConfig, platform, seq);
            seq.AppendCallback(() => m_tweenConfig.MoveSound.Result?.Play(platform.gameObject));
        }

        void FinalizePlatformSequence(ArenaPlatform platform, Sequence platformSeq)
        {
            platformSeq.AppendCallback(() =>
            {
                platform.MovingAction = null;
                //platform.SetDirty();
                m_tweenConfig.MoveEndSound.Result?.Play(platform.gameObject);
            });

            if (m_tweenConfig.WiggleConfig != null)
                platformSeq.AppendCallback(() => PlatformWiggleOperation.Revert(m_tweenConfig.WiggleConfig, platform));
        }

        Tween GetPlatformMoveTween(Rigidbody body, Vector3 trVec, float duration)
        {
            Tween tween = body.DOMove(trVec, duration).SetRelative(!m_absoluteFromIndexPos);
            if (m_tweenConfig.Ease > Ease.Unset && m_tweenConfig.Ease < Ease.INTERNAL_Zero)
                tween.SetEase(m_tweenConfig.Ease);
            else
                tween.SetEase(m_tweenConfig.Curve);
            return tween;
        }

        Vector3 GetAbsolutePos(ArenaPlatform platform)
        {
            var layoutData = platform.ID.LayoutData;
            var prefab = layoutData.GetPrefab(platform.ID.Index, platform.Variant);
            layoutData.GetDefaultTransformation(platform.ID.Index, prefab, out var pos, out _);
            return pos + m_vector;
        }

        float GetDurationForAbsolutePos(Rigidbody body, Vector3 pos) 
            => Vector3.Distance(body.position, pos) * m_tweenConfig.DurationPerUnit;

        public void Update(float dt) { }

        public void InstantComplete()
        {
            if (m_tween != null)
                m_tween.Complete();
            else
                InstantComplete(Selection.Platforms, m_vector);
        }

        public static void InstantComplete(IEnumerable<ArenaPlatform> platforms, Vector3 vec)
        {
            foreach (var platform in platforms)
            {
                if (platform == null)
                    continue;
                platform.transform.position += vec;
            }
        }

        void OnDone()
        {
            if (m_navMeshSurface != null)
                m_navMeshSurface.BuildNavMesh();
            
            ArenaPlatformGroup.DefaultGroup.AddSelection(Selection);

            m_group.gameObject.TryDespawnOrDestroy();
        }

        public void Stop()
        {
            m_tween?.Kill();
            foreach (var platform in Selection.Platforms)
            {
                if (platform == null)
                    continue;
                
                platform.MovingAction = null;

                m_tweenConfig.MoveEndSound.Result?.Play(platform.gameObject);
                if (m_tweenConfig.WiggleConfig != null)
                    PlatformWiggleOperation.Revert(m_tweenConfig.WiggleConfig, platform);
            }
        }
    }
}
