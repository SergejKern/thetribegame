﻿using System;
using Core.Unity.Attribute;
using Core.Unity.Interface;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;
using UnityEngine.AI;

namespace Game.ActionConfig
{
    public class RebuildNavMesh : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [DefaultAssetSync(nameof(NavMeshSync), nameof(DoGetStatic_NavMesh), nameof(DoSetStatic_NavMesh)),
         VarDrawer, SerializeField]
        GameObjectReference m_navMesh;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => "Rebuild NavMesh";
        static Type StaticFactoryType => typeof(RebuildNavMeshAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            m_navMesh.Init(ctx);
            return new RebuildNavMeshAction(m_navMesh);
        }

        public void Verify(ref VerificationResult result)
        {
            if (m_navMesh.IsNone())
                result.Error("Nav Mesh not set",this);
        }

        #region Default Asset Sync
        public static object NavMeshSync { get; set; }
        bool DoGetStatic_NavMesh => m_navMesh.Scriptable == null;
        bool DoSetStatic_NavMesh => m_navMesh.Scriptable != null;
        #endregion
    }

    public class RebuildNavMeshAction : IDefaultAction, IInstantCompletableAction
    {
        readonly NavMeshSurface m_navMesh;

        public RebuildNavMeshAction(GameObject navMeshObject) => 
            navMeshObject.TryGetComponent(out m_navMesh);

        public void Invoke() => m_navMesh.BuildNavMesh();
        public void InstantComplete() => Invoke();
    }
}