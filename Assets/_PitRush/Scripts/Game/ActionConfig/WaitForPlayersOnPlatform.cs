﻿using System;
using Core.Extensions;
using Game.Actors.Components;
using Game.Arena;
using Game.Globals;
using GameUtility.Components.Collision;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;


namespace Game.ActionConfig
{
    public class WaitForPlayersOnPlatform : ScriptableBaseAction
    {
        public bool AllPlayers = true;

        public override string Name => "Wait->Players on platform";
        static Type StaticFactoryType => typeof(WaitForPlayersOnPlatform);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx) => new WaitForPlayersOnPlatformAction(AllPlayers);
    }

    public class WaitForPlayersOnPlatformAction : IUpdatingAction,IArenaSelectionAction
    {
        public WaitForPlayersOnPlatformAction(bool allPlayers) 
            => m_allPlayersRequired = allPlayers;

        public IArenaSelection Selection { get; set; }

        readonly bool m_allPlayersRequired;

        PlatformRaycastDownBehaviour[] m_playersCastDowns;
        int m_playersOnCorrectPlatform;

        int PlayerCountRequired => m_allPlayersRequired ? GameGlobals.PlayerCount : 1;
        public bool IsFinished => m_playersOnCorrectPlatform >= PlayerCountRequired;

        public void Init()
        {
            m_playersCastDowns = new PlatformRaycastDownBehaviour[GameGlobals.PlayerCount];
            var i = 0;
            foreach (var player in GameGlobals.Players)
            {
                if (player == null)
                    continue;
                if (!i.IsInRange(m_playersCastDowns))
                    return;
                var prd = player.GetComponent<PlatformRaycastDownBehaviour>();
                m_playersCastDowns[i] = prd;
                ++i;
            }
        }

        public void Update(float dt)
        {
            if (m_playersCastDowns.Length != GameGlobals.PlayerCount)
                Init();

            m_playersOnCorrectPlatform = 0;
            foreach (var platformCast in m_playersCastDowns)
            {
                if (platformCast == null)
                    continue;

                var root = RelativeCollider.GetRoot(platformCast.LastHitGameObject);
                if (root==null)
                    continue;
                var ap = root.GetComponent<ArenaPlatform>();
                if (ap == null)
                    continue;
                if (Array.FindIndex(Selection.SelectionIds, p=>p==ap.ID) !=-1)
                    m_playersOnCorrectPlatform++;
            }
        }
    }
}
