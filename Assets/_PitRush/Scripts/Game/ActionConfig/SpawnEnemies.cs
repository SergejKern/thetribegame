﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using Game.Arena;
using Game.Arena.EnemySpawn;
using Game.Globals;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using UnityEngine;

namespace Game.ActionConfig
{
    public class SpawnEnemies : ScriptableBaseAction, IVerify //, IArenaSelectionAction
    {
        public EnemySpawnData[] NewSpawnData;

        public EnemySpawnOptions Options;
        public SpawnerConfig[] Spawners = new SpawnerConfig[0];

        public IntReference Seed;
        public GameObjectReference SpawnerPrefab;

        public string DebugKey;
        public bool WaitForVisible;
        public bool WaitForMoveOut;

        List<EnemyWaveSpawnEntry> m_lastGeneratedEntries;
        int m_lastGeneratedSeed;

        public override string Name => $"SPAWN {DebugKey} ENEMIES";
        static Type StaticFactoryType => typeof(SpawnEnemiesAction);
        public override Type FactoryType => StaticFactoryType;
        // ArenaSelectionConfig m_selection;

        public override IBaseAction CreateAction(IContext ctx)
        {
            Seed.Init(ctx);
            SpawnerPrefab.Init(ctx);

            var genData = GetGeneratorData();
            return new SpawnEnemiesAction(genData, Spawners, SpawnerPrefab, WaitForVisible, WaitForMoveOut, DebugKey);
        }

        EnemySpawnGeneratorData GetGeneratorData()
        {
            var genDat = new EnemySpawnGeneratorData()
                {Seed = Seed, SpawnerCount = Spawners.Length, SpawnData = NewSpawnData, Options = Options };

            return genDat;
        }

        //public void SetSelectionConfig(ArenaSelectionConfig config) =>
        //    m_selection = config;
        public void Verify(ref VerificationResult result)
        {
            VerifySpawnData(ref result);
            VerifySpawners(ref result);

            if (Seed.IsNone())
                result.Error("No Seed set!", this);
            if (SpawnerPrefab.IsNone())
                result.Error("No SpawnerPrefab set!", this);
            if (DebugKey.IsNullOrEmpty())
                result.Info("No DebugKey set!", this);
        }

        void VerifySpawners(ref VerificationResult result)
        {
            if (Spawners.IsNullOrEmpty())
            {
                result.Error("No Spawners set!", this);
                return;
            }

            var invalidEntries = Spawners.Any(s => !s.ID.IsValid);
            if (invalidEntries)
                result.Error("Invalid entries in Spawners!", this);
        }

        void VerifySpawnData(ref VerificationResult result)
        {
            if (NewSpawnData.IsNullOrEmpty())
            {
                result.Error("No Spawn Data",this);
                return;
            }
            for (var i = 0; i < NewSpawnData.Length; i++)
            {
                var spawnData = NewSpawnData[i];
                if (spawnData.Power <= 0)
                    result.Error($"Power invalid in {i}. SpawnData",this);
                if (spawnData.Delay < 0)
                    result.Error($"Delay invalid in {i}. SpawnData",this);

                if (spawnData.EnemyTypes.IsNullOrEmpty())
                    result.Error($"No EnemyTypes in {i}. SpawnData",this);
                var nullEntries = spawnData.EnemyTypes.Any(ec => ec == null);
                if (nullEntries)
                    result.Error($"Empty entries in EnemyTypes in {i}. SpawnData",this);
            }
        }
    }

    public class SpawnEnemiesAction : IUpdatingAction, IInstantCompletableAction, IStoppableAction
    {
        public SpawnEnemiesAction(EnemySpawnGeneratorData genData, 
            IEnumerable<SpawnerConfig> configs,
            GameObject spawnerPrefab, 
            bool waitForVisible,
            bool waitForMoveOut,
            string debugKey)
        {
            m_genData = genData;
            m_spawnerPrefab = spawnerPrefab;
            m_debugKey = debugKey;
            m_spawnerConfigs = configs;
            m_waitForMoveOut = waitForMoveOut;
            m_waitForVisible = waitForVisible;
        }

        enum State
        {
            WaitForReady,
            Spawning,
            WaitForMoveOut,
            Done
        }

        readonly IEnumerable<SpawnerConfig> m_spawnerConfigs;
        readonly GameObject m_spawnerPrefab;
        readonly string m_debugKey;

        EnemySpawnGeneratorData m_genData;
        List<EnemyWaveSpawnEntry> m_spawnEntries;

        readonly bool m_waitForVisible;
        readonly bool m_waitForMoveOut;

        State m_state;
        List<EnemySpawner> m_spawnersList;
        float m_timer;
        int m_currentSpawnIndex;
        bool m_stopped;

        public bool IsFinished => m_state == State.Done;

        public void Init()
        {
            GenerateEntries();
            InitSpawners(m_spawnerConfigs);
            m_state = State.WaitForReady;
            m_stopped = false;
        }

        void GenerateEntries()
        {
            if (m_spawnEntries != null) return;

            m_genData.PlayerCount = GameGlobals.PlayerCount;
            m_spawnEntries = EnemyWaveGeneration.GenerateSpawnerEntries(ref m_genData);
        }

        void InitSpawners(IEnumerable<SpawnerConfig> spawners)
        {
            m_spawnersList = SimplePool<List<EnemySpawner>>.I.Get();

            foreach (var spawnerData in spawners)
            {
                var spawnerInstance = m_spawnerPrefab.GetPooledInstance();
                var enemySpawner = spawnerInstance.GetComponent<EnemySpawner>();

                var platform = ArenaPlatformMapData.PlatformData.GetPlatform(spawnerData.ID);
                var attachData = Array.Find(platform.EnemySpawnerAttachments, a => a.Side == spawnerData.Side);

                enemySpawner.Platform = platform;
                enemySpawner.FlyTarget = attachData.AttachmentPos;
                m_spawnersList.Add(enemySpawner);
            }
        }

        public void Update(float dt)
        {
            switch (m_state)
            {
                case State.WaitForReady: UpdateWaitForReady(); break;
                case State.Spawning: UpdateSpawning(dt); break;
                case State.WaitForMoveOut: UpdateWaitForMoveOut(); break;
                case State.Done: break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        void UpdateWaitForReady()
        {
            var allReady = CheckEnemySpawnersReady();
            if (m_waitForVisible)
                allReady &= CheckAnyEnemySpawnerVisible();
            if (!allReady)
                return;

            m_state = State.Spawning;
            m_timer = 0f;
            m_currentSpawnIndex = 0;
        }

        void UpdateSpawning(float dt)
        {
            if (!m_stopped)
            {
                if (ContinueSpawning(dt)) 
                    return;
            }

            foreach (var spawner in m_spawnersList)
                spawner.AllowMoveOut();

            if (m_waitForMoveOut)
                m_state = State.WaitForMoveOut;
            else OnDone();
        }

        bool ContinueSpawning(float dt)
        {
            while (m_currentSpawnIndex < m_spawnEntries.Count)
            {
                var currentEntry = m_spawnEntries[m_currentSpawnIndex];
                var wait = currentEntry.Time - m_timer;
                if (wait > 0f)
                    break;

                m_timer = currentEntry.Time;
                if (currentEntry.EnemyConfig != null)
                {
                    m_spawnersList[currentEntry.SpawnerIndex].Spawn(currentEntry.EnemyConfig,
                        currentEntry.SideOffset,
                        $"{m_debugKey}#{m_currentSpawnIndex}");
                }

                ++m_currentSpawnIndex;
            }

            m_timer += dt;
            return m_currentSpawnIndex < m_spawnEntries.Count;
        }

        void UpdateWaitForMoveOut()
        {
            foreach (var spawner in m_spawnersList)
            {
                if (spawner == null)
                    continue;
                if (spawner.State != EnemySpawner.SpawnerState.MovedOut)
                    return;
            }

            OnDone();
        }

        bool CheckAnyEnemySpawnerVisible()
        {
            if (m_spawnersList == null)
                return false;
            foreach (var spawner in m_spawnersList)
            {
                if (spawner.IsVisible())
                    return true;
            }

            return false;
        }

        bool CheckEnemySpawnersReady()
        {
            var allReady = false;
            if (m_spawnersList == null)
                return false;
            foreach (var spawner in m_spawnersList)
            {
                allReady = spawner.State == EnemySpawner.SpawnerState.ReadyToSpawn;
                if (!allReady)
                    break;
            }
            return allReady;
        }

        void OnDone()
        {
            if (m_spawnersList != null)
                SimplePool<List<EnemySpawner>>.I.Return(m_spawnersList);
            m_spawnersList = null;
            m_state = State.Done;
        }

        public void InstantComplete()
        {
            if (m_spawnersList != null)
            {
                foreach (var spawner in m_spawnersList)
                    spawner.gameObject.TryDespawnOrDestroy();
            }

            OnDone();
        }

        public void Stop()
        {
            m_stopped = true;
            foreach (var spawner in m_spawnersList)
                spawner.Stop();
        }
    }

    [Serializable]
    public struct SpawnerConfig
    {
        public ArenaPlatform.PlatformID ID;

        public ArenaPlatform.EnemySpawnerAttachmentSide Side;
    }
}
