﻿using System;
using System.Collections.Generic;
using Core.Types;
using Core.Unity.Interface;
using Game.Arena;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;
using UnityEngine.Serialization;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif

namespace Game.ActionConfig
{
    public class PlatformSelector : ScriptableBaseAction, IPlatformSelector, IActionConfigContainer, IObjectContainer, IVerify //, ISerializationCallbackReceiver
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("Selection")] [SerializeField] ArenaSelectionConfig m_selection;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public ScriptableBaseAction ActionConfig;

        // ReSharper disable once ConvertToAutoPropertyWhenPossible
        public ArenaSelectionConfig Selection
        {
            get => m_selection;
            set => m_selection = value;
        }

        public override string Name => $"SEL -> {(ActionConfig?ActionConfig.Name:"Empty")}";
        static Type StaticFactoryType => typeof(PlatformSelectorAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction(IContext ctx)
        {
            var selectionData = LinkData();
            if (ActionConfig == null)
            {
                Debug.LogError("Action config is missing!");
                return null;
            }
            var action = ActionConfig.CreateAction(ctx);

            return new PlatformSelectorAction(selectionData, action);
        }

        public ArenaSelection LinkData()
        {
            // create data, so we can modify it without changing config
            var selectionData = new ArenaSelection
            {
                SelectionIds = (ArenaPlatform.PlatformID[]) m_selection.SelectionIds.Clone()
            };

            return selectionData;
        }

//#if UNITY_EDITOR
        //public void OnValidate() => SetSelectionActionsRecursively();
        //public void OnBeforeSerialize() => SetSelectionActionsRecursively();
        //public void OnAfterDeserialize() => SetSelectionActionsRecursively();
//#endif

        public IEnumerable<ScriptableBaseAction> GetContainedActions() => new[] { ActionConfig };
        public void Verify(ref VerificationResult result)
        {
            if (ActionConfig == null)
                result.Error("No Action Config set",this);

            if (m_selection == null)
                result.Error("SelectionConfig is null",this);

            var childResult = VerificationResult.Default;
            if (ActionConfig is IVerify childVerify)
                childVerify.Verify(ref childResult);
            if (childResult.Errors > 0)
                result.Error("Nested ActionConfig has errors!", this);
            if (childResult.Warnings > 0)
                result.Warning("Nested ActionConfig has warnings!", this);
        }

        public IEnumerable<ScriptableObject> GetContainedObjects() => new[] { m_selection };
    }

    public class PlatformSelectorAction : IDefaultAction, 
            IUpdatingAction, IStoppableAction, 
            IInstantCompletableAction, IContainingActions
    {
        readonly IArenaSelection m_selection;
        readonly IBaseAction m_action;

        bool m_initialized;

        public PlatformSelectorAction(IArenaSelection selection, IBaseAction action)
        {
            m_selection = selection;
            m_action = action;
            m_initialized = false;
        }

        public void Invoke() => Init();

        public bool IsFinished
        {
            get
            {
                if (m_action is IUpdatingAction up)
                    return up.IsFinished;
                return true;
            }
        }

        public void Init()
        {
            m_initialized = true;
            m_selection.InitConfig(ArenaPlatformMapData.PlatformData);

            SetSelectionActionsRecursively(m_selection);

            switch (m_action)
            {
                case IUpdatingAction up: up.Init(); break;
                case IDefaultAction def: def.Invoke(); break;
            }
        }

        public void Update(float dt)
        {
            if (m_action is IUpdatingAction up)
                up.Update(dt);
        }

        public void InstantComplete()
        {
            if (IsFinished)
                return;
            if (!m_initialized)
                Init();
            if (m_action is IInstantCompletableAction instant)
                instant.InstantComplete();
            //else if (m_action is IDefaultAction def)
            //    def.Invoke();
        }

        public void Stop()
        {
            if (m_action is IStoppableAction stoppable)
                stoppable.Stop();
        }

        void SetSelectionActionsRecursively(IArenaSelection sel)
        {
            using (var actionsObj = SimplePool<List<IArenaSelectionAction>>.I.GetScoped())
            {
                GetSelectionActionsRecursively(m_action, actionsObj.Obj);
                foreach (var selectionAction in actionsObj.Obj)
                    selectionAction.Selection = sel;
            }
        }

        static void GetSelectionActionsRecursively(object action, ICollection<IArenaSelectionAction> actions)
        {
            if (action == null)
                return;
            switch (action)
            {
                case PlatformSelectorAction _: return;
                case IArenaSelectionAction selAction: actions.Add(selAction); break;
                case IContainingActions acCon:
                {
                    foreach (var a in acCon.GetContainedActions())
                        GetSelectionActionsRecursively(a, actions);
                    break;
                }
            }
        }

        public IEnumerable<IBaseAction> GetContainedActions() => new[] { m_action };
    }

    public interface IPlatformSelector
    {
        // ReSharper disable once UnusedMemberInSuper.Global
        ArenaSelectionConfig Selection { get; set; }
    }
}