﻿using System;
using Core.Unity.Interface;
using Game.Configs.Elevator;
using Game.Elevator.Visuals;
using Game.Globals;
using Game.UI.HUD;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;

namespace Game.ActionConfig
{
    public class DialogueActionConfig : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] LeviDialogueSequence m_sequence;
#pragma warning restore 0649 // wrong warnings for SerializeField
        public override string Name => "Dialogue";

        static Type StaticFactoryType => typeof(DialogueAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction(IContext ctx)
        {
            return new DialogueAction(m_sequence);
        }

        public void Verify(ref VerificationResult result)
        {
            if (m_sequence == null)
                result.Error("No Dialogue asset set!", this);
        }
    }

    public class DialogueAction : IUpdatingAction, IInstantCompletableAction
    {
        enum State
        {
            StartUp,
            Dialogue,
            Shutdown
        }

        public DialogueAction(LeviDialogueSequence seq) => m_sequence = seq;
        readonly LeviDialogueSequence m_sequence;

        LeviTextHUD m_dialogue;
        ElevatorVisuals m_visuals;
        State m_state;

        bool DialogueDone => m_dialogue != null && !m_dialogue.IsRunning();

        bool VisualsShutdown => !ElevatorSpawned || (m_visuals != null && !m_visuals.VisualsActive);
        public bool IsFinished => m_state == State.Shutdown && VisualsShutdown;

        static bool ElevatorSpawned => GameGlobals.Elevator != null && GameGlobals.Elevator.gameObject.activeInHierarchy;
        public void Init()
        {
            if (!ElevatorSpawned)
                StartDialogue();
            else
            {
                GameGlobals.Elevator.ShowElevatorVisuals(false);
                m_visuals = GameGlobals.Elevator.Visuals;
                m_state = State.StartUp;
            }
        }

        public void Update(float dt)
        {
            switch (m_state)
            {
                case State.StartUp: StartUpUpdate(); break;
                case State.Dialogue: DialogueUpdate(); break;
                case State.Shutdown: ShutdownUpdate(); break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        static void ShutdownUpdate() {}

        void DialogueUpdate()
        {
            if (!DialogueDone)
                return;

            if (ElevatorSpawned && m_visuals != null
                && m_visuals.VisualsActive
                && !m_visuals.LerpRoutine)
                m_visuals.StartShutdownRoutine();

            m_state = State.Shutdown;
        }

        void StartUpUpdate()
        {
            if (m_visuals != null 
                && m_visuals.gameObject.activeInHierarchy 
                && m_visuals.LerpRoutine)
                return;

            StartDialogue();
        }

        void StartDialogue()
        {
            GameGlobals.GO_UICanvas.TryGetComponent(out m_dialogue);
            if (m_dialogue == null)
                return;

            GameGlobals.GO_UICanvas.SetActive(true);
            m_dialogue.StartDialogue(m_sequence);
            m_state = State.Dialogue;
        }

        public void InstantComplete()
        {
            if (m_dialogue != null)
                m_dialogue.ForceFinishDialogue();
            if (m_visuals!= null) 
                m_visuals.InstantShutDown();
        }
    }
}
