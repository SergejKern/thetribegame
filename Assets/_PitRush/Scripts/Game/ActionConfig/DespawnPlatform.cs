﻿using System;
using Game.Arena;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;

namespace Game.ActionConfig
{
    public class DespawnPlatform : ScriptableBaseAction
#if UNITY_EDITOR
        , IArenaSelectionAction
#endif
    {
        public override string Name => "Despawn";
        static Type StaticFactoryType => typeof(DespawnPlatformAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx) => new DespawnPlatformAction();

#if UNITY_EDITOR
        public IArenaSelection Selection { get; set; }
#endif
    }

    public class DespawnPlatformAction : IDefaultAction, IInstantCompletableAction, IArenaSelectionAction
    {
        public IArenaSelection Selection { get; set; }

        public void Invoke() => Selection.DespawnPlatforms();

        public void InstantComplete() => Invoke();
    }
}