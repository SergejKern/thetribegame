﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Interface;
using Game.Globals;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;


namespace Game.ActionConfig
{
    public class PlayerCountActionContainer : ScriptableBaseAction, IActionConfigContainer, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] internal ScriptableBaseAction[] m_actions = new ScriptableBaseAction[4];
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => "PlayerCount";

        static Type StaticFactoryType => typeof(PlayerCountActionContainerAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            var actions = new IBaseAction[m_actions.Length];
            for (var i = 0; i < m_actions.Length; i++)
                actions[i] = m_actions[i].CreateAction(ctx);

            return new PlayerCountActionContainerAction(actions);
        }

        public IEnumerable<ScriptableBaseAction> GetContainedActions() => m_actions;

#if UNITY_EDITOR
        public ScriptableBaseAction[] Editor_Actions => m_actions;
#endif
        public void Verify(ref VerificationResult result)
        {
            if (m_actions.IsNullOrEmpty())
            {
                result.Error("No actions set", this);
                return;
            }
            if (m_actions.Any(a=>a==null))
                result.Error("Empty Action slot", this);

            var childResult = VerificationResult.Default;

            foreach (var a in m_actions)
            {
                if (a is IVerify childVerify)
                    childVerify.Verify(ref childResult);
            }
            if (childResult.Errors > 0)
                result.Error("Nested actions with errors", this);
            else if (childResult.Warnings > 0)
                result.Warning("Nested actions with warnings", this);
        }
    }

    public class PlayerCountActionContainerAction : IDefaultAction
        , IUpdatingAction, IStoppableAction, IInstantCompletableAction
        , IContainingActions
    {
        readonly IBaseAction[] m_actions;
        public PlayerCountActionContainerAction(IBaseAction[] actions) => m_actions = actions;

        int m_finishedActions;
        int m_initPlayerCount;
        public void Invoke()
        {
            for (var i = 0; i < GameGlobals.PlayerCount; i++)
            {
                var a = m_actions[i];
                if (a is IDefaultAction def)
                    def.Invoke();
            }
        }

        public bool IsFinished => m_finishedActions >= GameGlobals.PlayerCount;
        public void Init()
        {
            m_finishedActions = 0;
            for (var i = 0; i < GameGlobals.PlayerCount; i++)
            {
                var action = m_actions[i];
                if (action is IUpdatingAction up)
                {
                    up.Init();
                    m_initPlayerCount++;
                }
                else if (action is IDefaultAction def)
                    def.Invoke();
            }
        }

        public void Update(float dt)
        {
            m_finishedActions = 0;
            for (var i = 0; i < GameGlobals.PlayerCount; i++)
            {
                var action = m_actions[i];
                if (!(action is IUpdatingAction up) || up.IsFinished)
                {
                    m_finishedActions++;
                    continue;
                }
                if (i >= m_initPlayerCount)
                {
                    up.Init();
                    m_initPlayerCount++;
                }
                up.Update(dt);
            }
        }

        public void Stop()
        {
            for (var i = 0; i < GameGlobals.PlayerCount; i++)
            {
                var action = m_actions[i];
                if (!(action is IUpdatingAction up) || up.IsFinished)
                    continue;
                
                if (action is IStoppableAction stoppable)
                    stoppable.Stop();
            }
        }

        public IEnumerable<IBaseAction> GetContainedActions() => m_actions;

        public void InstantComplete()
        {
            if (IsFinished)
                return;

            for (var i = 0; i < GameGlobals.PlayerCount; i++)
            {
                var action = m_actions[i];
                if (action is IInstantCompletableAction instant)
                    instant.InstantComplete();
                //else if (action is IDefaultAction def)
                //    def.Invoke();
            }

            m_finishedActions = GameGlobals.PlayerCount;
        }
    }
}
