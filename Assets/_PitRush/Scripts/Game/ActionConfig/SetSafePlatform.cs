﻿using System;
using Core.Unity.Interface;
using Game.Arena;
using Game.Globals;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;

namespace Game.ActionConfig
{
    public class SetSafePlatform : ScriptableBaseAction, IVerify
    {
        public ArenaPlatform.PlatformID ID;

        public override string Name => "SAFE Platform";
        static Type StaticFactoryType => typeof(SetSafePlatformAction);
        public override Type FactoryType => StaticFactoryType;
        // ArenaSelectionConfig m_selection;

        public override IBaseAction CreateAction(IContext ctx) => new SetSafePlatformAction(ID);
        public void Verify(ref VerificationResult result)
        {
            if (!ID.IsValid)
                result.Error("ID is invalid!", this);
        }
    }

    public class SetSafePlatformAction : IDefaultAction, IInstantCompletableAction
    {
        public SetSafePlatformAction(ArenaPlatform.PlatformID id) => m_id = id;

        readonly ArenaPlatform.PlatformID m_id;

        public void Invoke()
        {
            var platform = ArenaPlatformMapData.PlatformData.GetPlatform(m_id);
            if (platform == null)
            {
                Debug.LogError("Couldn't set safe platform because it does not exist!");
                return;
            }

            GameGlobals.SafePlatform = platform;
        }

        public void InstantComplete() => Invoke();
    }
}
