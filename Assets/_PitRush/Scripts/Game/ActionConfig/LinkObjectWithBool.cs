﻿using System;
using Core.Extensions;
using Core.Types.VariableWrapper;
using Core.Unity.Interface;
using Core.Unity.Types.VariableWrapper;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;

namespace Game.ActionConfig
{
    public class LinkObjectWithBool : ScriptableBaseAction, IVerify
    {
        [Serializable]
        public struct LinkData
        {
            public GameObjectReference TriggerGo;
            public BoolReference TriggerBool;
        }
        public LinkData[] Links;

        public override string Name => "Link Trigger with Bool";

        static Type StaticFactoryType => typeof(LinkObjectWithBoolAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            var links = new LinkObjectWithBoolAction.LinkData[Links.Length];
            for (var i = 0; i < links.Length; i++)
            {
                Links[i].TriggerGo.Init(ctx);
                Links[i].TriggerBool.Init(ctx);

                links[i].TriggerGo = Links[i].TriggerGo;
                links[i].TriggerBool = Links[i].TriggerBool;
            }

            //TriggerGo.Init(ctx);
            //TriggerBool.Init(ctx);
            return new LinkObjectWithBoolAction(links);
        }

        public void Verify(ref VerificationResult result)
        {
            if (Links.IsNullOrEmpty())
            {
                result.Error("Links not set", this);
                return;
            }          
            for (var index = 0; index < Links.Length; index++)
            {
                var l = Links[index];
                if (l.TriggerGo.IsNone())
                    result.Error($"Trigger Go Variable no set at index {index}", this);
                if (l.TriggerBool.IsNone())
                    result.Error($"Trigger Bool Variable no set at index {index}", this);
            }

            //if (TriggerGo.IsNone())
            //    result.Error($"Trigger Go Variable no set",this);
            //if (TriggerBool.IsNone())
            //    result.Error($"Trigger Bool Variable no set",this);
        }
    }

    public class LinkObjectWithBoolAction : IDefaultAction
    {
        public LinkObjectWithBoolAction(LinkData[] linkData) 
            => m_linkData = linkData;

        public struct LinkData
        {
            public GameObjectVar TriggerGo;
            public BoolVar TriggerBool;
        }
        readonly LinkData[] m_linkData;

        public void Invoke()
        {
            for (var i = 0; i < m_linkData.Length; i++)
            {
                if (m_linkData[i].TriggerGo.Value == null)
                    continue;
                var box = m_linkData[i].TriggerGo.Value.GetComponent<ILinkWithBool>();
                box?.Link(m_linkData[i].TriggerBool);
            }
        }
    }

    public interface ILinkWithBool
    {
        void Link(BoolVar var);
    }
}
