﻿using System;
using Game.Arena;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;

namespace Game.ActionConfig
{
    public class SetPlatformFencesActive : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Vector3Int[] m_customFences;
#pragma warning restore 0649 // wrong warnings for SerializeField
        public bool Active;

        public override string Name => $"FENCE {(Active ? "Active" : "Inactive")}";

        static Type StaticFactoryType => typeof(SetPlatformFencesActiveAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            return new SetPlatformFencesActiveAction(Active, m_customFences);
        }
    }

    public class SetPlatformFencesActiveAction : IDefaultAction, IInstantCompletableAction,IArenaSelectionAction
    {
        public SetPlatformFencesActiveAction(bool active, Vector3Int[] custom)
        {
            m_active = active;
            m_custom = custom;
        }

        public IArenaSelection Selection { get; set; }

        readonly Vector3Int[] m_custom;
        readonly bool m_active;

        public void Invoke()
        {
            foreach (var platform in Selection.Platforms)
            {
                if (platform == null)
                    continue;

                platform.SetRailingsActive(m_active, m_custom);
            }
        }

        public void InstantComplete() => Invoke();
    }
}
