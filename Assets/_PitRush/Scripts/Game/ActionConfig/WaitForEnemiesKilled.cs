﻿using System;
using Game.Actors;
using Game.Arena.Level;
using Game.Utility;
using GameUtility.Energy;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using UnityEngine;


namespace Game.ActionConfig
{
    public class WaitForEnemiesKilled : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] bool m_forceNextTrigger;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => "ALL_KILL WAIT";
        static Type StaticFactoryType => typeof(WaitForEnemiesKilledAction);
        public override Type FactoryType => StaticFactoryType;


        public override IBaseAction CreateAction(IContext ctx) => new WaitForEnemiesKilledAction(ctx.GameObject, m_forceNextTrigger);
    }

    public class WaitForEnemiesKilledAction : IUpdatingAction
    {
        readonly ArenaLevelFlowBehaviour m_flowBehaviour;
        readonly bool m_forceNextTrigger;
        int m_enemiesAlive;

        public WaitForEnemiesKilledAction(GameObject ctx, bool nextTrigger)
        {
            m_forceNextTrigger = nextTrigger;
            m_flowBehaviour = ctx.GetComponent<ArenaLevelFlowBehaviour>();
        }

        public bool IsFinished => m_enemiesAlive == 0;

        public void Init() => m_enemiesAlive = TargetsCollector.Targets.Count;

        public void Update(float dt)
        {
            m_enemiesAlive = 0;
            foreach (var t in TargetsCollector.Targets)
            {
                var e = t.Transform.GetComponent<Enemy>();
                if (e == null || !e.gameObject.activeInHierarchy)
                    continue;

                // better to use provider method, to get component also when prefab arrangement changes
                e.Get(out IHealthComponent health);
                m_enemiesAlive += (int) Mathf.Clamp01(health.HP * 10f);
            }
            if (IsFinished && m_forceNextTrigger)
                m_flowBehaviour.NextTrigger();
        }
    }
}
