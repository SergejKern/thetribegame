﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Types;
using Core.Unity.Types;
using Game.Globals;
using GameUtility.Operations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

namespace Game.UI
{
    public class MultiplePlayerInputInputModuleFix : MonoBehaviour
    {
        struct InputActionLinks
        {
            public PlayerInput Input;

            public Action<InputAction.CallbackContext> OnNavigate;
            public Action<InputAction.CallbackContext> OnConfirm;
            public Action<InputAction.CallbackContext> OnBackHolding;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] EventSystem m_eventSystem;

        [SerializeField] Selectable m_selectable;

        [SerializeField] InputActionReference m_backActionRef;
        [SerializeField] InputActionReference m_navigateActionRef;
        [SerializeField] InputActionReference m_confirmActionRef;

        [SerializeField] UnityEvent m_onBackHoldingDown;
        [SerializeField] UnityEvent m_onBackHoldingRelease;

        [SerializeField] GameObjectEvent m_onSelectionChanged;

        [SerializeField] UnityEvent m_onConfirm;

        [SerializeField] string m_keyboardSchemeName;
        [SerializeField] InputSystemUIInputModule m_inputModule;
        //[SerializeField] string m_menuActionMapNameOrId;

#pragma warning restore 0649 // wrong warnings for SerializeField

        //BaseEventData m_eventData;
        readonly List<InputActionLinks> m_inputLinks = new List<InputActionLinks>();

        Action<InputAction.CallbackContext> m_navigationHook;

        void OnEnable()
        {
            var inputs = GetAllPlayerInputs();
            foreach (var input in inputs)
            {
                // input.gameObject.SetActive(true);
                // input.SwitchCurrentActionMap(m_menuActionMapNameOrId);

                if (string.Equals(input.currentControlScheme, m_keyboardSchemeName))
                    m_inputModule.actionsAsset = input.actions;

                var inputLinks = new InputActionLinks
                {
                    Input = input,
                    OnConfirm = context => OnConfirmButtonPressed(),
                    OnBackHolding = OnBackHolding,
                    OnNavigate = OnNavigate
                };
                m_inputLinks.Add(inputLinks);
                InitActions(input, inputLinks);
            }

            Select(m_selectable);
        }

        void Update() => KeyboardMouseSelectionFix();

        void KeyboardMouseSelectionFix()
        {
            if (m_eventSystem.currentSelectedGameObject == m_selectable.gameObject)
                return;
            if (m_eventSystem.currentSelectedGameObject == null)
            {
                Select(m_selectable);
                return;
            }

            m_eventSystem.currentSelectedGameObject.TryGetComponent(out Selectable newSelected);
            if (newSelected == null)
                return;

            Select(newSelected);
        }

        void Select(Selectable select)
        {
            if (select == null)
                return;

            m_selectable = select;

            m_selectable.Select();
            m_selectable.OnSelect(null);
            m_onSelectionChanged.Invoke(m_selectable.gameObject);
        }

        void OnDisable() => RemoveInput();

        public void RemoveInput()
        {
            // Debug.Log($"{gameObject.name} RemoveInput  m_inputLinks.Count { m_inputLinks.Count}");

            for (var i = 0; i < m_inputLinks.Count; i++)
            {
                var input = m_inputLinks[i].Input;

                if (input == null)
                    continue;
                input.actions[m_backActionRef.name].Remove(m_inputLinks[i].OnBackHolding);
                input.actions[m_confirmActionRef.name].Remove(m_inputLinks[i].OnConfirm);
                input.actions[m_navigateActionRef.name].Remove(m_inputLinks[i].OnNavigate);
            }

            m_inputLinks.Clear();
        }

        void OnNavigate(InputAction.CallbackContext cc)
        {
            var vec = cc.ReadValue<Vector2>();
            
            if (m_navigationHook != null)
            {
                m_navigationHook.Invoke(cc);
                return;
            }

            var newSelect = m_selectable.FindSelectable(new Vector3(vec.x, vec.y, 0));
            Select(newSelect);
        }

        void OnBackHolding(InputAction.CallbackContext cc)
        {
            if (cc.ReadValue<float>() >= 1f)
                m_onBackHoldingDown.Invoke();
            else m_onBackHoldingRelease.Invoke();
        }

        void OnConfirmButtonPressed()
        {
            if (m_selectable is ISubmitHandler submitHandler) 
                submitHandler.OnSubmit(null);
            if (m_selectable.TryGetComponent(out EventTrigger trigger))
                trigger.OnSubmit(null);

            // if we don't want to submit via button, but only select and then confirm on different button:
            m_onConfirm.Invoke();
        }

        
        void InitActions(PlayerInput input, InputActionLinks inputLinks)
        {
            input.actions[m_backActionRef.name].Add(inputLinks.OnBackHolding);
            input.actions[m_navigateActionRef.name].Add(inputLinks.OnNavigate);
            input.actions[m_confirmActionRef.name].Add(inputLinks.OnConfirm);
        }

        IEnumerable<PlayerInput> GetAllPlayerInputs()
        {
            using (var inputsList = SimplePool<List<PlayerInput>>.I.GetScoped())
            {
                var inputs = inputsList.Obj;
                inputs.AddRange(from p in GameGlobals.Players where p != null where p.Input != null select p.Input);

                var foundInputs = FindObjectsOfType<PlayerInput>();
                foreach (var found in foundInputs)
                {
                    if (inputs.Contains(found))
                        continue;
                    inputs.Add(found);
                }

                return inputs.ToArray();
            }
        }

        public void SetNavigationHook(Action<InputAction.CallbackContext> navHook) 
            => m_navigationHook = navHook;
    }
}
