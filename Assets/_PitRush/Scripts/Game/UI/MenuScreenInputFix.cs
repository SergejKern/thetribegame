﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Game.UI
{
    public class MenuScreenInputFix : MonoBehaviour
    {
        enum InputMethod
        {
            // ReSharper disable once UnusedMember.Local
            Invalid,
            KeyboardMouse,
            GamePad
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PlayerInput m_input;
        [SerializeField] EventSystem m_eventSystem;

        [SerializeField] Selectable m_selectable;
        [SerializeField] string m_keyboardSchemeName;
#pragma warning restore 0649 // wrong warnings for SerializeField

        InputMethod m_inputMethod;

        // Update is called once per frame
        void Update()
        {
            var newInputMethod = string.Equals(m_input.currentControlScheme, m_keyboardSchemeName)
                ? InputMethod.KeyboardMouse
                : InputMethod.GamePad;

            var inputChanged = newInputMethod != m_inputMethod;
            var fixSelection = m_eventSystem.currentSelectedGameObject != null && m_inputMethod == InputMethod.KeyboardMouse;
            if (!inputChanged && !fixSelection)
                return;
            m_inputMethod = newInputMethod;
            m_eventSystem.SetSelectedGameObject(m_inputMethod == InputMethod.KeyboardMouse
                ? null
                : m_selectable.gameObject);
        }
    }
}
