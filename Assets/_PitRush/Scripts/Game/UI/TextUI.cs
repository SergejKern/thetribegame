﻿using UnityEngine;

namespace Game.UI
{
    public class TextUI : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] TMPro.TMP_Text m_text;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void SetText(string text) => m_text.text = text;
    }
}
