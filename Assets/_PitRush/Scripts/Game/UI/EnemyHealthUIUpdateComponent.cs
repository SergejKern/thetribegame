﻿using Game.Actors.Energy;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class EnemyHealthUIUpdateComponent : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] HealthComponent m_health;
        [SerializeField] Canvas m_canvas;
        [SerializeField] CanvasGroup m_canvasGroup;

        [SerializeField] Image m_healthFilling;
        [SerializeField] float m_showSeconds = 2f;
        [SerializeField] float m_minVisibility =0.1f;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_showTimer;
        void OnEnable()
        {
            m_health.AddChangeListener(HealthChanged);
            HealthChanged(m_health.HP);
        }

        void OnDisable() => m_health.RemoveChangeListener(HealthChanged);

        void Update()
        {
            if (m_canvasGroup == null || m_canvasGroup.alpha <= m_minVisibility)
                return;

            if (m_showTimer > 0)
            {
                m_showTimer -= Time.deltaTime;
                return;
            }

            m_canvasGroup.alpha -= Time.deltaTime;
        }

        void HealthChanged(float hp)
        {
            var visible = (hp < m_health.MaxHP && hp > 0);
            m_canvas.gameObject.SetActive(true);

            //if (!visible)
            //    return;

            m_healthFilling.fillAmount = hp / m_health.MaxHP;

            if (m_canvasGroup != null)
                m_canvasGroup.alpha = visible?1f: m_minVisibility;

            m_showTimer = m_showSeconds;
        }
    }
}
