﻿using Game.Arena.Level;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.ResultScreen
{
    public class PlayerScore : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Image m_playerImage;
        [SerializeField] TMPro.TMP_Text m_playerName;

        [SerializeField] StatElement m_enemiesDefeated;
        [SerializeField] StatElement m_damageTaken;
        [SerializeField] StatElement m_deaths;
        [SerializeField] StatElement m_revives;

        [SerializeField] Image m_badgeBG;
        [SerializeField] TMPro.TMP_Text m_badgeTitle;
        [SerializeField] TMPro.TMP_Text m_badgeDesc;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void SetData(LevelPlayerStats levelPlayerStat)
        {
            m_damageTaken.ResetWith((int) levelPlayerStat.DamageTaken);
            m_enemiesDefeated.ResetWith(levelPlayerStat.EnemiesDefeated);
            m_deaths.ResetWith(levelPlayerStat.Deaths);
            m_revives.ResetWith(levelPlayerStat.RevivedFriends);
        }
    }
}
