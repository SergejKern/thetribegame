﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.UI.ResultScreen
{
    public class StatElement : MonoBehaviour
    {
        enum Format
        {
            None,
            Counter,
            Time,
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        //[SerializeField] TMP_Text m_statTitle;
        [SerializeField] TMP_Text[] m_statCount;
        [SerializeField] CanvasGroup m_glow;
        [SerializeField] Image m_progressFilling;

        [FormerlySerializedAs("m_updateSpeedNumberPerSecond")]
        [SerializeField] float m_updateSpeedSecondPerNumber;

        [SerializeField] float m_updateMinSeconds = 3;
        [SerializeField] float m_updateMaxSeconds;

        [SerializeField] float m_glowFadeFactor;

        [SerializeField] UnityEvent m_onTargetChanged;       
        [SerializeField] UnityEvent m_onTargetReached;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public float Target { get; private set; }

        Format m_format;

        float m_from;
        float m_current;
        float m_max;

        float m_updateTime;
        float m_updateSeconds;

        const float k_delay = 2f;
        string m_lastText;


        public void SetMaxValue(int maxValue) 
            => m_max = maxValue;

        public void SetTarget(int statTarget)
        {
            m_updateTime = -k_delay;
            m_from = m_current;
            Target = statTarget;

            var diff = Target - m_current;
            m_updateSeconds = Mathf.Clamp(m_updateSpeedSecondPerNumber * diff, m_updateMinSeconds, m_updateMaxSeconds);
            m_format = Format.Counter;

            m_onTargetChanged.Invoke();
        }

        public void ResetWithNone()
        {
            foreach (var t in m_statCount)
                t.text = "-";
            m_format = Format.None;
        }

        public void ResetWith(int statTarget, int current = 0)
        {
            m_current = current;
            foreach (var t in m_statCount)
                t.text = $"{current}";

            SetTarget(statTarget);
        }

        internal void ResetWith(float targetTime, float current = 0)
        {
            m_updateTime = -k_delay;

            m_current = current;

            var time = TimeSpan.FromSeconds(m_current);
            var str = time.ToString(@"mm\:ss");

            foreach (var t in m_statCount) 
                t.text = $"{str}";
            Target = targetTime;

            m_updateSeconds = Mathf.Clamp(m_updateSpeedSecondPerNumber * Target, m_updateMinSeconds, m_updateMaxSeconds);
            m_format = Format.Time;

            m_onTargetChanged.Invoke();
        }

        void Update()
        {
            if (m_format == Format.None)
                return;

            m_glow.alpha = Mathf.Clamp01(m_glow.alpha - Time.deltaTime * m_glowFadeFactor);
            if (Math.Abs(m_current - Target) < float.Epsilon)
                return;
            m_updateTime += Time.deltaTime;
            if (m_updateTime < 0)
                return;
            var lerpVal = Mathf.Clamp01(m_updateTime / m_updateSeconds);
            var next = m_from + (Target-m_from) * lerpVal;
            
            if (m_format == Format.Counter)
                next = Mathf.FloorToInt(next);
            if (Math.Abs(m_current - next) < float.Epsilon)
                return;

            m_current = next;
            if (Math.Abs(m_current - Target) < float.Epsilon)
                m_onTargetReached.Invoke();
            
            m_glow.alpha = 1f;

            switch (m_format)
            {
                case Format.Counter:
                {
                    foreach (var t in m_statCount)
                        t.text = m_current.ToString("F0");

                    if (m_progressFilling != null && m_max > 0)
                        m_progressFilling.fillAmount = m_current / m_max;
                    break;
                }
                case Format.Time:
                {
                    var time = TimeSpan.FromSeconds(m_current);
                    var str = time.ToString(@"mm\:ss");

                    if (string.Equals(m_lastText, str))
                        return;
                    m_lastText = str;
                    foreach (var t in m_statCount)
                        t.text = m_lastText;
                    break;
                }
            }
        }
    }
}
