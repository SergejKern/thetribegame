﻿using System;
using System.Collections;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Game.Arena.Level;
using Game.Globals;
using Game.SaveLoad;
using GameUtility.Feature.Loading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.UI.ResultScreen
{
    public class ResultScreen : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] TMPro.TMP_Text m_levelName;

        [SerializeField] LayoutGroup m_playerSlotsLayout;
        //[SerializeField] GameObject m_playerSlotPrefab;
        [SerializeField] GameObject[] m_stars;

        //todo 3 stars
        [SerializeField] StatElement m_coins;
        [SerializeField] StatElement m_time;
        [SerializeField] StatElement m_bestTime;
        [SerializeField] StatElement m_coinProgress;

        [SerializeField] TMPro.TMP_Text m_goldTime;
        [SerializeField] TMPro.TMP_Text m_silverTime;
        [SerializeField] TMPro.TMP_Text m_bronzeTime;
        [SerializeField] TMPro.TMP_Text m_goldReward;
        [SerializeField] TMPro.TMP_Text m_silverReward;
        [SerializeField] TMPro.TMP_Text m_bronzeReward;

        [SerializeField] RectTransform m_markersParent;
        [SerializeField] RectTransform m_markerStarA;
        [SerializeField] RectTransform m_markerStarB;

        [SerializeField] SceneID m_levelSelectID;
        [SerializeField] LoadOperationID m_loadID;
#pragma warning restore 0649 // wrong warnings for SerializeField
        Coroutine m_loadLevelSelect;

        public void SetData(LevelStats levelStats)
        {
            m_levelName.text = levelStats.Level.name;

            var coins = levelStats.CoinsCollected;
            m_coins.ResetWith(coins,coins);
            m_time.ResetWith(levelStats.Time,levelStats.Time);

            SaveSystem.GetScores(levelStats.Level, out _, out var bestTime);
            m_bestTime.ResetWith(bestTime);

            var resultData = levelStats.Level.Result;

            m_goldTime.SetText(TimeSpan.FromSeconds(resultData.Gold.TimeSeconds).ToString(@"mm\:ss"));
            m_silverTime.SetText(TimeSpan.FromSeconds(resultData.Silver.TimeSeconds).ToString(@"mm\:ss"));
            m_bronzeTime.SetText(TimeSpan.FromSeconds(resultData.Bronze.TimeSeconds).ToString(@"mm\:ss"));

            m_goldReward.SetText(resultData.Gold.CoinsReward.ToString());
            m_silverReward.SetText(resultData.Silver.CoinsReward.ToString());
            m_bronzeReward.SetText(resultData.Bronze.CoinsReward.ToString());

            m_coinProgress.SetMaxValue(resultData.GoldTargetCoins);
            m_coinProgress.ResetWith(levelStats.Score);

            var width = m_markersParent.sizeDelta.x;

            var starA_anchorMin = m_markerStarA.anchoredPosition;
            m_markerStarA.anchoredPosition = new Vector2((float) resultData.BronzeTargetCoins / resultData.GoldTargetCoins * width, starA_anchorMin.y);
            var starB_anchorMin = m_markerStarB.anchoredPosition;
            m_markerStarB.anchoredPosition = new Vector2((float) resultData.SilverTargetCoins / resultData.GoldTargetCoins * width, starB_anchorMin.y);

            var slotsTr = m_playerSlotsLayout.transform;
            for (var i = 0; i < slotsTr.childCount; i++)
            {
                var active = levelStats.PlayerStats.Length > i;
                var slotChild = slotsTr.GetChild(i);
                slotChild.gameObject.SetActive(active);

                if (!active)
                    continue;
                if(!slotChild.TryGetComponent(out PlayerScore ps))
                    continue;

                ps.SetData(levelStats.PlayerStats[i]);
            }

            StartCoroutine(ShowStars(levelStats.Stars));
        }

        IEnumerator ShowStars(int stars)
        {
            for (var i = 0; i < stars; i++)
            {
                yield return new WaitForSeconds(0.35f);
                m_stars[i].SetActiveWithTransition();
            }
        }

        public void OnContinue()
        {
            if (LoadOperations.IsLoading)
                return;
            if (m_loadLevelSelect != null)
                return;
            m_loadLevelSelect = StartCoroutine(LoadLevelSelect());
        }

        IEnumerator LoadLevelSelect()
        {
            LoadOperations.StartLoading(m_loadID);

            var scenes = GameGlobals.SceneLinks;
            var lvlSelectSceneRef = scenes.GetScene(m_levelSelectID);

            Pool.Instance.Clear();
            foreach (var player in GameGlobals.Players)
            {
                player.gameObject.transform.SetParent(null);
                player.OnRevive();
            }
            yield return null;

            foreach (var player in GameGlobals.Players)
            {
                if (player == null)
                {
                    Debug.LogError("Player is null!");
                    continue;
                }

                var playerGo = player.gameObject;
                playerGo.SetActive(false);

                DontDestroyOnLoad(playerGo);
                DontDestroyOnLoad(player.Input.gameObject);
            }

            transform.SetParent(null);
            DontDestroyOnLoad(gameObject);
            yield return null;

            var asyncLoad = SceneManager.LoadSceneAsync(lvlSelectSceneRef);
            while (!asyncLoad.isDone)
                yield return null;

            LoadOperations.LoadingDone(m_loadID);
            //var scene = SceneManager.GetSceneByName(lvlSelectSceneRef);
            //SceneRoot.GetSceneRoot(scene, out var levelSelectionScene);
            //GameGlobals.SetScene(m_levelSelectID, levelSelectionScene);
            //levelSelectionScene.InitActive(true);
            m_loadLevelSelect = null;
            gameObject.DestroyEx();
        }
    }
}
