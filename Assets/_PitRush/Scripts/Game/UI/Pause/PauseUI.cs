﻿using System;
using Core.Unity.Extensions;
using Game.Globals;
using Game.SaveLoad;
using GameUtility.Feature.Loading;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Game.UI.Pause
{
    public class PauseUI : MonoBehaviour
    {
        [Serializable]
        public struct AudioSlider
        {
            // public AK.Wwise.RTPC Rtpc;
            public Slider Slider;
            public Selectable Selectable;
        }

        [Serializable]
        public struct StateData
        {
            public GameObject StateGroup;
            public Selectable FirstSelected;
        }

        enum State
        {
            Pause,
            Controls,
            Settings
        }

        State m_state;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] StateData m_pauseState;
        [SerializeField] StateData m_controlsState;
        [SerializeField] StateData m_settingsState;

        [SerializeField] AudioSlider m_master;
        [SerializeField] AudioSlider m_music;
        [SerializeField] AudioSlider m_sfx;

        [SerializeField] AK.Wwise.RTPC m_musicRtpc;
        [SerializeField] AK.Wwise.RTPC m_sfxRtpc;
        [SerializeField] AK.Wwise.RTPC m_uiRtpc;

        [SerializeField] MultiplePlayerInputInputModuleFix m_multiInputFix;

        [SerializeField] SceneID m_pauseSceneID;
        [SerializeField] SceneID m_titleID;
        [SerializeField] SceneID m_restartSceneID;

        [SerializeField] LoadOperationID m_reloadID;
        [SerializeField] LoadOperationID m_exitID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        AudioSlider m_currentSlider;
        int m_lastSelectedFrame;

        void OnEnable()
        {
            m_state = State.Pause;

            InitAudioSliders();
        }


        void OnDisable()
        {
            Time.timeScale = 1f;
        }

        void InitAudioSliders()
        {
            AudioVolumePrefs.Load(out var volumes);
            m_master.Slider.value = volumes.Master;
            m_music.Slider.value = volumes.Music;
            m_sfx.Slider.value = volumes.Sfx;
        }

        public void OnBack()
        {
            if (LoadOperations.IsLoading)
                return;
            if (m_currentSlider.Slider != null)
            {
                DeselectAudioSlider();
                return;
            }
            switch (m_state)
            {
                case State.Pause: OnContinue(); break;
                case State.Controls: OnBackToPause(); break;
                case State.Settings: OnBackToPause(); break;
            }
        }

        public void OnConfirm()
        {
            //if (LoadOperations.IsLoading)
            //    return;
            //if (m_currentSlider.Slider!= null) 
            //    DeselectAudioSlider();
        }

        public void OnAudioSliderConfirm()
        {
            if (LoadOperations.IsLoading)
                return;
            if (m_currentSlider.Slider != null)
                DeselectAudioSlider();
        }

        void DeselectAudioSlider()
        {
            var prev = m_currentSlider.Selectable;
            m_multiInputFix.SetNavigationHook(null);
            m_currentSlider = default;
            prev.Select();
            prev.OnSelect(null);
        }

        void OnBackToPause()
        {
            m_controlsState.StateGroup.SetActiveWithTransition(false);
            m_settingsState.StateGroup.SetActiveWithTransition(false);
            m_pauseState.StateGroup.SetActiveWithTransition();
            m_pauseState.FirstSelected.Select();
            m_pauseState.FirstSelected.OnSelect(null);

            m_state = State.Pause;
        }

        public void OnContinue()
        {
            GameGlobals.Scenes[m_pauseSceneID].SetActive(false);
            foreach (var player in GameGlobals.Players) 
                player.Input.SwitchCurrentActionMap("Player");
        }

        [UsedImplicitly]
        public void OnControls()
        {
            if (LoadOperations.IsLoading)
                return;
            m_pauseState.StateGroup.SetActiveWithTransition(false);
            m_controlsState.StateGroup.SetActiveWithTransition();
            m_controlsState.FirstSelected.Select();
            m_controlsState.FirstSelected.OnSelect(null);

            m_state = State.Controls;
        }

        [UsedImplicitly]
        public void OnSettings()
        {
            if (LoadOperations.IsLoading)
                return;
            m_pauseState.StateGroup.SetActiveWithTransition(false);
            m_settingsState.StateGroup.SetActiveWithTransition();
            m_settingsState.FirstSelected.Select();
            m_settingsState.FirstSelected.OnSelect(null);
            m_state = State.Settings;
        }

        [UsedImplicitly]
        public void OnRestart()
        {
            if (LoadOperations.IsLoading)
                return;

            StartCoroutine(GlobalOperations.Reloading(gameObject, m_restartSceneID, m_reloadID));
        }

        [UsedImplicitly]
        public void OnExit()
        {
            if (LoadOperations.IsLoading)
                return;

            StartCoroutine(GlobalOperations.Exit(m_titleID, m_exitID, m_multiInputFix));
        }

        void AdjustVolume(InputAction.CallbackContext cc)
        {
            if (LoadOperations.IsLoading)
                return;
            var slider = m_currentSlider.Slider;
            if (slider == null)
                return;
            
            var input = cc.ReadValue<Vector2>();
            if (input.x < -float.Epsilon)
                slider.value -= 0.1f;
            else if (input.x > float.Epsilon) 
                slider.value += 0.1f;

            ApplyVolumes();
        }

        void ApplyVolumes()
        {
            var volumes = new AudioVolumePrefs.AudioVolumes()
            {
                Master = m_master.Slider.value,
                Music = m_music.Slider.value,
                Sfx = m_sfx.Slider.value
            };
            AudioVolumePrefs.Save(volumes);
            AudioVolumePrefs.ApplyVolumes(volumes, m_musicRtpc, m_sfxRtpc, m_uiRtpc);
        }

        [UsedImplicitly]
        public void OnMasterVolumeSetting()
        {
            if (LoadOperations.IsLoading)
                return;
            m_currentSlider = m_master;
            m_currentSlider.Slider.Select();
            m_currentSlider.Slider.OnSelect(null);

            m_multiInputFix.SetNavigationHook(AdjustVolume);
        }

        [UsedImplicitly]
        public void OnMusicSetting()
        {
            if (LoadOperations.IsLoading)
                return;
            m_currentSlider = m_music;
            m_currentSlider.Slider.Select();
            m_currentSlider.Slider.OnSelect(null);

            m_multiInputFix.SetNavigationHook(AdjustVolume);
        }

        [UsedImplicitly]
        public void OnSoundSetting()
        {
            if (LoadOperations.IsLoading)
                return;
            m_currentSlider = m_sfx;
            m_currentSlider.Slider.Select();
            m_currentSlider.Slider.OnSelect(null);

            m_multiInputFix.SetNavigationHook(AdjustVolume);
        }
    }
}
