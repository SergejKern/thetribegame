﻿using System.Collections;
using Core.Unity.Extensions;
using Game.Globals;
using GameUtility.Feature.Loading;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace Game.UI.GameOver
{
    public class GameOverUI : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] MultiplePlayerInputInputModuleFix m_multiInputFix;
        [SerializeField] SceneID m_title;
        [SerializeField] SceneID m_restartScene;

        [SerializeField] LoadOperationID m_reloadID;
        [SerializeField] LoadOperationID m_exitID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void OnEnable()
        {
            //if (LoadingCanvas.Instance!= null)
            //    LoadingCanvas.Instance.SetActive(false);
            GameGlobals.GO_UICanvas.SetActive(false);

            foreach (var player in GameGlobals.Players)
                player.Input.SwitchCurrentActionMap("Menu");
            Time.timeScale = 0f;
        }

        //linked in unity
        [UsedImplicitly, Preserve]
        public void RestartLevel()
        {
            if (LoadOperations.IsLoading)
                return;
            StartCoroutine(Reload());
        }

        //linked in unity
        [UsedImplicitly, Preserve]
        public void Exit()
        {
            if (LoadOperations.IsLoading)
                return;
            StartCoroutine(GlobalOperations.Exit(m_title, m_exitID, m_multiInputFix));
        }

        IEnumerator Reload()
        {
            Time.timeScale = 1f;

            yield return StartCoroutine(GlobalOperations.Reloading(gameObject, m_restartScene, m_reloadID));
            gameObject.DestroyEx();
        }


        //public IEnumerator Reloading()
        //{
        //    var scenes = GameGlobals.SceneLinks;
        //    var elevatorSceneName = scenes.ElevatorScene.SceneName;
        //    var arenaScene = SceneManager.GetSceneByName(scenes.ArenaScene.SceneName);
        //    if (arenaScene.isLoaded)
        //    {
        //        var asyncUnload = SceneManager.UnloadSceneAsync(arenaScene);
        //        while (!asyncUnload.isDone)
        //            yield return null;
        //    }
        //    foreach (var player in GameGlobals.Players)
        //        player.transform.SetParent(null);
        //    Pool.Instance.DeactivateAllObjects();
        //    var elevatorScene = SceneManager.GetSceneByName(elevatorSceneName);
        //    if (elevatorScene.isLoaded)
        //    {
        //        var asyncUnload = SceneManager.UnloadSceneAsync(elevatorScene);
        //        while (!asyncUnload.isDone)
        //            yield return null;
        //    }
        //    var asyncLoad = SceneManager.LoadSceneAsync(elevatorSceneName, LoadSceneMode.Additive);
        //    while (!asyncLoad.isDone)
        //        yield return null;
        //    var scene = SceneManager.GetSceneByName(elevatorSceneName);
        //    SceneRoot.GetSceneRoot(scene, out GameGlobals.ElevatorScene);
        //    GameGlobals.ElevatorScene.InitActive(true);
        //}
    }
}
