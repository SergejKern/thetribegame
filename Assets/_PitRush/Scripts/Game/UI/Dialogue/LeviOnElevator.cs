﻿using System;
using Core.Unity.Types;
using Game.Arena.Level;
using Game.Configs;
using Game.Configs.Elevator;
using Game.Elevator;
using Game.Globals;
using UnityEngine;
using Game.UI.HUD;

namespace Game.UI.Dialogue
{
    public class LeviOnElevator : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ExpressionToAnimation[] m_expressionAnimations 
            = new ExpressionToAnimation[LeviDialogueSequence.FaceExpressionCount];

        [SerializeField] Animator m_animator;

        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_endingAnim;
#pragma warning restore 0649 // wrong warnings for SerializeField
        static LeviDialogueSequence Sequence
        {
            get
            {
                if (ArenaLevelFlowConfig.Current == null ||
                    ConfigLinks.Instance == null)
                    return null;
                var lvlConf= ConfigLinks.Instance.GetLevelsConfig();
                var lvlData = lvlConf.Get(ArenaLevelFlowConfig.Current);
                return lvlData.ElevatorDialogue;
            }
        }

        static bool m_hack;
        //void Awake() => StartCoroutine(Dialogue());
        void OnValidate()
        {
            if (m_expressionAnimations.Length != LeviDialogueSequence.FaceExpressionCount)
                Array.Resize(ref m_expressionAnimations, LeviDialogueSequence.FaceExpressionCount);
            for (var i = 0; i < m_expressionAnimations.Length; i++)
                m_expressionAnimations[i].Expression = (LeviDialogueSequence.LeviFaceExpressions) i;
        }

        bool m_dialogueIsRunning;

        public void StartDialogue()
        {
            GameGlobals.GO_UICanvas.TryGetComponent(out LeviTextHUD hud);
            if (hud == null || Sequence == null)
                return;
            if(m_hack)
                return;
            GameGlobals.GO_UICanvas.SetActive(true);
            hud.StartDialogue(Sequence);
            m_hack = true;
        }

        public void Play(LeviDialogueSequence.LeviFaceExpressions expression)
        {
            var anim = m_expressionAnimations[(int) expression];
            m_animator.Play(anim.Animation);
        }

        public void Finish() 
            => m_animator.Play(m_endingAnim);
    }
}
