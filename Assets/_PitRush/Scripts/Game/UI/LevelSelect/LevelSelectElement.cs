﻿using Game.Arena.Level;
using Game.SaveLoad;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.LevelSelect
{
    public class LevelSelectElement : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Selectable m_selectable;

        [SerializeField] ArenaLevelFlowConfig m_level;

        [SerializeField] RectTransform m_leviSocket;
        [SerializeField] GameObject[] m_activeStars;
#pragma warning restore 0649 // wrong warnings for SerializeField
        public ArenaLevelFlowConfig Level => m_level;
        public RectTransform LeviSocket => m_leviSocket;

        public void Init()
        {
            SaveSystem.GetScores(m_level, out var score, out _);
            var stars = m_level.Result.GetStars(score);
            for (var i = 0; i < m_activeStars.Length; i++)
                m_activeStars[i].SetActive(stars > i);
        }

        public void SelectLevel()
        {
            m_selectable.Select();
            //LevelSelectionUI.Instance.SelectLevel(m_level);
        }
    }
}
