﻿using Game.Actors;
using Game.Actors.Components;
using Game.Arena;
using Game.Globals;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;
using UnityEngine.Events;

namespace Game.UI.LevelSelect
{
    public class ElevatorScene : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onSceneStartedWithoutPlayers;
        [SerializeField] SceneID m_sceneID;
        [SerializeField] TeamFlagID[] m_pvpTeamIDs;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void Activate()
        {
            if (GameGlobals.Scenes.TryGetValue(m_sceneID, out var elevatorSceneRoot) 
                && elevatorSceneRoot is SceneSettings elevatorSceneSettings)
                GameGlobals.GroundPlaneY = elevatorSceneSettings.CameraSetupPos.position.y;
            else 
                Debug.LogWarning($"Scene not loaded {m_sceneID.name}");

            GameGlobals.Elevator.TryGetComponent(out ArenaPlatform elevatorPlatform);
            GameGlobals.Elevator.ShowElevatorVisuals(false);

            var playersJoined = GameGlobals.PlayerCount > 0;
            if (playersJoined) 
                SetupPlayersOnElevator(elevatorPlatform);
            else 
                m_onSceneStartedWithoutPlayers.Invoke();
        }

        void SetupPlayersOnElevator(ArenaPlatform elevatorPlatform)
        {
            var playerGos = new GameObject[GameGlobals.PlayerCount];
            for (var i = 0; i < GameGlobals.Players.Count; i++)
            {
                var player = GameGlobals.Players[i];
                GlobalOperations.MovePlayerToRuntimeSceneAndConnectHUD(player);

                if (GameGlobals.PVP && player.TryGetComponent(out TeamComponent tc))
                    tc.TeamFlag = m_pvpTeamIDs[i];

                player.gameObject.SetActive(true);
                // reset health of the player (eg. from game over -> retry || Results -> continue)
                player.OnRevive();

                GameGlobals.GameCamera.AddTarget(player.transform);

                // make sure player - inputs are enabled & active
                player.Input.gameObject.SetActive(true);
                player.Input.SwitchCurrentActionMap("Player");
                player.Input.ActivateInput();

                // position
                PutPlayerOnElevator(player, elevatorPlatform);

                playerGos[i] = player.gameObject;
            }

            // start elevator with players
            var elevatorMove = GameGlobals.Elevator.Move;
            elevatorMove.SetPlayersOnElevator(playerGos);
            elevatorMove.ParentPlayersOnElevatorTo(elevatorMove.transform);
            elevatorMove.StartElevator();
        }


        // ReSharper disable once SuggestBaseTypeForParameter
        static void PutPlayerOnElevator(Player player, ArenaPlatform elevatorPlatform)
        {
            player.TryGetComponent(out RigidbodyBehaviour rigidbodyComponent);
            player.transform.position = elevatorPlatform.OnPlatformPosition;
            rigidbodyComponent.Position = elevatorPlatform.OnPlatformPosition;
            if (player.TryGetComponent<PlatformRaycastDownBehaviour>(out var raycastDown))
                raycastDown.CheckGrounded();
            if (player.TryGetComponent<CoyoteFallBehaviour>(out var coyoteBehaviour))
                coyoteBehaviour.CheckGrounded();
        }
    }
}
