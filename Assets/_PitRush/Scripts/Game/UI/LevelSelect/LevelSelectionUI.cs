﻿using System;
using System.Collections;
using Game.Arena.Level;
using Game.Configs;
using Game.Globals;
using Game.SaveLoad;
using Game.UI.PlayerSelection;
using Game.UI.ResultScreen;
using GameUtility.Feature.Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.LevelSelect
{
    public class LevelSelectionUI : MonoBehaviour
    {
        // public static LevelSelectionUI Instance;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] TMP_Text m_levelTitle;

        [SerializeField] GameObject[] m_stars;
        [SerializeField] Image[] m_starsOff;
        [SerializeField] Image[] m_starsOn;

        [SerializeField] TMP_Text[] m_starRequiredCoins;

        [SerializeField] StatElement m_bestTime;
        [SerializeField] StatElement m_coinsHighscore;

        [SerializeField] TMP_Text m_goldTime;
        [SerializeField] TMP_Text m_silverTime;
        [SerializeField] TMP_Text m_bronzeTime;
        [SerializeField] TMP_Text m_goldReward;
        [SerializeField] TMP_Text m_silverReward;
        [SerializeField] TMP_Text m_bronzeReward;

        [SerializeField] Image m_holdingBackFill;
        [SerializeField] float m_holdBackTime;

        [SerializeField] RectTransform m_levi;
        [SerializeField] float m_leviTime;

        [SerializeField] LayoutGroup m_levelGroup;
        [SerializeField] SceneID m_lvlSelectScene;
        [SerializeField] SceneID m_charSelectScene;
        [SerializeField] SceneID m_elevatorScene;
        [SerializeField] LoadOperationID m_loadID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        RectTransform m_leviTarget;
        Vector2 m_leviPreviousPos;
        float m_leviTimer;

        bool m_holdingBack;
        float m_holdingProgress;

        LevelSelectElement m_currentSelectElement;

        void OnEnable()
        {
            // Instance = this;
            SaveSystem.LoadLevelData();
            if (m_levelGroup == null)
                return;
            var levelSelectElements = m_levelGroup.GetComponentsInChildren<LevelSelectElement>();
            foreach (var lvlEl in levelSelectElements) 
                lvlEl.Init();
        }

        void Update()
        {
            if (LoadOperations.IsLoading)
                return;

            UpdateHoldingBackButton();
            UpdateLeviPosition();
        }

        void UpdateLeviPosition()
        {
            if (m_levi == null || m_leviTarget == null)
                return;

            m_leviTimer += Time.deltaTime;
            m_levi.anchoredPosition =
                Vector2.Lerp(m_leviPreviousPos, m_leviTarget.anchoredPosition, m_leviTimer / m_leviTime);
        }

        public void OnSelectionChanged(GameObject selected)
        {
            if (LoadOperations.IsLoading)
                return;

            m_leviPreviousPos = m_levi.anchoredPosition;
            m_leviTimer = 0f;

            selected.TryGetComponent(out m_currentSelectElement);
            if (m_currentSelectElement == null)
                return;

            m_leviTarget = m_currentSelectElement.LeviSocket;
            InitUIWithLevel(m_currentSelectElement.Level);
        }

        void InitUIWithLevel(ArenaLevelFlowConfig level)
        {
            if (ConfigLinks.Instance == null)
                return;

            UpdateStarGraphic(level);

            SaveSystem.GetScores(level, out var highScore, out var bestTime);
            if (highScore <= 0) 
                m_coinsHighscore.ResetWithNone();
            else 
                m_coinsHighscore.ResetWith(highScore,highScore);

            if (bestTime >= float.MaxValue-float.Epsilon) 
                m_bestTime.ResetWithNone();
            else 
                m_bestTime.ResetWith(bestTime,bestTime);

            var stars = level.Result.GetStars(highScore);
            for (var i = 0; i < m_stars.Length; i++)
                m_stars[i].SetActive(stars > i);
            
            m_levelTitle.text = level.name;

            var resultData = level.Result;

            m_goldTime.SetText(TimeSpan.FromSeconds(resultData.Gold.TimeSeconds).ToString(@"mm\:ss"));
            m_silverTime.SetText(TimeSpan.FromSeconds(resultData.Silver.TimeSeconds).ToString(@"mm\:ss"));
            m_bronzeTime.SetText(TimeSpan.FromSeconds(resultData.Bronze.TimeSeconds).ToString(@"mm\:ss"));

            m_goldReward.SetText(resultData.Gold.CoinsReward.ToString());
            m_silverReward.SetText(resultData.Silver.CoinsReward.ToString());
            m_bronzeReward.SetText(resultData.Bronze.CoinsReward.ToString());

            m_starRequiredCoins[0].text = $"{resultData.BronzeTargetCoins}";
            m_starRequiredCoins[1].text = $"{resultData.SilverTargetCoins}";
            m_starRequiredCoins[2].text = $"{resultData.GoldTargetCoins}";

        }

        void UpdateStarGraphic(ArenaLevelFlowConfig level)
        {
            var levelData = ConfigLinks.LevelsConfig.Get(level);
            foreach (var starOff in m_starsOff)
                starOff.sprite = levelData.LampGraphicOff;
            foreach (var starOn in m_starsOn)
                starOn.sprite = levelData.LampGraphicOn;
        }

        void UpdateHoldingBackButton()
        {
            var wasHolding = m_holdingProgress > 0.0f;
            if (!wasHolding && !m_holdingBack)
                return;

            if (m_holdingBack)
                m_holdingProgress += Time.deltaTime;
            else
                m_holdingProgress = 0f;

            var progress= Mathf.Clamp01(m_holdingProgress / m_holdBackTime);
            m_holdingBackFill.fillAmount = progress;
            if (progress >= 1.0f)
                OnBack();
        }

        void OnBack()
        {
            if (!GameGlobals.Scenes.TryGetValue(m_lvlSelectScene, out var lvlSelectScene) || 
                !GameGlobals.Scenes.TryGetValue(m_charSelectScene, out var charSelectScene))
                return;

            if (lvlSelectScene.IsInTransition)
                return;
            
            lvlSelectScene.SetActive(false);
            charSelectScene.SetActive(true);

            OnBackButtonRelease();
        }


        public void OnBackButtonDown() => m_holdingBack = true;
        public void OnBackButtonRelease() => m_holdingBack = false;


        public void ConfirmSelection()
        {
            if (LoadOperations.IsLoading)
                return;

            StartCoroutine(SelectLevel(m_currentSelectElement.Level, 
                m_loadID, m_lvlSelectScene, m_elevatorScene));
        }

        public static IEnumerator SelectLevel(ArenaLevelFlowConfig level, 
            LoadOperationID loadId, SceneID inactivateId, SceneID activateId)
        {
            LoadOperations.StartLoading(loadId);

            ArenaLevelFlowConfig.Current = level;
            
            PlayerSelectionUI.Instance.LinkPlayersAndInput();

            while (LoadingCanvas.Instance.IsInTransition)
                yield return null;

            GameGlobals.Scenes[inactivateId].SetActive(false);
            GameGlobals.Scenes[activateId].SetActive(true);

            yield return null;
            LoadOperations.LoadingDone(loadId);
        }

        // fixes bug also where title-screen controls are blocked
        public void OnSceneActivate() => GlobalOperations.MovePlayersToRuntimeSceneAndConnectHUD();
    }
}
