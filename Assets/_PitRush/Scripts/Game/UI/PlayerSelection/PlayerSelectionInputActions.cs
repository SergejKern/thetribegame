using System;
using UnityEngine.InputSystem;

namespace Game.UI.PlayerSelection
{
    public struct SelectionInputActionLinks
    {
        public PlayerInput Input;
        public PlayerSelectionUISlot UISlot;

        public Action<InputAction.CallbackContext> OnNavigate;
        public Action<InputAction.CallbackContext> OnConfirm;
        public Action<InputAction.CallbackContext> OnLeave;
        public Action<InputAction.CallbackContext> OnBack;

        public Action<InputAction.CallbackContext> OnBackHolding;
    }
}