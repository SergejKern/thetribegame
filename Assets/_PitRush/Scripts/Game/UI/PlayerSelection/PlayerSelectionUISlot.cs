using System;
using System.Collections;
using System.Collections.Generic;
using Core.Unity.Extensions;
using Game.Actors;
using Game.Configs;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Game.UI.PlayerSelection
{
    public class PlayerSelectionUISlot : MonoBehaviour
    {
        public enum UIState
        {
            Offline,
            Selecting,
            Selected
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        //[SerializeField] internal TextMeshProUGUI m_playerLabel;
        [SerializeField] GameObject m_camera;

        [SerializeField] GameObject[] m_offlineState;
        [SerializeField] GameObject[] m_selectingState;
        [SerializeField] GameObject[] m_selectedState;

        [SerializeField] Image[] m_colorizeImages;
        [SerializeField] TMP_Text[] m_colorizeTexts;

        [SerializeField] RawImage m_renderTexImage;
#pragma warning restore 0649 // wrong warnings for SerializeField

        internal int CharacterIdx => CurrentPlayer.CharacterIdx;

        bool HasInput => Input != null;

        public bool Done => !HasInput || IsSelected;
        public bool IsSelected => HasInput && State == UIState.Selected;
        public bool IsOffline => State == UIState.Offline;

        PlayerInput Input { get; set; }

        public Player CurrentPlayer { get; private set; }
        public UIState State { get; private set; }

        //string m_playerName;
        LookAtConstraint m_lookAtCamera;
        PositionConstraint m_posCamera;
        Coroutine m_activateRoutine;

        public void Init(SelectableCharacter character, PlayerInput input)
        {
            Input = input;

            //m_playerName = $"P{Input.playerIndex + 1}";

            ChangeState(UIState.Selecting);

            m_camera.TryGetComponent(out m_lookAtCamera);
            m_camera.TryGetComponent(out m_posCamera);

            SetTarget(character);
        }

        public void OnBack()
        {
            switch (State)
            {
                case UIState.Offline: break;
                case UIState.Selecting: OnLeave(); break;
                case UIState.Selected: ChangeState(UIState.Selecting); break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public void OnLeave()
        {
            if (State == UIState.Offline)
                return;
            // if (InputActions.Input!= null) InputActions.Input.gameObject.DestroyEx();
            Input = null;
            ChangeState(UIState.Offline);
        }

        public void Confirm() => ChangeState(UIState.Selected);

        void ChangeState(UIState state)
        {
            var prevState = State;
            GetStateObjects(prevState).SetActive(false);
            GetStateObjects(state).SetActive();
            State = state;
        }

        IEnumerable<GameObject> GetStateObjects(UIState s)
        {
            switch (s)
            {
                case UIState.Offline: return m_offlineState;
                case UIState.Selecting: return m_selectingState;
                case UIState.Selected: return m_selectedState;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public void SetTarget(SelectableCharacter character)
        {
            if (m_posCamera == null || m_lookAtCamera == null)
                return;
            CurrentPlayer = character.Player;

            var charColor = ConfigLinks.Characters.GetColor(character.Player.CharacterIdx);
            foreach (var ci in m_colorizeImages)
                ci.color = charColor;

            foreach (var ct in m_colorizeTexts)
                ct.color = charColor;

            if (m_activateRoutine != null)
                StopCoroutine(m_activateRoutine);

            m_renderTexImage.gameObject.SetActive(false);

            var cs = new ConstraintSource() {sourceTransform = character.transform, weight = 1f};
            m_posCamera.SetSource(0, cs);
            m_lookAtCamera.SetSource(0, cs);
            m_activateRoutine = StartCoroutine(ActivateRenderTexture());
        }

        IEnumerator ActivateRenderTexture()
        {
            yield return null;
            yield return null;

            m_renderTexImage.gameObject.SetActive(true);
        }
    }
}
