﻿using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using Game.Configs;
using GameUtility.Data.Visual;
using GameUtility.PooledEventSpawning;
using UnityEngine;

namespace Game.UI.PlayerSelection
{
    public class SelectableCharacter : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Transform m_spawnPos;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public Player Player { get; private set; }

        bool Initialized => Player != null;
        public void Init(int charIdx)
        {
            if (Initialized)
            {
                Player.gameObject.SetActive(true);
                return;
            }

            var character = ConfigLinks.Characters.Characters[charIdx];
            var playerGo = character.Prefab.GetPooledInstance(m_spawnPos.position, m_spawnPos.rotation);
            //playerGo.transform.SetParent(thisTransform);

            Player = playerGo.GetComponent<Player>();
            Player.InitCharacterIdx(charIdx);

            playerGo.name = "Player_" + charIdx;
            PrefabSpawnedEvent.Trigger(playerGo, character.Prefab);

            MaterialExchange.Override(new MaterialExchangeData2(){ ForObject = playerGo, MaterialMap = character.ModelOverrideMaterialMap});
        }
    }
}
