using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Extensions;
using Game.Actors;
using Game.Configs;
using Game.Globals;
using Game.UI.LevelSelect;
using GameUtility.Feature.Loading;
using GameUtility.Operations;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.UI.PlayerSelection
{
    public class PlayerSelectionUI : MonoBehaviour
    {
        public static PlayerSelectionUI Instance;

#pragma warning disable 0649 // wrong warnings for SerializeField
        // todo 2: verification etc.
        // [SerializeField] InputActionAsset m_inputAsset;
        [SerializeField] string m_menuActionMapNameOrId;
        //[SerializeField] string m_playerActionMapNameOrId;

        [SerializeField] InputActionReference m_leaveActionRef;
        [SerializeField] InputActionReference m_backActionRef;
        [SerializeField] InputActionReference m_navigateActionRef;
        [SerializeField] InputActionReference m_confirmActionRef;
        [SerializeField] InputActionReference m_backHoldingActionRef;

        //[SerializeField] internal GameObject m_selectSlotPrefab;

        [SerializeField] internal SelectableCharacter[] m_characters;
        [SerializeField] internal PlayerSelectionUISlot[] m_playerUISlots = new PlayerSelectionUISlot[4];

        // todo 4: refactor StatElement or something
        [SerializeField] TextMeshProUGUI m_counter;
        [SerializeField] Image m_counterFill;
        [SerializeField] CanvasGroup m_counterCanvasGroup;

        [SerializeField] Image m_holdingBackFill;
        [SerializeField] float m_holdBackTime;

        [SerializeField] SceneID m_charSelectSceneID;
        [SerializeField] SceneID m_lvlSelectSceneID;
        [SerializeField] SceneID m_elevatorSceneID;
        [SerializeField] SceneID m_title;

        [SerializeField] LoadOperationID m_loadId;
        //[SerializeField] UnityEvent m_onSelectionFinished;
        //[SerializeField] LayoutGroup m_layoutGroup;
#pragma warning restore 0649 // wrong warnings for SerializeField
        bool AllPlayersDone => m_playerUISlots.All(s => s.Done) 
                               && m_playerUISlots.Count(s=>s.IsSelected) >= (GameGlobals.PVP?2:1);

        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
        bool Enabled => this != null && gameObject.activeInHierarchy && enabled;
        float m_countDown;
        Coroutine m_coroutine;

        bool m_holdingBack;
        float m_holdingProgress;

        readonly List<SelectionInputActionLinks> m_inputLinks = new List<SelectionInputActionLinks>();

        UnityAction<PlayerInput> m_deviceJoinedAction;
        UnityAction<PlayerInput> m_deviceLeftAction;


        void OnEnable()
        {
            Instance = this;

            StopAndHideCountdown();
            DeselectPlayers();
        }
        
        void OnDisable() => RemovePlayerInputListener();

        void RemovePlayerInputListener()
        {
            if (GameGlobals.PlayerInputManager == null)
                return;
            if (GameGlobals.PlayerInputManager.playerJoinedEvent == null ||
                GameGlobals.PlayerInputManager.playerLeftEvent == null)
                return;
            if (m_deviceJoinedAction == null || m_deviceLeftAction == null)
                return;
            GameGlobals.PlayerInputManager.playerJoinedEvent.RemoveListener(m_deviceJoinedAction);
            GameGlobals.PlayerInputManager.playerLeftEvent.RemoveListener(m_deviceLeftAction);
            m_deviceJoinedAction = null;
            m_deviceLeftAction = null;
        }

        void Update()
        {
            if (LoadOperations.IsLoading)
                return;
            UpdateHoldingBackButton();
        }

        void UpdateHoldingBackButton()
        {
            var wasHolding = m_holdingProgress > 0.0f;
            if (!wasHolding && !m_holdingBack)
                return;

            if (m_holdingBack)
                m_holdingProgress += Time.deltaTime;
            else
                m_holdingProgress = 0f;

            var progress= Mathf.Clamp01(m_holdingProgress / m_holdBackTime);
            m_holdingBackFill.fillAmount = progress;
            if (progress >= 1.0f)
                GoBackToStartScreen();
        }

        // Link on scene loaded
        public void LinkPlayerInputManager()
        {
            //Debug.Log("LinkPlayerInputManager");
            InitCharacters();
            DestroyPlayersFromPreviousRun();
            AddPlayerInputListener();

            JoinExistingDevices();
        }

        void DestroyPlayersFromPreviousRun()
        {
            for (var i = GameGlobals.Players.Count-1; i >= 0; i--)
            {
                var player = GameGlobals.Players[i];
                var playerIsFromPreviousRun = Array.FindIndex(m_characters, c => c.Player == player) == -1;
                if (!playerIsFromPreviousRun)
                    continue;
                player.Disconnect();
                player.gameObject.TryDespawnOrDestroy();
                GameGlobals.Players.RemoveAt(i);
            }
        }

        void JoinExistingDevices()
        {
            var inputs = FindObjectsOfType<PlayerInput>();
            foreach (var input in inputs)
            {
                if (DeviceKnown(input))
                    continue;

                OnDeviceJoined(input);
            }
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        bool DeviceKnown(PlayerInput input) => m_inputLinks.Find(link => link.Input == input).Input != null;

        void AddPlayerInputListener()
        {
            if (GameGlobals.PlayerInputManager == null)
            {
                Debug.LogError("No PlayerInputManager!");
                return;
            }

            GameGlobals.PlayerInputManager.EnableJoining();

            m_deviceJoinedAction = OnDeviceJoined;
            m_deviceLeftAction = OnDeviceLeft;

            GameGlobals.PlayerInputManager.playerJoinedEvent.AddListener(m_deviceJoinedAction);
            GameGlobals.PlayerInputManager.playerLeftEvent.AddListener(m_deviceLeftAction);
        }

        //internal PlayerInputUI GetSelector(int i) => m_playerInputs[i];

        void InitCharacters()
        {
            for (var i = 0; i < m_characters.Length; i++)
                m_characters[i].Init(i);
        }

        void OnDeviceJoined(PlayerInput input)
        {
            if (!Enabled)
                return;

            input.SwitchCurrentActionMap(m_menuActionMapNameOrId);

            var inputLinks = new SelectionInputActionLinks
            {
                Input = input,
                OnConfirm = context => OnConfirmButtonPressed(input),
                OnLeave = context => OnLeave(input),
                OnBack = context => OnBack(input),
                OnNavigate = context => OnNavigate(context, input),
                OnBackHolding = BackHolding
            };

            m_inputLinks.Add(inputLinks);

            InitActions(input, inputLinks);
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        void ConnectSlotAndInput(PlayerInput input)
        {
            var ilIdx= m_inputLinks.FindIndex(il => il.Input == input);
            if (ilIdx == -1)
                return;

            var inputLink = m_inputLinks[ilIdx];
            var uiSlot = GetUnassignedUISlot();
            inputLink.UISlot = uiSlot;
            uiSlot.Init(GetAvailableCharacter(), input);
            m_inputLinks[ilIdx] = inputLink;
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        void DisconnectSlotAndInput(PlayerInput input)
        {
            if (!Enabled)
                return;
            var ilIdx= m_inputLinks.FindIndex(il => il.Input == input);
            if (ilIdx==-1)
                return;

            var inputLink = m_inputLinks[ilIdx];
            if (inputLink.UISlot == null)
                return;

            inputLink.UISlot.OnLeave();
            inputLink.UISlot = null;
            m_inputLinks[ilIdx] = inputLink;
        }

        void InitActions(PlayerInput input, SelectionInputActionLinks inputLinks)
        {
            input.actions[m_leaveActionRef.name].Add(inputLinks.OnLeave);
            input.actions[m_backActionRef.name].Add(inputLinks.OnBack);
            input.actions[m_navigateActionRef.name].Add(inputLinks.OnNavigate);
            input.actions[m_confirmActionRef.name].Add(inputLinks.OnConfirm);
            input.actions[m_backHoldingActionRef.name].Add(inputLinks.OnBackHolding);
        }

        void BackHolding(InputAction.CallbackContext obj) => m_holdingBack = obj.ReadValue<float>() >= 1f;

        PlayerSelectionUISlot GetUnassignedUISlot()
        {
            var idx = Array.FindIndex(m_playerUISlots, p => p.IsOffline);
            var pi = m_playerUISlots[idx];
            return pi;
        }

        SelectableCharacter GetAvailableCharacter() => 
            m_characters.Where((t, i) => !IsPlayerSelected(i)).FirstOrDefault();

        void OnDeviceLeft(PlayerInput input)
        {
            var idx = m_inputLinks.FindIndex(link => link.Input == input);
            if (idx == -1)
                return;

            input.actions[m_backActionRef.name].Remove(m_inputLinks[idx].OnBack);
            input.actions[m_leaveActionRef.name].Remove(m_inputLinks[idx].OnLeave);
            input.actions[m_navigateActionRef.name].Remove(m_inputLinks[idx].OnNavigate);
            input.actions[m_confirmActionRef.name].Remove(m_inputLinks[idx].OnConfirm);
            input.actions[m_backHoldingActionRef.name].Remove(m_inputLinks[idx].OnBackHolding);

            if (m_inputLinks[idx].UISlot != null)
                m_inputLinks[idx].UISlot.OnLeave();

            m_inputLinks.RemoveAt(idx);
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        void OnNavigate(InputAction.CallbackContext cc, PlayerInput input)
        {            
            if (!Enabled || LoadOperations.IsLoading)
                return;
            var inputLinks = m_inputLinks.Find(il => il.Input == input);
            var uiSlot = inputLinks.UISlot;
            if (uiSlot == null)
                return;
            var vec = cc.ReadValue<Vector2>();
            const float kEpsilon = 0.01f;
            if (vec.x < -kEpsilon)
                OnLeftButtonPressed(uiSlot);
            if (vec.x > kEpsilon)
                OnRightButtonPressed(uiSlot);
        }

        void OnLeftButtonPressed(PlayerSelectionUISlot uiSLot) => CycleCharacter(uiSLot, -1);
        void OnRightButtonPressed(PlayerSelectionUISlot uiSlot) => CycleCharacter(uiSlot, 1);

        void CycleCharacter(PlayerSelectionUISlot uiSlot, int dir)
        {
            if (LoadOperations.IsLoading)
                return;
            if (uiSlot.State != PlayerSelectionUISlot.UIState.Selecting)
                return;

            var prevCharIdx = uiSlot.CharacterIdx;
            var cl = m_characters.Length;

            for (var slot = (prevCharIdx + cl + dir) % cl;
                slot != prevCharIdx;
                slot = (slot + cl + dir) % cl)
            {
                if (TrySelect(uiSlot, slot))
                    break;
            }
        }

        bool TrySelect(PlayerSelectionUISlot uiSlot, int charIdx)
        {
            if (!m_characters.IsIndexInRange(charIdx))
            {
                Debug.LogError("Should never happen");
                return true;
            }
            if (IsPlayerSelected(charIdx))
                return false;

            uiSlot.SetTarget(m_characters[charIdx]);
            return true;
        }

        bool IsPlayerSelected(int charIdx) => m_playerUISlots.Any(slot=> slot.IsSelected && slot.CurrentPlayer == m_characters[charIdx].Player);

        // ReSharper disable once SuggestBaseTypeForParameter
        void OnConfirmButtonPressed(PlayerInput input)
        {
            if (!Enabled || LoadOperations.IsLoading)
                return;

            var inputLinks = m_inputLinks.Find(il => il.Input == input);
            var uiSlot = inputLinks.UISlot;
            if (uiSlot == null)
            {
                StopAndHideCountdown();
                ConnectSlotAndInput(input);
                return;
            }
            if (uiSlot.State == PlayerSelectionUISlot.UIState.Selected)
                return;

            uiSlot.Confirm();

            //_todo remove on cancel:
            //player.AddControlBlocker(new ControlBlocker() { Source = GameGlobals.CharacterSelectionScene });
            //GameGlobals.GameCamera.AddTarget(player.transform);
            // todo 4: remove & disconnect SelectionInputActionLinks (when starting game, or leaving for good)

            FixOtherUISlots(uiSlot);

            StopAndHideCountdown();
            if (AllPlayersDone && m_coroutine == null)
                m_coroutine = StartCoroutine(Countdown());
        }

        IEnumerator Countdown()
        {
            m_countDown = 3;
            m_counter.text = $"{m_countDown:F0}";
            m_counterFill.fillAmount = 0f;

            while (m_counterCanvasGroup.alpha < 1f)
            {
                m_counterCanvasGroup.alpha += Time.deltaTime;
                yield return null;
            }

            while (m_countDown > 0)
            {
                m_countDown -= Time.deltaTime;
                m_counter.text = $"{m_countDown:F0}";
                m_counterFill.fillAmount = 1f - (m_countDown / 3f);
                yield return null;
            }

            m_coroutine = null;
            SelectionDone();
        }

        void StopAndHideCountdown()
        {
            if (m_coroutine != null)
            {
                StopCoroutine(m_coroutine);
                m_coroutine = null;
            }

            m_counterCanvasGroup.alpha = 0f;
        }

        void SelectionDone()
        {
            GameGlobals.PlayerInputManager.DisableJoining();
            foreach (var character in m_characters.Where((t, i) => !IsPlayerSelected(i)))
                character.Player.gameObject.SetActive(false);
            //GameGlobals.PlayerInputManager.gameObject.SetActive(false);
            if (GameGlobals.PVP)
            {
                StartCoroutine(LevelSelectionUI.SelectLevel(ConfigLinks.LevelsConfig.PVPLevel, m_loadId,
                    m_charSelectSceneID, m_elevatorSceneID));
            }
            else
            {
                // switch scene
                GameGlobals.Scenes[m_charSelectSceneID].SetActive(false);
                GameGlobals.Scenes[m_lvlSelectSceneID].SetActive(true);
                // m_onSelectionFinished.Invoke();
            }
        }

        public void LinkPlayersAndInput()
        {
            foreach (var link in m_inputLinks)
            {
                if (link.UISlot == null)
                    continue;
                var player = link.UISlot.CurrentPlayer;
                player.InitializeWithController(link.Input);
                GameGlobals.Players.Add(player);
            }
            GameGlobals.UpdatePlayerCount();
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        void FixOtherUISlots(PlayerSelectionUISlot recentSelectedUISlot)
        {
            var availablePlayer = GetAvailableCharacter();
            if (availablePlayer == null)
                return;

            foreach (var uiSlot in m_playerUISlots)
            {
                if (recentSelectedUISlot == uiSlot)
                    continue;
                if (uiSlot.CurrentPlayer != recentSelectedUISlot.CurrentPlayer)
                    continue;

                uiSlot.SetTarget(availablePlayer);
            }
        }

        void OnBack(PlayerInput input)
        {
            if (!Enabled || LoadOperations.IsLoading)
                return;

            var inputLinks = m_inputLinks.Find(il => il.Input == input);
            var uiSlot = inputLinks.UISlot;
            if (uiSlot == null)
                return;

            StopAndHideCountdown();
            uiSlot.OnBack();

            if (uiSlot.State == PlayerSelectionUISlot.UIState.Offline)
                DisconnectSlotAndInput(input);

            if (AllPlayersDone)
                m_coroutine = StartCoroutine(Countdown());
        }

        void OnLeave(PlayerInput input)
        {
            if (LoadOperations.IsLoading)
                return;

            StopAndHideCountdown();

            DisconnectSlotAndInput(input);
        }

        void GoBackToStartScreen()
        {
            GameGlobals.Scenes[m_charSelectSceneID].SetActive(false);
            SceneManager.LoadSceneAsync(SceneLinks.Instance.GetScene(m_title), LoadSceneMode.Single);
        }
        void DeselectPlayers()
        {
            foreach (var slot in m_playerUISlots)
            {
                if (slot.IsSelected)
                    slot.OnBack();
            }
        }
    }
}