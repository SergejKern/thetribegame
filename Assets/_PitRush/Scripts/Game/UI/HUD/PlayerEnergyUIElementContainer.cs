﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Attribute;
using GameUtility.Energy;
using UnityEngine;
using UnityEngine.Events;

namespace Game.UI.HUD
{
    public class PlayerEnergyUIElementContainer : MonoBehaviour
    {
        [Serializable]
        public struct Events
        {
            public UnityEvent OnEmergency;
            public UnityEvent OnEmergencyOver;

            public UnityEvent OnDepleted;
            public UnityEvent OnRefilled;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIEnergyUIElement m_elementPrefab;

        [SerializeField] List<RefIEnergyUIElement> m_initElements = new List<RefIEnergyUIElement>();

        [SerializeField] bool m_inverted;
        [SerializeField] bool m_connectElements;

        [SerializeField] bool m_useEmergencyColor;
        [SerializeField, ShowIf(nameof(m_useEmergencyColor))]
        Color m_emergencyColor;

        [SerializeField] Events m_events;
#pragma warning restore 0649 // wrong warnings for SerializeField

        GameObject ElementPrefabGo => (m_elementPrefab?.Result as MonoBehaviour)?.gameObject;
        readonly List<IEnergyUIElement> m_elements = new List<IEnergyUIElement>();

        IEnergyComponent m_energy;
        Color m_playerColor;
        bool m_emergency;
        bool m_depleted;

        bool m_initializedElements;
        public void Initialize(int elementCount, Color playerColor)
        {
            m_playerColor = playerColor;
            // m_elements.Clear();
            if (m_initializedElements)
                return;
            m_initializedElements = true;

            if (m_elements.IsNullOrEmpty() && !m_initElements.IsNullOrEmpty())
                m_elements.AddRange(m_initElements.Select(initElement => initElement.Result));

            var i = 0;
            IEnergyUIElement prevElement = null;

            for (; i < m_elements.Count; i++)
            {
                m_elements[i].Init(m_playerColor);
                if (m_connectElements)
                    prevElement = LinkNeighborElements(m_elements[i], prevElement);
            }

            for (; i < elementCount; i++)
            {
                var go = Instantiate(ElementPrefabGo, transform, false);
                go.name = $"{ElementPrefabGo.name} {i}";
                if (!go.TryGetComponent<IEnergyUIElement>(out var comp))
                    continue;

                comp.Init(m_playerColor);
                if (m_connectElements)
                    prevElement = LinkNeighborElements(comp, prevElement);

                m_elements.Add(comp);
            }

            if (!m_inverted)
                return;

            InvertDirection(m_elements);
        }

        static IEnergyUIElement LinkNeighborElements(IEnergyUIElement comp, IEnergyUIElement prevElement)
        {
            comp.PreviousElement = prevElement;
            if (prevElement != null)
                prevElement.NextElement = comp;
            prevElement = comp;
            return prevElement;
        }

        static void InvertDirection(IReadOnlyList<IEnergyUIElement> elements)
        {
            for (var i = 0; i < elements.Count; i++)
            {
                var idx = GetInvertedIdx(elements, i);
                if (elements[i] is MonoBehaviour mono)
                    mono.transform.SetSiblingIndex(idx);
            }
        }

        static int GetInvertedIdx(IReadOnlyCollection<IEnergyUIElement> elements, int i)
        {
            var idx = elements.Count - (i + 1);
            return idx;
        }

        public void Connect(IEnergyComponent component)
        {
            m_energy = component;
            component.AddChangeListener(EnergyChanged);
            EnergyChanged(m_energy.Energy);
        }

        public void Disconnect() => m_energy?.RemoveChangeListener(EnergyChanged);

        void EnergyChanged(float energy)
        {
            var p = energy / m_energy.MaxEnergy;
            var activeCount = m_elements.Count * p;
            var fullElements = Mathf.FloorToInt(activeCount);
            var restP = activeCount - fullElements;
            //Debug.Log($"energy{energy:F3} mAX{m_energy.MaxEnergy:F3} {activeCount:F3} {fullElements:F3} {restP:F3}");
            var i = 0;
            for (; i < fullElements; i++)
            {
                if (i >= m_elements.Count)
                    break;

                //var idx = m_inverted?GetInvertedIdx(m_elements, i) : i;
                //Debug.Log($"update: {i} 1f");
                m_elements[i].SetTargetEnergy(1f);
            }
            //Debug.Log(i);
            for (; i < m_elements.Count; i++)
            {
                //var idx = m_inverted?GetInvertedIdx(m_elements, i) : i;
                //if (restP > 0f) Debug.Log($"update: {i} {restP:F3}");
                m_elements[i].SetTargetEnergy(restP);
                restP = 0f;
            }

            var depleted = energy <= 0f;
            UpdateEmergency(!depleted && fullElements <= 1);
            UpdateDepletionState(depleted);
        }

        // ReSharper disable once FlagArgument
        void UpdateDepletionState(bool depleted)
        {
            if (m_depleted == depleted)
                return;
            m_depleted = depleted;
            if (m_depleted)
                m_events.OnDepleted.Invoke();
            else
                m_events.OnRefilled.Invoke();
        }

        // ReSharper disable once FlagArgument
        void UpdateEmergency(bool isEmergency)
        {
            if (m_emergency == isEmergency)
                return;
            m_emergency = isEmergency;

            if (m_emergency)
            {
                m_events.OnEmergency.Invoke();
                if (m_useEmergencyColor)
                    ApplyColorOnElements(m_emergencyColor);
            }
            else
            {
                m_events.OnEmergencyOver.Invoke();
                if (m_useEmergencyColor)
                    ApplyColorOnElements(m_playerColor);
            }
        }

        void ApplyColorOnElements(Color c)
        {
            foreach (var el in m_elements)
                el.SetColor(c);
        }

        internal void Preview(int previewCount)
        {
            var prevElements = new List<IEnergyUIElement>();
            if (!m_initElements.IsNullOrEmpty()) 
                prevElements.AddRange(m_initElements.Select(initElement => initElement.Result));

            for (var i = m_elements.Count; i < previewCount; i++)
            {
                var go = Instantiate(ElementPrefabGo, transform, false);
                go.hideFlags = HideFlags.DontSave;
                if (!go.TryGetComponent<IEnergyUIElement>(out var comp))
                    continue;
                prevElements.Add(comp);
            }
            if (!m_inverted)
                return;

            InvertDirection(prevElements);
        }

        public void NoEnergyFeedback()
        {
            foreach (var el in m_elements)
                el.NoEnergyFeedback();
        }
    }
}
