﻿using System;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Types;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.HUD
{
    public class PlayerEnergyUIElement : MonoBehaviour, IEnergyUIElement
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_fullEnergy;
        [SerializeField] GameObject m_damagedEnergy;
        [SerializeField] GameObject[] m_energyParts;

        [SerializeField] bool m_exclusiveImages = true;

        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_noEnergyFeedback;
#pragma warning restore 0649 // wrong warnings for SerializeField

        PlayerEnergyUIElement m_previousElement;
        PlayerEnergyUIElement m_nextElement;

        public IEnergyUIElement PreviousElement
        {
            get => m_previousElement;
            set => m_previousElement = value as PlayerEnergyUIElement;
        }
        public IEnergyUIElement NextElement
        {
            get => m_nextElement;
            set => m_nextElement = value as PlayerEnergyUIElement;
        }

        Color m_imgTint;
        float m_currentPercentage;
        float m_targetPercentage;

        bool m_transitionDone;
        bool IsDeclining => m_targetPercentage < m_currentPercentage;
        bool IsRising => m_targetPercentage > m_currentPercentage;
        bool NeedsUpdate => Math.Abs(m_targetPercentage - m_currentPercentage) > float.Epsilon;
        bool Done => !NeedsUpdate && m_transitionDone;

        bool CanUpdate
        {
            get
            {
                var canDecline = IsDeclining && (m_nextElement == null || m_nextElement.Done);
                var canRise = IsRising && (m_previousElement == null || m_previousElement.Done);
                return canRise || canDecline;
            }
        }
        public void SetColor(Color imageTint)
        {
            m_imgTint = imageTint;
            UpdateColor();
        }

        public void Init(Color imageTint)
        {
            SetColor(imageTint);
            if (m_fullEnergy != null)
                m_fullEnergy.SetActive(false);
            if (m_damagedEnergy!= null)
                m_damagedEnergy.SetActive(false);
            foreach (var img in m_energyParts)
                img.SetActive(false);
        }

        void UpdateColor()
        {
            if (m_fullEnergy != null && m_fullEnergy.TryGetComponent<Image>(out var fullImg))
                fullImg.color = m_imgTint;
            if (m_damagedEnergy != null && m_damagedEnergy.TryGetComponent<Image>(out var brokenImg))
                brokenImg.color = m_imgTint;
            foreach (var img in m_energyParts)
            {
                if (!img.TryGetComponent<Image>(out var energyImg))
                    continue;
                energyImg.color = m_imgTint;
            }
        }

        public void SetTargetEnergy(float percentage)
        {
            if (Mathf.Abs(m_targetPercentage-percentage) < float.Epsilon)
                return;
            //Debug.Log($"{transform.parent.name}.{gameObject.name} Set to {percentage}");
            m_targetPercentage = percentage;
            m_transitionDone = Math.Abs(m_targetPercentage - m_currentPercentage) < float.Epsilon;

            //if (CanUpdate)
            //    UpdateEnergy();
        }

        void Update()
        {
            if (NeedsUpdate && CanUpdate)
                UpdateEnergy();
        }

        void UpdateEnergy()
        {
            m_currentPercentage = m_targetPercentage;

            var pPerElement = 1f;
            if (m_energyParts.Length > 1 && m_exclusiveImages)
                pPerElement = 1f / (m_energyParts.Length - 1);

            var isFullElement = m_targetPercentage >= 1f - float.Epsilon;
            m_transitionDone = true;
            if (m_fullEnergy != null)
            {
                m_fullEnergy.SetActiveWithTransition(isFullElement, out var isInTransition);
                m_transitionDone &= !isInTransition;
            }
            if (m_damagedEnergy != null)
            {
                m_damagedEnergy.SetActiveWithTransition(!isFullElement, out var isInTransition);
                m_transitionDone &= !isInTransition;
            }
            if (m_energyParts.IsNullOrEmpty())
                return;

            var minP = 0f;
            var pRange = pPerElement;
            for (var i = 0; i < m_energyParts.Length - 1; i++, minP += pPerElement, pRange += pPerElement)
            {
                bool isInTransition;
                var minActive = m_targetPercentage > minP;
                var maxActive = m_targetPercentage <= pRange;
                if (m_exclusiveImages)
                {
                    var exclusiveActive = minActive && maxActive && !isFullElement;
                    m_energyParts[i].SetActiveWithTransition(exclusiveActive, out isInTransition);
                }
                else
                    m_energyParts[i].SetActiveWithTransition(minActive, out isInTransition);
                m_transitionDone &= !isInTransition;
            }

            {
                var fullHealthPart = m_energyParts[m_energyParts.Length - 1];
                fullHealthPart.SetActiveWithTransition(isFullElement, out var isInTransition);
                m_transitionDone &= !isInTransition;
            }
        }

        public void OnTransitionDone()
        {
            //Debug.Log($"{transform.parent.name}.{gameObject.name} OnTransitionDone {m_currentPercentage}");
            m_transitionDone = true;
            UpdateColor();
        }

        public void NoEnergyFeedback() => 
            m_noEnergyFeedback.Animator.Play(m_noEnergyFeedback);
    }
}
