﻿using System.Collections.Generic;
using Core.Unity.Extensions;
using Game.Actors;
using UnityEngine;

namespace Game.UI.HUD
{
    public class PlayerEnergyHUD : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField 
        [SerializeField] List<GameObject> m_playerEnergyUI;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void Awake()
        {
            foreach (var ui in m_playerEnergyUI)
                ui.SetActive(false);
        }

        public void ConnectPlayer(Player p)
        {
            foreach (var uiGo in m_playerEnergyUI)
            {
                if (!uiGo.TryGetComponent<PlayerEnergyUI>(out var ui))
                    continue;
                var isAlreadyConnected = ui.ConnectedPlayer == p;
                if (isAlreadyConnected)
                    return;
                if (uiGo.IsActiveOrActivating())
                    continue;
                ui.ConnectWithPlayer(p);
                uiGo.SetActiveWithTransition();
                break;
            }
        }

        public void DisconnectPlayer(Player p)
        {
            foreach (var uiGo in m_playerEnergyUI)
            {
                if (!uiGo.IsActiveOrActivating())
                    continue;
                if (!uiGo.TryGetComponent<PlayerEnergyUI>(out var ui))
                    continue;
                if (ui.ConnectedPlayer != p)
                    continue;

                uiGo.SetActiveWithTransition(false);
                ui.Disconnect();
            }
        }

        //public void SetCheeringValue(Player p, float value)
        //{
        //    var playerUi = m_playerEnergyUI.Where(ui => ui.GetComponent<PlayerEnergyUI>().ConnectedPlayer == p) as PlayerEnergyUI;
        //    playerUi.CheeringBar.fillAmount = value;
        //}
    }
}
