﻿using System;
using System.Collections;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Game.Configs.Elevator;
using Game.Elevator;
using Game.Globals;
using Game.UI.Dialogue;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.UI.HUD
{
    public class LeviTextHUD : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("DialogueText")] [SerializeField] 
        TMP_Text m_dialogueText;

        [SerializeField] Animator m_animator;

        [SerializeField]
        LeviDialogueSequence.LeviFaceExpressions m_defaultExpression;
        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_startingAnim;
        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_endingAnim;

        [SerializeField] ExpressionToAnimation[] m_expressionAnimations 
            = new ExpressionToAnimation[LeviDialogueSequence.FaceExpressionCount];

        [SerializeField] float m_timeBetweenCharacters;
        [SerializeField] RefIAudioAsset m_characterAudio;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_dialogueIsRunning;


        public void StartDialogue(LeviDialogueSequence sequence)
        {
            if (m_dialogueIsRunning)
                ForceFinishDialogue();
            StartCoroutine(Dialogue(sequence));
        }

        public bool IsRunning() => m_dialogueIsRunning;

        IEnumerator Dialogue(LeviDialogueSequence sequence)
        {
            m_dialogueText.text = "";

            m_dialogueIsRunning = true;
            m_animator.Play(m_startingAnim.StateHash);
            m_animator.Update(float.Epsilon);
            while (m_animator.IsPlaying(m_startingAnim.StateName))
                yield return null;

            foreach (var element in sequence.DialogueElements)
            {
                if (element.Delay > 0f)
                    yield return new WaitForSeconds(element.Delay);

                PlayExpressionAnimation(element.LeviExpression);
                yield return StartCoroutine(ShowText(element));
                yield return ClearTextAfterSeconds(element.ClearAfterSeconds);
            }
            //yield return new WaitForSeconds(4f);
            if (sequence.SleepOnFinish)
            {
                Finish();

                m_animator.Update(float.Epsilon);
                while (m_animator.IsPlaying(m_endingAnim.StateName))
                    yield return null;
            }

            m_dialogueIsRunning = false;
        }

        IEnumerator ClearTextAfterSeconds(float sec)
        {
            yield return new WaitForSeconds(sec);
            m_dialogueText.text = "";
        }

        IEnumerator ShowText(LeviDialogueSequence.DialogueElement el)
        {
            var textLength = el.Text.Length;
            m_dialogueText.text = "";

            string sound = null;
            AK.Wwise.Event @event = null;
            for (var i = 0; i < textLength; i++)
            {
                var c = el.Text[i];
                if (c.Equals('[')) 
                    sound = "";
                else if (c.Equals(']'))
                {
                    
                    if (int.TryParse(sound, out var soundIdx))
                    {
                        @event?.Stop(gameObject);
                        @event = el.SoundSet[soundIdx];
                        @event.Post(gameObject);
                    }   
                    sound = null;
                }   
                else if (sound != null)
                    sound += c;
                else
                {
                    m_dialogueText.text += c;
                    //m_characterAudio?.Result?.Play(gameObject);
                    yield return new WaitForSeconds(m_timeBetweenCharacters);
                }
            }

            PlayExpressionAnimation(m_defaultExpression);
        }

        void PlayExpressionAnimation(LeviDialogueSequence.LeviFaceExpressions expression)
        {
            var anim = m_expressionAnimations[(int) expression];
            m_animator.Play(anim.Animation);

            if (GetLeviOnElevator(out var levOnElev)) 
                levOnElev.Play(expression);
        }

        static bool GetLeviOnElevator(out LeviOnElevator levOnElev)
        {
            levOnElev = null;
            var elevator = GameGlobals.Elevator;
            if (elevator == null || !elevator.gameObject.activeInHierarchy)
                return false;
            levOnElev = elevator.LeviOnElevator;
            return levOnElev != null && levOnElev.gameObject.activeInHierarchy;
        }

        void Finish()
        {
            m_animator.Play(m_endingAnim);
            if (GetLeviOnElevator(out var levOnElev)) 
                levOnElev.Finish();
        }

        public void ForceFinishDialogue()
        {
            StopAllCoroutines();
            m_animator.Play(m_endingAnim.StateHash,0, 1f);
            if (GetLeviOnElevator(out var levOnElev)) 
                levOnElev.Finish();

            m_dialogueIsRunning = false;
        }

        void OnValidate()
        {
            if (m_expressionAnimations.Length != LeviDialogueSequence.FaceExpressionCount)
                Array.Resize(ref m_expressionAnimations, LeviDialogueSequence.FaceExpressionCount);
            for (var i = 0; i < m_expressionAnimations.Length; i++)
                m_expressionAnimations[i].Expression = (LeviDialogueSequence.LeviFaceExpressions) i;
        }
    }
}
