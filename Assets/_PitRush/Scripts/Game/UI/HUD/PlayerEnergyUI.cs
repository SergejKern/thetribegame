﻿using Core.Extensions;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Game.Actors;
using Game.Actors.Controls;
using Game.Actors.Energy;
using Game.Configs;
using GameUtility.Energy;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game.UI.HUD
{
    public class PlayerEnergyUI : MonoBehaviour
    {
        enum State
        {
            Alive,
            Rush,
            Dead
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_aliveState;
        [SerializeField] GameObject m_rushState;
        [SerializeField] GameObject m_reviveState;

        [SerializeField] Image[] m_colorizeImages;
        [SerializeField] ParticleSystem[] m_colorizeSystems;

        [SerializeField] Image[] m_playerFace;
        //[SerializeField] Image m_cheeringBar;

        [SerializeField] PlayerEnergyUIElementContainer m_healthContainer;
        [SerializeField] PlayerEnergyUIElementContainer m_staminaContainer;
        [SerializeField] PlayerEnergyUIElementContainer[] m_rushContainer;

        [InspectorButton("Preview")] [SerializeField] [UsedImplicitly]
        bool m_ignoreMe;
#pragma warning restore 0649 // wrong warnings for SerializeField

        //public Image CheeringBar => m_cheeringBar;
        public Player ConnectedPlayer { get; private set; }
        
        bool m_alive;
        bool m_rushMode;
        State m_state;

        UnityAction m_noDashFeedback;
        public void ConnectWithPlayer(Player p)
        {
            ConnectedPlayer = p;
            m_alive = true;
            m_rushMode = false;

            var character = ConfigLinks.Characters.Characters[ConnectedPlayer.CharacterIdx];
            var face = character.CharacterFace;
            var color = character.UIColor;
            foreach (var faceImg in m_playerFace)
                faceImg.sprite = face;

            foreach (var img in m_colorizeImages)
                img.color = color;
            foreach (var sys in m_colorizeSystems)
            {
                var mainSys = sys.main;
                mainSys.startColor = color;
            }            
            //m_playerFace.color = color;

            gameObject.SetActiveWithTransition();

            if (m_healthContainer != null)
            {
                p.Get(out IHealthComponent health);
                m_healthContainer.Initialize(5, color);
                m_healthContainer.Connect(health);

                health.AddChangeListener(HealthChanged);
            }

            if (m_staminaContainer != null)
            {
                p.Get(out IStaminaComponent stamina);
                m_staminaContainer.Initialize(3, Color.white);
                m_staminaContainer.Connect(stamina);

                m_noDashFeedback = m_staminaContainer.NoEnergyFeedback;
            }

            if (p.Centre is MonoBehaviour mono 
                && mono.TryGetComponent(out DashControl dc))
                dc.AddListenerForNoEnergyFeedback(m_noDashFeedback);

            if (!m_rushContainer.IsNullOrEmpty())
            {
                p.Get(out RushComponent rush);
                foreach (var rc in m_rushContainer)
                {
                    rc.Initialize(1, Color.white);
                    rc.Connect(rush);
                }
                rush.AddRushModeListener(RushChanged);
            }
        }

        void RushChanged()
        {
            ConnectedPlayer.Get(out RushComponent rc);
            m_rushMode = rc.RushMode;
            UpdateActivityState();
        }

        void HealthChanged(float hp)
        {
            m_alive = hp > 0f;
            UpdateActivityState();
        }

        void UpdateActivityState()
        {
            if (m_alive)
                m_state = m_rushMode ? State.Rush : State.Alive;
            else m_state = State.Dead;

            m_aliveState.SetActive(m_state == State.Alive);
            m_rushState.SetActive(m_state == State.Rush);
            m_reviveState.SetActive(m_state == State.Dead);
        }

        public void Disconnect()
        {
            if (ConnectedPlayer.Centre is MonoBehaviour mono 
                && mono.TryGetComponent(out DashControl dc))
                dc.RemoveListenerForNoEnergyFeedback(m_noDashFeedback);

            m_healthContainer.Disconnect();
            m_staminaContainer.Disconnect();

            ConnectedPlayer.Get(out IHealthComponent hc);
            ConnectedPlayer.Get(out RushComponent rc);
    
            hc.RemoveChangeListener(HealthChanged);
            rc.RemoveRushModeListener(RushChanged);

            gameObject.SetActiveWithTransition(false);

            ConnectedPlayer = null;
        }

        [UsedImplicitly]
        internal void Preview()
        {
            m_healthContainer.Preview(5);
            m_staminaContainer.Preview(3);
            foreach (var rc in m_rushContainer)
                rc.Preview(1);
        }
    }
}
