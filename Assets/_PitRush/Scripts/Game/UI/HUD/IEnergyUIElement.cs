﻿using System;
using Core.Unity.Types;
using UnityEngine;

namespace Game.UI.HUD
{
    public interface IEnergyUIElement
    {
        IEnergyUIElement PreviousElement { set; }
        IEnergyUIElement NextElement{ set; }

        void SetColor(Color imageTint);
        void Init(Color imageTint);
        void SetTargetEnergy(float percentage);
        void NoEnergyFeedback();
    }

    
    [Serializable]
    public class RefIEnergyUIElement : InterfaceContainer<IEnergyUIElement> { }
}
