﻿using System;
using System.Collections;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Game.Arena;
using Game.Arena.Level;
using Game.Arena.Level.Mission;
using Game.Globals;
using Game.InteractiveObjects;
using Game.UI.ResultScreen;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.HUD
{
    public class WaveHUD : MonoBehaviour 
        //, IEventListener<KillEvent>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_waveGroup;
        [SerializeField] GameObject m_missionGroup;

        [SerializeField] TMPro.TMP_Text m_timer;
        [SerializeField] TMPro.TMP_Text m_waveCount;
        
        [SerializeField] TMPro.TMP_Text m_missionText;
        [SerializeField] TMPro.TMP_Text m_missionProgressText;

        [SerializeField] Image m_progressImage;
        [SerializeField] float m_maxProgressFill;

        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_waveAnnounceAnim;
        [SerializeField] TMPro.TMP_Text m_waveAnnounceText;

        [SerializeField] StatElement m_coins;
#pragma warning restore 0649 // wrong warnings for SerializeField

        IMissionAction m_currentMission;
        bool m_missionClearedShown;

        float m_progressValue;
        float m_progressTarget;

        LevelStatTracker m_tracker;

        //void OnEnable() => EventMessenger.AddListener(this);
        //void OnDisable() => EventMessenger.RemoveListener(this);

        void Update()
        {
            if (Time.timeScale <= float.Epsilon)
                return;

            UpdateTimer();
            UpdateCoins();
            UpdateMissionGUI();
            UpdateProgressFilling();
        }


        void ClearMission()
        {
            m_progressTarget = 0f;
            m_progressImage.fillAmount = 0f;
            m_missionText.text = "";

            m_currentMission = null;
            m_missionClearedShown = false;
        }

        public void UpdateWave(LevelTriggerKey currentTrigger, IMissionAction mission, LevelStatTracker tracker)
        {
            ClearMission();

            m_currentMission = mission;
            m_missionGroup.SetActive(m_currentMission!= null);
            if (m_currentMission != null && m_missionText != null)
                m_missionText.text = m_currentMission.MissionTypeText;

            m_tracker = tracker;

            var isWave = (currentTrigger >= LevelTriggerKey.Wave1 && currentTrigger <= LevelTriggerKey.Wave9);
            m_waveCount.gameObject.SetActive(isWave);

            m_waveGroup.SetActive(isWave);
            if (isWave)
                StartCoroutine(Announcement(currentTrigger));
            else
                m_waveAnnounceText.text = currentTrigger.ToString();
        }

        IEnumerator Announcement(LevelTriggerKey currentTrigger)
        {
            var anim = m_waveAnnounceAnim.Animator;

            while (anim.IsPlaying(m_waveAnnounceAnim.StateName))
                yield return null;

            var countString = ((int) currentTrigger - 1).ToString();
            m_waveCount.text = $"{countString} : {GameGlobals.CurrentLevelFlow.WaveCount}";

            m_waveAnnounceText.text = $"Wave {countString}";
            anim.Play(m_waveAnnounceAnim.StateName);
            anim.Update(0f);
            while (anim.IsPlaying(m_waveAnnounceAnim.StateName))
                yield return null;
            m_waveAnnounceText.text = m_currentMission.Announcement;
            anim.Play(m_waveAnnounceAnim.StateName);
        }

        void UpdateProgressFilling()
        {
            var pVal = m_progressTarget - m_progressValue;
            if (Mathf.Abs(pVal) < float.Epsilon)
                return;
            var updateValue = Mathf.Clamp(Mathf.Abs(pVal), 0, 0.5f * Time.deltaTime) * Mathf.Sign(pVal);
            m_progressValue += updateValue;

            var filling = m_progressValue;
            m_progressImage.fillAmount = filling;
        }

        void UpdateCoins()
        {
            if (Coin.CoinsCollected > m_coins.Target)
                m_coins.SetTarget(Coin.CoinsCollected);
        }

        void UpdateTimer()
        {
            if (m_timer != null && m_tracker != null)
                m_timer.SetText(TimeSpan.FromSeconds(m_tracker.Stats.Time).ToString(@"mm\:ss"));
        }

        void UpdateMissionGUI()
        {
            if (m_currentMission == null)
                return;
            var prevText = m_missionProgressText.text;
            if (m_currentMission.MissionFinished && !m_missionClearedShown)
            {
                m_missionClearedShown = true;
                m_waveAnnounceText.text = "Mission Cleared!";
                m_waveAnnounceAnim.Animator.Play(m_waveAnnounceAnim.StateName);
            }
            if (string.Equals(prevText, m_currentMission.MissionProgressText))
                return;
            m_missionProgressText.text = m_currentMission.MissionProgressText;
            m_progressTarget = m_currentMission.MissionProgress * m_maxProgressFill;
        }

        //public void OnEvent(KillEvent killEvent)
        //{
        //    var isEnemy = killEvent.KilledActor.TryGetComponent<Enemy>(out _);
        //    if (!isEnemy)
        //        return;
        //    m_enemiesKilled++;
        //    UpdateProgress();
        //}
    }
}
