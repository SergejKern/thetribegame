﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.HUD
{
    public class FillingEnergyUIElement : MonoBehaviour, IEnergyUIElement
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Image m_animEnergy;
        [SerializeField] Image m_currentEnergy;

        [SerializeField] float m_fillingAnimSpeed;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public IEnergyUIElement PreviousElement { get; set; }
        public IEnergyUIElement NextElement { get; set; }

        RectTransform m_rectTransform;
        Color m_imgTint;

        float m_currentPercentage=0.1f;
        float m_targetPercentage=0.1f;

        bool m_transitionDone;

        bool IsDeclining => m_targetPercentage < m_currentPercentage;
        bool IsRising => m_targetPercentage > m_currentPercentage;
        bool NeedsUpdate => Math.Abs(m_targetPercentage - m_currentPercentage) > float.Epsilon;
        bool Done => !NeedsUpdate && m_transitionDone;

        bool CanUpdate => IsRising || IsDeclining;

        public void SetColor(Color imageTint)
        {
            m_imgTint = imageTint;
            UpdateColor();
        }

        public void Init(Color imageTint)
        {
            m_rectTransform = GetComponent<RectTransform>();
            SetColor(imageTint);
        }

        void UpdateColor()
        {
            if (m_animEnergy != null)
                m_animEnergy.color = m_imgTint;
        }

        public void SetTargetEnergy(float percentage)
        {
            if (Mathf.Abs(m_targetPercentage-percentage) < float.Epsilon)
                return;
            //Debug.Log($"{transform.parent.name}.{gameObject.name} Set to {percentage}");
            m_targetPercentage = percentage;
            m_transitionDone = Math.Abs(m_targetPercentage - m_currentPercentage) < float.Epsilon;

            //if (CanUpdate)
            //    UpdateEnergy();
        }

        void Update()
        {
            var updateEnergy = NeedsUpdate && CanUpdate;
            if (m_animEnergy != null)
                m_animEnergy.gameObject.SetActive(updateEnergy);
            if (updateEnergy)
                UpdateEnergy();
        }

        void UpdateEnergy()
        {       
            var w = m_rectTransform.sizeDelta.x;
            m_currentEnergy.rectTransform.offsetMax = new Vector2(-(w - m_targetPercentage * w), 0f);
            if (m_animEnergy == null)
            {
                m_currentPercentage = m_targetPercentage;
                return;
            }

            var dir = m_targetPercentage > m_currentPercentage ? 1f : -1f;

            var min = Mathf.Min(m_targetPercentage, m_currentPercentage);
            var max = Mathf.Max(m_targetPercentage, m_currentPercentage);
            var rectTransform = m_animEnergy.rectTransform;
            rectTransform.offsetMin = new Vector2(min * w, 0f);
            rectTransform.offsetMax = new Vector2(-(w - max * w), 0f);

            m_currentPercentage = Mathf.Clamp(m_currentPercentage + Time.deltaTime * dir * m_fillingAnimSpeed, min, max);
        }

        public void OnTransitionDone()
        {
            //Debug.Log($"{transform.parent.name}.{gameObject.name} OnTransitionDone {m_currentPercentage}");
            m_transitionDone = true;
            UpdateColor();
        }

        public void NoEnergyFeedback(){}
    }
}
