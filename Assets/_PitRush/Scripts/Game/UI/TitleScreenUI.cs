﻿using Game.Globals;
using UnityEngine;
using UnityEngine.Events;

namespace Game.UI
{
    public class TitleScreenUI : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onPlay;
        [SerializeField] UnityEvent m_onPVP;
        [SerializeField] UnityEvent m_onExit;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void OnPlay()
        {
            GameGlobals.PVP = false;
            m_onPlay.Invoke();
        }
        
        public void OnPVP()
        {
            GameGlobals.PVP = true;
            m_onPVP.Invoke();
        }

        public void OnExit() => m_onExit.Invoke();
    }
}
