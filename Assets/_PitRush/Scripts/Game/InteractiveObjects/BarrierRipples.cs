﻿using GameUtility.Components.Collision;
using GameUtility.Feature.CustomTags;
using UnityEngine;

// todo 3: use PrefabData? Make it actually spawn ripples
namespace Game.InteractiveObjects
{
    public class BarrierRipples : MonoBehaviour, ICollisionObjEnterHandling
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] CustomTagID m_affectID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public GameObject PrefabRipples;
        public GameObject PrefabHit;
        public GameObject PrefElectricity;
        public float DestroyHitObjectAfterSeconds = 2;
        public float DestroyRippleObjectAfterSeconds = 2;
        public float DestroyElectricityObjectAfterSeconds = 2;
        // shader vector:
        const string k_sphereCenter = "_SphereCenter";
        static readonly int k_center = Shader.PropertyToID(k_sphereCenter);

        void OnCollisionEnter(Collision co)
        {
            if (!co.gameObject.TryGetComponent<CustomTagsComponent>(out var tags))
                return;

            if (!tags.CustomTags.Contains(m_affectID))
                return;

            var pos = co.contacts[0].point;
            SpawnRipplePrefab(pos);
            SpawnHitPrefab(pos);
            SpawnElectricityPrefab(pos);
        }

        void SpawnHitPrefab(Vector3 pos)
        {
            if (!PrefabHit)
                return;

            var hit = Instantiate(PrefabHit, pos, Quaternion.identity);
            hit.transform.SetParent(transform);
            Destroy(hit, DestroyHitObjectAfterSeconds);
        }

        void SpawnRipplePrefab(Vector3 pos)
        {
            if (!PrefabRipples)
                return;

            var ripples = Instantiate(PrefabRipples, transform);
            var child = ripples.transform.GetChild(0);
            if (!child.TryGetComponent<ParticleSystemRenderer>(out var psr))
                return;
            var mat = psr.material;
            mat.SetVector(k_center, pos);

            Destroy(ripples, DestroyRippleObjectAfterSeconds);
        }

       void SpawnElectricityPrefab(Vector3 pos)
        {
            if (!PrefElectricity)
                return;

            var hit = Instantiate(PrefElectricity, pos, Quaternion.identity);
            Destroy(hit, DestroyElectricityObjectAfterSeconds);
        }

        public void OnCollisionObjectEnter(GameObject collidedWith, Collision data) => OnCollisionEnter(data);
    }
}
