using Game.Actors.Controls;
using Game.Configs.Combat;
using GameUtility.Data.TransformAttachment;
using GameUtility.Interface;

namespace Game.InteractiveObjects
{
    public enum GrabRestrictions
    {
        None,
        PrimaryOnly,
        SecondaryOnly,
        TwoHanded,
    }

    public interface IItemProperties
    {
        GrabRestrictions GrabRestrictions { get; }
        WeaponMovesType ItemAnimationType { get; }
    }

    /// <summary>
    /// Provided Grabbable could be something else spawned by the interactive thing
    /// </summary>
    public interface IGrabProvider : 
        IProvider<IGrabbable>, IInteractive, IItemProperties
    { }

    public interface IGrabbable : IInteractive, IItemProperties
    {
        void OnGrabbed(ControlCentre grabbingActor);
        void AdjustPositioning(TransformData grabbingHand);

        void Drop();
        void Throw();
    }
}
