﻿using Core.Interface;
using Core.Unity.Interface;
using Game.Actors.Energy;
using Game.Utility;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class TargetComponent : MonoBehaviour, ITarget, IPoolable
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] HealthComponent m_health;
        [SerializeField] bool m_static;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public Transform Transform { get; private set; }
        public GameObject GameObject { get; private set; }
        
        public bool IsStatic => m_static;
        public bool IsTargetingPossible => enabled && (m_health == null || m_health.HP > 0f);

        void OnEnable()
        {
            Transform = transform;
            GameObject = gameObject;
        }

        public void OnSpawn() => TargetSpawnedEvent.Trigger(this);

        public void OnDespawn() => TargetDespawnedEvent.Trigger(this);
    }
}
