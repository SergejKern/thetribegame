﻿using System.Collections;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class Barrier : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField

        [SerializeField] ParticleSystemRenderer m_renderer;
        [SerializeField] string[] m_colorProperties;
        [GradientUsage(true)]
        [SerializeField] Gradient m_gradient;
        [SerializeField] float m_raiseTime;
        [SerializeField] AnimationCurve m_localYScale;
        [SerializeField] RefIAudioAsset m_raiseBarrierSound;
        [SerializeField] RefIAudioAsset m_lowerBarrierSound;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_timer;
        Transform m_barrierTransform;

        public bool Raised { get; private set; }

        public void RaiseBarrier()
        {
            if (m_timer > 0f)
                return;
            var go = gameObject;
            go.SetActiveWithTransition();
            m_barrierTransform = m_renderer.transform;
            m_raiseBarrierSound?.Result?.Play(go);
            StartCoroutine(RaiseBarrierRoutine());
        }

        public void LowerBarrier()
        {
            m_lowerBarrierSound?.Result?.Play(gameObject);
            StartCoroutine(LowerBarrierRoutine());
        }

        IEnumerator RaiseBarrierRoutine()
        {
            m_timer = 0;
            while (m_timer < m_raiseTime)
            {
                UpdateBarrierVisual();
                m_timer += Time.deltaTime;
                yield return null;
            }

            UpdateBarrierVisual();
            Raised = true;
        }
        IEnumerator LowerBarrierRoutine()
        {
            m_timer = m_raiseTime;
            while (m_timer > 0)
            {
                UpdateBarrierVisual();
                m_timer -= Time.deltaTime;
                yield return null;
            }

            UpdateBarrierVisual();
            gameObject.SetActiveWithTransition(false);
            Raised = false;
        }

        void UpdateBarrierVisual()
        {
            var p = Mathf.Clamp01(m_timer / m_raiseTime);
            foreach (var cp in m_colorProperties)
                m_renderer.material.SetColor(cp, m_gradient.Evaluate(p));

            m_barrierTransform.localScale = new Vector3(1, m_localYScale.Evaluate(p), 1);
        }
    }
}
