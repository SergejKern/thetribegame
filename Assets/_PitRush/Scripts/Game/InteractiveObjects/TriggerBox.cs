﻿using Core.Interface;
using Core.Types.VariableWrapper;
using Core.Unity.Interface;
using Game.ActionConfig;
using Game.Globals;
using Game.Utility;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using UnityEngine;
using UnityEngine.Events;

namespace Game.InteractiveObjects
{
    public class TriggerBox : MonoBehaviour, ITarget, 
        IDamageHandler,
        ILinkWithBool, IPoolable
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_goOffInSeconds;
        [SerializeField] Renderer m_renderer;
        [SerializeField] MaterialOverrideMap[] m_override;
        [SerializeField] GameObject m_tutorialObject;

        [SerializeField] CollisionMaterialID m_materialID;
        [SerializeField] CollisionMaterialID m_absorbedMaterialID;

        [SerializeField] float m_collisionPosCenterPercent;
        [SerializeField] float m_minDamage;
        [SerializeField] Transform m_collisionPos;

        [SerializeField] UnityEvent m_onActivate;
        [SerializeField] UnityEvent m_onDeactivate;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public Transform Transform { get; private set; }
        public GameObject GameObject { get; private set; }

        public bool IsStatic => true;
        public bool IsTargetingPossible => enabled && (!IsOn || CanTurnOff);

        public Vector3 CollisionPos => m_collisionPos == null ? transform.position : m_collisionPos.position;
        public float CollisionPosCenterPercent => m_collisionPosCenterPercent;
        public CollisionMaterialID Material => m_materialID;

        float m_goOffTimer;

        public bool CanTurnOff;

        public bool IsOn;
        BoolVar m_activationVar;

        void OnEnable()
        {
            Transform = transform;
            GameObject = gameObject;
        }

        void Start()
        {
            if (m_tutorialObject != null)
                m_tutorialObject.SetActive(GameGlobals.IsTutorial);
        }

        // Update is called once per frame
        void Update() => UpdateActiveTrigger();

        void UpdateActiveTrigger()
        {
            if (!IsOn)
                return;
            if (m_goOffTimer <= 0)
                return;
            m_goOffTimer -= Time.deltaTime;
            if (m_goOffTimer > 0)
                return;

            Deactivate();
        }

        //void OnTriggerEnter(Collider other)
        //{
        //    var collisionRoot = RelativeCollider.GetRoot(other);
        //    OnTriggerObjectEnter(collisionRoot);
        //}
        //public void OnTriggerObjectEnter(GameObject collidedWith)
        //{
        //    if (!collidedWith.TryGetComponent<DamageComponent>(out var dmg))
        //        return;
        //    if (!enabled)
        //        return;
        //    if (dmg.Team!= null && dmg.Team.TeamID==(int)TeamFlag.Enemies)
        //        return;
        //    if (!IsOn)
        //        Activate();
        //    else if(CanTurnOff)
        //        Deactivate();
        //}

        void Activate()
        {
            if (IsOn)
                return;
            IsOn = true;
            m_onActivate.Invoke();

            MaterialExchange.Override(m_renderer, m_override);
            if (m_goOffInSeconds > 0f)
                m_goOffTimer = m_goOffInSeconds;
            UpdateActivationVar();
        }

        void Deactivate()
        {
            if (!IsOn)
                return;
            IsOn = false;
            m_onDeactivate.Invoke();

            MaterialExchange.Revert(m_renderer, m_override);

            UpdateActivationVar();
        }

        void UpdateActivationVar() => m_activationVar.SetValue(IsOn);
        public void Link(BoolVar triggerBool)
        {
            m_activationVar = triggerBool;
            UpdateActivationVar();
        }

        public void OnSpawn()
        {
            TargetSpawnedEvent.Trigger(this);
            Reset();
        }

        public void OnDespawn()
        {
            TargetDespawnedEvent.Trigger(this);
            Reset();
        }

        void Reset()
        {
            m_activationVar = default;
            m_goOffTimer = 0f;
            IsOn = false;
            m_tutorialObject.SetActive(false);
            MaterialExchange.Revert(m_renderer, m_override);
        }

        public void HandleDamage(float damage, ref HandleDamageResult result)
        {
            if (!enabled)
                return;

            if (damage < m_minDamage)
            {
                result.DamageDone = 0;
                result.DamageAbsorbed = damage;
                result.Result = DamageResult.Absorbed;
                result.Material = m_absorbedMaterialID;
                return;
            }

            result.DamageDone = damage;
            result.Result = DamageResult.Damaged;
            result.Material = Material;

            if (damage < m_minDamage)
                return;

            if (!IsOn)
                Activate();
            else if(CanTurnOff)
                Deactivate();
        }
    }
}
