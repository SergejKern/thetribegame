﻿using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using GameUtility.Components.Collision;
using GameUtility.Components.Transform;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class HealingOrb : MonoBehaviour, ITriggerObjEnterHandling
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIAudioAsset m_collectSound;
        [SerializeField] PrefabData m_effect;
        //[SerializeField] EnergyType m_type;
        [SerializeField] float m_healAmount;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void OnTriggerObjectEnter(GameObject root)
        {
            if (!root.TryGetComponent(out Player p))
                return;

            p.Health.Heal(m_healAmount);
            m_collectSound?.Result?.Play(gameObject);
            var effectPrefab = m_effect.Prefab;
            if (effectPrefab != null)
            {
                var effectGo = effectPrefab.GetPooledInstance(transform.position);
                if (!effectGo.TryGetComponent(out CopyPositionBehaviour cpyPos))
                    cpyPos = effectGo.AddComponent<CopyPositionBehaviour>();
                cpyPos.Follow = p.transform;
            }
            gameObject.TryDespawnOrDestroy();
        }
    }
}
