﻿using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Combat.Weapon;
using Game.Configs.Combat;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class WeaponsStand : MonoBehaviour, IGrabProvider
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PrefabData m_weapon;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public bool InteractEnabled => gameObject.activeInHierarchy && enabled;
        // ReSharper disable once MethodNameNotMeaningful
        public void Get(out IGrabbable provided) => provided = m_weapon.Prefab.GetPooledInstance().GetComponent<Weapon>();
        public GrabRestrictions GrabRestrictions => m_weapon.Prefab.GetComponent<IGrabbable>().GrabRestrictions;
        public WeaponMovesType ItemAnimationType => m_weapon.Prefab.GetComponent<IGrabbable>().ItemAnimationType;
    }
}
