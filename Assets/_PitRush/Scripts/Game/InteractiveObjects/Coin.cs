﻿using System;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using GameUtility.Components.Collision;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class Coin : MonoBehaviour, ITriggerObjEnterHandling
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIAudioAsset m_collectSound;
        [SerializeField] PrefabData m_effect;
        [SerializeField] Rigidbody m_body;
        [SerializeField] float m_dynamicDrag = 15f;
        [SerializeField] float m_dynamicRange = 1f;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public static int CoinsCollected;

        void Update() => UpdateDynamicDrag();

        void UpdateDynamicDrag() => m_body.drag = m_dynamicDrag * (m_dynamicRange - Mathf.Clamp(Mathf.Abs(transform.position.y), 0, m_dynamicRange));

        public void OnTriggerObjectEnter(GameObject root)
        {
            if (!root.TryGetComponent<Player>(out _))
                return;

            m_collectSound?.Result?.Play(gameObject);
            var effectPrefab = m_effect.Prefab;
            if (effectPrefab != null)
                effectPrefab.GetPooledInstance(transform.position);

            CoinsCollected++;
            gameObject.TryDespawnOrDestroy();
        }
    }
}
