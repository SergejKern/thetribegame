﻿using Core.Interface;
using Core.Unity.Extensions;
using Game.Utility.CollisionAndDamage;
using GameUtility.Interface;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class Bullet : MonoBehaviour, IWeaponSpawn, IPoolable
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] DamageComponent m_damage;

        [SerializeField] Rigidbody m_body;
        [SerializeField] float m_force;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void Initialize(IWeapon weapon) => Initialize(weapon.Actor ? weapon.Actor.transform: null);

        public void Initialize(Transform sourceActor)
        {
            if (sourceActor != null)
                m_damage.SetActor(sourceActor.transform);

            if (m_body == null)
                return;

            var rot = sourceActor.transform.rotation;
            m_body.MoveRotation(Quaternion.Euler(0, rot.eulerAngles.y, 0));
            m_body.AddForce(m_force * (m_body.rotation * Vector3.forward), ForceMode.Impulse);
        }

        void ResetVelocity()
        {
            if (m_body != null)
                m_body.velocity = Vector3.zero;
        }

        public void OnSpawn() => ResetVelocity();
        public void OnDespawn() => ResetVelocity();

        public void Despawn() => gameObject.TryDespawnOrDestroy();
    }
}
