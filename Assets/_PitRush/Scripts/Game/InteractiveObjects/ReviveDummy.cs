﻿using Core.Interface;
using Core.Types.VariableWrapper;
using Core.Unity.Types;
using Game.ActionConfig;
using Game.Actors.Energy;
using Game.Components.Behaviour;
using Game.Components.Events;
using GameUtility.Energy;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class ReviveDummy : MonoBehaviour, IRevivable, IPlatformMoveListener, ILinkWithBool, IPoolable
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] HealthComponent m_health;
        [SerializeField] PrefabRef m_reviveTrigger;
        [SerializeField] float m_timeToRevive;
        [SerializeField] GameObject m_deadModel;
        [SerializeField] GameObject m_aliveModel;
        //[SerializeField] ConfigurableJoint m_joint;
#pragma warning restore 0649 // wrong warnings for SerializeField

        RevivableTriggerBehaviour m_revivableTrigger;

        BoolVar m_reviveBool;

        public float TimeToRevive => m_timeToRevive;

        public void Get(out IHealthComponent provided) 
            => provided = m_health;

        [UsedImplicitly]
        public void Deplete() 
            => m_health.Deplete();

        public void OnDeath()
        {            
            var trigger = m_reviveTrigger.GetInstance();
            m_revivableTrigger = trigger.GetComponent<RevivableTriggerBehaviour>();
            m_revivableTrigger.Init(this);

            SetAlive(false);
        }

        public void OnRevive()
        {
            m_health.Reset();
            m_reviveBool.SetValue(true);
            SetAlive(true);
        }

        void SetAlive(bool alive)
        {
            if (alive) 
                ResetBodies();
            m_deadModel.SetActive(!alive);
            m_aliveModel.SetActive(alive);
        }

        void ResetBodies()
        {
            var rs = m_aliveModel.GetComponentsInChildren<Rigidbody>(true);
            foreach (var r in rs)
            {
                r.rotation = Quaternion.identity;
                r.velocity = Vector3.zero;
                r.angularVelocity = Vector3.zero;
                
                r.transform.localPosition = Vector3.zero;
            }
        }

        public void OnSpawn() => ResetBodies();

        public void OnDespawn() => ResetBodies();

        public void OnPlatformMoveChangeEvent(Transform platformTr, bool isMoving, bool upDown) => m_aliveModel.SetActive(!isMoving && m_health.HP > 0);

        public void Link(BoolVar var)
        {
            m_reviveBool = var;
            m_reviveBool.SetValue(false);
        }
    }
}
