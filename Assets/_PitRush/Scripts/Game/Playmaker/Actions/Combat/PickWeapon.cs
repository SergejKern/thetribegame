//using HutongGames.PlayMaker;

//namespace Game.Playmaker.Actions.Weapon
//{
//	[ActionCategory("Game/Weapon")]
//	[Tooltip("Resets the combo")]
//	public class PickWeapon : FsmStateAction
//    {
//        [RequiredField]
//        [CheckForComponent(typeof(InteractiveObjects.Weapon))]
//        [Tooltip("The target. A Weapon component is required")]
//        public FsmOwnerDefault wpnGameObject;

//        // Code that runs on entering the state.
//        public override void OnEnter()
//		{
//            var go = Fsm.GetOwnerDefaultTarget(wpnGameObject);
//            if (go == null)
//                return;
//            var weapon = go.GetComponent<InteractiveObjects.Weapon>();
//            if (weapon == null)
//                return;

//            go.GetComponent<InteractiveObjects.Weapon>().ResetCombo();

//            Finish();
//		}
//	}
//}