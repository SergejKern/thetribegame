using Game.Actors.Controls;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.Combat
{
	[ActionCategory("Game/Combat")]
	[Tooltip("Resets the combo")]
	public class AttackResetCombo : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(AttackControl))]
        [Tooltip("The target. An AttackControl component is required")]
        public FsmOwnerDefault Control;

        // Code that runs on entering the state.
        public override void OnEnter()
		{
            var go = Fsm.GetOwnerDefaultTarget(Control);
            if (go == null)
                return;
            if (!go.TryGetComponent<AttackControl>(out var attackControl))
                return;

            attackControl.OnWeaponReady();
            Finish();
		}
	}
}