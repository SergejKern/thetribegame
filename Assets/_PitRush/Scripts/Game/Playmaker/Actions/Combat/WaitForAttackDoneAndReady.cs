﻿using Game.Actors.Controls;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.Combat
{
    public class WaitForAttackDoneAndReady : FsmStateAction
    {
        [CheckForComponent(typeof(AttackControl))]
        public FsmOwnerDefault Control;

        AttackControl m_attackControl;
        public override void OnEnter()
        {
            base.OnEnter();
            var go = Fsm.GetOwnerDefaultTarget(Control);
            if (go == null)
                return;
            go.TryGetComponent(out m_attackControl);
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            if(m_attackControl== null)
                return;

            //UnityEngine.Debug.Log($"{m_attackControl.ControlCentre.ControlledObject.name} attack ready {m_attackControl.AttackReady} attack done {m_attackControl.AttackDone}");
        
            if (m_attackControl.AttackDone && m_attackControl.AttackReady)
                Finish();
        }
    }
}
