﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine;

namespace Game.Playmaker.Actions
{
    [ActionCategory(ActionCategory.GameObject)]
    public class SetLayerOnActor : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault controlCentre;
        [UIHint(UIHint.Layer)]
        public int layer;

        //public int RecursiveDepth;

        public override void Reset()
        {
            controlCentre = null;
            layer = 0;
        }

        public override void OnEnter()
        {
            DoSetLayer();

            Finish();
        }

        void DoSetLayer()
        {
            GameObject go = Fsm.GetOwnerDefaultTarget(controlCentre);
            if (go == null)
                return;

            go.TryGetComponent(out ControlCentre cc);
            cc.ControlledObject.layer = layer;
            _SetLayer(cc.Model, 1);
        }

        void _SetLayer(GameObject go, int depth)
        {
            go.layer = layer;
            if (depth <= 0)
                return;
            var goTransform = go.transform;
            for (var i = 0; i < goTransform.childCount; i++)
            {
                var cTr = goTransform.GetChild(i);
                _SetLayer(cTr.gameObject, depth - 1);
            }
        }
    }
}