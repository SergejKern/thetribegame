using Core.Unity.Interface;
using Core.Unity.Types;
using GameUtility.Validation;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.FMOD
{
	[ActionCategory("FMOD")]
	[Tooltip("Play Sound (StudioEventEmitter)")]
	public class FMODPlayEmitter : FsmStateAction, IAudioValidationFSM
    {
        [RequiredField]
        [CheckForComponent(typeof(IAudioComponent))]
        public FsmGameObject Emitter;

        public bool WaitForSoundEnd;

        IAudioComponent m_emitter;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            if (!Emitter.Value.TryGetComponent(out m_emitter))
                return;
            m_emitter.Stop();
            m_emitter.Play();
            if (!WaitForSoundEnd)
                Finish();
		}

        public override void OnUpdate()
        {
            base.OnUpdate();
            if (WaitForSoundEnd && !m_emitter.IsPlaying())
                Finish();
        }

        public bool IsAudioValid(PlayMakerFSM fsm, string state, out string error)
        {
            error = "";
            if (Emitter != null)
                return true;

            error = $"{nameof(FMODPlayEmitter)}: {fsm.gameObject} Not assigned Emitter in Playmaker-state {state}";
            return false;
        }
    }

}
