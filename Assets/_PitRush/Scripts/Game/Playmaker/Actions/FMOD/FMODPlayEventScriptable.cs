using Core.Unity.Interface;
using Core.Unity.Types;
using GameUtility.Validation;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.FMOD
{
	[ActionCategory("FMOD")]
	[Tooltip("Play Sound (StudioEventEmitter)")]
	public class FMODPlayEventScriptable : FsmStateAction, IAudioValidationFSM
    {
        [RequiredField]
        public FsmGameObject Emitter;

        [RequiredField]
        //[ObjectType(typeof(IAudioAsset))]
        public FsmObject StudioEventObj;
        IAudioAsset StudioEvent => StudioEventObj.Value as IAudioAsset;

        public bool WaitForSoundEnd;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var studioEvent = StudioEvent;
            if (studioEvent == null)
                return;
            if (Emitter.Value == null)
                return;

            studioEvent.Play(Emitter.Value.transform);

            if (!WaitForSoundEnd)
                Finish();
		}

        public override void OnUpdate()
        {
            base.OnUpdate();

            var studioEvent = StudioEvent;
            if (studioEvent == null)
                return;

            if (WaitForSoundEnd && !studioEvent.IsPlaying())
                Finish();
        }

        public bool IsAudioValid(PlayMakerFSM fsm, string state, out string error)
        {
            error = "";
            if (StudioEventObj != null)
                return true;

            error = $"{nameof(FMODPlayEventScriptable)}: {fsm.gameObject} Not assigned StudioEventObj in Playmaker-state {state}";
            return false;
        }
    }

}
