using Core.Unity.Interface;
using Core.Unity.Types;
using GameUtility.Validation;
using HutongGames.PlayMaker;

//using PARAMETER_ID = FMOD.Studio.PARAMETER_ID;

namespace Game.Playmaker.Actions.FMOD
{
    [ActionCategory("FMOD")]
    public class FMODSetParameterScriptable : FsmStateAction, IAudioValidationFSM
    {
        [RequiredField]
        [ObjectType(typeof(IAudioAsset))]
        public FsmObject StudioEventObj;

        IAudioAsset StudioEvent => StudioEventObj.Value as IAudioAsset;

        public FsmString Parameter;
        public FsmFloat Value;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var studioEvent = StudioEvent;
            if (studioEvent == null)
                return;

            StudioEvent.SetParameter(Parameter.Value, Value.Value);
            Finish();
        }

        public bool IsAudioValid(PlayMakerFSM fsm, string state, out string error)
        {
            error = "";
            if (StudioEvent == null)
            {
                error = $"{nameof(FMODSetParameterScriptable)}: {fsm.gameObject} -> StudioEvent not assigned sound in Playmaker-state {state}";
                return false;
            }

            var hasParam = StudioEvent.HasParameter(Parameter.Value);
            if (!hasParam)
                error = $"Problem with Parameter {Parameter} in {StudioEvent}";
            return hasParam;
        }
    }

}
