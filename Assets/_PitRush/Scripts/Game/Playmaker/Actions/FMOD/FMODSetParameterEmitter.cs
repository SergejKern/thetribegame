using Core.Unity.Interface;
using GameUtility.Validation;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.FMOD
{
    [ActionCategory("FMOD")]
    public class FMODSetParameterEmitter : FsmStateAction, IAudioValidationFSM
    {
        [RequiredField]
        [CheckForComponent(typeof(IAudioComponent))]
        public FsmGameObject Sound;

        public FsmString Parameter;
        public FsmFloat Value;

        IAudioComponent m_emitter;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            if (!Sound.Value.TryGetComponent(out m_emitter))
                return;
            m_emitter?.SetParameter(Parameter.Value, Value.Value);

            Finish();
        }

        public bool IsAudioValid(PlayMakerFSM fsm, string state, out string error)
        {
            error = "";
            if (Sound == null)
            {
                error = $"{nameof(FMODSetParameterEmitter)}: {fsm.gameObject} -> Sound not assigned sound in Playmaker-state {state}";
                return false;
            }

            if (!Sound.Value.TryGetComponent(out m_emitter))
            {
                error = $"{nameof(FMODSetParameterEmitter)}: {fsm.gameObject} -> Sound without StudioEventEmitter in Playmaker-state {state}";
                return false;
            }

            var hasParam = m_emitter.HasParameter(Parameter.Value);

            if (hasParam)
                error = $"Problem with Parameter {Parameter} in {Sound}";
            return hasParam;
        }
    }

}
