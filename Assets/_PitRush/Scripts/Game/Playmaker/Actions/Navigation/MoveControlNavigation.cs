﻿using Game.Actors;
using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class MoveControlNavigation : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(MoveControl))]
        [CheckForComponent(typeof(NavMeshAgent))]
        [HutongGames.PlayMaker.Tooltip("The target. An MoveControl component is required")]
        public FsmOwnerDefault MoveGameObject;

        public FsmFloat MoveFactor;
        public bool AllowFinish;
        //[UIHint(UIHint.Variable)]
        //public FsmVector2 MoveVector;

        NavMeshAgent m_navMeshAgent;
        MoveControl m_moveControl;

        Vector2 m_smoothDeltaInput = Vector2.zero;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            m_navMeshAgent = go.GetComponent<NavMeshAgent>();
            if (go.TryGetComponent(out m_moveControl))
            {
                m_moveControl.SetMoveInput(Vector2.zero);
                m_moveControl.SetDirectionInput(Vector2.zero);
            }

            m_smoothDeltaInput = Vector2.zero;
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;
            if (m_navMeshAgent == null)
                return;
            if (!m_navMeshAgent.isOnNavMesh)
                return;
            //Debug.DrawLine(go.transform.position, m_navMeshAgent.nextPosition, Color.green);
            //Debug.DrawLine(m_navMeshAgent.destination + Vector3.right, m_navMeshAgent.destination - Vector3.right, Color.green);
            //Debug.DrawLine(m_navMeshAgent.destination + Vector3.forward, m_navMeshAgent.destination - Vector3.forward, Color.green);

            DoMoveControlNavigation(go.transform, m_moveControl, m_navMeshAgent, MoveFactor.Value,
                ref m_smoothDeltaInput, out var targetReached);
            if (AllowFinish && targetReached)
                Finish();
        }

        // ReSharper disable once FlagArgument
        public static void DoMoveControlNavigation(Transform transform, MoveControl moveControl, NavMeshAgent agent, float moveFactor, 
            ref Vector2 smoothDelta, out bool targetReached)
        {
            var meters = moveControl.MetersPerSecond * Time.deltaTime;

            var moveVec = agent.steeringTarget - transform.position;
            var factor = Mathf.Clamp01(moveVec.magnitude / meters * moveFactor);

            var x = moveVec.x;
            var y = moveVec.z;
            var moveInput = new Vector2(x, y).normalized * factor;
            //Debug.DrawLine(go.transform.position, go.transform.position + new Vector3(moveInput.x, 0f, moveInput.y), Color.red);

            // Low-pass filter the deltaMove
            var smooth = Mathf.Min(1.0f, Time.deltaTime / 0.3f);
            smoothDelta = Vector2.Lerp(smoothDelta, moveInput, smooth);

            //m_navMeshAgent.velocity = m_velocity;
            //if (magnitude < 0.5f)
            //{
            //    //MoveVector.Value = Vector2.zero;
            var remainingDistance = (agent.destination - agent.nextPosition).magnitude;
            targetReached = remainingDistance < (0.25f * agent.radius);
            if (targetReached)
            {
                moveControl.SetMoveInput(Vector2.zero);
                agent.nextPosition = transform.position;
                moveControl.DoMove(moveFactor);
            }
            else
            {
                //MoveVector.Value = velocity;
                moveControl.SetMoveInput(smoothDelta);
                moveControl.DoMove(moveFactor);
                if (moveVec.magnitude > agent.radius)
                    agent.nextPosition = transform.position + 0.9f * moveVec;
            }
        }

        public override void OnExit()
        {
            base.OnExit();
            m_moveControl.SetMoveInput(Vector2.zero);
        }
    }

}
