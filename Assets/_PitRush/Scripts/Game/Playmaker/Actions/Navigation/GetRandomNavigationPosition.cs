﻿using Game.Actors;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class GetRandomNavigationPosition : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(NavMeshAgent))]
        [HutongGames.PlayMaker.Tooltip("The target. An MoveControl component is required")]
        public FsmOwnerDefault MoveGameObject;

        //[UIHint(UIHint.Variable)]
        //public FsmVector3 Position;
        public float Distance;

        NavMeshAgent m_navMeshAgent;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            if (!go.TryGetComponent(out m_navMeshAgent))
                return;

            var dest = RandomNavSphere(go.transform.position, Distance, -1);
            m_navMeshAgent.SetDestination(dest);

            Finish();
        }

        static Vector3 RandomNavSphere(Vector3 originPos, float dist, int layerMask)
        {
            var randDir = Random.insideUnitCircle * dist;
            var v3NavPos = originPos + new Vector3(randDir.x, 0, randDir.y);
            NavMesh.SamplePosition(v3NavPos, out var navHit, dist, layerMask);

            return !navHit.hit ? originPos : navHit.position;
        }
    }

}
