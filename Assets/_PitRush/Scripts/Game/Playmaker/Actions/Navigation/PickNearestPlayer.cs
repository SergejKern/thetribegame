﻿using Core.Unity.Interface;
using Game.Actors;
using Game.Combat;
using Game.Combat.Timeline;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.Object;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class PickNearestPlayer : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault AgentGameObject;

        public int TargetPriority;

        public bool UntilPlayerFound;
        public FsmEvent PlayerFoundEvent;

        Transform m_transform;
        ITimelineAgent m_agent;

        NavMeshAgent m_navMeshAgent;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(AgentGameObject);
            if (go == null)
                return;

            go.TryGetComponent(out m_agent);
            m_transform = go.transform;

            m_agent.Get(out m_navMeshAgent);

            DoPickNearestPlayer();
        }

        public override void OnUpdate()
        {
            DoPickNearestPlayer();
        }

        void DoPickNearestPlayer()
        {
            if (m_agent == null)
                return;

            var players = FindObjectsOfType<Player>();

            var minDistSqr = float.MaxValue;
            ITarget target = null;

            foreach (var pl in players)
            {
                var checkTarget = pl.GetComponent<ITarget>();
                if (!checkTarget.IsTargetingPossible)
                    continue;
                
                var nextDistSqr = Vector3.SqrMagnitude(pl.transform.position - m_transform.position);
                if (!(nextDistSqr < minDistSqr))
                    continue;

                minDistSqr = nextDistSqr;
                target = checkTarget;
            }

            var found = target != null;
            if (!found)
            {
                m_agent.ClearTargetPriority(TargetPriority);
                if (!UntilPlayerFound) 
                    Finish();
                return;
            }

            if (!m_navMeshAgent.isOnNavMesh)
                return;

            if (!m_navMeshAgent.CalculatePath(target.Transform.position, m_navMeshAgent.path)||
                m_navMeshAgent.path.status != NavMeshPathStatus.PathComplete)
                return;

            m_agent.SetTargetWithPriority(target, TargetPriority);

            if (PlayerFoundEvent != null)
                Fsm.Event(PlayerFoundEvent);

            Finish();
        }
    }

}
