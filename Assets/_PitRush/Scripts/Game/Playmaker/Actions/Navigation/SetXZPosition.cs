﻿using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class SetXZPosition : FsmStateAction
    {
        [RequiredField]
        [HutongGames.PlayMaker.Tooltip("The target. An MoveControl component is required")]
        public FsmOwnerDefault MoveGameObject;

        public FsmGameObject MoveTarget;


        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;
            var mPos = go.transform.position;
            var tPos = MoveTarget.Value.transform.position;
            go.transform.position = new Vector3(tPos.x, mPos.y, tPos.z);
        }
    }

}
