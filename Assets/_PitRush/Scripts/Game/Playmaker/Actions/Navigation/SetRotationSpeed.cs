﻿using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class SetRotationSpeed : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(MoveControl))]
        [Tooltip("A MoveControl component is required")]
        public FsmOwnerDefault MoveGameObject;

        [Tooltip("Set SpeedFactor to zero to provide specific value")]
        public float SpeedValue;
        [Tooltip("Set SpeedValue to zero to provide multiplication factor")]
        public float SpeedFactor;
        public bool ResetOnExit = true;

        MoveControl m_moveControl;

        float m_previousRotationLerpFactor;
        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            if (!go.TryGetComponent(out m_moveControl))
                return;

            m_moveControl.GetData(out var moveConfigData);
            m_previousRotationLerpFactor = moveConfigData.RotationSpeedSLerpFactor;
            m_moveControl.SetRotationLerpValue(SpeedValue + m_previousRotationLerpFactor * SpeedFactor);
        }

        public override void OnExit()
        {
            base.OnExit();
            if (!ResetOnExit || m_moveControl == null)
                return;

            m_moveControl.SetRotationLerpValue(m_previousRotationLerpFactor);
        }
    }
}