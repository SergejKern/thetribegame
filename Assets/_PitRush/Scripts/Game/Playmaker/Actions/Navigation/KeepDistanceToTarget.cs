﻿using Game.Actors;
using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class KeepDistanceToTarget : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(NavMeshAgent))]
        [HutongGames.PlayMaker.Tooltip("The target. An NavMeshAgent component is required")]
        public FsmOwnerDefault MoveGameObject;

        [UIHint(UIHint.Variable)]
        public FsmGameObject Target;

        public FsmFloat Distance;
        public FsmBool TargetReached;

        public bool FinishWhenClose;

        MoveControl m_moveControl;
        NavMeshAgent m_navMeshAgent;
        Transform m_transform;
        Vector2 m_smoothDeltaInput = Vector2.zero;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            if (!go.TryGetComponent(out m_moveControl))
                return;

            m_moveControl.SetMoveInput(Vector2.zero);
            m_moveControl.SetDirectionInput(Vector2.zero);
            var target = Target.Value;

            if (target == null)
            {
                Finish();
                return;
            }

            m_navMeshAgent = go.GetComponent<NavMeshAgent>();
            m_transform = go.transform;
            LookDirection(target);
            ResetNavAgent();
        }

        public override void OnUpdate()
        {
            if (!CanUpdate(out var target))
                return;
            var dist = Distance.Value;

            LookDirection(target);

            var myPos = m_transform.position;
            var targetPos = target.transform.position;

            var dir = (targetPos - myPos);
            dir.y = 0f;
            var dirNorm = dir.normalized;
            m_navMeshAgent.nextPosition = myPos;
            m_navMeshAgent.SetDestination(targetPos - dirNorm * dist);
            MoveControlNavigation.DoMoveControlNavigation(m_transform, m_moveControl, m_navMeshAgent, 1f, 
                ref m_smoothDeltaInput, out var targetReached);
            if (TargetReached != null)
                TargetReached.Value = targetReached;
            if (FinishWhenClose && targetReached)
                Finish();
        }

        bool CanUpdate(out GameObject target)
        {
            var error = false;
            error |= (m_navMeshAgent == null || !m_navMeshAgent.isOnNavMesh);
            error |= m_moveControl == null;

            target = Target.Value;
            error |= target == null;

            if (!error)
                return true;
            //Debug.Log($"Error {nameof(KeepDistanceToTarget)}");

            Finish();
            return false;
        }

        public override void OnExit()
        {
            base.OnExit();

            if (m_moveControl != null)
                m_moveControl.SetMoveInput(Vector2.zero);
            ResetNavAgent();
        }

        void ResetNavAgent()
        {
            if (m_navMeshAgent == null)
                return;
            if (!m_navMeshAgent.isOnNavMesh)
                return;

            var position = m_transform.position;
            m_navMeshAgent.SetDestination(position);
            m_navMeshAgent.nextPosition = position;
            m_navMeshAgent.velocity = Vector3.zero;
        }

        void LookDirection(GameObject target)
        {
            var lookVec = target.transform.position - m_moveControl.transform.position;
            m_moveControl.SetDirectionInput(new Vector2(lookVec.x, lookVec.z).normalized);
        }
    }

}
