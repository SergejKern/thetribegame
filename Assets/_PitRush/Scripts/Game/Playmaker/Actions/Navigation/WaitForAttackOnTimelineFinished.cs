﻿using Game.Combat;
using Game.Combat.Timeline;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class WaitForAttackOnTimelineFinished : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(ITimelineAgent))]
        public FsmOwnerDefault TimelineAgent;

        public FsmEvent OnFinish;

        ITimelineAgent m_timelineAgent;

        // NavMeshPath m_path;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(TimelineAgent);
            if (go == null)
                return;

            m_timelineAgent = go.GetComponent<ITimelineAgent>();
        }

        public override void OnUpdate() => UpdateAttackOnTimeline();

        void UpdateAttackOnTimeline()
        {
            if (m_timelineAgent.Data.State != TimelineState.Done) return;
            Finish();
            Fsm.Event(OnFinish);

        }
    }

}
