﻿using Game.Actors.Controls;
using Game.Combat.Timeline;
using GameUtility.Data.PhysicalConfrontation;
using HutongGames.PlayMaker;
using CombatTimeline = Game.Combat.Timeline.CombatTimeline;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class InitAttackOnTimeline : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(ITimelineAgent))]
        public FsmOwnerDefault TimelineAgent;

        public FsmGameObject Target;
        public AttackTypeID MoveType;
        public float MaxWaitTime;
        public FsmEvent OnAbort;
        public FsmEvent OnDone;
        public FsmFloat AttackRange;

        ITimelineAgent m_timelineAgent;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            if (!TryGetComponents(out m_timelineAgent, out var timeline))
            {
                Finish();
                return;
            }

            var time = timeline.GetSecondsUntilNextTimeForAttackHit(m_timelineAgent);
            m_timelineAgent.Get(out AttackControl attackControl);
            if (time > MaxWaitTime || !attackControl.CanInvoke)
            {
                Fsm.Event(OnAbort);
                Finish();
                return;
            }

            m_timelineAgent.InitAgentForTimeline(MoveType, 0, timeline);
            if (m_timelineAgent.Data.State == TimelineState.WaitForStart)
                Fsm.Event(OnDone);

            AttackRange.Value = m_timelineAgent.Data.AttackData.FullRangeUntilOnHit;

            Finish();
        }

        bool TryGetComponents(out ITimelineAgent agent, out CombatTimeline targetTimeline)
        {
            agent = null;
            targetTimeline = null;

            var go = Fsm.GetOwnerDefaultTarget(TimelineAgent);
            if (go == null)
                return false;

            agent = go.GetComponent<ITimelineAgent>();
            return Target.Value != null && Target.Value.TryGetComponent(out targetTimeline);
        }
    }
}
