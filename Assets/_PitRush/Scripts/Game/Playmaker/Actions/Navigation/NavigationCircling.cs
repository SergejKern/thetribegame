﻿using Core.Types;
using Game.Actors.Controls;
using GameUtility.Config;
using GameUtility.Data.Debugging;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class NavigationCircling : FsmStateAction
    {
        public enum EDirection
        {
            Left,
            Right,
            Random
        }

        [RequiredField]
        [CheckForComponent(typeof(NavMeshAgent))]
        [CheckForComponent(typeof(MoveControl))]
        [HutongGames.PlayMaker.Tooltip("The target. An NavMeshAgent component is required")]
        public FsmOwnerDefault MoveGameObject;

        [UIHint(UIHint.Variable)]
        public FsmGameObject Target;

        public FsmFloat Radius;

        public EDirection Direction = EDirection.Random;
        public float MoveFactor =1f;

        public DebugDrawID DebugDrawID;

        NavMeshAgent m_navMeshAgent;
        MoveControl m_moveControl;

        Vector3 m_direction;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            m_navMeshAgent = go.GetComponent<NavMeshAgent>();
            m_moveControl = go.GetComponent<MoveControl>();

            InitDirection();
        }

        void InitDirection()
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (Direction)
            {
                case EDirection.Left:
                    m_direction = Vector3.left;
                    break;
                case EDirection.Right:
                    m_direction = Vector3.right;
                    break;
                case EDirection.Random:
                    m_direction = Random.value > 0.5f ? Vector3.left : Vector3.right;
                    break;
            }
        }

        public override void OnUpdate()
        {
            if (m_navMeshAgent == null)
                return;
            if (Target.Value == null)
                return;
            if (!m_navMeshAgent.isOnNavMesh)
                return;

            var newPosOnCircle = GetNewPosOnCircle();
            using (var navMeshPathScoped = SimplePool<NavMeshPath>.I.GetScoped())
            {
                var path = navMeshPathScoped.Obj;
                m_navMeshAgent.CalculatePath(newPosOnCircle, path);

                if (path.status == NavMeshPathStatus.PathComplete)
                    m_navMeshAgent.SetDestination(newPosOnCircle);
                else
                {
                    Direction = (EDirection) (((int) Direction + 1) % 2);
                    InitDirection();
                    m_navMeshAgent.SetDestination(GetNewPosOnCircle());
                }
            }
        }

        Vector3 GetNewPosOnCircle()
        {
            var circleCentre = Target.Value.transform.position;

            var transform = m_navMeshAgent.transform;
            var dir = transform.rotation * m_direction;
            var moveVec = MoveFactor * m_moveControl.MetersPerSecond * Time.deltaTime * dir;
            var directionPosStraightLine = transform.position + moveVec;
            var vectorFromCircleCentre = (directionPosStraightLine - circleCentre).normalized * Radius.Value;
            var newPosOnCircle = circleCentre + vectorFromCircleCentre;

            DebugDrawConfig.I.DrawLine(circleCentre, newPosOnCircle, DebugDrawID);
            return newPosOnCircle;
        }
    }

}
