﻿using Game.Combat;
using Game.Combat.Timeline;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class SetNavigationPosition : FsmStateAction
    {
        [RequiredField]
        //[CheckForComponent(typeof(NavMeshAgent))]
        //[HutongGames.PlayMaker.Tooltip("The target. An NavMeshAgent component is required")]
        public new FsmOwnerDefault Owner;

        [UIHint(UIHint.Variable)]
        public FsmGameObject Target;

        public int TargetPriority;

        ITimelineAgent m_agent;
        NavMeshAgent m_navMeshAgent;
        Transform m_transform;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(Owner);
            if (go == null)
                return;
            m_transform = go.transform;
            m_agent = go.GetComponent<ITimelineAgent>();
            m_agent.Get(out m_navMeshAgent);
            ResetNavAgent();
        }

        public override void OnUpdate() => SetNavDestination();

        void SetNavDestination()
        {
            if (m_navMeshAgent == null)
                return;

            if (Target.Value == null)
                return;

            if (!m_navMeshAgent.isOnNavMesh)
                return;

            m_navMeshAgent.SetDestination(Target.Value.transform.position);
            if (m_navMeshAgent.pathStatus != NavMeshPathStatus.PathComplete)
                m_agent.ClearTargetPriority(TargetPriority);
        }

        public override void OnExit()
        {
            base.OnExit();
            ResetNavAgent();
        }

        void ResetNavAgent()
        {
            if (m_navMeshAgent == null)
                return;

            if (!m_navMeshAgent.isOnNavMesh)
                return;
            var position = m_transform.position;
            m_navMeshAgent.SetDestination(position);
            m_navMeshAgent.nextPosition = position;
            m_navMeshAgent.velocity = Vector3.zero;
        }
    }

}
