﻿using Game.Actors;
using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Playmaker.Actions.Navigation
{
    [ActionCategory(ActionCategory.Movement)]
    public class SetNavigationBackwards : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(NavMeshAgent))]
        [HutongGames.PlayMaker.Tooltip("The target. An NavMeshAgent component is required")]
        public FsmOwnerDefault MoveGameObject;

        NavMeshAgent m_navMeshAgent;
        Transform m_transform;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            m_navMeshAgent = go.GetComponent<NavMeshAgent>();
            m_transform = go.transform;
            ResetNavAgent();
        }

        public override void OnUpdate()
        {
            if (m_navMeshAgent == null)
                return;

            if (!m_navMeshAgent.isOnNavMesh)
                return;

            m_navMeshAgent.SetDestination(m_transform.position - m_transform.forward);
        }

        public override void OnExit()
        {
            base.OnExit();
            ResetNavAgent();
        }

        void ResetNavAgent()
        {
            if (m_navMeshAgent == null)
                return;

            if (!m_navMeshAgent.isOnNavMesh)
                return;
            var position = m_transform.position;
            m_navMeshAgent.SetDestination(position);
            m_navMeshAgent.nextPosition = position;
            m_navMeshAgent.velocity = Vector3.zero;
        }
    }

}
