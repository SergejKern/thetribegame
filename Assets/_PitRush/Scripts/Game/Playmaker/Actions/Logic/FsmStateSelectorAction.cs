﻿using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.Logic
{
    public class FsmStateSelectorAction : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault SourceGO;

        [Serializable]
        public class StateSelector
        {
            public int AmountToPutInPool;
            public FsmEvent Event;
        }

        public StateSelector[] Selectors = new StateSelector[0];

        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(SourceGO);
            if (go == null)
                return;
            if (!go.TryGetComponent<FsmStateSelectorComponent>(out var comp))
                return;

            var @event = comp.PopEventForState(Fsm.ActiveStateName, Selectors);
            Fsm.FsmComponent.SendEvent(@event);
        }

    }
}
