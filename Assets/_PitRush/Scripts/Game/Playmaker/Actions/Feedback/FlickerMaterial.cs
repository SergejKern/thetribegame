﻿using System.Collections.Generic;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.Scripting;

namespace Game.Playmaker.Actions.Feedback
{
    [ActionCategory("Feedback")]
    [HutongGames.PlayMaker.Tooltip("Trigger Feedback")]
    [Preserve]
    public class FlickerMaterial : FsmStateAction
    {
        [RequiredField]
        public FsmGameObject SourceGO;

        public List<Renderer> FlickeringRenderer;

        public bool StopOnExit;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            //m_data = new FeedbackPlayData()
            //{
            //    Config = Config as FeedbackConfig,
            //    FeedbackAnimator = FeedbackAnimator,
            //    FlickeringRenderer = FlickeringRenderer,
            //    ParticleSystem = ParticleSystem
            //};

            //FeedbackOperations.Init(ref m_data);
            //Play(ref m_data, SourceGO.Value.transform.position, SourceGO.Value.GetComponent<PlayMakerFSM>());
            Finish();
        }

        public override void OnExit()
        {
            if (!StopOnExit)
                return;
            //Stop(ref m_data);
        }
    }
}
