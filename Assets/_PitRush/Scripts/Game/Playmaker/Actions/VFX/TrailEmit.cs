﻿using HutongGames.PlayMaker;
using UnityEngine;

namespace Game.Playmaker.Actions.VFX
{
    [ActionCategory("VFX")]
    [HutongGames.PlayMaker.Tooltip("Start/Stop emitting trail")]
    public class TrailEmit : FsmStateAction
    {
        //[CheckForComponent(typeof(TrailRenderer))]
        [RequiredField]
        public FsmGameObject Renderer;

        public bool GetComponentsInChildren = true;
        public bool StopOnExit;

        TrailRenderer m_renderer;

        // Code that runs on entering the state.
        public override void OnEnter()
        {
            SetEmittingActive(true);
            Finish();
        }

        public override void OnExit()
        {
            if (!StopOnExit)
                return;

            SetEmittingActive(false);
        }

        void SetEmittingActive(bool emitting)
        {
            if (GetComponentsInChildren)
            {
                var rends = Renderer.Value.GetComponentsInChildren<TrailRenderer>();
                foreach (var r in rends)
                    r.emitting = emitting;
            }
            else
            {
                Renderer.Value.TryGetComponent(out TrailRenderer renderer);
                renderer.emitting = emitting;
            }
        }
    }
}
