﻿using System.Linq;
using Core.Unity.Interface;
using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.Scripting;

namespace Game.Playmaker.Actions.VFX
{
    [ActionCategory("VFX")]
    [HutongGames.PlayMaker.Tooltip("Play Particle Effect")]
    [Preserve]
    public class ParticlePlay : FsmStateAction
    {
        [RequiredField]
        // todo 0: Make explicit fields for ParticleSystem or PrefabRef? We cannot CheckForComponent when this is a PrefabRef, we dont have component ParticleSystem and vice versa
        // ^- With ScriptableUtility everything can be different
        //[CheckForComponent(typeof(ParticleSystem))]
        public FsmGameObject Emitter;

        public bool WaitForParticleEnd;
        public bool StopOnExit;

        ParticleSystem[] m_particleSystems;

        // Code that runs on entering the state.
        public override void OnEnter()
        {            
            var particleObj = GetInstance(Emitter.Value);
            //particleObj = particleObj.GetPooledInstanceIfPrefab();

            if (particleObj == null)
                return;
            m_particleSystems = particleObj.GetComponentsInChildren<ParticleSystem>();

            foreach (var sys in m_particleSystems)
            {
                var particleSystemEmission = sys.emission;
                particleSystemEmission.enabled = true;
                var sub = sys.subEmitters;
                sub.enabled = true;
                sys.Play(true);

            }
            if (!WaitForParticleEnd)
                Finish();
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            if (m_particleSystems == null)
                return;
            
            var systemIsEmitting = m_particleSystems.Aggregate(false, (current, p) => current | p.isEmitting);
            if (WaitForParticleEnd && !systemIsEmitting)
                Finish();
        }

        public override void OnExit()
        {
            if (!StopOnExit)
                return;
            if (m_particleSystems == null)
                return;

            foreach (var p in m_particleSystems)
            {
                var particleSystemEmission = p.emission;
                particleSystemEmission.enabled = false;
                var sub = p.subEmitters;
                sub.enabled = false;
                p.Stop(true);
                //particleSystem.gameObject.TryDespawn();
            }
            m_particleSystems = null;
        }

        // todo 2: make utility extension, use it everywhere where it makes sense and make it more obvious that it is used like this in several places
        public GameObject GetInstance(GameObject go)
        {
            if (go == null)
                return null;
            var marker = go.GetComponent<IPrefabSpawnMarker>();
            return marker != null ? marker.GetInstance() : Emitter.Value;
        }
    }
}
