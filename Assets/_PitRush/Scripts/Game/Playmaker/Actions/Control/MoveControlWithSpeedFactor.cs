using Game.Actors.Controls;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.Control
{
	[ActionCategory(ActionCategory.Movement)]
	public class MoveControlWithSpeedFactor : FsmStateAction
	{
        [RequiredField]
        [CheckForComponent(typeof(MoveControl))]
        [HutongGames.PlayMaker.Tooltip("The target. An MoveControl component is required")]
        public FsmOwnerDefault moveGameObject;

        public FsmFloat moveFactor;

        MoveControl move;

        // Code that runs on entering the state.
        public override void OnEnter()
		{
            var go = Fsm.GetOwnerDefaultTarget(moveGameObject);
            if (go == null)
                return;

            move = go.GetComponent<MoveControl>();
		}

        public override void OnUpdate()
        {
            base.OnUpdate();
            move.DoMove(moveFactor.Value);
        }
	}

}
