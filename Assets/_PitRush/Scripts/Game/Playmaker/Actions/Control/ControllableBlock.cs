using GameUtility.Interface;
using HutongGames.PlayMaker;

namespace Game.Playmaker.Actions.Control
{
	[ActionCategory("Control")]
	[Tooltip("Blocks Controls")]
	public class ControllableBlock : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(IControllable))]
        [Tooltip("The target. An IControllable component is required")]
        public FsmOwnerDefault ctrlGameObject;

        public FsmBool block;

        // Code that runs on entering the state.
        public override void OnEnter()
		{
            var go = Fsm.GetOwnerDefaultTarget(ctrlGameObject);
            if (go == null)
                return;
            if (!go.TryGetComponent<IControllable>(out var controllable))
                return;
            if (block.Value)
            {
                controllable.AddControlBlocker(new ControlBlocker() { Source = Fsm.FsmComponent });
            }
            else
            {
                controllable.RemoveControlBlocker(new ControlBlocker() { Source = Fsm.FsmComponent });
            }

            Finish();
		}
	}
}