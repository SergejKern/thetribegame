using Game.Actors.Controls;
using HutongGames.PlayMaker;
using UnityEngine;

namespace Game.Playmaker.Actions.Control
{
    [ActionCategory(ActionCategory.Movement)]
    public class MoveControlDirectionTarget : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(MoveControl))]
        [HutongGames.PlayMaker.Tooltip("The target. An MoveControl component is required")]
        public FsmOwnerDefault MoveGameObject;

        [UIHint(UIHint.Variable)]
        public FsmGameObject Target;

        public bool EveryFrame = true;

        MoveControl m_moveControl;

        public override void OnEnter()
        {
            base.OnEnter();
            var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
            if (go == null)
                return;

            if (!go.TryGetComponent(out m_moveControl))
                return;
            var target = Target.Value;

            LookDirection(target);

            if (!EveryFrame)
                Finish();
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            var target = Target.Value;
            if (target == null)
                return;
            if (m_moveControl == null)
                return;

            LookDirection(target);
            //Debug.DrawLine(m_moveControl.transform.position, m_moveControl.transform.position+ lookVec, Color.magenta);
        }

        public override void OnExit()
        {
            base.OnExit();
            if (m_moveControl == null)
                return;
            m_moveControl.SetDirectionInput(Vector2.zero);
        }

        void LookDirection(GameObject target)
        {
            if (m_moveControl == null || target == null)
            {
                var go = Fsm.GetOwnerDefaultTarget(MoveGameObject);
                Debug.LogError($"Could not LookDirection({target}) for {go} | {this}");
                return;
            }
            var lookVec = target.transform.position - m_moveControl.transform.position;
            m_moveControl.SetDirectionInput(new Vector2(lookVec.x, lookVec.z).normalized);
        }
    }
}