﻿using System;
using System.Collections.Generic;
using Core.Unity.Types.Fsm;
using UnityEngine;

namespace Game.Playmaker
{
    public class DebugPlaymakerState : MonoBehaviour
    {
        [Serializable]
        public struct DebugState
        {
            public KeyCode Key;
            [FSMDrawer()]
            public FSMStateRef State;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        List<DebugState> m_debugStates= new List<DebugState>();
#pragma warning restore 0649 // wrong warnings for SerializeField

        // Update is called once per frame
        void Update()
        {
            foreach (var s in m_debugStates)
            {
                if (!Input.GetKeyDown(s.Key))
                    continue;
                s.State.FSM.SetState(s.State.StateName);
            }
        }
    }
}
