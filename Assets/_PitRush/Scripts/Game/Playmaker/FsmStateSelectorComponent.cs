﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Types.Fsm;
using Game.Playmaker.Actions.Logic;
using UnityEngine;

namespace Game.Playmaker
{
    /// <inheritdoc />
    /// <summary>
    /// Put possible actions into a pool, so that probability decreases when action is picked,
    /// when pool empty, reset action pool
    /// </summary>
    public class FsmStateSelectorComponent : MonoBehaviour
    {
        readonly Dictionary<string, List<string>> m_current = new Dictionary<string, List<string>>();

        public string PopEventForState(string state, FsmStateSelectorAction.StateSelector[] stateSelectors)
        {
            EnsureValid(state, stateSelectors);

            return m_current.TryGetValue(state, out var selectors)
                ? selectors.PopRandom()
                : null;
        }

        void EnsureValid(string state, IEnumerable<FsmStateSelectorAction.StateSelector> stateSelectors)
        {
            if (!m_current.TryGetValue(state, out var selectors))
            {
                selectors = new List<string>();
                m_current.Add(state, selectors);
            }

            if (!selectors.IsNullOrEmpty())
                return;
            foreach (var s in stateSelectors)
            {
                for (var i = 0; i < s.AmountToPutInPool; i++)
                    selectors.Add(s.Event.Name);
            }
        }
    }
}
