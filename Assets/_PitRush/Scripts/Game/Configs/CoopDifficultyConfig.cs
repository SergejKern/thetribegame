﻿using System;
using UnityEngine;
using UnityExtensions;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/CoopDifficultyConfig")]
    public class CoopDifficultyConfig : ScriptableObject
    {
        [Serializable]
        public struct DifficultySetting
        {
            public float SpawnEnemiesMultiplier;
        }

        [ReorderableList(disableAdding = true, disableRemoving = true, elementHeaderFormat = "{1} Players")]
        public DifficultySetting[] CoopSettings = new DifficultySetting[4];
    }
}
