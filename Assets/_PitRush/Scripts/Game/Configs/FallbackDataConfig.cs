﻿using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;

namespace Game.Configs
{
    // todo 4: make config generic usable (user can add various different FallbackData-configs here, then can be moved to utility
    [CreateAssetMenu(menuName = "Game/FallbackDataConfig")]
    public class FallbackDataConfig : ScriptableObject
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] KnockBackRoutine.KnockBackConfigData m_knockBackConfigData;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public KnockBackRoutine.KnockBackConfigData GetKnockBackConfigData() => m_knockBackConfigData;
    }
}
