﻿using System.Collections.Generic;
using Core.Unity.Interface;
using Core.Unity.Types;
using Game.Utility;
using UnityEngine;

namespace Game.Configs.Elevator
{
    [CreateAssetMenu(menuName = "Game/ElevatorMoveConfig")]
    public class ElevatorMoveConfig : ScriptableObject
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PrefabData m_barrierPrefab;
        [SerializeField] RefIAudioAsset m_startMove;
        [SerializeField] RefIAudioAsset m_endMove;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public SceneReference Scene;
        public Vector3 ScenePositionOffset;
        public GameObject BarrierPrefab => m_barrierPrefab.Prefab;
        public IAudioAsset StartMove => m_startMove?.Result;
        public IAudioAsset EndMove => m_endMove?.Result;

        public float MoveTime;
        public List<AnimatedCameraSettings> AnimatedSettings;

        public bool ShutdownVisualsOnArrival = true;
    }
}