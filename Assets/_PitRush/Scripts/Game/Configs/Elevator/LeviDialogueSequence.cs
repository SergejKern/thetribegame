﻿using System;
using UnityEngine;

namespace Game.Configs.Elevator
{
    [CreateAssetMenu(menuName = "Game/LeviDialogueSequence")]
    public class LeviDialogueSequence : ScriptableObject
    {
        // ReSharper disable UnusedMember.Global
        public enum LeviFaceExpressions
        {
            Idle,
            Talking,
            Surprised,
            Playful,
            Happy,
            Blinking,
            Angry,
            Sleep
        }
        // ReSharper restore UnusedMember.Global

        public static readonly int FaceExpressionCount = Enum.GetNames(typeof(LeviFaceExpressions)).Length;

        [Serializable]
        public struct DialogueElement
        {
            public float Delay;
            public string Text;
            public float ClearAfterSeconds;
            public LeviFaceExpressions LeviExpression;

            public AK.Wwise.Event[] SoundSet;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        public DialogueElement[] DialogueElements;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public bool SleepOnFinish = true;
    }
}
