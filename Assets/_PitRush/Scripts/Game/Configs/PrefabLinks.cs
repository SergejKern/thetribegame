using System;
using Game.Globals;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Game.Configs
{
    /// <summary>
    /// PrefabLinks
    /// </summary>
    [CreateAssetMenu(menuName = "Game/PrefabLinks")]
    public class PrefabLinks : ScriptableObject
    {
        public static PrefabLinks Instance { get; internal set; }
        public static PrefabLinks I => Instance;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] internal GameObject[] m_prefabs;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public GameObject GetPrefab(Prefab prefab) => m_prefabs[(int) prefab];
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(PrefabLinks))]
    public class PrefabLinksEditor : Editor
    {
        // ReSharper disable once InconsistentNaming
        new PrefabLinks target => base.target as PrefabLinks;
        public override void OnInspectorGUI()
        {
            var enumNames = Enum.GetNames(typeof(Prefab));
            if (target.m_prefabs.Length != enumNames.Length)
                Array.Resize(ref target.m_prefabs, enumNames.Length);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                for (var i = 0; i < enumNames.Length; i++)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        GUILayout.Label(enumNames[i], GUILayout.Width(0.33f * Screen.width));

                        target.m_prefabs[i] = (GameObject) 
                            EditorGUILayout.ObjectField(target.m_prefabs[i], typeof(GameObject), false);
                    }
                }

                if (check.changed) 
                    OnChanged();
            }

        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }
    }
#endif
}
