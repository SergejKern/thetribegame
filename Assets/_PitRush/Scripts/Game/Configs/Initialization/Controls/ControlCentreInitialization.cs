﻿using Game.Actors.Controls;
using GameUtility.InitSystem.Config;
using UnityEngine;

namespace Game.Configs.Initialization.Controls
{
    [CreateAssetMenu(menuName = "Game/Initialization/ControllableConfig")]
    public class ControlCentreInitialization : InitializationConfig<ControlCentre, ControlCentre.ConfigData> { }
}
