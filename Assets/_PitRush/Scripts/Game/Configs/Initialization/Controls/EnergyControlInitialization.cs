﻿using Game.Actors.Controls;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.Controls
{
    public class EnergyControlInitialization : InitializationConfig<EnergyControl, EnergyControl.ConfigData> { }
}
