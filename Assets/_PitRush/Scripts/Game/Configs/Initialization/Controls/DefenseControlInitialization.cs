﻿using Game.Actors.Controls;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.Controls
{
    public class DefenseControlInitialization : InitializationConfig<DefenseControl, DefenseControl.ConfigData> { }
}
