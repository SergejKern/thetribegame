﻿using Game.Actors.Controls;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.Controls
{
    public class MoveControlInitialization : InitializationConfig<MoveControl, MoveControl.ConfigData> {}
}
