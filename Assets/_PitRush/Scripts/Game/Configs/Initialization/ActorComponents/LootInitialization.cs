﻿using GameUtility.Feature.Loot;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.ActorComponents
{
    public class LootInitialization : InitializationConfig<LootSpawnAC, LootSpawnAC.ConfigData> { }
}