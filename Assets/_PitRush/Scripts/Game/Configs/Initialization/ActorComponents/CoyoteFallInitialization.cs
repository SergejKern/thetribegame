﻿using Game.Actors.Components;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.ActorComponents
{
    public class CoyoteFallInitialization : InitializationConfig<CoyoteFallBehaviour, CoyoteFallBehaviour.ConfigData> { }
}
