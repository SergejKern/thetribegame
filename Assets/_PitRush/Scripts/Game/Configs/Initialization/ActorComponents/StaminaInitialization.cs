﻿using Game.Actors.Energy;
using GameUtility.Energy;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.ActorComponents
{
    public class StaminaInitialization : InitializationConfig<StaminaComponent, EnergyConfigData> { }
}
