﻿using Game.Actors.Components;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.ActorComponents
{
    public class RigidbodyInitialization : InitializationConfig<RigidbodyBehaviour, RigidbodyBehaviour.ConfigData> {}
}
