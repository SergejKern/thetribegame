﻿using Game.Actors.Energy;
using GameUtility.InitSystem.Config;

namespace Game.Configs.Initialization.ActorComponents
{
    public class RushInitialization : InitializationConfig<RushComponent, RushComponent.RushFullData> {}
}
