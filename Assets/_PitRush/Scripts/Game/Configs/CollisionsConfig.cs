﻿using System;
using Core.Unity.Interface;
using Core.Extensions;
using Core.Unity.Types;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/CollisionsConfig")]
    public class CollisionsConfig : ScriptableObject, IVerify
    {
        [Serializable]
        public struct CollisionData
        {
            public CollisionMaterialID MatA;
            public CollisionMaterialID MatB;

            public RefIAudioAsset Audio;
#pragma warning disable 0649 // wrong warnings for SerializeField
            [SerializeField] PrefabData[] m_effectData;
#pragma warning restore 0649 // wrong warnings for SerializeField
            public GameObject Effect => m_effectData.IsNullOrEmpty() ? null : m_effectData.RandomItem().Prefab;
        }

        public CollisionData[] CollisionDataList;
        //ref CollisionData Get(int i) => ref CollisionDataList[i];

        public CollisionData GetCollisionData(CollisionMaterialID a, CollisionMaterialID b)
        {
            bool Find(CollisionData dat) => 
                (dat.MatA == a && dat.MatB == b) || (dat.MatB == a && dat.MatA == b);

            var idx = Array.FindIndex(CollisionDataList, Find);
            return idx == -1 ? default : CollisionDataList[idx];
        }

        public void Verify(ref VerificationResult result)
        {
            for (var i = 0; i < CollisionDataList.Length; i++)
            {
                var data = CollisionDataList[i];
                if (data.MatA == null || data.MatB == null)
                {
                    result.Error($"element {i} has missing CollisionMaterialID", this);
                    continue;
                }

                var elements = Array.FindAll(CollisionDataList,
                    a => (a.MatA == data.MatA && a.MatB == data.MatB) ||
                         (a.MatB == data.MatA && a.MatA == data.MatB));
                if (elements.Length > 1) 
                    result.Error($"element with {data.MatA} {data.MatB} is redundant. idx {i}", this);
            }
        }
    }
}
