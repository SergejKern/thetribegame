﻿using System;
using Core.Extensions;
using Core.Unity.Types;
using Game.Utility.VisualAndFX;
using GameUtility.Data.Visual;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/PlayerCharacterConfig")]
    public class PlayerCharacterConfig : ScriptableObject
    {
        [Serializable]
        public struct MeleeWeaponTrailData
        {
            public Material Material;
            public Color[] Colors;
        }

        [Serializable]
        public struct PlayerFXObjects
        {
            public FX_ID ID;
            public PrefabData PrefabDat;
            public GameObject Prefab => PrefabDat.Prefab;
        }

        [Serializable]
        public struct PlayerData
        {
            public PrefabData PrefabDat;

            public Color UIColor;

            public MaterialOverrideMap[] ModelOverrideMaterialMap;
            public MaterialOverrideMap[] GridOverrideMap;
            public MaterialOverrideMap[] ItemGlowOverrideMap;
            [FormerlySerializedAs("WeaponTrailColors")]
            public MeleeWeaponTrailData[] WeaponTrailData;
            public PlayerFXObjects[] FXObjects;

            //public Sprite CharacterSign;
            public Sprite CharacterFace;

            public GameObject Prefab => PrefabDat.Prefab;
        }

        public PlayerData[] Characters;

        public Color GetColor(int idx) => 
            ! idx.IsInRange(Characters) 
            ? default 
            : Characters[idx].UIColor;
    }
}
