using Game.Arena.Level;
using Game.Configs.Combat;
using GameUtility.Config;
using GameUtility.InitSystem.Config;
using UnityEngine;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/ConfigLinks")]
    public class ConfigLinks : ScriptableObject
    {
        public static ConfigLinks Instance;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PrefabLinks m_prefabLinks;
        [SerializeField] SceneLinks m_sceneLinks;
        [SerializeField] DebugDrawConfig m_debugDrawConfig;

        [SerializeField] GameLevelsConfig m_levels;

        [SerializeField] PlayerCharacterConfig m_characters;

        [SerializeField] WeaponConfig m_weaponConfig;
        
        [SerializeField] SpawnInitListConfig m_spawnInitConfig;

        [SerializeField] ItemAnimationCombinationConfig m_animationCombinationConfig;

        [SerializeField] FallbackDataConfig m_fallbackDataConfig;

        [SerializeField] CollisionsConfig m_collisionsConfig;

        [SerializeField] CoopDifficultyConfig m_powerCoopDifficultyConfig;

        [SerializeField] bool m_debugDrawUsedConfigs;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable UnusedMember.Global
        public static PrefabLinks PrefabLinks => Instance == null ? null : Instance.m_prefabLinks;
        public static SceneLinks SceneLinks => Instance == null ? null : Instance.m_sceneLinks;
        public static DebugDrawConfig DebugDrawConfig => Instance == null ? null : Instance.m_debugDrawConfig;
        public static GameLevelsConfig LevelsConfig => Instance == null ? null : Instance.m_levels;
        public static PlayerCharacterConfig Characters => Instance == null ? null : Instance.m_characters;
        public static WeaponConfig WeaponConfig => Instance == null ? null : Instance.m_weaponConfig;
        public static SpawnInitListConfig SpawnInitConfig => Instance == null ? null : Instance.m_spawnInitConfig;
        public static ItemAnimationCombinationConfig AnimationCombinationConfig => Instance == null ? null : Instance.m_animationCombinationConfig;
        public static FallbackDataConfig FallbackDataConfig => Instance == null ? null : Instance.m_fallbackDataConfig;
        public static CollisionsConfig CollisionsConfig => Instance == null ? null : Instance.m_collisionsConfig;
        public static CoopDifficultyConfig CoopDifficultyConfig => 
            Instance == null ? null : Instance.m_powerCoopDifficultyConfig;
        // ReSharper restore UnusedMember.Global

        public PrefabLinks GetPrefabLinks() => m_prefabLinks;
        public SceneLinks GetSceneLinks() => m_sceneLinks;
        public DebugDrawConfig GetDebugDrawConfig() => m_debugDrawConfig;
        public GameLevelsConfig GetLevelsConfig() => m_levels;
        public PlayerCharacterConfig GetCharacters() => m_characters;
        public WeaponConfig GetWeaponConfig() => m_weaponConfig;
        public SpawnInitListConfig GetSpawnInitConfig() => m_spawnInitConfig;
        public ItemAnimationCombinationConfig GetItemAnimationCombinationConfig() => m_animationCombinationConfig;
        public FallbackDataConfig GetDefaultBehaviourConfig() => m_fallbackDataConfig;
        public CollisionsConfig GetCollisionsConfig() => m_collisionsConfig;
        public CoopDifficultyConfig GetPowerCoopMultiplierConfig() => m_powerCoopDifficultyConfig;

        public void DrawUsedConfigsGUI()
        {
            if (!m_debugDrawUsedConfigs)
                return;
            
            using (new GUILayout.HorizontalScope())
                GUILayout.Box($"Spawn Config: {m_spawnInitConfig.name}, Weapon Config: {m_weaponConfig.name}");
        }
    }
}
