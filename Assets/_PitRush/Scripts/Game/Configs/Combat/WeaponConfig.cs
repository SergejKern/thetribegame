using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Attribute;
using Core.Unity.Interface;
using Core.Unity.Types;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Interface;
using GameUtility.Validation;
using UnityEngine;
using UnityEngine.Serialization;

using static Core.Unity.Extensions.CoreEnumExtensions;

namespace Game.Configs.Combat
{
    [CreateAssetMenu(menuName = "Game/WeaponConfig")]
    public class WeaponConfig : ScriptableObject, IVerify, IAnimatorCollectionStateValidation, ISerializationCallbackReceiver
    {
        [Serializable]
        public struct SpawnableWeaponFX
        {
            public WeaponFXID FXID;
            public string Variation;
        }

        [Serializable]
        public struct WeaponComboMove
        {
            public float DamageFactor;
            public float KickbackForceFactor;

            public float AttackSpeedFactor;
            public float RecoverSpeedFactor;
            public float Cooldown;
            [FormerlySerializedAs("StaminaCost")]
            public float EnergyCost;

            // For Timeline-attacks movement in animation is currently supposed to be exactly 1 meter
            // when not timeline-attacking MotionFactor is used as multiplier for animation movement
            // when timeline-attacking MoveRange is used to indicate maximum possible movement (and targeting)
            public float MotionFactor;

            [FormerlySerializedAs("AttackTime")]
            public float AttackHitTime;

            // only for move/weapon-range calculation
            public int AddRangeWeaponIndex;
            // this is used for timeline-attacking
            public float WeaponRangeFactor;
            public float AddBodyRange;
            public float MoveRange;
            //----

            [AnimTypeDrawer(hideAnimator:true, hideLabel:true)]
            public AnimatorStateRef ModelAnimationState;

            public SpawnableWeaponFX[] WeaponFxList;
        }

        [Serializable]
        public struct WeaponComboMoveSet
        {
            public AttackTypeID AttackTypeID;
            public WeaponComboMove[] Moves;
        }

        [Serializable]
        public struct ActorWeaponMoves
        {
            public RuntimeAnimatorController ActorController;
            [SerializeField, HideInInspector] 
            internal string WeaponMovesTypeKey;
            [EnumStringSync(nameof(WeaponMovesTypeKey))]
            public WeaponMovesType WeaponMovesType;

            public WeaponComboMoveSet[] MoveSets;

            public string Name => 
                ActorController != null ? 
                    $"{ActorController.name} {WeaponMovesType}" 
                    : $"{WeaponMovesType}";
        }

        [FormerlySerializedAs("WeaponComboSets")]
        public ActorWeaponMoves[] ActorWeaponMoveSets;

        // ReSharper disable once FlagArgument
        public void GetMoves(RuntimeAnimatorController controller, WeaponMovesType movesType,
            ref Dictionary<AttackTypeID, WeaponComboMove[]> moveSets)
        {
            moveSets.Clear();
            foreach (var w in ActorWeaponMoveSets)
            {
                if (w.ActorController != controller)
                    continue;
                if (w.WeaponMovesType != movesType)
                    continue;

                foreach (var move in w.MoveSets)
                    moveSets.Add(move.AttackTypeID, move.Moves);

                return;
            }
        }

        // ReSharper disable once FlagArgument
        public int GetRelevantMoveIdx(RuntimeAnimatorController controller, WeaponMovesType movesType)
        {
            for (var i = 0; i < ActorWeaponMoveSets.Length; i++)
            {
                var w = ActorWeaponMoveSets[i];
                if (w.ActorController != controller)
                    continue;
                if (w.WeaponMovesType != movesType)
                    continue;

                return i;
            }

            return -1;
        }


        #region DataValidations
        public void Verify(ref VerificationResult result)
        {
            foreach (var set in ActorWeaponMoveSets)
            {
                var equalSets = Array.FindAll(ActorWeaponMoveSets, s =>
                    s.ActorController == set.ActorController && s.WeaponMovesType == set.WeaponMovesType);

                if (equalSets.Length > 1)
                    result.Error($"${nameof(WeaponConfig)} - Duplicated Entry for set [{set.ActorController}, {set.WeaponMovesType}]", this);

                VerifyMoveSets(ref result, set);
            }
        }

        void VerifyMoveSets(ref VerificationResult result, ActorWeaponMoves set)
        {
            foreach (var moveSet in set.MoveSets)
            {
                var equalMoveSets = Array.FindAll(set.MoveSets,m => m.AttackTypeID == moveSet.AttackTypeID);

                if (equalMoveSets.Length <= 1)
                    continue;
                if (moveSet.AttackTypeID == null)
                {
                    result.Error($"${nameof(WeaponConfig)} - No AttackTypeID set!", this);
                    continue;
                }
                result.Error($"${nameof(WeaponConfig)} - Duplicated Entry in set [{set.ActorController}, " +
                                     $"{set.WeaponMovesType}] for type {moveSet.AttackTypeID.name}", this);

                VerifyMoves(ref result, moveSet, set);
            }
        }

        void VerifyMoves(ref VerificationResult result, WeaponComboMoveSet moveSet, ActorWeaponMoves set)
        {
            foreach (var move in moveSet.Moves)
            {
                var animState = move.ModelAnimationState;
                if (animState.Controller == null || string.IsNullOrEmpty(animState.StateName))
                    result.Error(
                        $"${nameof(WeaponConfig)} - Missing Animation in Move Entry in set [{set.ActorController}, " +
                        $"{set.WeaponMovesType}] for type {moveSet.AttackTypeID.name}", this);

                VerifyFX(ref result, move, set, moveSet);
            }
        }

        // ReSharper disable MemberCanBeMadeStatic.Local, UnusedParameter.Local
        void VerifyFX(ref VerificationResult result, WeaponComboMove move, ActorWeaponMoves set,
            WeaponComboMoveSet moveSet)
        {
            //foreach (var fx in move.WeaponFxList)
            //{
            //    if (fx.PrefabDat.Prefab!= null)
            //        continue;
            //    result.Errors++;
            //    Debug.LogError(
            //        $"${nameof(WeaponConfig)} - Missing Prefab for fx {fx.FX} in Move Entry in set [{set.ActorController}, {set.WeaponMovesType}] for type {moveSet.AttackMoveType}");
            //}
        }
        // ReSharper restore MemberCanBeMadeStatic.Local, UnusedParameter.Local

        public IAnimatorStateValidation[] Data
        {
            get
            {
                var dict = new Dictionary<RuntimeAnimatorController, List<string>>();
                foreach (var set in ActorWeaponMoveSets)
                {
                    foreach (var moveSet in set.MoveSets)
                    {
                        foreach (var move in moveSet.Moves)
                        {
                            var animState = move.ModelAnimationState;
                            if (animState.Controller == null || string.IsNullOrEmpty(animState.StateName))
                                continue;
                            if (!dict.ContainsKey(move.ModelAnimationState.Controller))
                                dict.Add(move.ModelAnimationState.Controller, new List<string>());
                            dict[move.ModelAnimationState.Controller].Add(move.ModelAnimationState.StateName);
                        }
                    }
                }

                var returnDat = new IAnimatorStateValidation[dict.Count];

                var keys = dict.Keys;
                var i = 0;
                foreach (var k in keys)
                {
                    returnDat[i] = new AnimatorStateValidationData() { AnimatorController = k, States = dict[k].ToArray() };
                    i++;
                }

                return returnDat;
            }
        }
        #endregion

        #region Safe Enum Serialization
        public void OnBeforeSerialize()
        {
            for (var i = 0; i < ActorWeaponMoveSets.Length; i++)
                OnBeforeSerializeActorWeaponMoveSet(ref ActorWeaponMoveSets[i]);
        }

        static void OnBeforeSerializeActorWeaponMoveSet(ref ActorWeaponMoves move)
        {
            OnBeforeSerializeEnum(out move.WeaponMovesTypeKey,
                move.WeaponMovesType);
        }

        public void OnAfterDeserialize()
        {
            for (var i = 0; i < ActorWeaponMoveSets.Length; i++)
                OnAfterDeserializeActorWeaponMoveSet(ref ActorWeaponMoveSets[i]);
        }

        static void OnAfterDeserializeActorWeaponMoveSet(ref ActorWeaponMoves moves)
        {
            OnAfterDeserializeEnum(ref moves.WeaponMovesType,
                moves.WeaponMovesTypeKey);
        }
        #endregion
    }

    public static class WeaponComboMoveOperations
    {
        // public const float Tolerance = 0.5f;

        static float GetDurationUntilHit(this WeaponConfig.WeaponComboMove move) => 
            move.AttackHitTime / move.AttackSpeedFactor;

        static float GetWeaponRange(this WeaponConfig.WeaponComboMove move, IReadOnlyList<IWeapon> weapons)
        {
            var weaponLength = 0f;
            var wIdx = move.AddRangeWeaponIndex;
            if (wIdx == -1)
            {
                //foreach (var w in weapons) range = Mathf.Max(range, w.WeaponLength);
                weaponLength = weapons.Aggregate(weaponLength, (current, w) => Mathf.Max(current, w.WeaponLength));
            }
            else if (wIdx.IsInRange(0, weapons.Count))
                weaponLength = weapons[wIdx].WeaponLength;
            else return 0f;

            return weaponLength * move.WeaponRangeFactor + move.AddBodyRange; // - Tolerance;
        }

        public static float GetAttackRange(this WeaponConfig.WeaponComboMove move, IReadOnlyList<IWeapon> weapons) =>
            move.GetAttackRange(move.GetWeaponRange(weapons));

        static float GetAttackRange(this WeaponConfig.WeaponComboMove move, float weaponRange) =>
            weaponRange + move.MoveRange;

        public static AttackData GetAttackData(this WeaponConfig.WeaponComboMove move, 
            AttackTypeID typeID,
            IReadOnlyList<IWeapon> weapons)
        {
            var weaponRange = move.GetWeaponRange(weapons);
            // todo 3: agent radius, WeaponComboMove has AddBodyRange which is currently actor independent?
            // this becomes only relevant when using same animationControllers with different actors/ sizes I guess

            return new AttackData()
            {
                AttackDuration = move.GetDurationUntilHit(),
                OnHitRange = weaponRange,
                FullRangeUntilOnHit = move.GetAttackRange(weaponRange),
                AttackState = move.ModelAnimationState.StateName,
                AttackType = typeID
            };
        }
    }
}
