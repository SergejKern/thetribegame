using System;
using System.Collections.Generic;
using Game.Configs.Combat;
using UnityEngine;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/ItemAnimationCombinationConfig")]
    public class ItemAnimationCombinationConfig : ScriptableObject
    {
        // todo 3: safe enum serialization
        [Serializable]
        public struct AnimationCombination
        {
            public WeaponMovesType Primary;
            public WeaponMovesType Secondary;
            public WeaponMovesType Resulting;
        }

        public List<AnimationCombination> AnimationCombinations;

        public bool FindCombination(WeaponMovesType primary, WeaponMovesType secondary, out WeaponMovesType result)
        {
            result = WeaponMovesType.Default;
            var idx = AnimationCombinations.FindIndex((a) => a.Primary == primary && a.Secondary == secondary);
            if (idx == -1)
                return false;

            result = AnimationCombinations[idx].Resulting;
            return true;
        }
    }
}
