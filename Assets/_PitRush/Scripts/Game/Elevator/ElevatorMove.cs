﻿using System.Collections;
using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using Game.Actors.Components;
using Game.Arena;
using Game.Arena.Level;
using Game.Configs;
using Game.Configs.Elevator;
using Game.Globals;
using Game.InteractiveObjects;
using Game.Utility;
using GameUtility;
using GameUtility.Data.Feedback;
using GameUtility.Feature.Loading;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Game.Elevator
{
    public class ElevatorMove : MonoBehaviour
    {
        const float k_rumbleDuration = 0.3f;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Elevator m_elevator;
        [SerializeField] UnityEvent m_onElevatorStarted;
        [SerializeField] UnityEvent m_onElevatorFinished;
        [SerializeField] ArenaPlatform m_platform;
        [SerializeField] SceneID m_pauseSceneID;
        [SerializeField] LoadOperationID m_elevatorLoad;
        //[SerializeField] UnityEvent m_onHalfTransition;
#pragma warning restore 0649 // wrong warnings for SerializeField
        ElevatorMoveConfig m_config;
        ElevatorMoveConfig MoveConfig
        {
            get
            {
                if (m_config != null)
                    return m_config;
                if (ConfigLinks.Instance == null)
                    return null;
                var lvlConf= ConfigLinks.Instance.GetLevelsConfig();
                var lvlData = lvlConf.Get(ArenaLevelFlowConfig.Current);
                m_config = lvlData.MoveConfig;
                return m_config;
            }
            set => m_config = value;
        }

        Barrier m_barrier;

        Barrier BarrierInstance
        {
            get
            {
                if (m_barrier != null) return m_barrier;

                var tr = transform;
                var instance = MoveConfig.BarrierPrefab.GetPooledInstance(tr.position, tr.rotation);
                instance.transform.SetParent(tr);
                instance.TryGetComponent(out m_barrier);
                return m_barrier;
            }
        }

        float m_timer;

        readonly List<GameObject> m_playersOnElevator = new List<GameObject>();

        Rigidbody m_rigidbody;
        Vector3 m_targetPos;

        bool m_isRestingInHub = true;

        public bool CanMove => MoveConfig != null;
        public bool IsMoving { get; private set; }

        public void SetPlayersOnElevator(IEnumerable<GameObject> players)
        {
            foreach (var p in players)
            {
                if (m_playersOnElevator.Contains(p))
                    continue;
                m_playersOnElevator.Add(p);
            }
        }

        void Start()
        {
            if (TryGetComponent(out m_rigidbody))
                m_targetPos = m_rigidbody.position;
        }

        void FixedUpdate()
        {
            if (!IsMoving)
                return;
            if (m_rigidbody == null)
                return;

            m_rigidbody.MovePosition(m_targetPos);
        }

        public void AddOnElevatorStartedListener(UnityAction call) 
            => m_onElevatorStarted.AddListener(call);
        
        public void RemoveOnElevatorStartedListener(UnityAction call) 
            => m_onElevatorStarted.RemoveListener(call);

        public void StartElevator()
        {
            m_onElevatorStarted.Invoke();

            if (m_elevator.LeviOnElevator != null)
                m_elevator.LeviOnElevator.StartDialogue();

            m_targetPos = transform.position;
            IsMoving = true;
            StartCoroutine(ElevatorMovingRoutine());
        }

        IEnumerator ElevatorMovingRoutine()
        {
            var thisGO = gameObject;
            var startPos = transform.position;
            LoadOperations.StartLoading(m_elevatorLoad);

            yield return new WaitForSeconds(0.1f);

            transform.SetParent(null, true);
            if (thisGO.scene != GameGlobals.RuntimeScene)
                SceneManager.MoveGameObjectToScene(thisGO, GameGlobals.RuntimeScene);

            ParentPlayersOnElevatorTo(transform);
            m_platform.IsMoving = true;

            AsyncOperation loadArenaSceneOp = null;

            if (!MoveConfig.Scene.SceneName.IsNullOrEmpty())
                loadArenaSceneOp = SceneManager.LoadSceneAsync(MoveConfig.Scene.SceneName, LoadSceneMode.Additive);
            //loadSceneOp.allowSceneActivation = false;
            var pauseSceneRef = SceneLinks.I.GetScene(m_pauseSceneID);
            var loadPauseSceneOp = SceneManager.LoadSceneAsync(pauseSceneRef, LoadSceneMode.Additive);

            while (loadArenaSceneOp != null && !loadArenaSceneOp.isDone)
                yield return null;
            while (loadPauseSceneOp != null && !loadPauseSceneOp.isDone)
                yield return null;
            LoadOperations.LoadingDone(m_elevatorLoad);

            yield return new WaitForSeconds(0.1f);

            BarrierInstance.RaiseBarrier();
            while (!BarrierInstance.Raised)
                yield return null;

            var pauseScene = SceneManager.GetSceneByName(pauseSceneRef);
            SceneRoot.GetSceneRoot(pauseScene, out var pauseSceneRoot);
            GameGlobals.SetScene(m_pauseSceneID, pauseSceneRoot);
            pauseSceneRoot.InitActive(false);
            yield return null;

            var currentLightSettings =  GameGlobals.CurrentSettings != null ? GameGlobals.CurrentSettings.m_lightSettings : default;
            var nextLightSettings =  currentLightSettings;

            if (!MoveConfig.Scene.SceneName.IsNullOrEmpty())
            {
                var scene = SceneManager.GetSceneByName(MoveConfig.Scene);
                var rootGo = SceneRoot.GetSceneRoot(scene, out var rootComp);
                if (rootGo == null)
                {
                    Debug.LogError("No Scene Settings found!");
                    yield break;
                }

                if (rootComp is SceneSettings newSceneSettings)
                    nextLightSettings = newSceneSettings.LightSettings;

                rootGo.transform.position = MoveConfig.ScenePositionOffset;
            }

            m_timer = MoveConfig.MoveTime;

            InitCameraSettingsAnimation(out var originSettings, out var nextSettingIndex, 
                out var nextSetting, out var curSetting);

            MoveConfig.StartMove?.Play(gameObject);

            while (m_timer > float.Epsilon)
            {
                // elevator as character selection for now
                // this prevents starting game without players
                if (GameGlobals.PlayerCount <= 0)
                {
                    yield return new WaitForFixedUpdate();
                    continue;
                }

                m_timer = Mathf.Max(0f, m_timer - Time.fixedDeltaTime);
                var lerpVal = 1f - m_timer / MoveConfig.MoveTime;

                AnimateCameraSettings(lerpVal, ref nextSetting, originSettings, ref nextSettingIndex, ref curSetting);
                GameGlobals.SceneLights.LerpSettings(currentLightSettings, nextLightSettings, lerpVal);

                m_targetPos = Vector3.Lerp(startPos, MoveConfig.ScenePositionOffset, Mathf.SmoothStep(0, 1, lerpVal));
                GameGlobals.GroundPlaneY = m_targetPos.y;

                yield return new WaitForFixedUpdate();
            }

            OnElevatorFinishedMoving();
        }

        void InitCameraSettingsAnimation(out CameraSettings originSettings, out int nextSettingIndex,
            out AnimatedCameraSettings nextSetting, out AnimatedCameraSettings curSetting)
        {
            originSettings = GameGlobals.GameCamera.Settings;

            nextSettingIndex = 0;

            nextSetting = MoveConfig.AnimatedSettings.Count > nextSettingIndex
                ? MoveConfig.AnimatedSettings[nextSettingIndex]
                : new AnimatedCameraSettings() {m_lerpTime = 1f, m_settings = originSettings};
            curSetting = new AnimatedCameraSettings() {m_lerpTime = 0f, m_settings = originSettings};
        }

        void AnimateCameraSettings(float lerpVal, ref AnimatedCameraSettings nextSetting, CameraSettings originSettings,
            ref int nextSettingIndex, ref AnimatedCameraSettings curSetting)
        {
            if (lerpVal > nextSetting.m_lerpTime)
            {
                nextSettingIndex++;
                curSetting = nextSetting;
                GameGlobals.GameCamera.Settings = curSetting.m_settings;
                nextSetting = MoveConfig.AnimatedSettings.IsIndexInRange(nextSettingIndex)
                    ? MoveConfig.AnimatedSettings[nextSettingIndex]
                    : new AnimatedCameraSettings() {m_lerpTime = 1f, m_settings = originSettings};
            }

            GameGlobals.GameCamera.LerpSettings(curSetting, nextSetting, lerpVal);
        }

        void OnElevatorFinishedMoving()
        {
            IsMoving = false;
            m_platform.IsMoving = false;

            RumbleForAllPlayers();
            MoveConfig.EndMove?.Play(gameObject);
            var shutDown = MoveConfig.ShutdownVisualsOnArrival;

            ParentPlayersOnElevatorTo(null);

            m_barrier.LowerBarrier();
            if (m_elevator.Visuals != null && shutDown)
                m_elevator.Visuals.StartShutdownRoutine();
            if (GameGlobals.CurrentLevelFlow != null)
                GameGlobals.CurrentLevelFlow.StartLevelWhenOnInit();

            m_playersOnElevator.Clear();
            m_onElevatorFinished.Invoke();
        }

        void RumbleForAllPlayers()
        {
            foreach (var playerGo in m_playersOnElevator)
            {
                if (playerGo.TryGetComponent<Player>(out var player)
                    && player.Centre is IRumble r)
                    r.SetRumble(k_rumbleDuration);
            }
        }

        public void ParentPlayersOnElevatorTo(Transform parent)
        {
            var lockToElevator = parent != null;
            foreach (var player in m_playersOnElevator)
            {
                if (player == null)
                {
                    Debug.LogError($"Null player in m_playersOnElevator");
                    continue;
                }   
                player.transform.SetParent(parent, true);
                if (lockToElevator && player.TryGetComponent(out RigidbodyBehaviour rc))
                    m_platform.AddAgentToPlatform(rc);

                // we want player to stay affiliated on elevator, no updates when locked on elevator
                if (player.TryGetComponent(out PlatformAffiliationBehaviour aff))
                    aff.enabled = !lockToElevator;

                // coyoteFalls FixYPos will cause weird jumps through collision walls when attacking, dashing through
                // so we disable when locked on elevator for barrier collisions to work properly
                if (player.TryGetComponent(out CoyoteFallBehaviour cfb))
                    cfb.enabled = !lockToElevator;
            }
        }

        public void OnPlayerSpawnOnElevator(GameObject player)
        {
            m_playersOnElevator.Add(player);
            ParentPlayersOnElevatorTo(transform);

            if (!m_isRestingInHub)
                return;
            m_isRestingInHub = false;
            StartElevator();
        }

        public void SetConfig(ElevatorMoveConfig onFinishConfig) 
            => MoveConfig = onFinishConfig;
    }
}