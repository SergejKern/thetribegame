﻿using System;
using System.Collections;
using Core.Extensions;
using Core.Unity.Extensions;
using Game.Actors;
using Game.Configs;
using Game.Globals;
using GameUtility.Components.Collision;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Game.Elevator.Visuals
{
    public class ElevatorVisuals : MonoBehaviour, ITriggerObjHandling, IInteractiveHolding
    {
        [Serializable]
        public struct VisualSlotsForPlayerCount
        {
            public ElevatorPlayerSlotVisual[] VisibleGameObjects;
        }

        [Serializable]
        public struct SlotConfigData
        {
            public Color DefaultColor;
            public Material DefaultMaterial;
            [FormerlySerializedAs("PlayerEnteredMaterial")] 
            public Material PlayerOnElevatorMaterial;
            public float EmissionIntensity;
            public static readonly int K_EmissionColor = Shader.PropertyToID("_EmissionColor");

            public Vector3 MinScaleHoldFill;
            public float HoldTimeSeconds;
        }

        public struct ActivePlayerData
        {
            public Player Player;

            //public float HoldValue;
            public Color PlayerColor;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject[] m_allPlayerSlots;
        [SerializeField] VisualSlotsForPlayerCount[] m_visualSlotsForPlayerCount;
        [SerializeField] Sprite[] m_playerReadySprites;
        [FormerlySerializedAs("m_borderVisualData")] [SerializeField]
        SlotConfigData m_slotConfigData;

        [SerializeField] SpriteRenderer[] m_elevatorSpriteRenderer;
        [SerializeField] GameObject m_holoEffect;
        [SerializeField] GameObject m_arrows;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly ActivePlayerData[] m_activePlayerData = new ActivePlayerData[4];
        ref ActivePlayerData GetActivePlayerData(int idx) => ref m_activePlayerData[idx];

        int m_playerCount;
        PlayerInputManager m_playerInputManager;

        int PlayerCountSlotsIdx => m_playerCount - 1;
        ElevatorPlayerSlotVisual[] CurrentVisiblePlayerSlots => 
            ! PlayerCountSlotsIdx.IsInRange(m_visualSlotsForPlayerCount) 
            ? null 
            : m_visualSlotsForPlayerCount[PlayerCountSlotsIdx].VisibleGameObjects;

        ElevatorPlayerSlotVisual GetSlotForPlayerIdx(int idx) => 
            ! idx.IsInRange(CurrentVisiblePlayerSlots) 
            ? null 
            : CurrentVisiblePlayerSlots?[idx];

        Elevator m_elevator;

        bool CanMove => m_elevator.Move != null && m_elevator.Move.CanMove;
        public bool InteractEnabled => CanMove && !m_elevator.Move.IsMoving;

        bool m_showAndUpdateSlots;
        
        float[] m_initialAlpha;
        const float k_deactivatedAlpha = 0f;

        const float k_startUpTime = 1f;
        const float k_shutDownTime = 2f;

        Coroutine m_routine;
        public bool LerpRoutine { get; private set; }
        public bool VisualsActive { get; private set; }

        public void Init(Elevator elevator, bool showAndUpdateSlots)
        {
            m_elevator = elevator;

            ClearPlayerData();
            InitAlphaValues();

            //VisualsActive = true;
            m_arrows.SetActive(false);
            m_allPlayerSlots.SetActive(false);

            m_showAndUpdateSlots = showAndUpdateSlots;
            StartUpRoutine();
        }

        void LinkPlayerManagerAndSlots()
        {
            if (!m_showAndUpdateSlots)
                return;
            m_arrows.SetActive(true);

            m_playerInputManager = FindObjectOfType<PlayerInputManager>();
            if (m_playerInputManager == null)
                return;
            m_playerInputManager.playerJoinedEvent.AddListener(OnPlayerJoined);

            m_allPlayerSlots.SetActive(false);
            PlayerCountChangedUpdateUI();
        }

        void InitAlphaValues()
        {
            if (m_initialAlpha != null)
                return;
            m_initialAlpha = new float[m_elevatorSpriteRenderer.Length];
            for (var i = 0; i < m_initialAlpha.Length; i++)
                m_initialAlpha[i] = m_elevatorSpriteRenderer[i].color.a;
        }

        void HidePlayerSlots()
        {
            m_showAndUpdateSlots = false;
            m_allPlayerSlots.SetActive(false);
            m_arrows.SetActive(false);
            ClearPlayerData();
        }

        void OnPlayerJoined(PlayerInput playerInput)
        {
            if (!m_showAndUpdateSlots)
                return;
            if (playerInput.playerIndex.IsInRange(m_activePlayerData))
               m_activePlayerData[playerInput.playerIndex] = new ActivePlayerData();
            //if (m_playerInputManager.playerCount!= m_playerCount)
            //    PlayerCountChangedUpdateUI();
        }

        void PlayerCountChangedUpdateUI()
        {
            if (CurrentVisiblePlayerSlots != null)            
                foreach (var slotVis in CurrentVisiblePlayerSlots)
                    slotVis.gameObject.SetActiveWithTransition(false);

            m_playerCount = GameGlobals.PlayerCount;
            if (CurrentVisiblePlayerSlots == null)
                return;

            for (var i = 0; i < CurrentVisiblePlayerSlots.Length; i++)
            {
                var slotVis = CurrentVisiblePlayerSlots[i];
                slotVis.gameObject.SetActiveWithTransition();
                slotVis.PlayerReadyRenderer.sprite = m_playerReadySprites[i];

                UpdateSlotVisualForPlayerIdx(i);
            }
        }

        void PlayerInTriggerUpdate(Player playerEntered)
        {
            GetPlayerIndexAndColor(playerEntered, out var playerIdx, out var color);
            if (!playerIdx.IsInRange(m_activePlayerData))
                return;
            ref var activePlayerData = ref GetActivePlayerData(playerIdx);

            var changes = (activePlayerData.Player != playerEntered);
            if (!changes)
                return;
            activePlayerData.Player = playerEntered;
            // activePlayerData.HoldValue = 0f;
            activePlayerData.PlayerColor = color;
            UpdateSlotVisualForPlayerIdx(playerIdx);
        }

        public void OnTriggerObjectStay(GameObject root)
        {
            if (!m_showAndUpdateSlots)
                return;

            if (!root.TryGetComponent<Player>(out var playerEntered))
                return;

            PlayerInTriggerUpdate(playerEntered);
        }


        public void OnTriggerObjectEnter(GameObject root)
        {
            if (!m_showAndUpdateSlots)
                return;

            if (!root.TryGetComponent<Player>(out var playerEntered))
                return;

            PlayerInTriggerUpdate(playerEntered);
        }

        void UpdateSlotVisualForPlayerIdx(int playerIdx)
        {
            if (!playerIdx.IsInRange(m_activePlayerData))
                return;
            ref var activePlayerData = ref GetActivePlayerData(playerIdx);
            var slot = GetSlotForPlayerIdx(playerIdx);
            if (slot == null)
                return;

            slot.UpdateSlotVisual(activePlayerData, m_slotConfigData);
        }

        public void OnTriggerObjectExit(GameObject root)
        {
            if (!root.TryGetComponent<Player>(out var playerExited))
                return;
            GetPlayerIndexAndColor(playerExited, out var playerIdx, out _);
            if (!playerIdx.IsInRange(m_activePlayerData))
                return;
            ref var activePlayerData = ref GetActivePlayerData(playerIdx);
            //activePlayerData.HoldValue = 0f;
            activePlayerData.Player = null;

            UpdateSlotVisualForPlayerIdx(playerIdx);
        }
        public void Interact(GameObject actor)
        {
            //if (!actor.TryGetComponent<Player>(out var pl))
            //    return;
            //GetPlayerIndexAndColor(pl, out var playerIdx, out _);
            //if (!playerIdx.IsInRange(m_activePlayerData))
            //    return;
            //ref var activePlayerData = ref GetActivePlayerData(playerIdx);
            //var prevHold = activePlayerData.HoldValue;
            //activePlayerData.HoldValue = Mathf.Clamp01(prevHold + Time.deltaTime/ m_slotConfigData.HoldTimeSeconds);
            //if (Math.Abs(prevHold - activePlayerData.HoldValue) < float.Epsilon)
            //    return;
            //var slot = GetSlotForPlayerIdx(playerIdx);
            //if (slot == null)
            //    return;
            //if (pl.Centre is IRumble r)
            //    r.SetRumble(0.25f,0.05f * activePlayerData.HoldValue);
            //if (prevHold <= 0.0f || activePlayerData.HoldValue >= 1.0f)
            //    slot.UpdateSlotVisual(activePlayerData, m_slotConfigData);
            //else
            //    slot.UpdateHoldFill(activePlayerData.HoldValue, m_slotConfigData);
        }

        public void OnRelease(GameObject actor)
        {
            if (!actor.TryGetComponent<Player>(out var pl))
                return;
            GetPlayerIndexAndColor(pl, out var playerIdx, out _);
            if (!playerIdx.IsInRange(m_activePlayerData))
                return;
            ref var activePlayerData = ref GetActivePlayerData(playerIdx);
            //activePlayerData.HoldValue = 0f;
            var slot = GetSlotForPlayerIdx(playerIdx);
            if (slot == null)
                return;

            slot.UpdateSlotVisual(activePlayerData, m_slotConfigData);
        }

        static void GetPlayerIndexAndColor(Player player, out int playerIdx, out Color playerColor)
        {
            playerIdx = -1;
            playerColor = default;

            if (player == null)
                return;
            if (ConfigLinks.Characters == null)
                return;

            playerColor = ConfigLinks.Characters.GetColor(player.CharacterIdx);
            playerIdx = player.PlayerIdx;
        }

        void Update()
        {
            if (m_showAndUpdateSlots && m_playerCount != GameGlobals.PlayerCount)
                PlayerCountChangedUpdateUI();

            if (InteractEnabled)
                CheckPlayersReady();
        }

        void CheckPlayersReady()
        {
            if (m_playerCount <= 0)
                return;

            for (var i = 0; i < m_playerCount; i++)
            {
                ref var activePlayerData = ref GetActivePlayerData(i);
                //if (activePlayerData.HoldValue < 1f)
                if (activePlayerData.Player == null)
                    return;
            }

            OnAllPlayersReady();
        }

        void OnAllPlayersReady()
        {
            var playerGOs = new GameObject[m_playerCount];
            for (var i = 0; i < m_playerCount; i++)
            {
                ref var activePlayerData = ref GetActivePlayerData(i);
                playerGOs[i] = activePlayerData.Player.gameObject;
            }
            m_elevator.Move.SetPlayersOnElevator(playerGOs);
            m_elevator.Move.StartElevator();

            HidePlayerSlots();
        }

        void ClearPlayerData()
        {
            for (var i = 0; i < m_activePlayerData.Length; i++)
            {
                m_activePlayerData[i].Player = null;
                //m_activePlayerData[i].HoldValue = 0f;
            }
        }

        public void StartShutdownRoutine()
        {            
            if (m_routine != null)
                StopCoroutine(m_routine);
            m_routine = StartCoroutine(LerpVisuals(k_shutDownTime, k_shutDownTime, -1f));
        }

        void StartUpRoutine()
        {
            if (m_routine != null)
                StopCoroutine(m_routine);
            m_routine = StartCoroutine(LerpVisuals(k_startUpTime, 0f, 1f));
        }

        IEnumerator LerpVisuals(float duration, float initLerpVal, float dir)
        {
            LerpRoutine = true;
            if (dir < float.Epsilon)
                m_holoEffect.SetActive(false);

            var timer = 0f;
            while (timer < duration)
            {
                for (var i = 0; i < m_elevatorSpriteRenderer.Length; i++)
                {
                    var r = m_elevatorSpriteRenderer[i];
                    var col = r.color;

                    var lerpVal = initLerpVal + dir * timer;

                    col.a = Mathf.Lerp(k_deactivatedAlpha, m_initialAlpha[i], lerpVal / duration);
                    r.color = col;
                }

                timer += Time.deltaTime;
                yield return null;
            }

            LerpRoutine = false;
            VisualsActive = dir > float.Epsilon;
            if (!VisualsActive) 
                yield break;
            m_holoEffect.SetActive(true);
            LinkPlayerManagerAndSlots();
        }

        public void InstantShutDown()
        {
            StopAllCoroutines();
            LerpRoutine = false;

            HidePlayerSlots();
            m_holoEffect.SetActive(false);

            foreach (var r in m_elevatorSpriteRenderer)
            {
                var col = r.color;
                col.a = k_deactivatedAlpha;
                r.color = col;
            }

            VisualsActive = false;
        }
    }
}
