﻿using Core.Unity.Extensions;
using UnityEngine;

namespace Game.Elevator.Visuals
{
    public class ElevatorPlayerSlotVisual : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] SpriteRenderer m_playerReadyRenderer;
        [SerializeField] SpriteRenderer m_borderRenderer;

        [SerializeField] SpriteRenderer m_pressButtonA;
        [SerializeField] SpriteRenderer m_onHoldFillRenderer;
        [SerializeField] SpriteRenderer m_playerReady;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable ConvertToAutoProperty
        public SpriteRenderer PlayerReadyRenderer => m_playerReadyRenderer;
        // ReSharper restore ConvertToAutoProperty

        Vector3 m_maxScaleHoldFill;

        void Awake()
        {
            m_maxScaleHoldFill = m_onHoldFillRenderer.transform.localScale;
        }

        public void UpdateHoldFill(float holdFill, ElevatorVisuals.SlotConfigData slotConfigData)
        {
            var holdFillTr = m_onHoldFillRenderer.transform;
            holdFillTr.localScale = Vector3.Lerp(slotConfigData.MinScaleHoldFill, m_maxScaleHoldFill, holdFill);
        }

        public void UpdateSlotVisual(ElevatorVisuals.ActivePlayerData activePlayerData, ElevatorVisuals.SlotConfigData slotConfigData)
        {
            if (activePlayerData.Player != null)
            {
                m_playerReady.gameObject.SetActiveWithTransition();
                //m_playerReady.gameObject.SetActiveWithTransition(activePlayerData.HoldValue >= 1.0f);
                //m_pressButtonA.gameObject.SetActiveWithTransition(activePlayerData.HoldValue <= 0.0f);
                //var isHolding = activePlayerData.HoldValue > 0.0f && activePlayerData.HoldValue < 1f;
                //m_onHoldFillRenderer.gameObject.SetActiveWithTransition(isHolding);
                //if (isHolding)
                //    UpdateHoldFill(activePlayerData.HoldValue, slotConfigData);

                if (!m_borderRenderer.sharedMaterial.IsProbablyInstanceOf(slotConfigData.PlayerOnElevatorMaterial))
                    m_borderRenderer.sharedMaterial = slotConfigData.PlayerOnElevatorMaterial;

                var emissionColor = slotConfigData.EmissionIntensity * activePlayerData.PlayerColor;
                m_borderRenderer.material.SetColor(ElevatorVisuals.SlotConfigData.K_EmissionColor, emissionColor);
                m_borderRenderer.color = activePlayerData.PlayerColor;
                m_onHoldFillRenderer.color = activePlayerData.PlayerColor;
                m_playerReady.color = activePlayerData.PlayerColor;
            }
            else
            {
                m_playerReady.gameObject.SetActiveWithTransition(false);

                m_borderRenderer.sharedMaterial = slotConfigData.DefaultMaterial;
                m_borderRenderer.color = slotConfigData.DefaultColor;
            }
            m_pressButtonA.gameObject.SetActiveWithTransition(false);
            m_onHoldFillRenderer.gameObject.SetActiveWithTransition(false);
        }
    }

    // todo 3: move to utility?
    public static class ObjectExt
    {
        public static bool IsProbablyInstanceOf(this Object m, Object other) => m.name.Contains(other.name);
    }
}
