﻿using Core.Unity.Types;
using Game.Arena;
using Game.Elevator.Visuals;
using Game.Globals;
using Game.UI.Dialogue;
using UnityEngine;

namespace Game.Elevator
{
    public class Elevator : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ElevatorMove m_elevatorMove;
        [SerializeField] PrefabRef m_elevatorSurfaceVisuals;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ElevatorVisuals m_visuals;
        LeviOnElevator m_leviOnElevator;

        // ReSharper disable ConvertToAutoProperty
        public LeviOnElevator LeviOnElevator
        {
            get
            {
                if (m_leviOnElevator != null)
                    return m_leviOnElevator;
                if (m_visuals == null)
                    return null;
                m_leviOnElevator = m_visuals.GetComponentInChildren<LeviOnElevator>();
                return m_leviOnElevator;
            }
        }
        public ElevatorVisuals Visuals => m_visuals;
        public ElevatorMove Move => m_elevatorMove;
        // ReSharper restore ConvertToAutoProperty

        void OnEnable()
        {
            GameGlobals.Elevator = this;
            if (TryGetComponent(out ArenaPlatform platform))
                GameGlobals.SafePlatform = platform;
        }

        public void ShowElevatorVisuals() => ShowElevatorVisuals(true);
        public void ShowElevatorVisuals(bool showAndUpdateSlots)
        {
            if (m_visuals != null)
            {                
                m_visuals.Init(this, showAndUpdateSlots);
                return;
            }            

            var instance = m_elevatorSurfaceVisuals.GetInstance();
            instance.TryGetComponent(out m_visuals);
            if (m_visuals != null)
                m_visuals.Init(this, showAndUpdateSlots);
        }
    }
}
