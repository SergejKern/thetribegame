using System;
using Core.Unity.Types;
using Game.Configs.Elevator;
using JetBrains.Annotations;

namespace Game.Elevator
{
    [Serializable]
    public struct ExpressionToAnimation
    {
        [UsedImplicitly] // just for editor
        public LeviDialogueSequence.LeviFaceExpressions Expression;
        [AnimTypeDrawer]
        public AnimatorStateRef Animation;
    }
}