using Core.Events;
using UnityEngine;

namespace Game.Combat
{
    public struct DangerTargetUpdatedEvent
    {
        //todo 3: Events pros cons:
        // I don't like setting actor here and listening to all events ignoring unrelevant ones,
        // but I like you don't need to link in unity, it just works using the scripts...
        // think about how to get positive of both worlds.

        // ReSharper disable MemberCanBePrivate.Global, NotAccessedField.Global
        public readonly GameObject ForActor;

        public readonly float AttackArrivesOnTime;
        public readonly GameObject DangerTarget;
        // ReSharper restore MemberCanBePrivate.Global, NotAccessedField.Global
        DangerTargetUpdatedEvent(GameObject forActor, GameObject dangerTarget, float attackArrivesOnTime)
        {
            ForActor = forActor;
            DangerTarget = dangerTarget;
            AttackArrivesOnTime = attackArrivesOnTime;
        }

        public static void Trigger(GameObject forActor, GameObject dangerTarget, float attackArrivesOnTime)
            => Trigger(new DangerTargetUpdatedEvent(forActor, dangerTarget, attackArrivesOnTime));
        static void Trigger(DangerTargetUpdatedEvent e) => EventMessenger.TriggerEvent(e);

    }
}
