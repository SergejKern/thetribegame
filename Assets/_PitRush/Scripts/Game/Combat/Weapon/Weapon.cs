﻿using System;
using System.Collections.Generic;
using Core.Unity.Utility.GUITools;
using Game.Actors.Controls;
using Game.Configs.Combat;
using Game.InteractiveObjects;
using Game.Utility.CollisionAndDamage;
using GameUtility.Data.TransformAttachment;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game.Combat.Weapon
{
    public class Weapon : MonoBehaviour, IWeapon, IGrabbable
    {
        [Serializable]
        public struct WeaponEvents
        {
            public UnityEvent OnEquipped;
            public UnityEvent OnUnequipped;
            public UnityEvent OnActivated;
            public UnityEvent OnDeactivated;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct WeaponFXLink
        {
            // ReSharper disable once NotAccessedField.Global
            public string Name;

            public WeaponFXID ID;
            public TransformData TransformData;

            public float Scale;

            public string Variation;
            public WeaponFXData FXData;
        }

        [SerializeField] GrabRestrictions m_grabRestrictions;
        [SerializeField] WeaponMovesType m_movesType;

        [SerializeField] WeaponEvents m_events;

        [SerializeField] List<DamageComponent> m_damageComponentList = new List<DamageComponent>();

        [SerializeField] GameObject m_actor;
        [SerializeField] Transform m_handleOffset;

        [SerializeField] float m_weaponLength;

        [FormerlySerializedAs("m_fxAttachments")] [SerializeField]
        WeaponFXLink[] m_fxLinks = new WeaponFXLink[0];

        [SerializeField] int m_weaponIndex = -1;

        //[SerializeField] UnityEvent m_onRecover;

        [SerializeField] Transform m_scalingRoot;
        [SerializeField] WeaponFXID m_defaultFXID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        WeaponFXLink m_currentActivationData;

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible, ConvertToAutoPropertyWithPrivateSetter
        public WeaponMovesType MovesType => m_movesType;

        public GameObject Actor => m_actor;
        public int WeaponIndex => m_weaponIndex;

        bool IsGrabbed => m_actor != null;

        bool m_active;
        // This is only the direct collider for the weapon itself
        // When attacking with energy/ AoE collider could be spawned with the energy-effects? 
        public bool WeaponActive
        {
            get => m_active;
            set
            {
                //Debug.Log($"WeaponActive {gameObject} {WeaponIndex} {value}");
                m_active = value;
                if (value)
                {
                    Play(m_currentActivationData);
                    m_events.OnActivated.Invoke();
                }
                else
                {
                    Stop(m_currentActivationData);
                    m_events.OnDeactivated.Invoke();
                    if (m_scalingRoot != null)
                        m_scalingRoot.localScale = Vector3.one;
                }
            }
        }

        Vector3 Offset => m_handleOffset ? m_handleOffset.localPosition : Vector3.zero;
        public float WeaponLength => m_weaponLength;
        public bool InteractEnabled => !IsGrabbed;
        public GrabRestrictions GrabRestrictions => m_grabRestrictions;

        public WeaponMovesType ItemAnimationType => m_movesType;
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible, ConvertToAutoPropertyWithPrivateSetter

        public void InitWithActor(GameObject actor)
        {
            m_actor = actor;
            foreach (var dmgC in m_damageComponentList)
                dmgC.SetActor(m_actor.transform);
            
            m_events.OnEquipped.Invoke();
            WeaponActive = false;
        }

        public void OnGrabbed(ControlCentre actor) => InitWithActor(actor.ControlledObject);

        public void AdjustPositioning(TransformData grabbingHand)
        {
            var tr = transform;
            var rotation = grabbingHand.Rotation;
            tr.position = grabbingHand.Position - rotation * Offset;
            tr.rotation = rotation;
        }

        public void Drop()
        {
            m_actor = null;
            foreach (var dmgC in m_damageComponentList)
                dmgC.SetActor(null);

            m_events.OnUnequipped.Invoke();

            WeaponActive = false;
        }

        public void Throw() => Drop();

        public void ApplyDamage(float dmg, float kickback, List<ICollisionMaterialComponent> appliedDamageHandlers = null)
        {
            foreach (var dmgC in m_damageComponentList)
            {
                if (dmgC == null)
                    continue;
                dmgC.SetDamageHandlers(appliedDamageHandlers);
                dmgC.ApplyFactor(dmg, kickback);
            }
        }

        public void ForceHitTarget(GameObject target)
        {
            if (!m_active)
                return;
            foreach (var dmComponents in m_damageComponentList)
            {
                if (!dmComponents.isActiveAndEnabled)
                    continue;
                dmComponents.OnTriggerObjectEnter(target);
            }
        }

        void OnDrawGizmos()
        {
            DrawWeaponLength();

            if (Application.isPlaying)
                return;
            if (m_weaponIndex == -1)
                return;
            //CustomDebugDraw.DrawText(transform.position, "W "+m_weaponIndex, Color.red);
#if UNITY_EDITOR
            using (new ColorScope(Color.red))
                UnityEditor.Handles.Label(transform.position, "W " + m_weaponIndex);
#endif
        }

        void DrawWeaponLength()
        {
            var tr = transform;
            var pos = tr.position + tr.rotation*Offset;
            Gizmos.color = Color.red;
            Gizmos.DrawLine(pos, pos + WeaponLength * tr.forward);
        }

        public void SetCurrentActivationData(string variation) => 
            GetFXData(m_defaultFXID, variation, out m_currentActivationData);

        internal void Play(WeaponFXID fx, string variation = "")
        {
            GetFXData(fx, variation, out var fxDat);
            Play(fxDat);
        }

        void Play(WeaponFXLink fxDat)
        {
            FixTransform(ref fxDat);
            if (m_scalingRoot != null && fxDat.Scale > 0f)
                m_scalingRoot.localScale = new Vector3(fxDat.Scale, fxDat.Scale,fxDat.Scale);
            fxDat.FXData.Play(fxDat.TransformData, this);
        }

        void FixTransform(ref WeaponFXLink fxDat)
        {
            if (fxDat.TransformData.Parent != null)
                return;

            fxDat.TransformData.Parent = transform;
        }

        internal void Stop(WeaponFXID fx, string variation = "")
        {
            GetFXData(fx, variation, out var fxDat);
            Stop(fxDat);
        }

        void Stop(WeaponFXLink fxDat)
        {
            FixTransform(ref fxDat);
            fxDat.FXData.Stop(fxDat.TransformData);
        }

        void GetFXData(WeaponFXID fx, string variation, out WeaponFXLink fxLink)
        {
            fxLink = default;
            if (variation == null)
                variation = "";
            var fxIdx = Array.FindIndex(m_fxLinks, a => a.ID == fx && string.Equals(a.Variation, variation));
            if (fxIdx == -1)
                return;
            fxLink = m_fxLinks[fxIdx];
        }
    }
}
