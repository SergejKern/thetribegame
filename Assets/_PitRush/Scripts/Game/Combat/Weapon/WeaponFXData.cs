﻿using System;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using GameUtility.Data.TransformAttachment;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Combat.Weapon
{
    [Serializable]
    public struct FXEventData
    {
        public UnityEvent OnActive;
        public UnityEvent OnInactive;
    }

    // todo 1: we could make this an action-asset with scriptable system for full power/ control
    // ^-WIP (weapons, think about characters, boosts, charge, etc. How to best config the shit?)
    [Serializable]
    public struct WeaponFXData
    {
        public PrefabData SpawnFXPrefab;
        public bool DetachedFX;

        // play animation, f.e. a weapon-slash + collider animation (we probably need to reparent the animated object)
        public AnimatorStateRef Anim;
        public RefIAudioAsset Audio;

        public FXEventData Events;
    }

    public static class WeaponFXOperations
    {
        public static void Play(this WeaponFXData data, TransformData att, IWeapon w = null)
        {
            SpawnPrefab(data, att, w);
            PlayAnimation(data.Anim, w as MonoBehaviour);
            PlayAudio(data.Audio, att.Parent);
            data.Events.OnActive?.Invoke();
            //Activate(data, true);
        }

        public static void Stop(this WeaponFXData data, TransformData att)
        {
            data.Audio?.Result?.Stop(att.Parent);
            data.Events.OnInactive?.Invoke();
        }

        static void PlayAudio(RefIAudioAsset audio, Transform att) => audio?.Result?.Play(att);

        static void PlayAnimation(AnimatorStateRef anim, Component weapon)
        {
            if (string.IsNullOrEmpty(anim.StateName))
                return;

            var animator = anim.Animator;
            if (animator == null && weapon != null)
                animator = weapon.GetComponent<Animator>();
            if (animator == null)
                return;

            animator.Play(anim.StateName);
        }

        static void SpawnPrefab(WeaponFXData data, TransformData att, IWeapon w)
        {
            var prefab = data.SpawnFXPrefab.Prefab;
            if (prefab == null)
                return;
            var spawn = prefab.GetPooledInstance(att.Position, att.Rotation, prefab.transform.lossyScale);

            if (!data.DetachedFX)
            {
                if (spawn.TryGetComponent(out PoolEntity pe))
                    pe.DespawnOnDisable = true;
                spawn.transform.SetParent(att.Parent, true);
            }

            if (w != null && spawn.TryGetComponent<IWeaponSpawn>(out var ws))
                ws.Initialize(w);
        }

        //static void SpawnPrefab(GameObject prefab, TransformData data, IWeapon w)
        //{
        //    if (prefab == null)
        //        return;
        //    var spawn = prefab.GetPooledInstance(data.Position, data.Rotation);
        //    if (w!= null && spawn.TryGetComponent<IWeaponSpawn>(out var ws))
        //        ws.Initialize(w);
        //}
    }
}
