using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;

namespace Game.Combat.Weapon
{
    [CreateAssetMenu(menuName = "Game/AnimationEvents/PlayFX")]
    public class WeaponFXEventData : ScriptableObject
    {
        public WeaponFXID FXID;
        public int WeaponIdx;
    }
}