﻿using System;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.InitSystem;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Combat.Timeline
{
    public partial class CombatTimeline : IInitDataComponent<CombatTimeline.ConfigData>
    {
        [Serializable]
        public struct DifficultySetting
        {
            public float MinPauseBetweenHits;
            public float MaxPauseBetweenHits;

            public float MissPenaltyTimeFactor;
            public float LateAttackTimeFactor;

            [FormerlySerializedAs("TimeFactor")]
            public float SlowDownTimeFactor;
        }

        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;
            public int DangerTargetPriority;

            public DifficultySetting EasyDifficulty;
            public DifficultySetting InitialDifficulty;
            public DifficultySetting HardDifficulty;

            public float SlowDownBeforeHitSeconds;
            public float NormalizeAfterHitSeconds;

            public AnimationCurve SlowDownBeforeHitCurve;
            public AnimationCurve NormalizeAfterHitCurve;

            public static ConfigData Default = new ConfigData()
            {
                DangerTargetPriority = 10,
                SlowDownBeforeHitSeconds = 0.5f,
                NormalizeAfterHitSeconds = 0.35f,

                EasyDifficulty = new DifficultySetting()
                {
                    MinPauseBetweenHits = 1f,
                    MaxPauseBetweenHits = 3f,
                    MissPenaltyTimeFactor = 1f,
                    LateAttackTimeFactor = 0.9f,
                    SlowDownTimeFactor = 0.3f,
                },
                InitialDifficulty = new DifficultySetting()
                {
                    MinPauseBetweenHits = 0.5f,
                    MaxPauseBetweenHits = 2f,
                    MissPenaltyTimeFactor = 0.85f,
                    LateAttackTimeFactor = 0.75f,
                    SlowDownTimeFactor = 0.5f,
                },
                HardDifficulty = new DifficultySetting()
                {
                    MinPauseBetweenHits = 0.2f,
                    MaxPauseBetweenHits = 1f,
                    MissPenaltyTimeFactor = 0.75f,
                    LateAttackTimeFactor = 0.5f,
                    SlowDownTimeFactor = 1f,
                }
            };
        }


        DifficultySetting m_difficultySetting;
        float m_skillValue;

        float GetPause() => UnityEngine.Random.Range(MinPauseBetweenHits, MaxPauseBetweenHits);
        float MinPauseBetweenHits => m_difficultySetting.MinPauseBetweenHits;
        float MaxPauseBetweenHits => m_difficultySetting.MaxPauseBetweenHits;
        float SlowDownTimeFactor => m_difficultySetting.SlowDownTimeFactor;
        float MissPenalty => m_difficultySetting.MissPenaltyTimeFactor;
        float LateAttack => m_difficultySetting.LateAttackTimeFactor;

        int DangerTargetPriority => m_initData.DangerTargetPriority;
        float SlowDownBeforeHitSeconds => m_initData.SlowDownBeforeHitSeconds;
        float NormalizeAfterHitSeconds => m_initData.NormalizeAfterHitSeconds;
        AnimationCurve SlowDownBeforeHitCurve => m_initData.SlowDownBeforeHitCurve;
        AnimationCurve NormalizeAfterHitCurve => m_initData.NormalizeAfterHitCurve;

        public void GetData(out ConfigData data) => data = m_initData;
        public void InitializeWithData(ConfigData data)
        {
            m_initData = data;
            enabled = m_initData.Enabled;
            InitDifficulty();
        }

        public void SetDifficulty(DifficultySetting setting) 
            => m_difficultySetting = setting;

        void Init()
        {
            InitDifficulty();
            m_nextAvailableTime = Time;
            m_nextQueuePause = GetPause();

            m_lastSupposedHitTime = -1000;
            m_lastHitTime = -1000;
            m_lastMissTime = -1000;

            m_protectionActions = m_controlCentre.GetComponents<IDamageProtectionAction>();
        }

        void InitDifficulty()
        {
            m_skillValue = 0.5f;
            UpdateDifficulty();
        }

        public void UpdateDifficulty()
        {
            DifficultySetting from;
            DifficultySetting to;
            float lerpVal;
            var skill = Mathf.Clamp01(m_skillValue);
            if (m_skillValue < 0.5f)
            {
                from = m_initData.EasyDifficulty;
                to = m_initData.InitialDifficulty;
                lerpVal = Mathf.Clamp01(skill * 2);
            }
            else
            {
                from = m_initData.InitialDifficulty;
                to = m_initData.HardDifficulty;
                lerpVal = Mathf.Clamp01((skill - 0.5f) * 2);
            }

            var setting = new DifficultySetting()
            {
                MinPauseBetweenHits = Mathf.Lerp(from.MinPauseBetweenHits, to.MinPauseBetweenHits, lerpVal),
                MaxPauseBetweenHits = Mathf.Lerp(from.MaxPauseBetweenHits, to.MaxPauseBetweenHits, lerpVal),
                SlowDownTimeFactor = Mathf.Lerp(from.SlowDownTimeFactor, to.SlowDownTimeFactor, lerpVal),
                MissPenaltyTimeFactor = Mathf.Lerp(from.MissPenaltyTimeFactor, to.MissPenaltyTimeFactor, lerpVal),
                LateAttackTimeFactor = Mathf.Lerp(from.LateAttackTimeFactor, to.LateAttackTimeFactor, lerpVal)
            };
            SetDifficulty(setting);
        }
    }
}
