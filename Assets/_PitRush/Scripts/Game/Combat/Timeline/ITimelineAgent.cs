﻿using Game.Actors.Components;
using Game.Actors.Controls;
using Game.Actors.Model_Animation;
using GameUtility.Data.EntityAnima;
using GameUtility.Energy;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Combat.Timeline
{
    public interface ITimelineAgent : IAgent, ITimeMultiplier,
        IProvider<Transform>,
        IProvider<RigidbodyBehaviour>,
        IProvider<IHealthComponent>,
        IProvider<IControlCentre>,
        IProvider<AttackControl>,
        IProvider<MoveControl>,
        IProvider<NavMeshAgent>,
        IProvider<AnimatorMoveHandlerComponent>
    {
        ref TimelineAgentData Data { get; }
        void OnTimelineStarted();
        void OnTimelineDone();
    }
}