﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Core.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types.Fsm;
using Game.Actors.Components;
using Game.Actors.Controls;
using Game.Actors.Energy;
using Game.Actors.Model_Animation;
using GameUtility.Energy;
using GameUtility.Events;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.AI;
using TargetPriorityData = GameUtility.Data.EntityAnima.TargetPriorityData;

namespace Game.Combat.Timeline
{
    public class FsmTimelineAgent : MonoBehaviour, ITimelineAgent, IEventListener<KillEvent>
    {
        #region Serialized Fields
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        HealthComponent m_healthPoints;
        [SerializeField]
        protected ControlCentre m_controlCentre;
        [SerializeField]
        InRangeTargetFilterBehaviour m_targetFilter;

        [SerializeField] float m_targetAdjustMeters;

#pragma warning restore 0649 // wrong warnings for SerializeField
        #endregion

        #region Cached Components
        Transform m_transform;
        DashControl m_dashControl;
        AttackControl m_attackControl;
        MoveControl m_moveControl;
        NavMeshAgent m_navMeshAgent;
        AnimatorMoveHandlerComponent m_animatorMoveHandlerComponent;
        #endregion

        #region External Connections
        public FSMUnityCombinedEvent OnTimelineStartRef;
        public FSMUnityCombinedEvent OnTimelineDoneRef;
        public FSMUnityCombinedEvent OnTargetLost;

        [FSMDrawer]
        public FSMGameObjectRef TargetRef;
        #endregion

        #region Properties
        //public TargetingIntent Intent { get; set; }

        public float OverrideSpeed = -1;
        public virtual float SpeedFactor
        {
            get
            {
                if (OverrideSpeed >= 0f)
                    return OverrideSpeed;
                return (Target is CombatTimeline tl) ? tl.UnpenalizedSpeedFactor : 1f;
            }
        }
        public virtual float Time
        {
            get => (Target is CombatTimeline tl) ? tl.Time : UnityEngine.Time.time;
            protected set => throw new NotImplementedException();
        }

        public bool IsStatic => false;
        public bool IsTargetingPossible => enabled && m_healthPoints != null && m_healthPoints.HP > 0f;

        public ITarget Target
        {
            get
            {
                if (m_currentTarget == null)
                    ReevaluateCurrentTarget();

                return m_currentTarget;
            }
            private set
            {
                if (value == null && m_currentTarget != null)
                    OnTargetLost.Invoke();

                m_currentTarget = value;
                Get(out MoveControl moveControl);
                Get(out AnimatorMoveHandlerComponent animatorMoveHandler);
                if (!this.IsMovedViaTimeline())
                {
                    moveControl.SetAimingEnabled(m_currentTarget == null);
                    animatorMoveHandler.DoMove = m_currentTarget == null || !m_currentTarget.IsStatic;
                }
            }
        }

        protected ITarget LastTarget;
        protected float LastTargetSetTime;
        public ref TimelineAgentData Data => ref m_data;
        #endregion

        #region Fields
        protected readonly List<TargetPriorityData> Targets = new List<TargetPriorityData>();

        ITarget m_currentTarget;
        TimelineAgentData m_data;
        #endregion

        #region Targeting Methods
        public void SetTargetWithPriority(ITarget target, int priority)
        {
            if (target == null)
            {
                ClearTargetPriority(priority);
                return;
            }

            if (Data.OnTimeline != null)
                LastTargetSetTime = Data.OnTimeline.Time;
            LastTarget = target;

            var data = new TargetPriorityData() { Priority = priority, Target = target };
            var idx = Targets.FindIndex(t => t.Priority == priority);
            if (idx == -1)
                Targets.Add(data);
            else
                Targets[idx] = data;

            ReevaluateCurrentTarget();
        }
        public void ClearTargetPriority(int priority)
        {
            var idx = Targets.FindIndex(t => t.Priority == priority);
            if (idx != -1)
                Targets.RemoveAt(idx);

            ReevaluateCurrentTarget();
        }

        void ReevaluateCurrentTarget()
        {
            if (Targets.IsNullOrEmpty())
            {
                Target = null;
                TargetRef.Value = null;
                return;
            }
            Targets.Sort((a, b) => a.Priority.CompareTo(b.Priority));
            var newTarget = Targets.Last().Target;
            //if (gameObject.name.Contains("Player") && m_currentTarget_!= newTarget)
                //Debug.Log("New Target :"+ Targets.Last().Priority);
            Target = newTarget;
            TargetRef.Value = newTarget?.GameObject;
        }
        #endregion

        #region Unity Methods
        protected virtual void OnEnable()
        {
            EventMessenger.AddListener(this);
            m_data.FollowTargetMeters = m_targetAdjustMeters;
            LastTargetSetTime = -1000;
        }

        protected virtual void OnDisable() => EventMessenger.RemoveListener(this);
        protected virtual void Update() => UpdateLockedTargetAiming();

        #endregion

        #region Event Methods
        public void OnEvent(KillEvent eventType)
        {
            if (eventType.KilledActor == gameObject)
                OnThisKilled();
            else
            {
                if (eventType.KilledActor == Target?.GameObject)
                    OnTargetKilled();

                Targets.RemoveAll(a => a.Target?.GameObject == eventType.KilledActor);
                ReevaluateCurrentTarget();
            }
        }
        void OnTargetKilled()
        {
            if (m_data.OnTimeline != null)
                this.SetToDone(m_data.OnTimeline.Time, false);
        }
        void OnThisKilled()
        {
            if (m_data.OnTimeline != null)
                this.SetToDone(m_data.OnTimeline.Time, true);
            Targets.Clear();
            ReevaluateCurrentTarget();
        }

        public void CancelAttack()
        {
            if (m_data.OnTimeline != null)
                this.SetToDone(m_data.OnTimeline.Time, true);
        }

        public void OnTimelineStarted()
        {
            OnTimelineStartRef.Invoke();
            Get(out MoveControl moveControl);
            moveControl.SetAimingEnabled(false);
        }

        public void OnTimelineDone()
        {
            // if (OnTimelineDoneRef.m_fsmEvent.FSM == null)
            //    Debug.LogWarning($"{gameObject} no Fsm!");
            OnTimelineDoneRef.Invoke();
            ReevaluateCurrentTarget();
        }

        public void OnHitTarget(GameObject go)
        {
            if (Target?.GameObject != go)
                return;
            // todo 3: not sure about finishing timeline-attack early
            if (m_data.OnTimeline != null)
                this.AttackLanded();
        }
        #endregion

        void UpdateLockedTargetAiming()
        {
            if (m_targetFilter == null)
                return;
            if (this.IsMovedViaTimeline())
                return;
            if (m_currentTarget == null)
                return;
            var vec = (m_currentTarget.Transform.position - m_controlCentre.Position);
            var inRange = vec.sqrMagnitude < m_targetFilter.SqrLockDistance;
            if (!inRange || !m_currentTarget.IsTargetingPossible)
            {
                // m_moveControl.SetDirectionInput(Vector2.zero);
                return;
            }

            var input = new Vector2(vec.x, vec.z);
            if (m_moveControl != null)
                m_moveControl.SetDirectionInput(input.normalized);
        }

        #region Component Provider

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWithPrivateSetter
        public Transform Transform
        {
            get
            {
                Get(out Transform tr);
                return tr;
            }
        }
        public GameObject GameObject => gameObject;
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWithPrivateSetter
        // ReSharper disable MethodNameNotMeaningful
        public void Get(out IHealthComponent provided) =>
            provided = m_healthPoints;
        public void Get(out IControlCentre provided) =>
            provided = m_controlCentre;

        public void Get(out Transform provided)
        {
            if (m_transform == null)
                m_transform = transform;

            provided = m_transform;
        }
        public void Get(out RigidbodyBehaviour provided) => provided = m_controlCentre.RigidbodyComponent;

        public void Get(out AttackControl provided)
        {
            if (m_attackControl == null)
                m_controlCentre.TryGetComponent(out m_attackControl);

            provided = m_attackControl;
        }
        public void Get(out MoveControl provided)
        {
            if (m_moveControl == null)
                m_controlCentre.TryGetComponent(out m_moveControl);

            provided = m_moveControl;
        }
        public void Get(out AnimatorMoveHandlerComponent provided)
        {
            if (m_animatorMoveHandlerComponent == null)
                m_controlCentre.Animator.TryGetComponent(out m_animatorMoveHandlerComponent);
            provided = m_animatorMoveHandlerComponent;
        }
        public void Get(out NavMeshAgent agent)
        {
            if (m_navMeshAgent == null)
                m_controlCentre.TryGetComponent(out m_navMeshAgent);
            agent = m_navMeshAgent;
        }
        // ReSharper restore MethodNameNotMeaningful
        #endregion
    }
}
