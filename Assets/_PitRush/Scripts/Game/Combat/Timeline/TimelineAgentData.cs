﻿using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;

namespace Game.Combat.Timeline
{
    public struct TimelineAgentData
    {
        public TimelineState State;
        public CombatTimeline OnTimeline;

        public Transform Target;
        public Vector3 InitialTargetPos;
        public Vector3 CurrentTargetPos;

        // interpolate from this
        public Vector3 StartPos;

        public AttackTypeID MoveType;
        public int MoveIdx;

        public AttackData AttackData;

        public float MoveStartTime;
        public float MoveEndTime;
        public float MoveDuration;

        public float AttackArrivesOnTime;
        public bool OnTrack;
        public float FollowTargetMeters;

        public bool IsMovedViaTimeline => 
            State >= TimelineState.Moving && State < TimelineState.AttackLanded;
    }
    public enum TimelineState
    {
        None,
        WaitForStart,
        Moving,
        Attacking,
        AttackLanded,
        Done = None
    }
}
