﻿using Core.Events;
using Core.Unity.Types;
using Game.Actors.Energy;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Events;
using UnityEngine;

namespace Game.Combat.Timeline
{
    public class SkillEvaluation : MonoBehaviour,
        IEventListener<KillEvent>,
        IEventListener<DamageEvent>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] HealthComponent m_health;

        [SerializeField] int m_killsPerHitDefault;
        [SerializeField] int m_killsPerHitHardcore;
        [SerializeField] UnityFloatEvent m_onSkillChanged;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float MinDataAmount => 0.5f * m_killsPerHitDefault;
        float IdealDataAmount => m_killsPerHitHardcore + 1;

        int m_killCount;
        int m_getHitCount;
        float m_skill;

        void OnEnable()
        {
            EventMessenger.AddListener<KillEvent>(this);
            EventMessenger.AddListener<DamageEvent>(this);
            m_health.AddChangeListener(OnHealthChanged);
        }

        void OnDisable()
        {
            EventMessenger.RemoveListener<KillEvent>(this);
            EventMessenger.RemoveListener<DamageEvent>(this);
            m_health.RemoveChangeListener(OnHealthChanged);
        }

        void OnHealthChanged(float hp) => UpdateSkill();
        public void OnEvent(KillEvent eventType)
        {
            if (eventType.Killer != gameObject)
                return;
            m_killCount++;
            UpdateSkill();
        }

        public void OnEvent(DamageEvent eventType)
        {
            if (eventType.Damaged != gameObject)
                return;
            m_getHitCount++;
            UpdateSkill();
        }

        void UpdateSkill()
        {
            var maxHp = m_health.MaxHP;
            var hp = m_health.HP;
            var pValHp = (hp - 1) / (maxHp - 1);
            var pValHpHalf = pValHp * 0.5f;

            var dataAmount = m_killCount + m_getHitCount;
            if (dataAmount < MinDataAmount)
            {
                m_skill = pValHpHalf;
                m_onSkillChanged.Invoke(m_skill);

                return;
            }

            var killScoreDef = m_killCount / m_killsPerHitDefault;
            var killScoreHard = m_killCount / m_killsPerHitHardcore;

            var dataValidity = Mathf.Min(dataAmount / IdealDataAmount, 1f);
            var dataValidityInvert = 1f - dataValidity;

            var hitKillRatioDef = m_getHitCount > 0 ? killScoreDef / m_getHitCount : 1;
            var hitKillRatioHard = m_getHitCount > 0 ? killScoreHard / m_getHitCount : 1;

            var hitKillRatioDef05 = Mathf.Min(hitKillRatioDef, 0.5f);
            var hitKillRatioDef1 = Mathf.Min(hitKillRatioDef, 1f);
            var hitKillRatioDef1Sqr = hitKillRatioDef1 * hitKillRatioDef1;
            var hitKillRatioHard05 = Mathf.Min(hitKillRatioHard, 0.5f);
            var hitKillRatioHardFix = hitKillRatioHard05 * hitKillRatioDef1Sqr;

            var skillHitRatio = hitKillRatioDef05 + hitKillRatioHardFix;

            var skillResultPartA = skillHitRatio * dataValidity * pValHp;
            var skillResultPartB = pValHpHalf * dataValidityInvert;

            m_skill = skillResultPartA + skillResultPartB;

            m_onSkillChanged.Invoke(m_skill);
        }
    }
}
