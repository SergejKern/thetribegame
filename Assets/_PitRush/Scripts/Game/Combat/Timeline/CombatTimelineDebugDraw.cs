﻿using System;
using Core.Unity.Utility.Debug;
using Game.Actors.Controls;
using UnityEngine;

namespace Game.Combat.Timeline
{
    public partial class CombatTimeline
    {
#if UNITY_EDITOR
        const float k_kDrawGUIWorldZ = 10f;
        const float k_kTimelineGUIOffsetY = 30f;
        const float k_kVerticalHalfSize = 20f;

        const float k_offsetX = 15f;

        const float k_timelinePastStart = k_offsetX;
        const float k_timelinePastEnd = 100f;

        const float k_timelineStart = k_timelinePastEnd;
        const float k_timelineEnd = 1500f;

        const float k_unitsPerSecond = 400f;

        Camera m_mainCamForGUI;

        public void OnGUI()
        {
            m_mainCamForGUI = Camera.main;
            if (m_mainCamForGUI == null)
                return;

            var sec = 0;
            for (var i = k_timelineStart; i <= k_timelineEnd; i += k_unitsPerSecond, ++sec)
            {
                var p = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(i,
                    k_kTimelineGUIOffsetY + 30f, k_kDrawGUIWorldZ));
                CustomDebugDraw.DrawText(p, $"{sec} sec", Color.white);
            }

            foreach (var a in m_attackQueue)
            {
                var arrivesInSeconds = (a.Data.AttackArrivesOnTime - Time);
                var x = k_timelinePastEnd + arrivesInSeconds * k_unitsPerSecond;
                var p = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(x,
                    k_kTimelineGUIOffsetY + 75f, k_kDrawGUIWorldZ));

                var stateColor = Color.white;
                switch (a.Data.State)
                {
                    case TimelineState.None: break;
                    case TimelineState.WaitForStart: stateColor = Color.blue; break;
                    case TimelineState.Moving: stateColor = Color.yellow; break;
                    case TimelineState.Attacking: stateColor = Color.red; break;
                    case TimelineState.AttackLanded: stateColor = Color.green; break;
                    default: throw new ArgumentOutOfRangeException();
                }
                CustomDebugDraw.DrawText(p, $"{a.Data.State}\narrives in {arrivesInSeconds} sec", stateColor);
            }

            {
                var p = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(k_timelineStart,
                    k_kTimelineGUIOffsetY + 60f, k_kDrawGUIWorldZ));
                CustomDebugDraw.DrawText(p, $"Skill {m_skillValue}", Color.blue);
            }
        }


        void DrawTimelineGUI()
        {
            m_mainCamForGUI = Camera.main;
            if (m_mainCamForGUI == null)
                return;

            DrawHorizontal(k_timelinePastStart, k_timelinePastEnd, Color.black);
            DrawHorizontal(k_timelineStart, k_timelineEnd, Color.white);
            for (var i = k_timelineStart; i <= k_timelineEnd; i+= k_unitsPerSecond)
                DrawVertical(i, Color.gray, 0.25f);


            var t = GetNextTimeForAttackHit(null);
            DrawVertical(GetTimelineGUIPos(t), Color.magenta);

            var state = OwningAgent.Data.State;
            if (state != TimelineState.None)
            {
                var tPos = GetTimelineGUIPos(OwningAgent.Data.AttackArrivesOnTime);
                DrawVertical(tPos, Color.cyan);
            }
            else
            {
                OwningAgent.Get(out AttackControl attackControl);
                var tPos = GetTimelineGUIPos(Time + attackControl.GetNextAttackData.AttackDuration);
                DrawVertical(tPos, Color.cyan);
            }

            foreach (var a in m_attackQueue)
            {
                var x = GetTimelineGUIPos(a.Data.AttackArrivesOnTime);
                DrawVertical(x, Color.red);
            }
            //CustomDebugDraw.DrawText();
        }

        float GetTimelineGUIPos(float time)
        {
            var arrivesInSeconds = (time - Time);
            var x = k_timelinePastEnd + arrivesInSeconds * k_unitsPerSecond;
            return x;
        }

        void DrawHorizontal(float startX, float endX, Color c)
        {
            var p1 = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(startX, k_kTimelineGUIOffsetY, k_kDrawGUIWorldZ));
            var p2 = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(endX, k_kTimelineGUIOffsetY, k_kDrawGUIWorldZ));
            Debug.DrawLine(p1, p2, c);
        }

        void DrawVertical(float xVal, Color c, float size = 1f)
        {
            var yHalf = size * k_kVerticalHalfSize;
            var p1 = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(xVal, 
                k_kTimelineGUIOffsetY- yHalf, k_kDrawGUIWorldZ));
            var p2 = m_mainCamForGUI.ScreenToWorldPoint(new Vector3(xVal, 
                k_kTimelineGUIOffsetY+ yHalf, k_kDrawGUIWorldZ));
            Debug.DrawLine(p1, p2, c);
        }
#else
        void DrawTimelineGUI(){}
#endif
    }
}

//foreach (var inRange in m_targetFilter.InRangeTargets)
//{
//    if (_!(inRange is ITimelineAgent timelineAgent))
//        continue;
//    var isAttackingThisAgent = false;
//    if (timelineAgent.Target == m_thisTarget)
//    {
//        var nextAttack = timelineAgent.AttackArrivesInSeconds;
//        isAttackingThisAgent = (nextAttack > 0f);
//    }
//    // ui shit...
//    if (isAttackingThisAgent)
//    {
//    }
//    else
//    {
//    }
//}