﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Game.Actors.Components;
using Game.Actors.Controls;
using Game.Actors.Model_Animation;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Combat.Timeline
{
    public static class TimelineOperations
    {
        //const float k_timelineTargetAdjustingToleranceMeters = 3f;
        //const float k_sqrTimelineTargetAdjustingToleranceMeters =
        //    k_timelineTargetAdjustingToleranceMeters * k_timelineTargetAdjustingToleranceMeters;

        #region Public Extension Methods
        public static bool IsMovedViaTimeline(this ITimelineAgent agent) => agent.Data.IsMovedViaTimeline;
        public static void InitAgentForTimeline(this ITimelineAgent agent,
            AttackTypeID moveType, int moveIdx,
            CombatTimeline onTimeline)
        {
            ref var data = ref agent.Data;
            data.Target = agent.Target.Transform;
            data.State = TimelineState.WaitForStart;
            data.OnTimeline = onTimeline;

            agent.InitAttackData(moveType, moveIdx);
            agent.GetFullCombatMoveDuration(agent.Data.AttackData, data.Target, out var duration);
            var hitTime = onTimeline.GetNextTimeForAttackHit(agent, duration);

            agent.InitTimeline(hitTime, duration);
            onTimeline.RegisterAttack(agent);
            //Debug.Log($"{agent} registered, hitTime {hitTime}");
            if (data.MoveStartTime > onTimeline.Time)
                return;
            //Debug.Log($"{agent}will start immediately");

            if (data.MoveDuration <= float.Epsilon)
            {
                agent.OnTimelineStarted();
                agent.StartAttack();
            }
            else
                agent.StartAgentMoving(onTimeline);
        }

        public static void GetFullCombatMoveDuration(this ITimelineAgent agent,
            AttackData attackData,
            Transform target,
            out float minAttackDuration)
        {
            //agent.Get(out Transform transform);
            agent.Get(out RigidbodyBehaviour rigid);
            agent.Get(out MoveControl moveControl);
            var mps = moveControl.MetersPerSecond;
            var targetPos = target.position;
            agent.Data.InitialTargetPos = targetPos;
            agent.Data.CurrentTargetPos = targetPos;
            var preAttackPos = agent.GetPreAttackPosition(targetPos);
            var currentPos = rigid.Position;
            var distance = Vector3.Distance(preAttackPos, currentPos);
            var moveSeconds = distance / mps;

            //Debug.Log($"mps: {mps} distance: {distance} moveSeconds {moveSeconds}" +
            //         $"fromPos {currentPos} toPos {preAttackPos}");

            minAttackDuration = moveSeconds + attackData.AttackDuration;
        }

        internal static void UpdateOnTimeline(this ITimelineAgent agent, CombatTimeline onTimeline)
        {
            if (!agent.GameObject.activeInHierarchy)
            {
                agent.SetToDone(onTimeline.Time, true);
                return;
            }
            switch (agent.Data.State)
            {
                case TimelineState.WaitForStart: agent.UpdateWaitOnTimeline(onTimeline); break;
                case TimelineState.Moving: agent.UpdateMovingOnTimeline(onTimeline); break;
                case TimelineState.Attacking: agent.UpdateAttackingOnTimeline(onTimeline); break;
                case TimelineState.AttackLanded: agent.UpdateAfterAttackLanded(onTimeline); break;
                case TimelineState.None: break;
                default: throw new ArgumentOutOfRangeException();
            }
        }
        // ReSharper disable once FlagArgument
        internal static void SetToDone(this ITimelineAgent agent, float time, bool isCancel)
        {
            if (agent.Data.State == TimelineState.Attacking)
                agent.AttackingStateEnd();

            if (agent.Data.AttackArrivesOnTime > time)
                agent.Data.AttackArrivesOnTime = time;
            agent.Data.State = TimelineState.Done;

            if (isCancel)
            {
                agent.Get(out AttackControl attackControl);
                attackControl.OnAttackCanceled();
            }

            agent.OnTimelineDone();
        }

        public static void AttackLanded(this ITimelineAgent agent)
        {
            if (agent.Data.State != TimelineState.Attacking)
                return;

            agent.UpdateTargetTracking();

            if (agent.Data.OnTrack)
            {
                //Debug.Log($"{agent} Force Hit {agent.Data.Target.name}");
                agent.Data.OnTimeline.OnHit(agent);
                agent.ForceHitTarget();
            }
            else
            {
                //Debug.Log("AttackLanded - Updating Pause ");
                agent.Data.OnTimeline.OnMiss(agent);
            }
            agent.AttackingStateEnd();
            agent.Data.State = TimelineState.AttackLanded;
        }
        #endregion

        static void InitAttackData(this ITimelineAgent agent, AttackTypeID moveType, int moveIdx)
        {
            ref var data = ref agent.Data;
            agent.Get(out AttackControl attackControl);

            data.MoveType = moveType;
            data.MoveIdx = moveIdx;

            var attackData = attackControl.GetMoveData(data.MoveType, data.MoveIdx);
            data.AttackData = attackData.GetAttackData(data.MoveType, attackControl.CurrentWeapons);
            //data.AttackData.WeaponRange += 0.5f;
        }

        static void InitTimeline(this ITimelineAgent agent, float attackHitTime, float fullDuration)
        {
            ref var data = ref agent.Data;

            data.MoveEndTime = attackHitTime - data.AttackData.AttackDuration;
            data.MoveStartTime = attackHitTime - fullDuration;
            data.MoveDuration = data.MoveEndTime - data.MoveStartTime;

            // m_timelineAgent.Timeline = timeline;
            data.AttackArrivesOnTime = attackHitTime;
            // m_navAgent.SetDestination(preAttackPos);
        }


        static Vector3 GetPreAttackPosition(this ITimelineAgent agent, Vector3 targetPos)
            => agent.GetPreTargetPosition(targetPos, agent.Data.AttackData.FullRangeUntilOnHit);
        static Vector3 GetFinalPosition(this ITimelineAgent agent, Vector3 targetPos)
            => agent.GetPreTargetPosition(targetPos, agent.Data.AttackData.OnHitRange);

        static Vector3 GetPreTargetPosition(this ITimelineAgent agent, Vector3 targetPos, float stopMeters)
        {
            //agent.Get(out Transform transform);
            agent.Get(out RigidbodyBehaviour rigid);
            var transformPosition = rigid.Position;
            var currentVec = (targetPos - transformPosition);
            currentVec.y = 0;
            var dir = currentVec.normalized;
            var preAttackPos = targetPos - dir * stopMeters;
            var idealVec = (targetPos - preAttackPos);
            return idealVec.sqrMagnitude > currentVec.sqrMagnitude ?
                transformPosition + 0.001f * dir : 
                preAttackPos;
        }

        static void UpdateWaitOnTimeline(this ITimelineAgent agent, CombatTimeline onTimeline)
        {
            ref var data = ref agent.Data;
            var targetPos = agent.Data.Target.position;
            agent.Data.InitialTargetPos = targetPos;
            agent.Data.CurrentTargetPos = targetPos;
            if (onTimeline.Time < data.MoveStartTime)
                return;

            agent.StartAgentMoving(onTimeline);
        }

        static void UpdateMovingOnTimeline(this ITimelineAgent agent, CombatTimeline onTimeline)
        {
            ref var data = ref agent.Data;
            agent.UpdateTargetTracking();

            var preAttackPos = agent.GetPreAttackPosition(agent.Data.CurrentTargetPos);

            //navAgent.CalculatePath(preAttackPos, m_path);
            var lerpVal = (onTimeline.Time - data.MoveStartTime) / data.MoveDuration;
            agent.SetPosition(onTimeline, data.StartPos, preAttackPos, lerpVal);
            if (agent.Data.State == TimelineState.Done)
                return;
            if (onTimeline.Time <= data.MoveEndTime)
                return;
            agent.StartAttack();
        }

        // ReSharper disable once FlagArgument
        static void UpdateTargetTracking(this ITimelineAgent agent)
        {
            ref var data = ref agent.Data;

            var targetPos = data.Target.position;
            var targetMoveDir = targetPos - agent.Data.InitialTargetPos;

            //var wasOnTrack = agent.Data.OnTrack;
            var targetAdjust = agent.Data.FollowTargetMeters;
            var sqrTargetAdjusting = targetAdjust * targetAdjust;
            agent.Data.OnTrack = Vector3.SqrMagnitude(targetMoveDir) <= sqrTargetAdjusting;
            if (!agent.Data.OnTrack)
            {
                targetPos = agent.Data.InitialTargetPos + targetAdjust * targetMoveDir.normalized;
                //if(wasOnTrack)
                //    Debug.Log("Lost Track");
            }            //Debug.Log($"targetIsTracked {data.Target.name} {targetIsTracked}");

            agent.Data.CurrentTargetPos = targetPos;
        }

        static void UpdateAttackingOnTimeline(this ITimelineAgent agent, CombatTimeline onTimeline)
        {
            ref var data = ref agent.Data;
            agent.UpdateTargetTracking();
            var finalPos = agent.GetFinalPosition(agent.Data.CurrentTargetPos);

            var lerpVal = (onTimeline.Time - data.MoveEndTime) / data.AttackData.AttackDuration;

            //agent.SetAnimationTime(onTimeline);

            agent.SetPosition(onTimeline, data.StartPos, finalPos, lerpVal);

            // commented these lines, because of bugs where weapons have not been activated yet at this point. (it was always the first attack of the enemy)
            // AttackLanded will be caused by Animation-Event anyway
            //if (agent.Data.State == TimelineState.Done)
            //    return;
            if (onTimeline.Time < data.AttackArrivesOnTime+1)
                return;

            Debug.LogError($"No attack landed by animation! cancel attack {agent.GameObject}");
            agent.Data.OnTimeline.UpdatePause();
            agent.AttackingStateEnd();
            agent.Data.State = TimelineState.AttackLanded;
        }


        static void UpdateAfterAttackLanded(this ITimelineAgent agent, CombatTimeline onTimeline)
        {
            agent.Get(out AttackControl attackControl);
            var attackDone = attackControl.AttackDone;
            if (!attackDone)
                return;
            // Debug.Log("Done");
            agent.SetToDone(onTimeline.Time, false);
        }

        static void SetPosition(this ITimelineAgent agent, CombatTimeline onTimeline, Vector3 startPos, Vector3 finalPos, float lerpVal)
        {
            var path = agent.GetPath(startPos, finalPos);
            if (path == null)
                return;
            if (InterpolateOnPath(agent, onTimeline, lerpVal, path, out var newPos, out var vec) == OperationResult.Error)
                return;

            agent.Get(out MoveControl moveControl);
            agent.Get(out RigidbodyBehaviour rigid);

            var currentPos = rigid.Position;
            var moveAmount = vec.sqrMagnitude;
            rigid.Position = (moveAmount > 1f ? Vector3.Lerp(currentPos, newPos, 1f / vec.magnitude) : newPos);

            var finalVec = finalPos - currentPos;
            if (finalVec.sqrMagnitude >= float.Epsilon)
                moveControl.SetDirectionInput(new Vector2(finalVec.x, finalVec.z).normalized);
            else
                moveControl.SetDirectionInput(finalVec);


            moveControl.SetMoveInput(Vector2.zero);
            moveControl.DoMove();
        }

        static OperationResult InterpolateOnPath(this ITimelineAgent agent, CombatTimeline onTimeline, 
            float lerpVal, IList<Vector3> path,
            out Vector3 pos, out Vector3 vec)
        {
            pos = Vector3.zero;
            vec = Vector3.zero;

            var idx = Mathf.Clamp01(lerpVal) * (path.Count - 1);
            var minIdx = Mathf.FloorToInt(idx);
            var maxIdx = Mathf.CeilToInt(idx);
            var pathLerp = idx - minIdx;

            if (!path.IsIndexInRange(minIdx) || !path.IsIndexInRange(maxIdx))
            {
                //Debug.Log($"{agent} No Path!");
                agent.SetToDone(onTimeline.Time, true);
                return OperationResult.Error;
            }

            agent.Get(out RigidbodyBehaviour rigid);
            //agent.Get(out Transform transform);
            var trPos = rigid.Position;

            pos = Vector3.Lerp(path[minIdx], path[maxIdx], pathLerp);
            pos.y = trPos.y;
            //navAgent.SetDestination(pos);
            vec = pos - trPos;

            //Debug.Log($"pathLerp: {pathLerp} pos: {pos}" +
            //          $"fromPos {path.corners[minIdx]} toPos {path.corners[maxIdx]}" +
            //          $"current pos {trPos} new pos {pos}");

            return OperationResult.OK;
        }

        static IList<Vector3> GetPath(this ITimelineAgent agent, Vector3 startPos, Vector3 finalPos)
        {
            if (agent is CombatTimeline timeline)
            {
                timeline.Path[0] = startPos;
                timeline.Path[1] = finalPos;
                return timeline.Path;
            }

            agent.Get(out NavMeshAgent navAgent);
            if (navAgent == null)
                return null;

            navAgent.nextPosition = startPos;
            var path = navAgent.path;

            if (navAgent.isOnNavMesh)
                navAgent.CalculatePath(finalPos, path);
            return path.corners;
        }

        static void StartAgentMoving(this ITimelineAgent agent, CombatTimeline onTimeline)
        {
            ref var data = ref agent.Data;
            agent.Get(out RigidbodyBehaviour rigid);
            //agent.Get(out Transform transform);
            data.StartPos = rigid.Position;
            data.State = TimelineState.Moving;
            //data.MoveStartTime = timeline.Time;

            // re-adjust times for the move
            agent.GetFullCombatMoveDuration(agent.Data.AttackData, data.Target, out var duration);
            agent.InitTimeline(onTimeline.Time + duration, duration);

            //var preAttackPos = agent.GetPreAttackPosition(data.Target.position);
            //var vec = preAttackPos - data.StartPos;
            //agent.Get(out MoveControl moveControl);
            //moveControl.SetDirectionInput(new Vector2(vec.x, vec.z).normalized);
            //Debug.Log($"{agent} On Timeline start!");
            agent.OnTimelineStarted();
        }

        static void StartAttack(this ITimelineAgent agent)
        {
            ref var data = ref agent.Data;

            agent.Get(out RigidbodyBehaviour rigid);
            //agent.Get(out Transform transform);
            agent.Get(out AttackControl attackControl);
            if (!attackControl.CanInvoke)
            {
                agent.SetToDone(data.OnTimeline.Time, true);
                return;
            }

            //agent.Get(out ControlCentre cc);
            agent.Get(out AnimatorMoveHandlerComponent animatorMoveHandler);

            //Debug.Log("Attack started");
            data.State = TimelineState.Attacking;
            //data.MoveEndTime = timeline.Time;

            animatorMoveHandler.DoMove = false;

            data.StartPos = rigid.Position;

            //var preAttackPos = agent.GetFinalPosition(data.Target.position);
            //var vec = preAttackPos - data.StartPos;
            //agent.Get(out MoveControl moveControl);
            //moveControl.SetDirectionInput(new Vector2(vec.x, vec.z).normalized);

            // cc.Animator.StartPlayback();
            //cc.Animator.speed = timeline.SpeedFactor;
            //attackControl.SpeedFactor = timeline.SpeedFactor;

            //Debug.Log($"{agent} triggers attack {data.MoveType}");

            attackControl.Trigger(data.MoveType);
            attackControl.IndicateAttack();
        }

        static void ForceHitTarget(this ITimelineAgent agent)
        {
            agent.Get(out AttackControl attackControl);
            agent.Get(out RigidbodyBehaviour rigid);
            var distance = Vector3.Distance(rigid.Position, agent.Data.Target.position);
            if (agent.Data.Target != null && agent.Data.AttackData.OnHitRange + 0.1f > distance)
                attackControl.ForceHitTarget(agent.Data.Target.gameObject);
        }

        static void AttackingStateEnd(this ITimelineAgent agent)
        {
            //agent.Get(out ControlCentre cc);
            //cc.Animator.speed = 1f;
            //cc.Animator.StopPlayback();
            //agent.Get(out AttackControl attackControl);
            //attackControl.SpeedFactor = 1f;

            agent.Get(out AnimatorMoveHandlerComponent animatorMoveHandler);
            animatorMoveHandler.DoMove = agent.Target == null || !agent.Target.IsStatic;
        }

        //public static float GetAnimationTime(this ITimelineAgent agent, float lastTime, float currentTime)
        //{
        //    ref var data = ref agent.Data;
        //    agent.Get(out IControlCentre cc);
        //    var animationState = cc.Animator.GetCurrentAnimatorStateInfo(1);
        //    var myAnimatorClip = cc.Animator.GetCurrentAnimatorClipInfo(1);
        //    if (myAnimatorClip.IsNullOrEmpty())
        //        return currentTime;
        //    var animTime = myAnimatorClip[0].clip.length * animationState.normalizedTime;
        //    var timeDictatedByAnimation = data.MoveEndTime + animTime;
        //    var compromisedTime = Mathf.Lerp(currentTime, timeDictatedByAnimation, 0.25f);
        //    return Mathf.Max(lastTime, compromisedTime);
        //}
    }
}
