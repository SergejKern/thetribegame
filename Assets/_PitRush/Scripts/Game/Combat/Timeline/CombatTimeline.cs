﻿using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Core.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Controls;
using Game.UI;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Energy;
using GameUtility.Events;
using UnityEngine;
using UnityEngine.Serialization;
using IDamageProtectionAction = GameUtility.Data.PhysicalConfrontation.IDamageProtectionAction;

namespace Game.Combat.Timeline
{
    public partial class CombatTimeline : FsmTimelineAgent,
        IEventListener<AttackDataChangedEvent>
    {
        #region Serialized Fields
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ConfigData m_initData;

        [FormerlySerializedAs("m_missTime")]
        [SerializeField] float m_missDuration;
        [SerializeField] float m_missingSecondsEnabledAfterTargetLoss=2f;
        [SerializeField] GameObject m_missEffect;
        //[SerializeField] DashControl m_dash;
#pragma warning restore 0649 // wrong warnings for SerializeField
        #endregion

        #region Properties
        //float ReactionTimeInSeconds => m_defaultReactionTime * m_speedFactor;

        // ReSharper disable ConvertToAutoPropertyWhenPossible
        public override float SpeedFactor => PenalizedSpeedFactor;
        public float UnpenalizedSpeedFactor =>
            OverrideSpeed >= 0
                ? OverrideSpeed
                : m_currentSpeed;

        float PenalizedSpeedFactor => UnpenalizedSpeedFactor 
                                     * m_latePenalty * m_missPenalty;

        float Delta { get; set; }
        public override float Time { get; protected set; }
        //public bool Active { get; private set; }

        public Vector3[] Path { get; } = new Vector3[2];
        public ITimelineAgent OwningAgent => this;
        public bool TargetIsDanger => Targets.Find(item => item.Priority == DangerTargetPriority).Target != null;
        // ReSharper restore ConvertToAutoPropertyWhenPossible

        float LastAttackArriveTime
        {
            get
            {
                if (m_attackQueue.IsNullOrEmpty())
                    return m_lastSupposedHitTime;
                var lastAgent = m_attackQueue[m_attackQueue.Count - 1];
                return lastAgent.Data.AttackArrivesOnTime;
            }
        }
        #endregion

        #region Fields
        readonly List<ITimelineAgent> m_attackQueue = new List<ITimelineAgent>();
        AttackData m_nextAttackData;
        float m_currentSpeed;

        float m_lastHitTime;
        float m_lastSupposedHitTime;
        float m_lastMissTime;

        public bool IsProtected => m_protectionActions.Any(a => a.IsProtected);


        float m_nextQueuePause;
        float m_nextAvailableTime;

        float m_latePenalty = 1f;
        float m_missPenalty = 1f;

        IDamageProtectionAction[] m_protectionActions;
        #endregion

        #region Public Methods

        public float GetSecondsUntilNextTimeForAttackHit(ITimelineAgent agent)
            => GetNextTimeForAttackHit(agent) - Time;
        public float GetNextTimeForAttackHit(ITimelineAgent agent, float timeNeeded =0f)
        {
            var ownTimeline = OwningAgent == agent;
            var nextAvailableTime = Mathf.Max(Time, OwningAgent.Data.AttackArrivesOnTime);

            if (!ownTimeline)
            {
                var needWait = !m_attackQueue.IsNullOrEmpty();
                if (needWait)
                    nextAvailableTime = Mathf.Max(nextAvailableTime, LastAttackArriveTime + m_nextQueuePause);

                nextAvailableTime = Mathf.Max(nextAvailableTime, m_nextAvailableTime);
            }

            var secondsUntilAvailableTime = nextAvailableTime - Time;

            //Debug.Log($"Time: {Time}\nAttackArrivesOnTime: { OwningAgent.Data.AttackArrivesOnTime}\npauseTime: {pauseTime}" +
            //          $"\nnextAvailableTime: {nextAvailableTime}\nsecondsUntilAvailableTime: {secondsUntilAvailableTime} m_lastHitTime{m_lastHitTime} pauseTime{pauseTime}");
            if (timeNeeded > secondsUntilAvailableTime)
                return Time + timeNeeded;
            return nextAvailableTime;
        }

        public void OnHit(ITimelineAgent aggressorAgent)
        {
            if (aggressorAgent == OwningAgent)
            {
                m_missPenalty = 1f;
                return;
            }
            if (IsProtected)
            {
                OnMiss(aggressorAgent);
                return;
            }

            m_lastSupposedHitTime = Time;
            m_lastHitTime = Time;
            UpdatePause();
        }

        public void OnMiss(ITimelineAgent aggressorAgent)
        {
            if (aggressorAgent == OwningAgent)
                return;
            

            m_lastSupposedHitTime = Time;
            UpdatePause();
        }

        public void RegisterAttack(ITimelineAgent agent)
        {
            if (OwningAgent == agent)
                return;
            if (m_attackQueue.Contains(agent))
                return;

            m_attackQueue.Add(agent);
            m_nextQueuePause = GetPause();
        }
        #endregion

        #region Unity Methods
        //void Start() => m_thisAgent = m_agentObject.GetComponent<ITimelineAgent>();
        protected override void OnEnable()
        {
            base.OnEnable();
            EventMessenger.AddListener<AttackDataChangedEvent>(this);

            Init();
        }
        protected override void OnDisable()
        {
            base.OnDisable();
            EventMessenger.RemoveListener<AttackDataChangedEvent>(this);
        }

        void FixedUpdate() => UpdateTimeline(UnityEngine.Time.fixedDeltaTime);
        #endregion

        #region Update
        protected override void Update()
        {
            base.Update();
            UpdateMissPenalty();
            DrawTimelineGUI();
        }

        public void UpdatePause()
        {
            var nextTime = Time + GetPause();
            m_nextAvailableTime = Mathf.Max(m_nextAvailableTime, nextTime);
        }

        void UpdateTimeline(float delta)
        {
            UpdateTime(delta);

            //Active = OwningAgent.IsMovedViaTimeline();
            UpdateThisAgent();
            for (var i = m_attackQueue.Count - 1; i >= 0; i--)
            {
                var agent = m_attackQueue[i];
                //Active |= agent.IsMovedViaTimeline();

                agent.UpdateOnTimeline(this);
                if (agent.Data.State == TimelineState.Done)
                    m_attackQueue.RemoveAt(i);
            }
        }

        void UpdateTime(float delta)
        {
            UpdateSpeedFactor(delta);
            Delta = delta * UnpenalizedSpeedFactor;
            //var lastTime = Time;
            Time += Delta;
            //var attackAgent = GetNextAgentWithState(TimelineState.Attacking);
            //if (attackAgent!= null)
            //    Time = attackAgent.GetAnimationTime(lastTime, Time);
            //else Debug.Log(Time);
        }

        void UpdateSpeedFactor(float delta)
        {
            //Debug.Log("Updating speed");
            var time = Time + delta;
            var timeUntilHit = GetTimeUntilNextHit(time);
            if (IsProtectedForSeconds(timeUntilHit))
            {
                m_currentSpeed = 1f;
                return;
            }

            m_latePenalty =
                (OwningAgent.IsMovedViaTimeline() && (OwningAgent.Data.AttackArrivesOnTime - time) > timeUntilHit)
                    ? LateAttack : 1f;

            // if (OwningAgent.Data.AttackArrivesOnTime > Time && OwningAgent.Data.AttackArrivesOnTime < timeUntilHit)
            //    timeUntilHit = OwningAgent.Data.AttackArrivesOnTime;
            var timeSinceHit = time - m_lastHitTime;
            var speedLerp = 1f;
            if (timeUntilHit < SlowDownBeforeHitSeconds)
            {
                var lerpVal = (SlowDownBeforeHitSeconds - timeUntilHit) / SlowDownBeforeHitSeconds;
                speedLerp *= SlowDownBeforeHitCurve.Evaluate(lerpVal);
            }

            if (timeSinceHit < NormalizeAfterHitSeconds)
            {
                var lerpVal = timeSinceHit / NormalizeAfterHitSeconds;
                speedLerp *= NormalizeAfterHitCurve.Evaluate(lerpVal);
            }
            m_currentSpeed = Mathf.Lerp(SlowDownTimeFactor, 1.0f, speedLerp);
        }

        void UpdateThisAgent()
        {
            UpdateDangerTarget();

            OwningAgent.UpdateOnTimeline(this);
        }

        void UpdateDangerTarget()
        {
            var dangerAgent = GetNextIncomingAgentWithState(TimelineState.Attacking, TimelineState.Moving);
            if (dangerAgent == null)
            {
                //Debug.Log($"block cleared because no danger target");
                ClearTargetPriority(DangerTargetPriority);
                return;
            }

            var timeUntilHit = dangerAgent.Data.AttackArrivesOnTime - Time;// GetTimeUntilNextHit();
            //if (Protected && timeUntilHit > 0f)
            //    m_evade.EvasionTimeFix(timeUntilHit);

            this.GetFullCombatMoveDuration(m_nextAttackData, dangerAgent.Transform, out var timeForHittingTarget);
            var enoughTime = timeUntilHit > timeForHittingTarget;
            if (!enoughTime)
            {
                if (Target != dangerAgent)
                    DangerTargetUpdatedEvent.Trigger(m_controlCentre.ControlledObject, 
                        dangerAgent.GameObject, dangerAgent.Data.AttackArrivesOnTime);

                SetTargetWithPriority(dangerAgent, DangerTargetPriority);
            }
            else ClearTargetPriority(DangerTargetPriority);
        }

        ITimelineAgent GetNextAgentWithState(params TimelineState[] states)
        {
            var incoming = GetNextIncomingAgentWithState(states);
            if (!OwningAgent.Data.State.IsAny(states))
                return incoming;
            if (incoming == null)
                return OwningAgent;
            return incoming.Data.AttackArrivesOnTime < OwningAgent.Data.AttackArrivesOnTime ? incoming : OwningAgent;
        }

        ITimelineAgent GetNextIncomingAgentWithState(params TimelineState[] states) 
            => m_attackQueue.IsNullOrEmpty() 
                ? null 
                : m_attackQueue.FirstOrDefault(agent => agent.Data.State.IsAny(states));

        //    if (m_attackQueue.IsNullOrEmpty()) return null;
        // foreach (var agent in m_attackQueue) {
        //    if (!agent.Data.State.IsAny(states))
        //        continue;
        //    return agent; } return null;

        float GetTimeUntilNextHit(float currentTime)
        {
            if (m_attackQueue.IsNullOrEmpty())
                return float.MaxValue;

            foreach (var agent in m_attackQueue)
            {
                if (!agent.Data.OnTrack)
                    continue;
                if (agent.Data.AttackArrivesOnTime < currentTime)
                    continue;
                return agent.Data.AttackArrivesOnTime - currentTime;
            }
            return float.MaxValue;
        }
        #endregion

        public void OnEvent(AttackDataChangedEvent @event)
        {
            if (@event.Originator != gameObject)
                return;
            var attDataProv = @event.AttackDataProvider;
            m_nextAttackData = attDataProv.GetNextAttackData;
        }

        void OnRecentTargetMiss()
        {
            if (Time - LastTargetSetTime > m_missingSecondsEnabledAfterTargetLoss)
                return;

            m_lastMissTime = Time;
            m_missPenalty = MissPenalty;
            var effectInstance = m_missEffect.GetPooledInstance(transform.position+3*Vector3.up, Quaternion.identity);
            effectInstance.TryGetComponent(out TextUI textUI);
            textUI.SetText("Miss!");
        }

        void UpdateMissPenalty() => m_missPenalty = Time - m_lastMissTime > m_missDuration ? 1f : MissPenalty;

        public void CheckIfMissed(IEnumerable<ICollisionMaterialComponent> hitComponents)
        {
            var hitStuff= hitComponents.Count(p => p is IDamageHandler) > 0;

            Get(out AttackControl attackControl);
            if (hitStuff)
                return;
            if (LastTarget is ITimelineAgent ta)
            {
                ta.Get(out IHealthComponent hc);
                if (hc.HP <= 0f)
                    return;
            }
            if (LastTarget != null && attackControl.Released)
                OnRecentTargetMiss();
        }

        bool IsProtectedForSeconds(float sec)
            => m_protectionActions.Any(a => a.IsProtectedForSeconds(sec));

        public void OnSkillChanged(float skill)
        {
            m_skillValue = skill;
            UpdateDifficulty();
        }
    }
}

//foreach (var inRange in m_targetFilter.InRangeTargets)
//{
//    if(_!(inRange is ITimelineAgent timelineAgent))
//        continue;
//    var isAttackingThisAgent = false;
//    if (timelineAgent.Target == m_thisTarget)
//    {
//        var nextAttack = timelineAgent.AttackArrivesInSeconds;
//        isAttackingThisAgent = (nextAttack > 0f);
//    }
//    // ui shit...
//    if (isAttackingThisAgent)
//    {
//    }
//    else
//    {
//    }
//}