﻿using Game.Actors.Controls;
using Game.Actors.Energy;
using UnityEngine;

namespace Game.Combat.Timeline
{
    public class TimelineTargetVisualizer : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        //[SerializeField] GameObject m_actor;
        //[SerializeField] InRangeTargetFilter m_filter;
        [SerializeField] HealthComponent m_health;
        [SerializeField] CombatTimeline m_timeline;

        [SerializeField] LineRenderer m_lineRenderer;
        [SerializeField] Material m_normalLine;
        [SerializeField] Material m_dangerLine;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_timeForHittingTarget;
        const float k_aboveFloor = 0.1f;
        readonly Vector3[] m_positions = new Vector3[2];

        void Update()
        {
            m_timeline.Get(out AttackControl att);

            var attacking = !att.AttackDone || !att.AttackReady;
            var target = m_timeline.Target;
            var targetIsDanger = m_timeline.TargetIsDanger;
            var attackingViaTimeline = attacking && m_timeline.Data.IsMovedViaTimeline;
            var showing = targetIsDanger 
                          || attackingViaTimeline 
                          || !attacking;

            m_lineRenderer.enabled = target != null && m_health.HP >= 0f && showing;

            if (!showing)
                return;
            if (target == null)
                return;

            var pos = transform.position;
            if (targetIsDanger && target is ITimelineAgent tlAgent)
                pos = tlAgent.Data.CurrentTargetPos;

            m_positions[0] = pos + Vector3.up * k_aboveFloor;
            m_positions[1] = target.Transform.position + Vector3.up * k_aboveFloor;
            m_lineRenderer.SetPositions(m_positions);

            m_lineRenderer.sharedMaterial = targetIsDanger ? m_dangerLine : m_normalLine;
        }
    }
}
