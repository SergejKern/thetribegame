using Core.Events;
using UnityEngine;

namespace Game.Combat
{
    public struct AvoidingDamageActionEvent
    {
        public enum ActionCategory
        {
            Dash,
            Evade,
            Block
        }
        public readonly GameObject AvoidingActor;
        // ReSharper disable once MemberCanBePrivate.Global, NotAccessedField.Global
        public readonly ActionCategory ActionType;
        AvoidingDamageActionEvent(GameObject avoidingActor, ActionCategory type)
        {
            AvoidingActor = avoidingActor;
            ActionType = type;
        }

        public static void Trigger(GameObject avoidingActor,  ActionCategory type)
            => Trigger(new AvoidingDamageActionEvent(avoidingActor, type));
        static void Trigger(AvoidingDamageActionEvent e) => EventMessenger.TriggerEvent(e);
    }
}
