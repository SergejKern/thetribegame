using System;
using System.Collections;
using Core.Unity.Utility.PoolAttendant;
using Game.Configs;
using Game.Utility;
using Game.Utility.VisualAndFX;
using GameUtility;
using GameUtility.Config;
using GameUtility.Data.TransformAttachment;
using GameUtility.Feature.Loading;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Game.Globals
{
    public class GameSetup : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        struct LoadSceneSetting
        {
            public SceneID SceneID;
            public bool Load;
            public bool Activate;
        }

        [SerializeField]
        internal ConfigLinks m_configLinks;

        [SerializeField] bool m_hudInitialActive;
        [SerializeField] LoadSceneSetting[] m_scenes;
        [SerializeField] LoadOperationID m_setupLoad;

        public UnityEvent SceneSetup;
        [FormerlySerializedAs("OnGameSetup")] 
        public UnityEvent OnGameSetupFromThisScene;
#pragma warning restore 0649

        // readonly List<IControllable> m_spawnedControllables = new List<IControllable>();

        internal SceneSettings m_currentSceneSettings;
        public PrefabLinks Links => m_configLinks.GetPrefabLinks();

        static bool HasGameBeenSetup => GameGlobals.GameSetup != null;

        // Start is called before the first frame update
        void OnEnable()
        {
            // weird bug if we SetupGame immediately, where when we immediately join,
            // GameCamera-targets will be reset next frame and won't follow player
            if (HasGameBeenSetup)
            {
                SceneSetup.Invoke();
                Destroy(this, 0.1f);
            }
            else StartCoroutine(SetupGame());
        }

        void OnDisable()
        {
            if (GameGlobals.GameSetup != this)
                return;

            if (ConfigLinks.SpawnInitConfig)
                ConfigLinks.SpawnInitConfig.StopListen();
            GameGlobals.Reset();
        }

        IEnumerator SetupGame()
        {
            //var o = gameObject;
            //Debug.Log($"SetupGame: scene {o.scene.name} loaded {o.scene.isLoaded}");

            GameGlobals.ConfigLinks = m_configLinks;
            GameGlobals.Prefabs = m_configLinks.GetPrefabLinks();

            if (LoadingCanvas.Instance == null)
                Instantiate(GameGlobals.Prefabs.GetPrefab(Prefab.LoadingCanvas))
                    .TryGetComponent(out LoadingCanvas.Instance);

            LoadOperations.StartLoading(m_setupLoad);
            yield return null;

            GameGlobals.GroundPlaneY = transform.position.y;
            GameGlobals.RuntimeScene = SceneManager.CreateScene("Runtime");
            SceneManager.SetActiveScene(GameGlobals.RuntimeScene);
            var thisGo = gameObject;
            thisGo.transform.SetParent(null);
            //SceneManager.MoveGameObjectToScene(thisGo, GameGlobals.RuntimeScene);
            GameGlobals.GameSetup = this;

            GameGlobals.SceneLinks = m_configLinks.GetSceneLinks();
            DebugDrawConfig.Instance = m_configLinks.GetDebugDrawConfig();

            if (Application.isEditor)
            {
                GameGlobals.GO_SceneGUI = Instantiate(GameGlobals.Prefabs.GetPrefab(Prefab.SceneGUI));
                GameGlobals.SceneGUI.AddSceneGUI(GameGlobals.ConfigLinks.DrawUsedConfigsGUI);
            }

            if (ConfigLinks.SpawnInitConfig)
                ConfigLinks.SpawnInitConfig.StartListen();

            GameGlobals.GOAudio = Instantiate(PrefabLinks.I.GetPrefab(Prefab.AudioSetup), Vector3.zero, Quaternion.identity);

            yield return null;
            var cameraPos = transform;

            // todo 3: Make sure always correct SceneSettings available
            m_currentSceneSettings = FindObjectOfType<SceneSettings>();
            if (m_currentSceneSettings == null)
                Debug.LogWarning("No SceneSettings have been found!");
            else
                cameraPos = m_currentSceneSettings.CameraSetupPos;

            var camSetupPos = cameraPos.position;
            GameGlobals.GOGameCamera = Instantiate(PrefabLinks.I.GetPrefab(Prefab.CameraSetup), camSetupPos, cameraPos.rotation);
            if (GameGlobals.SpotLightDirector != null)
                GameGlobals.SpotLightDirector.Init( GameGlobals.GameCamera.GetComponent<AttachmentPointsComponent>());

            GameGlobals.GameCamera.SetupCameraAtAnchor(camSetupPos);

            GameGlobals.GOSceneLights = Instantiate(PrefabLinks.I.GetPrefab(Prefab.LightSetup), Vector3.zero, Quaternion.identity);
            GameGlobals.GO_UICanvas = Instantiate(PrefabLinks.I.GetPrefab(Prefab.UICanvas), Vector3.zero, Quaternion.identity);
            GameGlobals.GO_UICanvas.SetActive(m_hudInitialActive);

            GameGlobals.GOPlayerInputManager = Instantiate(PrefabLinks.I.GetPrefab(Prefab.PlayerInputManager), Vector3.zero, Quaternion.identity);

            if (GameGlobals.SceneLights!= null && m_currentSceneSettings != null)
                GameGlobals.SceneLights.Apply(m_currentSceneSettings.m_lightSettings);

            yield return null;
            Pool.Instance.CreateDefaultItems();

            yield return StartCoroutine(LoadScenes());

            SceneSetup.Invoke();
            OnGameSetupFromThisScene.Invoke();
        }

        IEnumerator LoadScenes()
        {
            foreach (var sc in m_scenes)
            {
                if (sc.Load)
                    yield return StartCoroutine(LoadAsync(SceneLinks.I.GetScene(sc.SceneID)));
            }
            foreach (var sc in m_scenes)
            {
                if (sc.Load)
                    Setup(sc.SceneID, sc.Activate);
            }

            yield return null;
            LoadOperations.LoadingDone(m_setupLoad);
        }

        static IEnumerator LoadAsync(string sceneName)
        {
            //Debug.Log($"LoadScenes: scene {sceneName} loaded? {SceneManager.GetSceneByName(sceneName).isLoaded}");

            if (SceneManager.GetSceneByName(sceneName).isLoaded)
                yield break;
            var asyncScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            while (!asyncScene.isDone) yield return null;
            yield return null;
        }

        static void Setup(SceneID sceneID, bool activated)
        {
            var sceneRef = SceneLinks.I.GetScene(sceneID);

            var scene = SceneManager.GetSceneByName(sceneRef);
            SceneRoot.GetSceneRoot(scene, out var sceneRoot);
            GameGlobals.SetScene(sceneID, sceneRoot);

            //Debug.Log($"{sceneID} {activated}");
            sceneRoot.InitActive(activated);
        }

        //internal void CheckLoseCondition()
        //{
        //    AudioBank.Stop();
        //
        //    // release controls
        //    foreach (var controllable in m_spawnedControllables)
        //        controllable.RemoveControl();
        //
        //    StartCoroutine(LoadScene());
        //}

        //IEnumerator LoadScene()
        //{
        //    if (m_settings == null)
        //    {
        //        Debug.LogError("Cannot load GameOver-scene, because no SceneSettings have been found!");
        //        yield break;
        //    }
        //    //!need to delay loading scene, or controls will not be released properly -> no controls next time
        //    yield return new WaitForSeconds(1.0f);
        //    UnityEngine.SceneManagement.SceneManager.LoadScene(m_settings.MainScene);
        //}
    }
}