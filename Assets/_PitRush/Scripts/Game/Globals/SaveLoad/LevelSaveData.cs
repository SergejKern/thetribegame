﻿using System;
using Game.Arena.Level;

namespace Game.SaveLoad
{
    [Serializable]
    public class LevelSaveData
    {
        public int[] HighScores;
        public float[] BestTimes;

        public LevelSaveData(GameLevelsConfig levelsConfig)
        {
            HighScores = new int[levelsConfig.LevelCount];
            BestTimes = new float[levelsConfig.LevelCount];
            for (var i = 0; i < levelsConfig.LevelCount; i++) 
                BestTimes[i] = float.MaxValue;
        }
    }
}
