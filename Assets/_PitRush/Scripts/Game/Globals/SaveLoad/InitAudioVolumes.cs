﻿using UnityEngine;

namespace Game.SaveLoad
{
    public class InitAudioVolumes : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AK.Wwise.RTPC m_musicRtpc;
        [SerializeField] AK.Wwise.RTPC m_sfxRtpc;
        [SerializeField] AK.Wwise.RTPC m_uiRtpc;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // Start is called before the first frame update
        void Awake()
        {
            AudioVolumePrefs.Load(out var volumes);
            AudioVolumePrefs.ApplyVolumes(volumes, m_musicRtpc, m_sfxRtpc, m_uiRtpc);
        }
    }
}
