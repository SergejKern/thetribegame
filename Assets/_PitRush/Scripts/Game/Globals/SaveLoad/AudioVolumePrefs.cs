﻿using UnityEngine;

namespace Game.SaveLoad
{
    public static class AudioVolumePrefs
    {
        const string k_masterVolumePrefKey = "MasterVolume";
        const string k_musicVolumePrefKey = "MusicVolume";
        const string k_sfxVolumePrefKey = "SFXVolume";

        public struct AudioVolumes
        {
            public float Master;
            public float Music;
            public float Sfx;
        }

        public static void Save(AudioVolumes volumes)
        {
            PlayerPrefs.SetFloat(k_masterVolumePrefKey, volumes.Master);
            PlayerPrefs.SetFloat(k_musicVolumePrefKey, volumes.Music);
            PlayerPrefs.SetFloat(k_sfxVolumePrefKey, volumes.Sfx);
        }

        public static void Load(out AudioVolumes volumes)
        {
            volumes.Master = PlayerPrefs.GetFloat(k_masterVolumePrefKey, 1f);
            volumes.Music = PlayerPrefs.GetFloat(k_musicVolumePrefKey, 1f);
            volumes.Sfx = PlayerPrefs.GetFloat(k_sfxVolumePrefKey, 1f);
        }

        public static void ApplyVolumes(AudioVolumes volumes,
            AK.Wwise.RTPC music, 
            AK.Wwise.RTPC sound, 
            AK.Wwise.RTPC ui)
        {
            music.SetGlobalValue(volumes.Master * volumes.Music * 100f);
            sound.SetGlobalValue(volumes.Master * volumes.Sfx * 100f);
            ui.SetGlobalValue(volumes.Master * 100f);
        }
    }
}
