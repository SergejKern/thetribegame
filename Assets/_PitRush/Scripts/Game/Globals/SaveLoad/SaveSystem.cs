﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Core.Extensions;
using Game.Arena.Level;
using Game.Configs;
using UnityEngine;

namespace Game.SaveLoad
{
    public static class SaveSystem
    {
        static LevelSaveData m_levelSaveData;
        static string LevelsPath => $"{Application.persistentDataPath}/levels.pit";

        public static void SaveLevelData(ArenaLevelFlowConfig level, int highScore, float bestTime)
        {
            if (m_levelSaveData == null) 
                m_levelSaveData = new LevelSaveData(ConfigLinks.LevelsConfig);

            var idx = ConfigLinks.LevelsConfig.GetLevelIdx(level);
            if (!idx.IsInRange(m_levelSaveData.HighScores))
                return;

            m_levelSaveData.HighScores[idx] = highScore;
            m_levelSaveData.BestTimes[idx] = bestTime;

            var formatter = new BinaryFormatter();
            var stream = new FileStream(LevelsPath, FileMode.Create);

            formatter.Serialize(stream, m_levelSaveData);
            stream.Close();
        }


        public static void LoadLevelData()
        {
            if (!File.Exists(LevelsPath))
                return;
            var formatter = new BinaryFormatter();
            var stream =new FileStream(LevelsPath, FileMode.Open);

            m_levelSaveData = (LevelSaveData) formatter.Deserialize(stream);
            stream.Close();

            FixLevelSaveData();
        }

        static void FixLevelSaveData()
        {
            if (ConfigLinks.LevelsConfig == null)
                return;
            if (m_levelSaveData.BestTimes == null)
                m_levelSaveData.BestTimes = new float [ConfigLinks.LevelsConfig.LevelCount];
            if (m_levelSaveData.HighScores == null)
                m_levelSaveData.HighScores = new int [ConfigLinks.LevelsConfig.LevelCount];
            if (m_levelSaveData.BestTimes.Length != ConfigLinks.LevelsConfig.LevelCount)
                Array.Resize(ref m_levelSaveData.BestTimes, ConfigLinks.LevelsConfig.LevelCount);
            if (m_levelSaveData.HighScores.Length != ConfigLinks.LevelsConfig.LevelCount)
                Array.Resize(ref m_levelSaveData.HighScores, ConfigLinks.LevelsConfig.LevelCount);
        }

        public static void GetScores(ArenaLevelFlowConfig lvl, out int highScore, out float bestTime)
        {
            highScore = 0;
            bestTime = float.MaxValue;
            var idx = ConfigLinks.LevelsConfig.GetLevelIdx(lvl);
            if (m_levelSaveData == null)
                return;
            if (!idx.IsInRange(m_levelSaveData.HighScores))
                return;
            highScore = m_levelSaveData.HighScores[idx];
            bestTime = m_levelSaveData.BestTimes[idx];
        }
    }
}
