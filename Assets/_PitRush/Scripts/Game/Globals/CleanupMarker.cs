

using UnityEngine;

namespace Game.Globals
{
    /// <inheritdoc />
    /// <summary>
    /// Used for Cleaning stuff up, that is only used in Editor context
    /// </summary>
    public class CleanupMarker : MonoBehaviour
    {
    }
}