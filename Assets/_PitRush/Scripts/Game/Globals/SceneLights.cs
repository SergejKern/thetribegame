﻿using System;
using UnityEngine;

namespace Game.Globals
{
    public class SceneLights : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Light m_mainLight;
        [SerializeField] Light m_rimLight;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void Apply(SceneLightSettings sceneLights)
        {
            m_mainLight.color = sceneLights.m_mainLightColor;
            m_rimLight.color = sceneLights.m_rimLightColor;
        }

        public void LerpSettings(SceneLightSettings from, SceneLightSettings to, float lerpValue)
        {
            m_mainLight.color = Color.Lerp(from.m_mainLightColor, to.m_mainLightColor, lerpValue);
            m_rimLight.color = Color.Lerp(from.m_rimLightColor, to.m_rimLightColor, lerpValue);
        }
    }

    [Serializable]
    public struct SceneLightSettings
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] internal Color m_mainLightColor;
        [SerializeField] internal Color m_rimLightColor;
#pragma warning restore 0649 // wrong warnings for SerializeField
    }
}
