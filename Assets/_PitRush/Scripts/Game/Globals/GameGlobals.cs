using System.Collections.Generic;
using Core.Unity.Extensions;
using Game.Actors;
using Game.Arena;
using Game.Arena.Level;
using Game.Configs;
using Game.Utility;
using Game.Utility.VisualAndFX;
using GameUtility;
using GameUtility.Data.TransformAttachment;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Game.Globals
{
    public static class GameGlobals
    {
#region Need Initialization

        internal static GameSetup GameSetup;

        internal static PrefabLinks Prefabs
        {
            get => PrefabLinks.Instance;
            set => PrefabLinks.Instance = value;
        }

        public static SceneLinks SceneLinks 
        {
            get => SceneLinks.Instance;
            set => SceneLinks.Instance = value;
        }

        internal static ConfigLinks ConfigLinks
        {
            get => ConfigLinks.Instance;
            set => ConfigLinks.Instance = value;
        }

        #region GameObject Initialization-Properties
        public static GameObject GOGameCamera
        {
            get => m_goGameCamera;
            set
            {
                if (m_goGameCamera != null)
                    m_goGameCamera.DestroyEx();

                m_goGameCamera = value;
                GameCamera = null;
                if (m_goGameCamera == null)
                    return;

                GameCamera = m_goGameCamera.GetComponentInChildren<GameCamera>();
                SpotLightDirector = m_goGameCamera.GetComponentInChildren<SpotLightDirector>();
            }
        }
        public static GameObject GOSceneLights
        {
            get => m_goSceneLights;
            set
            {
                if (m_goSceneLights != null)
                    m_goSceneLights.DestroyEx();

                m_goSceneLights = value;
                SceneLights = null;
                if (m_goSceneLights == null)
                    return;

                SceneLights = m_goSceneLights.GetComponent<SceneLights>();
            }
        }
        public static GameObject GOAudio
        {
            get => m_goAudio;
            set
            {
                if (m_goAudio != null)
                    m_goAudio.DestroyEx();

                m_goAudio = value;
            }
        }

        public static GameObject GOPlayerInputManager
        {
            get => m_goPlayerInputManager;
            set
            {
                if (m_goPlayerInputManager != null)
                    m_goPlayerInputManager.DestroyEx();

                m_goPlayerInputManager = value;
                PlayerInputManager = null;
                if (m_goPlayerInputManager == null)
                    return;

                PlayerInputManager = m_goPlayerInputManager.GetComponent<PlayerInputManager>();
            }
        }


        public static GameObject GO_UICanvas
        {
            get => m_goUICanvas;
            set
            {
                if (m_goUICanvas != null)
                    m_goUICanvas.DestroyEx();

                m_goUICanvas = value;
                //Canvas = null;
                //if (m_goUICanvas == null)
                //    return;

                //Canvas = m_goUICanvas.GetComponent<Canvas>();
            }
        }

        public static GameObject GO_SceneGUI
        {
            get => m_goSceneGUI;
            set
            {
                if (m_goSceneGUI != null)
                    m_goSceneGUI.DestroyEx();

                m_goSceneGUI = value;
                SceneGUI = null;
                if (m_goSceneGUI == null)
                    return;

                SceneGUI = m_goSceneGUI.GetComponent<SceneGUI>();
            }
        }
        #endregion
        #endregion

        public static bool IsTutorial;

        public static int PlayerCount { get; private set; }
        // can have null entries
        public static List<Player> Players { get; } = new List<Player>();
        public static GameCamera GameCamera { get; private set; }
        public static SpotLightDirector SpotLightDirector { get; private set; }
        public static SceneLights SceneLights { get; private set; }
        public static PlayerInputManager PlayerInputManager { get; private set; }
        public static SceneGUI SceneGUI { get; private set; }
        public static SceneSettings CurrentSettings => GameSetup.m_currentSceneSettings;
        public static Scene RuntimeScene { get; set; }

        public static readonly Dictionary<SceneID, SceneRoot> Scenes = new Dictionary<SceneID, SceneRoot>();

        // todo 3: move to level related globals?
        public static ArenaLevelFlowBehaviour CurrentLevelFlow { get; set; }
        public static Elevator.Elevator Elevator { get; set; }
        public static ArenaPlatform SafePlatform;

        public static Vector3 SafePosition
        {
            get
            {
                if (SafePlatform != null) 
                    return SafePlatform.OnPlatformPosition;
                Debug.LogError("SafePlatform is not set!");
                return default;
            }
        }

        public static bool PVP;

        public static float GroundPlaneY;

        static GameObject m_goAudio;
        static GameObject m_goGameCamera;
        static GameObject m_goSceneLights;
        static GameObject m_goPlayerInputManager;
        static GameObject m_goAudioManager;
        static GameObject m_goUICanvas;
        static GameObject m_goSceneGUI;

        public static void Reset()
        {
            if (PlayerInputManager != null)
                PlayerInputManager.playerJoinedEvent.RemoveAllListeners();

            GameSetup = null;
            Prefabs = null;
            ConfigLinks = null;

            GOGameCamera = null;
            GOSceneLights = null;
            GOAudio = null;

            GOPlayerInputManager = null;
            GO_UICanvas = null;
            GO_SceneGUI = null;
        }

        public static void UpdatePlayerCount()
        {
            var playerCount = 0;
            foreach (var player in Players)
            {
                if (player == null)
                    continue;

                player.SetPlayerIdx(playerCount);
                playerCount++;
            }

            PlayerCount = playerCount;
        }

        public static void RemoveAllInput()
        {
            foreach (var p in Players)
            {
                if (p == null)
                    continue;

                var centre = p.Centre;
                var input = centre.PlayerInput;
                p.RemoveControl();
                input.gameObject.DestroyEx();
            }
            GOPlayerInputManager.DestroyEx();
            GOPlayerInputManager = null;
        }

        public static void SetScene(SceneID sceneID, SceneRoot sceneRoot)
        {
            if (Scenes.ContainsKey(sceneID))
                Scenes[sceneID] = sceneRoot;
            else 
                Scenes.Add(sceneID, sceneRoot);
        }
    }
}
