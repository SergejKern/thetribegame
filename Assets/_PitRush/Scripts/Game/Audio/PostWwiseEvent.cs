﻿using JetBrains.Annotations;
using UnityEngine;

namespace Game.Audio
{
    public class PostWwiseEvent : MonoBehaviour
    {
        [Header("Play_Tutorial")]
        public AK.Wwise.Event Press_Button_A;

        [UsedImplicitly]
        public void PressA() 
            => Press_Button_A.Post(gameObject);

        public AK.Wwise.Event Press_Button_X;
        [UsedImplicitly]
        public void PressX() 
            => Press_Button_X.Post(gameObject);

        public AK.Wwise.Event Press_Button_LB;
        [UsedImplicitly]
        public void PressLB() 
            => Press_Button_LB.Post(gameObject);

        public AK.Wwise.Event Press_Button_RT;
        [UsedImplicitly]
        public void PressRT() => Press_Button_RT.Post(gameObject);

        public AK.Wwise.Event Hold_Button_A;
        [UsedImplicitly]
        public void HoldA() => Hold_Button_A.Post(gameObject);

        public AK.Wwise.Event Hold_Button_LT;
        [UsedImplicitly]
        public void HoldLT() => Hold_Button_LT.Post(gameObject);

        [Header("Play_HUD")]
        public AK.Wwise.Event NoDash_Feedback;
        [UsedImplicitly]
        public void NoDash() => NoDash_Feedback.Post(gameObject);

        public AK.Wwise.Event Load_Dash;
        [UsedImplicitly]
        public void LoadDash() => Load_Dash.Post(gameObject);

        public AK.Wwise.Event Health_Full;
        [UsedImplicitly]
        public void HealthFull() => Health_Full.Post(gameObject);

        public AK.Wwise.Event Low_Health;
        [UsedImplicitly]
        public void LowHealth() => Low_Health.Post(gameObject);

        public AK.Wwise.Event Lose_Health;
        [UsedImplicitly]
        public void LoseHealth() => Lose_Health.Post(gameObject);

        public AK.Wwise.Event Stop_LowHealth;
        [UsedImplicitly]
        public void StopLowHealth() => Stop_LowHealth.Post(gameObject);

        public AK.Wwise.Event Health_Activate;
        [UsedImplicitly]
        public void HealthActivate() => Health_Activate.Post(gameObject);

        [Header("Play_Player_Shared")]
        public AK.Wwise.Event Dash;
        [UsedImplicitly]
        public void PlayDash() => Dash.Post(gameObject);

        public AK.Wwise.Event DashEnergyTrail;
        [UsedImplicitly]
        public void PlayDashTrail() => DashEnergyTrail.Post(gameObject);

        public AK.Wwise.Event FootstepsMetal;
        [UsedImplicitly]
        public void PlaySharedFootstepsMetal() => FootstepsMetal.Post(gameObject);

        public AK.Wwise.Event SwordBlock;
        [UsedImplicitly]
        public void PlaySharedSwordBlock() => SwordBlock.Post(gameObject);

        public AK.Wwise.Event PlayerDying;
        [UsedImplicitly]
        public void PlayPlayerDying() => PlayerDying.Post(gameObject);

        public AK.Wwise.Event PlayerDeath;
        [UsedImplicitly]
        public void PlayPlayerDeath() => PlayerDeath.Post(gameObject);

        public AK.Wwise.Event WeaponImpact;
        [UsedImplicitly]
        public void WeaponImpactType() => WeaponImpact.Post(gameObject);

        [Header("Play_Player01")]
        public AK.Wwise.Event P01TakeDamage;
        [UsedImplicitly]
        public void PlayPlayer01TakeDamage() => P01TakeDamage.Post(gameObject);

        [Header("Play_Weapon_Shared")]
        public AK.Wwise.Event FastWeaponSwingShort;
        [UsedImplicitly]
        public void FWSwingShort() => FastWeaponSwingShort.Post(gameObject);

        public AK.Wwise.Event FastWeaponSwingLong;
        [UsedImplicitly]
        public void FWSwingLong() => FastWeaponSwingLong.Post(gameObject);

        public AK.Wwise.Event FastWeaponThrow;
        [UsedImplicitly]
        public void FWThrow() => FastWeaponThrow.Post(gameObject);

        [Header("Play_Enemies_BigRobot")]
        public AK.Wwise.Event Gunshot;
        [UsedImplicitly]
        public void RobotShoot() => Gunshot.Post(gameObject);

        public AK.Wwise.Event BRTakeDamage;
        [UsedImplicitly]
        public void BigRobotTakeDamage() => BRTakeDamage.Post(gameObject);

        public AK.Wwise.Event Footsteps;

        public void BigRobotFootsteps() => Footsteps.Post(gameObject);

        public AK.Wwise.Event ArmMoveDown;
        [UsedImplicitly]
        public void BRArmDown() => ArmMoveDown.Post(gameObject);

        public AK.Wwise.Event ArmMoveUp;
        [UsedImplicitly]
        public void BRArmUp() => ArmMoveUp.Post(gameObject);

        public AK.Wwise.Event MovementWalk;
        [UsedImplicitly]
        public void BRWalk() => MovementWalk.Post(gameObject);

        public AK.Wwise.Event JumpAttackStart;
        [UsedImplicitly]
        public void BRJumpStart() => JumpAttackStart.Post(gameObject);

        public AK.Wwise.Event JumpAttackEnd;
        [UsedImplicitly]
        public void BRJumpEnd() => JumpAttackEnd.Post(gameObject);

        public AK.Wwise.Event VoicesDamage;
        [UsedImplicitly]
        public void BRVoicesDamage() => VoicesDamage.Post(gameObject);

        public AK.Wwise.Event VoicesDestroy;
        [UsedImplicitly]
        public void BRVoicesDestroy() => VoicesDestroy.Post(gameObject);

        public AK.Wwise.Event VoicesIgnition;
        [UsedImplicitly]
        public void BRVoicesIgnition() => VoicesIgnition.Post(gameObject);

        public AK.Wwise.Event VoicesShield;
        [UsedImplicitly]
        public void BRVoicesShield() => VoicesShield.Post(gameObject);

        public AK.Wwise.Event VoicesShoot;
        [UsedImplicitly]
        public void BRVoicesShoot() => VoicesShoot.Post(gameObject);

        [Header("Play_Enemies_SmallRobot")]
        public AK.Wwise.Event Death;
        [UsedImplicitly]
        public void SRDeath() => Death.Post(gameObject);

        public AK.Wwise.Event MovementArms;
        [UsedImplicitly]
        public void SRMoveArms() => MovementArms.Post(gameObject);

        public AK.Wwise.Event SRTakeDamage;
        [UsedImplicitly]
        public void SmallRobotTakeDamage() => SRTakeDamage.Post(gameObject);

        public AK.Wwise.Event SRWalk;
        [UsedImplicitly]
        public void SmallRobotWalk() => SRWalk.Post(gameObject);

        [Header("Play_AmbientEmitters")]
        public AK.Wwise.Event ActivateSpikes;
        [UsedImplicitly]
        public void ActivateFloorSpikes() => ActivateSpikes.Post(gameObject);

        public AK.Wwise.Event ShowSpikes;
        [UsedImplicitly]
        public void ShowFloorSpikes() => ShowSpikes.Post(gameObject);

        public AK.Wwise.Event HoloBridgeDeactivate;
        [UsedImplicitly]
        public void DeactivateHoloBridge() => HoloBridgeDeactivate.Post(gameObject);

        public AK.Wwise.Event HoloBridgeIdle;
        [UsedImplicitly]
        public void IdleHoloBridge() => HoloBridgeIdle.Post(gameObject);

        public AK.Wwise.Event HoloWallsIdle;
        [UsedImplicitly]
        public void IdleHoloWalls() => HoloWallsIdle.Post(gameObject);

        [Header("Play_GameOverScreen")]
        public AK.Wwise.Event GameOverJingle;
        [UsedImplicitly]
        public void GOJingle() => GameOverJingle.Post(gameObject);
    }
}