﻿using UnityEngine;

namespace Game.Tutorial
{
    public class TutorialButton : MonoBehaviour
    {
        [SerializeField] 
        UnityEngine.UI.Image m_holdImage;

        [SerializeField] Animator m_animator;

        // Start is called before the first frame update
        void OnEnable() => EnableAnimator();
        public void EnableAnimator() => m_animator.enabled = true;

        public void SetHoldValue(float progress)
        {
            m_holdImage.fillAmount = progress;
            m_animator.enabled = false;
        }
    }
}
