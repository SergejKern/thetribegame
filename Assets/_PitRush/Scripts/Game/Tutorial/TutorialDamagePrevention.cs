﻿using System.Collections;
using Core.Events;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Controls;
using Game.Combat;
using Game.Globals;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;
using CombatTimeline = Game.Combat.Timeline.CombatTimeline;

namespace Game.Tutorial
{
    public class TutorialDamagePrevention : MonoBehaviour, IEventListener<AvoidingDamageActionEvent>
    {
        enum State
        {
            Idle,

            Tutorial,
            Executed
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_actor;

        [SerializeField] GameObject m_tutorialButtonPrefab;
        [SerializeField] CombatTimeline m_timeline;
        [SerializeField] Vector3 m_tutorialButtonOffset = Vector3.forward;

        [SerializeField] CombatTimeline.DifficultySetting m_tutorialSetting;
        [SerializeField] ControlCentre m_controlCentre;
#pragma warning restore 0649 // wrong warnings for SerializeField

        IDamageProtectionAction[] m_protectionActions;

        GameObject m_tutorialButtonInstance;

        State m_state;
        void OnEnable()
        {
            EventMessenger.AddListener(this);
            m_protectionActions = m_controlCentre.GetComponents<IDamageProtectionAction>();
        }

        void OnDisable() => EventMessenger.RemoveListener(this);

        void Update()
        {
            if (m_state != State.Idle)
                return;

            UpdateIdle();
        }

        IEnumerator UpdateTutorial()
        {
            while(true)
            {
                yield return null;

                foreach (var action in m_protectionActions)
                    if (action.IsProtected)
                        OnExecuted();

                if (m_timeline.TargetIsDanger && !m_timeline.IsProtected)
                    continue;

                yield return new WaitForSeconds(0.1f);

                if (m_state == State.Executed) 
                    break;

                m_state = State.Idle;
                ResetToDefault();
                break;
            }

        }

        void UpdateIdle()
        {
            if (!GameGlobals.IsTutorial)
                return;

            if (!m_timeline.TargetIsDanger)
                return;

            m_tutorialButtonInstance = m_tutorialButtonPrefab.GetPooledInstance(transform.position 
                                                                                + m_tutorialButtonOffset, Quaternion.identity);
            m_timeline.SetDifficulty(m_tutorialSetting);

            m_state = State.Tutorial;
            StartCoroutine(UpdateTutorial());
        }

        public void OnEvent(AvoidingDamageActionEvent eventType) => ActorExecutedAction(eventType.AvoidingActor);

        void ActorExecutedAction(GameObject actor)
        {
            if (actor != m_actor)
                return;
            if (m_state == State.Idle)
                return;

            OnExecuted();
        }

        void OnExecuted()
        {
            m_state = State.Executed;
            // to continue slowing time tutorial:
            //m_state = State.Idle;

            ResetToDefault();
        }

        void ResetToDefault()
        {
            if (m_tutorialButtonInstance != null)
                m_tutorialButtonInstance.TryDespawnOrDestroy();
            m_timeline.UpdateDifficulty();
        }
    }
}
