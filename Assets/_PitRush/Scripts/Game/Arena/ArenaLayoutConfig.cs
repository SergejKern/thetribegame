﻿using System.Collections;
using UnityEngine;

namespace Game.Arena
{
    public abstract class ArenaLayoutConfig : ScriptableObject
    { 
        public abstract void GetDefaultTransformation(Vector3Int idx, GameObject prefab, out Vector3 pos, out Quaternion rot);

        public abstract GameObject GetPrefab(Vector3Int idx, ArenaPrefabVariant variant);

        public abstract LayoutIndexEnum GetIndexEnumerator();

        public abstract bool IsValid(Vector3Int idx);
        public abstract Vector3Int GetValidIndex(Vector3Int idx);
    }

    public abstract class LayoutIndexEnum : IEnumerator, IEnumerable
    {
        public abstract bool MoveNext();
        public abstract void Reset();
        object IEnumerator.Current => Current;
        protected abstract Vector3Int Current { get; }
        public IEnumerator GetEnumerator() => this;
    }
}
