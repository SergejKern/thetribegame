﻿using System;
using Core.Unity.Interface;
using DG.Tweening;
using GameUtility.Data.Visual;
using UnityEngine;

namespace Game.Arena
{
    [CreateAssetMenu(menuName = "Game/Arena/PlatformWiggleConfig")]
    public class PlatformWiggleConfig : ScriptableObject
    {
        public float WiggleTime = 2.0f;

        [Serializable]
        public struct WiggleData
        {
            public float Strength;
            public int Vibrato;
            public float Randomness;
            public static WiggleData Default => new WiggleData() { Randomness = 75f, Strength = 0.8f, Vibrato = 5 };
        }

        public WiggleData PositionWiggle = WiggleData.Default;
        public WiggleData RotationWiggle = WiggleData.Default;
        public RefIAudioAsset WiggleSound;

        public MaterialOverrideMap[] MaterialOverride;
    }

    public static class PlatformWiggleOperation
    {
        public static void Wiggle(PlatformWiggleConfig config, ArenaPlatform platform, Sequence seq) //, out TweenCallback revertCallback)
        {
            var platformTr = platform.transform;
            var rend = platform.GetBorderRenderer();

            seq.AppendCallback(() => MaterialExchange.Override(rend, config.MaterialOverride));
            if (!(config.WiggleTime > 0))
                return;
            Wiggle(config, platform, seq, platformTr);
            //revertCallback = () => MaterialExchange.Revert(rend, config.MaterialOverride);
        }
        static void Wiggle(PlatformWiggleConfig config, Component platform, Sequence seq, Transform platformTr)
        {
            var posWiggle = config.PositionWiggle;
            var rotWiggle = config.RotationWiggle;
            seq.AppendCallback(() => config.WiggleSound.Result?.Play(platform.gameObject));

            seq.Insert(0,
                platformTr.DOShakeRotation(config.WiggleTime, rotWiggle.Strength, rotWiggle.Vibrato, rotWiggle.Randomness));
            seq.Insert(0,
                platformTr.DOShakePosition(config.WiggleTime, posWiggle.Strength, posWiggle.Vibrato, posWiggle.Randomness));
        }

        public static void Revert(PlatformWiggleConfig config, ArenaPlatform platform)
        {
            var rend = platform.GetBorderRenderer();
            MaterialExchange.Revert(rend, config.MaterialOverride);
        }

    }
}
