﻿using System.Collections.Generic;
using Core.Types;
using Core.Unity.Extensions;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Arena
{
    public class ArenaSelectionConfig : ScriptableObject, IArenaSelection
    {
        public ArenaPlatform.PlatformID[] SelectionIds
        {
            get => m_selectionIds;
            set => m_selectionIds = value;
        }

        //public ArenaLayoutConfig Layout;
        //public List<Vector3Int> Selection = new List<Vector3Int>();
        [FormerlySerializedAs("SelectionIds")] 
        [SerializeField]
        #if UNITY_EDITOR
        public 
        #endif
        // ReSharper disable once InconsistentNaming
        ArenaPlatform.PlatformID[] m_selectionIds = new ArenaPlatform.PlatformID[0];

        public List<ArenaPlatform> Platforms { get; } = new List<ArenaPlatform>();

        public Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData> PlatformMap { get; set; }
    }

    public class ArenaSelection : IArenaSelection
    {
        public ArenaPlatform.PlatformID[] SelectionIds { get; set; }
        public List<ArenaPlatform> Platforms { get; } = new List<ArenaPlatform>();
        public Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData> PlatformMap { get; set; }
    }

    public interface IArenaSelection
    {
        ArenaPlatform.PlatformID[] SelectionIds { get; set; }
        List<ArenaPlatform> Platforms { get; }
        Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData> PlatformMap { get; set; }
    }

    public static class ArenaSelectionOperations
    {
        public static void InitConfig(this IArenaSelection s, IArenaPlatformData arena)
        {
            s.PlatformMap = arena.GetPlatformMap();
            if (s.PlatformMap == null)
                return;

            s.InitPlatforms();
        }

        public static void SetPreferredVariantType(this IArenaSelection s, ArenaPlatform.PlatformID id, ArenaPrefabVariant variant)
        {
            if (s.PlatformMap.TryGetValue(id, out var platformInstanceData) && platformInstanceData.PlatformInstance != null)
            {
                Debug.LogError($"Cannot change variant type {variant} of spawned platform {id.Index} {id.UniqueLayoutIdx}");
                return;
            }
            s.PlatformMap[id] = new PlatformInstanceData(){Variant = variant};
        }

        public static bool GetInstance(this IArenaSelection s, ArenaPlatform.PlatformID id, out PlatformInstanceData instance) =>
            s.PlatformMap.TryGetValue(id, out instance);

        public static ArenaPrefabVariant GetPreferredVariantType(this IArenaSelection s, ArenaPlatform.PlatformID id) => 
            s.PlatformMap.TryGetValue(id, out var platformInstanceData) 
                ? platformInstanceData.Variant 
                : ArenaPrefabVariant.Default;

        public static void AddPlatform(this IArenaSelection s, ArenaPlatform.PlatformID id, ArenaPlatform platform)
        {
            s.PlatformMap[id] = new PlatformInstanceData() { PlatformInstance = platform, Variant = platform.Variant };
            s.Platforms.Add(platform);
        }

        public static void DespawnPlatforms(this IArenaSelection s)
        {
            s.RemovePlatformsFromGroups();

            foreach (var platform in s.Platforms)
            {
                if (platform == null)
                    continue;
                platform.SetDirty();
                platform.UpdateHighlightBorderVisibility();
                s.PlatformMap.Remove(platform.ID);

                platform.Group = null;
                platform.gameObject.TryDespawnOrDestroy();
            }

            s.Platforms.Clear();
        }

        static void RemovePlatformsFromGroups(this IArenaSelection s)
        {
            #if UNITY_EDITOR
            using (var groups = SimplePool<List<ArenaPlatformGroup>>.I.GetScoped())
            {
                foreach (var platform in s.Platforms)
                    if (platform.Group != null)
                    {
                        groups.Obj.Add(platform.Group);
                        Debug.LogWarning("Platform was still on Group! Unexpected!");
                    }

                foreach (var g in groups.Obj)
                    g.RemovePlatformsAndRebuild(s.Platforms);
            }
            #endif
        }

        public static void ChangeIndex(this IArenaSelection s, ArenaPlatform platform, Vector3Int platformIdx)
        {
            var oldID = platform.ID;
            if (s.PlatformMap.TryGetValue(oldID, out var platformAtOldIndex)
                && platformAtOldIndex.PlatformInstance == platform)
                s.PlatformMap.Remove(oldID);

            platform.SetIndex(platformIdx);
            s.PlatformMap[platform.ID] = new PlatformInstanceData(){ PlatformInstance = platform , Variant = platform.Variant };
        }

        static void InitPlatforms(this IArenaSelection s)
        {
            s.Platforms.Clear();
            foreach (var sel in s.SelectionIds)
            {
                s.PlatformMap.TryGetValue(sel, out var platform);
                if (platform.PlatformInstance == null)
                    continue;
                s.Platforms.Add(platform.PlatformInstance);
            }
        }
    }

    public interface IArenaSelectionAction
    {
        IArenaSelection Selection { get; set; }
    }
}
