﻿using System;
using Core.Extensions;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Game.Arena.Level.Mission;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Arena.Level
{
    [CreateAssetMenu(menuName = "Game/Level/ArenaLevelFlowConfig")]
    public class ArenaLevelFlowConfig : ScriptableObject, ISerializationCallbackReceiver
    {
        public static ArenaLevelFlowConfig Current;

        [Serializable]
        public struct ArenaLayoutData
        {
            public string DebugName;
            [FormerlySerializedAs("LayoutConfig")]
            public ArenaLayoutConfig Config;

            public Vector3 Position;

            public void GetDefaultTransformation(Vector3Int idx, GameObject prefab, out Vector3 pos, out Quaternion rot)
            {
                Config.GetDefaultTransformation(idx, prefab, out pos, out rot);
                pos += Position;
            }
            public GameObject GetPrefab(Vector3Int idx, ArenaPrefabVariant variant)
            {
                if (Config != null) 
                    return Config.GetPrefab(idx, variant);
                Debug.LogError("Config is null!");
                return null;
            }

            public bool IsValid(Vector3Int index) => Config.IsValid(index);
        }

        [Serializable]
        public struct FlowData
        {
            [FormerlySerializedAs("m_triggerKeyString"),
             SerializeField, HideInInspector] 
            internal string m_flowKeyString;

            [FormerlySerializedAs("TriggerKey")]
            [EnumStringSync(nameof(m_flowKeyString))]
            public LevelTriggerKey FlowKey;

            [SerializeField] 
            public MissionConfig Mission;
            
            public AK.Wwise.State MusicState;

            public ScriptableBaseAction[] Actions;
            public static FlowData Default => 
                new FlowData() { Actions = new ScriptableBaseAction[0]};
        }

        [Serializable]
        public struct TimeTargetReward
        {
            public float TimeSeconds;
            public int CoinsReward;
        }

        [Serializable]
        public struct ResultData
        {
            public TimeTargetReward Bronze;
            public TimeTargetReward Silver;
            public TimeTargetReward Gold;
            public int BronzeTargetCoins;
            public int SilverTargetCoins;
            public int GoldTargetCoins;

            public int GetStars(int score)
            {
                var stars = 0;
                if (score >= BronzeTargetCoins)
                    stars++;
                if (score >= SilverTargetCoins)
                    stars++;
                if (score >= GoldTargetCoins)
                    stars++;
                return stars;
            }
        }

        [FormerlySerializedAs("Triggers")]
        public FlowData[] FlowDataList = new FlowData[0];

        public ArenaLayoutData[] ArenaLayoutDataList;

        public ResultData Result;

        //void ExtractAllPlatformSelector(ICollection<ScriptableBaseAction> actions, ICollection<ArenaSelectionConfig> selectionConfigs)
        //{
        //    foreach (var t in FlowDataList)
        //    foreach (var d in t.Actions)
        //        GetAllActionsRecursively(d, actions);
        //    foreach (var o in actions)
        //    {
        //        if (_!(o is IPlatformSelector sel) || sel.Selection == null)
        //            continue;
        //        selectionConfigs.Add(sel.Selection);
        //    }
        //}

        public void OnBeforeSerialize()
        {
            for (var i = 0; i < FlowDataList.Length; i++)
                CoreEnumExtensions.OnBeforeSerializeEnum(out FlowDataList[i].m_flowKeyString, FlowDataList[i].FlowKey);
        }

        public void OnAfterDeserialize()
        {
            for (var i = 0; i < FlowDataList.Length; i++)
                CoreEnumExtensions.OnAfterDeserializeEnum(ref FlowDataList[i].FlowKey, FlowDataList[i].m_flowKeyString);
        }

        public bool IsValidLayoutID(int layoutID) => layoutID.IsInRange(ArenaLayoutDataList);
        public ArenaLayoutData GetLayoutData(int layoutID)
        {
            return IsValidLayoutID(layoutID)
                ? ArenaLayoutDataList[layoutID] 
                : default;
        }
    }
}