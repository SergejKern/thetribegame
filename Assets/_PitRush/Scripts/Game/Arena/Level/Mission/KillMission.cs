﻿using System;
using Core.Events;
using Core.Types.VariableWrapper;
using Game.Actors;
using GameUtility.Events;
using ScriptableUtility;
using ScriptableUtility.Actions;

namespace Game.Arena.Level.Mission
{
    public class KillMission : MissionConfig
    {        
        public int KillEnemies;

        public override string Name => "Kill Mission";
        static Type StaticFactoryType => typeof(KillMissionAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            BaseInit(ctx, out var finishAction);
            return new KillMissionAction(finishAction, MissionFinished, WaitForFinishWaveActions, KillEnemies);
        }
    }

    public class KillMissionAction : MissionAction, IEventListener<KillEvent>
    {
        public KillMissionAction(IBaseAction onFinishAction, BoolVar missionFinishedBool, bool waitForWaveActions, 
            int kill) : base(onFinishAction, missionFinishedBool, waitForWaveActions)
        {
            m_killed = 0;
            m_killEnemies = kill;
        }

        readonly int m_killEnemies;
        int m_killed;

        public override float MissionProgress => m_killEnemies==0
                ? 0f 
                : (float) m_killed / m_killEnemies;

        public override string MissionTypeText => "kill Enemies!";
        public override string MissionProgressText => $"{m_killed} : {m_killEnemies}";

        public override bool MissionFinished => m_killed >= m_killEnemies;
        public override string Announcement => $"Kill {m_killEnemies} enemies";

        public override void Init()
        {
            base.Init();
            m_killed = 0;
            EventMessenger.AddListener(this);
        }

        public void OnEvent(KillEvent killEvent)
        {
            var isEnemy = killEvent.KilledActor.TryGetComponent<Enemy>(out _);
            if (!isEnemy)
                return;
            m_killed++;
            if (MissionFinished)
                EventMessenger.RemoveListener(this);
        }
    }
}
