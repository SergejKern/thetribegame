﻿using System;
using Core.Types.VariableWrapper;
using ScriptableUtility;
using ScriptableUtility.Actions;

namespace Game.Arena.Level.Mission
{
    public class SurviveMission : MissionConfig
    {        
        public float SurviveForSeconds;

        public override string Name => "Survive Mission";
        static Type StaticFactoryType => typeof(SurviveMissionAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            BaseInit(ctx, out var finishAction);
            return new SurviveMissionAction(finishAction, MissionFinished, WaitForFinishWaveActions, 
                SurviveForSeconds);
        }
    }

    public class SurviveMissionAction : MissionAction
    {
        public SurviveMissionAction(IBaseAction onFinishAction, BoolVar missionFinishedBool, bool waitForWaveActions, 
            float seconds) : base(onFinishAction, missionFinishedBool, waitForWaveActions)
        {
            m_survivedTime = 0;
            m_surviveForSeconds = seconds;
        }

        readonly float m_surviveForSeconds;
        float m_survivedTime;

        public override float MissionProgress => 
            Math.Abs(m_surviveForSeconds) < float.Epsilon ? 0f 
                : m_survivedTime / m_surviveForSeconds;

        public override string MissionTypeText => "survive!";
        public override string MissionProgressText  
        {
            get
            {
                var timeLeft = Math.Max(m_surviveForSeconds - m_survivedTime, 0);
                var ts = TimeSpan.FromSeconds(timeLeft);
                return $"{ts:mm\\:ss}";
            }
        }

        public override bool MissionFinished => m_survivedTime >= m_surviveForSeconds;
        public override string Announcement => $"Survive for {m_surviveForSeconds} seconds";

        public override void Init()
        {
            base.Init();
            m_survivedTime = 0;
        }

        public override void Update(float dt)
        {
            base.Update(dt);

            m_survivedTime += dt;
        }
    }
}
