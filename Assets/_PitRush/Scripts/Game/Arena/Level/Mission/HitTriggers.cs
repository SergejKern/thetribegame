﻿using System;
using System.Linq;
using Core.Types.VariableWrapper;
using ScriptableUtility;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables.Reference;

namespace Game.Arena.Level.Mission
{
    public class HitTriggers : MissionConfig
    {        
        public BoolReference[] Triggers;

        public override string Name => "Hit Triggers";
        static Type StaticFactoryType => typeof(HitTriggersAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction(IContext ctx)
        {
            BaseInit(ctx, out var finishAction);

            var triggerVars = new BoolVar[Triggers.Length];
            for (var i = 0; i < Triggers.Length; i++)
            {
                Triggers[i].Init(ctx);
                triggerVars[i] = Triggers[i];
            }

            return new HitTriggersAction(finishAction, MissionFinished, WaitForFinishWaveActions, triggerVars);
        }
    }

    public class HitTriggersAction : MissionAction
    {
        public HitTriggersAction(IBaseAction onFinishAction, BoolVar missionFinishedBool, bool waitForWaveActions, 
            BoolVar[] triggerVars) : base(onFinishAction, missionFinishedBool, waitForWaveActions)
            => m_triggerVars = triggerVars;

        readonly BoolVar[] m_triggerVars;
        int m_triggersActive;

        public override float MissionProgress => m_triggerVars.Length == 0
                ? 0f 
                : (float) m_triggersActive / m_triggerVars.Length;

        public override string MissionTypeText => "Hit the Bells!";
        public override string MissionProgressText => $"{m_triggersActive} : {m_triggerVars.Length}";

        public override bool MissionFinished => m_triggersActive >= m_triggerVars.Length;
        public override string Announcement => $"Hit {m_triggerVars.Length} Triggers";

        public override void Init()
        {
            base.Init();
            m_triggersActive = 0;
        }

        public override void Update(float dt)
        {
            base.Update(dt);
            m_triggersActive = m_triggerVars.Count(t => t.Value);
        }
    }
}
