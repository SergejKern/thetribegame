﻿using System.Collections.Generic;
using Core.Interface;
using Core.Types.VariableWrapper;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables.Reference;
using UnityEngine;

namespace Game.Arena.Level.Mission
{
    public abstract class MissionConfig : ScriptableBaseAction, IActionConfigContainer
    {
        [HideInInspector]
        public ScriptableBaseAction OnFinish;

        public BoolReference MissionFinished;
        public bool WaitForFinishWaveActions;

        public IEnumerable<ScriptableBaseAction> GetContainedActions() 
            => new[] {OnFinish};

        protected void BaseInit(IContext ctx, out IBaseAction finishAction)
        {
            finishAction = OnFinish ? OnFinish.CreateAction(ctx) : null;
            MissionFinished.Init(ctx);
        }
    }

    public interface IMissionAction : IUpdatingAction, IInstantCompletableAction
    {
        float MissionProgress { get; }
        string MissionTypeText { get; }
        string MissionProgressText { get; }

        bool MissionFinished { get; }
        string Announcement { get; }

        bool WaitForFinishWaveActions { get; }

        void OnFinishMission();
    }

    public abstract class MissionAction : IMissionAction, IContainingActions
    {
        protected MissionAction(IBaseAction onFinish, 
            BoolVar missionFinishedBool,
            bool waitForWaveActions)
        {
            m_onFinish = onFinish;
            m_missionFinBoolVar = missionFinishedBool;
            WaitForFinishWaveActions = waitForWaveActions;
        }

        readonly IBaseAction m_onFinish;
        BoolVar m_missionFinBoolVar;

        bool FinalActionFinished => m_onFinishedCalled && (!(m_onFinish is IUpdatingAction up) || up.IsFinished);
        public bool WaitForFinishWaveActions { get; }

        public bool IsFinished 
            => MissionFinished && FinalActionFinished;
        public abstract string Announcement { get; }

        bool m_onFinishedCalled;

        public virtual void Init()
        {
            m_missionFinBoolVar.SetValue(false);
        }

        public virtual void Update(float dt)
        {
            if (!MissionFinished) 
                return;
            UpdateFinishing(dt);
        }

        void UpdateFinishing(float dt)
        {
            if (!m_onFinishedCalled)
                OnFinishMission();
            m_onFinishedCalled = true;

            if (m_onFinish is IUpdatingAction finishingUpdate)
                finishingUpdate.Update(dt);
        }

        public void OnFinishMission()
        {
            m_missionFinBoolVar.SetValue(true);
            switch (m_onFinish)
            {
                case IUpdatingAction finishingUpdate: finishingUpdate.Init();
                    break;
                case IDefaultAction finishAction: finishAction.Invoke();
                    break;
            }
        }

        public abstract float MissionProgress { get; }
        public abstract string MissionTypeText { get; }
        public abstract string MissionProgressText { get; }

        public abstract bool MissionFinished { get; }

        public void InstantComplete()
        {
            if (m_onFinish is IInstantCompletableAction instant)
                instant.InstantComplete();
        }

        public IEnumerable<IBaseAction> GetContainedActions() => 
            new[] {m_onFinish};
    }
}
