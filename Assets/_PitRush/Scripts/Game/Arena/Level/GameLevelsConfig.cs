﻿using System;
using Game.Configs.Elevator;
using UnityEngine;

namespace Game.Arena.Level
{
    [CreateAssetMenu(menuName = "Game/LevelsConfig")]
    public class GameLevelsConfig : ScriptableObject
    {
        [Serializable]
        public struct LevelData
        {
            public ArenaLevelFlowConfig Level;
            public ElevatorMoveConfig MoveConfig;
            public LeviDialogueSequence ElevatorDialogue;

            public Sprite LampGraphicOff;
            public Sprite LampGraphicOn;
            //todo 2: unlock requirements
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] LevelData[] m_levelData;
        [SerializeField] LevelData m_default;
        [SerializeField] LevelData m_pvpLevel;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public int LevelCount => m_levelData.Length;
        public ArenaLevelFlowConfig PVPLevel => m_pvpLevel.Level;

        int GetLevelIdx(string lvlName) => Array.FindIndex(m_levelData, dat => string.Equals(dat.Level.name,lvlName, StringComparison.Ordinal));
        public int GetLevelIdx(ArenaLevelFlowConfig config) => Array.FindIndex(m_levelData, dat => dat.Level == config);
        // ReSharper disable once UnusedMember.Global
        public LevelData Get(int idx) => m_levelData[idx];
        // ReSharper disable once UnusedMember.Global
        public LevelData Get(string lvlName)
        {
            var idx = GetLevelIdx(lvlName);
            return idx == -1 ? default : m_levelData[idx];
        }

        public LevelData Get(ArenaLevelFlowConfig lvlConfig)
        {
            if (lvlConfig == PVPLevel)
                return m_pvpLevel;
            var idx = GetLevelIdx(lvlConfig);
            return idx == -1 ? m_default : m_levelData[idx];
        }
    }
}
