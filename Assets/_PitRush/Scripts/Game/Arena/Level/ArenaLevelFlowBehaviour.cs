using System;
using System.Collections;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Utility.GUITools;
using Game.Arena.Level.Mission;
using Game.Configs.Elevator;
using Game.Globals;
using Game.SaveLoad;
using Game.UI.HUD;
using Game.UI.ResultScreen;
using GameUtility;
using JetBrains.Annotations;
using ScriptableUtility;
using ScriptableUtility.Actions;
using ScriptableUtility.Variables.Reference;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Game.Arena.Level
{
    public class ArenaLevelFlowBehaviour : MonoBehaviour, IContextProvider
    {
        struct RunningLevelData
        {
            public ArenaLevelFlowConfig Level;
            public FlowData[] Flow;
            public int CurrentTriggerIdx;
            public int CurrentWave;
            public int WaveCount;
            public int Seed;
        }

        struct FlowData
        {
            public FlowData(ArenaLevelFlowConfig.FlowData flow, IContext ctx)
            {
                TriggerKey = flow.FlowKey;
                // EnemyCount = CountEnemiesInTrigger(ref flow, ctx);
                MusicState = flow.MusicState;
                Mission = null;
                if (flow.Mission != null)
                    Mission = (IMissionAction) flow.Mission.CreateAction(ctx);
                Actions = new IBaseAction[flow.Actions.Length];
                for (var i = 0; i < Actions.Length; i++)
                {
                    if (flow.Actions[i]==null)
                        continue;

                    Actions[i] = flow.Actions[i].CreateAction(ctx);
                }
            }

            public readonly LevelTriggerKey TriggerKey;

            public readonly AK.Wwise.State MusicState;
            //public readonly int EnemyCount;

            public readonly IMissionAction Mission;
            public readonly IBaseAction[] Actions;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ArenaLevelFlowConfig m_defaultConfig;
        [SerializeField] ContextComponent m_context;
        [SerializeField] UnityEvent m_onAllTriggerDone;

        [SerializeField] IntReference m_seed;
        [SerializeField] bool m_debugDrawCurrentWave;

        [SerializeField] bool m_cheatKeysEnabled;
        [SerializeField] KeyCode m_nextWave;

        //[SerializeField] GameObject m_resultScreenPrefab;
        [SerializeField] string m_resultScreenScene;

        [SerializeField] LevelStatTracker m_levelStatTracker;

        [SerializeField] ElevatorMoveConfig m_onFinishConfig;
#pragma warning restore 0649 // wrong warnings for SerializeField

        RunningLevelData m_currentLevelData;
        UnityAction m_onLevelDoneElevatorStarted;
        ref FlowData GetFlowData(int i) => ref m_currentLevelData.Flow[i];

        bool LevelStarted => m_currentLevelData.Flow != null;
        bool LevelDone => LevelStarted && m_currentLevelData.CurrentTriggerIdx >= m_currentLevelData.Flow.Length;
        public IContext Context => m_context;
        public int WaveCount => m_currentLevelData.WaveCount;

        int m_currentRunningActions;

        void OnDisable() => StopLevel();

        public void StartLevel()
        {
            if (ArenaLevelFlowConfig.Current == null) 
                ArenaLevelFlowConfig.Current = m_defaultConfig;

            StartLevel( ArenaLevelFlowConfig.Current );
        }

        void StartLevel(ArenaLevelFlowConfig level)
        {
            if (TryGetComponent(out ContextComponent cc))
                cc.Init();

            GameGlobals.CurrentLevelFlow = this;
            // already started
            if (m_currentLevelData.Level == level)
                return;
            if (GameGlobals.SceneGUI != null)
                GameGlobals.SceneGUI.AddSceneGUI(DrawCurrentTriggerGUI);
            InitSeed();

            m_currentLevelData.Level = level;

            InitPlatformData();
            AddElevatorToMap();

            InitFlowData();

            m_currentLevelData.CurrentTriggerIdx = -1;

            NextTrigger();
        }

        static void AddElevatorToMap()
        {
            if (GameGlobals.Elevator == null)
                return;

            GameGlobals.Elevator.TryGetComponent(out ArenaPlatform platform);
            var elevatorId = new ArenaPlatform.PlatformID {Index = ArenaLayoutConfigCircular.CenterIdx};
            platform.Init(elevatorId, ArenaPrefabVariant.Default);
            ArenaPlatformMapData.PlatformData.GetPlatformMap().Add(elevatorId, new PlatformInstanceData
            {
                Variant = ArenaPrefabVariant.Default,
                PlatformInstance = platform
            });
        }

        void InitFlowData()
        {
            var lvl = m_currentLevelData.Level;
            m_currentLevelData.Flow = new FlowData[lvl.FlowDataList.Length];

            var waveCount = 0;
            for (var i = 0; i < lvl.FlowDataList.Length; i++)
            {
                var flowDat = lvl.FlowDataList[i];
                var isWave = (flowDat.FlowKey >= LevelTriggerKey.Wave1 && flowDat.FlowKey <= LevelTriggerKey.Wave9);
                if (isWave)
                    waveCount++;

                m_currentLevelData.Flow[i] = new FlowData(lvl.FlowDataList[i], Context);
            }

            m_currentLevelData.CurrentWave = 0;
            m_currentLevelData.WaveCount = waveCount;
        }

        static void InitPlatformData()
        {
            var data = new ArenaPlatformMapData();
            ArenaPlatformMapData.PlatformData = data;
        }

        void InitSeed()
        {
            var r = new System.Random();
            m_currentLevelData.Seed = r.Next();

            m_seed.Init(Context);
            m_seed.SetValue(m_currentLevelData.Seed);
        }


        void Update()
        {
            if (!LevelStarted || LevelDone)
                return;
            if (!m_currentLevelData.CurrentTriggerIdx.IsInRange(m_currentLevelData.Flow))
                return;

            ref var trigger = ref GetFlowData(m_currentLevelData.CurrentTriggerIdx);
            UpdateTrigger(ref trigger);
            //UpdateMission(r)
            UpdateCheats();
        }

        static void InitTrigger(ref FlowData flowData)
        {
            if (flowData.MusicState.IsValid())
                flowData.MusicState.SetValue();
            foreach (var a in flowData.Actions)
            {
                switch (a)
                {
                    case IUpdatingAction updating: updating.Init(); break;
                    case IDefaultAction def: def.Invoke(); break;
                }
            }
            flowData.Mission?.Init();
        }

        void UpdateTrigger(ref FlowData flowData)
        {
            if (UpdateMission(ref flowData) == FlowResult.Stop) 
                return;

            UpdateTriggerActions(ref flowData, 
                out m_currentRunningActions);

            var allowNextOnCompletedActions = flowData.Mission == null 
                            && flowData.TriggerKey != LevelTriggerKey.Initial;
            if (m_currentRunningActions <= 0 && allowNextOnCompletedActions)
                NextTrigger();
        }

        static void UpdateTriggerActions(ref FlowData flowData, 
            out int runningActions)
        {
            runningActions = 0;
            foreach (var a in flowData.Actions)
            {
                if (!(a is IUpdatingAction updateAction))
                    continue;
                if (updateAction.IsFinished)
                    continue;
                updateAction.Update(Time.deltaTime);
                if (!updateAction.IsFinished)
                    runningActions++;
            }
        }

        FlowResult UpdateMission(ref FlowData flowData)
        {
            var mission = flowData.Mission;
            if (mission == null) 
                return FlowResult.Continue;

            mission.Update(Time.deltaTime);

            if (!mission.MissionFinished) 
                return FlowResult.Continue;
            if (mission.WaitForFinishWaveActions && m_currentRunningActions>0)
                return FlowResult.Continue;

            if (!m_wasFinished)
                StopAllActions(ref flowData);
            m_wasFinished = true;
            if (mission.IsFinished)
                NextTrigger();
            return FlowResult.Stop;
        }

        static void StopAllActions(ref FlowData flowData)
        {
            foreach (var a in flowData.Actions)
            {
                if (!(a is IStoppableAction stop))
                    continue;
                
                stop.Stop();
            }
        }

        bool m_wasFinished;

        void StopLevel()
        {
            var idx = m_currentLevelData.CurrentTriggerIdx;
            if (!idx.IsInRange( m_currentLevelData.Flow))
                return;
            ref var trigger = ref GetFlowData(idx);
            StopAllActions(ref trigger);
            m_currentLevelData = default;
        }

        public void NextTrigger()
        {
            m_wasFinished = false;
            m_currentLevelData.CurrentTriggerIdx++;
            if (LevelDone)
            {
                OnLevelDone();
                m_onAllTriggerDone.Invoke();
                return;
            }

            ref var trigger = ref GetFlowData(m_currentLevelData.CurrentTriggerIdx);
            var isWave = (trigger.TriggerKey >= LevelTriggerKey.Wave1 && trigger.TriggerKey <= LevelTriggerKey.Wave9);
            if (isWave)
                m_currentLevelData.CurrentWave++;
            // pause time for intermission, Outro, etc.
            m_levelStatTracker.PauseTracking(trigger.TriggerKey > LevelTriggerKey.Boss);
            GameGlobals.IsTutorial = trigger.TriggerKey == LevelTriggerKey.Tutorial;
            GameGlobals.GO_UICanvas.SetActive(true);

            if (GameGlobals.GO_UICanvas.TryGetComponent<WaveHUD>(out var waveHUD))
                waveHUD.UpdateWave(trigger.TriggerKey, trigger.Mission, m_levelStatTracker);

            InitTrigger(ref trigger);
        }

        public void StartLevelWhenOnInit()
        {
            ref var trigger = ref GetFlowData(m_currentLevelData.CurrentTriggerIdx);
            if (trigger.TriggerKey != LevelTriggerKey.Initial)
                return;

            m_levelStatTracker.StartTracking(m_currentLevelData.Level);
            NextTrigger();
        }


        void OnLevelDone()
        {
            if (m_onLevelDoneElevatorStarted != null)
                return;
            m_levelStatTracker.StopTracking();

            SaveSystem.GetScores(m_currentLevelData.Level, out var highScore, out var bestTime);
            var newHighscore = Math.Max(highScore, m_levelStatTracker.Stats.Score);
            var newBestTime = Math.Min(bestTime, m_levelStatTracker.Stats.Time);
            if (newHighscore > highScore || newBestTime < bestTime)
                SaveSystem.SaveLevelData(m_currentLevelData.Level, newHighscore, newBestTime);

            var move = GameGlobals.Elevator.Move;
            move.SetConfig(m_onFinishConfig);
            m_onLevelDoneElevatorStarted = OnLevelDoneAndElevatorStarted;
            move.AddOnElevatorStartedListener(m_onLevelDoneElevatorStarted);
            GameGlobals.Elevator.ShowElevatorVisuals(true);
        }

        void OnLevelDoneAndElevatorStarted()
        {
            if (m_onLevelDoneElevatorStarted == null)
                return;

            var move = GameGlobals.Elevator.Move;
            move.RemoveOnElevatorStartedListener(m_onLevelDoneElevatorStarted);
            m_onLevelDoneElevatorStarted = null;

            foreach (var player in GameGlobals.Players)
                player.Input.SwitchCurrentActionMap("Menu");
            //GameGlobals.RemoveAllInput();
            //GameGlobals.Players.Clear();
            StartCoroutine(LoadResultScreenAsync());
        }

        IEnumerator LoadResultScreenAsync()
        {
            var asyncOp = SceneManager.LoadSceneAsync(m_resultScreenScene, LoadSceneMode.Additive);
            while (!asyncOp.isDone)
                yield return null;
            var sceneRootGo = SceneRoot.GetSceneRoot(SceneManager.GetSceneByName(m_resultScreenScene), out var sceneRoot);
            var resultScreen = sceneRootGo.GetComponentInChildren<ResultScreen>();

            sceneRoot.InitActive(false);
            sceneRoot.SetActive(true);
            while (sceneRoot.IsInTransition)
                yield return null;

            GameGlobals.GO_UICanvas.SetActive(false);
            resultScreen.SetData(m_levelStatTracker.Stats);
            //GameGlobals.Players.Clear();
        }

        #region Debug & Cheats

        void DrawCurrentTriggerGUI()
        {
            if (!m_debugDrawCurrentWave)
                return;
            if (LevelDone)
                return;
            var key = m_currentLevelData.Flow[m_currentLevelData.CurrentTriggerIdx].TriggerKey;

            using (new ColorScope(Color.green))
                GUILayout.Box($"Current trigger: {key}");
        }

        void UpdateCheats()
        {
            if (!m_cheatKeysEnabled || !Input.anyKeyDown)
                return;
            if (Input.GetKeyDown(m_nextWave))
                SkipToTrigger((LevelTriggerKey) m_currentLevelData.CurrentTriggerIdx + 1);
            else if (Input.GetKeyDown(KeyCode.Alpha1)) SkipToTrigger(LevelTriggerKey.Wave1);
            else if (Input.GetKeyDown(KeyCode.Alpha2)) SkipToTrigger(LevelTriggerKey.Wave2);
            else if (Input.GetKeyDown(KeyCode.Alpha3)) SkipToTrigger(LevelTriggerKey.Wave3);
            else if (Input.GetKeyDown(KeyCode.Alpha4)) SkipToTrigger(LevelTriggerKey.Wave4);
            else if (Input.GetKeyDown(KeyCode.Alpha5)) SkipToTrigger(LevelTriggerKey.Wave5);
            else if (Input.GetKeyDown(KeyCode.Alpha6)) SkipToTrigger(LevelTriggerKey.Wave6);
            else if (Input.GetKeyDown(KeyCode.Alpha7)) SkipToTrigger(LevelTriggerKey.Wave7);
            else if (Input.GetKeyDown(KeyCode.Alpha8)) SkipToTrigger(LevelTriggerKey.Wave8);
            else if (Input.GetKeyDown(KeyCode.Alpha9)) SkipToTrigger(LevelTriggerKey.Wave9);
            else if (Input.GetKeyDown(KeyCode.Alpha0)) SkipToTrigger(LevelTriggerKey.Boss);
        }

        // ReSharper disable once FlagArgument
        void SkipToTrigger(LevelTriggerKey key)
        {
            try
            {
                while (m_currentLevelData.CurrentTriggerIdx < m_currentLevelData.Flow.Length)
                {
                    ref var trigger = ref GetFlowData(m_currentLevelData.CurrentTriggerIdx);
                    var isWave = (trigger.TriggerKey >= LevelTriggerKey.Wave1 && trigger.TriggerKey <= LevelTriggerKey.Wave9);
                    if (isWave && key <= trigger.TriggerKey)
                        return;
                    foreach (var a in trigger.Actions)
                        InstantComplete(a);
                    trigger.Mission?.InstantComplete();

                    NextTrigger();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        static void InstantComplete(IBaseAction a)
        {
            if (a is IUpdatingAction updateAction && updateAction.IsFinished)
                return;
            if (a is IInstantCompletableAction instant)
                instant.InstantComplete();
            //else if (a is IDefaultAction def)
            //    def.Invoke();
            
            //if(a is IContainingActions container)
            //    foreach (var conAction in container.GetContainedActions())
            //        InstantComplete(conAction);
        }

        #endregion
    }
}