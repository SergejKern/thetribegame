using System;
using Core.Events;
using Core.Extensions;
using Game.Actors;
using Game.Globals;
using Game.InteractiveObjects;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Events;
using UnityEngine;

namespace Game.Arena.Level
{
    public class LevelStatTracker : MonoBehaviour, 
        IEventListener<KillEvent>, 
        IEventListener<ReviveEvent>,
        IEventListener<DamageEvent>
    {
        public LevelStats Stats => m_stats;
        LevelStats m_stats;

        bool m_tracking;
        ref LevelPlayerStats GetPlayerStat(int idx) => ref m_stats.PlayerStats[idx];

        public void StartTracking(ArenaLevelFlowConfig level)// int waveCount)
        {
            m_stats = new LevelStats
            {
                Level = level,
                //WaveCount = waveCount,
                PlayerStats = new LevelPlayerStats[GameGlobals.Players.Count]
            };
            m_tracking = true;
            EventMessenger.AddListener<KillEvent>(this);
            EventMessenger.AddListener<ReviveEvent>(this);
            EventMessenger.AddListener<DamageEvent>(this);
        }

        public void StopTracking()
        {
            m_tracking = false;
            EvaluateScore();

            EventMessenger.RemoveListener<KillEvent>(this);
            EventMessenger.RemoveListener<ReviveEvent>(this);
            EventMessenger.RemoveListener<DamageEvent>(this);
        }

        public void PauseTracking(bool pause) => m_tracking = !pause;

        public void OnPlayerJoin() => 
            Array.Resize(ref m_stats.PlayerStats, GameGlobals.Players.Count);

        void Update()
        {
            if (!m_tracking)
                return;
            if (Time.timeScale <= float.Epsilon)
                return;
            m_stats.Time += Time.deltaTime;
        }

        public void OnEvent(KillEvent killEvent)
        {
            if (killEvent.KilledActor == null)
                return;
            UpdateForPlayerDeath(killEvent);
            UpdateForEnemyKill(killEvent);
        }

        void UpdateForPlayerDeath(KillEvent killEvent)
        {
            if (!killEvent.KilledActor.TryGetComponent(out Player player))
                return;

            if (!IdxOK(player)) return;
            ref var plStat = ref GetPlayerStat(player.PlayerIdx);
            plStat.Deaths++;
            //Debug.Log($"deaths {plStat.Deaths}");
        }


        void UpdateForEnemyKill(KillEvent killEvent)
        {
            if (killEvent.Killer == null)
                return;
            if (!killEvent.KilledActor.TryGetComponent(out Enemy _) ||
                !killEvent.Killer.TryGetComponent(out Player player))
                return;
            if (!IdxOK(player)) return;

            ref var plStat = ref GetPlayerStat(player.PlayerIdx);
            plStat.EnemiesDefeated++;
            //Debug.Log($"enemies killed {plStat.EnemiesDefeated}");
        }

        public void OnEvent(ReviveEvent reviveEvent)
        {
            if (reviveEvent.Revivor == null)
                return;
            if (!reviveEvent.Revivor.TryGetComponent(out Player player))
                return;
            if (!IdxOK(player)) return;

            ref var plStat = ref GetPlayerStat(player.PlayerIdx);
            plStat.RevivedFriends++;
            //Debug.Log($"revived {plStat.RevivedFriends}");
        }

        public void OnEvent(DamageEvent dmgEvent)
        {
            if (dmgEvent.Damaged == null)
                return;
            if (dmgEvent.DamageResult.DamageDone < 0)
                return;
            if (!dmgEvent.Damaged.TryGetComponent(out Player player))
                return;
            if (!IdxOK(player)) return;

            ref var plStat = ref GetPlayerStat(player.PlayerIdx);
            plStat.DamageTaken += dmgEvent.DamageResult.DamageDone;
            //Debug.Log($"dmg taken {plStat.DamageTaken}");
        }
        bool IdxOK(Player player)
        {
            if (player.PlayerIdx.IsInRange(m_stats.PlayerStats)) 
                return true;
            Debug.LogError("Not in range of PlayerStats {player.PlayerIdx}");
            return false;
        }
        void EvaluateScore()
        {
            var coins = Coin.CoinsCollected;
            Coin.CoinsCollected = 0;
            m_stats.CoinsCollected = coins;

            var resultData = m_stats.Level.Result;
            
            if (m_stats.Time <= resultData.Gold.TimeSeconds)
                coins += resultData.Gold.CoinsReward;
            else if (m_stats.Time <= resultData.Silver.TimeSeconds)
                coins += resultData.Silver.CoinsReward;
            else if (m_stats.Time <= resultData.Bronze.TimeSeconds)
                coins += resultData.Bronze.CoinsReward;

            var stars = resultData.GetStars(coins);

            m_stats.Score = coins;
            m_stats.Stars = stars;
        }
    }

    public struct LevelStats
    {
        public ArenaLevelFlowConfig Level;
        //public int WaveCount;
        public float Time;
        public int Score;
        public int Stars;
        public int CoinsCollected;

        public LevelPlayerStats[] PlayerStats;
    }

    public struct LevelPlayerStats
    {
        public int EnemiesDefeated;
        public float DamageTaken;
        public int Deaths;
        public int RevivedFriends;
    }
}
