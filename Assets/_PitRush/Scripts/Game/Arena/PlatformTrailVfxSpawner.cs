﻿using System.Collections.Generic;
using System.Linq;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using Game.Configs;
using GameUtility.Data.Visual;
using UnityEngine;

namespace Game.Arena
{
    public class PlatformTrailVfxSpawner : MonoBehaviour
    {
        struct FollowedPlayerData
        {
            public float UpdateTimer;
            public MaterialOverrideMap[] Materials;
        }

        struct RippleData
        {
            public GameObject GameObject;
            public ParticleSystemRenderer Renderer;
            //public Renderer Renderer;
            //public MeshFilter Filter;

            public MaterialOverrideMap[] OverrideMap;
            public float AliveTimer;
            public Vector3 Position;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_updateFrequencyInSeconds = 0.5f;
        [SerializeField] PrefabData m_ripples;
        [SerializeField] ArenaPlatform m_platform;
        [SerializeField] MeshFilter m_platformMesh;
        [SerializeField] string[] m_colorProperties;
        [SerializeField] float m_aliveInSeconds = 0.2f;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly Dictionary<GameObject, FollowedPlayerData> m_followedPlayers = new Dictionary<GameObject, FollowedPlayerData>();
        readonly List<RippleData> m_rippleObjects = new List<RippleData>();

        const string k_sphereCenter = "Sphere_Center";
        static readonly int k_center = Shader.PropertyToID(k_sphereCenter);

        void OnEnable() => InitMesh();

        void InitMesh()
        {
            TryGetComponent(out m_platform);
            if (m_platform == null)
                return;
            if (m_platformMesh != null) 
                return;
            m_platformMesh = m_platform.Filter;
        }

        public void Remove(GameObject o)
        {
            if (!m_followedPlayers.ContainsKey(o))
                return;

            m_followedPlayers.Remove(o);
        }

        public void Add(GameObject o)
        {
            if (!enabled)
                return;

            MaterialOverrideMap[] materials = null;
            if (o.TryGetComponent<Player>(out var pl))
            {
                var character = ConfigLinks.Characters.Characters[pl.CharacterIdx];
                materials = character.GridOverrideMap;
            }
            SpawnRipplePrefab(o.transform.position, materials);
            var followPlayerData = new FollowedPlayerData() {UpdateTimer = m_updateFrequencyInSeconds, Materials = materials };
            m_followedPlayers.Add(o, followPlayerData);
        }

        void Update()
        {
            var followedPlayers = m_followedPlayers.Keys.ToList();
            foreach (var go in followedPlayers)
            {
                if (go == null)
                {
                    Debug.LogError("Should not happen!");
                    continue;
                }

                var dat = m_followedPlayers[go];
                dat.UpdateTimer -= Time.deltaTime;

                if (dat.UpdateTimer < 0f)
                {
                    dat.UpdateTimer = m_updateFrequencyInSeconds;

                    SpawnRipplePrefab(go.transform.position, dat.Materials);
                }

                m_followedPlayers[go] = dat;
            }

            for (var i = m_rippleObjects.Count-1; i >= 0; i--)
            {
                var rippleObj = m_rippleObjects[i];
                rippleObj.AliveTimer -= Time.deltaTime;

                FadeColorDown(rippleObj);

                if (rippleObj.AliveTimer <= 0)
                {
                    RemoveVFXAtIndex(rippleObj, i);
                    continue;
                }
                m_rippleObjects[i] = rippleObj;
            }
        }

        void FadeColorDown(RippleData rippleObj)
        {
            foreach (var colorProp in m_colorProperties)
            {
                if (!rippleObj.Renderer.material.HasProperty(colorProp))
                    continue;
                var col = rippleObj.Renderer.material.GetColor(colorProp);
                rippleObj.Renderer.material.SetColor(colorProp,
                    new Color(col.r, col.g, col.b, rippleObj.AliveTimer / m_aliveInSeconds));
            }
        }

        void RemoveVFXAtIndex(RippleData rippleObj, int i)
        {
            if (rippleObj.Renderer != null && rippleObj.OverrideMap != null)
            {
                rippleObj.Renderer.material = rippleObj.OverrideMap[0].Default;
                rippleObj.Renderer.mesh = null;
            }

            if (rippleObj.GameObject != null)
                rippleObj.GameObject.TryDespawnOrDestroy();
            m_rippleObjects.RemoveAt(i);
        }

        void SpawnRipplePrefab(Vector3 pos, MaterialOverrideMap[] overrideMap)
        {
            if (m_platform == null)
                return;
            var group = m_platform.Group;
            if (group == null)
                return;
            var ripPref = m_ripples.Prefab;
            if (ripPref == null)
                return;

            RippleData data;
            var groupPos = group.Body.position;
            
            data.GameObject = ripPref.GetPooledInstance(groupPos + 0.01f * Vector3.up, group.Body.rotation); //meshTransform.position + 0.01f*Vector3.up, meshTransform.rotation, meshTransform.lossyScale);
            var tr = data.GameObject.transform;
            data.Renderer = tr.GetComponent<ParticleSystemRenderer>();

            data.Renderer.mesh = group.Mesh;
            data.OverrideMap = overrideMap;
            MaterialExchange.Override(data.Renderer, overrideMap);

            data.AliveTimer = m_aliveInSeconds;
            data.Position = pos;
            var mat = data.Renderer.material;
            mat.SetVector(k_center, pos);
            m_rippleObjects.Add(data);

            tr.SetParent( group.Body.transform, true);
        }

        public void MoveEffectCenter(Quaternion angle, Vector3 thisPos, Vector3 moveVec)
        {
            for (var i = 0; i < m_rippleObjects.Count; i++)
            {
                var ripple = m_rippleObjects[i];
                var ripplePos = ripple.Position;
                var newPos = angle * (ripplePos - thisPos) + thisPos;
                ripple.Position = newPos + moveVec;
                ripple.Renderer.material.SetVector(k_center, ripple.Position);
                m_rippleObjects[i] = ripple;
            }
        }
    }
}
