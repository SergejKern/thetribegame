﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Game.Actors;
using Game.Actors.Components;
using Game.Actors.Controls;
using Game.Combat.Timeline;
using GameUtility.Interface;
using GameUtility.PooledEventSpawning;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Arena.EnemySpawn
{
    public class EnemySpawner : MonoBehaviour, IPoolable
    {
        public enum SpawnerState
        {
            MovingIn,
            ReadyToSpawn,
            MovingOut,
            MovedOut
        }

        struct SpawnedEnemy
        {
            public float Timer;
            public float EnemyMoveOutTime;
            public GameObject Enemy;
            public MoveControl MoveControl;
            public RigidbodyBehaviour Rigidbody;
            public NavMeshAgent NavMeshAgent;
            public float SideOffset;
            //public Vector3 StartPos;
            //public Vector3 EndPos;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_spawnOffsetHeight;
        [SerializeField] float m_spawnOffsetDistance;
        [SerializeField] float m_moveOutMeters;

        [SerializeField] RefIAudioAsset m_moveIn;
        [SerializeField] RefIAudioAsset m_moveInEnd;
        [SerializeField] RefIAudioAsset m_moveOut;
        [SerializeField] RefIAudioAsset m_moveOutEnd;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public Transform FlyTarget
        {
            get => m_flyTarget;
            set
            {
                m_posInterpolation = 0f;
                m_flyTarget = value;
                InitStartPosition();
            }
        }

        ArenaPlatform m_platform;
        bool m_railingsWereActive;
        public ArenaPlatform Platform
        {
            get => m_platform;
            set
            {
                m_platform = value;
                m_railingsWereActive = m_platform.FencesActive;
                m_platform.SetRailingsActive(false);
            }
        }

        public SpawnerState State { get; private set; }

        readonly List<SpawnedEnemy> m_spawnedEnemies = new List<SpawnedEnemy>();
        Transform m_flyTarget;
        float m_posInterpolation;
        float m_velocity;

        Vector3 StartPos => FlyTarget.position + Vector3.up * m_spawnOffsetHeight -
                            FlyTarget.forward * m_spawnOffsetDistance;


        bool m_moveOutAllowed;

        void InitStartPosition()
        {
            var tr = transform;
            tr.position = StartPos;
            tr.rotation = FlyTarget.rotation;
        }

        public void Update()
        {
            switch (State)
            {
                case SpawnerState.MovingIn: UpdateMovingIn(); break;
                case SpawnerState.ReadyToSpawn: UpdateReadyToSpawn(); break;
                case SpawnerState.MovingOut: UpdateMovingOut(); break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        void UpdateMovingIn()
        {
            InterpolatePosition(1f, 1f, 0.01f, 0.3f, false);

            if (m_posInterpolation < 1.0f)
                return;

            m_posInterpolation = 1.0f;
            State = SpawnerState.ReadyToSpawn;
            m_moveInEnd.Result?.Play(gameObject);
        }

        void UpdateMovingOut()
        {
            InterpolatePosition(-1f, 0f, 1f, 999f, true);
            if (m_posInterpolation > 0.0f) return;
            m_posInterpolation = 0.0f;

            State = SpawnerState.MovedOut;
            m_platform.SetRailingsActive(m_railingsWereActive);
            gameObject.TryDespawnOrDestroy();
        }

        void UpdateReadyToSpawn()
        {
            InterpolatePosition(0f, 1f, 0f, 0f, false);
            // test if all spawned enemies moved out of the platform

            if (m_spawnedEnemies.IsNullOrEmpty())
            {
                if (!m_moveOutAllowed)
                    return;
                MoveOut();
                return;
            }

            var tr = transform;
            var fwd = tr.forward;

            for (var i = m_spawnedEnemies.Count-1; i >= 0; i--)
            {
                var enemyDat = m_spawnedEnemies[i];
                enemyDat.Timer += Time.deltaTime;
                var lerpVal = enemyDat.Timer / enemyDat.EnemyMoveOutTime;

                var startPos = tr.position + enemyDat.SideOffset * tr.right;
                var endPos = startPos + tr.forward * m_moveOutMeters;
                var pos= Vector3.Lerp(startPos, endPos, lerpVal);

                enemyDat.Rigidbody.Position = pos;
                enemyDat.MoveControl.SetMoveInput(new Vector2(fwd.x, fwd.z));
                //enemyDat.Enemy.transform.position = pos;
                m_spawnedEnemies[i] = enemyDat;
                if (!(lerpVal >= 1.0f))
                    continue;
                if (enemyDat.Enemy.TryGetComponent(out Enemy e))
                    e.Activate();

                enemyDat.MoveControl.FakeMove = false;
                
                enemyDat.NavMeshAgent.Warp(pos);
                enemyDat.Rigidbody.Position = pos;

                m_spawnedEnemies.RemoveAt(i);
            }
        }

        void MoveOut()
        {
            if (State >= SpawnerState.MovingOut)
                return;
            State = SpawnerState.MovingOut;
            m_moveOut.Result?.Play(gameObject);
        }

        void InterpolatePosition(float dir, float target, float linearSpeed, float smoothDamp, bool smoothStep)
        {
            var tr = transform;

            m_posInterpolation = Mathf.Clamp(m_posInterpolation + linearSpeed * dir * Time.deltaTime, 0f, 1f);

            m_posInterpolation = Mathf.SmoothDamp(m_posInterpolation, target, ref m_velocity, smoothDamp);

            var smooth = smoothStep ? Mathf.SmoothStep(0f, 1f, m_posInterpolation) : m_posInterpolation;
            tr.position = Vector3.Lerp(StartPos, FlyTarget.position, smooth);
            tr.rotation = FlyTarget.rotation;
        }

        public void Spawn(EnemyConfig enemyConfig, float sideOffset = 0f, string debugName = null)
        {
            if (State == SpawnerState.MovingIn || State == SpawnerState.MovingOut)
                return;
            State = SpawnerState.ReadyToSpawn;

            var prefab = enemyConfig.Prefab;
            var tr = transform;
            var pos = tr.position + sideOffset * tr.right;
            var enemy = prefab.PooledSpawn(pos, tr.rotation, $"{prefab.name} | {debugName}");
            enemy.TryGetComponent(out RigidbodyBehaviour rbC);
            var navMeshAgent = enemy.GetComponentInChildren<NavMeshAgent>();
            MoveControl moveControl = null;
            if (enemy.TryGetComponent(out IProvider<MoveControl> agent))
            {
                agent.Get(out moveControl);
                moveControl.FakeMove = true;
            }

            m_spawnedEnemies.Add(new SpawnedEnemy()
            {
                Enemy = enemy,
                MoveControl = moveControl,
                Rigidbody = rbC,
                NavMeshAgent = navMeshAgent,
                Timer = 0f,
                EnemyMoveOutTime = m_moveOutMeters / enemyConfig.MoveSpeedInMetersPerSec,
                SideOffset = sideOffset,
                //StartPos = pos,
                //EndPos = pos + tr.forward * m_moveOutMeters
            });
        }

        public void AllowMoveOut() => m_moveOutAllowed = true;

        public void OnSpawn()
        {
            State = SpawnerState.MovingIn;
            m_moveOutAllowed = false;
            m_moveIn.Result?.Play(gameObject);
        }

        public void OnDespawn()
        {
            m_moveOutAllowed = false;
            m_moveOutEnd.Result?.Play(gameObject);
        }

        public bool IsVisible()
        {
            var cam = Camera.main;
            if (cam == null)
                return false;

            var vp = cam.WorldToViewportPoint(transform.position);
            return vp.x >= 0f && vp.x <= 1f
                              && vp.y >= 0f && vp.y <= 1f;
        }

        public void Stop() => MoveOut();
    }
}
