using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Game.Actors;
using Game.Configs;
using UnityEngine;

namespace Game.Arena.EnemySpawn
{
    public static class EnemyWaveGeneration
    {

        struct SpawnerTimeInfo
        {
            public int Idx;
            public float FreeTime;
        }

        public static List<EnemyWaveSpawnEntry> GenerateSpawnerEntries(ref EnemySpawnGeneratorData data)
        {
            if (data.SpawnerCount <= 0)
                return null;
            var spawnerEntries = new List<EnemyWaveSpawnEntry>(128);
            var rnd = new System.Random(data.Seed);

            var powerFactor = GetPowerFactor(data);
            InitSpawnerInfo(data, out var spawnerInfo);

            var lastTime = 0f;
            foreach (var spawnData in data.SpawnData)
            {
                if (spawnData.Delay > 0)
                    spawnerEntries.Add(new EnemyWaveSpawnEntry()
                    {
                        SpawnerIndex = -1,
                        EnemyConfig = null,
                        Time = lastTime + spawnData.Delay
                    });

                using (var pool = SimplePool<List<EnemyConfig>>.I.GetScoped())
                {
                    pool.Obj.AddRange(spawnData.EnemyTypes);
                    PickConfigsFromSetForPowerLevel(ref rnd,
                        pool.Obj,
                        spawnData.Power * powerFactor,
                        data.Options,
                        out var result);

                    DistributeEnemies(data, spawnData, result, spawnerInfo, spawnerEntries, ref lastTime);
                }
            }

            CalculateEnemySideOffsets(spawnerEntries);
            spawnerEntries.Sort((e1,e2)=> e1.Time.CompareTo(e2.Time));
            return spawnerEntries;
        }

        static float GetPowerFactor(EnemySpawnGeneratorData data)
        {
            var coopConfig = ConfigLinks.CoopDifficultyConfig;
            var difficultySettings = coopConfig.CoopSettings;
            var difficultySetting = difficultySettings[data.PlayerCount-1];
            var powerFactor = difficultySetting.SpawnEnemiesMultiplier;
            return powerFactor;
        }

        static void DistributeEnemies(EnemySpawnGeneratorData data, EnemySpawnData spawnData,
            Queue<EnemyConfig> result, SpawnerTimeInfo[] spawnerInfo,
            List<EnemyWaveSpawnEntry> spawnerEntries, ref float lastTime)
        {
            while (!result.IsNullOrEmpty())
            {
                var timeFactor = spawnData.TimeFactor > 1f ? spawnData.TimeFactor : 1f;
                var nextConfig = result.Dequeue();
                for (var i = 0; i < data.SpawnerCount; i++)
                    spawnerInfo[i].FreeTime = GetLatestFreeTimeForSpawner(spawnerEntries, timeFactor, spawnerInfo[i].Idx,
                        nextConfig.Radius <= k_sideOffsetRadius);

                Array.Sort(spawnerInfo, (a, b) => a.FreeTime.CompareTo(b.FreeTime));

                var entry = new EnemyWaveSpawnEntry
                {
                    Time = Mathf.Max(0f, spawnerInfo[0].FreeTime),
                    SpawnerIndex = spawnerInfo[0].Idx,
                    EnemyConfig = nextConfig
                };

                lastTime = entry.Time;
                spawnerEntries.Add(entry);
            }
        }

        static void InitSpawnerInfo(EnemySpawnGeneratorData data, out SpawnerTimeInfo[] spawnerInfo)
        {
            spawnerInfo = new SpawnerTimeInfo[data.SpawnerCount];
            for (var i = 0; i < data.SpawnerCount; i++)
            {
                spawnerInfo[i].Idx = i;
                spawnerInfo[i].FreeTime = 0;
            }
        }

        static void PickConfigsFromSetForPowerLevel(ref System.Random rnd, 
            IList<EnemyConfig> configSet, 
            float powerLevel, EnemySpawnOptions options, 
            out Queue<EnemyConfig> result)
        {
            result = new Queue<EnemyConfig>();

            var powerUsed = 0.0f;
            while (powerUsed < powerLevel)
            {
                var idx = rnd.Next() % configSet.Count;
                var chosenConf = configSet[idx];
                var exceeded = powerUsed + chosenConf.Power > powerLevel;

                if (exceeded && options.RemoveOnExceed && configSet.Count > 0)
                {
                    configSet.RemoveAt(idx);
                    continue;
                }
                powerUsed += chosenConf.Power;
                result.Enqueue(chosenConf);
            }
        }


        static bool IsWaitEntry(EnemyWaveSpawnEntry entry) => entry.SpawnerIndex == -1 && entry.EnemyConfig == null;

        const float k_sideOffsetRadius = 0.5f;
        // ReSharper disable once FlagArgument
        static float GetLatestFreeTimeForSpawner(IReadOnlyList<EnemyWaveSpawnEntry> spawnerEntries, 
            float timeFactor,
            int spawner, bool allowSideOffset)
        {
            for (var i = spawnerEntries.Count - 1; i >= 0; --i)
            {
                var entry = spawnerEntries[i];

                if (IsWaitEntry(entry))
                    return entry.Time;

                if (entry.SpawnerIndex != spawner)
                    continue;
                if (entry.EnemyConfig == null)
                    continue;

                var enemyConfig = entry.EnemyConfig;
                const float extraSpacing = 0.3f;
                var requiredSpace = enemyConfig.Radius * 2 + extraSpacing;
                var timeToMove = requiredSpace / enemyConfig.MoveSpeedInMetersPerSec * timeFactor;
                if (allowSideOffset && enemyConfig.Radius <= k_sideOffsetRadius)
                    timeToMove *= 0.5f;
                return entry.Time + timeToMove;
            }

            return 0f;
        }

        static void CalculateEnemySideOffsets(IList<EnemyWaveSpawnEntry> spawnerEntries)
        {
            const float extraSpacing = 0.3f;

            var laneLeftRight = new bool[32];
            for (var i = 0; i < spawnerEntries.Count; ++i)
            {
                var entry = spawnerEntries[i];
                if (entry.EnemyConfig == null)
                    continue;
                if (!entry.SpawnerIndex.IsInRange(laneLeftRight))
                    continue;
                if (entry.EnemyConfig.Radius > k_sideOffsetRadius)
                    continue;
                var leftRight = laneLeftRight[entry.SpawnerIndex];
                laneLeftRight[entry.SpawnerIndex] = !leftRight;
                entry.SideOffset = leftRight ? 1f : -1f;
                entry.SideOffset *= entry.EnemyConfig.Radius + extraSpacing * 0.5f;
                spawnerEntries[i] = entry;
            }
        }
    }
}
