using Core.Unity.Attribute;
using Game.Actors;

namespace Game.Arena.EnemySpawn
{
    [System.Serializable]
    public struct EnemySpawnData
    {
        [Color(1f,0.5f,0f)]
        public float Power;
        public float Delay;
        [Range(1, 100f)]
        public float TimeFactor;

        //[ReorderableList()]
        public EnemyConfig[] EnemyTypes;

        //public static EnemySpawnData Default =>
            //new EnemySpawnData() {Power = 10f, Delay = 0f, TimeFactor = 1f, EnemyTypes = new EnemyConfig[0]};
    }

    [System.Serializable]
    public struct EnemySpawnOptions
    {
        public bool RemoveOnExceed;
    }

    public struct EnemySpawnGeneratorData
    {
        public int Seed;
        public int SpawnerCount;
        public int PlayerCount;

        public EnemySpawnOptions Options;
        public EnemySpawnData[] SpawnData;
    }

    public struct EnemyWaveSpawnEntry
    {
        public float Time;
        public int SpawnerIndex;

        public EnemyConfig EnemyConfig;

        public float SideOffset;
    }
}
