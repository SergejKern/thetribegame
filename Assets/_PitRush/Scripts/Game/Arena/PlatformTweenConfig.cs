using Core.Unity.Interface;
using DG.Tweening;
using UnityEngine;

namespace Game.Arena
{
    [CreateAssetMenu(menuName = "Game/Arena/PlatformTweenConfig")]
    public class PlatformTweenConfig : ScriptableObject
    {
        public float DurationPerUnit;
        public float DurationVariance;

        public Ease Ease;
        public AnimationCurve Curve;

        public RefIAudioAsset MoveSound;
        public RefIAudioAsset MoveEndSound;

        public PlatformWiggleConfig WiggleConfig;
    }
}
