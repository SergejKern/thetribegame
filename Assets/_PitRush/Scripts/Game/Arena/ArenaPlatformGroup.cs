using System.Collections.Generic;
using Core.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Globals;
using UnityEngine;

namespace Game.Arena
{
    public class ArenaPlatformGroup: MonoBehaviour
    {
        static ArenaPlatformGroup m_defaultGroup;
        public static ArenaPlatformGroup DefaultGroup
        {
            get
            {
                if (m_defaultGroup != null)
                    return m_defaultGroup;

                var groupGo = GameGlobals.Prefabs.GetPrefab(Prefab.PlatformMovingGroup).GetPooledInstance(Vector3.zero, Quaternion.identity);
                groupGo.TryGetComponent(out m_defaultGroup);
                return m_defaultGroup;
            }
        }

        public Rigidbody Body;
        public List<ArenaPlatform> Platforms = new List<ArenaPlatform>();
        public MeshFilter MeshFilter;
        public Mesh Mesh
        {
            get => MeshFilter.sharedMesh;
            private set => MeshFilter.sharedMesh = value;
        }

        void OnDisable() => Platforms.Clear();

        public void BuildGroupFromSelection(IArenaSelection selection, Vector3? position = null)
        {
            if (!position.HasValue)
                position = GetAveragePosition(selection.Platforms);

            TryGetComponent(out Rigidbody body);
            body.position = position.Value;
            transform.position = position.Value;

            Platforms.AddRange(selection.Platforms);

            UpdateMovingGroups(selection, this);

            BuildMesh();
        }

        public void AddSelection(IArenaSelection selection)
        {
            Platforms.AddRange(selection.Platforms);
            BuildMesh();
            UpdateMovingGroups(selection, this);
        }

        public static void UpdateMovingGroups(IArenaSelection selection, ArenaPlatformGroup newGroup)
        {
            using (var prev = SimplePool<List<ArenaPlatformGroup>>.I.GetScoped())
            {
                ChangeMovingGroup(selection, newGroup, prev.Obj);
                foreach (var movingGroup in prev.Obj)
                    movingGroup.RemovePlatformsAndRebuild(selection.Platforms);
            }
        }

        static Vector3 GetAveragePosition(IReadOnlyCollection<ArenaPlatform> platforms)
        {
            var position = Vector3.zero;
            foreach (var p in platforms)
            {
                p.TryGetComponent(out Rigidbody pBody);
                position += pBody.position;
            }

            position /= platforms.Count;
            //position.y = 0f;
            return position;
        }

        static void ChangeMovingGroup(IArenaSelection selection, ArenaPlatformGroup newGroup, ICollection<ArenaPlatformGroup> prev)
        {
            foreach (var p in selection.Platforms)
            {
                var prevGroup = p.Group;
                p.Group = newGroup;
                p.transform.SetParent(newGroup != null ? newGroup.transform : null, true);
                p.SetDirty();

                if (prevGroup == null || prev.Contains(prevGroup))
                    continue;
                prev.Add(prevGroup);
            }
        }

        public void RemovePlatformsAndRebuild(ICollection<ArenaPlatform> selectionPlatforms)
        {
            Platforms.RemoveAll(selectionPlatforms.Contains);
            BuildMesh();
        }

        void BuildMesh()
        {            
            if (Mesh != null)
            {
                //Debug.Log($"{gameObject.name} Return {Mesh.GetInstanceID()} ");
                SimplePool<Mesh>.I.Return(Mesh);
            }   
            var m = SimplePool<Mesh>.I.Get();
            //Debug.Log($"{gameObject.name} Got mesh instance {m.GetInstanceID()} ");
            m.Clear();
            var inst = GetCombineInstanceList();
            m.CombineMeshes(inst, true, true);

            Mesh = m;
        }

        CombineInstance[] GetCombineInstanceList()
        {
            using (var combine = SimplePool<List<CombineInstance>>.I.GetScoped())
            {
                foreach (var p in Platforms)
                {
                    if (!HasValidMesh(p)) 
                        continue;
                    for (var i = 0; i < p.Filter.sharedMesh.subMeshCount; i++)
                    {
                        var ci = new CombineInstance
                        {
                            mesh = p.Filter.sharedMesh,
                            subMeshIndex = i,
                            transform = transform.worldToLocalMatrix * p.Filter.transform.localToWorldMatrix
                        };
                        combine.Obj.Add(ci);
                    }
                }

                var arr = combine.Obj.ToArray();
                return arr;
            }
        }

        static bool HasValidMesh(ArenaPlatform p)
        {
            if (p == null)
            {
                Debug.LogError("Null-Platform in group");
                return false;
            }

            if (p.Filter == null || p.Filter.sharedMesh == null)
            {
                Debug.LogWarning($"Filter or mesh not set in platform {p}");
                return false;
            }

            return true;
        }
    }
}