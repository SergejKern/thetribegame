﻿using System;
using Core.Unity.Attribute;
using UnityEngine;
using static Core.Unity.Extensions.CoreEnumExtensions;

namespace Game.Arena
{
    [CreateAssetMenu(menuName = "Game/ArenaLayoutConfigCircular")]
    public class ArenaLayoutConfigCircular : ArenaLayoutConfig, ISerializationCallbackReceiver
    {
        [Serializable]
        public struct RingPrefabData
        {
            public GameObject[] RingPlatforms;
        }

        [Serializable]
        public struct ArenaPrefabVariantData
        {
            [SerializeField, HideInInspector] 
            internal string VariantString;
            [EnumStringSync(nameof(VariantString))]
            public ArenaPrefabVariant Variant;
            public GameObject Prefab;
            public RingPrefabData[] RingData;
            public GameObject Center;
        }

        public static readonly Vector3Int CenterIdx = new Vector3Int(0, 0, -1);

        public int Segments;
        public int Rings => Array.Find(VariantData, v => v.Variant == ArenaPrefabVariant.Default).RingData.Length;
        public int GetSegmentCount(int ring) => Array.Find(VariantData, v => v.Variant == ArenaPrefabVariant.Default)
            .RingData[ring].RingPlatforms.Length;
        public float AnglesPerSegment => 360f / Segments;

        public ArenaPrefabVariantData[] VariantData = new ArenaPrefabVariantData[0];

        public void OnBeforeSerialize()
        {
            for (var i = 0; i < VariantData.Length; i++)
                OnBeforeSerializeEnum(out VariantData[i].VariantString, VariantData[i].Variant);
        }

        public void OnAfterDeserialize()
        {
            for (var i = 0; i < VariantData.Length; i++)
                OnAfterDeserializeEnum(ref VariantData[i].Variant, VariantData[i].VariantString);
        }

        public override void GetDefaultTransformation(Vector3Int idx, GameObject prefab, out Vector3 pos, out Quaternion rot)
        {
            pos = Vector3.zero;
            rot = Quaternion.Euler(0, idx.x * AnglesPerSegment, 0);
            if (prefab!=null)
                rot *= prefab.transform.rotation;
        }

        public override GameObject GetPrefab(Vector3Int idx, ArenaPrefabVariant variant)
        {
            var variantData = Array.Find(VariantData, v => v.Variant == variant);
            if (idx.Equals(CenterIdx))
                return variantData.Center;
            var ring = variantData.RingData[idx.y];
            return ring.RingPlatforms[idx.z];
        }

        public override LayoutIndexEnum GetIndexEnumerator() => new LayoutIndexCircularEnum(this);
        public override bool IsValid(Vector3Int idx)
        {
            if (idx.x < 0 || idx.x >= Segments)
                return false;
            if (idx.y < 0 || idx.y >= Rings)
                return false;

            var ring = VariantData[0].RingData[idx.y];
            return idx.z >= -1 && idx.z < ring.RingPlatforms.Length;
        }

        public override Vector3Int GetValidIndex(Vector3Int idx)
        {
            if (idx.z == -1)
                return CenterIdx;
            while (idx.x < 0)
                idx.x += Segments;
            while (idx.x>=Segments)
                idx.x -= Segments;
            return idx;
        }
    }

    public class LayoutIndexCircularEnum : LayoutIndexEnum
    {
        public LayoutIndexCircularEnum(ArenaLayoutConfigCircular circularLayout) => m_circularLayout = circularLayout;
        readonly ArenaLayoutConfigCircular m_circularLayout;
        int m_segmentIdx;
        int m_ringIdx;
        int m_platformIdx = -2;

        public override bool MoveNext()
        {
            if (m_platformIdx+1 < m_circularLayout.GetSegmentCount(m_ringIdx))
            {
                if (Current.Equals(ArenaLayoutConfigCircular.CenterIdx))
                {
                    m_platformIdx = 0;
                    m_ringIdx = 0;
                    m_segmentIdx = 0;
                    return true;
                }
                m_platformIdx++;
                return true;
            }

            m_platformIdx = 0;
            if (m_ringIdx+1 < m_circularLayout.Rings)
            {
                m_ringIdx++;
                return true;
            }

            m_ringIdx = 0;
            if (m_segmentIdx+1 < m_circularLayout.Segments)
            {
                m_segmentIdx++;
                return true;
            }

            return false;
        }

        public override void Reset()
        {
            m_segmentIdx = 0;
            m_ringIdx = 0;
            m_platformIdx = -2;
        }

        protected override Vector3Int Current => CircularPlatformExtensions.CreateIndexCircular(m_segmentIdx, m_ringIdx, m_platformIdx);
    }

    public static class CircularPlatformExtensions {
        // ReSharper disable UnusedMember.Global
        public static void GetIndexCircular(this ArenaPlatform p, out int segment, out int ringIdx, out int platformIdx)
        {
            segment = p.Index.x;
            ringIdx = p.Index.y;
            platformIdx = p.Index.z;
        }

        static void SetIndexCircular(this ArenaPlatform p, int segment, int ringIdx, int platformIdx) =>
            p.SetIndex(CreateIndexCircular(segment, ringIdx, platformIdx));
        public static Vector3Int CreateIndexCircular(int segment, int ringIdx, int platformIdx) =>
            new Vector3Int(segment, ringIdx, platformIdx);

        public static void SetSegment(this ArenaPlatform platform, int segment) => 
            platform.SetIndexCircular(segment, platform.Index.y, platform.Index.z);
        public static void SetRing(this ArenaPlatform platform, int ringIdx) =>
            platform.SetIndexCircular(platform.Index.x, ringIdx, platform.Index.z);
        public static void SetPlatformIdx(this ArenaPlatform platform, int platformIdx) =>
            platform.SetIndexCircular(platform.Index.x, platform.Index.y, platformIdx);
        // ReSharper restore UnusedMember.Global
    }
}
