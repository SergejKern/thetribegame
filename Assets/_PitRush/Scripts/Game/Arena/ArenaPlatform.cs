﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using DG.Tweening;
using Game.ActionConfig;
using Game.Actors.Components;
using Game.Arena.Level;
using Game.Components.Events;
using JetBrains.Annotations;
using ScriptableUtility.Actions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using static System.Single;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Game.Arena
{
    public class ArenaPlatform : MonoBehaviour, IPoolable
    {
        [Serializable]
        public struct PlatformID
        {
            public ArenaLevelFlowConfig.ArenaLayoutData LayoutData
            {
                get
                {
                    var flow = ArenaLevelFlowConfig.Current;
                    return flow.GetLayoutData(UniqueLayoutIdx);
                }
            }

            //public ArenaLayoutConfig Layout;
            public int UniqueLayoutIdx;
            public Vector3Int Index;

            public bool IsValid
            {
                get
                {
                    var flow = ArenaLevelFlowConfig.Current;
                    if (flow == null) return false;
                    return flow.IsValidLayoutID(UniqueLayoutIdx)
                           && LayoutData.IsValid(Index);
                }
            }

            public static bool operator ==(PlatformID a, PlatformID b) 
                => a.UniqueLayoutIdx == b.UniqueLayoutIdx && a.Index == b.Index;
            public static bool operator !=(PlatformID a, PlatformID b)
                => a.UniqueLayoutIdx != b.UniqueLayoutIdx || a.Index != b.Index;

            public bool Equals(PlatformID other) => UniqueLayoutIdx == other.UniqueLayoutIdx && Index.Equals(other.Index);
            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                return obj is PlatformID other && Equals(other);
            }
            public override int GetHashCode()
            {
                unchecked
                {
                    return (UniqueLayoutIdx * 397) ^ Index.GetHashCode();
                }
            }
        }

        // ReSharper disable UnusedMember.Global
        public enum EnemySpawnerAttachmentSide
        {
            Inner,
            Outer,
            Left,
            Right
        }
        // ReSharper restore UnusedMember.Global

        [Serializable]
        public struct EnemySpawnerAttachData
        {
            public EnemySpawnerAttachmentSide Side;
            public Transform AttachmentPos;
        }

        [Serializable]
        public struct PlatformBorders
        {
            public Vector3Int RelativeIndex;
            [FormerlySerializedAs("BorderRenderer")]
            public GameObject BorderObject;
        }

        public PlatformID ID;
        public Vector3Int Index => ID.Index;

        public ArenaPrefabVariant Variant;


        [FormerlySerializedAs("Attachments")]
        public EnemySpawnerAttachData[] EnemySpawnerAttachments;

        public MeshFilter Filter;
        public Collider Collider;
        //void OnValidate()
        //{
        //    Filter = GetComponentInChildren<MeshFilter>();
        //    Collider = GetComponentInChildren<Collider>();
        //    EditorUtility.SetDirty(gameObject);
        //}

#pragma warning disable 0649 // wrong warning
        [FormerlySerializedAs("m_borders")]
        [SerializeField] PlatformBorders[] m_highlightBorders;

        [SerializeField] UnityEvent m_onAddToPlatform;
        [SerializeField] PrefabRef m_fencesPrefab;
        [SerializeField] public PrefabData FencesPrefabData;

        [SerializeField] bool m_isUsuallySafe;

        [SerializeField] PlatformTrailVfxSpawner m_spawner;
        [ToggleButton("DrawGizmos", nameof(DrawGizmos))]
        [SerializeField, UsedImplicitly]
        bool m_drawGizmos;
#pragma warning restore 0649 // wrong warning
        Renderer[] m_highlightBordersCachedRenderer;

        //public TransformData[] OnPlatformAttachments;
        readonly List<Transform> m_onPlatformStatics = new List<Transform>();
        readonly List<RigidbodyBehaviour> m_onPlatformAgents = new List<RigidbodyBehaviour>();

        Sequence m_tweenSequence;

        Vector3 m_lastPos;
        Quaternion m_lastRotation;

        bool m_needUpdateBorders;
        int m_updatedBorderFrame;
        int m_dirtyFrame;

        ArenaPlatformFences m_fencesInstance;

        Rigidbody m_cachedRigidbody;

        public bool FencesActive => m_fencesInstance != null && m_fencesInstance.Active && MovingAction == null;

        public bool IsMoving { get; set; }
        public IBaseAction MovingAction
        {
            get => m_movingAction;
            set
            {
                if (m_movingAction == value)
                    return;
                if (m_movingAction != null)
                    UpdateOnPlatformObjects();

                m_movingAction = value;
                IsMoving = m_movingAction != null;

                SetDirty();

                NotifyMoveChanged();
                TryGetComponent(out m_cachedRigidbody);

                var tr = transform;
                m_lastRotation = tr.rotation;
                m_lastPos = tr.position;
            }
        }
        public ArenaPlatformGroup Group { get; set; }

        // if group is null platform is probably going to move down & despawn
        bool MovingUpDown => m_movingAction is PlatformMoveUpAndDownAction;
        bool MovingDown => m_movingAction is PlatformMoveUpAndDownAction moveUpAndDown
                             && moveUpAndDown.MoveHeight < -1;
        //bool DespawningProbable => Group == null;
        public bool IsCurrentlySafe => m_isUsuallySafe && !MovingUpDown;
        //public ArenaPlatformMovingGroup MovingGroup;
        IBaseAction m_movingAction;

        static bool DrawGizmos { get; set; }

        public Vector3 OnPlatformPosition
        {
            get
            {
                if (Collider == null)
                {
                    Debug.LogError($"Platform {this} Collider is missing!");
                    return default;
                }

                var cTr = Collider.transform;
                var pos = cTr.position;
                pos.y = Collider.bounds.max.y;
                return pos;
            }
        }


        void OnDisable()
        {
            m_onPlatformAgents.Clear();
            m_onPlatformStatics.Clear();
        }

        public void Init(PlatformID id, ArenaPrefabVariant variant)
        {
            SetDirty();
            ID = id;
            Variant = variant;
            if (m_spawner == null)
                TryGetComponent(out m_spawner);
        }

        public void SetDirty() => SetDirty(Time.frameCount);

        void SetDirty(int dirtyFrame)
        {
            if (m_updatedBorderFrame >= dirtyFrame)
                return;

            m_dirtyFrame = dirtyFrame;
            m_needUpdateBorders = true;
        }

        public void SetIndex(Vector3Int index) => ID.Index = index;

        //public void AddToPlatform(GameObject g)
        //{
        //    if (g.TryGetComponent(out RigidbodyComponent r))
        //    {
        //        AddAgentToPlatform(r);
        //        return;
        //    }
        //    AddStaticToPlatform(g.transform);
        //}

        public void AddStaticToPlatform(Transform tr)
        {
            tr.SetParent(transform, true);
            m_onPlatformStatics.Add(tr);
            m_onAddToPlatform.Invoke();
        }

        public void AddAgentToPlatform(RigidbodyBehaviour r)
        {
            // avoid add on despawning platform, or it will drag player down?
            if (MovingDown)
                return;
            //r.GetBody().velocity = m_cachedRigidbody.velocity;
            if (m_onPlatformAgents.Contains(r))
                return;

            m_onPlatformAgents.Add(r);
            m_onAddToPlatform.Invoke();
        }

        void NotifyMoveChanged()
        {
            var isMoving = m_movingAction != null;
            foreach (var go in m_onPlatformStatics)
            {
               if (go.TryGetComponent<IPlatformMoveListener>(out var listener))
                   listener.OnPlatformMoveChangeEvent(transform, isMoving, MovingUpDown);
            }
        }

        public void RemoveAgentPlatform(RigidbodyBehaviour r) => m_onPlatformAgents.Remove(r);
        public void RemoveStaticFromPlatform(Transform tr)
        {
            m_onPlatformStatics.Remove(tr);
            tr.SetParent(null);
        }

        // ReSharper disable once FlagArgument
        public void SetRailingsActive(bool active, Vector3Int[] custom = null)
        {
            if (active && m_fencesInstance == null)
            {
                var tr = transform;
                GameObject fencesGo = null;
                if (FencesPrefabData.Prefab != null)
                    fencesGo = FencesPrefabData.Prefab.GetPooledInstance(tr.position, tr.rotation);
                else if (m_fencesPrefab != null)
                    fencesGo = m_fencesPrefab.GetInstance();
                if (fencesGo == null)
                    return;

                m_fencesInstance = fencesGo.GetComponent<ArenaPlatformFences>();
                m_fencesInstance.Platform = this;
                fencesGo.transform.SetParent(tr);
                m_fencesInstance.SetCustom(custom);
            }
            if (m_fencesInstance == null)
                return;
            m_fencesInstance.SetFencesActive(active);
            m_needUpdateBorders = true;
        }

        void OnEnable()
        {
            m_lastRotation = transform.rotation;
            SetUvs();
        }

        void Update()
        {
            UpdateHighlightBorderVisibility();
            //UpdateRailingVisibility();
        }

        public void UpdateHighlightBorderVisibility()
        {
            if (!m_needUpdateBorders)
                return;

            //var debugLog = $"{nameof(UpdateHighlightBorderVisibility)}: Index {Index} MovingAction {MovingAction}";

            for (var i = 0; i < m_highlightBorders.Length; i++)
            {
                var idx = Index + m_highlightBorders[i].RelativeIndex;
                idx = ID.LayoutData.Config.GetValidIndex(idx);
                var platform = ArenaPlatformMapData.PlatformData.GetPlatform(new PlatformID
                {
                    UniqueLayoutIdx = ID.UniqueLayoutIdx,
                    Index = idx,
                });

                var wasActive = m_highlightBorders[i].BorderObject.activeSelf;
                var newActive = !FencesActive && (platform == null || platform.MovingAction != MovingAction ||
                                                  platform.Group !=Group);

                //debugLog = $"{debugLog}\n relative idx {idx} platform exists? {platform!= null} " +
                //           $"same moving action? {platform?.MovingAction == MovingAction}" +
                //           $"same moving Group? {platform?.Group == Group}" +
                //           $"was active {wasActive} new Active {newActive}";

                m_highlightBorders[i].BorderObject.SetActive(newActive);
                if (newActive != wasActive && platform != null)
                    platform.SetDirty(m_dirtyFrame);
            }
            //Debug.Log(debugLog);

            m_updatedBorderFrame = m_dirtyFrame;
            m_needUpdateBorders = false;
        }

        void FixedUpdate() => UpdateOnPlatformObjects();

        void UpdateOnPlatformObjects()
        {
            if (!IsMoving)
                return;

            TryGetComponent(out m_cachedRigidbody);

            var thisPos = m_cachedRigidbody.position;

            var rotDiff = Quaternion.Inverse(m_lastRotation) *  m_cachedRigidbody.rotation;
            var moveVec = thisPos - m_lastPos;
            var yawRollDeg = rotDiff.GetPitchYawRollDeg();

            var amount = yawRollDeg.y+yawRollDeg.z;
            //Debug.Log($"name: {name} rotation: {RelevantRigidbody.rotation.ToString("F3")} rotDiff: {rotDiff.ToString("F3")} amount: {yawRollDeg.ToString("F3")}");

            var angle = Quaternion.AngleAxis(amount, Vector3.up);
            for (var i = m_onPlatformAgents.Count - 1; i >= 0; i--)
            {
                var r = m_onPlatformAgents[i];
                if (Mathf.Abs(amount) < Epsilon && moveVec.sqrMagnitude < Epsilon)
                    continue;
                MoveRigidWithPlatform(angle, thisPos, moveVec, r);
            }

            if (m_spawner != null)
                m_spawner.MoveEffectCenter(angle, thisPos, moveVec);

            m_lastRotation = m_cachedRigidbody.rotation;
            m_lastPos = thisPos;
        }

        static void MoveRigidWithPlatform(Quaternion angle, Vector3 thisPos, Vector3 moveVec, RigidbodyBehaviour rigidBody)
        {
            var newPos = angle * (rigidBody.Position - thisPos) + thisPos;
            //Debug.Log($"Platform Moved {rigidBody.name} by y {moveVec.y:F3}");
            //rigidBody.GetBody().velocity = m_cachedRigidbody.velocity;

            rigidBody.Position = newPos + moveVec;
            rigidBody.Rotation *= angle;
        }

        public override string ToString() => base.ToString() + Index;

        public Renderer[] GetBorderRenderer()
        {
            if (m_highlightBordersCachedRenderer != null)
                return m_highlightBordersCachedRenderer;

            //if (m_cacheThisRendererForHighlight)
            //{
            //    m_highlightBordersCachedRenderer = GetComponentsInChildren<Renderer>();
            //    return m_highlightBordersCachedRenderer;
            //}
            m_highlightBordersCachedRenderer = new Renderer[m_highlightBorders.Length];
            for (var i = 0; i < m_highlightBorders.Length; i++)
                m_highlightBordersCachedRenderer[i] = m_highlightBorders[i].BorderObject.GetComponent<Renderer>();

            return m_highlightBordersCachedRenderer;
        }

        public void RemoveAllAgents() => m_onPlatformAgents.Clear();

        public void OnSpawn() => SetRailingsActive(false);
        public void OnDespawn()
        {
            for (var index = m_onPlatformStatics.Count-1; index >= 0; index--)
            {
                var tr = m_onPlatformStatics[index];
                RemoveStaticFromPlatform(tr);
                tr.gameObject.TryDespawnOrDestroy();
            }

            m_onPlatformAgents.Clear();
            m_onPlatformStatics.Clear();

            if (m_fencesInstance != null)
                m_fencesInstance.gameObject.TryDespawnOrDestroy(true);
            m_fencesInstance = null;
        }

        void OnDrawGizmos()
        {
            if (!DrawGizmos)
                return;
#if UNITY_EDITOR
            var mr = GetComponentInChildren<Renderer>();
            Handles.Label(mr.transform.position + Vector3.up - Vector3.left, $"({Index.x}, {Index.y}, {Index.z})");
            Handles.color = Color.white;
#endif
        }

        void SetUvs()
        {
            var mr = Filter;
            if (mr == null)
                return;

            var mesh = mr.mesh;
            var tr = mr.transform;
            var pos = tr.position;
            var uvs = mesh.uv;
            // Iterate over each face (here assuming triangles)
            //for (var i = 0; i < mesh.uv.Length; i += 3)
            //{
            //    var normal = mesh.normals[i];
            //    var worldNormal = tr.TransformVector(normal);
            //    var blendWeights = new Vector3(Mathf.Abs(worldNormal.x),
            //        Mathf.Abs(worldNormal.y), Mathf.Abs(worldNormal.z));
            //    blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);

            //    var blend = new Vector2(pos.z, pos.y) * blendWeights.x
            //                + new Vector2(pos.x, pos.z) * blendWeights.y
            //                + new Vector2(pos.x, pos.y) * blendWeights.z;
            //    mesh.uv[i] += blend;
            //}


            for (var i = 0; i < mesh.triangles.Length; i += 3)
            {
                var t1 = mesh.triangles[i];
                var t2 = mesh.triangles[i+1];
                var t3 = mesh.triangles[i+2];

                var worldV1 = pos+tr.TransformVector(mesh.vertices[t1]);
                var worldV2 = pos+tr.TransformVector(mesh.vertices[t2]);
                var worldV3 = pos+tr.TransformVector(mesh.vertices[t3]);

                var normal = (mesh.normals[t1] + mesh.normals[t2] + mesh.normals[t3]) / 3;
                var worldNormal = tr.TransformDirection(normal);

                var blendWeights = new Vector3( Mathf.Abs(worldNormal.x),
                    Mathf.Abs(worldNormal.y), Mathf.Abs(worldNormal.z));
                blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);


                //var blend = new Vector2(pos.z, pos.y) * blendWeights.x
                //               + new Vector2(pos.x, pos.z) * blendWeights.y
                //               + new Vector2(pos.x, pos.y) * blendWeights.z;
                //mesh.uv[t1] += blend;
                //mesh.uv[t2] += blend;
                //mesh.uv[t3] += blend;

                uvs[t1] = new Vector2(worldV1.z, worldV1.y) * blendWeights.x
                             + new Vector2(worldV1.x, worldV1.z) * blendWeights.y
                             + new Vector2(worldV1.x, worldV1.y) * blendWeights.z;
                uvs[t2] = new Vector2(worldV2.z, worldV2.y) * blendWeights.x
                             + new Vector2(worldV2.x, worldV2.z) * blendWeights.y
                             + new Vector2(worldV2.x, worldV2.y) * blendWeights.z;
                uvs[t3] = new Vector2(worldV3.z, worldV3.y) * blendWeights.x
                             + new Vector2(worldV3.x, worldV3.z) * blendWeights.y
                             + new Vector2(worldV3.x, worldV3.y) * blendWeights.z;
            }

            mesh.uv = uvs;
            mr.mesh = mesh;
        }
    }
}
