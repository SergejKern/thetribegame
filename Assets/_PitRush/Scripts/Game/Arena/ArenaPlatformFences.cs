﻿using System;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Extensions;
using UnityEngine;

namespace Game.Arena
{
    public class ArenaPlatformFences : MonoBehaviour, IPoolable
    {
        [Serializable]
        public struct PlatformBorders
        {
            public Vector3Int RelativeIndex;
            public GameObject BorderObject;
        }

#pragma warning disable 0649 // wrong warning
        [SerializeField] PlatformBorders[] m_fences;
        [SerializeField] Vector3Int[] m_customFenceLayout;
#pragma warning restore 0649 // wrong warning

        public ArenaPlatform Platform;
        public bool Active { get; private set; }

        bool m_needUpdateFences;

        public void SetFencesActive(bool active)
        {
            Active = active;
            m_needUpdateFences = true;
        }

        void Update() => UpdateRailingVisibility();

        void UpdateRailingVisibility()
        {
            if (!m_needUpdateFences || Platform == null)
                return;

            for (var i = 0; i < m_fences.Length; i++)
            {
                var idx = Platform.Index + m_fences[i].RelativeIndex;
                idx = Platform.ID.LayoutData.Config.GetValidIndex(idx);

                var isCustom = !m_customFenceLayout.IsNullOrEmpty();
                bool setActive;
                if (isCustom)
                    setActive = Array.Exists(m_customFenceLayout, fi => fi == idx);
                else
                {
                    var platform = ArenaPlatformMapData.PlatformData.GetPlatform(new ArenaPlatform.PlatformID
                    {
                        UniqueLayoutIdx = Platform.ID.UniqueLayoutIdx,
                        Index = idx
                    });
                    setActive = platform == null;
                }

                m_fences[i].BorderObject.SetActiveWithTransition(Active && setActive);
            }

            m_needUpdateFences = false;
        }

        public void OnSpawn() { }

        public void OnDespawn()
        {
            SetFencesActive(false);
            for (var i = 0; i < m_fences.Length; i++)
                m_fences[i].BorderObject.SetActive(false);
            Platform = null;
        }

        public void SetCustom(Vector3Int[] custom) => m_customFenceLayout = custom;
    }
}
