﻿using System;
using Core.Unity.Attribute;
using Core.Unity.Types;
using UnityEngine;
using static Core.Unity.Extensions.CoreEnumExtensions;

namespace Game.Arena
{
    [CreateAssetMenu(menuName = "Game/ArenaLayoutConfigGrid")]
    public class ArenaLayoutConfigGrid : ArenaLayoutConfig, ISerializationCallbackReceiver
    {
        [Serializable]
        public struct ArenaPrefabVariantData
        {
            [SerializeField, HideInInspector] 
            internal string VariantString;
            [EnumStringSync(nameof(VariantString))]
            public ArenaPrefabVariant Variant;
            public PrefabData Prefab;
        }

        public int BlockSize;
        public int GridElementCount;
        int GridSize => BlockSize * GridElementCount;

        float Offset => -0.5f * GridSize;

        public ArenaPrefabVariantData[] VariantData = new ArenaPrefabVariantData[0];

        public void OnBeforeSerialize()
        {
            for (var i = 0; i < VariantData.Length; i++)
                OnBeforeSerializeEnum(out VariantData[i].VariantString, VariantData[i].Variant);
        }

        public void OnAfterDeserialize()
        {
            for (var i = 0; i < VariantData.Length; i++)
                OnAfterDeserializeEnum(ref VariantData[i].Variant, VariantData[i].VariantString);
        }

        public override void GetDefaultTransformation(Vector3Int idx, GameObject _, out Vector3 pos, out Quaternion rot)
        {
            pos = new Vector3(Offset + BlockSize * idx.x, 0, Offset + BlockSize * idx.y);
            rot = Quaternion.identity;
        }

        public override GameObject GetPrefab(Vector3Int idx, ArenaPrefabVariant variant)
        {
            var variantData = Array.Find(VariantData, v => v.Variant == variant);
            return variantData.Prefab.Prefab;
        }

        public override LayoutIndexEnum GetIndexEnumerator() => new LayoutIndexGridEnum(GridElementCount);
        public override bool IsValid(Vector3Int idx)
        {
            return idx.x >= 0 && idx.x < GridElementCount &&
                   idx.y >= 0 && idx.y < GridElementCount;
        }

        public override Vector3Int GetValidIndex(Vector3Int idx) => idx;
    }

    public class LayoutIndexGridEnum : LayoutIndexEnum
    {
        public LayoutIndexGridEnum(int elementCount) => m_elementCount = elementCount;

        readonly int m_elementCount;
        int m_xIdx;
        int m_yIdx = -1;

        public override bool MoveNext()
        {
            if (m_yIdx+1 < m_elementCount)
            {
                m_yIdx++;
                return true;
            }

            m_yIdx = 0;
            if (m_xIdx+1 < m_elementCount)
            {
                m_xIdx++;
                return true;
            }

            return false;
        }

        public override void Reset()
        {
            m_xIdx = 0;
            m_yIdx = -1;
        }

        protected override Vector3Int Current => new Vector3Int(m_xIdx, m_yIdx,0);
    }
}
