using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Extensions;

namespace Game.Arena
{
    public class ArenaPlatformMapData : IArenaPlatformData
    {
        public static IArenaPlatformData PlatformData;

        readonly Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData> m_platformInstances = new Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData>();
        public void ClearMap() => m_platformInstances.Clear();

        public void DestroyArena()
        {
            if (m_platformInstances.IsNullOrEmpty())
                return;

            foreach (var instanceData in m_platformInstances.Values)
            {
                var platform = instanceData.PlatformInstance;
                if (platform == null)
                    continue;
                platform.gameObject.DestroyEx();
            }

            m_platformInstances.Clear();
        }

        public Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData> GetPlatformMap() => m_platformInstances;

        public ArenaPlatform GetPlatform(ArenaPlatform.PlatformID id) => 
            m_platformInstances.TryGetValue(id, out var instanceData) ? instanceData.PlatformInstance : null;
    }

    // helper struct to pre-setup platform variant to spawn
    public struct PlatformInstanceData
    {
        public ArenaPrefabVariant Variant;
        public ArenaPlatform PlatformInstance;
    }

    public interface IArenaPlatformData
    {
        Dictionary<ArenaPlatform.PlatformID, PlatformInstanceData> GetPlatformMap();
        ArenaPlatform GetPlatform(ArenaPlatform.PlatformID id);
    }
}
