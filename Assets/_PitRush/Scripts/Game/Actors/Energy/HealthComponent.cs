using GameUtility.Energy;

namespace Game.Actors.Energy
{
    /// <summary>
    /// Component for Health
    /// </summary>
    public class HealthComponent : EnergyComponent, IHealthComponent
    {
        public float HP => Energy;
        // ReSharper disable once ConvertToAutoPropertyWhenPossible
        public float MaxHP => MaxEnergy;

        public void Heal(float heal) => Regenerate(heal);

        public void Kill() => Deplete();
    }
}