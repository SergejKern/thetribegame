//using System;
//using Core.Unity.Extensions;
//using GameUtility.Energy;
//using UnityEngine;
//using UnityEngine.Serialization;
//namespace Game.Actors.Energy
//{
//    /// <summary>
//    /// Used to visualize Energy as part of the model.
//    /// Plays ParticleEffect on loosing energy
//    /// </summary>
//    public class EnergyVisualsUpdateBehaviour : MonoBehaviour
//    {
//#pragma warning disable 0649 // wrong warnings for SerializeField
//        [FormerlySerializedAs("hpObjectsParent")] [SerializeField]
//        Transform m_hpObjectsParent;
//        [SerializeField]
//        RefEnergyComponent m_energy;
//        [FormerlySerializedAs("particles")] [SerializeField]
//        ParticleSystem m_particles;
//#pragma warning restore 0649 // wrong warnings for SerializeField
//        IEnergyComponent Energy => m_energy.Result;
//        float m_lastEnergy;
//        // Update is called once per frame
//        void Update() => UpdateVisuals();
//        void UpdateVisuals()
//        {
//            if (m_energy == null)
//                return;
//            if (Math.Abs(m_lastEnergy - Energy.Energy) < float.Epsilon)
//                return;
//            if (Energy.Energy < m_lastEnergy)
//                m_particles.Play();
//            m_lastEnergy = Energy.Energy;
//            var hpPercent = Energy.Energy / Energy.MaxEnergy;
//            for (var i = 0; i < m_hpObjectsParent.childCount; i++)
//            {
//                var childP = (float)(i + 1) / m_hpObjectsParent.childCount;
//                var shouldBeActive = hpPercent >= childP;
//                var childGo = m_hpObjectsParent.GetChild(i).gameObject;
//                if (shouldBeActive!= childGo.activeSelf)
//                    childGo.SetActiveWithTransition(shouldBeActive);
//            }
//        }
//    }
//}