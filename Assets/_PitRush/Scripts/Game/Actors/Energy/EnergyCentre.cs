﻿using System.Collections.Generic;
using System.Linq;
using GameUtility.Energy;
using UnityEngine;

namespace Game.Actors.Energy
{
    /// <summary>
    /// Used by other components to easily get the EnergyComponent of EnergyType required
    /// </summary>
    public class EnergyCentre : MonoBehaviour
    {
        readonly List<IEnergyComponent> m_energyComponents = new List<IEnergyComponent>();

        void Awake()
        {
            var energies = GetComponents<IEnergyComponent>();
            m_energyComponents.AddRange(energies);
        }

        public IEnergyComponent GetEnergyComponentOfType(EnergyID id)
            => m_energyComponents.FirstOrDefault(e => e.EnergyType == id);
    }
}
