using System;
using Core.Events;
using Core.Unity.Attribute;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Controls;
using Game.Configs.Combat;
using Game.UI;
using GameUtility.Data.FX;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using GameUtility.Energy;
using GameUtility.Events;
using GameUtility.InitSystem;
using GameUtility.InitSystem.Config;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Actors.Energy
{
    /// <summary>
    /// When playing good player will go into a powerful RushMode
    /// </summary>
    public class RushComponent : EnergyComponent, 
        IInitDataComponent<RushComponent.RushConfigData>,
        IInitDataComponent<RushComponent.RushFullData>,
        IEventListener<KillEvent>, IEventListener<DamageEvent>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct RushFullData
        {
            public EnergyConfigData EnergyConfigData;
            public RushConfigData RushConfig;
        }

        [Serializable]
        public struct RushConfigData
        {
            public float EnergyForDamageConst;
            public float EnergyForDamageFactor;

            public float EnergyForKill;
            public float EnergyForKillComboBonus;
            public float LooseComboAfterSeconds;
            public float LooseEnergyForGetDamage;
            public static RushConfigData Default => new RushConfigData()
            {
                EnergyForDamageConst = 0f,
                EnergyForDamageFactor = 0.25f,
                EnergyForKill = 1f,
                EnergyForKillComboBonus = 0.1f,
                LooseComboAfterSeconds = 3f,
                LooseEnergyForGetDamage = 3f,
            };
        }

        [CopyPaste]
        [SerializeField] RushConfigData m_rushData = RushConfigData.Default;
        [SerializeField] GameObject m_actor;

        [SerializeField] SpawnInitConfig m_rushConfig;
        [SerializeField] SpawnInitConfig m_revertConfig;
        [SerializeField] WeaponConfig m_rushWeaponConfig;

        [SerializeField] RefIAudioAsset m_onRushModeOn;
        [SerializeField] RefIAudioAsset m_onRushModeOff;
        [SerializeField] GameObject m_onRushModeOnEffect;
        [SerializeField] GameObject m_onRushModeOffEffect;

        [SerializeField] UnityEvent m_onRushChanged;
        [SerializeField] UnityEvent m_onRushActivated;
        [SerializeField] UnityEvent m_onRushDeactivated;

        [SerializeField] FX_ID m_rushFXID;

        [SerializeField] GameObject m_comboEffect;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public bool RushMode { get; private set; }

        IFXComponent m_fxComponent;
        GameObject m_rushModeEffect;
        
        float m_timer;
        int m_combo;

        public void GetData(out RushConfigData data) => data = m_rushData;
        public void InitializeWithData(RushConfigData data) => m_rushData = data;
        public void GetData(out RushFullData data) => data = new RushFullData(){EnergyConfigData = m_data, RushConfig = m_rushData };
        public void InitializeWithData(RushFullData data)
        {
            InitializeWithData(data.EnergyConfigData);
            InitializeWithData(data.RushConfig);
        }

        void OnEnable()
        {
            EventMessenger.AddListener<KillEvent>(this);
            EventMessenger.AddListener<DamageEvent>(this);
        }
        void OnDisable()
        {
            EventMessenger.RemoveListener<KillEvent>(this);
            EventMessenger.RemoveListener<DamageEvent>(this);
        }

        protected override void Update()
        {
            if (Energy >= MaxEnergy) 
                ActivateRushMode();
            else if (Energy <= 0)
                DeactivateRushMode();

            if (m_timer > 0)
                m_timer -= Time.deltaTime;
            else m_combo = 0;

            base.Update();

            UpdateRushCheat();
        }

        void DeactivateRushMode()
        {
            if (!RushMode)
                return;
            RushMode = false;
            if (m_rushModeEffect!= null)
                m_rushModeEffect.SetActive(false);
            m_rushModeEffect = null;

            foreach (var rc in m_revertConfig.Configs)
            {
                var component = m_actor.GetComponentInChildren(rc.Result.ComponentType, true);
                rc.Result.InitializeComponent(component);
            }
            m_actor.GetComponentInChildren<AttackControl>().SetOverrideConfig(null);

            var tr = transform;
            m_onRushModeOff?.Result?.Play(gameObject);
            if (m_onRushModeOffEffect != null) m_onRushModeOffEffect.GetPooledInstance(tr.position, tr.rotation);

            m_onRushDeactivated.Invoke();
            m_onRushChanged.Invoke();
        }

        void ActivateRushMode()
        {
            if (RushMode)
                return;
            RushMode = true;

            m_rushModeEffect = m_fxComponent.GetInstance(m_rushFXID);
            foreach (var rc in m_rushConfig.Configs)
            {
                var component = m_actor.GetComponentInChildren(rc.Result.ComponentType, true);
                rc.Result.InitializeComponent(component);
            }
            m_actor.GetComponentInChildren<AttackControl>().SetOverrideConfig(m_rushWeaponConfig);

            var tr = transform;
            m_onRushModeOn?.Result?.Play(gameObject);
            if (m_onRushModeOnEffect != null) m_onRushModeOnEffect.GetPooledInstance(tr.position, tr.rotation);

            m_onRushActivated.Invoke();
            m_onRushChanged.Invoke();
        }

        public void OnEvent(KillEvent eventType)
        {
            if (eventType.Killer != m_actor)
                return;

            var bonus = m_combo * m_rushData.EnergyForKillComboBonus;
            Regenerate(m_rushData.EnergyForKill + bonus);

            m_timer = m_rushData.LooseComboAfterSeconds;
            m_combo++;

            if (m_combo > 1)
            {
                var pos = eventType.KilledActor.transform.position;
                var effectInstance = m_comboEffect.GetPooledInstance(pos + 3* Vector3.up, 
                    Quaternion.identity);
                effectInstance.TryGetComponent(out TextUI textUI);
                textUI.SetText(m_combo.ToString());
            }
            ResetRechargeTimer();
        }

        public void OnEvent(DamageEvent eventType)
        {
            if (eventType.Damaged == m_actor)
                Damage(m_rushData.LooseEnergyForGetDamage);
            else if (eventType.Aggressor == m_actor && eventType.DamageResult.DamageDone > 0f)
                Regenerate(m_rushData.EnergyForDamageConst +
                           m_rushData.EnergyForDamageFactor * eventType.DamageResult.DamageDone);
            ResetRechargeTimer();
        }

        void UpdateRushCheat()
        {
            #if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.B)) 
                Regenerate(1f);
            #endif
        }

        public void OnFXConnected(IFXComponent playerFX) => m_fxComponent = playerFX;
        public void AddRushModeListener(UnityAction action) => m_onRushChanged.AddListener(action);
        public void RemoveRushModeListener(UnityAction action) => m_onRushChanged.RemoveListener(action);
    }
}