﻿//using Core.Unity.Interface;
//using JetBrains.Annotations;
//using UnityEngine;
//namespace Game.Actors.Energy
//{
//    /// <summary>
//    /// Used to handle Audio on low energy
//    /// </summary>
//    public class LowEnergyAudioFeedbackAC : MonoBehaviour
//    {
//#pragma warning disable 0649 // wrong warnings for SerializeField
//        [SerializeField] string m_param;
//        IAudioComponent LowEnergySound => m_audioComponent.Result;
//        [SerializeField]
//        RefIAudioComponent m_audioComponent;
//#pragma warning restore 0649 // wrong warnings for SerializeField
//        [UsedImplicitly]
//        public void OnEnergyChanged(float energy)
//        {
//            if (LowEnergySound == null)
//                return;
//            if (energy > 0 && energy <= 1)
//                LowEnergySound.SetParameter(m_param, 0.0f);
//            else
//                LowEnergySound.SetParameter(m_param, 100.0f);
//        }
//        [UsedImplicitly]
//        public void OnDeath()
//        {
//            LowEnergySound?.SetParameter(m_param, 100.0f);
//        }
//    }
//}
