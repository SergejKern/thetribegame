using GameUtility.Energy;

namespace Game.Actors.Energy
{
    /// <summary>
    /// Stamina currently used for Dash
    /// </summary>
    public class StaminaComponent : EnergyComponent, IStaminaComponent
    {
//#pragma warning disable 0649 // wrong warnings for SerializeField
//        [SerializeField] ControlCentre m_controlCentre;
//#pragma warning restore 0649 // wrong warnings for SerializeField
        public float Stamina => Energy;
        // ReSharper disable once ConvertToAutoPropertyWhenPossible
        public float MaxStamina => MaxEnergy;
        //protected override void UpdateRegeneration()
        //{
        //    base.UpdateRegeneration();
        //    if (Stamina > 0 && m_controlCentre!= null)
        //        m_controlCentre.MemoryEnabled = true;
        //}
        //public override void EnergyChanged()
        //{
        //    base.EnergyChanged();
        //    if (Stamina < 0 && m_controlCentre!= null)
        //        m_controlCentre.MemoryEnabled = false;
        //}
    }
}