using System;
using Core.Interface;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Types.Fsm;
using Game.Actors.Controls;
using Game.Actors.Energy;
using Game.Actors.Model_Animation;
using Game.Components.Behaviour;
using Game.Globals;
using Game.UI.HUD;
using Game.Utility.VisualAndFX;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Energy;
using GameUtility.Events;
using GameUtility.InitSystem;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Game.Actors
{
    public class Player: MonoBehaviour, IControllable, 
        IProvider<IStaminaComponent>, IProvider<RushComponent>, 
        IRevivable, IPoolable, IDamagedByActorMarker,
        IInitDataComponent<Player.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public float TimeToRevive;
            public static ConfigData Default = new ConfigData() { TimeToRevive = 3, };
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("controlCentre")] [SerializeField] ControlCentre m_controlCentre;

        [SerializeField] HealthComponent m_healthComponent;
        [SerializeField] StaminaComponent m_staminaComponent;
        [SerializeField] RushComponent m_rushComponent;

        [SerializeField] FSMUnityCombinedEvent m_onDeath;
        [SerializeField] FSMUnityCombinedEvent m_onRevive;

        [SerializeField] PlayerFXComponent m_playerFXComponent;

        [SerializeField] PrefabRef m_playerModelPrefab;
        [SerializeField] PrefabRef m_reviveTrigger;

        //todo 3: Disable controls without disabling Playmaker? ..Couldn't do it like with the enemies, because controls need to be disabled (FSM-disabled).
        [SerializeField] [AnimTypeDrawer]
        AnimatorStateRef m_playerDeath;
        [SerializeField]
        [AnimTypeDrawer]
        AnimatorStateRef m_defaultStateRef;
        //[SerializeField]
        //[AnimTypeDrawer]
        //AnimatorStateRef m_reviveStateRef;

        [SerializeField] GameObjectEvent m_onModelSpawned;
        [SerializeField] UnityEvent m_onDespawn;
        [SerializeField] UnityEvent m_onInputConnected;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        public IControlCentre Centre => m_controlCentre;
        public PlayerInput Input => m_controlCentre.PlayerInput;
        public HealthComponent Health => m_healthComponent;
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible

        public int CharacterIdx { get; private set; }

        public int PlayerIdx { get; private set; }

        ControlBlocker m_controlBlockerDeath;
        RevivableTriggerBehaviour m_revivableTrigger;

        public float TimeToRevive => m_data.TimeToRevive;

        // todo 3: add player initializer
        public void InitializeWithData(ConfigData data) => m_data = data;
        public void GetData(out ConfigData data) => data = m_data;

        // ReSharper disable MethodNameNotMeaningful
        public void Get(out IStaminaComponent provided) => provided = m_staminaComponent;
        public void Get(out IHealthComponent provided) => provided = m_healthComponent;
        public void Get(out RushComponent provided) => provided = m_rushComponent;
        // ReSharper restore MethodNameNotMeaningful

        void Awake()
        {
            m_controlBlockerDeath = new ControlBlocker() { Source = this };

            if (Centre.Model == null)
                Centre.InitializeModel(Instantiate(m_playerModelPrefab.Prefab, transform));
            if (Centre.Model.TryGetComponent<ModelLink>(out var link))
                link.Connect(gameObject);
            m_onModelSpawned.Invoke(Centre.Model);

            InitAgent();
        }
        void InitAgent()
        {
            if (!m_controlCentre.TryGetComponent<NavMeshAgent>(out var agent))
                return;
            agent.updatePosition = false;
            agent.updateRotation = false;
        }

        void Update()
        {
            //if (m_controlCentre.TryGetComponent<NavMeshAgent>(out var agent))
            //    agent.nextPosition = transform.position;

            UpdateCheats();
        }

        public bool IsBeingControlled => Input != null;

        //internal static string PlayerDeathEvent = "PlayerDeathEvent";
        //internal static string NPCDeathEvent = "NPCDeathEvent";
        public void InitCharacterIdx(int characterIdx) => CharacterIdx = characterIdx;
        public void SetPlayerIdx(int playerIdx) => PlayerIdx = playerIdx;

        public void InitializeWithController(PlayerInput input)
        {
            m_controlCentre.InitializeWithController(input);

            GlobalOperations.MovePlayerToRuntimeSceneAndConnectHUD(this);

            m_playerFXComponent.Connect(this);

            m_onInputConnected.Invoke();
            //var goPlayerUI = Instantiate(PrefabLinks.I.PlayerUI, GameGlobals.GO_UICanvas.transform);
            //var playerUI = goPlayerUI.GetComponent<OldPlayerUI>();
            //playerUI.Init(GameGlobals.GO_UICanvas.GetComponent<Canvas>(), this);
        }

        public void RemoveControl() => m_controlCentre.RemoveControl();
        public void AddControlBlocker(ControlBlocker blocker) => m_controlCentre.AddControlBlocker(blocker);
        public void RemoveControlBlocker(ControlBlocker blocker)=> m_controlCentre.RemoveControlBlocker(blocker);
        //public void RemoveControlBlocker(object source) => m_controlCentre.RemoveControlBlocker(source);

        public void OnDeath()
        {
            // we don't actually know if last aggressor caused the kill, but we account him for it for the Results
            // (could also be that he fell, because of last attack or so) todo 0: check if this is OK
            KillEvent.Trigger(gameObject, LastAggressor);

            if (!enabled)
                return;
            enabled = false;

            Health.Kill();
            m_onDeath.Invoke();

            m_controlCentre.Animator.Play(m_playerDeath);

            // todo 0: add event for player death
            // EventDistribution.TriggerEvent(PlayerDeathEvent, null);
            // gameObject.AddComponent<DecomposeBody>();

            AddControlBlocker(m_controlBlockerDeath);
            var trigger = m_reviveTrigger.GetInstance();
            m_revivableTrigger = trigger.GetComponent<RevivableTriggerBehaviour>();
            m_revivableTrigger.Init(this);
            //RemoveControl();

            //Destroy(Input.gameObject);
        }

        public void OnRevive()
        {
            Health.Reset();
            enabled = true;
            RemoveControlBlocker(m_controlBlockerDeath);

            m_onRevive.Invoke();

            m_controlCentre.Animator.Play(m_defaultStateRef);
            m_controlCentre.Model.transform.localRotation = Quaternion.identity;
        }

        //public void GainCheering(float amount)
        //{
        //    m_cheeringComponent.Regenerate(amount);
        //}

        public void OnSpawn() => enabled = true;
        public void OnDespawn()
        {
            m_controlCentre.Animator.Play(m_defaultStateRef);
            m_onDespawn.Invoke();
        }

        public void Disconnect()
        {
            //Health.Kill();
            if (m_revivableTrigger!= null)
                m_revivableTrigger.gameObject.TryDespawnOrDestroy();
            if (Health.HP <= 0f)
                OnRevive();

            RemoveControl();
            if (GameGlobals.GO_UICanvas.TryGetComponent<PlayerEnergyHUD>(out var energyHUD))
                energyHUD.DisconnectPlayer(this);

            GameGlobals.SpotLightDirector.Disconnect(this);
        }

        public GameObject LastAggressor { get; set; }




        void UpdateCheats()
        {
            if (UnityEngine.Input.GetKey(KeyCode.LeftAlt) && UnityEngine.Input.GetKeyDown(KeyCode.K))
                Health.Kill();
            if (UnityEngine.Input.GetKey(KeyCode.J))
            {
                Health.SetVulnerable();
                Health.Damage(1f);
            }   
            if (UnityEngine.Input.GetKeyDown(KeyCode.H))
            {
                if (Health.HP<=0)
                    OnRevive();
                else Health.Reset();
            }
        }
    }
}