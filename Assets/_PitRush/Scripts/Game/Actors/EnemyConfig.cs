﻿using Core.Unity.Types;
using UnityEngine;

namespace Game.Actors
{
    [CreateAssetMenu(menuName = "Game/EnemyConfig")]
    public class EnemyConfig : ScriptableObject
    {
        public Sprite Icon;
        public PrefabData PrefabData;
        public int Power = 10;

        // todo 1: Bestiary
        // public string Name;
        // public string Description;

        // todo 3: extract this from prefabs/ configs
        public float Radius;
        public float MoveSpeedInMetersPerSec;

        public GameObject Prefab => PrefabData.Prefab;
    }

}
