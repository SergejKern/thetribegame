using System.Collections.Generic;
using Core.Events;
using Core.Interface;
using Core.Unity.Types.Fsm;
using Core.Unity.Operations;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;

using Game.Actors.Components;
using Game.Actors.Controls;
using Game.Actors.Model_Animation;
using Game.Combat.Timeline;
using Game.Utility;
using GameUtility.Components.Collision;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using GameUtility.Energy;
using GameUtility.Events;
using GameUtility.Feature.Exploding;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

namespace Game.Actors
{
    public class Enemy : MonoBehaviour, IPoolable, 
        IEventListener<AttackEvent>, IDamagedByActorMarker
    {
        [FSMDrawer]
        public FSMEventRef OnImmediateNearestPlayerChanged;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] int m_nearPlayerPriority;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [FSMDrawer]
        public FSMEventRef OnDashEventRef;
        [FSMDrawer]
        public FSMEventRef OnActivate;
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("controlCentre")] [SerializeField]
        ControlCentre m_controlCentre;

        [SerializeField] FsmTimelineAgent m_agent;

        [SerializeField, FSMDrawer]
        FSMEventRef m_deathEvent;

        [SerializeField] PrefabRef m_model;
        [SerializeField] DamageHandlerComponent m_damageHandlerComponent;
        [SerializeField] bool m_dashReactionNearAttack;
        [SerializeField] RefIAudioAsset m_explosionSound;
        [SerializeField] PrefabData m_explosionFX;

        [SerializeField] GameObject m_explodingPartsPrefab;
        [SerializeField] Vector3 m_explodingPosRandomization = 0.1f*Vector3.one;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly List<ITarget> m_immediateRangePlayers = new List<ITarget>();
        GameObject m_colliderParent;

        DashControl m_dashControl;

        // ReSharper disable MethodNameNotMeaningful
        public void Get(out IHealthComponent provided) => m_agent.Get(out provided);
        // ReSharper restore MethodNameNotMeaningful

        void Awake()
        {
            if (!SetupValid())
                return;
            //m_agent.Intent = TargetingIntent.Combat;

            if (m_controlCentre.Model == null)
                m_controlCentre.InitializeModel(Instantiate(m_model.Prefab, transform));
            m_dashControl = m_controlCentre.GetComponent<DashControl>();

            if (m_controlCentre.Model.TryGetComponent<ModelLink>(out var link))
            {
                link.Connect(gameObject);
                m_colliderParent = link.GetColliderParent();
                m_colliderParent.SetComponentsEnabled<RelativeCollider>();
                m_colliderParent.SetColliderEnabled<Collider>();
            }

            InitAgent();
        }

        void Update() => UpdateCheats();
        bool SetupValid()
        {
            if (m_agent == null)
            {
                Debug.LogError($"Enemy without m_agent assigned on {gameObject}");
                return false;
            }

            return true;
        }

        //void Start() => InitAgent();

        void InitAgent()
        {
            if (!m_controlCentre.TryGetComponent<NavMeshAgent>(out var agent)) 
                return;
            agent.updatePosition = false;
            agent.updateRotation = false;
        }

        public void OnImmediateAwarenessStay(GameObject collisionRoot)
        {
            if (!collisionRoot.TryGetComponent<Player>(out _))
                return;
            if (!collisionRoot.TryGetComponent<ITarget>(out var target))
                return;
            if (!target.IsTargetingPossible)
            {
                RemoveImmediateRangePlayer(target);
                return;
            }

            AddImmediateRangePlayer(target);
        }

        public void OnImmediateAwarenessLeave(GameObject collisionRoot)
        {
            if (!collisionRoot.TryGetComponent<Player>(out _))
                return;
            if (!collisionRoot.TryGetComponent<ITarget>(out var target))
                return;
            RemoveImmediateRangePlayer(target);
        }
        void AddImmediateRangePlayer(ITarget target)
        {
            if (!m_immediateRangePlayers.Contains(target))
                m_immediateRangePlayers.Add(target);

            UpdateImmediatePlayer();
        }

        void RemoveImmediateRangePlayer(ITarget target)
        {
            if (m_immediateRangePlayers.Contains(target))
                m_immediateRangePlayers.Remove(target);
            UpdateImmediatePlayer();
        }

        void UpdateImmediatePlayer()
        {
            if (m_immediateRangePlayers.Count <= 0)
                return;
            var nearest = m_immediateRangePlayers[0];

            if (m_immediateRangePlayers.Count > 1)
                nearest = TargetOperations.GetNearest2(transform, m_immediateRangePlayers);
            if (nearest.IsTargetingPossible)
            {
                // keep previous target, otherwise clear target
                if (m_agent.Target != nearest)
                    return;
                nearest = null;
            }
            if (m_agent.Target == nearest)
                return;

            // todo 4: maybe use also auto-aim?
            m_agent.SetTargetWithPriority(nearest, m_nearPlayerPriority);
            if (nearest != null)
                OnImmediateNearestPlayerChanged.Invoke();
        }

        public void OnDeath()
        {
            if (!enabled) //<- already dead
                return;

            // we don't actually know if last aggressor caused the kill, but we account him for it for the Results
            // (could also be that he fell, because of last attack or so) todo 0: check if this is OK
            KillEvent.Trigger(gameObject, LastAggressor);
            m_deathEvent.Invoke();

            m_colliderParent.SetComponentsEnabled<RelativeCollider>(false);
            m_colliderParent.SetColliderEnabled<Collider>(false);
            m_controlCentre.gameObject.SetActive(false);

            GetExplodingObject(out var explodingObject, out var explodingPrefab);

            // todo 2: this has to be set up properly for every enemy that explodes
            Pool.Instance.DelayInactivate(explodingPrefab, 1);

            ExplodeParts.ApplyExplosion(explodingObject, 
                m_damageHandlerComponent.LastDamagePos, m_explodingPosRandomization,
                25f, 10f, 0f);
            m_explosionSound.Result?.Play(explodingObject);
            var explosionPrefab = m_explosionFX.Prefab;
            if (explosionPrefab != null)
                explosionPrefab.GetPooledInstance(transform.position +Vector3.up);

            enabled = false;
        }

        void GetExplodingObject(out GameObject explodingObject, out GameObject prefab)
        {
            prefab = m_explodingPartsPrefab;

            if (prefab == null)
            {
                if (!gameObject.TryGetComponent(out DecomposeBodyRC decompose1))
                    decompose1 = gameObject.AddComponent<DecomposeBodyRC>();
                decompose1.DoDecompose();

                explodingObject = m_controlCentre.Model;

                if (TryGetComponent<IPrefabInstanceMarker>(out var marker))
                    prefab = marker.Prefab;
                return;
            }

            var tr = transform;
            explodingObject = prefab.GetPooledInstance(tr.position, tr.rotation);

            if (!gameObject.TryGetComponent(out DecomposeBodyRC decompose2))
                decompose2 = gameObject.AddComponent<DecomposeBodyRC>();
            decompose2.DoDecompose();

            gameObject.TryDespawnOrDestroy();
        }

        public void OnSpawn()
        {
            if (!SetupValid())
                return;
            if (TryGetComponent<PlayMakerFSM>(out var fsm))
                fsm.SetState(fsm.Fsm.StartState);

            m_agent.Get(out IHealthComponent health);
            health.Reset();

            //Debug.Log(name + " OnSpawn " + Transform.position);
            m_colliderParent.SetComponentsEnabled<RelativeCollider>();
            m_colliderParent.SetColliderEnabled<Collider>();
            m_controlCentre.gameObject.SetActive(true);

            enabled = true;

            // todo 3: revisit deactivated lines of code: why is this disabled? Because of animation_bug? Does it still exist? Are those lines needed?
            // old_todo: hard coded string is ugly. Also put this somewhere else? Playmaker?
            // same code is in player.cs via AnimatorStateRef m_defaultStateRef;
            //m_controlCentre.Animator.Play("Locomotion");
            //m_controlCentre.Animator.Update(0f);
            TargetSpawnedEvent.Trigger(m_agent);
        }

        public void OnDespawn()
        {
            //Debug.Log(name + " OnDespawn");
            if (m_controlCentre.Model.TryGetComponent<ExplodingParent>(out var memory))
                StartCoroutine(memory.Restore());

            if (TryGetComponent(out IMemorizeMaterials mm))
                mm.Revert();

            TargetDespawnedEvent.Trigger(m_agent);
        }

        void OnDestroy() { TargetDespawnedEvent.Trigger(m_agent); }

        public void OnEnable() => this.EventStartListening();
        public void OnDisable() => this.EventStopListening();
        public void OnEvent(AttackEvent eventType)
        {
            if (m_dashReactionNearAttack)
                DashReaction(eventType.Originator);
        }

        void DashReaction(GameObject attackOrigin)
        {
            if (!attackOrigin.TryGetComponent(out ITarget target))
                return;
            var inImmediateRange = m_immediateRangePlayers.Contains(target);
            if (!inImmediateRange)
                return;
            var attackOrigTr = attackOrigin.transform;

            var ideal = (transform.position - attackOrigTr.position).normalized;
            var attackAngle = Vector3.Angle(attackOrigTr.forward, ideal);
            if (attackAngle > 100)
                return;
            //Debug.DrawLine(transform.position, ideal+ transform.position, Color.magenta, 1f);
            //Debug.Log($"Originator {eventType.Originator} attacks {gameObject.name} at {attackAngle}");

            m_dashControl.OnMove(new Vector2(ideal.x, ideal.z));
            OnDashEventRef.Invoke();
        }

        public GameObject LastAggressor { get; set; }


        void UpdateCheats()
        {
            if (!Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.K))
            {
                Get(out IHealthComponent health);
                health.Kill();
            }

        }

        public void Activate() => OnActivate.Invoke();
    }
}