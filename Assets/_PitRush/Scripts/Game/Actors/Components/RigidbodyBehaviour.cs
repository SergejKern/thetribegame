﻿using System;
using System.Collections.Generic;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Game.Configs;
using GameUtility.Components.Collision;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.TransformAttachment;
using GameUtility.InitSystem;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Actors.Components
{
    // todo 5: why then we can still go through barrier while attacking?
    /// <summary>
    /// For smoothly moving rigidbody, scripts change position of RigidbodyUpdateBehaviour
    /// Final position is then applied in FixedUpdate via MovePosition, also we then can raycast once if position is OK
    /// </summary>
    public class RigidbodyBehaviour : MonoBehaviour, IMutableTransformData, //IPoolable,
        IInitDataComponent<RigidbodyBehaviour.ConfigData>
    {
        //copied from Rigidbody, so that we have flag-inspector
        // ReSharper disable UnusedMember.Global
        [Flags]
        public enum RigidbodyComponentConstraints
        {
            None = 0,
            FreezePositionX = 2,
            FreezePositionY = 4,
            FreezePositionZ = 8,
            FreezePosition = 14, 
            FreezeRotationX = 16,
            FreezeRotationY = 32,
            FreezeRotationZ = 64,
            FreezeRotation = 112,
            FreezeAll = 126,
        }
        // ReSharper restore UnusedMember.Global

        [Serializable]
        public struct ConfigData
        {
            public float Mass;
            public float Drag;
            public bool UseGravity;

            public RigidbodyComponentConstraints Constraints;
            // default assignment can mess up editor/ initializing
            //public static ConfigData Default = new ConfigData()
            //{
            //    Mass = 10f,
            //    Drag = 5f,
            //    UseGravity = false,
            //    Constraints = RigidbodyComponentConstraints.FreezePositionY | RigidbodyComponentConstraints.FreezeRotation,
            //};
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ConfigData m_configData;
        [SerializeField] Vector3 m_boundsSize;
        [SerializeField] float m_skinWidth = 0.1f;

        [SerializeField] LayerMask m_blocking;
        [SerializeField] float m_speedForCollisionEffectMps = 1f;
        [SerializeField] RefITimeMultiplier m_time;
        //todo 3 check again if needed:
        [SerializeField] NavMeshAgent m_agent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_updatePosition;
        bool m_updateRotation;

        Vector3 m_newPosition;
        Quaternion m_newRotation;

        Rigidbody m_cachedRigidbody;
        Vector3 m_previousPosition;
        float m_minimumExtent;
        float m_partialExtent;
        float m_sqrMinimumExtent;

        readonly List<Collider> m_ignoreCollidersForCollisionEffect = new List<Collider>();

        public Vector3 Position
        {
            get => m_updatePosition ? m_newPosition : GetBody().position;
            set
            {
                //Debug.Log($"set pos: {Environment.StackTrace}");
                //Debug.Log($"Moved from {m_newPosition} to {value}");
                m_newPosition = value;
                m_updatePosition = true;
            }
        }

        public Quaternion Rotation
        {
            get => m_updateRotation ? m_newRotation : GetBody().rotation;
            set
            {
                m_newRotation = value;
                m_updateRotation = true;
            }
        }

        //public Vector3 Velocity;
        
        public Rigidbody GetBody()
        {
            if (m_cachedRigidbody != null)
                return m_cachedRigidbody;

            //Debug.Log($"Add Rigidbody {gameObject}");
            if (!TryGetComponent(out m_cachedRigidbody))
                m_cachedRigidbody = gameObject.AddComponent<Rigidbody>();
            InitRigidbody(m_configData, m_cachedRigidbody);
            return m_cachedRigidbody;
        }

        public void GetData(out ConfigData data) => data = m_configData;
        public void InitializeWithData(ConfigData data)
        {
            var rigid = GetBody();
            InitRigidbody(data, rigid);
        }

        void InitRigidbody(ConfigData data, Rigidbody rigid)
        {
            rigid.mass = data.Mass;
            rigid.drag = data.Drag;
            rigid.useGravity = data.UseGravity;
            rigid.constraints = (RigidbodyConstraints) data.Constraints;
            // kinematic might help avoiding unneeded physics
            // rigid.isKinematic = true;
            // rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

            m_previousPosition = rigid.position; 
            m_minimumExtent = Mathf.Min(Mathf.Min(0.5f*m_boundsSize.x, 0.5f*m_boundsSize.y), 0.5f*m_boundsSize.z); 
            m_partialExtent = m_minimumExtent * (1.0f - m_skinWidth); 
            m_sqrMinimumExtent = m_minimumExtent * m_minimumExtent;
        }

        void FixedUpdate()
        {
            if (!enabled)
                return;

            var rigid = GetBody();

            //m_newPosition += (Time.fixedDeltaTime * Velocity);
            UpdatePosition(rigid);
            UpdateRotation(rigid);

            //Velocity = Vector3.zero;
            m_updatePosition = false;
            m_updateRotation = false;
        }

        void UpdateRotation(Rigidbody rigid)
        {
            if (!m_updateRotation)
                return;
            rigid.MoveRotation(m_newRotation);
        }

        void UpdatePosition(Rigidbody rigid)
        {
            if (!m_updatePosition)
            {
                DontGoThroughThings();
                return;
            }

            var moveVec = m_newPosition - m_previousPosition;
            var travelDistance = moveVec.magnitude;
            //moveVec.y = 0;
            var ray = new Ray(m_previousPosition + 0.25f * Vector3.up, moveVec);
            var movementBlocked = Physics.Raycast(ray, out var hit, travelDistance, m_blocking);

            //Debug.DrawRay(ray.origin, ray.direction * travelDistance, movementBlocked?Color.red : Color.cyan, 1.0f);
            if (movementBlocked)
            {
                rigid.MovePosition(m_previousPosition);
                TriggerCollisionEffect(hit, travelDistance);
                return;
            }

            m_ignoreCollidersForCollisionEffect.Clear();
            rigid.MovePosition(m_newPosition);
            m_previousPosition = m_newPosition;

            if (m_agent != null)
                m_agent.nextPosition = m_newPosition;
        }

        // https://wiki.unity3d.com/index.php/DontGoThroughThings
        void DontGoThroughThings()
        {
            var body = GetBody();
            //have we moved more than our minimum extent? 
            var movementThisStep = body.position - m_previousPosition; 
            var movementSqrMagnitude = movementThisStep.sqrMagnitude;
            var prev = m_previousPosition;
            m_previousPosition = m_cachedRigidbody.position;

            if (!(movementSqrMagnitude > m_sqrMinimumExtent)) 
                return;
            var movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);

            //check for obstructions we might have missed 
            if (!Physics.Raycast(prev, movementThisStep, out var hitInfo, movementMagnitude, m_blocking.value)) 
                return;
            if (!hitInfo.collider)
                return;
            if (hitInfo.collider.isTrigger) 
                return;
            var setBackPos = hitInfo.point - (movementThisStep / movementMagnitude) * m_partialExtent;
            m_cachedRigidbody.position = setBackPos;
            m_previousPosition = setBackPos;
        }

        void TriggerCollisionEffect(RaycastHit hit, float distance)
        {
            if (Math.Abs(m_time.FixedDeltaTime) < float.Epsilon)
                return;
            var speed = distance / m_time.FixedDeltaTime;
            if (speed < m_speedForCollisionEffectMps)
                return;
            //Debug.Log(speed);
            if (m_ignoreCollidersForCollisionEffect.Contains(hit.collider))
                return;

            m_ignoreCollidersForCollisionEffect.Add(hit.collider);

            var root = RelativeCollider.GetRoot(hit.collider);

            root.TryGetComponent(out ICollisionMaterialComponent otherMaterial);
            gameObject.TryGetComponent(out ICollisionMaterialComponent myMaterial);
            if (otherMaterial == null || myMaterial == null)
                return;

            var collisionData = ConfigLinks.CollisionsConfig.GetCollisionData(myMaterial.Material, otherMaterial.Material);
            collisionData.Audio?.Result?.Play(gameObject);
            var eff = collisionData.Effect;

            if (eff != null)
                eff.GetPooledInstance(hit.point);
        }

        void Reset()
        {
            var rigid = GetBody();
            rigid.velocity = Vector3.zero;

            m_ignoreCollidersForCollisionEffect.Clear();
        }

        void OnEnable() => Reset();
        void OnDisable() => RemoveRigidbody();

        //public void OnSpawn() => Reset();
        //public void OnDespawn() => RemoveRigidbody();

        void RemoveRigidbody()
        {
            if (m_cachedRigidbody != null)
                m_cachedRigidbody.DestroyEx();
        }
    }
}
