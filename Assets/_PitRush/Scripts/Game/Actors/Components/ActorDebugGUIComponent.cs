﻿using System;
using System.Collections.Generic;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Utility.Debug;
using GameUtility.Config;
using GameUtility.Data.Debugging;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Actors.Components
{
    /// <summary>
    /// Used to draw some Debug GUI for actors
    /// </summary>
    public class ActorDebugGUIComponent : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct DrawPlaymakerStateSetting
        {
            public DebugDrawID ID;
            public PlayMakerFSM PlaymakerFSM;
        }

        [SerializeField] NavMeshAgent m_agent;
        [SerializeField] List<DrawPlaymakerStateSetting> m_drawPlaymakerStates;

        [SerializeField] DebugDrawID m_drawAgentDestination;
        [SerializeField] DebugDrawID m_drawAgentNextPos;
        [SerializeField] DebugDrawID m_drawAgentSteeringTarget;
        [SerializeField] DebugDrawID m_drawAgentPath;
#pragma warning restore 0649 // wrong warnings for SerializeField

        //public void Update() { }
        Vector2 m_screenPosition;

        public void Update()
        {
            if (!Application.isEditor)
                return;

            DebugDrawAgent();
        }

        void OnGUI()
        {
            if (!Application.isEditor)
                return;
            m_screenPosition = CustomDebugDraw.CalculateScreenPosition(transform.position);
            foreach (var t in m_drawPlaymakerStates)
                DebugDrawPlaymaker(t);
        }

        void DebugDrawPlaymaker(DrawPlaymakerStateSetting setting)
        {
            var playMaker = setting.PlaymakerFSM;
            if (setting.PlaymakerFSM == null)
                return;

            var text = playMaker.ActiveStateName;
            DebugDrawConfig.I.DebugDrawText(m_screenPosition, text, setting.ID);
        }

        void DebugDrawAgent()
        {
            if (m_agent == null)
                return;

            var destination = m_agent.destination;
            var nextPos = m_agent.nextPosition;
            var steeringTarget = m_agent.steeringTarget;
            var path = m_agent.path.corners;

            DebugDrawConfig.I.DrawPosition(destination, m_drawAgentDestination);
            DebugDrawConfig.I.DrawPosition(nextPos, m_drawAgentNextPos);
            DebugDrawConfig.I.DrawPosition(steeringTarget, m_drawAgentSteeringTarget);
            DebugDrawConfig.I.DrawPath(path, m_drawAgentPath);
        }
    }
}
