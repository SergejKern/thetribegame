﻿using Core.Unity.Utility.Debug;
using Game.Arena;
using GameUtility.Components.Collision;
using GameUtility.Config;
using GameUtility.Data.Debugging;
using UnityEngine;

namespace Game.Actors.Components
{
    /// <summary>
    /// Makes sure actor is affiliated with platform below,
    /// This is needed for smoothly moving along with the platform and for the HexagonTrail-Effect
    /// </summary>
    [RequireComponent(typeof(PlatformRaycastDownBehaviour))]
    public class PlatformAffiliationBehaviour : MonoBehaviour
    {
        public struct CurrentPlatformData
        {
            public GameObject PlatformObj;
            public ArenaPlatform Platform;
            public PlatformTrailVfxSpawner VFXSpawner;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PlatformRaycastDownBehaviour m_raycast;
        [SerializeField] RigidbodyBehaviour m_rigidbody;
        [SerializeField] bool m_addHexagonTrail;
        [SerializeField] DebugDrawID m_debugDrawID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public CurrentPlatformData Current { get; private set; }

        void OnEnable()
        {
            if (m_rigidbody == null)
                m_rigidbody = GetComponent<RigidbodyBehaviour>();
        }

        void Update()
        {
            GetCurrentPlatformDataFromCast(out var platformRoot, out var arenaPlatform, out var vfxSpawner);
            if (platformRoot == Current.PlatformObj)
                return;
            
            RemoveFromCurrentPlatform();

            SetCurrent(platformRoot, arenaPlatform, vfxSpawner);

            if (vfxSpawner != null && m_addHexagonTrail)
                vfxSpawner.Add(gameObject);
            if (arenaPlatform!=null)
                arenaPlatform.AddAgentToPlatform(m_rigidbody);
        }

        void SetCurrent(GameObject platformRoot, ArenaPlatform arenaPlatform, PlatformTrailVfxSpawner vfxSpawner)
        {
            var current = Current;
            current.PlatformObj = platformRoot;
            current.Platform = arenaPlatform;
            current.VFXSpawner = vfxSpawner;
            Current = current;
        }

        void RemoveFromCurrentPlatform()
        {
            if (Current.PlatformObj == null)
                return;
            var platform = Current.Platform;
            var vfxSpawner = Current.VFXSpawner;

            if (vfxSpawner != null && m_addHexagonTrail)
                vfxSpawner.Remove(gameObject);
            if (platform != null)
                platform.RemoveAgentPlatform(m_rigidbody);
        }

        void GetCurrentPlatformDataFromCast(out GameObject root, out ArenaPlatform platform, out PlatformTrailVfxSpawner vfxSpawner)
        {
            root = null;
            platform = null;
            vfxSpawner = null;

            if (!m_raycast.IsGrounded)
                return;
            if (m_raycast.LastHitGameObject == null)
                return;

            root = RelativeCollider.GetRoot(m_raycast.LastHitGameObject);
            platform = root.GetComponent<ArenaPlatform>();
            vfxSpawner= root.GetComponent<PlatformTrailVfxSpawner>();
        }

        void OnDestroy() => RemoveFromCurrentPlatform();

        void OnGUI()
        {
            #if UNITY_EDITOR
            if (Current.PlatformObj == null)
                return;
            var sc = CustomDebugDraw.CalculateScreenPosition(transform.position);
            DebugDrawConfig.I.DebugDrawText(sc, Current.PlatformObj.name, m_debugDrawID);
            #endif
        }
    }
}
