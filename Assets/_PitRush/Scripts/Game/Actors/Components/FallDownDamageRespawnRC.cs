﻿using System.Collections;
using Game.Actors.Energy;
using Game.Arena;
using Game.Globals;
using GameUtility.Components.Collision;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Actors.Components
{
    /// <summary>
    /// Handles respawning when actor fell down
    /// RC = Routine Component 
    /// </summary>
    public class FallDownDamageRespawnRC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PlatformRaycastDownBehaviour m_downCast;
        [SerializeField] HealthComponent m_healthComponent;
        [SerializeField] float m_damageAmount;
        [SerializeField] RigidbodyBehaviour m_rigid;

        [SerializeField] bool m_testFallDownRespawnUpdate;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly Collider[] m_colliderBuffer = new Collider[25];
        Rigidbody Body => m_rigid.GetBody();

        [UsedImplicitly]
        public void OnFallenDown()
        {
            m_healthComponent.Damage(m_damageAmount);

            StartCoroutine(OnFallenDownRespawnRoutine());
        }

        IEnumerator OnFallenDownRespawnRoutine()
        {
            while (true)
            {
                var position = m_rigid.Position;
                var groundHeightPos = new Vector3(position.x, 0, position.z);
                var bestPos = m_downCast.LastRaycastHit.point + (m_downCast.LastRaycastHit.point - groundHeightPos).normalized;
                if (TryPutOnPosition(bestPos))
                    yield break;

                if (TryPutOnPosition(GameGlobals.SafePosition))
                    yield break;

                //Debug.Log("Best pos not ok, doing sphere check");
                if (CheckSurroundingPositions(bestPos, out var validPos))
                {
                    TryPutOnPosition(validPos);
                    yield break;
                }
                if (IterateAllPlatforms(out validPos))
                {
                    TryPutOnPosition(validPos);
                    yield break;
                }

                Debug.LogError($"Could not spawn {gameObject} on safe platform!");
                // we try again later, platforms are constantly moving, maybe next time we find a safe platform
                yield return new WaitForSeconds(0.5f);
            }
        }

        bool IterateAllPlatforms(out Vector3 validPos)
        {
            validPos = default;

            if (ArenaPlatformMapData.PlatformData == null)
                return false;

            // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
            foreach (var kv in ArenaPlatformMapData.PlatformData.GetPlatformMap())
            {
                var platform = kv.Value.PlatformInstance;
                if (platform == null)
                    continue;
                if (!platform.IsCurrentlySafe)
                    continue;
                if (!IsValidPos(platform.OnPlatformPosition, out _)) 
                    continue;
                validPos = platform.OnPlatformPosition;
                return true;
            }

            return false;
        }

        bool CheckSurroundingPositions(Vector3 bestPos, out Vector3 validPos)
        {
            validPos = default;
            for (var i = 9; i < 100; i += i)
            {
                var results = Physics.OverlapSphereNonAlloc(bestPos, i, m_colliderBuffer);
                //Debug.Log($"Results radius {i} count {results}");

                if (results < 0)
                    continue;

                for (var j = 0; j < results; j++)
                {
                    //Debug.Log("Checking collider: " + m_colliderBuffer[j].name);
                    var pos = m_colliderBuffer[j].ClosestPointOnBounds(bestPos);
                    if (!IsValidPos(pos, out _)) 
                        continue;
                    validPos = pos;
                    return true;
                }
            }
            return false;
        }

        bool TryPutOnPosition(Vector3 bestPos)
        {
            if (!IsValidPos(bestPos, out var hit)) 
                return false;

            Body.velocity = Vector3.zero;
            transform.position = hit.point;
            m_rigid.Position = hit.point;

            if (TryGetComponent<PlatformRaycastDownBehaviour>(out var raycastDown))
                raycastDown.CheckGrounded();

            if (TryGetComponent<CoyoteFallBehaviour>(out var coyoteBehaviour))
                coyoteBehaviour.CheckGrounded();
            return true;
        }

        bool IsValidPos(Vector3 bestPos, out RaycastHit hit)
        {
            if (!Physics.Raycast(new Ray(bestPos + 0.5f * Vector3.up, Vector3.down), out hit, 2f, m_downCast.GroundLayers))
                return false;

            var root = RelativeCollider.GetRoot(hit.collider);
            if (root == null)
                return false;

            root.TryGetComponent(out ArenaPlatform ap);
            return ap != null && ap.IsCurrentlySafe;
        }

        void Update()
        {
            if (!m_testFallDownRespawnUpdate)
                return;

            Test(out var debugLog, out var testFails);
            if (testFails == 4)
                Debug.LogError($"{nameof(FallDownDamageRespawnRC)} Failed all Test!! {debugLog} ");
            else if(testFails > 0)
                Debug.LogWarning($"{debugLog} \nDONE");
        }

        void Test(out string debugLog, out int testFailures)
        {
            testFailures = 0;
            debugLog = $"Test {nameof(FallDownDamageRespawnRC)}";

            var position = m_rigid.Position;
            var groundHeightPos = new Vector3(position.x, 0, position.z);
            var bestPos = m_downCast.LastRaycastHit.point + (m_downCast.LastRaycastHit.point - groundHeightPos).normalized;

            if (!IsValidPos(bestPos, out _))
            {
                testFailures++;
                debugLog += $"\nRaycast-Pos failed! {bestPos}";
            }
            if (!IsValidPos(GameGlobals.SafePosition, out _))
            {
                testFailures++;
                debugLog += $"\nSafe-Pos failed! {GameGlobals.SafePosition}";
            }
            if (!CheckSurroundingPositions(bestPos, out _))
            {
                testFailures++;
                debugLog += "\nCheckSurroundingPositions failed!";
            }
            if (!IterateAllPlatforms(out _))
            {
                testFailures++;
                debugLog += "\nIterateAllPlatforms failed!";
            }
        }
    }
}
