﻿using System.Collections.Generic;
using Core.Events;
using Core.Unity.Interface;
using Game.Utility;
using GameUtility.Events;
using UnityEngine;

namespace Game.Actors.Components
{
    public class InRangeTargetFilterBehaviour : MonoBehaviour, IEventListener<AttackDataChangedEvent>
    {
        //float m_timeMultiplier = 1f;
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_actor;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public float SqrLockDistance { get; private set; }

        public List<ITarget> InRangeTargets = new List<ITarget>();

        void OnEnable() => EventMessenger.AddListener(this);
        void OnDisable() => EventMessenger.RemoveListener(this);

        void Update() => UpdateTargetsInRange();

        void UpdateTargetsInRange()
        {
            InRangeTargets.Clear();
            foreach (var target in TargetsCollector.Targets)
            {
                if (!target.IsTargetingPossible)
                    continue;
                var isInRange = Vector3.SqrMagnitude(target.Transform.position - transform.position) <= SqrLockDistance;
                if (isInRange)
                    InRangeTargets.Add(target);
            }
        }

        public void OnEvent(AttackDataChangedEvent @event)
        {
            if (@event.Originator != m_actor)
                return;
            var attDataProvider = @event.AttackDataProvider;
            var rng = attDataProvider.GetNextAttackData.FullRangeUntilOnHit;
            //Debug.Log($"Range: {rng} {attDataProvider.GetNextAttackData.AttackState}");
            SqrLockDistance = rng * rng;
        }
    }
}
