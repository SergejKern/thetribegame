﻿using System.Collections;
using Core.Unity.Extensions;
using UnityEngine;

namespace Game.Actors.Components
{
    /// <summary>
    /// Used to drag actors down into the ground when dead.
    /// </summary>
    public class DecomposeBodyRC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_decomposeAfterSeconds =1f;
        [SerializeField] float m_destroyAfterSeconds = 2.0f;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void DoDecompose() => StartCoroutine(Decompose());

        IEnumerator Decompose()
        {
            yield return new WaitForSeconds(m_decomposeAfterSeconds);

            var time = m_destroyAfterSeconds;
            while (time > 0.0f)
            {
                // todo 3: should maybe use RigidbodyBehaviour
                transform.position += Vector3.down * Time.deltaTime;
                time -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Destroy(this);
            gameObject.TryDespawnOrDestroy();
        }
    }
}
