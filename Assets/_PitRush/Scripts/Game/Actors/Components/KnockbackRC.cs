﻿using System.Collections;
using Core.Unity.Attribute;
using Game.Actors.Controls;
using Game.Configs;
using GameUtility.Components.Collision;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Interface;
using UnityEngine;

namespace Game.Actors.Components
{
    /// <summary>
    /// Handles Knockback when applied
    /// RC = Routine Component
    /// </summary>
    public class KnockbackRC : MonoBehaviour, IKnockback
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RigidbodyBehaviour m_rigidbody;
        [SerializeField] ControlCentre m_controlCentre;

        [SerializeField] float m_knockFactor = 1f;
        [SerializeField] bool m_customKnockBackData;
        [ShowIf("m_customKnockBackData")]
        [SerializeField] KnockBackRoutine.KnockBackConfigData m_knockbackConfigData;
#pragma warning restore 0649 // wrong warnings for SerializeField

        Coroutine m_knockCoroutine;
        ControlBlocker m_knockBackBlocker;

        void Awake() => m_knockBackBlocker = new ControlBlocker(){ Source = this };

        void OnDisable() => m_controlCentre.RemoveControlBlocker(m_knockBackBlocker);

        KnockBackRoutine.KnockBackData GetKnockBackData()
        {
            var data = m_customKnockBackData? m_knockbackConfigData: ConfigLinks.FallbackDataConfig.GetKnockBackConfigData();
            return KnockBackRoutine.GetKnockBackData(data, m_rigidbody);
        }

        public void ApplyKnockback(Vector3 direction, float knockBackFactor, float knockBackDurationFactor,
            float knockBackRecoverFactor)
        {
            var knockData = GetKnockBackData();
            knockData.Direction = direction;
            knockData.KnockFactor = m_knockFactor* knockBackFactor;
            knockData.KnockDuration *= knockBackDurationFactor;
            knockData.RecoverDuration *= knockBackRecoverFactor;

            if (m_knockCoroutine!= null)
                StopCoroutine(m_knockCoroutine);
            m_knockCoroutine = StartCoroutine(Knockback(knockData));
            //Debug.Log($"Knockback on {gameObject}");
        }

        IEnumerator Knockback(KnockBackRoutine.KnockBackData knockBackData)
        {
            m_controlCentre.AddControlBlocker(m_knockBackBlocker);
            yield return StartCoroutine(KnockBackRoutine.KnockBack(knockBackData));
            m_controlCentre.RemoveControlBlocker(m_knockBackBlocker);
        } 

        void OnCollisionEnter(Collision other)
        {
            var collisionRoot = RelativeCollider.GetRoot(other);
            OnCollision(collisionRoot);
        }

        void OnTriggerEnter(Collider other)
        {
            var collisionRoot = RelativeCollider.GetRoot(other);
            OnCollision(collisionRoot);
        }

        void OnCollision(GameObject collisionRoot)
        {
            if (!enabled)
                return;

            if (collisionRoot.layer != 11) return;
            if (m_knockCoroutine == null)
                return;
            StopCoroutine(m_knockCoroutine);
            m_controlCentre.RemoveControlBlocker(m_knockBackBlocker);

            //Debug.Log($"Set velocity zero on {gameObject}");
            m_rigidbody.GetBody().velocity = Vector3.zero;
        }
    }
}
