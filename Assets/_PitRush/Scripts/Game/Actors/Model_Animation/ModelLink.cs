﻿using Core.Unity.Types;
using UnityEngine;

namespace Game.Actors.Model_Animation
{
    public class ModelLink : MonoBehaviour
    {
        //[SerializeField] Animator m_animator;
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_colliderParent;
        [SerializeField] GameObjectEvent m_connectActions;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public GameObject GetColliderParent() => m_colliderParent;
        public void Connect(GameObject logic) { m_connectActions.Invoke(logic); }
    }
}
