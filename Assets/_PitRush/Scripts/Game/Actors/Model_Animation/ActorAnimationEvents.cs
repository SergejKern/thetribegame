﻿using Game.Actors.Controls;
using Game.Combat.Timeline;
using Game.Combat.Weapon;
using GameUtility.Data.PhysicalConfrontation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Actors.Model_Animation
{
    public class ActorAnimationEvents : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warning
        [FormerlySerializedAs("attackControl")]
        [SerializeField] AttackControl m_attackControl;
#pragma warning restore 0649 // wrong warning

        public void SetAttackControl(AttackControl attackControl) => m_attackControl = attackControl;

        [UsedImplicitly]
        public void AttackActivateAll() => m_attackControl.Activate();

        [UsedImplicitly]
        public void AttackActivate(int idx) => m_attackControl.Activate(idx);

        [UsedImplicitly]
        public void Shoot(WeaponFXID fx) => m_attackControl.Shoot(fx);
        [UsedImplicitly]
        public void ShootWithData(WeaponFXEventData fx) => m_attackControl.Shoot(fx.FXID, fx.WeaponIdx);

        [UsedImplicitly]
        public void Recover() => m_attackControl.Recover();
        [UsedImplicitly]
        public void ComboMoveClose() => m_attackControl.ComboMoveOpen = false;

        [UsedImplicitly]
        public void PlayWeaponFX(WeaponFXID fx) => m_attackControl.Play(fx);
        [UsedImplicitly]
        public void PlayWeaponFXWithData(WeaponFXEventData fx) => m_attackControl.Play(fx.FXID, fx.WeaponIdx);

        [UsedImplicitly]
        public void StopWeaponFX(WeaponFXID fx) => m_attackControl.Stop(fx);
        [UsedImplicitly]
        public void StopWeaponFXWithData(WeaponFXEventData fx) => m_attackControl.Stop(fx.FXID, fx.WeaponIdx);

        public void OnHit()
        {
            var cc = m_attackControl.ControlCentre;
            if (cc == null || cc.ControlledObject == null)
                return;
            if (cc.ControlledObject.TryGetComponent(out ITimelineAgent agent))
                agent.AttackLanded();
            
            //Debug.Log("On HIt!");
        }
    }
}
