﻿using System;
using Game.Actors.Components;
using UnityEngine;

namespace Game.Actors.Model_Animation
{
    public class AnimatorMoveHandlerComponent : MonoBehaviour
    {
        Animator m_animator;
        RigidbodyBehaviour m_body;
        float m_motionFactor = 1f;

        public bool DoMove = true;

        public void LinkRigidbody(RigidbodyBehaviour rigidBody) => m_body = rigidBody;
        public void SetMotionFactor(float currentMoveMotionFactor) => m_motionFactor = currentMoveMotionFactor;

        void Awake() => m_animator = GetComponent<Animator>();

        void OnAnimatorMove()
        {
            if (!enabled)
                return;

            var moved = (m_animator.deltaPosition.sqrMagnitude > 2 * float.Epsilon);
            var rotated = (m_animator.deltaRotation.eulerAngles.sqrMagnitude > 2 * float.Epsilon);

            if (moved && DoMove)
            {
                //if (m_motionFactor <= float.Epsilon)
                //    transform.position += m_animator.deltaPosition;
                //else
                    m_body.Position += m_motionFactor * m_animator.deltaPosition;
            }
            else transform.localPosition = Vector3.zero;

            if (rotated)
                transform.rotation *= m_animator.deltaRotation;
            else
            {
                var trRot = transform.localRotation;
                var trEuler = trRot.eulerAngles;
                if (!(Math.Abs(trEuler.y) > float.Epsilon))
                    return;
                var bodyEuler = m_body.Rotation.eulerAngles;
                m_body.Rotation = Quaternion.Euler(0, bodyEuler.y + trEuler.y, 0);
                var euler =Quaternion.Euler(trEuler.x, 0, trEuler.z);
                // todo 4: HACK
                var dot = Mathf.Abs(1f - Quaternion.Dot(Quaternion.identity, euler));
                //Debug.Log(dot);
                transform.localRotation = dot < 0.1f ? Quaternion.identity : euler;
            }
            //m_animator.ApplyBuiltinRootMotion();
            //transform.localPosition = Vector3.zero;
        }
    }
}
