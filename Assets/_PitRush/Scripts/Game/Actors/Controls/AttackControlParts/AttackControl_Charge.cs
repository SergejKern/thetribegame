using System;
using System.Linq;
using Core.Extensions;
using Core.Unity.Types.Fsm;
using Game.Configs.Combat;
using GameUtility.Data.FX;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    public partial class AttackControl
    {
        #region Serialized Fields
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, FSMDrawer] FSMEventRef m_onCharging;
        [FormerlySerializedAs("m_chargeId"), SerializeField] FX_ID m_chargeFXID;
        [SerializeField] AttackTypeID m_charge1AttackTypeID;
#pragma warning restore 0649 // wrong warnings for SerializeField
        #endregion

        #region fields

        bool m_releasing;

        float m_charge;
        WeaponFXID m_weaponChargeFXID;
        //bool m_canRelease;
        bool m_chargePaused;

        float m_minChargeForRelease;
        #endregion

        #region Properties

        IFXComponent m_fxComponent;
        GameObject m_chargeEffect;

        float Charge
        {
            get => m_charge;
            set
            {
                if (!CanCharge)
                    return;

                for (var i = 0; i < m_data.ChargeLevels.Length; i++)
                {
                    if (value < m_data.ChargeLevels[i].HoldDuration ||
                        m_charge >= m_data.ChargeLevels[i].HoldDuration)
                        continue;

                    NextAttackType = m_data.ChargeLevels[i].AttackType;
                    SetWeaponChargeState(m_data.ChargeLevels[i].FXID);
                    m_charge = value;
                    return;
                }
                if (value <= 0f && m_charge > 0f)
                    SetWeaponChargeState(m_defaultFXID);
                
                m_charge = value;
            }
        }

        public bool Released { get; private set; } = true;

        bool ChargeFeature => m_data.ChargeEnabled && m_moveSets.ContainsKey(m_charge1AttackTypeID);
        bool CanCharge => ChargeFeature && (m_fsm.IsFollowUpState || m_fsm.IsCooldownState|| m_fsm.IsReadyState);
        //bool CanChargeAndRelease => CanCharge && m_canRelease;

        #endregion

        // called from charging state
        [Preserve, UsedImplicitly]
        public void ResumeCharge() => m_chargePaused = false;

        void InitCharge()
        {
            if (m_data.ChargeLevels.IsNullOrEmpty())
                return;
            m_minChargeForRelease = m_data.ChargeLevels.Min(a => a.HoldDuration);
            Array.Sort(m_data.ChargeLevels, (a, b) => b.HoldDuration.CompareTo(a.HoldDuration));
        }

        void SetWeaponChargeState(WeaponFXID fx)
        {
            var play = (fx != m_defaultFXID);
            var stop = (m_weaponChargeFXID != m_defaultFXID);
            foreach (var w in CurrentWeapons)
            {
                if (stop) w.Stop(m_weaponChargeFXID);
                if (play) w.Play(fx);
            }

            if (play)
            {
                if (m_chargeEffect == null)
                    m_chargeEffect = m_fxComponent.GetInstance(m_chargeFXID);
            }           
            else if (m_chargeEffect != null)
            {
                m_chargeEffect.SetActive(false);
                m_chargeEffect = null;
            }

            m_weaponChargeFXID = fx;
        }

        void UpdateWeaponAttackForCharge(float delta)
        {
            if (Action == null)
            {
                Released = true;
                return;
            }
            Released |= Action.ReadValue<float>() <= float.Epsilon;
            if (!CanCharge)
                return;
            if (!m_chargePaused)
                Charge += delta;
            //if (!m_canRelease)
            //    return;
            //if (!AttackDone)
            //    return;
            if (Released)
                ReleasedAttack();
            if (m_chargePaused)
                m_onCharging.Invoke();
        }

        void ReleasedAttack()
        {
            if (Charge >= m_minChargeForRelease) // && !m_chargePaused)
            {
                m_releasing = true;
                Trigger();
            }
            else
                OnAttackDone();

            ResetCharge();
        }

        void ResetCharge()
        {
            Charge = 0;
            //m_canRelease = false;
            m_chargePaused = false;
        }
    }
}