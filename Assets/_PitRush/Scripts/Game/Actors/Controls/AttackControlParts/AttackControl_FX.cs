using System;
using Game.Combat.Weapon;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.TransformAttachment;
using UnityEngine;

namespace Game.Actors.Controls
{
    public partial class AttackControl : IUpdateAttachmentPoints
    {
        #region Serialized Fields
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct GeneralFX
        {
            public WeaponFXID ID;
            public WeaponFXData FXData;
            public AttachmentOrTransformData Attach;
            //public GameObject Prefab => PrefabData.Prefab;
        }

        [SerializeField] GeneralFX[] m_fx;
        [SerializeField] WeaponFXID m_indicateFXID;
        [SerializeField] WeaponFXID m_defaultFXID;

#pragma warning restore 0649 // wrong warnings for SerializeField
        #endregion

        ref GeneralFX GetFX(int i) => ref m_fx[i];

        public void UpdateAttachmentPoints(AttachmentPointsProvider provider)
        {
            for (var i = 0; i < m_fx.Length; i++)
            {
                ref var fx = ref GetFX(i);
                if (fx.Attach.UseID)
                    fx.Attach.TransformData = provider.GetAttachment(fx.Attach.ID);
            }
        }

        // ReSharper disable FlagArgument
        void PlayGeneralFX(WeaponFXID fx)
        {
            for (var i = 0; i < m_fx.Length; i++)
            {
                var wfx = m_fx[i];
                if (wfx.ID != fx)
                    continue;

                FixTransform(ref wfx, i);
                // wfx.AudioAsset?.Result.Play(tr);
                wfx.FXData.Play(wfx.Attach.TransformData);
            }
        }

        void StopGeneralFX(WeaponFXID fx)
        {
            for (var i = 0; i < m_fx.Length; i++)
            {
                var wfx = m_fx[i];
                if (wfx.ID != fx)
                    continue;
                FixTransform(ref wfx, i);
                wfx.FXData.Stop(wfx.Attach.TransformData);
            }
        }
        // ReSharper restore FlagArgument
        public void IndicateAttack() => Play(m_indicateFXID);

        void FixTransform(ref GeneralFX wfx, int i)
        {
            if (wfx.Attach.TransformData.Parent != null) 
                return;
            wfx.Attach.TransformData.Parent = transform;
            m_fx[i] = wfx;
        }
    }
}