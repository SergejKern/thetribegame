using System;
using Core.Extensions;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;

namespace Game.Actors.Controls
{
    public partial class AttackControl
    {
        // ReSharper disable once FlagArgument
        public void Play(WeaponFXID fx, int weaponIdx = -1)
        {
            PlayGeneralFX(fx);

            WeaponConfig.SpawnableWeaponFX fxFilter = default;
            if (!CurrentMove.WeaponFxList.IsNullOrEmpty())
                fxFilter = Array.Find(CurrentMove.WeaponFxList,wfx => wfx.FXID == fx);
            if (fxFilter.Variation == null)
                fxFilter.Variation = "";
            if (!weaponIdx.IsInRange(0, CurrentWeapons.Count))
            {
                foreach (var w in CurrentWeapons)
                    w.Play(fx, fxFilter.Variation);
            }
            else CurrentWeapons[weaponIdx].Play(fx, fxFilter.Variation);
        }

        public void Shoot(WeaponFXID fx, int weaponIdx = -1)
        {
            Play(fx, weaponIdx);
            m_fsm.OnWeaponForce();
        }

        // ReSharper disable once FlagArgument
        internal void Stop(WeaponFXID fx, int weaponIdx = -1)
        {
            StopGeneralFX(fx);

            if (!weaponIdx.IsInRange(0, CurrentWeapons.Count))
            {
                foreach (var w in CurrentWeapons)
                    w.Stop(fx);
            }
            else CurrentWeapons[weaponIdx].Stop(fx);
        }

        public void Activate(int weaponIdx = -1)
        {
            if (!weaponIdx.IsInRange(0, CurrentWeapons.Count))
                SetColliderActive(true);
            else CurrentWeapons[weaponIdx].WeaponActive = true;
            //StartCoroutine(VelocityRoutine.Move(new VelocityRoutine.MoveData() { Body = body, Curve = m_curve, Direction = body.transform.forward, Distance = CurrentMove.ForwardMoveDistance, Duration = 0.2f }));
            m_fsm.OnWeaponForce();
        }

        public void Recover()
        {
            m_attackHook?.Result?.AttackDone(m_hitDamageHandlers);

            SetColliderActive(false);

            m_fsm.OnRecover();

            if (HasNextMove)
                ComboMoveOpen = true;
        }
    }
}