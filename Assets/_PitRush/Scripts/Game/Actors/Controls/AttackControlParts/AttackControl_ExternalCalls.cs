using System;
using Core.Unity.Types;
using Game.Combat.Weapon;
using Game.Configs;
using Game.Configs.Combat;
using Game.InteractiveObjects;
using GameUtility.Data.FX;
using GameUtility.Data.PhysicalConfrontation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace Game.Actors.Controls
{
    public partial class AttackControl
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AnimatorStateRef m_noAttackState;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable once UnusedMember.Global
        [Preserve] // playmaker
        public bool AttackDone => m_fsm.AttackDone;

        #region Playmaker-Actions
        // linked by playmaker-action AttackResetCombo
        [Preserve]
        public void OnWeaponReady()
        {
            if (m_releasing)
                return;
            //m_canRelease = true;
            if (!Released && CanCharge)
            {
                m_onCharging.Invoke();
                return;
            }

            OnAttackDone();
        }

        // linked by playmaker-call
        [Preserve, UsedImplicitly]
        public void OnAttack() => DoAttack(NextAttackType);

        // linked by playmaker-call
        [Preserve, UsedImplicitly]
        public void OnAttack(AttackTypeID attackType) => DoAttack(attackType);

        // linked by playmaker-call
        [Preserve, UsedImplicitly]
        public void Trigger(AttackTypeID attackType)
        {
            NextAttackType = attackType;
            base.Trigger();
        }

        //todo 3: remove this complexity
        public void OnAttackCanceled()
        {
            AttackCancel();

            if (ControlCentre.Animator != null)
                ControlCentre.Animator.Play(m_noAttackState);
            m_moveHandler.SetMotionFactor(0f);
            m_fsm.Forget();
            SetColliderActive(false);
        }
        public void OnAttackAndChargeCanceled()
        {
            OnAttackDone();

            if (ControlCentre.Animator != null)
                ControlCentre.Animator.Play(m_noAttackState);
            m_moveHandler.SetMotionFactor(0f);
            m_fsm.Forget();
            SetColliderActive(false);
        }
        #endregion

        #region Unity-Events
        // linked by unity-events
        [Preserve]
        public void LinkModelWeaponsAsDefault()
        {
            if (!Initialized)
                Init();
            m_defaultWeapons = ControlCentre.Model.GetComponentsInChildren<Weapon>();
            Array.Sort(m_defaultWeapons, (a, b) => a.WeaponIndex.CompareTo(b.WeaponIndex));

            foreach (var weapon in m_defaultWeapons)
                weapon.InitWithActor(ControlCentre.ControlledObject);

            //OnModelInitialized();
        }

        // ReSharper disable once UnusedMember.Global
        // linked by unity-events
        [Preserve]
        public void OnEquipChanged(IGrabbable primary, IGrabbable secondary)
        {
            ComboMoveOpen = false;

            var noWeapon = !(primary is Weapon) && !(secondary is Weapon);
            if (primary != null && secondary != null)
            {
                if (!ConfigLinks.AnimationCombinationConfig.FindCombination(primary.ItemAnimationType,
                    secondary.ItemAnimationType, out m_currentWeaponMovesType))
                {
                    if (primary is Weapon)
                        m_currentWeaponMovesType = primary.ItemAnimationType;
                    else
                        m_currentAttackType = m_defaultAttackId;
                }
            }
            else if (primary is Weapon)
                m_currentWeaponMovesType = primary.ItemAnimationType;

            CurrentWeapons.Clear();

            if (noWeapon)
            {
                CurrentWeapons.AddRange(m_defaultWeapons);
                m_currentWeaponMovesType = WeaponMovesType.Default;
            }
            else
            {
                if (primary is Weapon pW)
                    CurrentWeapons.Add(pW);
                if (primary is Weapon sW)
                    CurrentWeapons.Add(sW);
            }

            InitMoves(ControlCentre, m_currentWeaponMovesType);
        }

        [Preserve]
        public void OnFXConnected(IFXComponent playerFX) => m_fxComponent = playerFX;

        #endregion
    }
}