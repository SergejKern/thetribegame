using System;
using Core.Unity.Attribute;
using Game.Actors.Energy;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Energy;
using GameUtility.InitSystem;
using UnityEngine;

namespace Game.Actors.Controls
{
    public partial class AttackControl : IInitDataComponent<AttackControl.ConfigData>
    {
        #region Serialized Fields
#pragma warning disable 0649 // wrong warnings for SerializeField

        [Serializable]
        public struct ChargeLevelData
        {
            public AttackTypeID AttackType;
            public WeaponFXID FXID;

            public float HoldDuration;
        }

        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;

            public EnergyID EnergyID;

            public bool ChargeEnabled;

            // public float HoldDurationHeavyAttackRelease;
            public float HoldDurationForChargingFX;
            public ChargeLevelData[] ChargeLevels;

            public static ConfigData Default = new ConfigData()
            {
                Enabled = true,
                ChargeEnabled = true,
                // HoldDurationHeavyAttackRelease = 0.2f,
                HoldDurationForChargingFX = 0.15f,
                ChargeLevels = new ChargeLevelData[0],
            };
        }

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
        [SerializeField] EnergyCentre m_energyCentre;
#pragma warning restore 0649 // wrong warnings for SerializeField
        #endregion

        public void GetData(out ConfigData data) => data = m_data;

        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;
            if (m_energyCentre != null)
                m_energy = m_energyCentre.GetEnergyComponentOfType(m_data.EnergyID);
        }
    }
}