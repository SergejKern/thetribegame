using System.Collections.Generic;
using Core.Extensions;
using Game.Combat.Weapon;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Events;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Actors.Controls
{
    public partial class AttackControl : IAttackDataProvider
    {
        #region fields
        Dictionary<AttackTypeID, WeaponConfig.WeaponComboMove[]> m_moveSets =
            new Dictionary<AttackTypeID, WeaponConfig.WeaponComboMove[]>();
        bool m_moveSetsInitialized;
        WeaponMovesType m_currentWeaponMovesType;
        AttackTypeID m_currentAttackType;
        int m_currentMoveIdx = -1;

        //todo init with m_defaultAttackId
        AttackTypeID m_nextAttackType;// = m_defaultAttackId;
        WeaponConfig m_overrideConfig;
        #endregion

        #region Properties
        public List<Weapon> CurrentWeapons { get; } = new List<Weapon>();
        public IReadOnlyList<IWeapon> GetCurrentWeapons() => CurrentWeapons;


        WeaponConfig.WeaponComboMove[] Moves
        {
            get
            {
                if (m_moveSets.ContainsKey(CurrentAttackType))
                    return m_moveSets[CurrentAttackType];

                Debug.LogError($"Key does not exist {CurrentAttackType} for {ControlCentre.ControlledObject}");
                return new WeaponConfig.WeaponComboMove[0];
            }
        }

        WeaponConfig.WeaponComboMove CurrentMove => CurrentMoveIdx.IsInRange(Moves) ? Moves[CurrentMoveIdx] : default;
        bool HasNextMove => CurrentMoveIdx + 1 < Moves.Length;
        int NextMoveIdx => (int) Mathf.Repeat(CurrentMoveIdx + 1, Moves.Length);
        float NextEnergyCost => Moves[NextMoveIdx].EnergyCost;

        public AttackTypeID CurrentAttackType => m_currentAttackType!= null ? m_currentAttackType : m_defaultAttackId;
        public AttackTypeID NextAttackType
        {
            get => m_nextAttackType != null ? m_nextAttackType : m_defaultAttackId;
            set
            {
                if (m_nextAttackType == value)
                    return;
                m_nextAttackType = value;
                m_currentMoveIdx = -1;
                AttackDataChangedEvent.Trigger(ControlCentre.ControlledObject, this);
            }
        }

#if UNITY_EDITOR
        public int Editor_CurrentMoveIdx
        {
            get => CurrentMoveIdx;
            set => CurrentMoveIdx = value;
        }
#endif
        int CurrentMoveIdx
        {
            get => m_currentMoveIdx;
            set
            {
                if (m_currentMoveIdx == value)
                    return;
                m_currentMoveIdx = value;
                AttackDataChangedEvent.Trigger(ControlCentre.ControlledObject, this);
            }
        }

        public AttackData GetCurrentAttackData
        {
            get
            {
                var move = GetMoveData(m_currentAttackType, m_currentMoveIdx);
                return move.GetAttackData(m_currentAttackType, CurrentWeapons);
            }
        }

        public AttackData GetNextAttackData
        {
            get
            {
                var move = HasNextMove ? GetMoveData(NextAttackType, NextMoveIdx) : GetMoveData(m_defaultAttackId);
                return move.GetAttackData(NextAttackType, CurrentWeapons);
            }
        }

        #endregion

        // todo 4: still needed?
        // currently used from playmaker
        [UsedImplicitly]
        public float GetAttackRange(AttackTypeID attackType) => GetMoveData(attackType).GetAttackRange(CurrentWeapons);

        public WeaponConfig.WeaponComboMove GetMoveData(AttackTypeID attackType, int move = 0) =>
            AttackTypeIDValid(attackType) ? m_moveSets[attackType][move] : default;

        // ReSharper disable once FlagArgument
        bool AttackTypeIDValid(AttackTypeID attackType)
        {
            if (this == null || ControlCentre == null)
                return false;

            if (!m_moveSetsInitialized)
            {
                Debug.Log("Moves have not been initialized!");
                return false;
            }
            if (m_moveSets.ContainsKey(attackType))
                return true;

            Debug.LogError($"Key does not exist {attackType} for {ControlCentre.ControlledObject}");
            return false;
        }
    }
}