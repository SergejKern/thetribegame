using Core.Extensions;
using Game.Actors.Model_Animation;
using Game.Combat.Weapon;
using Game.Configs;
using Game.Configs.Combat;
using GameUtility.Data.EntityAnima;
using GameUtility.Events;
using GameUtility.Interface;
using UnityEngine;

namespace Game.Actors.Controls
{
    public partial class AttackControl : IReactToModelInitialized
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        /// <summary>
        /// when no Weapon is equipped, default settings for attacking (f.e. barehanded)
        /// if not set, not able to attack
        /// </summary>
        [SerializeField]
        Weapon[] m_defaultWeapons = new Weapon[0];
#pragma warning restore 0649 // wrong warnings for SerializeField

        protected override void Start()
        {
            base.Start();
            if (ControlCentre.Animator != null)
                InitWithDefaultWeapon();

            LinkAnimation();
            InitCharge();
        }

        public void OnModelInitialized()
        {
            if (!Initialized)
                Init();
            if (!m_defaultWeapons.IsNullOrEmpty() && CurrentWeapons.IsNullOrEmpty())
                InitWithDefaultWeapon();

            LinkAnimation();
        }

        void LinkAnimation()
        {
            var c = ControlCentre;
            if (c == null)
                return;
            var m = c.Model;
            if (m == null)
                return;
            if (!m.TryGetComponent(out m_moveHandler))
                m_moveHandler = m.AddComponent<AnimatorMoveHandlerComponent>();
            m_moveHandler.LinkRigidbody(ControlCentre.RigidbodyComponent);

            if (!m.TryGetComponent<ActorAnimationEvents>(out var animationEvents))
                return;
            animationEvents.SetAttackControl(this);
        }

        public void SetOverrideConfig(WeaponConfig config)
        {
            m_overrideConfig = config;
            ReInitMoves();
        }

        public void ReInitMoves() => InitMoves(ControlCentre, m_currentWeaponMovesType);

        void InitMoves(IControlCentre actor, WeaponMovesType movesType)
        {
            m_moveSetsInitialized = true;
            var weaponConfig = m_overrideConfig == null ? ConfigLinks.WeaponConfig : m_overrideConfig;
            weaponConfig.GetMoves(actor.Animator.runtimeAnimatorController, movesType, ref m_moveSets);
            AttackDataChangedEvent.Trigger(actor.ControlledObject, this);
        }


        void InitWithDefaultWeapon()
        {
            if (!CurrentWeapons.IsNullOrEmpty())
                return;
            SetToDefaultWeapons();
            InitMoves(ControlCentre, WeaponMovesType.Default);
        }

        void SetToDefaultWeapons()
        {
            CurrentWeapons.Clear();
            CurrentWeapons.AddRange(m_defaultWeapons);
        }
    }
}