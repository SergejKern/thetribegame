#define PHYSICS_BASED_MOVEMENT

using System;
using System.Collections.Generic;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Types;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Input;
using GameUtility.InitSystem;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class MoveControl : InputVec2ToFSMEvent, IInputActionReceiver, IInitDataComponent<MoveControl.ConfigData>, IReactToModelInitialized
    {
        const float k_rayDownOriginCastHeight = 0.7f;
        const float k_rayCastHeightForPlatforms = -0.5f;

        [Serializable]
        public struct ConfigData
        {
            public float MoveSpeedMetersPerSecond;
            public float RotationSpeedSLerpFactor;
            public bool DirectionControlEnabled;
            public bool RestrictMoveToViewport;

            public float BlockMoveOverPlatformForSeconds;

            public static ConfigData Default = new ConfigData()
            {
                MoveSpeedMetersPerSecond = 8,
                RotationSpeedSLerpFactor = 20f,
                DirectionControlEnabled = true,
                RestrictMoveToViewport = false,
                BlockMoveOverPlatformForSeconds = 0.5f
            };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        //[Header("Input")]
        [FormerlySerializedAs("dirActionRef")] [SerializeField]
        InputActionReference m_dirActionRef;

        [Header("Move")]
        [SerializeField] AnimatorFloatRef m_directionX;
        [SerializeField] AnimatorFloatRef m_directionZ;

        [SerializeField] RefITimeMultiplier m_timeControl;
        [SerializeField] float m_lerpAnimValuesSpeed = 5f;

        [SerializeField] LayerMask m_platformLayerMask;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        const float k_vpMin = 0.1f;
        const float k_vpMax = 1 - k_vpMin;

        InputAction m_moveAction;
        InputAction m_dirAction;

        Vector2 m_moveInput;
        Vector2 m_dirInput;

        bool m_moveOnUpdate;
        float m_speedFactor = 1f;

        bool m_fixPos;
        bool m_dirControlEnabled;

        public bool FakeMove;

        // ReSharper disable ConvertToAutoPropertyWhenPossible
        public float MetersPerSecond => m_data.MoveSpeedMetersPerSecond;
        // ReSharper restore ConvertToAutoPropertyWhenPossible

        Vector3 Position
        {
            get
            {
#if PHYSICS_BASED_MOVEMENT
                return ControlCentre.RigidbodyComponent.Position;
#else
                return m_controlCentre.ControlledObject.transform.position;
#endif
            }
            set
            {
#if PHYSICS_BASED_MOVEMENT
                //Debug.Log(value.ToString("F3"));
                ControlCentre.RigidbodyComponent.Position = value;
#else
                m_controlCentre.ControlledObject.transform.position = value;
#endif
            }
        }
        Quaternion Rotation
        {
            get
            {
#if PHYSICS_BASED_MOVEMENT
                return ControlCentre.RigidbodyComponent.Rotation;
#else
                return m_controlCentre.ControlledObject.transform.rotation;
#endif
            }
            set
            {
#if PHYSICS_BASED_MOVEMENT
                ControlCentre.RigidbodyComponent.Rotation = value;
#else
                m_controlCentre.ControlledObject.transform.rotation = value;
#endif
            }
        }

        void OnDisable()
        {
            if (ControlCentre == null)
                return;
            m_directionX.Value = 0.0f;
            m_directionZ.Value = 0.0f;
        }

        public void SetRotationLerpValue(float r) => m_data.RotationSpeedSLerpFactor = r;

        public void SetMoveInput(Vector2 input)
        {
            m_moveInput = input;
            SetControlInput(input.sqrMagnitude);
        }

        public void SetDirectionInput(Vector2 dir) => m_dirInput = dir;

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            m_dirControlEnabled = m_data.DirectionControlEnabled;
        }

        // Start is called before the first frame update
        protected override void Init()
        {
            if (Initialized)
                return;
            base.Init();
        }

        public void OnModelInitialized()
        {
            if (!Initialized)
                Init();
            //if (m_controlCentre == null || m_controlCentre.Animator == null)
            //    return;
            m_directionX.Animator = ControlCentre.Animator;
            m_directionZ.Animator = ControlCentre.Animator;
        }

#if PHYSICS_BASED_MOVEMENT
        // Physics based movement must be in FixedUpdate or camera/movement will jitter
        void FixedUpdate()
        {
            if (ControlCentre == null || ControlCentre.RigidbodyComponent == null)
                return;

            UpdateInputFromInputAction();
            if (!m_moveOnUpdate)
            {
                m_directionX.Value = Mathf.Lerp(m_directionX.Value, 0f, m_lerpAnimValuesSpeed * Time.deltaTime);
                m_directionZ.Value = Mathf.Lerp(m_directionZ.Value, 0f, m_lerpAnimValuesSpeed * Time.deltaTime);
                return;
            }   
            MoveUpdate();
        }
#else
        void Update()
        {
            if (m_controlCentre == null)
                return;
            if (!m_moveOnUpdate)
                return;

            UpdateInputFromInputAction();
            MoveUpdate();
        }
#endif
        void UpdateInputFromInputAction()
        {
            if (m_moveAction != null)
                SetMoveInput(m_moveAction.ReadValue<Vector2>());
            if (m_dirAction != null && m_dirAction.enabled && m_dirControlEnabled)
                SetDirectionInput(m_dirAction.ReadValue<Vector2>());
        }

        void MoveUpdate()
        {
            // Debug.Log(m_moveInput);
            var moveDir = new Vector3(m_moveInput.x, 0, m_moveInput.y);
            var lookDir = m_dirInput;

            Move(moveDir, lookDir, m_speedFactor);

            if (ControlCentre.PlayerInput == null)
                return;
            if (m_fixPos)
                FixPositionToViewPort();
        }

        [Preserve] // Playmaker
        public void DoMove() => DoMove(m_speedFactor);
        public void DoMove(float speedFactor)
        {
            if (ControlCentre == null)
                return;

            m_moveOnUpdate = true;
            m_speedFactor = speedFactor;
        }

        [Preserve, UsedImplicitly] // Playmaker
        public void DoRotate()
        {
            if (ControlCentre == null)
                return;
            UpdateInputFromInputAction();
            var moveDir = new Vector3(m_moveInput.x, 0, m_moveInput.y);
            var lookDir = m_dirInput;

            var targetRot = GetTargetRotation(moveDir, lookDir);
            Rotation = targetRot;
        }

        void Move(Vector3 moveDir, Vector2 lookDir, float speedFactor)
        {
            if (Time.timeScale < float.Epsilon)
                return;
            if (ControlCentre.Blocked || !enabled)
                return;

            var meters = m_timeControl.DeltaTime * m_data.MoveSpeedMetersPerSecond * speedFactor;

            Rotate(moveDir, lookDir);

            var magnitude = moveDir.sqrMagnitude;
            if (ControlCentre.Animator != null)
            {
                m_directionX.Sync();
                m_directionZ.Sync();
                var speed = magnitude * speedFactor;

                var animVector = speed * Vector2.up;
                if (m_moveInput.sqrMagnitude > float.Epsilon && lookDir.sqrMagnitude > float.Epsilon)
                {
                    var angle = (360 + Vector2.SignedAngle(m_moveInput, lookDir)) % 360;
                    animVector = speed * Vector2.up.Rotate(angle);
                    //Debug.Log($"moveDir {moveInput} lookDir {lookDir} angle {angle}");
                }
                m_directionX.Value = Mathf.Lerp(m_directionX.Value, animVector.x, m_lerpAnimValuesSpeed * Time.deltaTime);
                m_directionZ.Value = Mathf.Lerp(m_directionZ.Value, animVector.y, m_lerpAnimValuesSpeed * Time.deltaTime);

                if (Mathf.Abs(m_directionX.Value - animVector.x) < 0.25f)
                    m_directionX.Value = animVector.x;
                if (Mathf.Abs(m_directionZ.Value - animVector.y) < 0.25f)
                    m_directionZ.Value = animVector.y;
            }

            var newPos = Position + (meters * moveDir);

            var isValidPos = true;
            SlowMoveNearEdge(ref newPos, moveDir, meters);
            var cam = Camera.main;
            if (cam != null && m_data.RestrictMoveToViewport)
            {
                var vp = cam.WorldToViewportPoint(newPos);
                isValidPos = (vp.x >= k_vpMin && vp.x <= k_vpMax & vp.y >= k_vpMin && vp.y <= k_vpMax);
            }

            m_fixPos = !isValidPos;
            if (isValidPos && !FakeMove)
                Position = newPos;
        }

        float m_blockedMoveSeconds;
        void SlowMoveNearEdge(ref Vector3 pos, Vector3 moveDir, float meters)
        {
            var testMeters = 5f * meters;
            var testPos = Position + testMeters * moveDir;

            var ray = new Ray(testPos + k_rayDownOriginCastHeight * Vector3.up, Vector3.down);
            var result = Physics.Raycast(ray, out _, 2f, m_platformLayerMask);
            //var color = result? Color.green : Color.red;
            //Debug.DrawRay(ray.origin, ray.direction * 2f, color, 1.0f);

            if (SlowMoveNearEdge_MoveOk(result)) 
                return;

            ray = new Ray(testPos + k_rayCastHeightForPlatforms * Vector3.up, -moveDir);
            result = Physics.Raycast(ray, out _, 0.5f*testMeters, m_platformLayerMask);

            if (SlowMoveNearEdge_MoveOk(result)) 
                return;

            pos = Vector3.Lerp(Position, pos, m_blockedMoveSeconds / m_data.BlockMoveOverPlatformForSeconds);
            m_blockedMoveSeconds += Time.deltaTime;
            //return m_blockedMoveSeconds >= m_data.BlockMoveOverPlatformForSeconds;
        }     
        
        bool SlowMoveNearEdge_MoveOk(bool ok)
        {
            if (ok) m_blockedMoveSeconds = 0f;
            return ok;
        }

        void FixPositionToViewPort()
        {
            var cam = Camera.main;
            if (cam == null)
                return;

            var vp = cam.WorldToViewportPoint(Position);
            vp = new Vector3(Mathf.Clamp(vp.x, k_vpMin, k_vpMax), Mathf.Clamp(vp.y, k_vpMin, k_vpMax), vp.z);

            Position = cam.ViewportToWorldPoint(vp);
            m_fixPos = false;
        }

        void Rotate(Vector3 moveDir, Vector2 lookDir)
        {
            var targetRot = GetTargetRotation(moveDir, lookDir);
            Rotation = Quaternion.Slerp(Rotation, targetRot, m_timeControl.DeltaTime * m_data.RotationSpeedSLerpFactor);
        }

        Quaternion GetTargetRotation(Vector3 moveDir, Vector2 lookDir)
        {
            var magnitude = moveDir.sqrMagnitude;
            var targetRot = Rotation;

            if (lookDir.sqrMagnitude > float.Epsilon)
                targetRot = Quaternion.LookRotation(new Vector3(lookDir.x, 0, lookDir.y));
            else if (magnitude > float.Epsilon)
                targetRot = Quaternion.LookRotation(moveDir);
            return targetRot;
        }


        [Preserve, UsedImplicitly] // Playmaker
        public void MoveStopped() => m_moveOnUpdate = false;

        public override IReadOnlyList<ActionMapping> Mapping => new[]
        {
            new ActionMapping() { ActionRef = ActionRef,
                LinkAction = (a) => {
                    m_moveAction = a;
                    //m_moveAction.Enable();
                },
                UnlinkAction = (a) =>
                {
                    //m_moveAction?.Disable();
                    m_moveAction = null;
                },
                Call = OnControl
            },
            new ActionMapping() { ActionRef = m_dirActionRef,
                LinkAction = (a) =>
                {
                    m_dirAction = a;
                    //m_dirAction.Enable();
                },
                UnlinkAction = (a) =>
                {
                    //m_dirAction?.Disable();
                    m_dirAction = null;
                }
            },
        };

        public void SetAimingEnabled(bool enable) => m_dirControlEnabled = enable && m_data.DirectionControlEnabled;
    }
}