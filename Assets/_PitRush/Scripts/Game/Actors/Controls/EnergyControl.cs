using System;
using Core.Unity.Interface;
using Core.Unity.Attribute;
using Core.Unity.Types.Fsm;
using Core.Unity.Types;
using Game.Actors.Energy;
using GameUtility.Energy;
using GameUtility.InitSystem;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class EnergyControl : InputStateToFSMEvent, IInitDataComponent<EnergyControl.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;
            [FormerlySerializedAs("SecondsToHeal")]
            public float SecondsToRegenerate;

            public EnergyID SourceEnergyID;
            public EnergyID DestinationEnergyID;

            public float EnergyCost;
            [FormerlySerializedAs("HealAmount")]
            public float RegenerateAmount;

            public static ConfigData Default = new ConfigData()
            {
                Enabled = true,
                SecondsToRegenerate = 1.5f,
                EnergyCost = 1,
                RegenerateAmount = 3,
            };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [Header("EnergyControl")]
        [SerializeField]
        AnimatorBoolRef m_casting;

        [SerializeField] EnergyCentre m_energyCentre;

        [SerializeField]
        ParticleSystem m_onHealedParticles;

        [SerializeField]
        RefIAudioAsset m_onHealedSoundAsset;

        [FormerlySerializedAs("allowCasting")] [FSMDrawer(editVariable: false), SerializeField]
        FSMBoolRef m_allowCasting;

        [SerializeField] RefITimeMultiplier m_timeControl;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField
        IAudioAsset OnHealedSound => m_onHealedSoundAsset.Result;
        IEnergyComponent m_sourceEnergy;
        IEnergyComponent m_destinationEnergy;

        bool m_shouldCast;
        bool m_isCasting;
        float m_castingTime = -1f;

        bool HealReady => m_castingTime > m_data.SecondsToRegenerate && SourceEnergyAvailable;
        bool SourceEnergyAvailable => m_sourceEnergy?.CanUse(1f) == true;
        protected override bool CanInvokeActive => base.CanInvokeActive && m_allowCasting.Value;

        // ReSharper disable ConvertToAutoPropertyWhenPossible


        void LinkEnergy()
        {
            if (m_energyCentre == null)
                return;
            m_sourceEnergy = m_energyCentre.GetEnergyComponentOfType(m_data.SourceEnergyID);
            m_destinationEnergy = m_energyCentre.GetEnergyComponentOfType(m_data.DestinationEnergyID);
        }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;
            LinkEnergy();
        }
        // ReSharper restore ConvertToAutoPropertyWhenPossible

        [Preserve, UsedImplicitly] // Is Called from Playmaker
        public void OnCast()
        {
            m_shouldCast = true;
            m_castingTime = 0f;
        }

        [Preserve, UsedImplicitly] // Is Called from Playmaker
        public void OnCastRelease()
        {
            m_shouldCast = false;
            m_castingTime = -1f;
        }

        protected override void Update()
        {
            base.Update();

            m_allowCasting.Value = SourceEnergyAvailable;
            if (ControlCentre.Blocked)
                return;
            UpdateCasting();
        }

        void UpdateCasting()
        {
            if (m_isCasting != m_shouldCast)
            {
                m_isCasting = m_shouldCast;
                m_castingTime = 0f;

                m_casting.Value = m_isCasting;
            }

            if (!m_isCasting)
                return;

            if (!SourceEnergyAvailable)
                InvokeRelease();

            //Debug.Log(castingTime);
            m_castingTime += m_timeControl.DeltaTime;

            if (HealReady)
                DoHeal();
        }

        void DoHeal()
        {
            if (m_onHealedParticles != null)
                m_onHealedParticles.Play();
            OnHealedSound.Play(ControlCentre.ControlledTransform);
            m_sourceEnergy.Use(m_data.EnergyCost);
            m_destinationEnergy.Regenerate(m_data.RegenerateAmount);
            m_castingTime = 0.0f;
        }
        //public IReadOnlyList<ActionMapping> Mapping => new []
        //{
        //    new ActionMapping() { ActionRef = energyActionRef, Call = OnCast }
        //};
    }
}