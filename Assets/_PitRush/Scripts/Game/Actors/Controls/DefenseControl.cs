using System;
using Core.Unity.Interface;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Types.Fsm;
using Core.Unity.Types;
using Game.Actors.Energy;
using Game.Combat;
using GameUtility.Data.FX;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using GameUtility.Energy;
using GameUtility.InitSystem;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public class DefenseControl : InputStateToFSMEvent, IDamageProtectionAction,
        IInitDataComponent<DefenseControl.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;
            public bool StaggerEnabled;
            public float BlockAngle;
            public float BlockSecondsBeforeHitToStagger;

            public float EnergyCostFromDamageMultiplier;
            public float EnergyCostConstant;
            public float MoveFactorWhenBlocking;
            public float NoShieldBlockTime;
            public float NoShieldBlockCooldown;

            public EnergyID EnergyID;

            public static ConfigData Default = new ConfigData()
            {
                Enabled = true, StaggerEnabled = false /*not implemented*/,
                BlockAngle = 65, BlockSecondsBeforeHitToStagger = 0.2f,
                EnergyCostFromDamageMultiplier = 1,
                EnergyCostConstant = 0,
                MoveFactorWhenBlocking = 0.4f,
                NoShieldBlockTime = 1f,
                NoShieldBlockCooldown = 1f
            };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [Header("Defense")]
        [SerializeField, AnimTypeDrawer]
        AnimatorBoolRef m_noShieldBlock;

        [SerializeField, AnimTypeDrawer]
        AnimatorBoolRef m_shieldBlock;

        [FSMDrawer]
        [SerializeField] FSMFloatRef m_moveFactorWhenBlocking;

        [SerializeField] CollisionMaterialID m_noShieldCollisionMaterial;

        [SerializeField]
        RefIAudioAsset m_staggerSoundAsset;

        [SerializeField] EnergyCentre m_energyCentre;

        [SerializeField] RefITimeMultiplier m_timeControl;
        [SerializeField] FX_ID m_defenseId;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField
        IAudioAsset StaggerSound => m_staggerSoundAsset.Result;
        //IAudioAsset BlockSound => m_blockSoundAsset.Result;
        IEnergyComponent m_energy;
        IFXComponent m_fxComponent;
        GameObject BlockVisual => m_fxComponent.GetInstance(m_defenseId);

        protected override bool CanInvokeActive => base.CanInvokeActive && CanBlock;

        bool CanBlock => (m_energy == null || m_energy.Energy > 0f) && m_coolTimer <= float.Epsilon;
        bool HasShield => m_currentShield != null;
        public bool IsProtected { get; private set; }

        bool m_shouldBlock;
        float m_blockTime = -1f;
        float m_coolTimer = -1f;
        float m_noShieldBlockTime;

        IShield m_currentShield;
        //IEnergyComponent m_health;

        public void GetData(out ConfigData data) => data = m_data;

        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;
            m_moveFactorWhenBlocking.Value = m_data.MoveFactorWhenBlocking;
            m_noShieldBlockTime = m_data.NoShieldBlockTime;

            if (m_energyCentre != null)
                m_energy = m_energyCentre.GetEnergyComponentOfType(m_data.EnergyID);

            // m_health = m_energyCentre.GetEnergyComponentOfType(EnergyType.Health);
        }

        [UsedImplicitly]
        public void ResetNoShieldBlockTime() => m_noShieldBlockTime = m_data.NoShieldBlockTime;
        [UsedImplicitly]
        public void SetNoShieldBlockTime(float time) => m_noShieldBlockTime = time;
        public bool IsProtectedForSeconds(float seconds) => IsProtected;

        [Preserve, UsedImplicitly] // Is Called from Playmaker
        public void OnDefense()
        {
            m_shouldBlock = true;
            m_blockTime = 0f;

            m_energy?.SetRegenerationBlocked(true);
            //Debug.Log(blockTime);
        }

        [Preserve, UsedImplicitly] // Is Called from Playmaker
        public void OnDefenseRelease()
        {
            if (!HasShield)
                m_coolTimer = m_data.NoShieldBlockCooldown;// Mathf.Min(m_blockTime, m_data.NoShieldBlockCooldown);

            m_shouldBlock = false;
            m_blockTime = -1f;
            m_energy?.SetRegenerationBlocked(false);
            //Debug.Log(blockTime);
        }

        public bool IsBlocking(Vector2 direction, float damage, out CollisionMaterialID collisionMaterial)
        {
            collisionMaterial = null;
            if (!IsProtected)
                return false;

            var tr = ControlCentre.ControlledTransform;

            var forward = tr.forward;
            var angle = Vector2.Angle(-direction.normalized, new Vector2(forward.x,forward.z));

            var blocked = angle < m_data.BlockAngle;
            // if (blocked)
            //     BlockSound?.Play(tr);

            if (m_data.StaggerEnabled && m_blockTime < m_data.BlockSecondsBeforeHitToStagger)
                StaggerSound.Play(tr);

            m_energy?.Use(m_data.EnergyCostConstant + damage * m_data.EnergyCostFromDamageMultiplier);

            collisionMaterial = m_currentShield != null ? m_currentShield.CollisionMaterial : m_noShieldCollisionMaterial;

            if (blocked)
                AvoidingDamageActionEvent.Trigger(ControlCentre.ControlledObject, 
                    AvoidingDamageActionEvent.ActionCategory.Block);

            return blocked;
        }

        protected override void Update()
        {
            base.Update();
            if (m_coolTimer > 0f)
                m_coolTimer -= m_timeControl.DeltaTime;
            if (ControlCentre.Blocked)
                return;

            UpdateBlocking();
        }

        void UpdateBlocking()
        {
            if (IsProtected)
            {
                m_blockTime += m_timeControl.DeltaTime;
                if (!HasShield && m_blockTime > m_noShieldBlockTime)
                    SetControlInput(0f);
            }
            if (IsProtected == (m_shouldBlock && CanBlock))
                return;

            IsProtected = m_shouldBlock && CanBlock;

            //Debug.Log($"m_isBlocking {m_isBlocking}");
            ControlCentre.Animator.SetBool(m_currentShield != null ? m_shieldBlock : m_noShieldBlock, IsProtected);
            if (BlockVisual != null)
                BlockVisual.SetActiveWithTransition(IsProtected);
        }

        public void OnFXConnected(IFXComponent playerFX)
        {
            if (playerFX == null)
                return;
            m_fxComponent = playerFX;
        }
    }

}
