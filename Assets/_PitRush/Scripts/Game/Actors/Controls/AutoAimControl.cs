﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Core.Unity.Attribute;
using Core.Unity.Interface;
using Core.Unity.Operations;
using Game.Actors.Components;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Input;
using GameUtility.InitSystem;
using UnityEngine.InputSystem;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public class AutoAimControl : MonoBehaviour, IInputActionReceiver, IInitDataComponent<AutoAimControl.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;
            public int Priority;

            public float FarAngleForAutoAim;
            public float CloseAngleForAutoAim;
            public static ConfigData Default = new ConfigData() { Enabled = true };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] InputActionReference m_aimingInputActionRef;
        [SerializeField] InputActionReference m_moveInputActionRef;

        [SerializeField] InRangeTargetFilterBehaviour m_targetFilter;
        [SerializeField] RefIAgent m_agent;

        [CopyPaste, SerializeField] ConfigData m_configData = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float SqrLockDistance => m_targetFilter.SqrLockDistance;

        InputAction m_aimAction;
        InputAction m_moveAction;

        ControlCentre m_controlCentre;

        Vector3 Position => m_controlCentre.Position;
        const float k_heightTolerance = 1.0f;

        public void GetData(out ConfigData data) => data = m_configData;
        public void InitializeWithData(ConfigData data)
        {
            m_configData = data;
            enabled = m_configData.Enabled;
        }

        void Start() => m_controlCentre = GetComponent<ControlCentre>();

        void Update() => GetNearestTargetFromAiming();

        void GetNearestTargetFromAiming()
        {
            var targets = m_targetFilter.InRangeTargets;
            var nearestTarget = TargetOperations.GetNearest2(transform, targets);
            var targetValid = nearestTarget != null;

            if (targetValid)
            {
                var sqrMagnitudePlayer = Vector3.SqrMagnitude(nearestTarget.Transform.position - Position);
                if (sqrMagnitudePlayer > SqrLockDistance)
                    targetValid = false;
                if (Mathf.Abs(nearestTarget.Transform.position.y) > k_heightTolerance)
                    targetValid = false;

                CheckValidFromAiming(nearestTarget, sqrMagnitudePlayer, ref targetValid);
            }

            if (targetValid)
                m_agent.Result.SetTargetWithPriority(nearestTarget, m_configData.Priority);
            else m_agent.Result.ClearTargetPriority(m_configData.Priority);
        }

        void CheckValidFromAiming(ITransformProvider nearestTarget, float sqrMagnitudePlayer, ref bool targetValid)
        {
            var aimInput = m_aimAction.ReadValue<Vector2>();
            if (aimInput.Equals(Vector2.zero))
            {
                aimInput = m_moveAction.ReadValue<Vector2>();
                if (aimInput.Equals(Vector2.zero))
                    return;
            }
            var position = nearestTarget.Transform.position;
            var v2Pos = new Vector2(position.x, position.z);
            var v2 = v2Pos - m_controlCentre.V2Position;
            var newAng = Vector2.Angle(v2.normalized, aimInput.normalized);
            var lerpVal = sqrMagnitudePlayer / SqrLockDistance;
            var validAngle = Mathf.Lerp(m_configData.CloseAngleForAutoAim, m_configData.FarAngleForAutoAim, lerpVal);
            if (newAng > validAngle)
                targetValid = false;
        }

        public IReadOnlyList<ActionMapping> Mapping => new[]
        {
            new ActionMapping()
            {
                ActionRef = m_aimingInputActionRef,
                LinkAction = (a) => m_aimAction = a,
                UnlinkAction = (a) => m_aimAction = null
            },
            new ActionMapping()
            {
                ActionRef = m_moveInputActionRef,
                LinkAction = (a) => m_moveAction = a,
                UnlinkAction = (a) => m_moveAction = null
            }
        };
    }
}