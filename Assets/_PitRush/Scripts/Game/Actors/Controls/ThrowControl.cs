using System;
using Core.Unity.Attribute;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Energy;
using Game.Utility;
using GameUtility.Energy;
using GameUtility.InitSystem;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(GrabControl))]
    public class ThrowControl : InputToFSMMemoryTriggerEvent, 
        IInitDataComponent<ThrowControl.ConfigData>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;

            public bool ReturnToThrower;
            public float EnergyCost;

            public EnergyID EnergyID;

            public static ConfigData Default = new ConfigData()
            {
                Enabled = true,
                ReturnToThrower = true,
                EnergyCost = 0,
            };
        }

        //[Header("Input")]
        //[SerializeField] InputActionReference m_throwActionRef;

        [Header("Throwing")]
        [SerializeField] PrefabData m_throwPrefab;

        [SerializeField] EnergyCentre m_energyCentre;

        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_throwAnim;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ControlCentre m_controlCentre;
        GrabControl m_grabControl;
        IEnergyComponent m_energy;

        protected override void Start()
        {
            base.Start();
            m_grabControl = GetComponent<GrabControl>();
            m_controlCentre = GetComponent<ControlCentre>();
        }

        // so the collider of the container does not move the player
        const float k_throwContainerSpawnDistance = 0.25f;
        const float k_throwContainerSpawnHeight = 0.5f;

        //public void OnThrow(InputAction.CallbackContext obj) => OnThrow();
        [UsedImplicitly]
        public void OnThrow()
        {
            //Debug.Log("OnThrow");
            if (!enabled)
                return;
            if (m_energy != null && m_energy.Energy <= 0f)
                return;

            m_grabControl.Release(out var item);
            if (item == null)
                return;

            m_controlCentre.Animator.Play(m_throwAnim);
            item.Throw();
            m_energy?.Use(m_data.EnergyCost);

            var tr = transform;
            var throwPos = tr.position + k_throwContainerSpawnDistance * tr.forward +
                           k_throwContainerSpawnHeight * Vector3.up;
            var throwContainer = m_throwPrefab.Prefab.GetPooledInstance(throwPos, tr.rotation);

            var itemComponent = item as Component;
            var itemTr = itemComponent != null ? itemComponent.transform : null;
            if (!throwContainer.TryGetComponent<ThrownBehaviour>(out var throwBehaviour))
                return;
            throwBehaviour.Throw(itemTr, m_controlCentre.ControlledObject, transform.forward, new Vector3(0f, 1f, 0f), m_data.ReturnToThrower);
            m_controlCentre.SetRumble(0.2f, 0.1f);
        }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;
            m_energy = m_energyCentre.GetEnergyComponentOfType(m_data.EnergyID);
        }

    }
}