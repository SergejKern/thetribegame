using System;
using System.Collections.Generic;
using Core.Unity.Types.Fsm;
using Game.Actors.Model_Animation;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Energy;
using GameUtility.Events;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public partial class AttackControl : InputToFSMMemoryTriggerEvent
    {
        #region Serialized Fields
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AttackFSMGlue m_fsm;
        [SerializeField, FSMDrawer] FSMEventRef m_onDone;

        [SerializeField] RefIAttackHook m_attackHook;
        [SerializeField] RefITimeMultiplier m_timeControl;

        [SerializeField] AttackTypeID m_defaultAttackId;
#pragma warning restore 0649 // wrong warnings for SerializeField
        #endregion

        #region fields
        IEnergyComponent m_energy;

        AnimatorMoveHandlerComponent m_moveHandler;
        #endregion

        #region Properties
        //public override bool CanInvoke
        //{
        //    get
        //    {
        //        if (ControlCentre.Blocked)
        //            Debug.LogError($"{ControlCentre.ControlledObject} can invoke {base.CanInvoke} blocker {ControlCentre.GetBlocker()} IsValidState {IsValidState} energy {HasEnergy} Ready {Ready} Time {Time.time}");
        //        return base.CanInvoke && Ready;
        //    }
        //}
        public override bool CanInvoke => base.CanInvoke && Ready;
        protected override bool HasEnergy => m_energy == null || m_energy.CanUse(CurrentMove.EnergyCost);

        bool IsReadyState => m_fsm.IsReadyState;
        public bool Ready => AttackReady;
        public bool AttackReady
        {
            get
            {
                var isFollowUpState = m_fsm.IsFollowUpState;

                var followUp = isFollowUpState
                               && HasNextMove
                               && ComboMoveOpen;

                // Debug.Log($"followUp {followUp} IsReadyState {IsReadyState}");
                return IsReadyState || followUp;
            }
        }
        public bool ComboMoveOpen { get; set; }
        #endregion

        readonly List<ICollisionMaterialComponent> m_hitDamageHandlers = new List<ICollisionMaterialComponent>();

        public override void Update()
        {
            base.Update();

            if (CurrentWeapons == null)
                SetToDefaultWeapons();
            //CancelPrevious = CurrentWeapon.HasNextMove;
            //if (!m_released)
            //    UpdateWeaponAttackForCharge(m_timeControl.DeltaTime);

            if (!AttackDone)
                m_fsm.SetSpeedMultiplier(m_timeControl.SpeedFactor);
        }

        void FixedUpdate()
        {
            if (!Released)
                UpdateWeaponAttackForCharge(m_timeControl.FixedDeltaTime);
        }

        void SetColliderActive(bool active)
        {
            foreach (var w in CurrentWeapons)
            {
                if (w == null)
                    continue;
                w.WeaponActive = active;
            }
        }
        public void ForceHitTarget(GameObject target)
        {
            foreach (var w in CurrentWeapons)
            {
                if (w.WeaponActive)
                    w.ForceHitTarget(target);
            }
        }

        [UsedImplicitly]
        public void ResetCombo()
        {
            //Debug.Log("ResetCombo");
            m_releasing = false;
            CurrentMoveIdx = -1;
            ComboMoveOpen = false;
            SetColliderActive(false);
        }

        public override void Trigger()
        {
            ResetCharge();
            base.Trigger();
        }

        // ReSharper disable once FlagArgument
        public void Attack(AttackTypeID attackType)
        {
            if (!AttackReady && !(m_releasing && CanCharge))
                return;
            if (m_attackHook.Result?.HandleAttack(attackType, NextMoveIdx) == AttackHookHandleResult.Handled)
                return;
            m_releasing = false;
            AttackEvent.Trigger(ControlCentre.ControlledObject);
            //Debug.Log("Attack!");
            ComboMoveOpen = false;
            ResetCharge();

            foreach (var w in CurrentWeapons)
                w.WeaponActive = false;

            if (CurrentMoveIdx == -1 || CurrentAttackType != attackType)
                m_currentAttackType = attackType;

            CurrentMoveIdx = NextMoveIdx;
            ApplyCurrentMove();

            m_fsm.OnWeaponUse();

            if (!HasNextMove)
                NextAttackType = m_defaultAttackId;
        }

        [UsedImplicitly]
        internal void DoAttack(AttackTypeID attackType)
        {
            if (CurrentWeapons == null)
                return;
            if (!Ready || ControlCentre.Blocked)
                return;

            m_energy?.Use(NextEnergyCost);
            Released = Action == null;
            //m_releasing = false;
            Attack(attackType);
        }

        void ApplyCurrentMove()
        {
            foreach (var w in CurrentWeapons)
            {
                var fxFilter = Array.Find(CurrentMove.WeaponFxList, wfx => wfx.FXID == m_defaultFXID);
                w.SetCurrentActivationData(fxFilter.Variation);
            }
            // todo 3: add rumble to config
            // ControlCentre.SetRumble(0.1f, 0.05f);

            m_hitDamageHandlers.Clear();

            foreach (var w in CurrentWeapons)
                w.ApplyDamage(CurrentMove.DamageFactor, CurrentMove.KickbackForceFactor, m_hitDamageHandlers);
            
            if (m_moveHandler != null)
                m_moveHandler.SetMotionFactor(CurrentMove.MotionFactor);
            m_fsm.ApplyCurrentMove(CurrentMove, m_timeControl.SpeedFactor);
        }

        // todo 4: what if on timeline already for next attack?
        void OnAttackDone()
        {
            Released = true;
            Charge = 0f;
            ResetCombo();
            m_onDone.Invoke();

            m_currentAttackType = m_defaultAttackId;
            NextAttackType = m_defaultAttackId;
        }

        // todo 3: test if we can use this also instead of OnAttackDone
        void AttackCancel()
        {
            ResetCombo();
            m_onDone.Invoke();

            if (!Released && Charge > m_data.HoldDurationForChargingFX)
                m_chargePaused = true;
            else
            {
                Released = true;

                Charge = 0;
                NextAttackType = m_defaultAttackId;
            }
        }
    }

}