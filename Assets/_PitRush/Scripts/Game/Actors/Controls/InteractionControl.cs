using System.Collections.Generic;
using System.Linq;
using Core.Unity.Interface;
using Core.Unity.Operations;
using GameUtility.Components.Collision;
using GameUtility.Data.Input;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class InteractionControl : InputStateToFSMEvent, IInputActionReceiver, ITriggerObjEnterExitHandling
    {
        internal struct InteractiveData : ITransformProvider
        {
            public readonly GameObject GO;
            public Transform Transform { get; }
            public readonly IInteractive Interactive;
            public readonly IHighlightable Highlightable;

            public bool Holding;
            public InteractiveData(GameObject go)
            {
                GO = go;
                Transform = go.transform;
                Interactive = go.GetComponent<IInteractive>();
                Highlightable = go.GetComponent<IHighlightable>();
                Holding = false;
            }
        }

        InteractiveData m_currentInteractive;
        internal InteractiveData CurrentInteractive
        {
            get => m_currentInteractive;
            private set
            {
                Release();
                m_currentInteractive = value;
            }
        }

        protected override bool CanInvokeActive => base.CanInvokeActive && InteractPossible;
        protected bool InteractPossible
        {
            get
            {
                if (!enabled)
                    return false;
                if (CurrentInteractive.Interactive == null)
                    return false;
                if (CurrentInteractive.Interactive.InteractEnabled == false)
                    return false;
                return !ControlCentre.Blocked;
            }
        }

        readonly List<InteractiveData> m_interactive = new List<InteractiveData>();

        //public virtual void OnInteract(InputAction.CallbackContext _) => OnInteract();
        [UsedImplicitly]
        public virtual void OnInteract()
        {
            if (!InteractPossible)
                return;
            if (CurrentInteractive.Interactive is IInteractivePress device)
                device.Interact(ControlCentre.ControlledObject);
            if (CurrentInteractive.Interactive is IInteractiveHolding)
                m_currentInteractive.Holding = true;
        }

        public void Release()
        {
            if (!m_currentInteractive.Holding)
                return;
            if (m_currentInteractive.Interactive is IInteractiveHolding holdingDevice)
                holdingDevice.OnRelease(ControlCentre.ControlledObject);
            m_currentInteractive.Holding = false;
            InvokeRelease();
        }

        protected override void Update()
        {
            base.Update();
            HighlightClosestInteractive();
            UpdateActiveHolding();
        }

        void UpdateActiveHolding()
        {
            if (!m_active)
            {
                Release();
                return;
            }
            if (CurrentInteractive.Interactive is IInteractiveHolding)
                OnInteract();
        }

        void HighlightClosestInteractive()
        {
            m_interactive.RemoveAll((a) => a.GO == null);
            var closest = TargetOperations.GetNearest2(transform, m_interactive.Where((a) => a.Interactive.InteractEnabled));

            if (CurrentInteractive.GO == closest.GO)
                return;

            DisableCurrentHighlight();

            CurrentInteractive = closest;

            if (CurrentInteractive.GO == null)
                return;

            if (CurrentInteractive.Highlightable == null)
            {
                Debug.LogWarning($"interactive Object {gameObject} is not highlighted, missing visual communication to player!");
                return;
            }

            CurrentInteractive.Highlightable.EnableHighlight();
        }

        void DisableCurrentHighlight()
        {
            CurrentInteractive.Highlightable?.DisableHighlight();
            CurrentInteractive = default;
        }

        public void OnTriggerObjectEnter(GameObject root)
        {
            if (!root.TryGetComponent<IInteractive>(out _))
                return;

            var containedIdx = m_interactive.FindIndex(data => data.GO == root);

            if (containedIdx == -1)
                m_interactive.Add(new InteractiveData(root));
        }

        public void OnTriggerObjectExit(GameObject root)
        {
            if (!root.TryGetComponent<IInteractive>(out _))
                return;

            var containedIdx = m_interactive.FindIndex(data => data.GO == root);
            m_interactive.RemoveAt(containedIdx);
        }

        public override IReadOnlyList<ActionMapping> Mapping => new[] { new ActionMapping()
        {
            ActionRef = ActionRef,
            Call = OnControl
        }};
    }
}