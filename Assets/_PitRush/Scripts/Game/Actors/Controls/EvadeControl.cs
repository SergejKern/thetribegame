using System;
using System.Linq;
using Core.Events;
using Core.Unity.Interface;
using Core.Unity.Attribute;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Energy;
using Game.Combat;
using GameUtility.Coroutines;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.FX;
using GameUtility.Energy;
using GameUtility.InitSystem;
using GameUtility.Interface;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public class EvadeControl : InputToFSMMemoryTriggerEvent, IDamageProtectionAction,
        IEventListener<DangerTargetUpdatedEvent>,
        IInitDataComponent<EvadeControl.ConfigData>, 
        IReactToModelInitialized
    {
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;
            public bool TimeFixEnabled;
            public float TimeFixMaxAmount;
            public float Duration;
            public float CooldownSeconds;

            public EnergyID EnergyID;
            public float EnergyCost;

            public static ConfigData Default = new ConfigData() 
            { 
                Enabled = true,
                Duration = 0.25f, CooldownSeconds = 0.1f, 
                EnergyCost = 1
            };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] EnergyID m_healthID;

        [SerializeField, AnimTypeDrawer(self: false)]
        AnimatorStateRef m_evadeAnim;
        [SerializeField, AnimTypeDrawer(self: false)]
        AnimatorFloatRef m_evadeSpeed;

        [SerializeField] FlickerData m_flickerData;

        [SerializeField]
        RefIAudioAsset m_evadeSoundAsset;
        [SerializeField]
        FlashDataNoTarget m_flashData;

        [SerializeField] EnergyCentre m_energyCentre;

        [SerializeField] RefITimeMultiplier m_timeMultiplier;
        [SerializeField] FX_ID m_evadeFXID;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField
        IAudioAsset EvadeSound => m_evadeSoundAsset.Result;
        IEnergyComponent m_energy;
        IEnergyComponent m_health;
        GameObject EvadeEffect => m_fxComponent.GetInstance(m_evadeFXID);

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        bool IsEvading => m_timer > m_data.CooldownSeconds;
        bool IsOnCooldown => m_timer > 0;

        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        bool CanEvade => !IsEvading && !IsOnCooldown;
        public override bool CanInvoke => base.CanInvoke && CanEvade;
        protected override bool HasEnergy => m_energy == null || m_energy.CanUse(m_data.EnergyCost);
        public bool CanInvokeAsFallback => !ControlCentre.Blocked && IsValidState
                                                                   && HasEnergy && CanEvade;

        public bool IsProtected => IsEvading;

        IFXComponent m_fxComponent;

        float m_timer;
        //bool m_wasEvading;

        float m_evadeStretch;

        void OnEnable() => EventMessenger.AddListener(this);
        void OnDisable() => EventMessenger.RemoveListener(this);

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;

            InitEnergy();
        }

        public void OnModelInitialized()
        {
            if (!Initialized)
                Init();
            var rend = ControlCentre.Model.GetComponentsInChildren<Renderer>().ToList();
            m_flickerData.Renderer = rend;
        }

        public void OnFXConnected(IFXComponent playerFX)
        {
            if (playerFX == null)
                return;
            m_fxComponent = playerFX;
        }

        protected override void Init()
        {
            base.Init();
            InitEnergy();
        }

        void InitEnergy()
        {
            if (m_energyCentre == null)
                return;
            m_energy = m_energyCentre.GetEnergyComponentOfType(m_data.EnergyID);
            m_health = m_energyCentre.GetEnergyComponentOfType(m_healthID);
        }

        [Preserve, UsedImplicitly] // Is Called from Playmaker
        public void OnEvade() => DoEvade();
       
        void DoEvade()
        {
            if (ControlCentre.Blocked)
                return;
            if (!CanEvade)
                return;

            var evadeDuration = m_data.Duration;
            m_evadeStretch = 1f;

            var time = m_timeMultiplier?.Result?.Time ?? Time.time;
            if (time + evadeDuration <= m_nextAttackArrives + 0.05f && m_data.TimeFixEnabled)
            {
                var neededTime = m_nextAttackArrives - time;
                if (neededTime < m_data.TimeFixMaxAmount)
                    evadeDuration = Mathf.Min(neededTime + 0.05f, m_data.TimeFixMaxAmount);

                m_evadeStretch = m_data.Duration / evadeDuration;
            }

            m_timer = evadeDuration + m_data.CooldownSeconds;

            //if (m_timeMultiplier?.Result?.Time)
            if (ControlCentre.Animator != null)
                ControlCentre.Animator.Play(m_evadeAnim);

            EvadeSound?.Play(ControlCentre.ControlledTransform);
            m_energy?.Use(m_data.EnergyCost);

            var tr = transform;
            EvadeEffect.GetPooledInstance(tr.position+Vector3.up, tr.rotation);
            m_health.SetInvulnerable(evadeDuration);
            StartCoroutine(FlickerRoutine.Flicker(m_flickerData));

            m_flashData.FlashDuration = evadeDuration;
            StartCoroutine(FlashByMaterialExchangeRoutine.Flash(m_flashData, ControlCentre.Model));

            AvoidingDamageActionEvent.Trigger(ControlCentre.ControlledObject, 
                AvoidingDamageActionEvent.ActionCategory.Evade);
        }

        public override void Update()
        {
            base.Update();
            if (m_timer > 0)
                m_timer -= m_timeMultiplier.DeltaTime;

            if (IsEvading)
                ControlCentre.Animator.SetFloat(m_evadeSpeed, m_timeMultiplier.SpeedFactor * m_evadeStretch);

            // m_wasEvading = IsEvading;
        }

        // ReSharper disable once FlagArgument
        float m_nextAttackArrives;

        public bool IsProtectedForSeconds(float seconds) => 
            m_timer - seconds > m_data.CooldownSeconds;

        public void OnEvent(DangerTargetUpdatedEvent eventType) 
            => m_nextAttackArrives = eventType.AttackArrivesOnTime;
    }

}
