using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Types.Fsm;
using GameUtility.Data.Input;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class InputStateToFSMEvent : MonoBehaviour, IInputActionReceiver
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Header("Input")]
        [SerializeField]
        protected InputActionReference ActionRef;

        [SerializeField, FSMDrawer]
        FSMEventRef m_fsmActiveEvent;
        [SerializeField, FSMDrawer]
        FSMEventRef m_fsmReleaseEvent;

        [SerializeField]
        bool m_memorizeStateAndRepeat = true;
        [SerializeField]
        float m_activateReleaseThreshold = float.Epsilon;

        [SerializeField]
        protected bool CancelPrevious;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly List<int> m_validActiveStates = new List<int>();
        readonly List<int> m_validReleaseStates = new List<int>();

        // ReSharper disable VirtualMemberNeverOverridden.Global, MemberCanBePrivate.Global
        protected virtual bool CanInvokeActive => m_validActiveStates.Contains(m_fsmActiveEvent.FSM.ActiveStateName.GetHashCode()) 
                                                  && !ControlCentre.Blocked;
        protected virtual bool CanInvokeRelease => m_validReleaseStates.Contains(m_fsmReleaseEvent.FSM.ActiveStateName.GetHashCode());

        protected bool m_active;

        protected void InvokeActive() => m_fsmActiveEvent.Invoke();
        protected void InvokeRelease() => m_fsmReleaseEvent.Invoke();
        // ReSharper restore VirtualMemberNeverOverridden.Global, MemberCanBePrivate.Global

        public ControlCentre ControlCentre { get; private set; }

        protected bool Initialized => ControlCentre != null;
        protected virtual void Awake() => Init();
        // ReSharper disable once Unity.RedundantEventFunction
        protected virtual void Start() { }

        protected virtual void Init()
        {
            if (Initialized)
                return;
            InitValidStates();
            ControlCentre = GetComponent<ControlCentre>();
        }

        protected virtual void Update()
        {
            if (!m_memorizeStateAndRepeat || !enabled)
                return;

            if (CancelPrevious && m_active)
                ControlCentre.CancelMemorizedInputs();
            if (m_active && CanInvokeActive)
                m_fsmActiveEvent.Invoke();
            else if (!m_active && CanInvokeRelease)
            {
                //Debug.Log($"{nameof(InputStateToFSMEvent)} Release");

                m_fsmReleaseEvent.Invoke();
            }
        }

        void InitValidStates()
        {
            InitValidStatesList(m_fsmActiveEvent, m_validActiveStates);
            InitValidStatesList(m_fsmReleaseEvent, m_validReleaseStates);
        }

        protected virtual void OnControl(InputAction.CallbackContext cc)
        {
            var inputValue = cc.ReadValue<float>();
            SetControlInput(inputValue);
        }

        [UsedImplicitly]
        public void SetControlInput(float input)
        {
            m_active = input > m_activateReleaseThreshold;
            if (!enabled)
                return;

            if (m_active && CanInvokeActive)
                InvokeActive();
            else if (!m_active && CanInvokeRelease)
            {
                InvokeRelease();
                //Debug.Log($"{nameof(InputStateToFSMEvent)} Release");
            }

        }

        static void InitValidStatesList(FSMEventRef e, List<int> validStates)
        {
            var fsmMove = (e.FSM.FsmTemplate != null) ? e.FSM.FsmTemplate.fsm : e.FSM.Fsm;

            validStates.AddRange(from state in fsmMove.States
                from t in state.Transitions
                where string.Equals(e.EventName, t.EventName, StringComparison.Ordinal)
                select state.Name.GetHashCode());
        }

        public virtual IReadOnlyList<ActionMapping> Mapping => new[] { new ActionMapping() { ActionRef = ActionRef, Call = OnControl } };
    }
}