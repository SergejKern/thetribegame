using System;
using System.Collections.Generic;
using Core.Unity.Types.Fsm;
using GameUtility.Data.Input;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class InputToFSMMemoryTriggerEvent : MonoBehaviour, IInputActionReceiver, IInputTrigger, IMemorizedInput
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Header("Input")]
        [SerializeField]
        protected InputActionReference ActionRef;

        [SerializeField, FSMDrawer]
        FSMEventRef m_fsmEventRef;

        [SerializeField]
        protected bool CancelPrevious;

        [SerializeField] UnityEvent m_noEnergyFeedback;

        //todo 3: review this
        [SerializeField] InputActionReference m_requiredVec2Input;
        [SerializeField] bool m_requiredActive;
        [SerializeField] float m_activeTolerance;
        //---

#pragma warning restore 0649 // wrong warnings for SerializeField

        protected InputAction Action;
        InputAction m_vec2ReqAction;

        protected Vector2 Vec2Value;
        bool m_vec2RequirementFulfilled;

        readonly List<int> m_validStates = new List<int>();

        public virtual string Name => ActionRef.name;
        protected bool IsValidState => m_validStates.Contains(m_fsmEventRef.FSM.ActiveStateName.GetHashCode());
        public virtual bool CanInvoke => IsValidState && !ControlCentre.Blocked && HasEnergy && m_vec2RequirementFulfilled;
        protected virtual bool HasEnergy => true;

        public ControlCentre ControlCentre { get; private set; }

        protected bool Initialized => ControlCentre != null;

        protected virtual void Awake() => Init();
        protected virtual void Start() { }

        protected virtual void Init()
        {
            if (Initialized)
                return;

            InitValidStates();
            ControlCentre = GetComponent<ControlCentre>();
        }

        void InitValidStates()
        {
            if (m_fsmEventRef.FSM == null)
                return;

            var fsm = (m_fsmEventRef.FSM.FsmTemplate != null) ? m_fsmEventRef.FSM.FsmTemplate.fsm : m_fsmEventRef.FSM.Fsm;

            foreach (var state in fsm.States)
            {
                foreach (var t in state.Transitions)
                {
                    if (!string.Equals(m_fsmEventRef.EventName, t.EventName, StringComparison.Ordinal))
                        continue;
                    m_validStates.Add(state.Name.GetHashCode());
                }
            }
        }

        void OnControl(InputAction.CallbackContext cc)
        {
            UpdateVec2Requirement();
            if (!m_vec2RequirementFulfilled)
                return;
            
            Trigger();
        }

        void UpdateVec2Requirement()
        {
            m_vec2RequirementFulfilled = true;
            if (m_vec2ReqAction == null)
                return;
            Vec2Value = m_vec2ReqAction.ReadValue<Vector2>();
            var isActive = (Vec2Value.sqrMagnitude > m_activeTolerance);
            m_vec2RequirementFulfilled = (isActive == m_requiredActive);
        }

        public virtual void Update() => UpdateVec2Requirement();

        public virtual void Trigger()
        {
            if (!enabled || ControlCentre == null)
                return;

            if (CanInvoke)
            {
                ControlCentre.CancelMemorizedInputs();

                Invoke();
                return;
            }
            if (!HasEnergy)
            {
                NoEnergyFeedback();
                return;
            }            
            if (CancelPrevious)
                ControlCentre.CancelMemorizedInputs();
            ControlCentre.MemorizeInput(this);
        }

        protected virtual void NoEnergyFeedback() => m_noEnergyFeedback.Invoke();

        public void AddListenerForNoEnergyFeedback(UnityAction action) => m_noEnergyFeedback.AddListener(action);
        public void RemoveListenerForNoEnergyFeedback(UnityAction action) => m_noEnergyFeedback.RemoveListener(action);

        public void Invoke()
        {
            if (CancelPrevious)
                ControlCentre.CancelMemorizedInputs();
            m_fsmEventRef.Invoke();
        }

        public virtual IReadOnlyList<ActionMapping> Mapping
        {
            get
            {
                var size = m_requiredVec2Input != null ? 2 : 1;

                var mapping = new ActionMapping[size];
                mapping[0] = new ActionMapping()
                {
                    ActionRef = ActionRef,
                    Call = OnControl,
                    LinkAction = (a) => Action = a,
                    UnlinkAction = (a) => Action = null
                };
                if (m_requiredVec2Input == null)
                    return mapping;

                mapping[1] = new ActionMapping()
                {
                    ActionRef = m_requiredVec2Input,
                    LinkAction = (a) => m_vec2ReqAction = a,
                    UnlinkAction = (a) => m_vec2ReqAction = null,
                };
                
                return mapping;
            }
        }
    }
}
