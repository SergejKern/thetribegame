using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class InputVec2ToFSMEvent : InputStateToFSMEvent
    {
        protected override void OnControl(InputAction.CallbackContext cc)
        {
            if (!enabled)
                return;
            var input = cc.ReadValue<Vector2>();
            SetControlInput(input.sqrMagnitude);
        }
    }
}