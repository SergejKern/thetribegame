using System.Collections.Generic;
using Game.Globals;
using GameUtility.Data.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class PauseControl : MonoBehaviour, IInputActionReceiver
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] InputActionReference m_pauseAction;
        [SerializeField] SceneID m_pauseSceneID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ControlCentre m_centre;

        void OnEnable() => TryGetComponent(out m_centre);

        void OnPause(InputAction.CallbackContext cc)
        {
            if (!GameGlobals.Scenes.TryGetValue(m_pauseSceneID, out var pauseScene) 
                || pauseScene == null)
                return;

            Time.timeScale = 0f;
            m_centre.PlayerInput.SwitchCurrentActionMap("Menu");

            if (pauseScene.ActiveOrActivating)
                return;

            pauseScene.SetActive(true);
        }

        public IReadOnlyList<ActionMapping> Mapping
        => new[]
        {
            new ActionMapping()
            {
                ActionRef = m_pauseAction,
                Call = OnPause,
            }
        };
    }

}
