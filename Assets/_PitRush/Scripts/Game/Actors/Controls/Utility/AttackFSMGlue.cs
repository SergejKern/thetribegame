﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Types.Fsm;
using Game.Configs.Combat;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Input;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    public class AttackFSMGlue : MonoBehaviour, IReactToModelInitialized
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, FSMDrawer("AttackSpeedFactor", editVariable: false)]
        FSMFloatRef m_attackSpeedFactor;
        [SerializeField, FSMDrawer("RecoverSpeedFactor", editVariable: false)]
        FSMFloatRef m_recoverSpeedFactor;
        [SerializeField, FSMDrawer(editVariable: false)]
        FSMFloatRef m_cooldown;
        [SerializeField, FSMDrawer("ForwardForce", editVariable: false)]
        FSMFloatRef m_forwardForce;
        [SerializeField, FSMDrawer("ModelAnimationState", editVariable: false)]
        FSMStringRef m_modelAnimationState;
        [SerializeField, FSMDrawer("Ready")]
        FSMStateRef m_readyStateRef;
        [SerializeField, FSMDrawer("Recover")]
        FSMStateRef m_recoverStateRef;
        [SerializeField, FSMDrawer("Cooldown")]
        FSMStateRef m_cooldownStateRef;

        [SerializeField, FSMDrawer]
        FSMEventRef m_onWeaponUse;
        [FormerlySerializedAs("m_onWeaponActivate")] 
        [SerializeField, FSMDrawer]
        FSMEventRef m_onWeaponForce;
        [SerializeField, FSMDrawer]
        FSMEventRef m_onRecover;

        [SerializeField, FSMDrawer("AttackDone", editVariable: false)]
        FSMBoolRef m_attackDone;

        [SerializeField, FSMDrawer("ModelAnimator", editVariable: false)]
        FSMGameObjectRef m_modelAnimator;

        [SerializeField] ControlCentre m_controlCentre;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable once UnusedMember.Global
        [Preserve] // playmaker
        public bool AttackDone
        {
            get
            {
                m_attackDone.Sync();
                return m_attackDone.Value;
            }
        }

        public bool IsReadyState =>
            m_readyStateRef.FSM != null
            && string.Equals(m_readyStateRef.StateName, m_readyStateRef.FSM.ActiveStateName, StringComparison.Ordinal);
        public bool IsCooldownState =>
            m_cooldownStateRef.FSM != null
            && string.Equals(m_cooldownStateRef.StateName, m_cooldownStateRef.FSM.ActiveStateName,
                StringComparison.Ordinal);

        public bool IsFollowUpState => m_recoverStateRef.FSM != null && string.Equals(m_recoverStateRef.StateName,
                                           m_recoverStateRef.FSM.ActiveStateName, StringComparison.Ordinal);

        EventMemory m_weaponUse = EventMemory.Default;
        EventMemory m_weaponForce = EventMemory.Default;
        EventMemory m_recover = EventMemory.Default;

        float m_attackSpeed;
        float m_recoverSpeed;

        public void OnModelInitialized()
        {
            if (m_controlCentre == null)
            {
                Debug.LogError("m_controlCentre == null");
                return;
            }

            if (m_controlCentre.Animator == null)
            {
                Debug.LogError("Animator == null");
                return;
            }
            m_modelAnimator.Value = m_controlCentre.Animator.gameObject;
        }

        void Start() => InitValidStates();

        public void OnWeaponUse() => InvokeEvent(ref m_weaponUse);

        public void OnWeaponForce() => InvokeEvent(ref m_weaponForce);

        public void OnRecover() => InvokeEvent(ref m_recover);

        public void Forget()
        {
            m_weaponUse.ShouldInvoke = false;
            m_weaponForce.ShouldInvoke = false;
            m_recover.ShouldInvoke = false;
            m_readyStateRef.FSM.SetState(m_readyStateRef.StateName);
            m_attackDone.Value = true;
        }

        void Update()
        {
            UpdateEvent(ref m_weaponUse);
            UpdateEvent(ref m_weaponForce);
            UpdateEvent(ref m_recover);
        }

        public void SetSpeedMultiplier(float speedMultiplier)
        {
            m_attackSpeedFactor.Value = m_attackSpeed * speedMultiplier;
            m_recoverSpeedFactor.Value = m_recoverSpeed * speedMultiplier;
        }

        public void ApplyCurrentMove(WeaponConfig.WeaponComboMove currentMove, float speedMultiplier =1f)
        {
            m_attackSpeed = currentMove.AttackSpeedFactor;
            m_recoverSpeed = currentMove.RecoverSpeedFactor;

            m_attackSpeedFactor.Value = m_attackSpeed * speedMultiplier;
            m_recoverSpeedFactor.Value = m_recoverSpeed * speedMultiplier;
            m_cooldown.Value = currentMove.Cooldown;
            m_forwardForce.Value = currentMove.MotionFactor;
            m_modelAnimationState.Value = currentMove.ModelAnimationState.StateName;
        }

        void InitValidStates()
        {
            m_weaponUse.Event = m_onWeaponUse;
            m_weaponForce.Event = m_onWeaponForce;
            m_recover.Event = m_onRecover;

            InitValidStatesList(m_weaponUse.Event, m_weaponUse.ValidStates);
            InitValidStatesList(m_weaponForce.Event, m_weaponForce.ValidStates);
            InitValidStatesList(m_recover.Event, m_recover.ValidStates);
        }
        static void InvokeEvent(ref EventMemory m)
        {
            if (!m.CanInvoke)
            {
                m.ShouldInvoke = true;
                return;
            }
            m.ShouldInvoke = false;
            m.Invoke();
        }

        static void UpdateEvent(ref EventMemory m)
        {
            if (m.ShouldInvoke)
                InvokeEvent(ref m);
        }

        static void InitValidStatesList(FSMEventRef e, List<int> validStates)
        {
            var fsmMove = (e.FSM.FsmTemplate != null) ? e.FSM.FsmTemplate.fsm : e.FSM.Fsm;

            validStates.AddRange(from state in fsmMove.States
                from t in state.Transitions
                where string.Equals(e.EventName, t.EventName, StringComparison.Ordinal)
                select state.Name.GetHashCode());
        }
    }
}
