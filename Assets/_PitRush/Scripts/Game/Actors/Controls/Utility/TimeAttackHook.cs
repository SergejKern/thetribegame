using System.Collections.Generic;
using Game.Combat.Timeline;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;
using CombatTimeline = Game.Combat.Timeline.CombatTimeline;

namespace Game.Actors.Controls
{
    public class TimeAttackHook : MonoBehaviour, IAttackHook
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] CombatTimeline m_timeline;
        [SerializeField] AttackControl m_attackControl;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [Preserve, UsedImplicitly]
        public void CancelAttack()
        {
            if (m_timeline == null || m_timeline.OwningAgent.Target == null)
            {
                m_attackControl.OnAttackCanceled();
                return;
            }
            m_timeline.SetToDone(m_timeline.Time, true);
        }
        [Preserve, UsedImplicitly]
        public void CancelAttackAndCharge()
        {
            if (m_timeline == null || m_timeline.OwningAgent.Target == null)
            {
                m_attackControl.OnAttackAndChargeCanceled();
                return;
            }
            m_timeline.SetToDone(m_timeline.Time, true);
        }

        public AttackHookHandleResult HandleAttack(AttackTypeID attackType, int move)
        {
            if (m_timeline == null) return AttackHookHandleResult.Unhandled;
            var agent = m_timeline.OwningAgent;
            if (agent.Target == null)
            {
                //if(agent is CombatTimeline ctl)
                //    ctl.OnRecentTargetMiss();
                return AttackHookHandleResult.Unhandled;
            }
            if (agent.Data.IsMovedViaTimeline)
                return AttackHookHandleResult.Unhandled;
            agent.InitAgentForTimeline(attackType, move, m_timeline);
            return AttackHookHandleResult.Handled;
        }

        public void AttackDone(IEnumerable<ICollisionMaterialComponent> hitComponents) => 
            m_timeline.CheckIfMissed(hitComponents);
    }
}