
// todo 3: decouple input and actions more, so AttackControl does not need this hook

using System;
using System.Collections.Generic;
using Core.Unity.Types;
using GameUtility.Data.PhysicalConfrontation;

namespace Game.Actors.Controls
{
    public enum AttackHookHandleResult
    {
        Unhandled,
        Handled
    }
    public interface IAttackHook
    {
        AttackHookHandleResult HandleAttack(AttackTypeID attackType, int move);
        void AttackDone(IEnumerable<ICollisionMaterialComponent> hitComponents);
    }

    [Serializable]
    public class RefIAttackHook: InterfaceContainer<IAttackHook> { }
}