using System;
using System.Collections.Generic;
using Core.Unity.Attribute;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Configs;
using Game.Configs.Combat;
using Game.InteractiveObjects;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using GameUtility.InitSystem;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class GrabControl : InteractionControl, IInitDataComponent<GrabControl.ConfigData>, IUpdateAttachmentPoints
    {
        [Serializable]
        public struct ConfigData
        {
            public bool GrabEnabled;
            public bool DropEnabled;
            public bool SwitchEnabled;

            public PrefabData InitialItemRight;
            public PrefabData InitialItemLeft;

            public static ConfigData Default = new ConfigData()
            {
                GrabEnabled = true,
                DropEnabled = true,
                SwitchEnabled = true
            };
        }

        enum Hand
        {
            Left,
            Right,
        }

        const int k_hands = 2;

        public struct GrabSlot
        {
            public TransformData HandTransform;
            public IGrabbable Item;
            public bool Free => Item == null;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [Header("Input")]
        [SerializeField] InputActionReference m_dropActionRef;

        [SerializeField] InputActionReference m_switchItemActionRef;

        [Header("Grabbing")]
        [SerializeField] GrabEvent m_onEquipChanged;

        [SerializeField] Hand m_primaryHand = Hand.Right;

        [SerializeField] AttachmentID m_leftHandID;
        [SerializeField] AttachmentID m_rightHandID;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        Hand m_secondaryHand;// => (Hand)(((int)m_primaryHand + 1) % m_hands);

        readonly GrabSlot[] m_grabSlots = new GrabSlot[k_hands];
        internal ref GrabSlot PrimarySlot => ref m_grabSlots[(int) m_primaryHand];
        internal ref GrabSlot SecondarySlot => ref m_grabSlots[(int) m_secondaryHand];

        bool HasFreeHand => (SecondarySlot.Free || PrimarySlot.Free);
        bool EmptyHands => (SecondarySlot.Free && PrimarySlot.Free);

        bool CanDrop
        {
            get
            {
                if (!enabled)
                    return false;
                return !EmptyHands;
            }
        }

        protected override void Init()
        {
            if (Initialized)
                return;

            base.Init();

            m_secondaryHand = (Hand) (((int)m_primaryHand + 1) % k_hands);
        }

        public void UpdateAttachmentPoints(AttachmentPointsProvider prov)
        {
            if (!Initialized)
                Init();

            InitSlots(prov);
        }

        internal void InitSlots(AttachmentPointsProvider app)
        {
            m_grabSlots[(int)Hand.Left] = new GrabSlot() { HandTransform = app.GetAttachment(m_leftHandID) };
            m_grabSlots[(int)Hand.Right] = new GrabSlot() { HandTransform = app.GetAttachment(m_rightHandID) };
        }

        /// <summary>
        /// returns slot & item preferring secondary
        /// </summary>
        ref GrabSlot GetEquippedItem()
        {
            if (SecondarySlot.Free)
                return ref PrimarySlot;
            return ref SecondarySlot;
        }

        //public override void OnInteract(InputAction.CallbackContext _) => OnInteract();
        [UsedImplicitly]
        public override void OnInteract()
        {
            if (!InteractPossible)
                return;
            var interactive = CurrentInteractive.Interactive;
            var provider = interactive is IGrabProvider iProv ? iProv:null;
            var grabbable = interactive is IGrabbable iGrab ? iGrab : null;

            if ((provider == null) && (grabbable == null))
            {
                base.OnInteract();
                return;
            }

            Grab(provider, grabbable);
        }

        // ReSharper disable once FlagArgument
        public void Grab(IGrabProvider provider, IGrabbable grabbable)
        {
            if (!gameObject.activeInHierarchy)
                return;
            if (!enabled || !m_data.GrabEnabled)
                return;
            if (!HasFreeHand)
                return;

            var itemProperties = provider != null ? (IItemProperties) provider : grabbable;
            if (!EmptyHands && !CanEquip(itemProperties.ItemAnimationType))
                return;

            // todo 2: switch current item if equipping is otherwise not possible
            // first I have to think about the needs of different games, what is best solution.
            // Currently ugly but not important for PitRush
            ref var grabSlot = ref GetSlotForGrabbable(itemProperties, out var ok);
            if (!ok)
                return;

            provider?.Get(out grabbable);

            var handTr = grabSlot.HandTransform;

            grabbable.AdjustPositioning(handTr);
            var grabbableComponent = (grabbable as MonoBehaviour);
            if (grabbableComponent != null)
                grabbableComponent.transform.SetParent(handTr.Parent, true);
            grabSlot.Item = grabbable;
            grabbable.OnGrabbed(ControlCentre);

            if (itemProperties.GrabRestrictions == GrabRestrictions.TwoHanded)
                SecondarySlot.Item = grabbable;

            m_onEquipChanged?.Invoke(PrimarySlot.Item, SecondarySlot.Item);
            ControlCentre.SetRumble(0.2f, 0.1f);
        }

        /// <summary>
        /// Prevent equipping weapon combinations that don't have an animation set eg. one-handed sword+axe
        /// </summary>
        bool CanEquip(WeaponMovesType animationType)
        {
            ref var slot = ref GetEquippedItem();
            return slot.Item == null || 
                   ConfigLinks.AnimationCombinationConfig.FindCombination(slot.Item.ItemAnimationType, 
                       animationType, 
                       out _);
        }

        void OnSwitchItems(InputAction.CallbackContext obj) => OnSwitchItems();

        [UsedImplicitly]
        public void OnSwitchItems()
        {
            if (!m_data.SwitchEnabled)
                return;
            //Debug.Log("OnSwitchItems");

            var bItem = SecondarySlot.Item;
            var aItem = PrimarySlot.Item;

            if (bItem != null && bItem.GrabRestrictions != GrabRestrictions.None)
                return;
            if (aItem != null && aItem.GrabRestrictions != GrabRestrictions.None)
                return;

            bItem?.AdjustPositioning(PrimarySlot.HandTransform);
            aItem?.AdjustPositioning(SecondarySlot.HandTransform);

            PrimarySlot.Item = bItem;
            SecondarySlot.Item = aItem;

            bItem?.OnGrabbed(ControlCentre);
            aItem?.OnGrabbed(ControlCentre);
        }
        void OnDrop(InputAction.CallbackContext cc) => OnDrop();
        [UsedImplicitly]
        public void OnDrop()
        {
            if (!m_data.DropEnabled)
                return;
            
            //Debug.Log("OnDrop");
            Release(out var item);
            item?.Drop();
        }

        internal void Release(out IGrabbable item)
        {
            item = null;
            if (!CanDrop)
                return;
            ref var slot = ref GetEquippedItem();
            if (slot.Item == null)
                return;

            item = slot.Item;
            OnItemRelease(item, ref slot);
        }

        ref GrabSlot GetSlotForGrabbable(IItemProperties g, out bool ok)
        {
            ok = true;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (g.GrabRestrictions)
            {
                case GrabRestrictions.TwoHanded when !EmptyHands:
                case GrabRestrictions.PrimaryOnly when !PrimarySlot.Free:
                case GrabRestrictions.SecondaryOnly when !SecondarySlot.Free:
                    ok = false;
                    return ref PrimarySlot;
                case GrabRestrictions.SecondaryOnly:
                    return ref SecondarySlot;
                default:
                    return ref GetFreeSlot();
            }
        }

        ref GrabSlot GetFreeSlot()
        {
            if (PrimarySlot.Free)
                return ref PrimarySlot;
            if (SecondarySlot.Free)
                return ref SecondarySlot;
            throw new InvalidOperationException("There is no FreeSlot!");
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        void OnItemRelease(IGrabbable item, ref GrabSlot slot)
        {
            var itemComponent = item as Component;
            if (itemComponent == null)
                Debug.LogError($"{item} is no Component!");
            else itemComponent.transform.SetParent(null, true);

            slot.Item = null;
            if (item.GrabRestrictions == GrabRestrictions.TwoHanded)
            {
                if (SecondarySlot.Item != item)
                    Debug.LogError("TwoHanded was wielded one handed!");
                SecondarySlot.Item = null;
            }

            if (SecondarySlot.Item != null)
                OnSwitchItems();

            m_onEquipChanged?.Invoke(PrimarySlot.Item, SecondarySlot.Item);
        }

        public override IReadOnlyList<ActionMapping> Mapping => new[]
        {
            new ActionMapping() { ActionRef = m_dropActionRef, Call = OnDrop },
            new ActionMapping() { ActionRef = m_switchItemActionRef, Call = OnSwitchItems },
            new ActionMapping() { ActionRef = ActionRef, Call = OnControl }
        };

        void InitSlotWithItem(Hand hand, GameObject prefab)
        {
            ref var slot = ref m_grabSlots[(int)hand];
            if (!slot.Free)
                return;
            
            if (!prefab.GetPooledInstance().TryGetComponent<IGrabbable>(out var grabbable))
                return;
            //ref var slot = ref m_grabSlots[(int) hand];
            grabbable.AdjustPositioning(slot.HandTransform);
            var grabbableComponent = grabbable as MonoBehaviour;
            if (grabbableComponent != null)
                grabbableComponent.transform.SetParent(slot.HandTransform.Parent, true);
            slot.Item = grabbable;
            grabbable.OnGrabbed(ControlCentre);
        }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            if (m_data.InitialItemLeft.Prefab != null)
                InitSlotWithItem(Hand.Left, m_data.InitialItemLeft.Prefab);
            if (m_data.InitialItemRight.Prefab != null)
                InitSlotWithItem(Hand.Right, m_data.InitialItemRight.Prefab);

            m_onEquipChanged?.Invoke(PrimarySlot.Item, SecondarySlot.Item);
        }

        public void SetGrabEnabled(bool enable) => m_data.GrabEnabled = enable;

        // ReSharper disable once UnusedMember.Global
        public void GetCurrentEquipment(out IGrabbable right, out IGrabbable left)
        {
            right = PrimarySlot.Item;
            left = SecondarySlot.Item;
        }

        public void DespawnEquipment()
        {
            var bItem = SecondarySlot.Item;
            var aItem = PrimarySlot.Item;

            if (aItem is MonoBehaviour aComponent)
                aComponent.gameObject.TryDespawnOrDestroy();

            if (bItem is MonoBehaviour bComponent)
                bComponent.gameObject.TryDespawnOrDestroy();

            PrimarySlot.Item = null;
            SecondarySlot.Item = null;
        }
    }

    [Serializable]
    public class GrabEvent : UnityEvent<IGrabbable, IGrabbable> { }

#if UNITY_EDITOR
    [CustomEditor(typeof(GrabControl))]
    public class GrabControlEditor : Editor
    {
        // ReSharper disable once InconsistentNaming
        new GrabControl target => base.target as GrabControl;
        AttachmentPointsComponent m_apc;
        AttachmentPointsProvider m_app;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DefaultEquipGUI();
        }

        void DefaultEquipGUI()
        {
            m_apc = (AttachmentPointsComponent) EditorGUILayout.ObjectField("Model-Attachments: ", m_apc, typeof(AttachmentPointsComponent),true);
            if (m_apc == null || !GUILayout.Button("Default Equip"))
                return;
            var prov = target.ControlCentre.Attachments;
            prov.AddAttachmentPoints(m_apc);
            target.InitSlots(prov);
            target.GetData(out var data);
            EditorEquip(data.InitialItemRight.Prefab, target.PrimarySlot.HandTransform);
            EditorEquip(data.InitialItemLeft.Prefab, target.SecondarySlot.HandTransform);
        }

        static void EditorEquip(GameObject prefab, TransformData handTr)
        {
            if (prefab == null)
                return;
            var obj = Instantiate(prefab);
            obj.name = $"_{obj.name}_Unsaved";
            obj.hideFlags = HideFlags.DontSave;
            if (!obj.TryGetComponent<IGrabbable>(out var grabbable))
                return;
            //ref var slot = ref m_grabSlots[(int) hand];
            grabbable.AdjustPositioning(handTr);

            var grabbableComponent = grabbable as MonoBehaviour;
            if (grabbableComponent != null)
                grabbableComponent.transform.SetParent(handTr.Parent, true);
        }
    }
#endif
}