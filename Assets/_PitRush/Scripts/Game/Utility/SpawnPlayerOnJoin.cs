﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using Game.Configs;
using Game.Globals;
using GameUtility.Data.Visual;
using GameUtility.PooledEventSpawning;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Game.Utility
{
    public class SpawnPlayerOnJoin : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] bool m_respawn;

        [SerializeField] bool m_ignoreSchemes;
        //, ShowIf("m_ignoreSchemes")
        [SerializeField] string[] m_ignoredSchemes;

        [SerializeField] string m_reloadScene;
        [SerializeField] KeyCode m_reloadKey;

        [FormerlySerializedAs("m_event")]
        [SerializeField] GameObjectEvent m_joinEvent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly List<PlayerInput> m_inputs = new List<PlayerInput>();

        bool PlayerJoinValid
        {
            get
            {
                if (GameGlobals.CurrentSettings == null)
                {
                    Debug.LogError("Cannot join, because no SceneSettings have been found!");
                    return false;
                }
                if (GameGlobals.GOPlayerInputManager == null)
                {
                    Debug.LogError("Cannot join, because no InputManager has been found!");
                    return false;
                }

                return enabled;
            }
        }

        public void LinkPlayerInputManager()
        {
            GameGlobals.PlayerInputManager.playerJoinedEvent.AddListener(PlayerJoined);
            //GameGlobals.PlayerInputManager.playerLeftEvent.AddListener(PlayerLeft);
            InputSystem.onDeviceChange += OnDeviceChange;
            //GameGlobals.PlayerInputManager..AddListener(PlayerLeft);
            enabled = true;

            var inputs = FindObjectsOfType<PlayerInput>();
            foreach (var input in inputs)
                PlayerJoined(input);
        }

        // todo 5: Make sure unlink is used properly
        [UsedImplicitly]
        public void Unlink()
        {
            GameGlobals.PlayerInputManager.playerJoinedEvent.RemoveListener(PlayerJoined);
            InputSystem.onDeviceChange -= OnDeviceChange;
            enabled = false;
        }

        bool ShouldIgnore(PlayerInput playerInput) =>
            m_ignoreSchemes && Array.Find(m_ignoredSchemes, s => s.Equals(playerInput.currentControlScheme)) != null;

        bool CheckExisting(PlayerInput playerInput, out int idx)
        {
            idx = m_inputs.IndexOf(playerInput);
            if (idx == -1)
                return false;
            // if player is still alive and well, should not happen
            return GameGlobals.Players[idx] != null && GameGlobals.Players[idx].gameObject.activeInHierarchy;
        }

        // triggered by PlayerInputManager notification
        void PlayerJoined(PlayerInput playerInput)
        {
            if (ShouldIgnore(playerInput))
                return;

            if (!PlayerJoinValid)
                return;
            if (CheckExisting(playerInput, out var idx))
                return;

            StartCoroutine(JoinPlayerRoutine(playerInput, idx));
        }

        IEnumerator JoinPlayerRoutine(PlayerInput playerInput, int idx)
        {
            // postpone one frame to avoid immediate action of player
            yield return null;
            if (idx == -1)
            {
                idx = m_inputs.IndexOf(null);
                if (idx == -1)
                    idx = m_inputs.Count;
            }

            // max player count
            if (idx >= 4)
                yield break;

            SpawnPlayer(playerInput, idx);
        }

        void SpawnPlayer(PlayerInput playerInput, int idx)
        {
            var character = ConfigLinks.Characters.Characters[idx];
            //var spawnPos = GameGlobals.CurrentSettings.SpawnPos;

            var spawned = character.Prefab.GetPooledInstance(GameGlobals.SafePosition, Quaternion.identity);
            spawned.name = $"Player {idx}";
            //var spawned = character.Prefab.PooledSpawn(m_spawnPos.position, m_spawnPos.rotation);
            MaterialExchange.Override(new MaterialExchangeData2()
                {ForObject = spawned, MaterialMap = character.ModelOverrideMaterialMap});

            if (!spawned.TryGetComponent<Player>(out var player))
                return;
            player.InitCharacterIdx(idx);
            player.InitializeWithController(playerInput);

            AssignPlayer(playerInput, idx, player);
            PrefabSpawnedEvent.Trigger(spawned, character.Prefab);
            GameGlobals.GameCamera.AddTarget(spawned.transform);

            m_joinEvent.Invoke(spawned);
        }

        void AssignPlayer(PlayerInput playerInput, int idx, Player controllable)
        {
            if (idx < m_inputs.Count)
            {
                m_inputs[idx] = playerInput;
                GameGlobals.Players[idx] = controllable;
            }
            else
            {
                m_inputs.Add(playerInput);
                GameGlobals.Players.Add(controllable);
            }
            GameGlobals.UpdatePlayerCount();
        }

        void Update()
        {
            Respawn();

            if (!Input.GetKeyDown(m_reloadKey))
                return;
            StartCoroutine(Reload());
        }

        IEnumerator Reload()
        {
            for (var idx = 0; idx < m_inputs.Count; idx++)
            {
                var input = m_inputs[idx];
                var player = GameGlobals.Players[idx];

                if (input != null)
                    input.gameObject.DestroyEx();
                if (player != null)
                    player.Disconnect();

                m_inputs[idx] = null;
                GameGlobals.Players[idx] = null;
            }

            yield return null;
            yield return null;

            SceneManager.LoadScene(m_reloadScene, LoadSceneMode.Single);
        }

        void Respawn()
        {
            if (!m_respawn)
                return;

            // Usually m_inputs.Count == PlayerCount, but when winning/ loosing players are cleared, but this script is still active.
            // todo 4: make it more intuitive/ readable (@see comment above)
            var count = Mathf.Min(m_inputs.Count, GameGlobals.Players.Count);
            for (var i = 0; i < count; i++)
            {
                if (GameGlobals.Players[i] == null)
                    continue;
                if (GameGlobals.Players[i].Health.HP > 0f)
                    continue;
                if (GameGlobals.Players[i].gameObject != null && GameGlobals.Players[i].gameObject.activeSelf)
                    continue;
                //m_spawnedPlayers[i].gameObject.SetActive(false);
                PlayerJoined(m_inputs[i]);
                return;
            }
        }

        // ReSharper disable once FlagArgument
        void OnDeviceChange(InputDevice device, InputDeviceChange change)
        {
            if (!enabled)
                return;

            if (change != InputDeviceChange.Disconnected) return;
            var idx = m_inputs.FindIndex(a => a.devices.Contains(device) || a.devices.IsNullOrEmpty());
            if (idx == -1)
                return;

            var input = m_inputs[idx];
            var player = GameGlobals.Players[idx];

            if (input != null)
                input.gameObject.DestroyEx();
            if (player != null)
                player.Disconnect();

            player.gameObject.TryDespawnOrDestroy();
            m_inputs[idx] = null;
            GameGlobals.Players[idx] = null;
            GameGlobals.UpdatePlayerCount();
            //InputSystem.FlushDisconnectedDevices();
        }
    }
}
