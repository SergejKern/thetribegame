﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors;
using Game.Actors.Energy;
using Game.Globals;
using GameUtility.Components.Transform;
using GameUtility.Data.TransformAttachment;
using UnityEngine;

namespace Game.Utility.VisualAndFX
{
    public class SpotLightDirector : MonoBehaviour
    {
        [Serializable]
        public struct SpotIds
        {
            public AttachmentID Origin;
            public AttachmentID Idle;
        }

        struct Data
        {
            public GameObject LineGo;
            public LineRenderer LineRenderer;
            public LerpPositionBehaviour LerpPos;
            public GameObject SpotGo;
            public Transform SpotPos;
            public AttachmentPoint Attachment;
            public AttachmentPoint Idle;
        }

#pragma warning disable 0649
        [SerializeField] SpotIds[] m_attachmentIds;

        [SerializeField] GameObject m_spotLinePrefab;
        [SerializeField] GameObject m_spotPrefab;
#pragma warning restore 0649

        Data[] m_data;

        readonly List<Transform> m_targets = new List<Transform>();
        bool m_initialized;

        public void Init(AttachmentPointsComponent apc)
        {
            if (m_initialized)
                return;
            if (apc == null)
                return;

            m_data = new Data[m_attachmentIds.Length];

            var tr = transform;

            using (var apListScoped = SimplePool<List<AttachmentPoint>>.I.GetScoped())
            {
                var obj = apListScoped.Obj;
                apc.Extract(obj);

                for (var i = 0; i < m_attachmentIds.Length; i++)
                {
                    var originID = m_attachmentIds[i].Origin;
                    var idleID = m_attachmentIds[i].Idle;

                    var origIdx = obj.FindIndex(a => a.ID == originID);
                    var idleIdx = obj.FindIndex(a => a.ID == idleID);

                    if (origIdx == -1 || idleIdx == -1)
                        continue;

                    var lrGo = m_spotLinePrefab.GetPooledInstance(tr.position, Quaternion.identity);
                    lrGo.transform.SetParent(tr, true);

                    var spotGo = m_spotPrefab.GetPooledInstance(Vector3.zero, Quaternion.identity);
                    spotGo.transform.SetParent(tr, true);

                    if (lrGo.TryGetComponent(out LineRenderer lr))
                        m_data[i].LineRenderer = lr;
                    if (spotGo.TryGetComponent(out LerpPositionBehaviour lpb))
                    {
                        lpb.Follow = obj[idleIdx].TransformData;
                        m_data[i].LerpPos = lpb;
                    }

                    m_data[i].LineGo = lrGo;
                    m_data[i].SpotGo = spotGo;

                    m_data[i].SpotPos = spotGo.transform;
                    m_data[i].Attachment = obj[origIdx];
                    m_data[i].Idle = obj[idleIdx];
                }
            }

            m_initialized = true;

            SetLightsActive(false);
        }

        void Update()
        {
            if (!m_initialized)
                return;

            for (var i = 0; i < m_data.Length; i++)
            {
                m_data[i].LineRenderer.SetPosition(0, m_data[i].Attachment.TransformData.Position);
                m_data[i].LineRenderer.SetPosition(1, m_data[i].SpotPos.position);
            }
        }


        void ClearTargets()
        {
            m_targets.Clear();
            for (var i = 0; i < m_data.Length; i++) 
                m_data[i].LerpPos.Follow = m_data[i].Idle.TransformData;
        }

        void UpdateTargets()
        {
            if (m_targets.IsNullOrEmpty())
                return;
            var f = m_targets.Count / (float) m_data.Length;
            
            for (var i = 0; i < m_data.Length; i++)
            {
                var targetIdx = Mathf.Clamp(Mathf.FloorToInt(f * i), 0, m_targets.Count-1);
                m_data[i].LerpPos.Follow = new TransformData(){ Parent = m_targets[targetIdx], LocalPosition = 0.1f * Vector3.up};
            }
        }

        public void Connect(Player p)
        {
            p.Get(out RushComponent rc);
            rc.AddRushModeListener(RushModeChanged);
        }

        public void Disconnect(Player p)
        {
            p.Get(out RushComponent rc);
            rc.RemoveRushModeListener(RushModeChanged);
            RushModeChanged();
        }

        void RushModeChanged()
        {
            var wasOn = !m_targets.IsNullOrEmpty();
            ClearTargets();
            foreach (var p in GameGlobals.Players)
            {
                p.Get(out RushComponent rc);
                if (rc.RushMode)
                    m_targets.Add(p.transform);
            }
            var isOn = !m_targets.IsNullOrEmpty();

            if (wasOn != isOn)
                SetLightsActive(isOn);

            UpdateTargets();
        }

        void SetLightsActive(bool isOn)
        {
            for (var i = 0; i < m_data.Length; i++)
            {
                m_data[i].SpotGo.SetActiveWithTransition(isOn);
                m_data[i].LineGo.SetActiveWithTransition(isOn);
            }
        }

        //void AddTarget(Transform target)
        //{
        //    if (m_targets.Contains(target))
        //        return;
        //    m_targets.Add(target);
        //    UpdateTargets();
        //}
        //void RemoveTarget(Transform target)
        //{
        //    m_targets.Remove(target);
        //    UpdateTargets();
        //}

    }
}
