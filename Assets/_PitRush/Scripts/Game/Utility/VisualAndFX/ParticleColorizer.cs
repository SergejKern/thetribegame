﻿using Game.Actors;
using Game.Configs;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Utility.VisualAndFX
{
    public class ParticleColorizer : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ParticleSystemRenderer m_renderer;
        [SerializeField] string m_colorProperty;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [UsedImplicitly]
        public void Colorize(Player p)
        {
            if (!TryGetCharacterColor(p, out var color))
                return;

            if (m_renderer == null)
                return;
            m_renderer.material.SetColor(m_colorProperty, color);
        }

        static bool TryGetCharacterColor(Player p, out Color color)
        {
            color = default; 
            
            var c = ConfigLinks.Characters;
            if (c == null)
                return false;
            if (p == null)
                return false;

            var charIdx = p.CharacterIdx;
            color = c.GetColor(charIdx);
            return true;
        }
    }
}
