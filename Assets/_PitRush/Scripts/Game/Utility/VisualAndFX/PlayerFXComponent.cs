﻿using System;
using Core.Extensions;
using Game.Actors;
using Game.Configs;
using GameUtility.Components.Visual;
using GameUtility.Data.FX;
using UnityEngine;

namespace Game.Utility.VisualAndFX
{
    public class PlayerFXComponent : FXComponent
    {
#pragma warning disable 0649
        [SerializeField] FXEvent m_onConnected;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ref FXLink GetLink(int i) => ref m_links[i];
        public void Connect(Player player)
        {
            if (!TryGetCharacterColor(player, out var prefabConfigs))
                return;

            for (var i = 0; i < m_links.Length; i++)
            {
                ref var link = ref GetLink(i);
                var id = link.ID;

                var prefabConfig = Array.Find(prefabConfigs, a => a.ID == id);
                if (prefabConfig.Prefab == null)
                    continue;

                link.Prefab = prefabConfig.Prefab;
                if (link.InitialActive)
                    GetInstance(id);
            }

            m_onConnected.Invoke(this);
        }

        static bool TryGetCharacterColor(Player p, out PlayerCharacterConfig.PlayerFXObjects[] fxObjects)
        {
            fxObjects = null;

            var c = ConfigLinks.Characters;
            if (c == null)
                return false;
            if (p == null)
                return false;

            var charIdx = p.CharacterIdx;
            if (!charIdx.IsInRange(c.Characters))
                return false;
            var charConfig = c.Characters[charIdx];
            fxObjects = charConfig.FXObjects;
            return true;
        }
    }
}
