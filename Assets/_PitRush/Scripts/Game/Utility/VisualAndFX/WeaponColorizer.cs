using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Interface;
using Game.Actors;
using Game.Combat.Weapon;
using Game.Configs;
using GameUtility.Data.Visual;
using UnityEngine;

using Trail = GameUtility.MeleeWeaponTrail.MeleeWeaponTrail;

namespace Game.Utility.VisualAndFX
{
    public class WeaponColorizer : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        Weapon m_weapon;

        [SerializeField]
        List<Trail> m_colorizeTrails = new List<Trail>();

        PlayerCharacterConfig.MeleeWeaponTrailData[] m_chargeLevels;
#pragma warning restore 0649 // wrong warnings for SerializeField

        MaterialOverrideMap[] m_itemOverrideMap;

        public void SetChargeLevel(int level)
        {
            if (!level.IsInRange(m_chargeLevels))
                return;

            var data = m_chargeLevels[level];
            ColorizeTrail(data);
        }

        void InitChargeLevelData(PlayerCharacterConfig.MeleeWeaponTrailData[] levels)
            => m_chargeLevels = levels;

        public void OnGrabbed()
        {
            if (!TryGetCharacterColor(out m_itemOverrideMap, out var weaponTrail)) 
                return;
            InitChargeLevelData(weaponTrail);

            MaterialExchange.Override(gameObject, m_itemOverrideMap);

            if (weaponTrail != null && weaponTrail.Length >= 1)
                ColorizeTrail(weaponTrail[0]);
        }

        public void OnDisable() => 
            MaterialExchange.Revert(gameObject, m_itemOverrideMap);

        void ColorizeTrail(PlayerCharacterConfig.MeleeWeaponTrailData data)
        {
            if (m_colorizeTrails == null)
                return;
            foreach (var colTrail in m_colorizeTrails)
            {
                if (colTrail == null)
                    continue;
                colTrail.SetMaterial(data.Material);
                if (!data.Colors.IsNullOrEmpty())
                    colTrail.SetColors(data.Colors);
            }
        }

        bool TryGetCharacterColor(out MaterialOverrideMap[] map, out PlayerCharacterConfig.MeleeWeaponTrailData[] data)
        {
            map = null;
            data = default;

            if (!GetPlayerAndWeaponPrefab(out var player, out _)) 
                return false;

            if (!GetCharacterConfig(player, out var charConf)) 
                return false;

            map = charConf.ItemGlowOverrideMap;
            data = charConf.WeaponTrailData;
            return true;
        }

        static bool GetCharacterConfig(Player player, out PlayerCharacterConfig.PlayerData charConf)
        {
            charConf = default;
            var characterConfig = ConfigLinks.Characters;
            if (characterConfig == null)
                return false;

            var charIdx = player.CharacterIdx;
            if (!charIdx.IsInRange(characterConfig.Characters))
                return false;

            charConf = characterConfig.Characters[charIdx];
            return true;
        }

        bool GetPlayerAndWeaponPrefab(out Player player, out GameObject prefab)
        {
            player = null;
            prefab = null;
            if (m_weapon == null)
                return false;

            var actor = m_weapon.Actor;
            if (actor == null)
                return false;

            if (!m_weapon.TryGetComponent<IPrefabInstanceMarker>(out var prefabEntity))
                return false;

            prefab = prefabEntity.Prefab;
            return prefab != null && actor.TryGetComponent(out player);
        }
    }
}