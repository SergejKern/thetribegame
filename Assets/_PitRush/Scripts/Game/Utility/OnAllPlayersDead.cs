﻿using System.Collections;
using Core.Extensions;
using Game.ActionConfig;
using Game.Configs.Elevator;
using Game.Globals;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Utility
{
    public class OnAllPlayersDead : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onAllPlayersDead;
        [SerializeField] LeviDialogueSequence m_tutorialReviveDialogue;
#pragma warning restore 0649 // wrong warnings for SerializeField

        DialogueAction m_tutorialDialogueAction;
        void Start() 
            => m_tutorialDialogueAction = new DialogueAction(m_tutorialReviveDialogue);

        void Update()
        {
            if (GameGlobals.Players.IsNullOrEmpty())
                return;
            // Don't Game Over when disconnected (testing mode)
            if (GameGlobals.PlayerCount <= 0)
                return;

            // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
            foreach (var p in GameGlobals.Players)
            {            
                if (p == null)
                    continue;

                if (p.Health.HP > 0f)
                    return;
            }

            if (GameGlobals.IsTutorial)
            {
                foreach (var p in GameGlobals.Players)
                    p.OnRevive();

                StartCoroutine(ReviveDialogueAction());
                return;
            }

            //_todo: 
            //GameGlobals.RemoveAllInput();
            //GameGlobals.Players.Clear();

            m_onAllPlayersDead.Invoke();

            enabled = false;
        }

        IEnumerator ReviveDialogueAction()
        {
            m_tutorialDialogueAction.Init();
            while (!m_tutorialDialogueAction.IsFinished)
            {
                m_tutorialDialogueAction.Update(Time.deltaTime);
                yield return null;
            }
        }

    }
}
