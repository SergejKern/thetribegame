using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Utility.CollisionAndDamage
{
    public class CollisionMaterialComponent : MonoBehaviour, IHitHandler
    {
#pragma warning disable 0649 // wrong warning
        [SerializeField] CollisionMaterialID m_materialID;
        [SerializeField] UnityEvent m_onHit;
        [SerializeField] float m_collisionPosCenterPercent;
        [SerializeField] Transform m_collisionPos;
#pragma warning restore 0649 // wrong warning

        // ReSharper disable once ConvertToAutoProperty
        public Vector3 CollisionPos => m_collisionPos == null ? transform.position : m_collisionPos.position;
        public float CollisionPosCenterPercent => m_collisionPosCenterPercent;
        public CollisionMaterialID Material => m_materialID;
        public void OnHit() => m_onHit.Invoke();
    }
}