using System;
using System.Collections.Generic;
using Core.Unity.Utility.PoolAttendant;
using Game.Configs;
using GameUtility.Components.Collision;
using GameUtility.Data.Feedback;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using IControllable = GameUtility.Interface.IControllable;


namespace Game.Utility.CollisionAndDamage
{
    public class DamageComponent : MonoBehaviour, ITriggerObjEnterHandling
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible, ConvertToAutoPropertyWithPrivateSetter
        [FormerlySerializedAs("damage"), SerializeField]
        float m_damage;
        [FormerlySerializedAs("force"), SerializeField]
        float m_knockBack;

        [FormerlySerializedAs("actor")] [SerializeField]
        Transform m_actor;

        [SerializeField] TeamComponent m_teamComponent;

        [SerializeField] Collider[] m_collider;

        // it seems weird when we block actor in front but weapon-collider comes from back and will hit us, so we take the actor.position for damage direction
        [FormerlySerializedAs("m_directionFromActorPercent")]
        [SerializeField]
        float m_originIsActorPercent = 0.85f;
        [SerializeField] float m_damagePosOriginPercent;

        [SerializeField] CollisionMaterialID m_materialID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly List<ICollisionMaterialComponent> m_damageHandlers = new List<ICollisionMaterialComponent>();
        IList<ICollisionMaterialComponent> m_currentHitObjects;

        [FormerlySerializedAs("OnDamage")]
        public DamageUnityEvent OnDamageDone;
        [FormerlySerializedAs("OnDamageHandlerDone")]
        public UnityEvent OnCollision;

        float m_dmgFactor = 1f;
        float m_knockBackFactor = 1f;

        float Damage => m_damage * m_dmgFactor;
        float KnockBack => m_knockBack * m_knockBackFactor;
        public CollisionMaterialID Material
        {
            get => m_materialID;
            set => m_materialID = value;
        }

        GameObject Aggressor => m_actor != null ? m_actor.gameObject : m_lastActor != null? m_lastActor.gameObject : null;
        Transform Actor
        {
            get => m_actor;
            set
            {
                m_actor = value;
                if (value != null)
                    m_lastActor = value;
            }
        }

        ITeamComponent Team => m_teamComponent ? 
            m_teamComponent :
            Aggressor.GetComponent<ITeamComponent>();

        Transform m_lastActor;

        DamageEvent m_lastDamageData;
        HandleDamageResult m_damageResult;
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible,ConvertToAutoPropertyWithPrivateSetter

        void OnEnable()
        {
            if (m_currentHitObjects == null)
                m_currentHitObjects = m_damageHandlers;
            m_damageHandlers.Clear();
            foreach (var c in m_collider)
                c.enabled = true;
        }

        void OnDisable()
        {
            foreach (var c in m_collider)
                c.enabled = false;
        }

        public HandleDamageResult GetLastHandleDamageResult() => m_damageResult;
        public void SetDamageHandlers(IList<ICollisionMaterialComponent> appliedDamageHandlers = null) => 
            m_currentHitObjects = appliedDamageHandlers ?? m_damageHandlers;

        public void ApplyFactor(float dmg, float knockBack)
        {
            // Debug.Log($"Applied dmg: {dmg} knockBack {knockBack} on {transform.parent.name}/{gameObject.name}");
            m_dmgFactor = dmg;
            m_knockBackFactor = knockBack;
        }

        public void SetActor(Transform act) => Actor = act;

        public void OnTriggerObjectEnter(GameObject root) => OnTriggerObjectEnter(root, root.transform.position);

        void OnTriggerObjectEnter(GameObject root, Vector3 collisionPos)
        {
            if (!enabled)
                return;
            if (Aggressor == root)
                return;
            if (IgnoreDamage(root))
                return;

            var info = AddDamage(root, collisionPos);
            AddForce(root, info);
        }

        HandleDamageResult AddDamage(GameObject collidedWith, Vector3 collisionPos)
        {
            if (!CollisionValid(collidedWith, out var materialComponent, out var hitHandler, out var damageHandler))
                return new HandleDamageResult() { Result = DamageResult.Ignored };

            collisionPos = Vector3.Lerp(collisionPos, 
                materialComponent.CollisionPos, 
                materialComponent.CollisionPosCenterPercent);

            m_damageResult = default;
            m_damageResult.AffectedObject = collidedWith;

            m_damageResult.Material = materialComponent.Material;
            m_damageResult.Result = DamageResult.Ignored;
            // fixed feeling of blocking
            m_damageResult.DamagePos = GetDamagePos(collisionPos);

            var dir = collisionPos - m_damageResult.DamagePos;
            m_damageResult.DamageDirection = new Vector2(dir.x, dir.z); //.normalized;

            hitHandler?.OnHit();
            damageHandler?.HandleDamage(Damage, ref m_damageResult);

            if (damageHandler == null || m_damageResult.Result != DamageResult.Ignored)
                CollisionEffect(m_damageResult);

            if (m_damageResult.DamageDone <= 0f)
                return NoDamageDone(collidedWith, dir, m_damageResult);

            // Debug.Log($"Damage Done: {dmgInfo.DamageDone} Damage Factor {m_dmgFactor} * Damage {m_damage} = {Damage} on {transform.parent.name}/{gameObject.name}");
            // todo 3: add rumble to config
            OnDamageRumble();
            return DamageDone(collidedWith, dir, m_damageResult);
        }

        #region AddDamage methods
        HandleDamageResult DamageDone(GameObject collidedWith, Vector3 dir, HandleDamageResult dmgInfo)
        {
            DEBUG_DrawDamageDir(collidedWith.transform.position, dir, Color.red);

            var dmgData = new DamageEvent(collidedWith, Aggressor, dir, dmgInfo);
            DamageEvent.Trigger(dmgData);

            OnDamageDone.Invoke(dmgData); //new DamageData() { Damaged = collidedWith, Direction = dir }
            
            if (collidedWith.TryGetComponent(out IDamagedByActorMarker marker) && Aggressor != null)
                marker.LastAggressor = Aggressor;

            return dmgInfo;
        }

        HandleDamageResult NoDamageDone(GameObject collidedWith, Vector3 dir, HandleDamageResult dmgInfo)
        {
            //OnCollision.Invoke();
            DEBUG_DrawDamageDir(collidedWith.transform.position, dir, Color.gray);
            return dmgInfo;
        }

        void OnDamageRumble()
        {
            if (m_actor != null 
                && m_actor.TryGetComponent<IControllable>(out var c)
                && c.Centre is IRumble r)
                r.SetRumble(0.15f, 0.05f);
        }

        void CollisionEffect(HandleDamageResult dmgInfo)
        {
            OnCollision.Invoke();

            var collisionData = ConfigLinks.CollisionsConfig.GetCollisionData(dmgInfo.Material, m_materialID);
            collisionData.Audio?.Result?.Play(transform);
            var eff = collisionData.Effect;
            if (eff != null)
                eff.GetPooledInstance(dmgInfo.DamagePos);
        }

        bool CollisionValid(GameObject collidedWith, 
            out ICollisionMaterialComponent matComponent, 
            out IHitHandler hitHandler, 
            out IDamageHandler dmg)
        {
            dmg = null;
            hitHandler = null;

            if (!collidedWith.TryGetComponent(out matComponent))
                return false;

            // already handled
            if (m_currentHitObjects.Contains(matComponent))
                return false;
            m_currentHitObjects.Add(matComponent);

            collidedWith.TryGetComponent(out hitHandler);
            collidedWith.TryGetComponent(out dmg);
            return true;
        }

        Vector3 GetDamageOrigin()
        {
            var thisPos = transform.position;
            return Actor != null ? Vector3.Lerp(thisPos, Actor.position, m_originIsActorPercent) : thisPos;
        }

        Vector3 GetDamagePos(Vector3 otherPos)
        {
            // todo 3: might be pretty inaccurate, but maybe ok for now:
            return Vector3.Lerp(otherPos, GetDamageOrigin(), m_damagePosOriginPercent);
        }

        #endregion

        static void DEBUG_DrawDamageDir(Vector3 pos, Vector3 dir, Color c)
        {
            Debug.DrawLine(pos - Vector3.forward, pos + Vector3.forward, c, 2.0f);
            Debug.DrawLine(pos - Vector3.left, pos + Vector3.left, c, 2.0f);
            Debug.DrawLine(pos, pos + 5.0f * -dir.normalized, c, 2.0f);
        }

        bool IgnoreDamage(GameObject other)
        {
            if (Aggressor == null && m_teamComponent == null)
                return false;

            if (Team == null || !other.TryGetComponent<ITeamComponent>(out var otherTeam))
                return false;

            return Team.IsOnSameTeam(otherTeam);
        }

        void OnTriggerEnter(Collider other)
        {
            if (!enabled)
                return;

            var collisionRoot = RelativeCollider.GetRoot(other);
            var collisionPos = other.ClosestPointOnBounds(GetDamageOrigin());
            //Debug.Log($"DMG-OnTriggerEnter {gameObject} collisionRoot: {collisionRoot}");
            OnTriggerObjectEnter(collisionRoot, collisionPos);
        }

        void AddForce(GameObject toObject, HandleDamageResult dmgResult)
        {
            if (!toObject.TryGetComponent<IKnockback>(out var knock))
                return;
            // todo 5: direction from attack??
            var dir = (toObject.transform.position - transform.position);
            dir.y = 0.0f;

            var direction = dir.normalized;
            var amount = KnockBack;
            var durationFactor = 1f;
            var recoverFactor = 1f;

            switch (dmgResult.Result)
            {
                case DamageResult.Damaged: break;
                case DamageResult.Blocked: amount *= 0.5f; break;
                case DamageResult.Killed:
                    amount = 10;
                    durationFactor = 10;
                    recoverFactor = 0;
                    break;
                case DamageResult.Ignored: return;
                case DamageResult.Absorbed: return;
                default: throw new ArgumentOutOfRangeException();
            }
            knock.ApplyKnockback(direction, amount, durationFactor, recoverFactor);
        }
    }
}
