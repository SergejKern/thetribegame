﻿using System.Collections.Generic;
using Core.Events;
using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Utility
{
    public static class TargetsCollector
    {
        public static readonly List<ITarget> Targets = new List<ITarget>();
        static readonly TargetsCollectorEventHook k_hook = new TargetsCollectorEventHook();

        [RuntimeInitializeOnLoadMethod]
        static void OnRuntimeMethodLoad()
        {
            k_hook.EventStartListening<TargetSpawnedEvent>();
            k_hook.EventStartListening<TargetDespawnedEvent>();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        static void OnSceneLoaded(Scene sc, LoadSceneMode mode)
        {
            if (mode != LoadSceneMode.Single)
                return;
            Targets.Clear();
        }
    }        
    internal class TargetsCollectorEventHook : IEventListener<TargetSpawnedEvent>, IEventListener<TargetDespawnedEvent>
    {
        public void OnEvent(TargetSpawnedEvent ev) => TargetsCollector.Targets.Add(ev.Target);
        public void OnEvent(TargetDespawnedEvent ev) => TargetsCollector.Targets.Remove(ev.Target);
    }

    public struct TargetSpawnedEvent
    {
        public readonly ITarget Target;
        TargetSpawnedEvent(ITarget target) => Target = target;
        public static void Trigger(ITarget target) => Trigger(new TargetSpawnedEvent(target));
        static void Trigger(TargetSpawnedEvent e) => EventMessenger.TriggerEvent(e);
    }

    public struct TargetDespawnedEvent
    {
        public readonly ITarget Target;
        TargetDespawnedEvent(ITarget target) => Target = target;
        public static void Trigger(ITarget target) => Trigger(new TargetDespawnedEvent(target));
        static void Trigger(TargetDespawnedEvent e) => EventMessenger.TriggerEvent(e);
    }
}
