﻿using System;
using Core.Interface;
using Game.Actors.Components;
using Game.Actors.Controls;
using Game.Combat.Weapon;
using Game.InteractiveObjects;
using Game.Utility.CollisionAndDamage;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Utility
{
    // todo 0: make weapon bouncy (michail)
    // ^- Since weapon does not fall to ground bouncy is currently unimportant 
    // todo 0: Split this behaviour to playmaker-actions? So to re-use velocity over time with dash?
    // ^- both behaviour grew a bit too complex to unite them, we could rethink this with ScriptableUtility,
    // but rather unsure
    public class ThrownBehaviour : MonoBehaviour, IPoolable
    {
        enum ThrowState
        {
            None,
            Throw,
            Returning,
            Done
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AnimationCurve m_velocityCurve;
        [SerializeField] AnimationCurve m_torqueCurve;
        [SerializeField] float m_duration;
        [SerializeField] float m_velocityFactor;
        [SerializeField] float m_torqueFactor;

        [SerializeField] CoyoteFallBehaviour m_fallBehaviour;
        [SerializeField] DamageComponent m_damageComponent;
        [SerializeField] WeaponFXID m_throwWeaponFXID;
#pragma warning restore 0649 // wrong warnings for SerializeField
        float TimeMultiplier => m_multiplier?.SpeedFactor ?? 1f;

        ThrowState m_throwState;
        bool m_returnToThrower;
        float m_returnDuration;

        Vector3 m_startPosition;

        Vector3 m_angularDirection;
        Vector3 m_direction;
        GameObject m_actor;

        Rigidbody m_rigidbody;
        float m_time;
        Transform m_item;
        ITimeMultiplier m_multiplier;
        public void Throw(Transform item, GameObject actor, Vector3 dir, Vector3 angular, bool returnToThrower)
        {
            if (actor == null)
                return;
            m_returnToThrower = returnToThrower;
            m_throwState = ThrowState.Throw;
            m_actor = actor;
            m_multiplier = actor.GetComponent<ITimeMultiplier>();

            if (TryGetComponent<TeamComponent>(out var teamComponent) && m_actor.TryGetComponent<TeamComponent>(out var actorTeam))
                teamComponent.TeamFlag = actorTeam.TeamFlag;

            m_angularDirection = angular;
            m_direction = dir;
            m_item = item;
            m_item.SetParent(transform, true);
            m_item.localPosition = Vector3.zero;
            m_item.localRotation = Quaternion.identity;
            
            m_startPosition = m_item.position;

            if (m_item.TryGetComponent<Weapon>(out var w))
                w.Play(m_throwWeaponFXID);

            var dmg = m_item.GetComponentInChildren<DamageComponent>();
            if (m_damageComponent != null && dmg != null)
                m_damageComponent.Material = dmg.Material;
        }

        void FixedUpdate()
        {
            if (m_duration < float.Epsilon)
                return;

            switch (m_throwState)
            {
                case ThrowState.Throw: ThrowUpdate(); break;
                case ThrowState.Returning: ReturnToThrowerUpdate(); break;
                case ThrowState.None:
                case ThrowState.Done: break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        void ThrowUpdate()
        {
            m_time += Time.fixedDeltaTime;
            var interpolation = Mathf.Clamp01(m_time / m_duration);

            var velocity = Mathf.Clamp01(m_velocityCurve.Evaluate(interpolation));
            var torque = Mathf.Clamp01(m_torqueCurve.Evaluate(interpolation));

            //Debug.Log(velocity + " " +torque);
            m_rigidbody.velocity = m_velocityFactor * velocity * m_direction;
            m_rigidbody.maxAngularVelocity = float.MaxValue;
            m_rigidbody.angularVelocity = m_torqueFactor * torque * m_angularDirection;

            if (interpolation >= 1 - float.Epsilon)
                Finish();
        }

        float m_distanceTraveled;
        void ReturnToThrowerInit()
        {
            m_throwState = ThrowState.Returning;

            m_distanceTraveled = Vector3.Distance(m_startPosition, m_rigidbody.position);
            //Debug.Log($"m_distanceTraveled {m_distanceTraveled}, {m_rigidbody.position}");
            m_startPosition = m_rigidbody.position;
            m_rigidbody.velocity = Vector3.zero;

            m_returnDuration = m_time;
            m_time = 0;
        }

        void ReturnToThrowerUpdate()
        {
            m_time += Time.fixedDeltaTime * TimeMultiplier;

            var actorPos = m_actor.transform.position;
            var remaining = Vector3.Distance(m_rigidbody.position, actorPos);
            var distanceInterpolation = Mathf.Clamp01((m_distanceTraveled - remaining) / m_distanceTraveled);
            var interpolation = Mathf.Clamp01(m_time / m_returnDuration);
            var diff = distanceInterpolation - interpolation;
            if (diff > 0.1f)
            {
                interpolation = Mathf.Lerp(interpolation, distanceInterpolation,0.25f);
                var newTime = interpolation * m_returnDuration;
                //Debug.Log($"Fixing Time {m_time} to {newTime}");

                m_time = newTime;
            }

            var torque = Mathf.Clamp01(m_torqueCurve.Evaluate(interpolation));
            m_rigidbody.angularVelocity = m_torqueFactor * torque * m_angularDirection;
            var newPos = Vector3.Lerp(m_startPosition, actorPos, interpolation);
            //Debug.Log($"Moving from {m_startPosition} to {actorPos} by {interpolation} = {newPos}");

            m_rigidbody.MovePosition(newPos);

            if (!(interpolation >= 1 - float.Epsilon)) 
                return;

            if (m_item.TryGetComponent<Weapon>(out var w))
                ForceGrab(w);
            Finish();
        }

        void ForceGrab(IGrabbable weapon)
        {
            var grabCtrl = m_actor.GetComponentInChildren<GrabControl>();
            grabCtrl.GetData(out var data);
            var prevGrabEnable = data.GrabEnabled;
            grabCtrl.SetGrabEnabled(true);
            grabCtrl.Grab(null, weapon);
            grabCtrl.SetGrabEnabled(prevGrabEnable);
        }

        [UsedImplicitly]
        public void OnHit()
        {
            var lastResult = m_damageComponent.GetLastHandleDamageResult();
            var dmgDir = lastResult.DamageDirection;
            if (dmgDir.Equals(Vector2.zero))
                return;
            var v2Dir = new Vector2(m_direction.x, m_direction.z).normalized;
            var angle = Vector2.Angle(v2Dir, dmgDir);
            //Debug.Log($"v2Dir {v2Dir} dmgDir {dmgDir} angle {angle} {lastResult.AffectedObject}");
            if (angle < 55)
                OnHit(-m_rigidbody.velocity);
        }

        [UsedImplicitly]
        public void OnHit(GameObject hitGameObject) =>
            OnHit(transform.position - hitGameObject.transform.position);

        [UsedImplicitly]
        public void OnHit(DamageEvent data) => OnHit(data.Direction);

        void OnHit(Vector3 bounceDir)
        {
            if (m_throwState == ThrowState.Returning)
                return;

            var velocity = -bounceDir * m_rigidbody.velocity.magnitude;
            velocity.y = 0;
            m_rigidbody.velocity = velocity;
            Finish();
        }

        void Finish()
        {
            if (m_throwState == ThrowState.Throw && m_returnToThrower)
            {
                ReturnToThrowerInit();
                return;
            }
            m_throwState = ThrowState.Done;

            if (m_item.TryGetComponent<Weapon>(out var w))
                w.Stop(m_throwWeaponFXID);

            enabled = false;
            if (TryGetComponent<DamageComponent>(out var dmg))
                dmg.enabled = false;

            m_fallBehaviour.enabled = true;
            Invoke($"{nameof(DestroyUnparent)}", 2f);
        }

        void DestroyUnparent()
        {
            if (m_item != null && m_item.transform.parent == transform)
                m_item.transform.SetParent(null, true);
            gameObject.SetActive(false);
        }

        public void OnSpawn()
        {
            m_throwState = ThrowState.None;
            m_rigidbody = GetComponent<Rigidbody>();

            m_time = 0f;
            enabled = true;
            m_fallBehaviour.enabled = false;
            if (TryGetComponent<DamageComponent>(out var dmg))
                dmg.enabled = true;
        }

        public void OnDespawn() { }
    }
}
