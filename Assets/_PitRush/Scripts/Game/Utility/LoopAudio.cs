﻿using System.Linq;
using UnityEngine;

namespace Game.Utility
{
    public class LoopAudio : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AudioClip m_loopingClip;
        [SerializeField] float m_subtractSeconds;
        [SerializeField] float m_volume;
#pragma warning restore 0649 // wrong warnings for SerializeField

        int m_flip;
        readonly AudioSource[] m_audioSources = new AudioSource[2];
        double m_nextEventTime;
        bool m_running;
        AudioSource m_currentSource;
        void Start()
        {
            var loopAudios = FindObjectsOfType<LoopAudio>();
            // don't add multiple of this to DontDestroy and play audio, when we go back to title screen
            if (loopAudios.Any(a => a != this))
                return;
            
            DontDestroyOnLoad(gameObject);
            for (var i = 0; i < 2; i++)
            {
                var child = new GameObject("Player");
                child.transform.parent = gameObject.transform;
                m_audioSources[i] = child.AddComponent<AudioSource>();
                m_audioSources[i].volume = m_volume;
                //DontDestroyOnLoad(child);
            }

            m_currentSource = m_audioSources[1];
            m_currentSource.clip = m_loopingClip;
            m_currentSource.Play();

            m_nextEventTime = AudioSettings.dspTime + m_loopingClip.length - m_subtractSeconds;
            m_running = true;
        }

        void Update()
        {
            if (!m_running)
                return;
            
            var time = AudioSettings.dspTime;

            m_nextEventTime = AudioSettings.dspTime + (m_loopingClip.length - m_currentSource.time) - m_subtractSeconds;

            if (!(time + 1.0f > m_nextEventTime))
                return;

            m_currentSource = m_audioSources[m_flip];
            m_currentSource.clip = m_loopingClip;
            m_currentSource.PlayScheduled(m_nextEventTime);
            m_nextEventTime += m_loopingClip.length;
            m_flip = 1 - m_flip;
        }
    }
}
