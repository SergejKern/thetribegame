// GENERATED AUTOMATICALLY FROM 'Assets/_PitRush/Configs/Default.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace TribeWorldInput
{
    public class @DefaultControls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @DefaultControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""Default"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""0350beeb-e69b-4681-92ca-c00c1bfcbf6a"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""9fa63457-4c76-4cb2-a524-bcc356708300"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Aim"",
                    ""type"": ""Value"",
                    ""id"": ""5023e23f-7bfe-4169-8d27-a898a3206398"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Special"",
                    ""type"": ""Value"",
                    ""id"": ""f27caa4a-e467-476e-b80f-fc6197368320"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Energy"",
                    ""type"": ""PassThrough"",
                    ""id"": ""958a817a-e6a2-4d5d-940a-f3cb76183579"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""PassThrough"",
                    ""id"": ""eeccceaa-0364-4bfe-820c-a575d8728621"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""Value"",
                    ""id"": ""eeb194c7-2f77-4e2a-acbf-a5f18c2018e5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Value"",
                    ""id"": ""7f337160-1fe3-438d-a4f9-4d1bfc28935c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Drop"",
                    ""type"": ""Value"",
                    ""id"": ""016b204b-467b-41ce-8895-f3045b158b31"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""SwitchItems"",
                    ""type"": ""Value"",
                    ""id"": ""1acda1c7-5947-49fa-a755-6025ee4856b6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Join"",
                    ""type"": ""Value"",
                    ""id"": ""fc81b2af-1ea3-48c7-8041-9df46c79d309"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Defense"",
                    ""type"": ""Value"",
                    ""id"": ""bedf8df9-019d-42c9-80b2-c00719885152"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Lock"",
                    ""type"": ""Button"",
                    ""id"": ""eeef97f3-7cac-4409-aec8-e8ea99b5809f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""NextTarget"",
                    ""type"": ""Button"",
                    ""id"": ""ee61f515-3f56-4533-8bb5-2c65dc0e934d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""PreviousTarget"",
                    ""type"": ""Button"",
                    ""id"": ""d6536fbc-9e57-4bf0-bf1e-63dc395db288"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""1ef05d3f-5581-409e-bef4-20f6a3811924"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""54aebf4b-1ad2-47cf-acdb-123210ed3cba"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""88c31c6e-3409-41f8-8407-1b5b169fe5d1"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Up"",
                    ""id"": ""71eb51a6-feb5-483c-8eeb-f0a9f1035bdf"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Down"",
                    ""id"": ""c43858ad-4c62-47e8-9c98-9e8453d22ae5"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left"",
                    ""id"": ""ef585d8f-0c20-4da8-9dbf-6bff6da54fcb"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Right"",
                    ""id"": ""610cc234-7eaf-47c1-9f4a-b584447fd6d1"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""3b7ef5cd-3abd-48d3-8cea-89e1cea4b9e6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""66b99b79-ea7c-4080-94d2-764648d8c4bc"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""e5f02fb3-44e7-4382-99d6-09c4e0c95662"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""17dc5bcf-2363-4d36-b8ad-3794e62e11e6"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f118fcde-a449-42d1-89f1-377959843a21"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""0032f862-0962-4cbd-a2a9-361e76f4b8e4"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Special"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8f92d927-f5dc-4b00-8134-6054f2e01aab"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Special"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6ac7161b-b349-4ca8-93de-6c86547f62e4"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Special"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""15ebce50-3bcf-4eda-9d4a-66b63e50bbab"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Energy"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8ed7e167-45bf-43cb-9b9e-4f984e65084a"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1548081e-387b-45c2-a1d9-8bda169e03ba"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""19c93d4f-4a93-4d92-80c5-51fa611ec362"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf3d4034-0b5f-4569-8406-683e3ee6ed1d"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""955155e7-b5d6-4483-9a5a-2101b34f30d1"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97f11a9a-e374-4c05-9273-f03d4be2b8f5"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6a89c0d8-cb40-4b45-b21e-6e8bdf678785"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f199a997-9fde-489c-9bac-fe58d2520e93"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25cf2829-f452-4257-9407-e53d9a23bcc5"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aa9a5d74-e5c4-4ccd-8e61-f668a3492045"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a67c468d-ff94-41db-8002-00f0fc123c40"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Aim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""51f4d9b8-a60d-45dd-894a-81ddb992e303"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": ""MouseAim"",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Aim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""adb53253-ee08-497d-bb32-4fc66ce513a8"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Drop"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""42afc60e-e784-4edd-b964-e7cde8a3d73b"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Drop"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7f651cb9-0de5-442e-8967-0c95e801e12f"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""SwitchItems"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8cd12710-e49a-4f4f-88bd-cb2fe12dc52e"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""SwitchItems"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""afd42407-5ef5-4694-931d-20e8b4a24cfd"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Join"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""91485766-b4f7-4d11-aa2c-ee2d25c118e2"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Join"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5d79628d-e17f-412f-98b9-cdf8a07072e2"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Join"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""263f3d6c-da18-48c0-a3c6-5ee1112093c0"",
                    ""path"": ""<Gamepad>/rightStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Lock"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de795c1a-699e-465d-aef7-e0a169d12c8b"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Lock"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ab7bf053-4059-4cb4-9d76-9a4e6d55eb2b"",
                    ""path"": ""<Gamepad>/rightStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""NextTarget"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""42dec3e7-e091-4a35-9973-3e333a67205b"",
                    ""path"": ""<Gamepad>/rightStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PreviousTarget"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""72aa85d7-843b-4fbf-9cce-53feb8014651"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";KeyboardScheme"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cc4ef271-f97b-4d23-83d7-01b6d2b7f0b7"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eb4a556f-d844-45c9-b81a-77eabe157037"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""111ce07c-d83c-4ef6-9f75-e32b9afc2694"",
            ""actions"": [
                {
                    ""name"": ""Confirm"",
                    ""type"": ""Button"",
                    ""id"": ""b492b8cf-e413-4ff8-a1e9-3155ec356bc6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Leave"",
                    ""type"": ""Button"",
                    ""id"": ""feb5f209-ef84-403b-8434-569dee7d4051"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Navigate"",
                    ""type"": ""Value"",
                    ""id"": ""2a165f83-3c20-4db0-8c76-14ab1da3e819"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""a0b606d5-0806-43fb-9982-af8fd726b97e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Point"",
                    ""type"": ""PassThrough"",
                    ""id"": ""47707ec7-9933-4420-8f1f-df60b8402df8"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Click"",
                    ""type"": ""PassThrough"",
                    ""id"": ""9b5238ef-468f-4631-aa45-3c0024e20361"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""GoBackHold"",
                    ""type"": ""PassThrough"",
                    ""id"": ""d0da3435-cad9-46c1-a9eb-bbc259417f9e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""36b8f8dd-f5b5-43a8-a1b7-cdaebf92601f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fcb3a36b-4dea-4064-a79b-969d77dec3b2"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7ae1a805-5e65-4802-bb6d-c245692956b3"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""baaa0670-8a96-48ce-83ec-a856e21d4236"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Stick"",
                    ""id"": ""32be84e5-dd8e-4b3a-9f66-d9e3cbc1a944"",
                    ""path"": ""2DVector"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""53ab1444-5627-4ad9-816f-e0e32c7a4f05"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""7251729f-8c70-4003-a776-246bb39d8857"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""52d88d69-81e3-4112-b521-2db80f39106c"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""75bbb241-7281-4f29-8607-01ef6ad044b2"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""84f21f4d-01b5-49a2-bd44-5eea9a935375"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7e4f6dbb-1eb1-4835-95c5-fa57e5d19709"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""f7d6acfb-8251-4182-a97b-6e80c2229ee9"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""561469fe-60a0-45d8-9259-4652a1007e7d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e6189800-5583-4dbf-8520-193dd57dcffb"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""fe3405d7-6bc7-4dd8-bc2a-61f561694b66"",
                    ""path"": ""<Gamepad>/dpad"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""3b91f083-2470-4acd-8369-4f9d8a2779dd"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""3d438893-74d8-429c-8701-06f508e1c11e"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""7b986251-0b15-4a9b-b907-ad125a5dcd30"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6227df90-1ce0-452d-b379-2fbb2e0fc6c1"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d9f04d9e-7b9b-4e5c-8ef8-e0992442ccf5"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""46ef29a7-5488-416e-9422-b6eb534354ca"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""612b1aa3-934f-4e3a-a217-312c2f9dd038"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Point"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0c8ffdbf-fb8d-4ccc-afd6-32e725aa4403"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5188fdc9-3be0-4599-aee2-14a0d32ce0b0"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6cc1bc1e-9649-4f47-af01-cd3924aa1f78"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""GoBackHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e47a635b-c644-43ee-89bf-0262105fbd71"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""GoBackHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6ce66331-b342-4a6d-a6fa-bfdab04bf954"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f5c89037-7f7f-4c8e-9216-1b5b06483dad"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardScheme"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyboardScheme"",
            ""bindingGroup"": ""KeyboardScheme"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Player
            m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
            m_Player_Move = m_Player.FindAction("Move", throwIfNotFound: true);
            m_Player_Aim = m_Player.FindAction("Aim", throwIfNotFound: true);
            m_Player_Special = m_Player.FindAction("Special", throwIfNotFound: true);
            m_Player_Energy = m_Player.FindAction("Energy", throwIfNotFound: true);
            m_Player_Interact = m_Player.FindAction("Interact", throwIfNotFound: true);
            m_Player_Attack = m_Player.FindAction("Attack", throwIfNotFound: true);
            m_Player_Dash = m_Player.FindAction("Dash", throwIfNotFound: true);
            m_Player_Drop = m_Player.FindAction("Drop", throwIfNotFound: true);
            m_Player_SwitchItems = m_Player.FindAction("SwitchItems", throwIfNotFound: true);
            m_Player_Join = m_Player.FindAction("Join", throwIfNotFound: true);
            m_Player_Defense = m_Player.FindAction("Defense", throwIfNotFound: true);
            m_Player_Lock = m_Player.FindAction("Lock", throwIfNotFound: true);
            m_Player_NextTarget = m_Player.FindAction("NextTarget", throwIfNotFound: true);
            m_Player_PreviousTarget = m_Player.FindAction("PreviousTarget", throwIfNotFound: true);
            m_Player_Pause = m_Player.FindAction("Pause", throwIfNotFound: true);
            // Menu
            m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
            m_Menu_Confirm = m_Menu.FindAction("Confirm", throwIfNotFound: true);
            m_Menu_Leave = m_Menu.FindAction("Leave", throwIfNotFound: true);
            m_Menu_Navigate = m_Menu.FindAction("Navigate", throwIfNotFound: true);
            m_Menu_Cancel = m_Menu.FindAction("Cancel", throwIfNotFound: true);
            m_Menu_Point = m_Menu.FindAction("Point", throwIfNotFound: true);
            m_Menu_Click = m_Menu.FindAction("Click", throwIfNotFound: true);
            m_Menu_GoBackHold = m_Menu.FindAction("GoBackHold", throwIfNotFound: true);
            m_Menu_Pause = m_Menu.FindAction("Pause", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Player
        private readonly InputActionMap m_Player;
        private IPlayerActions m_PlayerActionsCallbackInterface;
        private readonly InputAction m_Player_Move;
        private readonly InputAction m_Player_Aim;
        private readonly InputAction m_Player_Special;
        private readonly InputAction m_Player_Energy;
        private readonly InputAction m_Player_Interact;
        private readonly InputAction m_Player_Attack;
        private readonly InputAction m_Player_Dash;
        private readonly InputAction m_Player_Drop;
        private readonly InputAction m_Player_SwitchItems;
        private readonly InputAction m_Player_Join;
        private readonly InputAction m_Player_Defense;
        private readonly InputAction m_Player_Lock;
        private readonly InputAction m_Player_NextTarget;
        private readonly InputAction m_Player_PreviousTarget;
        private readonly InputAction m_Player_Pause;
        public struct PlayerActions
        {
            private @DefaultControls m_Wrapper;
            public PlayerActions(@DefaultControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move => m_Wrapper.m_Player_Move;
            public InputAction @Aim => m_Wrapper.m_Player_Aim;
            public InputAction @Special => m_Wrapper.m_Player_Special;
            public InputAction @Energy => m_Wrapper.m_Player_Energy;
            public InputAction @Interact => m_Wrapper.m_Player_Interact;
            public InputAction @Attack => m_Wrapper.m_Player_Attack;
            public InputAction @Dash => m_Wrapper.m_Player_Dash;
            public InputAction @Drop => m_Wrapper.m_Player_Drop;
            public InputAction @SwitchItems => m_Wrapper.m_Player_SwitchItems;
            public InputAction @Join => m_Wrapper.m_Player_Join;
            public InputAction @Defense => m_Wrapper.m_Player_Defense;
            public InputAction @Lock => m_Wrapper.m_Player_Lock;
            public InputAction @NextTarget => m_Wrapper.m_Player_NextTarget;
            public InputAction @PreviousTarget => m_Wrapper.m_Player_PreviousTarget;
            public InputAction @Pause => m_Wrapper.m_Player_Pause;
            public InputActionMap Get() { return m_Wrapper.m_Player; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerActions instance)
            {
                if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
                {
                    @Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                    @Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                    @Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                    @Aim.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAim;
                    @Aim.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAim;
                    @Aim.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAim;
                    @Special.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial;
                    @Special.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial;
                    @Special.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial;
                    @Energy.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnEnergy;
                    @Energy.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnEnergy;
                    @Energy.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnEnergy;
                    @Interact.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                    @Interact.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                    @Interact.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                    @Attack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                    @Attack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                    @Attack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                    @Dash.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                    @Dash.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                    @Dash.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                    @Drop.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDrop;
                    @Drop.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDrop;
                    @Drop.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDrop;
                    @SwitchItems.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSwitchItems;
                    @SwitchItems.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSwitchItems;
                    @SwitchItems.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSwitchItems;
                    @Join.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJoin;
                    @Join.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJoin;
                    @Join.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJoin;
                    @Defense.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefense;
                    @Defense.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefense;
                    @Defense.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefense;
                    @Lock.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLock;
                    @Lock.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLock;
                    @Lock.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLock;
                    @NextTarget.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnNextTarget;
                    @NextTarget.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnNextTarget;
                    @NextTarget.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnNextTarget;
                    @PreviousTarget.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPreviousTarget;
                    @PreviousTarget.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPreviousTarget;
                    @PreviousTarget.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPreviousTarget;
                    @Pause.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                    @Pause.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                    @Pause.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                }
                m_Wrapper.m_PlayerActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Move.started += instance.OnMove;
                    @Move.performed += instance.OnMove;
                    @Move.canceled += instance.OnMove;
                    @Aim.started += instance.OnAim;
                    @Aim.performed += instance.OnAim;
                    @Aim.canceled += instance.OnAim;
                    @Special.started += instance.OnSpecial;
                    @Special.performed += instance.OnSpecial;
                    @Special.canceled += instance.OnSpecial;
                    @Energy.started += instance.OnEnergy;
                    @Energy.performed += instance.OnEnergy;
                    @Energy.canceled += instance.OnEnergy;
                    @Interact.started += instance.OnInteract;
                    @Interact.performed += instance.OnInteract;
                    @Interact.canceled += instance.OnInteract;
                    @Attack.started += instance.OnAttack;
                    @Attack.performed += instance.OnAttack;
                    @Attack.canceled += instance.OnAttack;
                    @Dash.started += instance.OnDash;
                    @Dash.performed += instance.OnDash;
                    @Dash.canceled += instance.OnDash;
                    @Drop.started += instance.OnDrop;
                    @Drop.performed += instance.OnDrop;
                    @Drop.canceled += instance.OnDrop;
                    @SwitchItems.started += instance.OnSwitchItems;
                    @SwitchItems.performed += instance.OnSwitchItems;
                    @SwitchItems.canceled += instance.OnSwitchItems;
                    @Join.started += instance.OnJoin;
                    @Join.performed += instance.OnJoin;
                    @Join.canceled += instance.OnJoin;
                    @Defense.started += instance.OnDefense;
                    @Defense.performed += instance.OnDefense;
                    @Defense.canceled += instance.OnDefense;
                    @Lock.started += instance.OnLock;
                    @Lock.performed += instance.OnLock;
                    @Lock.canceled += instance.OnLock;
                    @NextTarget.started += instance.OnNextTarget;
                    @NextTarget.performed += instance.OnNextTarget;
                    @NextTarget.canceled += instance.OnNextTarget;
                    @PreviousTarget.started += instance.OnPreviousTarget;
                    @PreviousTarget.performed += instance.OnPreviousTarget;
                    @PreviousTarget.canceled += instance.OnPreviousTarget;
                    @Pause.started += instance.OnPause;
                    @Pause.performed += instance.OnPause;
                    @Pause.canceled += instance.OnPause;
                }
            }
        }
        public PlayerActions @Player => new PlayerActions(this);

        // Menu
        private readonly InputActionMap m_Menu;
        private IMenuActions m_MenuActionsCallbackInterface;
        private readonly InputAction m_Menu_Confirm;
        private readonly InputAction m_Menu_Leave;
        private readonly InputAction m_Menu_Navigate;
        private readonly InputAction m_Menu_Cancel;
        private readonly InputAction m_Menu_Point;
        private readonly InputAction m_Menu_Click;
        private readonly InputAction m_Menu_GoBackHold;
        private readonly InputAction m_Menu_Pause;
        public struct MenuActions
        {
            private @DefaultControls m_Wrapper;
            public MenuActions(@DefaultControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Confirm => m_Wrapper.m_Menu_Confirm;
            public InputAction @Leave => m_Wrapper.m_Menu_Leave;
            public InputAction @Navigate => m_Wrapper.m_Menu_Navigate;
            public InputAction @Cancel => m_Wrapper.m_Menu_Cancel;
            public InputAction @Point => m_Wrapper.m_Menu_Point;
            public InputAction @Click => m_Wrapper.m_Menu_Click;
            public InputAction @GoBackHold => m_Wrapper.m_Menu_GoBackHold;
            public InputAction @Pause => m_Wrapper.m_Menu_Pause;
            public InputActionMap Get() { return m_Wrapper.m_Menu; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
            public void SetCallbacks(IMenuActions instance)
            {
                if (m_Wrapper.m_MenuActionsCallbackInterface != null)
                {
                    @Confirm.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnConfirm;
                    @Confirm.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnConfirm;
                    @Confirm.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnConfirm;
                    @Leave.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeave;
                    @Leave.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeave;
                    @Leave.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeave;
                    @Navigate.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnNavigate;
                    @Navigate.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnNavigate;
                    @Navigate.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnNavigate;
                    @Cancel.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnCancel;
                    @Cancel.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnCancel;
                    @Cancel.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnCancel;
                    @Point.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnPoint;
                    @Point.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnPoint;
                    @Point.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnPoint;
                    @Click.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnClick;
                    @Click.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnClick;
                    @Click.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnClick;
                    @GoBackHold.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnGoBackHold;
                    @GoBackHold.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnGoBackHold;
                    @GoBackHold.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnGoBackHold;
                    @Pause.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnPause;
                    @Pause.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnPause;
                    @Pause.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnPause;
                }
                m_Wrapper.m_MenuActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Confirm.started += instance.OnConfirm;
                    @Confirm.performed += instance.OnConfirm;
                    @Confirm.canceled += instance.OnConfirm;
                    @Leave.started += instance.OnLeave;
                    @Leave.performed += instance.OnLeave;
                    @Leave.canceled += instance.OnLeave;
                    @Navigate.started += instance.OnNavigate;
                    @Navigate.performed += instance.OnNavigate;
                    @Navigate.canceled += instance.OnNavigate;
                    @Cancel.started += instance.OnCancel;
                    @Cancel.performed += instance.OnCancel;
                    @Cancel.canceled += instance.OnCancel;
                    @Point.started += instance.OnPoint;
                    @Point.performed += instance.OnPoint;
                    @Point.canceled += instance.OnPoint;
                    @Click.started += instance.OnClick;
                    @Click.performed += instance.OnClick;
                    @Click.canceled += instance.OnClick;
                    @GoBackHold.started += instance.OnGoBackHold;
                    @GoBackHold.performed += instance.OnGoBackHold;
                    @GoBackHold.canceled += instance.OnGoBackHold;
                    @Pause.started += instance.OnPause;
                    @Pause.performed += instance.OnPause;
                    @Pause.canceled += instance.OnPause;
                }
            }
        }
        public MenuActions @Menu => new MenuActions(this);
        private int m_KeyboardSchemeSchemeIndex = -1;
        public InputControlScheme KeyboardSchemeScheme
        {
            get
            {
                if (m_KeyboardSchemeSchemeIndex == -1) m_KeyboardSchemeSchemeIndex = asset.FindControlSchemeIndex("KeyboardScheme");
                return asset.controlSchemes[m_KeyboardSchemeSchemeIndex];
            }
        }
        private int m_GamepadSchemeIndex = -1;
        public InputControlScheme GamepadScheme
        {
            get
            {
                if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
                return asset.controlSchemes[m_GamepadSchemeIndex];
            }
        }
        public interface IPlayerActions
        {
            void OnMove(InputAction.CallbackContext context);
            void OnAim(InputAction.CallbackContext context);
            void OnSpecial(InputAction.CallbackContext context);
            void OnEnergy(InputAction.CallbackContext context);
            void OnInteract(InputAction.CallbackContext context);
            void OnAttack(InputAction.CallbackContext context);
            void OnDash(InputAction.CallbackContext context);
            void OnDrop(InputAction.CallbackContext context);
            void OnSwitchItems(InputAction.CallbackContext context);
            void OnJoin(InputAction.CallbackContext context);
            void OnDefense(InputAction.CallbackContext context);
            void OnLock(InputAction.CallbackContext context);
            void OnNextTarget(InputAction.CallbackContext context);
            void OnPreviousTarget(InputAction.CallbackContext context);
            void OnPause(InputAction.CallbackContext context);
        }
        public interface IMenuActions
        {
            void OnConfirm(InputAction.CallbackContext context);
            void OnLeave(InputAction.CallbackContext context);
            void OnNavigate(InputAction.CallbackContext context);
            void OnCancel(InputAction.CallbackContext context);
            void OnPoint(InputAction.CallbackContext context);
            void OnClick(InputAction.CallbackContext context);
            void OnGoBackHold(InputAction.CallbackContext context);
            void OnPause(InputAction.CallbackContext context);
        }
    }
}
