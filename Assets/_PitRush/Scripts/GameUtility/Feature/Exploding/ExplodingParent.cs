﻿using System.Collections;
using UnityEngine;

namespace GameUtility.Feature.Exploding
{
    public class ExplodingParent : MonoBehaviour
    {
        public bool DestroyThisOnRestore = true;
        public ExplodingPartMemory[] Parts;
        public IEnumerator Restore()
        {
            if (Parts == null)
                yield break;

            foreach (var p in Parts)
            {
                if (p == null)
                    continue;
                p.DestroyRigidbody();
            }
            yield return null;
            foreach (var p in Parts)
                p.Restore();

            if (gameObject.TryGetComponent<Animator>(out var animator))
                animator.enabled = true;
            if (DestroyThisOnRestore)
                Destroy(this);
            else Parts = null;
        }

        public void DoRestore() => StartCoroutine(Restore());
    }
}
