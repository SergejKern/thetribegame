﻿using UnityEngine;

namespace GameUtility.Feature.Exploding
{
    public static class ExplodeParts
    {
        // Start is called before the first frame update
        public static void ApplyExplosion(GameObject explodingRoot, 
            Vector3 pos, Vector3 posRandomization,
            float force, float radius, float upwardsMod)
        {
            if (explodingRoot.TryGetComponent<Animator>(out var animator))
                animator.enabled = false;

            var parent = explodingRoot.GetComponent<ExplodingParent>();
            if (parent == null)
                parent = explodingRoot.AddComponent<ExplodingParent>();

            var renderer = explodingRoot.GetComponentsInChildren<Renderer>();
            var parts = new ExplodingPartMemory[renderer.Length];

            for (var i = 0; i < renderer.Length; i++)
            {
                var tr = renderer[i];
                var part = tr.gameObject.AddComponent<ExplodingPartMemory>();
                part.Memorize();
                parts[i] = part;
            }

            parent.Parts = parts;
            foreach (var p in parts)
                p.ApplyExplosion(pos, posRandomization, force, radius, upwardsMod);
        }

    }
}
