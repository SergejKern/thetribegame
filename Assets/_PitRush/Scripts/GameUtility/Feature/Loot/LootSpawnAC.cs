﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Attribute;
using GameUtility.Config;
using GameUtility.Data.TransformAttachment;
using GameUtility.InitSystem;
using UnityEngine;

namespace GameUtility.Feature.Loot
{
    public class LootSpawnAC : MonoBehaviour, IInitDataComponent<LootSpawnAC.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public List<WeightedLoot> SpawnData;
            //public static ConfigData Default = new ConfigData() {SpawnData = new List<WeightedLoot>()};
        }

        [Serializable]
        public struct WeightedLoot
        {
            public PrefabLootConfig Loot;
            public int Weight;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [CopyPaste]
        [SerializeField] ConfigData m_data;

        [SerializeField] TransformData m_transformData;
#pragma warning restore 0649 // wrong warnings for SerializeField
        // ReSharper disable once ConvertToAutoPropertyWithPrivateSetter
        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data) => m_data = data;
        
        public void Spawn()
        {
            if (m_transformData.Parent == null)
                m_transformData.Parent = transform;
            var weightSum = m_data.SpawnData.Sum(s => s.Weight);
            var spawnAtWeight = UnityEngine.Random.Range(0, weightSum);

            var currentWeight = 0;
            var lootSpawned = new List<GameObject>();

            foreach (var s in m_data.SpawnData)
            {
                currentWeight += s.Weight;
                if (spawnAtWeight >= currentWeight) 
                    continue;
                // No loot!
                if (s.Loot == null)
                    break;
                s.Loot.GetLoot(ref lootSpawned, m_transformData);
                break;
            }

            if (!lootSpawned.IsNullOrEmpty())
                OnSpawned(lootSpawned);
        }

        protected virtual void OnSpawned(List<GameObject> spawned){}
    }
}