﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using UnityEngine;

namespace GameUtility.Feature.Loading
{
    public static class LoadOperations
    {
        static readonly List<LoadOperationID> k_operationIds = new List<LoadOperationID>();
        public static bool IsLoading => k_operationIds.Count > 0;
        public static void StartLoading(LoadOperationID id)
        {
            if (!k_operationIds.Contains(id))
                k_operationIds.Add(id);
            else
            {
                Debug.LogError($"Already loading {id}\n{Environment.StackTrace} ");
                return;
            }

            if (LoadingCanvas.Instance != null)
                LoadingCanvas.Instance.SetActive(true);
        }

        public static void LoadingDone(LoadOperationID id)
        {
            k_operationIds.Remove(id);
            if (k_operationIds.IsNullOrEmpty() 
                && LoadingCanvas.Instance != null)
                LoadingCanvas.Instance.SetActive(false);

            var log = $"Loading Done {id}\n";
            log = k_operationIds.Aggregate(log, (current, opId) => current + $"still loading: {opId}\n");
            Debug.Log(log);
        }
    }
}
