using UnityEngine.InputSystem;

namespace GameUtility.Interface
{
    public interface IControllable
    {
        IControlCentre Centre { get; }
        // ReSharper disable UnusedMemberInSuper.Global
        void InitializeWithController(PlayerInput input);
        void RemoveControl();
        // ReSharper restore UnusedMemberInSuper.Global

        void AddControlBlocker(ControlBlocker blocker);
        void RemoveControlBlocker(ControlBlocker blocker);
    }

    public struct ControlBlocker
    {
        public object Source;
    }
}