﻿using System;
using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.Interface
{
    public interface ITimeMultiplier
    {
        float SpeedFactor { get; }
        float Time { get; }
    }

    [Serializable]
    public class RefITimeMultiplier : InterfaceContainer<ITimeMultiplier>
    {
        public float SpeedFactor => Result?.SpeedFactor ?? 1f;
        public float DeltaTime => SpeedFactor * Time.deltaTime;
        public float FixedDeltaTime => SpeedFactor * Time.fixedDeltaTime;
    }
}
