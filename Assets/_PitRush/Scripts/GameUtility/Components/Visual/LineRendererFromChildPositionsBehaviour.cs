﻿using Core.Unity.Extensions;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Visual
{
    [RequireComponent(typeof(LineRenderer))]
    public class LineRendererFromChildPositionsBehaviour : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("addZeroPosAtStart")] [SerializeField] bool m_addZeroPosAtStart;
#pragma warning restore 0649 // wrong warnings for SerializeField

        int m_lastChildCount;

        LineRenderer m_lineRenderer;

        void Start() => m_lineRenderer = GetComponent<LineRenderer>();

        void FixedUpdate()
        {
            var childCount = transform.GetActiveChildCount();

            if (childCount != m_lastChildCount)
                UpdateLineRendererCount(childCount);

            var lineRendPos = 0;
            if (m_addZeroPosAtStart)
            {
                m_lineRenderer.SetPosition(lineRendPos, Vector3.zero);
                lineRendPos++;
            }

            for (var i = 0; i < childCount; i++, lineRendPos++)
            {
                var pos = transform.GetChild(i).localPosition;
                m_lineRenderer.SetPosition(lineRendPos, pos);
            }
        }

        void UpdateLineRendererCount(int childCount)
        {
            var linePointCount = childCount;

            if (m_addZeroPosAtStart)
                linePointCount++;

            m_lineRenderer.positionCount = linePointCount;
            m_lastChildCount = childCount;
        }
    }
}
