using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Visual
{
    public class PulsatingLight : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField

        [FormerlySerializedAs("minRange")] [SerializeField] float m_minRange = 0.5f;
        [FormerlySerializedAs("maxRange")] [SerializeField] float m_maxRange = 1.5f;

        [FormerlySerializedAs("minScale")] [SerializeField] float m_minScale = 0.5f;
        [FormerlySerializedAs("maxScale")] [SerializeField] float m_maxScale = 1.5f;

        [FormerlySerializedAs("frequency")] [SerializeField] float m_frequency = 1.0f;

        [FormerlySerializedAs("lightComponent")] [SerializeField]
        Light m_lightComponent;
        [FormerlySerializedAs("scaleObject")] [SerializeField]
        GameObject m_scaleObject;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_current;

        // Start is called before the first frame update
        void Start()
        {
            if (m_lightComponent == null)
                m_lightComponent = GetComponent<Light>();
            StartCoroutine(PulsatingRoutine());
        }

        IEnumerator PulsatingRoutine()
        {
            while (true)
            {
                while (m_current < 1.0f)
                {
                    m_current = Mathf.Clamp01(m_current + Time.deltaTime * m_frequency);

                    UpdateLight();
                    UpdateScale();

                    yield return new WaitForEndOfFrame();
                }
                while (m_current > 0.0f)
                {
                    m_current = Mathf.Clamp01(m_current - Time.deltaTime * m_frequency);

                    UpdateLight();
                    UpdateScale();
                    yield return new WaitForEndOfFrame();
                }
            }
            // ReSharper disable once IteratorNeverReturns
        }

        void UpdateLight()
        {
            if (m_lightComponent == null)
                return;
            var c = Mathf.SmoothStep(0.0f, 1.0f, m_current);
            m_lightComponent.range = Mathf.Lerp(m_minRange, m_maxRange, c);
        }

        void UpdateScale()
        {
            if (m_scaleObject == null)
                return;
            var c = Mathf.SmoothStep(0.0f, 1.0f, m_current);
            var scale = Mathf.Lerp(m_minScale, m_maxScale, c);
            m_scaleObject.transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}
