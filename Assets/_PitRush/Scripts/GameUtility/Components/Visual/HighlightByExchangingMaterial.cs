using System.Collections.Generic;
using Core.Extensions;
using GameUtility.Data.Visual;
using GameUtility.Interface;
using UnityEngine;

namespace GameUtility.Components.Visual
{
    public class HighlightByExchangingMaterial : MonoBehaviour, IHighlightable
    {
#pragma warning disable 0649 // wrong warnings for SerializeField

        [SerializeField] List<MaterialOverrideMap> m_materialMap = new List<MaterialOverrideMap>();
        [SerializeField] List<Renderer> m_renderer;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void EnableHighlight()
        {
            if (m_materialMap.IsNullOrEmpty())
                return;
            if (m_renderer.IsNullOrEmpty())
                return;
            
            foreach (var r in m_renderer)
            {
                if (r== null)
                    continue;
                var idx= m_materialMap.FindIndex(m => m.Default == r.sharedMaterial);
                if (idx != -1)
                    r.sharedMaterial = m_materialMap[idx].Override;
            }
        }

        public void DisableHighlight()
        {
            if (m_materialMap.IsNullOrEmpty())
                return;
            if (m_renderer.IsNullOrEmpty())
                return;

            foreach (var r in m_renderer)
            {
                if (r == null)
                    continue;
                var idx = m_materialMap.FindIndex(m => m.Override == r.sharedMaterial);
                if (idx != -1)
                    r.sharedMaterial = m_materialMap[idx].Default;
            }
        }
    }
}
