﻿using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.Components.Visual
{
    public class OnEnableAnimSelector : MonoBehaviour
    {
        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_anim;
        void OnEnable() => m_anim.Animator.Play(m_anim);
    }
}
