﻿using System;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Visual
{
    public class AnimationTransitionActivation : MonoBehaviour, ITransitionActivation
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Animator m_animator;
        [AnimTypeDrawer, SerializeField] 
        AnimatorStateRef m_onActivate;
        [AnimTypeDrawer, SerializeField]
        AnimatorStateRef m_onDeactivate;

        [SerializeField] UnityEvent m_onActiveTransitionDone;
        [SerializeField] UnityEvent m_onDeactivateTransitionDone;

        [SerializeField] bool m_deactivateAnimatorOnDone;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_isActive;
        bool m_initialized;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public InactiveFollowup InactiveFollowup { get; set; }
        public bool IsInTransition => m_isActive != IsActiveOrActivating;
        public bool IsActiveOrActivating { get; private set; }
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        // ReSharper disable once FlagArgument
        public void InitActive(bool active)
        {
            if (active != gameObject.activeSelf)
                gameObject.SetActive(active);
            IsActiveOrActivating = active;
            m_initialized = true;
        }

        public void SetActive(bool active)
        {
            if (!m_initialized)
                InitActive(gameObject.activeSelf);
            
            m_isActive = IsActiveOrActivating;
            IsActiveOrActivating = active;
            if (active)
                gameObject.SetActive(true);

            if (!gameObject.activeSelf) 
                return;
            m_animator.enabled = true;
            m_animator.Play(active ? m_onActivate : m_onDeactivate);
            m_animator.Update(0f);
        }

        void Update()
        {
            if (m_isActive == IsActiveOrActivating)
                return;
            if (m_animator.IsPlaying())
                return;

            if (m_deactivateAnimatorOnDone)
                m_animator.enabled = false;

            m_isActive = IsActiveOrActivating;

            if (m_isActive)
                m_onActiveTransitionDone.Invoke();
            else
                m_onDeactivateTransitionDone.Invoke();

            if (!IsActiveOrActivating)
            {
                switch (InactiveFollowup)
                {
                    case InactiveFollowup.None: break;
                    case InactiveFollowup.Despawn:
                        gameObject.TryDespawn(true);
                        return;
                    case InactiveFollowup.DespawnOrDestroy:
                        gameObject.TryDespawnOrDestroy(true);
                        return;
                    default: throw new ArgumentOutOfRangeException();
                }
            }

            gameObject.SetActive(IsActiveOrActivating);
        }
    }
}
