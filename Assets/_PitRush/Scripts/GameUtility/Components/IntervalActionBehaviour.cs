﻿using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace GameUtility.Components
{
    public class IntervalActionBehaviour : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_minIntervalSeconds;
        [SerializeField] float m_maxIntervalSeconds;

        [SerializeField] UnityEvent m_action;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_currentInterval = -1;

        void Awake() => RandomizeInterval();

        void RandomizeInterval() => m_currentInterval = Random.Range(m_minIntervalSeconds, m_maxIntervalSeconds);

        void Update()
        {
            if (m_currentInterval < 0)
            {
                m_action.Invoke();
                RandomizeInterval();
            }

            m_currentInterval -= Time.deltaTime;
        }
    }
}
