using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.Components.Collision
{
    /// <summary>
    /// UnityEvents (GameObjectEvent) for OnCollisionEnter, OnCollisionStay, OnCollisionExit
    /// [EC] = Event Component
    /// </summary>
    public class OnCollisionEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        GameObjectEvent m_onCollisionEnterEvent;
        [SerializeField]
        GameObjectEvent m_onCollisionStayEvent;
        [SerializeField]
        GameObjectEvent m_onCollisionExitEvent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void OnCollisionEnter(UnityEngine.Collision other)
        {
            if (other != null)
                m_onCollisionEnterEvent?.Invoke(RelativeCollider.GetRoot(other));
        }
        void OnCollisionStay(UnityEngine.Collision other)
        {
            if (other != null)
                m_onCollisionStayEvent?.Invoke(RelativeCollider.GetRoot(other));
        }

        void OnCollisionExit(UnityEngine.Collision other)
        {
            if (other != null)
                m_onCollisionExitEvent?.Invoke(RelativeCollider.GetRoot(other));
        }
    }
}
