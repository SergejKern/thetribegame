using Core.Unity.Types;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Collision
{
    /// <summary>
    /// Simple forwarding of a trigger-event with unity events
    /// [EC] = Event Component
    /// </summary>
    public class OnTriggerEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("m_onTriggerEnterEvent")]
        public GameObjectEvent OnTriggerEnterEvent;
        [FormerlySerializedAs("m_onTriggerStayEvent")]
        public GameObjectEvent OnTriggerStayEvent;
        [FormerlySerializedAs("m_onTriggerExitEvent")]
        public GameObjectEvent OnTriggerExitEvent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void OnTriggerEnter(Collider other)
        {
            if (other != null)
                OnTriggerEnterEvent?.Invoke(RelativeCollider.GetRoot(other));
        }
        void OnTriggerStay(Collider other)
        {
            if (other != null)
                OnTriggerStayEvent?.Invoke(RelativeCollider.GetRoot(other));
        }

        void OnTriggerExit(Collider other)
        {
            if (other != null)
                OnTriggerExitEvent?.Invoke(RelativeCollider.GetRoot(other));
        }
    }
}