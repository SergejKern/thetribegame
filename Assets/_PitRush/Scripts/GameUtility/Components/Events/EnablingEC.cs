﻿using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Events
{
    /// <summary>
    /// Unity Event on enable/ disable
    /// [EC] = Event Component
    /// </summary>
    public class EnablingEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onEnable;
        [SerializeField] UnityEvent m_onDisable;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void OnEnable() => m_onEnable.Invoke();
        void OnDisable() => m_onDisable.Invoke();
    }
}
