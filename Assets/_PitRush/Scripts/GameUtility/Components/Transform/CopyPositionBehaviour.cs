﻿using UnityEngine;

namespace GameUtility.Components.Transform
{
    public class CopyPositionBehaviour : MonoBehaviour
    {
        public UnityEngine.Transform Follow;

        void Update()
        {
            if (Follow == null)
                return;
            transform.position = Follow.position;
        }
    }
}
