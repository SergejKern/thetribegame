﻿using System.Collections;
using System.Collections.Generic;
using Core.Unity.Extensions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for destroying GameObjects
    /// [AC] = Action Component
    /// </summary>
    public class DestroyGameObjectsAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("postponeDestructionFrames")] [SerializeField] int m_postponeDestructionFrames = 1;

        [FormerlySerializedAs("destroyGameObjects")] [SerializeField] List<GameObject> m_destroyGameObjects;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [UsedImplicitly]
        public void DoDestroy()
        {
            // if other actions like unparenting are pending we cannot destroy yet
            StartCoroutine(DestroyRoutine());
        }

        IEnumerator DestroyRoutine()
        {
            var frames = 0;
            while (frames < m_postponeDestructionFrames)
            {
                frames++;
                yield return new WaitForEndOfFrame();
            }
            foreach (var g in m_destroyGameObjects)
                g.TryDespawnOrDestroy();
        }

        [UsedImplicitly]
        public void Clear() => m_destroyGameObjects.Clear();
    }
}
