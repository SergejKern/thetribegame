﻿using System.Collections.Generic;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for reverting materials, memorizes material changes
    /// [AC] = Action Component
    /// </summary>
    public class MaterialMemoryAC : MonoBehaviour, IMemorizeMaterials
    {
        public List<MaterialMemory> Memory { get; } = new List<MaterialMemory>();
        public void Revert()
        {
            MaterialExchange.Revert(Memory);
            Memory.Clear();
        }
    }
}
