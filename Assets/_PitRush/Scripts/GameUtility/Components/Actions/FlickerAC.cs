
using System.Linq;
using GameUtility.Coroutines;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for flickering
    /// [AC] = Action Component
    /// </summary>
    public class FlickerAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] FlickerData m_flickerData;
        [SerializeField] GameObject m_flickerModel;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void Awake()
        {
            if (m_flickerModel == null)
                return;

            var duration = m_flickerData.FlickerDuration;
            var frequency = m_flickerData.Frequency;

            m_flickerData = new FlickerData()
            {
                Renderer = m_flickerModel.GetComponentsInChildren<Renderer>().ToList(),
                FlickerDuration = duration,
                Frequency = frequency
            };
        }

        Coroutine m_routine;
        public void StartFlicker()
        {
            if (m_routine != null)
                StopCoroutine(m_routine);
            m_routine = StartCoroutine(FlickerRoutine.Flicker(m_flickerData));
        }

        public void StopFlicker()
        {
            if (m_routine != null)
                StopCoroutine(m_routine);
            foreach (var r in m_flickerData.Renderer)
                r.enabled = true;
        }

        public void SetModel(GameObject model) =>
            m_flickerModel = model;
    }
}