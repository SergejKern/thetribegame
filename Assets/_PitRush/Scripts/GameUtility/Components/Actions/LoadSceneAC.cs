﻿using System.Collections;
using Core.Unity.Extensions;
using Core.Unity.Types;
using GameUtility.Feature.Loading;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for loading scenes
    /// [AC] = Action Component
    /// </summary>
    public class LoadSceneAC : MonoBehaviour
    {
        enum AutoTrigger
        {
            // ReSharper disable once UnusedMember.Local
            None,
            Awake,
            Start
        };

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] SceneReference m_scene;

        // [FormerlySerializedAs("LoadScene")] [SerializeField] string m_loadScene;
        [FormerlySerializedAs("OnSceneLoaded")] [SerializeField] UnityEvent m_onSceneLoaded;

        [SerializeField] LoadSceneMode m_mode = LoadSceneMode.Additive;
        [SerializeField] AutoTrigger m_autoTrigger = AutoTrigger.Start;
        [SerializeField] bool m_autoTriggerLoadActivated;
        [SerializeField] LoadOperationID m_loadID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        AsyncOperation m_asyncOperation;
        //GameObject m_sceneGameObject;
        
        void Awake()
        {
            if (m_autoTrigger != AutoTrigger.Awake)
                return;
            StartCoroutine(LoadAsync(m_autoTriggerLoadActivated));
        }

        void Start()
        {
            if (m_autoTrigger!= AutoTrigger.Start)
                return;
            StartCoroutine(LoadAsync(m_autoTriggerLoadActivated));
        }

        [UsedImplicitly]
        public void LoadScene() => LoadScene(true);
        void LoadScene(bool activated) => StartCoroutine(LoadAsync(activated));

        IEnumerator LoadAsync(bool activated)
        {
            LoadOperations.StartLoading(m_loadID);
            if (LoadingCanvas.Instance != null)
            {
                while (LoadingCanvas.Instance.IsInTransition)
                    yield return null;
            }

            if (m_asyncOperation != null)
            {
                m_asyncOperation.allowSceneActivation = activated;
                yield break;
            }

            if (m_mode == LoadSceneMode.Single)
            {
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
                yield return null;
            }
            m_asyncOperation = SceneManager.LoadSceneAsync(m_scene.SceneName, m_mode);
            m_asyncOperation.allowSceneActivation = activated;

            while (!m_asyncOperation.isDone)
                yield return null;

            //var scene = SceneManager.GetSceneByName(m_scene.SceneName);
            //m_sceneGameObject = SceneRoot.GetSceneRoot(scene, out _);

            LoadOperations.LoadingDone(m_loadID);

            m_onSceneLoaded.Invoke();

            if (m_mode == LoadSceneMode.Single)
                gameObject.DestroyEx();
        }

    }
}
