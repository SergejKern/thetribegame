﻿using System.Collections.Generic;
using Core.Extensions;
using GameUtility.Coroutines;
using GameUtility.Data.Visual;
using JetBrains.Annotations;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for animating material color
    /// [AC] = Action Component
    /// </summary>
    public class AnimateMaterialColorFromToAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] MaterialFromToColorMap[] m_materialColorMapOffOn;

        [SerializeField] float m_onDuration;
        [SerializeField] float m_offDuration;
        [SerializeField] Renderer[] m_renderer;

        [SerializeField] bool m_restoreMemoryOnDisable;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_colorized;

        IMemorizeMaterials m_memoryComp;
        readonly List<MaterialMemory> m_memory = new List<MaterialMemory>();
        List<MaterialMemory> Memory => m_memoryComp != null ? m_memoryComp.Memory : m_memory;

        MaterialColorMap[] m_onColorMap;
        MaterialColorMap[] m_offColorMap;

        MaterialColorFromToAnimData m_fromToMap;

        void Awake()
        {
            m_memoryComp = GetComponent<IMemorizeMaterials>();
            InitFromToMap();
        }
        void InitFromToMap()
        {
            if (m_materialColorMapOffOn.IsNullOrEmpty())
            {
                Debug.LogError("No MaterialColorMap setup!");
                return;
            }

            m_fromToMap = new MaterialColorFromToAnimData()
            {
                Duration = m_onDuration,
                Revert = false,
                Renderer = m_renderer,
                ColorMapData = new MaterialFromToColorMap[m_materialColorMapOffOn.Length]
            };

            m_onColorMap = new MaterialColorMap[m_materialColorMapOffOn.Length];
            m_offColorMap = new MaterialColorMap[m_materialColorMapOffOn.Length];
            for (var i = 0; i < m_materialColorMapOffOn.Length; i++)
            {
                m_offColorMap[i].Material = m_materialColorMapOffOn[i].Material;
                m_onColorMap[i].Material = m_materialColorMapOffOn[i].Material;
                m_offColorMap[i].ColorKey = m_materialColorMapOffOn[i].ColorKey;
                m_onColorMap[i].ColorKey = m_materialColorMapOffOn[i].ColorKey;
                m_offColorMap[i].Color = m_materialColorMapOffOn[i].FromColor;
                m_onColorMap[i].Color = m_materialColorMapOffOn[i].ToColor;
            }
        }

        Coroutine m_animateMaterialCoroutine;

        [UsedImplicitly]
        public void ColorizeOn() => Colorize(m_onColorMap, m_onDuration);

        [UsedImplicitly]
        public void ColorizeOff() => Colorize(m_offColorMap, m_offDuration);

        void Colorize(MaterialColorMap[] colorMap, float duration)
        {
            m_colorized = true;
            m_animateMaterialCoroutine = StartCoroutine(AnimateMaterialColorRoutine.Animate(new MaterialColorAnimData()
            {
                ColorMapData = new MaterialColorizeData()
                {
                    MaterialColorMap = colorMap,
                    Renderer = m_renderer
                },
                Duration = duration,
                Revert = false
            }, m_fromToMap, Memory));
        }


        [UsedImplicitly]
        public void RestoreMemory()
        {
            if (!m_colorized)
                return;
            m_colorized = false;
            if (m_animateMaterialCoroutine!= null)
                StopCoroutine(m_animateMaterialCoroutine);
            MaterialExchange.Revert(Memory);
            Memory.Clear();
        }

        void OnDisable()
        {
            if (m_colorized && m_restoreMemoryOnDisable)
                RestoreMemory();
        }
    }
}
