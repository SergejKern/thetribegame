﻿using JetBrains.Annotations;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for enabling particle emission
    /// [AC] = Action Component
    /// </summary>
    public class ParticleSystemsEmissionAC : MonoBehaviour
    {
        ParticleSystem[] m_particleSystems;

        void Awake() => m_particleSystems = GetComponentsInChildren<ParticleSystem>();

        [UsedImplicitly]
        public void EnableEmissions(bool enable)
        {
            foreach (var ps in m_particleSystems)
            {
                var e = ps.emission;
                e.enabled = enable;
            }
        }
    }
}
