﻿using Core.Unity.Extensions;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for creating rigidbody
    /// [AC] = Action Component
    /// </summary>
    public class CreateRigidbodyAC : MonoBehaviour
    {
        public float Mass;
        public float Drag;

        public void CreateRigidbody()
        {
            if (gameObject.TryGetComponent<Rigidbody>(out _))
                return;

            var r = gameObject.AddComponent<Rigidbody>();
            r.mass = Mass;
            r.drag = Drag;
        }

        public void DestroyRigidbody()
        {
            if (gameObject.TryGetComponent<Rigidbody>(out var r))
                return;
            r.DestroyEx();
        }

    }
}
