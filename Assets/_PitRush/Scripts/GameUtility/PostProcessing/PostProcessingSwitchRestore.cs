using Core.Unity.Extensions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Serialization;

namespace GameUtility.PostProcessing
{
    public class PostProcessingSwitchRestore : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("switchProfile")] [SerializeField] PostProcessProfile m_switchProfile;
        [FormerlySerializedAs("restoreTime")] [SerializeField] float m_restoreTime;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_time = -1.0f;

        PostProcessVolume m_volume;
        [UsedImplicitly]
        public void SwitchAndRestore()
        {
            if (!enabled)
                return;
            if (m_switchProfile == null)
                return;

            if (m_volume == null)
            {
                m_volume = PostProcessingProfileSwitchUtil.Switch(m_switchProfile);
            }

            m_time = m_restoreTime;
        }

        void Update()
        {
            if (m_volume == null)
                return;
            m_time -= Time.deltaTime;
            var w = Mathf.Clamp01(m_time / m_restoreTime);
            m_volume.weight = w;
            if (!(m_time < 0.0f))
                return;

            // todo 1: deactivate instead
            m_volume.DestroyEx();
            m_volume = null;
        }
    }
}