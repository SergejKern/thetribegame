﻿using System.Collections;
using UnityEngine;

namespace GameUtility.Coroutines
{
    public static class VelocityRoutine
    {
        public struct MoveData
        {
            public Rigidbody Body;
            public Vector3 Direction;
            public float Duration;
            public float Distance;
            public AnimationCurve Curve;
        }

        public struct VelocityData
        {
            public Rigidbody Body;
            public Vector3 Direction;
            public float Duration;
            public float VelocityFactor;
            public AnimationCurve Curve;
        }

        /// <summary>
        /// Coroutine used to apply move over time
        /// </summary>
        public static IEnumerator Move(MoveData data)
        {
            if (data.Body == null)
                yield break;

            var body = data.Body;
            var time = 0f;
            var traveledDistance = 0f;
            while (time < data.Duration)
            {
                time += Time.deltaTime;
                var i = Mathf.Clamp01(time / data.Duration);
                var amount = Mathf.Clamp01(data.Curve.Evaluate(i));
                var newTraveledDistance = amount * data.Distance;
                var curTravel = newTraveledDistance - traveledDistance;
                traveledDistance = newTraveledDistance;
                body.MovePosition(body.position + curTravel * data.Direction);
                yield return null;
            }
        }

        /// <summary>
        /// Coroutine used to apply velocity over time
        /// </summary>
        public static IEnumerator Velocity(VelocityData data)
        {
            if (data.Body == null)
                yield break;

            var body = data.Body;
            var time = 0f;
            while (time < data.Duration)
            {
                time += Time.deltaTime;
                var i = Mathf.Clamp01(time / data.Duration);
                var force = data.Curve.Evaluate(i);
                var velocity = data.VelocityFactor * Mathf.Clamp01(force);
                body.velocity = velocity * data.Direction;
                yield return null;
            }
        }
    }
}