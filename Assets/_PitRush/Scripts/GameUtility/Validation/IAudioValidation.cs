﻿namespace GameUtility.Validation
{
    public interface IAudioValidation
    {
        bool IsAudioValid(out string error);
    }
#if PLAYMAKER
    public interface IAudioValidationFSM
    {
        bool IsAudioValid(PlayMakerFSM fsm, string state, out string error);
    }
#endif
}
