﻿using Core.Events;
using UnityEngine;

namespace GameUtility.PooledEventSpawning
{
    public struct PrefabSpawnedEvent
    {
        public readonly GameObject SpawnedObject;
        public readonly GameObject FromPrefab;

        PrefabSpawnedEvent(GameObject spawnedObject, GameObject fromPrefab)
        {
            SpawnedObject = spawnedObject;
            FromPrefab = fromPrefab;
        }

        public static void Trigger(GameObject spawnedObject, GameObject fromPrefab) => Trigger(new PrefabSpawnedEvent(spawnedObject, fromPrefab));
        static void Trigger(PrefabSpawnedEvent e) => EventMessenger.TriggerEvent(e);
    }
}
