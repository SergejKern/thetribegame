﻿using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using GameUtility.Components;
using UnityEngine;

namespace GameUtility.Config
{
    public class AudioClipAsset : ScriptableObject, IAudioAsset
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AudioClip m_clip;
        [SerializeField] float m_volume;
#pragma warning restore 0649 // wrong warnings for SerializeField

        static GameObject m_audioPrefab;

        AudioSource m_lastSource;

        public bool IsPlaying() => m_lastSource != null && m_lastSource.isPlaying;

        public void Play()
        {
            var audio = GetAudioInstance();
            PlayOnAudioSource(audio);
        }

        public void Play(Transform t)
        {
            var audio = GetAudioInstance();
            audio.transform.position = t.position;
            PlayOnAudioSource(audio);
        }

        public void Play(GameObject g) => Play(g.transform);

        void PlayOnAudioSource(GameObject audioSource)
        {
            if (audioSource.TryGetComponent(out m_lastSource))
            {
                m_lastSource.clip = m_clip;
                m_lastSource.volume = m_volume;
                m_lastSource.Play();
            }

            if (audioSource.TryGetComponent<Despawner>(out var despawner))
                despawner.SetSeconds(m_clip.length);
        }

        static GameObject GetAudioInstance()
        {
            if (m_audioPrefab == null)
                InitPrefab();
            var audio = m_audioPrefab.GetPooledInstance();
            return audio;
        }

        public void Stop()
        {
            if (m_lastSource != null)
                m_lastSource.Stop();
        }
        public void Stop(Transform t) => Stop();
        public void Stop(GameObject g) => Stop();

        public bool HasParameter(string parameter) => false;
        public void SetParameter(string parameter, float value) { }

        static void InitPrefab()
        {
            m_audioPrefab = new GameObject("AudioPrefab", typeof(AudioSource), typeof(Despawner));
            m_audioPrefab.gameObject.SetActive(false);
        }
    }
}
