﻿using AK.Wwise;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;

namespace GameUtility.Config
{
    [CreateAssetMenu(menuName = "Game/WwiseEventAsset")]
    public class WwiseEventAsset : ScriptableObject, IAudioAsset
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AK.Wwise.Event m_event;
        [SerializeField] CallbackFlags m_flags;
#pragma warning restore 0649 // wrong warnings for SerializeField

        static GameObject m_audioPrefab;

        GameObject m_audioInstance;

        //GameObject m_lastSourceObject;
        bool m_lastSourceIsPlaying;
        //uint m_lastId;
        //AudioSource m_lastSource;

        public bool IsPlaying() => m_lastSourceIsPlaying;

        public void Play()
        {
            if (m_audioPrefab == null)
                InitPrefab();
            if (m_audioInstance == null)
                m_audioInstance = m_audioPrefab.GetPooledInstance();
            //Debug.Log($"Play {m_event.Name} on audio object {m_audioInstance.GetInstanceID()}");
            Play(m_audioInstance);
        }
        public void Play(Transform t) => Play(t.gameObject);
        public void Play(GameObject g)
        {
            m_lastSourceIsPlaying = true;
            var lastSourceObject = g;
            //m_lastId = 
            m_event.Post(lastSourceObject, m_flags, Callback);
        }

        public void Stop(Transform t)=> Stop(t.gameObject);
        public void Stop(GameObject g) => m_event.Stop(g);
        public void Stop()
        {
            if (m_audioInstance == null)
                return;
            //Debug.Log($"Stop {m_event.Name} on audio object {m_audioInstance.GetInstanceID()}");
            Stop(m_audioInstance);
        }

        public bool HasParameter(string parameter) => false;

        public void SetParameter(string parameter, float value) { }

        // ReSharper disable once FlagArgument
        void Callback(object in_cookie, AkCallbackType in_type, AkCallbackInfo in_info)
        {
            if (in_type == AkCallbackType.AK_EndOfEvent)
                m_lastSourceIsPlaying = false;
        }

        static void InitPrefab()
        {
            m_audioPrefab = new GameObject("AudioPrefab");
            m_audioPrefab.gameObject.SetActive(false);
        }
    }
}
