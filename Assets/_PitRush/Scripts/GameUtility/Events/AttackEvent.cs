using Core.Events;
using UnityEngine;

namespace GameUtility.Events
{
    public struct AttackEvent
    {
        public readonly GameObject Originator;
        AttackEvent(GameObject originator) => Originator = originator;
        public static void Trigger(GameObject originator) => Trigger(new AttackEvent(originator));
        static void Trigger(AttackEvent e) => EventMessenger.TriggerEvent(e);
    }
}
