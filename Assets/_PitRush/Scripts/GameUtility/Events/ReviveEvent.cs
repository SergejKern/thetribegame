using Core.Events;
using UnityEngine;

namespace GameUtility.Events
{
    public struct ReviveEvent
    {
        // ReSharper disable once NotAccessedField.Global, MemberCanBePrivate.Global
        public readonly GameObject RevivedActor;
        public readonly GameObject Revivor;

        ReviveEvent(GameObject revivedActor, GameObject revivor)
        {
            RevivedActor = revivedActor;
            Revivor = revivor;
        }

        public static void Trigger(GameObject revivedActor, GameObject revivor) => Trigger(new ReviveEvent(revivedActor, revivor));
        static void Trigger(ReviveEvent e) => EventMessenger.TriggerEvent(e);
    }
}
