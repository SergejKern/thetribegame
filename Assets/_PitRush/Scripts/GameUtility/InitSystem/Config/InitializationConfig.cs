﻿using System;
using Core.Unity.Attribute;
using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.InitSystem.Config
{
    public abstract class InitializationConfig<TComponent, TData> : ScriptableObject, IInitializationConfig 
        where TData : struct 
        where TComponent : Component, IInitDataComponent<TData>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [CopyPaste, SerializeField] TData m_data;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public Type ComponentType => typeof(TComponent);
        public void InitializeComponent(Component component) => InitializeComponent(component as TComponent);
        void InitializeComponent(TComponent component) => component.InitializeWithData(m_data);
        public void InitDataFrom(Component component) => ((TComponent) component).GetData(out m_data);
    }

    public interface IInitializationConfig
    {
        Type ComponentType { get; }
        void InitializeComponent(Component component);
        void InitDataFrom(Component component);
    }

    [Serializable]
    public class RefIInitializationConfig: InterfaceContainer<IInitializationConfig> { }
}
