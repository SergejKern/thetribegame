﻿using System.Collections.Generic;
using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.InitSystem.Config
{
    [CreateAssetMenu(menuName = "Game/SpawnInitConfig")]
    public class SpawnInitConfig : ScriptableObject
    {
        public PrefabData PrefabDat;
        public GameObject Prefab => PrefabDat.Prefab;
        public List<RefIInitializationConfig> Configs = new List<RefIInitializationConfig>();
    }
}