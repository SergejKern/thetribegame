#define USE_INTERPOLATION

//
// By Anomalous Underdog, 2011
//
// ReSharper disable CommentTypo
// Based on code made by Forest Johnson (Yoggy) and xyber
// ReSharper enable CommentTypo

using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;
using UnityEngine.Rendering;
using GameUtility.Interface;
using JetBrains.Annotations;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
#endif

namespace GameUtility.MeleeWeaponTrail
{
    public class MeleeWeaponTrail : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warning
        [SerializeField]
        bool m_emit = true;

        [SerializeField]
        Material m_material;

        [SerializeField]
        float m_lifeTime = 1.0f;

        [SerializeField]
        Color[] m_colors;

        [SerializeField]
        float[] m_sizes;

        [SerializeField]
        float m_minVertexDistance = 0.1f;
        [SerializeField]
        float m_maxVertexDistance = 10.0f;

        float m_minVertexDistanceSqr;
        float m_maxVertexDistanceSqr;

        [SerializeField]
        float m_maxAngle = 3.00f;

#if USE_INTERPOLATION
        [SerializeField]
        int m_subdivisions = 4;

        [SerializeField] int m_maxSmoothPoints = 4;
#endif

        [SerializeField]
        Transform m_base;
        [SerializeField]
        Transform m_tip;

        [SerializeField] GameObject m_weaponGo;
        [SerializeField] MeleeWeaponTrailConfig m_config;
        [SerializeField] bool m_useConfigOnly;

#pragma warning restore 0649 // wrong warning

        readonly List<Point> m_points = new List<Point>();
#if USE_INTERPOLATION
        readonly List<Point> m_smoothedPoints = new List<Point>();
#endif

        internal GameObject m_trailObject;
        MeshRenderer m_meshRenderer;
        Mesh m_trailMesh;

        Vector3 m_lastTipPos;
        Vector3 m_lastBasePos;

        Animator m_animator;
        ITimeMultiplier m_timeMultiplier;

        static GameObject m_meleeWeaponPrefab;
        const string k_trailName = "Trail";
        public MeleeWeaponTrailConfig Config => m_config;

#if UNITY_EDITOR
        [NonSerialized] // ReSharper disable once InconsistentNaming
        public bool Editor_IgnoreConfig;
#endif

        // ReSharper disable once ConvertToAutoPropertyWhenPossible
        [UsedImplicitly]
        public bool Emit
        {
            get => m_emit;
            set => m_emit = value;
        }

        [Serializable]
        public struct Point
        {
            public float Time;
            public Vector3 BasePosition;
            public Vector3 TipPosition;
        }

        //public Color[] GetColors() => m_colors;
        public void SetColors(Color[] colors) => m_colors = colors;
        public void SetMaterial(Material m)
        {
            m_material = m;
            if (m_meshRenderer != null)
                m_meshRenderer.material = m_material;
        }

        void OnEnable() => Init();

        void OnDisable()
        {
            if (m_trailMesh != null)
                Destroy(m_trailMesh);
            m_trailObject.TryDespawnOrDestroy();
        }

        void Init()
        {
            m_lastTipPos = m_tip.position;
            m_lastBasePos = m_base.position;

            if (m_meleeWeaponPrefab == null)
                InitPrefab();

            var trailObject = m_meleeWeaponPrefab.GetPooledInstance(Vector3.zero,
                Quaternion.identity,
                Vector3.one,
                k_trailName);
            //m_trailObject.transform.SetParent(null, true);
            InitTrailObject(trailObject);
        }

        public void OnActorChanged()
        {
            m_animator = null;
            var weapon = m_weaponGo.GetComponent<IWeapon>();
            if (weapon == null)
                return;
            var actor = weapon.Actor;
            if (actor == null)
                return;
            actor.GetComponent<IProvider<IControlCentre>>().Get(out var cc);
            m_timeMultiplier = actor.GetComponent<ITimeMultiplier>();
            m_animator = cc.Animator;
            //m_animator = weapon.Actor;
        }

        internal void InitTrailObject(GameObject trailObject)
        {
            m_trailObject = trailObject;
            var filter = m_trailObject.GetComponent<MeshFilter>();
            m_meshRenderer = m_trailObject.GetComponent<MeshRenderer>();
            m_meshRenderer.material = m_material;

            m_trailMesh = new Mesh();
            filter.mesh = m_trailMesh;
            m_meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
            m_minVertexDistanceSqr = m_minVertexDistance * m_minVertexDistance;
            m_maxVertexDistanceSqr = m_maxVertexDistance * m_maxVertexDistance;
        }

        void Update() => Update(Time.deltaTime * m_timeMultiplier?.SpeedFactor ?? 1f);

        internal void Update(float delta)
        {
            //AutoDestructIfNeeded();

            // if we have moved enough, create a new vertex and make sure we rebuild the mesh
            var tipSqr = (m_lastTipPos - m_tip.position).sqrMagnitude;
            var baseSqr = (m_lastBasePos - m_base.position).sqrMagnitude;

            CleanupOldPoints();

            var theDistanceSqr = tipSqr + baseSqr;
            if (m_emit)
                DoEmit(theDistanceSqr);
            
#if USE_INTERPOLATION
            var pointsToUse = m_smoothedPoints;
#else
		    var pointsToUse = m_points;
#endif

            if (pointsToUse.Count <= 1)
                return;
            
            var newVertices = new Vector3[pointsToUse.Count * 2];
            var newUV = new Vector2[pointsToUse.Count * 2];
            var newTriangles = new int[(pointsToUse.Count - 1) * 6];
            var newColors = new Color[pointsToUse.Count * 2];

            UpdateMesh(pointsToUse, newVertices, newColors, newUV, newTriangles, delta);

            m_trailMesh.Clear();
            m_trailMesh.vertices = newVertices;
            m_trailMesh.colors = newColors;
            m_trailMesh.uv = newUV;
            m_trailMesh.triangles = newTriangles;
        }

        void UpdateMesh(IList<Point> pointsToUse, 
            IList<Vector3> newVertices, 
            IList<Color> newColors, 
            IList<Vector2> newUV, 
            IList<int> newTriangles,
            float delta)
        {
            for (var n = 0; n < pointsToUse.Count; ++n)
            {
                var p = pointsToUse[n];
                var time = p.Time / m_lifeTime;
                p.Time += delta;
                pointsToUse[n] = p;

                AnimateTrail(time, out var color, out var size);

                var lineDirection = p.TipPosition - p.BasePosition;

                newVertices[n * 2] = p.BasePosition - (lineDirection * (size * 0.5f));
                newVertices[(n * 2) + 1] = p.TipPosition + (lineDirection * (size * 0.5f));

                newColors[n * 2] = newColors[(n * 2) + 1] = color;

                var uvRatio = (float) n / pointsToUse.Count;
                newUV[n * 2] = new Vector2(uvRatio, 0);
                newUV[(n * 2) + 1] = new Vector2(uvRatio, 1);

                if (n <= 0)
                    continue;

                newTriangles[(n - 1) * 6] = (n * 2) - 2;
                newTriangles[((n - 1) * 6) + 1] = (n * 2) - 1;
                newTriangles[((n - 1) * 6) + 2] = n * 2;

                newTriangles[((n - 1) * 6) + 3] = (n * 2) + 1;
                newTriangles[((n - 1) * 6) + 4] = n * 2;
                newTriangles[((n - 1) * 6) + 5] = (n * 2) - 1;
            }
        }

        void AnimateTrail(float time, out Color color, out float size)
        {
            AnimateColor(time, out color);
            AnimateSize(time, out size);
        }

        void AnimateSize(float time, out float size)
        {
            size = 0f;
            if (m_sizes == null || m_sizes.Length <= 0)
                return;

            var sizeTime = time * (m_sizes.Length - 1);
            var min = Mathf.Clamp(Mathf.Floor(sizeTime), 0, m_sizes.Length - 1);
            var max = Mathf.Clamp(Mathf.Ceil(sizeTime), 1, m_sizes.Length - 1);
            var lerp = Mathf.InverseLerp(min, max, sizeTime);
            size = Mathf.Lerp(m_sizes[(int) min], m_sizes[(int) max], lerp);
        }

        void AnimateColor(float time, out Color color)
        {
            color = Color.Lerp(Color.white, Color.clear, time);
            if (m_colors == null || m_colors.Length <= 0)
                return;

            var colorTime = time * (m_colors.Length - 1);
            var min = Mathf.Clamp(Mathf.Floor(colorTime), 0, m_colors.Length - 1);
            var max = Mathf.Clamp(Mathf.Ceil(colorTime), 1, m_colors.Length - 1);
            var lerp = Mathf.InverseLerp(min, max, colorTime);
            color = Color.Lerp(m_colors[(int) min], m_colors[(int) max], lerp);
        }

        void CleanupOldPoints()
        {
            RemoveOldPoints(m_points);
            if (m_points.Count == 0)
                m_trailMesh.Clear();
            
#if USE_INTERPOLATION
            RemoveOldPoints(m_smoothedPoints);
            if (m_smoothedPoints.Count == 0)
                m_trailMesh.Clear();
#endif
        }

        //void AutoDestructIfNeeded()
        //{
        //    if (m_emit || m_points.Count!= 0)
        //        return;
        //    if (!m_autoDestruct)
        //        return;
        //    Destroy(m_trailObject);
        //    Destroy(gameObject);
        //}

        void DoEmit(float theDistanceSqr)
        {
            if (!(theDistanceSqr > m_minVertexDistanceSqr) || !ShouldMake(theDistanceSqr))
            {
                UpdateBaseAndTip();
                return;
            }

            if (TryGetBakedTrail())
                return;
            if (m_useConfigOnly && m_animator != null)
                return;
            AddPoint();

#if USE_INTERPOLATION
            // we use 2 control points for the smoothing minimum
            if (m_points.Count < 2)
                return;

            var points = Mathf.Min(m_points.Count, m_maxSmoothPoints);
            var tipPoints = new Vector3[points];
            var i = 0; var j = points;
            for (; i < points; i++, j--)
                tipPoints[i] = m_points[m_points.Count - j].TipPosition;

            //IEnumerable<Vector3> smoothTip = Interpolate.NewBezier(Interpolate.Ease(Interpolate.EaseType.Linear), tipPoints, subdivisions);
            var smoothTip = Interpolate.NewCatmullRom(tipPoints, m_subdivisions, false);

            var basePoints = new Vector3[points];
            i = 0; j = points;
            for (; i < points; i++, j--)
                basePoints[i] = m_points[m_points.Count - j].BasePosition;

            //IEnumerable<Vector3> smoothBase = Interpolate.NewBezier(Interpolate.Ease(Interpolate.EaseType.Linear), basePoints, subdivisions);
            var smoothBase = Interpolate.NewCatmullRom(basePoints, m_subdivisions, false);

            var smoothTipList = new List<Vector3>(smoothTip);
            var smoothBaseList = new List<Vector3>(smoothBase);

            var firstTime = m_points[m_points.Count - points].Time;
            var secondTime = m_points[m_points.Count - 1].Time;

            UpdateSmoothTipList(smoothTipList, smoothBaseList, firstTime, secondTime);
#endif
        }

        bool TryGetBakedTrail()
        {
#if UNITY_EDITOR
            if (Editor_IgnoreConfig)
                return false;
#endif
            if (m_animator == null || m_config == null)
                return false;
            var stateInfo = m_animator.GetCurrentAnimatorStateInfo(1);
            var usingBakedTrail = m_config.TryGetPoints(stateInfo.shortNameHash, stateInfo.normalizedTime, m_smoothedPoints, transform);
            //if (!usingBakedTrail)
            //    Debug.LogWarning($"No Baked Trail was used! {stateInfo.shortNameHash} {stateInfo.normalizedTime} {transform}");
            
            return usingBakedTrail;
        }

        void UpdateSmoothTipList(IReadOnlyList<Vector3> smoothTipList, 
            IReadOnlyList<Vector3> smoothBaseList, float firstTime, float secondTime)
        {
            for (var n = 0; n < smoothTipList.Count; ++n)
            {
                var idx = m_smoothedPoints.Count - (smoothTipList.Count - n);
                // there are moments when the _smoothedPoints are lesser
                // than what is required, when elements from it are removed
                if (idx <= -1 || idx >= m_smoothedPoints.Count)
                    continue;
                var sp = new Point
                {
                    BasePosition = smoothBaseList[n],
                    TipPosition = smoothTipList[n],
                    Time = Mathf.Lerp(firstTime, secondTime, (float) n / smoothTipList.Count)
                };
                m_smoothedPoints[idx] = sp;
            }
        }

        void AddPoint()
        {
            m_lastTipPos = m_tip.position;
            m_lastBasePos = m_base.position;
            var p = new Point { BasePosition = m_lastBasePos, TipPosition = m_lastTipPos, Time = 0f };
            m_points.Add(p);

#if USE_INTERPOLATION
            if (m_points.Count == 1)
                m_smoothedPoints.Add(p);
            else if (m_points.Count > 1)
            {
                // add 1+subdivisions for every possible pair in the _points
                for (var n = 0; n < 1 + m_subdivisions; ++n)
                    m_smoothedPoints.Add(p);
            }
#endif
        }

        bool ShouldMake(float theDistanceSqr)
        {
            var make = false;
            if (m_points.Count < 3)
                make = true;
            else
            {
                //Vector3 l1 = _points[_points.Count - 2].basePosition - _points[_points.Count - 3].basePosition;
                //Vector3 l2 = _points[_points.Count - 1].basePosition - _points[_points.Count - 2].basePosition;
                var l1 = m_points[m_points.Count - 2].TipPosition - m_points[m_points.Count - 3].TipPosition;
                var l2 = m_points[m_points.Count - 1].TipPosition - m_points[m_points.Count - 2].TipPosition;
                if (Vector3.Angle(l1, l2) > m_maxAngle || theDistanceSqr > m_maxVertexDistanceSqr)
                    make = true;
            }

            return make;
        }

        void UpdateBaseAndTip()
        {
            UpdateBaseAndTip(m_points);
#if USE_INTERPOLATION
            UpdateBaseAndTip(m_smoothedPoints);
#endif
        }

        void UpdateBaseAndTip(IList<Point> points)
        {
            if (points.Count <= 0)
                return;
            var idx = points.Count - 1;
            var p = points[idx];
            points[idx] = new Point()
            {
                BasePosition = m_base.position,
                TipPosition = m_tip.position,
                Time = p.Time
            };
        }

        void RemoveOldPoints(ICollection<Point> pointList)
        {
            var remove = pointList.Where(p => p.Time > m_lifeTime).ToList();
            foreach (var p in remove)
                pointList.Remove(p);
            //foreach (var p in pointList)
            //{
            //    // cull old points first
            //    if (Time.time - p.TimeCreated > m_lifeTime)
            //        remove.Add(p);
            //}
        }

        static void InitPrefab()
        {
            m_meleeWeaponPrefab = new GameObject("MeleeWeaponTrailPrefab", typeof(MeshFilter), typeof(MeshRenderer));
            m_meleeWeaponPrefab.gameObject.SetActive(false);
        }

        public List<Point> GetPoints()
        {
#if USE_INTERPOLATION
            var pointsToUse = m_smoothedPoints;
#else
		    var pointsToUse = m_points;
#endif
            return pointsToUse;
        }
    }

#if UNITY_EDITOR
        [CustomEditor(typeof(MeleeWeaponTrail))]
    public class MeleeWeaponTrailEditor : Editor
    {
        // ReSharper disable once InconsistentNaming, MemberCanBePrivate.Global
        public new MeleeWeaponTrail target => base.target as MeleeWeaponTrail;
        static GameObject m_preview;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            SimulateGUI();
        }

        void SimulateGUI()
        {
            if (!GUILayout.Button("Simulate"))
                return;
            if (m_preview == null)
                InitPrefabInEditor();
            if (target.m_trailObject == null)
            {
                target.InitTrailObject(m_preview);
                var scene = PrefabStageUtility.GetCurrentPrefabStage().scene;
                m_preview.transform.SetParent(scene.GetRootGameObjects()[0].transform, true);
            }
            target.Update(0.033f);
        }

        static void InitPrefabInEditor()
        {
            m_preview = new GameObject("MeleeWeaponTrailPreview", typeof(MeshFilter), typeof(MeshRenderer))
            {
                hideFlags = HideFlags.DontSave
            };
        }
    }
#endif
}
