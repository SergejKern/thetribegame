﻿using System;
using Core.Unity.Attribute;
using Core.Unity.Types;
using GameUtility.InitSystem;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Energy
{
    public abstract class EnergyComponent : MonoBehaviour, IEnergyComponent,
        IInitDataComponent<EnergyConfigData>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] EnergyID m_id;
        [CopyPaste]
        [SerializeField] protected EnergyConfigData m_data = EnergyConfigData.Default;

        [Header("Events")]
        [SerializeField]
        UnityEvent m_onDamaged;
        [SerializeField]
        UnityEvent m_onDepleted;
        [SerializeField]
        UnityFloatEvent m_onChanged;

        [SerializeField]
        UnityEvent m_onInvulnerable;
        [SerializeField]
        UnityEvent m_onVulnerable;

        [SerializeField] RefITimeMultiplier m_timeMultiplier;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public EnergyID EnergyType => m_id;
        public float Energy
        {
            get => m_energy;
            private set
            {
                if (Math.Abs(m_energy - value) < float.Epsilon)
                    return;
                m_energy = value;
                if (!m_data.DiscreteValues)
                {
                    EnergyChanged();
                    return;
                }
                var discrete = Mathf.FloorToInt(m_energy);
                if (discrete == m_discreteValue)
                    return;
                m_discreteValue = discrete;
                EnergyChanged();
            }
        }

        public float MaxEnergy => m_data.MaxEnergy;
        public bool IsInvulnerable => m_invulnerableTimer > 0;

        // todo 1: for modification and buffs we need to know mod-source and time to be able to restore states
        // todo 1: create another specialized component for this?
        bool m_regenerationBlocked;
        float m_invulnerableTimer;
        float m_rechargeTimer;

        float m_energy;
        int m_discreteValue;

        public bool CanUse(float amount)
        {
            if (Energy <= 0)
                return false;
            if (m_data.CanOvershoot)
                return true;
            return Energy >= amount;
        }

        // ReSharper disable once MethodNameNotMeaningful
        public virtual void Use(float amount)
        {
            if (Energy <= 0)
                return;
            Energy -= amount;
            if (Energy <= 0)
                m_onDepleted.Invoke();

            ResetRechargeTimer();
        }

        protected void ResetRechargeTimer() => m_rechargeTimer = m_data.TimeBeforeRecharging;

        // ReSharper disable once VirtualMemberNeverOverridden.Global
        public virtual void SetVulnerable() => m_invulnerableTimer = 0f;
        public virtual void SetInvulnerable(float seconds)
        {
            m_invulnerableTimer = Mathf.Max(m_invulnerableTimer, seconds);
            m_onInvulnerable.Invoke();
        }

        public virtual void Damage(float amount)
        {
            if (Energy <= 0)
                return;
            if (IsInvulnerable)
                return;

            SetInvulnerable(m_data.InvulnerableTimeAfterDamage);
            Use(amount);

            m_onDamaged.Invoke();
        }

        public virtual void Regenerate(float amount)
        {
            Energy = Mathf.Clamp(Energy + amount, 0, MaxEnergy);
        }

        public void Deplete() => Use(Energy);

        public void Reset() => Energy = m_data.MaxEnergy * m_data.InitEnergyLevelPercent;

        public void SetRegenerationBlocked(bool blocked) => m_regenerationBlocked = blocked;
        protected virtual void Awake() => Reset();

        protected virtual void Update()
        {
            UpdateRegeneration();
            UpdateInvulnerability();
        }

        void UpdateInvulnerability()
        {
            if (!IsInvulnerable)
                return;
            //Debug.Log($"invulnerable {m_invulnerableTimer}");

            m_invulnerableTimer -= m_timeMultiplier.DeltaTime;
            if (!IsInvulnerable)
                m_onVulnerable.Invoke();
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global
        protected virtual void UpdateRegeneration()
        {
            if (m_rechargeTimer > 0f)
            {
                m_rechargeTimer -= m_timeMultiplier.DeltaTime;
                return;
            }
            if (m_regenerationBlocked)
                return;
            Regenerate(m_timeMultiplier.DeltaTime * m_data.RechargeRate);
        }

        public void GetData(out EnergyConfigData data) => data = m_data;
        public void InitializeWithData(EnergyConfigData data)
        {
            m_data = data;
            Reset();
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global
        protected virtual void EnergyChanged()
        {
            if (m_data.DiscreteValues)
            {
                m_onChanged.Invoke(m_discreteValue);
                return;
            }
            m_onChanged.Invoke(Mathf.Max(0, Energy));
        }

        public void AddChangeListener(UnityAction<float> action) => m_onChanged.AddListener(action);
        public void RemoveChangeListener(UnityAction<float> action) => m_onChanged.RemoveListener(action);
    }
}
