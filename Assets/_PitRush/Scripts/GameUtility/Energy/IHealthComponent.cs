﻿namespace GameUtility.Energy
{
    public interface IHealthComponent: IEnergyComponent
    {        
        // ReSharper disable UnusedMemberInSuper.Global
        float HP { get; } // = Energy
        float MaxHP { get; } // = MaxEnergy
        void Heal(float heal); // Regenerate
        void Kill(); // Deplete
        // ReSharper restore UnusedMemberInSuper.Global
    }
}
