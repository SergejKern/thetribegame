﻿namespace GameUtility.Energy
{
    public interface IStaminaComponent: IEnergyComponent
    {
        float Stamina { get; }
        float MaxStamina { get; }
    }
}
