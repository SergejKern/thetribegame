﻿using Core.Interface;

namespace GameUtility.Data.PhysicalConfrontation
{
    public struct AttackData
    {        
        // complete range
        public float FullRangeUntilOnHit;
        public float OnHitRange;
        public float AttackDuration;
        public string AttackState;
        public AttackTypeID AttackType;
    }

    public interface IAttackData : IVariable<AttackData> { }
}
