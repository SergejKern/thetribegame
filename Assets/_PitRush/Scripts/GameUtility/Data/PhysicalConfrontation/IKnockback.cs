using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public interface IKnockback
    {
        //KnockBackRoutine.KnockBackData GetKnockBackData();
        void ApplyKnockback(Vector3 direction, float knockBackFactor, float knockBackDurationFactor, float knockBackRecoverFactor);
    }
}
