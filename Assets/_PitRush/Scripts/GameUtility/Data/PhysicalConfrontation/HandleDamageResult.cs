﻿using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public struct HandleDamageResult
    {
        public GameObject AffectedObject;

        public Vector3 DamagePos;
        public Vector2 DamageDirection;

        public float DamageDone;
        public float DamageAbsorbed;

        public CollisionMaterialID Material;
        public DamageResult Result;
    }

    public enum DamageResult
    {
        Damaged,
        Killed,
        Ignored,
        Blocked,
        Absorbed // (healed)
    }
}

