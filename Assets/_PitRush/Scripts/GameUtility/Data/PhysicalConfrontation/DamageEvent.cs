using System;
using Core.Events;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Data.PhysicalConfrontation
{
    [Serializable]
    public struct DamageEvent
    {
        public GameObject Damaged;
        public GameObject Aggressor;

        public Vector3 Direction;
        public HandleDamageResult DamageResult;

        public DamageEvent(GameObject damagedActor, GameObject aggressor,
            Vector3 direction, HandleDamageResult damageResult)
        {
            Damaged = damagedActor;
            Aggressor = aggressor;
            Direction = direction;
            DamageResult = damageResult;
        }

        public static void Trigger(DamageEvent e) => EventMessenger.TriggerEvent(e);
    }

    [Serializable]
    public class DamageUnityEvent : UnityEvent<DamageEvent> { }
}
