﻿using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public interface ICollisionMaterialComponent
    {
        Vector3 CollisionPos { get; }
        float CollisionPosCenterPercent { get; }
        CollisionMaterialID Material { get; }
    }
}
