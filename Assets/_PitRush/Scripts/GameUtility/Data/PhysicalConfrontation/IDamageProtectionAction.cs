﻿namespace GameUtility.Data.PhysicalConfrontation
{
    public interface IDamageProtectionAction
    {
        bool IsProtected { get; }
        bool IsProtectedForSeconds(float seconds);
    }
}
