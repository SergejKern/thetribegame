﻿using System;
using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.Data.TransformAttachment
{
    public interface ITransformData
    {
        Vector3 Position { get; }
        Quaternion Rotation { get; }
    }

    public interface IMutableTransformData : ITransformData
    {
        new Vector3 Position { get; set; }
        new Quaternion Rotation { get; set; }
    }

    [Serializable]
    public class RefITransformComponent : InterfaceContainer<IMutableTransformData> { }
}
