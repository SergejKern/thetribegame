﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Data.TransformAttachment
{
    /// <summary>
    /// This is just a data struct for easy use in inspector, has to be initialized properly by code when setup to use AttachmentID
    /// </summary>
    [Serializable]
    public struct AttachmentOrTransformData : ITransformData
    {
        [FormerlySerializedAs("UseLabel")] 
        public bool UseID;
        public AttachmentID ID;
        public TransformData TransformData;

        public Transform Parent => TransformData.Parent;
        public Vector3 Position => TransformData.Position;
        public Quaternion Rotation => TransformData.Rotation;
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(AttachmentOrTransformData))]
    public class AttachmentOrTransformDataDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position = EditorGUI.PrefixLabel(position, label);
            
            var rowA = new Rect(position.x, position.y, position.width, 18f);
            var buttonRect = new Rect(rowA.x, rowA.y, 20f, rowA.height);
            var restRectA = new Rect(rowA.x + 20f, rowA.y, rowA.width - 20f, rowA.height);

            var use = property.FindPropertyRelative(nameof(AttachmentOrTransformData.UseID));
            use.boolValue = GUI.Toggle(buttonRect, use.boolValue, "ID", "Button");

            //var prevWidth = EditorGUIUtility.labelWidth;
            //EditorGUIUtility.labelWidth = 0f;
            if (use.boolValue)
            {
                EditorGUI.PropertyField(restRectA, property.FindPropertyRelative(nameof(AttachmentOrTransformData.ID)),
                    GUIContent.none, true);
                return;
            }

            var resRect = new Rect(position.x, position.y+ rowA.height, position.width, position.height-rowA.height);
            var trDataProp = property.FindPropertyRelative(nameof(AttachmentOrTransformData.TransformData));
            EditorGUI.PropertyField(resRect, trDataProp);
            //EditorGUIUtility.labelWidth = prevWidth;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var defaultHeight=base.GetPropertyHeight(property, label);
            
            var use = property.FindPropertyRelative(nameof(AttachmentOrTransformData.UseID));
            if (use.boolValue)
                return defaultHeight;

            var trDataProp = property.FindPropertyRelative(nameof(AttachmentOrTransformData.TransformData));
            var height = EditorGUI.GetPropertyHeight(trDataProp, true);
            return defaultHeight + height;
        }
    }
#endif
}
