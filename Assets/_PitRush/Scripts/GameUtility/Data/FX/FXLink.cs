﻿using System;
using Core.Unity.Types.Fsm;
using GameUtility.Data.TransformAttachment;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Data.FX
{
    [Serializable]
    public struct FXLink
    {
        public FX_ID ID;
        public AttachmentOrTransformData TransformData;

        [FSMDrawer(editVariable: false)]
        public FSMGameObjectRef Ref;

        public bool DontParent;

        public bool InitialActive;
        [HideInInspector] public GameObject Instance;
        [HideInInspector] public GameObject Prefab;
    }
}