﻿using System;
using UnityEngine.Events;

namespace GameUtility.Data.FX
{
    [Serializable]
    public class FXEvent : UnityEvent<IFXComponent> { }
}