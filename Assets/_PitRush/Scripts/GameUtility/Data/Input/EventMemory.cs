﻿using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Types.Fsm;

namespace GameUtility.Data.Input
{
    public struct EventMemory
    {
        public FSMEventRef Event;
        public List<int> ValidStates;
        public bool CanInvoke => !ValidStates.IsNullOrEmpty() && ValidStates.Contains(Event.FSM.ActiveStateName.GetHashCode());
        public bool ShouldInvoke;

        public void Invoke() => Event.Invoke();
        public static EventMemory Default => new EventMemory() {ValidStates = new List<int>()};
    }
}