using System;
using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Data.Visual
{
    [Serializable]
    public struct MaterialOverrideMap
    {
        [FormerlySerializedAs("UnHighlighted")]
        public Material Default;
        [FormerlySerializedAs("Highlighted")]
        public Material Override;
    }

    [Serializable]
    public struct MaterialExchangeData
    {
        public MaterialOverrideMap[] MaterialMap;
        public Renderer[] Renderer;
    }
    [Serializable]
    public struct MaterialExchangeData2
    {
        public MaterialOverrideMap[] MaterialMap;
        public GameObject ForObject;
    }

    public struct MaterialMemory
    {
        public Renderer Renderer;
        public Material[] Material;

        //public MaterialPropertyBlock[] PropertyBlocks;
        //public List<Material> InstancedMaterials;
    }

    public interface IMemorizeMaterials
    {
        List<MaterialMemory> Memory { get; }
        void Revert();
    }

    public static class MaterialExchange
    {
        // ReSharper disable MemberCanBePrivate.Global, UnusedMember.Global
        public static void Override(GameObject obj, MaterialOverrideMap[] map, List<MaterialMemory> memory = null) => Override(new MaterialExchangeData2(){ForObject = obj, MaterialMap = map }, memory);
        public static void Override(Renderer[] r, MaterialOverrideMap[] map, List<MaterialMemory> memory = null) => Override(new MaterialExchangeData() { Renderer = r, MaterialMap = map }, memory);
        public static void Override(Renderer r, MaterialOverrideMap[] map, List<MaterialMemory> memory = null)
        {
            Arrays<Renderer>.Single[0] = r;
            Override(Arrays<Renderer>.Single, map, memory);
        }

        public static void Override(MaterialExchangeData2 data, List<MaterialMemory> memory = null)
        {
            var r = data.ForObject.GetComponentsInChildren<Renderer>();
            Override(new MaterialExchangeData()
            {
                MaterialMap = data.MaterialMap,
                Renderer = r
            }, memory);
        }

        public static void Override(MaterialExchangeData data, List<MaterialMemory> memory = null)
        {
            if (data.MaterialMap.IsNullOrEmpty())
                return;
            if (data.Renderer.IsNullOrEmpty())
                return;

            foreach (var r in data.Renderer)
            {
                if (r == null)
                    continue;
                var didOverride = false;
                var mats = r.sharedMaterials;
                for (var rendMatIdx = 0; rendMatIdx < mats.Length; rendMatIdx++)
                {
                    var idx = Array.FindIndex(data.MaterialMap, m => m.Default == mats[rendMatIdx]);
                    if (idx == -1)
                        continue;
                    didOverride = true;
                    mats[rendMatIdx] = data.MaterialMap[idx].Override;
                }

                if (!didOverride)
                    continue;
                if (memory != null && memory.FindIndex(m => m.Renderer == r) == -1)
                    memory.Add(new MaterialMemory() { Material = r.sharedMaterials, Renderer = r });
                r.sharedMaterials = mats;
            }
        }

        public static void Revert(GameObject obj, MaterialOverrideMap[] map) => Revert(new MaterialExchangeData2() { ForObject = obj, MaterialMap = map });
        public static void Revert(Renderer[] r, MaterialOverrideMap[] map) => Revert(new MaterialExchangeData() { Renderer = r, MaterialMap = map });
        public static void Revert(Renderer r, MaterialOverrideMap[] map)
        {
            Arrays<Renderer>.Single[0] = r;
            Revert(Arrays<Renderer>.Single, map);
        }

        public static void Revert(MaterialExchangeData2 data)
        {
            var r = data.ForObject.GetComponentsInChildren<Renderer>();
            Revert(new MaterialExchangeData()
            {
                MaterialMap = data.MaterialMap,
                Renderer = r
            });
        }

        public static void Revert(MaterialExchangeData data)
        {
            if (data.MaterialMap.IsNullOrEmpty())
                return;
            if (data.Renderer.IsNullOrEmpty())
                return;
            foreach (var r in data.Renderer)
            {
                var mats = r.sharedMaterials;
                var didRevert = false;

                for (var matIdx = 0; matIdx < r.sharedMaterials.Length; matIdx++)
                {
                    var idx = Array.FindIndex(data.MaterialMap, m => m.Override == mats[matIdx]);
                    if (idx == -1) 
                        continue;
                    didRevert = true;
                    mats[matIdx] = data.MaterialMap[idx].Default;
                }
                if (!didRevert)
                    continue;
                r.sharedMaterials = mats;
            }
        }

        public static void Revert(IEnumerable<MaterialMemory> mem)
        {
            foreach (var m in mem)
            {
                if (m.Renderer == null)
                    continue;
                m.Renderer.sharedMaterials = m.Material;

                //RevertPropertyBlocks(m);
                //if (m.InstancedMaterials == null)
                //    continue;
                //foreach (var i in m.InstancedMaterials)
                //    i.DestroyEx();
            }
        }

        //static void RevertPropertyBlocks(MaterialMemory m)
        //{
        //    if (m.PropertyBlocks == null) 
        //        return;
        //    for (var i = 0; i < m.PropertyBlocks.Length; i++)
        //    {
        //        var propertyBlock = m.PropertyBlocks[i];
        //        m.Renderer.SetPropertyBlock(null, i);
        //        SimplePool<MaterialPropertyBlock>.I.Return(propertyBlock);
        //    }
        //}
        // ReSharper restore MemberCanBePrivate.Global, UnusedMember.Global
    }
}