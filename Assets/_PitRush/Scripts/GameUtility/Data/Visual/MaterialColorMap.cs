using System;
using System.Collections.Generic;
using Core.Extensions;
using GameUtility.Coroutines;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameUtility.Data.Visual
{
    [Serializable]
    public struct MaterialColorMap
    {
        public Material Material;
        public string ColorKey;
        [ColorUsage(true, true)]
        public Color Color;
    }

    [Serializable]
    public struct MaterialColorizeData
    {
        public MaterialColorMap[] MaterialColorMap;
        public Renderer[] Renderer;
    }

    [Serializable]
    public struct MaterialColorizeData2
    {
        public MaterialColorMap[] MaterialColorMap;
        public GameObject ForObject;
    }

    [Serializable]
    public struct MaterialFromToColorMap
    {
        //public Material OriginalMaterial;
        public Material Material;

        public string ColorKey;
        public Color FromColor;
        public Color ToColor;
    }
    
    //[Serializable]
    //public struct MaterialFromToColorMapWithBlock
    //{
    //    public MaterialPropertyBlock PropertyBlock;
    //    public string ColorKey;
    //    public Color FromColor;
    //    public Color ToColor;
    //}

    public static class MaterialColorize
    {
        // ReSharper disable MemberCanBePrivate.Global, UnusedMember.Global
        public static void Colorize(GameObject go, MaterialColorMap map, List<MaterialMemory> memory)
        {
            var arr = Arrays<MaterialColorMap>.Single;
            arr[0] = map;
            Colorize(go, arr, memory);
        }

        public static void Colorize(GameObject go, MaterialColorMap[] colorMaps, List<MaterialMemory> memory) => 
            Colorize(new MaterialColorizeData2() { MaterialColorMap = colorMaps, ForObject = go }, memory);

        public static void Colorize(MaterialColorizeData2 data, List<MaterialMemory> memory) => 
            Colorize(new MaterialColorizeData(){
                MaterialColorMap = data.MaterialColorMap,
                Renderer = data.ForObject.GetComponentsInChildren<Renderer>()
            }, memory);

        public static void Colorize(MaterialColorizeData data, List<MaterialMemory> memory)
        {
            var rends = data.Renderer;
            foreach (var r in rends)
                ColorizeForRenderer(data, memory, r);
        }
        // ReSharper restore MemberCanBePrivate.Global, UnusedMember.Global

        static void ColorizeForRenderer(MaterialColorizeData data, List<MaterialMemory> memory, Renderer r)
        {
            var instanceProvider = GetInstanceProvider(r);
            var currentMats = r.sharedMaterials;

            var didColorize = false;
            for (var rendMatIdx = 0; rendMatIdx < currentMats.Length; rendMatIdx++)
                ColorizeForRendererMaterial(data, currentMats, rendMatIdx, instanceProvider, ref didColorize);

            if (didColorize)
                Apply(memory, r, currentMats);
        }

        static void Apply(List<MaterialMemory> memory, Renderer r, Material[] mats)
        {
            var memIdx = memory.FindIndex(m => m.Renderer == r);
            if (memIdx == -1)
                memory.Add(new MaterialMemory() { Material = r.sharedMaterials, Renderer = r } ); 
            //, InstancedMaterials = instanceMatList});
            r.sharedMaterials = mats;
        }

        static void ColorizeForRendererMaterial(MaterialColorizeData data, 
            IList<Material> currentMats, int rendMatIdx,
            MaterialInstanceProvider provider,
            ref bool didColorize)
        {
            provider.GetInstances(out var originMats, out var instanceMats);
            var mapIdx = Array.FindIndex(data.MaterialColorMap, m => m.Material == originMats[rendMatIdx]);
            if (mapIdx == -1)
                return;

            var dat = data.MaterialColorMap[mapIdx];
            instanceMats[mapIdx].SetColor(dat.ColorKey, dat.Color);
            currentMats[rendMatIdx] = instanceMats[mapIdx];
            didColorize = true;
        }



        public static void PrepareColorize(MaterialColorizeData data, 
            List<MaterialMemory> memory, 
            MaterialColorFromToAnimData outData)
        {
            if (memory == null)
                return;
            var rends = data.Renderer;

            foreach (var r in rends)
                PrepareColorizeForRenderer(data, memory, outData, r);
        }

        static void PrepareColorizeForRenderer(MaterialColorizeData targetData, 
            List<MaterialMemory> memory, 
            MaterialColorFromToAnimData outData, 
            Renderer r)
        {
            var instanceProvider = GetInstanceProvider(r);
            var currentMats = r.sharedMaterials;

            var didInstantiate = false;
            for (var rendMatIdx = 0; rendMatIdx < currentMats.Length; rendMatIdx++)
                PrepareColorizeForRendererMaterial(targetData, currentMats, rendMatIdx, outData, instanceProvider, ref didInstantiate);

            if (didInstantiate)
                Apply(memory, r, currentMats);
        }

        static void PrepareColorizeForRendererMaterial(MaterialColorizeData data, IList<Material> currentMats, int rendMatIdx,
            MaterialColorFromToAnimData outData, 
            MaterialInstanceProvider prov,
            ref bool didInstantiate)
        {
            prov.GetInstances(out var originMats, out var instanceMats);

            var mapIdx = Array.FindIndex(data.MaterialColorMap, m => m.Material == originMats[rendMatIdx]);
            if (mapIdx == -1)
                return;

            var dat = data.MaterialColorMap[mapIdx];
            var col = instanceMats[rendMatIdx].GetColor(dat.ColorKey);
            outData.ColorMapData[mapIdx] = new MaterialFromToColorMap()
            {
                //OriginalMaterial = mats[rendMatIdx],
                Material = instanceMats[rendMatIdx],
                ColorKey = dat.ColorKey,
                FromColor = col,
                ToColor = dat.Color
            };

            currentMats[rendMatIdx] = instanceMats[rendMatIdx];
            didInstantiate = true;
        }

        static MaterialInstanceProvider GetInstanceProvider(Component c)
        {
            if (!c.TryGetComponent(out MaterialInstanceProvider mip))
                mip = c.gameObject.AddComponent<MaterialInstanceProvider>();
            return mip;
        }

        //public static void PrepareColorizeWithBlock(MaterialColorizeData data, 
        //    List<MaterialMemory> memory, 
        //    List<MaterialFromToColorMapWithBlock> outData)
        //{
        //    if (outData == null || memory == null)
        //        return;
        //    var rends = data.Renderer;
        //    foreach (var r in rends)
        //        PrepareColorizeForRendererWithBlock(data, memory, outData, r);
        //}
        //static void PrepareColorizeForRendererWithBlock(MaterialColorizeData data, 
        //    List<MaterialMemory> memory, 
        //    ICollection<MaterialFromToColorMapWithBlock> outData, 
        //    Renderer r)
        //{
        //    var memIdx = memory.FindIndex(m => m.Renderer == r);
        //    var instanceMatList = GetInstanceMatList(memory, memIdx);
        //    var mats = r.sharedMaterials;
        //    var blocks = new MaterialPropertyBlock[mats.Length];
        //    for (var rendMatIdx = 0; rendMatIdx < mats.Length; rendMatIdx++)
        //    {
        //        PreparePropertyBlock(data, mats, rendMatIdx, outData, out var block);
        //        blocks[rendMatIdx] = block;
        //    }
        //    if (memIdx == -1)
        //        memory.Add(new MaterialMemory() { Material = r.sharedMaterials, 
        //            Renderer = r, 
        //            InstancedMaterials = instanceMatList, 
        //            PropertyBlocks = blocks});
        //}
        //static void PreparePropertyBlock(MaterialColorizeData data, IList<Material> mats, int rendMatIdx, 
        //    ICollection<MaterialFromToColorMapWithBlock> outData, out MaterialPropertyBlock propertyBlock)
        //{
        //    propertyBlock = null;
        //    var mapIdx = Array.FindIndex(data.MaterialColorMap, m => m.Material == mats[rendMatIdx]);
        //    if (mapIdx == -1)
        //        return;
        //    propertyBlock = SimplePool<MaterialPropertyBlock>.I.Get();
        //    var dat = data.MaterialColorMap[mapIdx];
        //    var col = mats[rendMatIdx].GetColor(dat.ColorKey);
        //    outData.Add(new MaterialFromToColorMapWithBlock()
        //    {
        //        PropertyBlock = propertyBlock,
        //        ColorKey = dat.ColorKey,
        //        FromColor = col,
        //        ToColor = dat.Color
        //    });
        //}
    }
}