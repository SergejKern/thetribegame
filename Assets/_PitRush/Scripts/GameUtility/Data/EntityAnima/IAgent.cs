﻿using System;
using Core.Unity.Interface;
using Core.Unity.Types;

namespace GameUtility.Data.EntityAnima
{
    public interface IAgent : ITarget
    {
        ITarget Target { get; }
        //TargetingIntent Intent { get; }
        void SetTargetWithPriority(ITarget target, int priority);
        void ClearTargetPriority(int priority);
    }

    [Serializable]
    public class RefIAgent: InterfaceContainer<IAgent> { }

    //public enum TargetingIntent
    //{
    //    Investigation,
    //    Interaction,
    //    Combat
    //}
}
