﻿using System.Linq;
using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace GameUtility
{
    public class SceneRoot : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onSceneActivated;
        [SerializeField] UnityEvent m_onSceneDeactivated;

        [SerializeField] RefITransitionActivation[] m_transitions;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public bool IsInTransition { get; private set; }
        public bool ActiveOrActivating { get; private set; }

        public void InitActive(bool activated)
        {
            IsInTransition = false;
            ActiveOrActivating = activated;

            gameObject.SetActive(activated);
            foreach (var t in m_transitions)
                t.Result?.InitActive(activated);
            
            if (activated)
                m_onSceneActivated.Invoke();
        }

        void Update() => UpdateTransition();

        void UpdateTransition()
        {
            if (!IsInTransition)
                return;

            if (m_transitions.Any(t => t.Result.IsInTransition))
                return;

            gameObject.SetActive(ActiveOrActivating);

            if (ActiveOrActivating)
                m_onSceneActivated.Invoke();
            else
                m_onSceneDeactivated.Invoke();

            IsInTransition = false;
        }

        public void SetActive(bool active)
        {
            IsInTransition = true;
            ActiveOrActivating = active;

            if (active)
                gameObject.SetActive(true);

            foreach (var t in m_transitions) 
                t?.Result?.SetActive(active);
        }

        public static GameObject GetSceneRoot(Scene scene, out SceneRoot sceneRoot)
        {
            sceneRoot = null;
            var roots = scene.GetRootGameObjects();
            foreach (var r in roots)
            {
                if (r.TryGetComponent(out sceneRoot))
                    return r;
            }

            return null;
        }
    }

}