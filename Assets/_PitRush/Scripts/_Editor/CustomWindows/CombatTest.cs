﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Utility.Debug;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Controls;
using Game.Combat.Timeline;
using Game.Combat.Weapon;
using Game.Configs.Combat;
using Game.Globals;
using GameUtility.MeleeWeaponTrail;
using GameUtility.Data.PhysicalConfrontation;
using UnityEditor;
using UnityEngine;

namespace _Editor.CustomWindows
{
    public class CombatTest : EditorWindow
    {
        struct NestedEditorData
        {
            public Editor Editor;
            public bool Shown;
        }

        struct MeleeTrailData
        {
            public MeleeWeaponTrail Trail;
            public MeleeWeaponTrailConfig Config;
        }

        [MenuItem("Tools/CombatTest")]
        static void Open() => GetWindow<CombatTest>();

        [SerializeField]
        GameObject m_prefab;
        [SerializeField]
        GrabControl.ConfigData m_grabData = new GrabControl.ConfigData()
        {
            DropEnabled = false,
            GrabEnabled = false,
            SwitchEnabled = false
        };

        [SerializeField] AttackTypeID m_attackTypeID;

        [SerializeField] float m_pointsPerSecond;

        GameObject m_spawnedObject;
        FsmTimelineAgent m_timeline;

        AttackControl m_spawnedObjectAttackControl;
        List<Weapon> m_spawnedObjectWeapons;
        NestedEditorData[] m_weaponEditors = new NestedEditorData[2];

        MeleeTrailData[] m_trailData;

        //string m_attackState;
        AttackData m_attackData;
        float m_bodyRange;

        ref NestedEditorData GetWeaponEditor(int i) => ref m_weaponEditors[i];
        //Editor m_attackControlEditor;

        SerializedObject m_serializedObject;
        void OnEnable()
        {
            m_serializedObject = new SerializedObject(this);
            SceneView.duringSceneGui += OnSceneGUI;
        }

        void OnGUI()
        {
            if (m_spawnedObject == null)
                SpawnObjectGUI();

            EquipGUI();

            WeaponsGUI();
            AttackGUI();
        }

        void WeaponsGUI()
        {
            if (m_spawnedObjectWeapons == null)
                return;

            m_spawnedObjectWeapons.RemoveAll(w => w == null);
            if (m_spawnedObjectWeapons.IsNullOrEmpty())
                return;
            if (m_weaponEditors.Length < m_spawnedObjectWeapons.Count)
                Array.Resize(ref m_weaponEditors, m_spawnedObjectWeapons.Count);
            for (var i = 0; i < m_spawnedObjectWeapons.Count; i++)
            {
                ref var ed = ref GetWeaponEditor(i);
                Editor.CreateCachedEditor(m_spawnedObjectWeapons[i], null, ref ed.Editor);

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    ed.Shown = EditorGUILayout.Foldout(ed.Shown, ed.Editor.target.name);
                    if (!ed.Shown)
                        continue;
                    ed.Editor.OnInspectorGUI();
                }
            }
        }

        bool m_sceneGUIWasCalled;
        void OnSceneGUI(SceneView sceneView)
        {
            m_sceneGUIWasCalled = true;
            if (m_spawnedObjectAttackControl == null)
                return;
            if (m_attackData.AttackState.IsNullOrEmpty())
                return;
            var tr = m_spawnedObject.transform;
            var fwd = tr.forward;

            var moveDebugPos = 0.25f * Vector3.up;
            var weaponDebugPos = tr.position + (0.5f * Vector3.up) + (m_bodyRange * fwd);

            var wpnRange = m_attackData.OnHitRange - m_bodyRange;
            var wpnEndPos = weaponDebugPos + wpnRange * fwd;

            Debug.DrawLine(moveDebugPos, moveDebugPos + m_attackData.FullRangeUntilOnHit * fwd, Color.magenta);
            Debug.DrawLine(weaponDebugPos, wpnEndPos, Color.red);
            CustomDebugDraw.DrawPosition(wpnEndPos, Color.red, 0.25f);
            UpdateTrailConfig();
        }

        float m_lastAnimationLength;
        bool m_needFinalize;
        void UpdateTrailConfig()
        {
            if (m_trailData.IsNullOrEmpty())
                return;

            if (string.IsNullOrEmpty(m_attackData.AttackState))
                return;

            var animator = m_spawnedObjectAttackControl.ControlCentre.Animator;
            var state = animator.GetCurrentAnimatorStateInfo(1);
            if (Animator.StringToHash(m_attackData.AttackState) != state.shortNameHash)
                return;
            if (state.normalizedTime<=0 || state.normalizedTime>=1)
                return;
            m_lastAnimationLength = state.length;

            foreach (var td in m_trailData)
            {
                if (td.Config == null || td.Trail == null)
                    continue;
                var points = td.Trail.GetPoints();
                if (points.IsNullOrEmpty())
                    continue;
                td.Config.SetPoints(state.shortNameHash, state.normalizedTime, td.Trail.GetPoints(), td.Trail.transform);
                EditorUtility.SetDirty(td.Config);
            }
        }

        void FinalizeTrailConfig()
        {
            if (m_trailData.IsNullOrEmpty())
                return;
            if (string.IsNullOrEmpty(m_attackData.AttackState))
                return;
            foreach (var td in m_trailData)
            {
                if (td.Config == null || td.Trail == null)
                    continue;
                td.Config.ReducePoints(Animator.StringToHash(m_attackData.AttackState), m_lastAnimationLength * m_timeline.OverrideSpeed,
                    m_pointsPerSecond);
                EditorUtility.SetDirty(td.Config);
            }

            m_needFinalize = false;
        }

        void AttackGUI()
        {
            if (m_spawnedObjectAttackControl == null)
                return;
            if (!m_spawnedObjectAttackControl.Ready)
                return;

            if (m_attackData.AttackState != null && m_needFinalize)
            {
                FinalizeTrailConfig();
                m_attackData = default;
            }
            m_attackTypeID = (AttackTypeID) EditorGUILayout.ObjectField(m_attackTypeID, typeof(AttackTypeID), false);
            var currentMoveIdx = m_spawnedObjectAttackControl.Editor_CurrentMoveIdx;
            var nextMoveIdx = EditorGUILayout.IntField("Next Move Idx", currentMoveIdx + 1);
            if (nextMoveIdx != currentMoveIdx + 1)
                m_spawnedObjectAttackControl.Editor_CurrentMoveIdx = nextMoveIdx - 1;
            
            var attMoveData = m_spawnedObjectAttackControl.GetMoveData(m_attackTypeID, nextMoveIdx);
            m_bodyRange = attMoveData.AddBodyRange;

            m_attackData = attMoveData.GetAttackData(m_attackTypeID, m_spawnedObjectAttackControl.CurrentWeapons);

            //Debug.DrawLine(pos, pos + range* tr.forward, Color.red);
            GUILayout.Label($"Weapon Range: {m_attackData.OnHitRange}");
            GUILayout.Label($"Move Range: {attMoveData.MoveRange}");
            GUILayout.Label($"Sum: {m_attackData.OnHitRange + attMoveData.MoveRange}");

            //GUILayout.Label($"Tolerance: {Tolerance}");
            GUILayout.Label($"Range used by AI: {m_attackData.FullRangeUntilOnHit}");

            m_timeline.OverrideSpeed = EditorGUILayout.FloatField("Speed: " , m_timeline.OverrideSpeed);
            //m_trail = (MeleeWeaponTrail) EditorGUILayout.ObjectField(m_trail, typeof(MeleeWeaponTrail), true);
            //m_trailConfig = (MeleeWeaponTrailConfig) 
            //    EditorGUILayout.ObjectField(m_trailConfig, typeof(MeleeWeaponTrailConfig), false);

            m_pointsPerSecond = EditorGUILayout.FloatField("Trail: Points per Second", m_pointsPerSecond);
            if (GUILayout.Button("Attack"))
                Attack();

            if (!m_sceneGUIWasCalled)
                EditorGUILayout.HelpBox("Scene View has to be visible or no Trail Configs will be generated!",
                    MessageType.Warning);
            else EditorGUILayout.HelpBox("Scene View visible!",
                MessageType.Info);

            if (Event.current.type == EventType.Repaint)
                m_sceneGUIWasCalled = false;
        }

        void Attack()
        {
            m_spawnedObject.transform.position = Vector3.zero;
            m_spawnedObject.transform.rotation = Quaternion.identity;

            m_spawnedObjectAttackControl.NextAttackType = m_attackTypeID;

            if (!m_trailData.IsNullOrEmpty())
            {
                foreach (var td in m_trailData)
                {
                    if (td.Config != null)
                        td.Config.ClearPoints(Animator.StringToHash(m_attackData.AttackState));
                }
            }

            m_spawnedObjectAttackControl.Attack(m_attackTypeID);
            m_needFinalize = true;
        }

        void EquipGUI()
        {
            var grabDataProp = m_serializedObject.FindProperty(nameof(m_grabData));
            var initialItemRightProp = grabDataProp.FindPropertyRelative(nameof(GrabControl.ConfigData.InitialItemRight));
            var initialItemLeftProp = grabDataProp.FindPropertyRelative(nameof(GrabControl.ConfigData.InitialItemLeft));

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(initialItemRightProp);
                EditorGUILayout.PropertyField(initialItemLeftProp);

                if (!check.changed)
                    return;
                m_serializedObject.ApplyModifiedProperties();
                if (m_spawnedObject != null)
                    UpdateEquipment();
            }
        }

        void SpawnObjectGUI()
        {
            m_prefab = (GameObject) EditorGUILayout.ObjectField("Prefab: ", m_prefab, typeof(GameObject), false);

            if (!GUILayout.Button("Spawn"))
                return;

            GameGlobals.GroundPlaneY = 0f;

            m_spawnedObject = m_prefab.GetPooledInstance(Vector3.zero, Quaternion.identity);
            if (m_spawnedObject.TryGetComponent<PlayMakerFSM>(out var fsm))
                fsm.enabled = false;
            m_spawnedObject.TryGetComponent(out m_timeline);

            UpdateEquipment();

            m_spawnedObjectAttackControl = m_spawnedObject.GetComponentInChildren<AttackControl>();
            if (m_spawnedObjectAttackControl != null)
                m_spawnedObjectWeapons = m_spawnedObjectAttackControl.CurrentWeapons;

            GetTrails();
        }

        void UpdateEquipment()
        {
            var grabControl = m_spawnedObject.GetComponentInChildren<GrabControl>();
            if (grabControl == null)
                return;
            grabControl.InitializeWithData(m_grabData);
            GetTrails();
            //grabControl.GetCurrentEquipment(out var r, out var l);
        }

        void GetTrails()
        {
            var trails = m_spawnedObject.GetComponentsInChildren<MeleeWeaponTrail>();
            m_trailData = new MeleeTrailData[trails.Length];
            for (var i = 0; i < trails.Length; i++)
            {
                m_trailData[i].Trail = trails[i];
                m_trailData[i].Config = trails[i].Config;
                trails[i].Editor_IgnoreConfig = true;
            }
        }
    }
}