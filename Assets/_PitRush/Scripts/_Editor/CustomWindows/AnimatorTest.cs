using Core.Editor.Extensions;
using UnityEditor;
using UnityEngine;

namespace _Editor.CustomWindows
{
    public class AnimatorTest : EditorWindow
    {
        [MenuItem("Tools/AnimatorTest")]
        static void Open()
        {
            GetWindow<AnimatorTest>();
        }

        readonly UnityEditor.Animations.AnimatorController m_lastController = null;
        string[] m_animationStates;

        void OnGUI()
        {
            GUILayout.Label("Selection.activeGameObject: " + Selection.activeGameObject);
            if (Selection.activeGameObject == null)
                return;

            if (!Selection.activeGameObject.TryGetComponent<Animator>(out var anim))
                return;

            var controller = anim.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
            if (controller == null)
                return;

            if (m_lastController != controller)
                controller.GetAllStateNames(out m_animationStates);
            
            AnimationStatesGUI();
        }

        void AnimationStatesGUI()
        {
            if (!Selection.activeGameObject.TryGetComponent<Animator>(out var anim))
                return;
            foreach (var item in m_animationStates)
            {
                var hash = Animator.StringToHash(item);
                if (!GUILayout.Button($"{item} - {hash}"))
                    continue;

                anim.Play(item);
            }
        }

    }
}
