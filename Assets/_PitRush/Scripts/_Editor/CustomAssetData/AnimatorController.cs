using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace _Editor.CustomAssetData
{
    [System.Serializable]
    public class AnimatorControllerCustomData
    {
        public bool IgnoreStateNameValidator;

        public static void LoadCustomData(AnimatorController controller, out AnimatorControllerCustomData customData)
        {
            LoadCustomData(controller, out customData, out _);
        }
        public static void LoadCustomData(AnimatorController controller, out AnimatorControllerCustomData customData, out AssetImporter importer)
        {
            var path = AssetDatabase.GetAssetPath(controller);
            importer = AssetImporter.GetAtPath(path);
            if (importer == null)
            {
                customData = new AnimatorControllerCustomData();
                return;
            }
            customData = JsonUtility.FromJson<AnimatorControllerCustomData>(importer.userData) ?? new AnimatorControllerCustomData();
        }
    }


}