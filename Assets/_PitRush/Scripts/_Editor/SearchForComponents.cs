﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace _Editor
{
    public class SearchForComponents : EditorWindow
    {
        [MenuItem("Tools/Search For Components")]
        static void Init()
        {
            var window = (SearchForComponents)GetWindow(typeof(SearchForComponents));
            window.Show();
            window.position = new Rect(20, 80, 400, 300);
        }


        readonly string[] m_modes = { "Search for component usage", "Search for missing components" };

        List<string> m_listResult;
        int m_editorMode, m_editorModeOld;
        MonoScript m_targetComponent, m_lastChecked;
        string m_componentName = "";
        Vector2 m_scroll;

        void OnGUI()
        {
            GUILayout.Space(3);
            var oldValue = GUI.skin.window.padding.bottom;
            GUI.skin.window.padding.bottom = -20;
            var windowRect = GUILayoutUtility.GetRect(1, 17);
            windowRect.x += 4;
            windowRect.width -= 7;
            m_editorMode = GUI.SelectionGrid(windowRect, m_editorMode, m_modes, 2, "Window");
            GUI.skin.window.padding.bottom = oldValue;

            if (m_editorModeOld != m_editorMode)
            {
                m_editorModeOld = m_editorMode;
                m_listResult = new List<string>();
                m_componentName = m_targetComponent == null ? "" : m_targetComponent.name;
                m_lastChecked = null;
            }

            switch (m_editorMode)
            {
                case 0:
                    m_targetComponent = (MonoScript)EditorGUILayout.ObjectField(m_targetComponent, typeof(MonoScript), false);

                    if (m_targetComponent != m_lastChecked)
                    {
                        m_lastChecked = m_targetComponent;
                        m_componentName = m_targetComponent.name;
                        AssetDatabase.SaveAssets();
                        var targetPath = AssetDatabase.GetAssetPath(m_targetComponent);
                        var allPrefabs = GetAllPrefabs();
                        m_listResult = new List<string>();
                        foreach (var prefab in allPrefabs)
                        {
                            var single = new[] { prefab };
                            var dependencies = AssetDatabase.GetDependencies(single);
                            foreach (var dependedAsset in dependencies)
                            {
                                if (dependedAsset == targetPath) 
                                    m_listResult.Add(prefab);
                            }
                        }
                    }
                    break;
                case 1:
                    if (GUILayout.Button("Search!"))
                    {
                        var allPrefabs = GetAllPrefabs();
                        m_listResult = new List<string>();
                        foreach (var prefab in allPrefabs)
                        {
                            var o = AssetDatabase.LoadMainAssetAtPath(prefab);
                            try
                            {
                                var go = (GameObject)o;
                                var components = go.GetComponentsInChildren<Component>(true);
                                foreach (var c in components)
                                {
                                    if (c == null) 
                                        m_listResult.Add(prefab);
                                }
                            }
                            catch
                            {
                                Debug.Log("For some reason, prefab " + prefab + " won't cast to GameObject");
                            }
                        }
                    }
                    break;
            }

            if (m_listResult == null) 
                return;
            if (m_listResult.Count == 0)
                GUILayout.Label(m_editorMode == 0
                    ? (m_componentName == "" ? "Choose a component" : "No prefabs use component " + m_componentName)
                    : ("No prefabs have missing components!\nClick Search to check again"));
            else
            {
                GUILayout.Label(m_editorMode == 0 ? ("The following prefabs use component " + m_componentName + ":") : ("The following prefabs have missing components:"));
                m_scroll = GUILayout.BeginScrollView(m_scroll);
                foreach (var s in m_listResult)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(s, GUILayout.Width(position.width / 2));
                    if (GUILayout.Button("Select", GUILayout.Width(position.width / 2 - 10)))
                        Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(s);
                    GUILayout.EndHorizontal();
                }
                GUILayout.EndScrollView();
            }
        }

        static IEnumerable<string> GetAllPrefabs()
        {
            var temp = AssetDatabase.GetAllAssetPaths();
            return temp.Where(s => s.Contains(".prefab")).ToArray();
        }
    }
}