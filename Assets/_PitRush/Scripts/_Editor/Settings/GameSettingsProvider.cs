﻿// Create MyCustomSettingsProvider by deriving from SettingsProvider:

using System.IO;
using Game.Configs;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace _Editor.Settings
{
    internal class GameSettingsProvider : SettingsProvider
    {
        //SerializedObject m_configLinks;
        Editor m_configLinksEditor;

        const string k_configLinksPath = "Assets/_PitRush/Configs/ConfigLinks.asset";

        GameSettingsProvider(string path, SettingsScope scope = SettingsScope.Project)
            : base(path, scope) {}

        static bool IsSettingsAvailable() => File.Exists(k_configLinksPath);

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            // This function is called when the user clicks on the MyCustom element in the Settings window.
            //m_configLinks = GetConfigLinkSerialized();
            //Debug.Log("Activated");
            m_configLinksEditor = Editor.CreateEditor(GetOrCreateConfigLinks());
        }

        public override void OnGUI(string searchContext) => 
            m_configLinksEditor.OnInspectorGUI();

        // Register the SettingsProvider
        [SettingsProvider]
        public static SettingsProvider CreateGameSettingsProvider()
        {
            if (!IsSettingsAvailable())
                return null;
            var provider = new GameSettingsProvider("Project/Game Settings");

            // Automatically extract all keywords from the Styles.
            // provider.keywords = GetSearchKeywordsFromGUIContentProperties<Styles>();
            return provider;

            // Settings Asset doesn't exist yet; no need to display anything in the Settings window.
        }

        static ConfigLinks GetOrCreateConfigLinks()
        {
            var settings = AssetDatabase.LoadAssetAtPath<ConfigLinks>(k_configLinksPath);
            if (settings != null)
                return settings;
            settings = ScriptableObject.CreateInstance<ConfigLinks>();
            AssetDatabase.CreateAsset(settings, k_configLinksPath);
            AssetDatabase.SaveAssets();
            return settings;
        }

        //internal static SerializedObject GetConfigLinkSerialized() => 
        //    new SerializedObject(GetOrCreateConfigLinks());
    }
}