﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace _Editor
{
    [InitializeOnLoad]
    public class EditorCoroutine
    {
        public static IEnumerator StartCoroutine(IEnumerator newCoroutine)
        {
            k_coroutineInProgress.Add(newCoroutine);
            return newCoroutine;
        }

        /// <summary>
        ///  Coroutine to execute. Manage by the EasyLocalization_script
        /// </summary>
        static readonly List<IEnumerator> k_coroutineInProgress = new List<IEnumerator>();

        static EditorCoroutine() => EditorApplication.update += ExecuteCoroutine;

        static int m_currentExecute = 0;

        static void ExecuteCoroutine()
        {
            if (k_coroutineInProgress.Count <= 0)
                return;

            m_currentExecute = (m_currentExecute + 1) % k_coroutineInProgress.Count;

            var finish = !k_coroutineInProgress[m_currentExecute].MoveNext();

            if (finish) 
                k_coroutineInProgress.RemoveAt(m_currentExecute);
        }
    }
}