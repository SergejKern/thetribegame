﻿using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using _Editor.CustomAssetData;
using Core.Editor.Inspector;

namespace _Editor.CustomInspector
{
    [CustomEditor(typeof(AnimatorController))]
    public class AnimatorControllerInspector : BaseInspector
    {
        struct EditData
        {
            public AnimatorController Target;
            public AssetImporter Importer;
            public AnimatorControllerCustomData CustomData;
        }

        // ReSharper disable once InconsistentNaming
        new AnimatorController target => base.target as AnimatorController;

        EditData m_lastData;

        // Start is called before the first frame update
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUI.enabled = true;

            AnimatorControllerGUI();
        }

        void AnimatorControllerGUI()
        {
            if (m_lastData.Target != target)
                InitData();

            if (GUILayout.Button("Open Animator"))
                EditorApplication.ExecuteMenuItem("Window/Animation/Animator");

            GUILayout.Label("Custom Data: ", EditorStyles.boldLabel);

            using (new GUILayout.HorizontalScope())
            {
                using (new GUILayout.VerticalScope())
                {
                    GUILayout.Label("State-Name Validator: ");
                    m_lastData.CustomData.IgnoreStateNameValidator =
                        GUILayout.Toggle(m_lastData.CustomData.IgnoreStateNameValidator,
                            "Ignore");
                }

                if (m_lastData.CustomData.IgnoreStateNameValidator)
                    EditorGUILayout.HelpBox("State-Name Validator is ignored! " +
                                            "Please use this option only if you know what you are doing! ", MessageType.Warning);
                else EditorGUILayout.HelpBox("We choose the convention of animator states having the same name as the animation clip! " +
                                             "This way we can avoid a lot of problems with NullReferenceExceptions! " +
                                             "The validator checks that this convention is not violated!", MessageType.Info);
            }
        }

        void InitData()
        {
            AnimatorControllerCustomData.LoadCustomData(target, out var customData, out var importer);

            m_lastData.Target = target;
            m_lastData.Importer = importer;
            m_lastData.CustomData = customData;
        }

        void OnDisable()
        {
            if(m_lastData.Target == null)
                return;

            m_lastData.Importer.userData = JsonUtility.ToJson(m_lastData.CustomData);
            m_lastData.Importer.SaveAndReimport();
            m_lastData = new EditData();
        }
    }
}
