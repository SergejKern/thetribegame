﻿using System.IO;
using Core.Editor.Inspector;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace _Editor.CustomInspector
{
    [CustomEditor(typeof(DefaultAsset))]
    public class FolderInspector : BaseInspector
    {
        struct EditData
        {
            public DefaultAsset Target;
            public AssetImporter Importer;
            public string Desc;
        }

        // ReSharper disable once InconsistentNaming
        new DefaultAsset target => base.target as DefaultAsset;

        EditData m_lastData;

        bool m_edit;

        // Start is called before the first frame update
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUI.enabled = true;

            var path = AssetDatabase.GetAssetPath(target);
            var ext = Path.GetExtension(path);
            if (!ext.IsNullOrEmpty())
                return;
            FolderGUI(path);
        }

        void FolderGUI(string path)
        {
            if (m_lastData.Target != target)
                InitData(path);

            GUILayout.Label("Description: ");

            if (m_edit)
                m_lastData.Desc = GUILayout.TextArea(m_lastData.Desc);
            else
                EditorGUILayout.HelpBox(m_lastData.Desc, MessageType.Info);

            m_edit = GUILayout.Toggle(m_edit, "Edit Description", "Button", GUILayout.Width(150f));
        }

        void InitData(string path)
        {
            m_lastData.Target = target;
            m_lastData.Importer = AssetImporter.GetAtPath(path);
            m_lastData.Desc = m_lastData.Importer.userData;
        }

        void OnDisable()
        {
            if(m_lastData.Target == null)
                return;

            m_lastData.Importer.userData = m_lastData.Desc;
            m_lastData.Importer.SaveAndReimport();
            m_lastData = new EditData();
        }
    }
}
