using Game.Configs;
using UnityEditor;
using _Editor.CustomInspector.Combat;
using Core.Editor.Inspector;
using UnityEngine;

namespace _Editor.CustomInspector.Configs
{
    [CustomEditor(typeof(ConfigLinks))]
    public class ConfigLinksInspector : BaseInspector
    {
        struct NestedEditorData
        {
            public Editor Editor;
            public bool Show;
        }

        readonly NestedEditorData[] m_editorData = new NestedEditorData[11];

        ref NestedEditorData GetEditorData(int idx) => ref m_editorData[idx];

        // ReSharper disable once InconsistentNaming
        new ConfigLinks target => base.target as ConfigLinks;

        const string k_prefKeyEditorShown = "ConfigLinksInspector_EditorShown_";


        public override void Init()
        {
            base.Init();
            LoadVisibility();
        }


        void LoadVisibility()
        {
            for (var i = 0; i < m_editorData.Length; i++)
            {
                ref var edDat = ref GetEditorData(i);
                edDat.Show = EditorPrefs.GetBool(k_prefKeyEditorShown + i);
            }
        }

        void SaveVisibility()
        {
            for (var i = 0; i < m_editorData.Length; i++)
            {
                ref var edDat = ref GetEditorData(i);
                EditorPrefs.SetBool(k_prefKeyEditorShown + i, edDat.Show);
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var editorIdx = 0;
            EnsureNestedEditorAndContinue(target.GetPrefabLinks(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetSceneLinks(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetDebugDrawConfig(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetLevelsConfig(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetWeaponConfig(), typeof(WeaponConfigInspector), ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetSpawnInitConfig(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetItemAnimationCombinationConfig(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetCharacters(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetDefaultBehaviourConfig(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetCollisionsConfig(), null, ref editorIdx);
            EnsureNestedEditorAndContinue(target.GetPowerCoopMultiplierConfig(), null, ref editorIdx);

            var visibilityToggled = false;
            for (var i = 0; i < m_editorData.Length; i++)
            {
                ref var edDat = ref GetEditorData(i);
                var prevShown = edDat.Show;
                NestedEditor(ref edDat.Show, edDat.Editor);
                visibilityToggled |= (edDat.Show != prevShown);
            }

            if (visibilityToggled)
                SaveVisibility();
        }

        void EnsureNestedEditorAndContinue(Object obj, System.Type editorType, ref int editorDataIdx)
        {
            CreateCachedEditor(obj, editorType, ref GetEditorData(editorDataIdx).Editor);
            editorDataIdx++;
        }

        // ReSharper disable once FlagArgument
        static void NestedEditor(ref bool show, Editor editor)
        {
            if (editor == null || editor.target == null)
                return;

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                show = EditorGUILayout.Foldout(show, editor.target.name);
                if (!show)
                    return;
                using (new EditorGUILayout.HorizontalScope())
                {
                    GUILayout.Space(20);
                    using (new EditorGUILayout.VerticalScope())
                        editor.OnInspectorGUI();
                }
            }

            //EditorGUILayout.EndFoldoutHeaderGroup();
        }
    }
}