﻿using System.Collections.Generic;
using System.Linq;
using Game.Actors;
using Game.Arena.EnemySpawn;
using UnityEditor;
using UnityEngine;

namespace _Editor.CustomInspector.EnemySpawning
{
    public static class EnemySpawnsTimelineGUI
    {
        const float k_timeHeight = 20f;
        const float k_timeMul = 600f;
        const float k_spawnerLabelWidth = 75f;
        const float k_entryWidth = 28f;
        const float k_entryHeight = 32f;
        const float k_sideWayMul = 32f;
        const float k_spawnerHeight = k_entryHeight * 1.8f;

        public static void OnGUI(ref Vector2 scroll, List<EnemyWaveSpawnEntry> spawnerEntries, float[] spawnerCursorTimes = null)
        {
            if (spawnerEntries == null)
                return;

            var spawnerIndices = GetSpawners(spawnerEntries);

            var totalDuration = GetDuration(spawnerEntries);
            var visibleDuration = totalDuration;

            //if (spawnerData!= null)
            //    SpawnerInfoDataGUI(spawnerData);
            var parentRect = GUILayoutUtility.GetLastRect();
            var rect = GUILayoutUtility.GetRect(parentRect.width-50, 
                spawnerIndices.Count * k_spawnerHeight + k_timeHeight + GUI.skin.horizontalScrollbar.fixedHeight);

            var viewRect = new Rect {width = visibleDuration * k_timeMul + 128f};

            viewRect.width = Mathf.Max(viewRect.width, rect.width);
            viewRect.height = rect.height - GUI.skin.horizontalScrollbar.fixedHeight;

            DrawSpawnerLabels(spawnerIndices, rect);
            scroll = GUI.BeginScrollView(rect, scroll, viewRect, false, false);

            DrawTimes(visibleDuration, viewRect);
            DrawEnemies(spawnerEntries, spawnerIndices, viewRect);

            if (spawnerCursorTimes != null)
                DrawTimeCursors(spawnerIndices, viewRect, spawnerCursorTimes);

            GUI.EndScrollView();
        }

        static List<int> GetSpawners(IEnumerable<EnemyWaveSpawnEntry> spawnerEntries)
        {
            var spawners = new List<int>();
            foreach (var entry in spawnerEntries)
            {
                var spawnIndex = entry.SpawnerIndex;
                if (spawnIndex == -1)
                    continue;
                if (!spawners.Contains(spawnIndex))
                    spawners.Add(spawnIndex);
            }

            spawners.Sort();
            return spawners;
        }

        // foreach (var t in spawnerEntries) duration = Mathf.Max(duration, t.Time);
        static float GetDuration(IEnumerable<EnemyWaveSpawnEntry> spawnerEntries) =>
            spawnerEntries.Aggregate(0f, (current, t) => Mathf.Max(current, t.Time));

        //static void SpawnerInfoDataGUI(IReadOnlyList<SpawnerData> spawnerData)
        //{
        //    var spawnerInfoRect = GUILayoutUtility.GetRect(50, 50);
        //    spawnerInfoRect.width = 50;
        //    for (var spawnerIdx = 0; spawnerIdx < spawnerData.Count; ++spawnerIdx)
        //    {
        //        var spawner = spawnerData[spawnerIdx];
        //        GUI.Label(spawnerInfoRect, "Spawner " + (spawnerIdx + 1));
        //        spawnerInfoRect.x += 50;
        //        foreach (var el in spawner.SpawnedEnemyTypes)
        //        {
        //            DrawEnemy(spawnerInfoRect, el);
        //            spawnerInfoRect.x += 50;
        //        }
        //    }
        //}

        static void DrawTimeCursors(IReadOnlyList<int> spawnerIndices, Rect viewRect, IReadOnlyList<float> spawnerTimes)
        {
            GUI.backgroundColor = Color.yellow;
            for (var i = 0; i < spawnerIndices.Count; i++)
            {
                var spawnerIndex = spawnerIndices[i];
                if (spawnerIndex > spawnerTimes.Count)
                    continue;
                var spawnerTime = spawnerTimes[spawnerIndex];

                var spawnerRect = viewRect;
                spawnerRect.x += k_spawnerLabelWidth;
                spawnerRect.y += i * k_spawnerHeight + k_timeHeight;
                spawnerRect.height = k_spawnerHeight;

                var cursorRect = spawnerRect;
                cursorRect.width = 1f;
                cursorRect.x += k_timeMul * spawnerTime;
                GUI.Box(cursorRect, GUIContent.none);
            }
            GUI.backgroundColor = Color.white;
        }

        static void DrawTimes(float duration, Rect viewRect)
        {
            for (float timeSecs = 0; timeSecs < duration; timeSecs += 15)
            {
                var timeRect = viewRect;
                timeRect.width = 38;
                timeRect.height = k_timeHeight;
                timeRect.x += k_spawnerLabelWidth + timeSecs * k_timeMul;
                DrawTime(timeRect, viewRect.height, timeSecs);
            }
        }

        static void DrawTime(Rect rect, float lineLength, float time)
        {
            var labelRect = rect;
            labelRect.x -= labelRect.width / 2;

            var minutes = Mathf.FloorToInt(time / 60);
            var secs = Mathf.FloorToInt(time % 60f);
            GUI.Label(labelRect, minutes.ToString("D2") + ":" + secs.ToString("D2"));

            var lineRect = rect;
            lineRect.height = lineLength;
            lineRect.y += 16;
            lineRect.width = 1;
            lineRect.x = rect.xMin;
            GUI.Box(lineRect, GUIContent.none);
        }

        static void DrawSpawnerLabels(IReadOnlyList<int> spawnerIndices, Rect viewRect)
        {
            for (var i = 0; i < spawnerIndices.Count; i++)
            {
                var spawnerRect = viewRect;
                spawnerRect.y += i * k_spawnerHeight + k_timeHeight;
                spawnerRect.height = k_spawnerHeight;
                GUI.Box(spawnerRect, GUIContent.none);
                var spawnerIndex = spawnerIndices[i];
                var spawnerLabelRect = spawnerRect;
                spawnerLabelRect.width = k_spawnerLabelWidth;
                GUI.Label(spawnerLabelRect, "Spawner " + (spawnerIndex + 1));
            }
        }

        static void DrawEnemies(IEnumerable<EnemyWaveSpawnEntry> spawnerEntries, IList<int> spawnerIndices, Rect viewRect)
        {
            foreach (var entry in spawnerEntries)
            {
                if (entry.EnemyConfig == null)
                    continue;

                var localSpawnerIndex = spawnerIndices.IndexOf(entry.SpawnerIndex);
                var entryRect = viewRect;
                entryRect.width = k_entryWidth;
                entryRect.height = k_entryHeight;
                entryRect.x += k_spawnerLabelWidth + entry.Time * k_timeMul - k_entryWidth * 0.5f;
                const float entryHalfWidth = k_entryWidth * 0.5f;
                const float spawnerHalfHeight = k_spawnerHeight * 0.5f;
                entryRect.y += k_timeHeight + localSpawnerIndex * k_spawnerHeight + spawnerHalfHeight - entryHalfWidth;
                entryRect.y += Mathf.Clamp01(entry.SideOffset) * k_sideWayMul;

                //if (entry.IsBoss)
                //{
                //    entryRect.width *= 1.4f;
                //    entryRect.height *= 1.4f;
                //}

                DrawEnemy(entryRect, entry.EnemyConfig);
            }
        }

        static void DrawEnemy(Rect rect, EnemyConfig conf)
        {
            GUIContent content;
            if (conf != null)
            {
                var sprite = conf.Icon;
                content = new GUIContent(sprite.texture, conf.name);
            }
            else
                content = GUIContent.none;

            if (GUI.Button(rect, content, GUI.skin.label))
                EditorGUIUtility.PingObject(conf);
        }
    }
}
