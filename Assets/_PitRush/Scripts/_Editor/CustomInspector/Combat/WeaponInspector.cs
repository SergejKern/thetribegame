﻿using System;
using Core.Editor.Inspector;
using Game.Actors.Controls;
using Game.Combat.Weapon;
using Game.Configs;
using Game.Configs.Combat;
using UnityEditor;
using UnityEngine;

namespace _Editor.CustomInspector.Combat
{
    [CustomEditor(typeof(Weapon))]
    public class WeaponInspector : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new Weapon target => base.target as Weapon;
        Weapon m_lastInspectedWeapon;

        RuntimeAnimatorController m_controller;
        WeaponComboMoveSetGUI.Data m_data;
        int m_idx;

        static WeaponConfig WeaponConfig => ConfigLinks.WeaponConfig;

        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                DrawPropertiesExcluding(serializedObject, "m_script");
                if (check.changed)
                    serializedObject.ApplyModifiedProperties();
            }

            if (target.Actor == null)
                return;
            if (WeaponConfig == null)
                return;

            InitTypes();

            GUILayout.Label("Moves Config:", EditorStyles.boldLabel);
            var setsProp = m_data.TargetSerializedObject.FindProperty($"{nameof(WeaponConfig.ActorWeaponMoveSets)}");
            var childProp = setsProp.GetArrayElementAtIndex(m_idx);
            var moveSetsProp = childProp.FindPropertyRelative($"{nameof(WeaponConfig.ActorWeaponMoves.MoveSets)}");

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                WeaponComboMoveSetGUI.DrawList(m_data, ref WeaponConfig.ActorWeaponMoveSets[m_idx].MoveSets, moveSetsProp);
                if (check.changed)
                    target.Actor.GetComponentInChildren<AttackControl>().ReInitMoves();
            }
        }

        void InitTypes()
        {
            if (m_lastInspectedWeapon == target)
                return;
            if (target.Actor == null)
                return;

            var cc = target.Actor.GetComponentInChildren<ControlCentre>();
            if (cc == null || cc.Animator == null)
                return;

            m_lastInspectedWeapon = target;
            m_idx = WeaponConfig.GetRelevantMoveIdx(cc.Animator.runtimeAnimatorController, target.MovesType);

            m_data = new WeaponComboMoveSetGUI.Data
            {
                TargetSerializedObject = new SerializedObject(WeaponConfig),
                BackgroundColor = Color.white,
                PassDownController = cc.Animator.runtimeAnimatorController
            };
        }
    }
}