﻿using System;
using Core.Editor.Inspector;
using Game.Configs.Combat;
using UnityEditor;
using UnityEngine;

namespace _Editor.CustomInspector.Combat
{
    [CustomEditor(typeof(WeaponConfig))]
    public class WeaponConfigInspector : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new WeaponConfig target => base.target as WeaponConfig;
        ActorWeaponMovesGUI.Data m_actorWeaponMovesGUIData;

        public override void OnInspectorGUI()
        {
            // base.OnInspectorGUI();
            InitTypes();
            var actorWeaponMoveSetsProp = serializedObject.FindProperty(nameof(target.ActorWeaponMoveSets));

            // ReSharper disable once ConvertToUsingDeclaration
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                ActorWeaponMovesGUI.DrawList(ref m_actorWeaponMovesGUIData, ref target.ActorWeaponMoveSets, actorWeaponMoveSetsProp);
                if (!check.changed)
                    return;
                EditorUtility.SetDirty(target);
                serializedObject.Update();
            }
        }

        void InitTypes()
        {
            if (m_actorWeaponMovesGUIData.TargetSerializedObject == serializedObject)
                return;
            m_actorWeaponMovesGUIData = new ActorWeaponMovesGUI.Data()
            {
                BackgroundColor = Color.white,
                TargetSerializedObject = serializedObject,
                WeaponMoveTypes = Enum.GetNames(typeof(WeaponMovesType)),
                ChildGUIData = new WeaponComboMoveSetGUI.Data()
                {
                    BackgroundColor = new Color(0.75f, 0.75f, 0.75f),
                    TargetSerializedObject = serializedObject,
                }
            };
        }
    }
}
