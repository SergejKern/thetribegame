﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Extensions;
using Core.Types;
using Core.Unity.Types;
using Game.Actors.Model_Animation;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace _Editor.CustomInspector.Combat
{
    public static class WeaponComboMoveGUI
    {
        public struct Data
        {
            public RuntimeAnimatorController PassThroughController;
            public SerializedObject TargetSerializedObject;
        }

        // static readonly GUIContent gridSeparator = new GUIContent("|");
        static readonly GUIContent k_dmg = new GUIContent("Dmg", "Damage Multiplier");
        static readonly GUIContent k_kickback = new GUIContent("Knk", "Knockback-Force Multiplier");

        static readonly GUIContent k_attackSpeed = new GUIContent("Spd", "Attack-Speed");
        static readonly GUIContent k_recoverSpeed = new GUIContent("Rec", "Recover-Speed");
        static readonly GUIContent k_cooldown = new GUIContent("Cool", "Cooldown");
        static readonly GUIContent k_motionFactor = new GUIContent("Fwd", "Motion Factor (multiplier for root motion in animation)");
        static readonly GUIContent k_energyCost = new GUIContent("Sta", "Stamina Cost");

        static readonly GUIContent k_range = new GUIContent("Rng", "Motion Range (for enemy AI & Timeline)");
        static readonly GUIContent k_addBodyRange = new GUIContent("+B", "Body-Range onHit (where does the weapon start from body center)");
        static readonly GUIContent k_weaponRangeFac = new GUIContent("WpRF", "Weapon Range Factor: 1 for straight sword attacks, 0 for no weapon range influence");

        static readonly GUIContent k_wIdx = new GUIContent("WIdx", "Weapon Index added to Range");

        static readonly GUIContent k_attackTime = new GUIContent("Time", "Attack-Time extracted from OnHit in animation. Used with Attack-Timeline-Feature");

        static readonly GUIContent k_animState = new GUIContent("Anim", "Model Animator State");
        static readonly GUIContent k_fxHeader = new GUIContent("FX Variation", "Effects");
        static readonly GUILayoutOption k_gridW = GUILayout.Width(35f);
        static readonly GUILayoutOption k_animStateW = GUILayout.Width(100f);

        //static readonly GUILayoutOption fxW = GUILayout.Width(200f);

        public static void Draw(Data data, SerializedProperty prop,
            ref WeaponConfig.WeaponComboMove[] moveMoves)
        {
            //var splitterW = GUILayout.Width(15);
            //void Next() => GUILayout.Label(gridSeparator, splitterW);
            //var w = EditorGUIUtility.currentViewWidth -20f;

            WeaponComboMoveGridLabels();

            var deleteIdx = -1;
            for (var i = 0; i < moveMoves.Length; i++)
                Draw(data, prop, ref moveMoves, i, ref deleteIdx);
            

            AddMove(data, ref moveMoves);
            RemoveMove(data, ref moveMoves, deleteIdx);
        }

        static void Draw(Data data, SerializedProperty prop,
            ref WeaponConfig.WeaponComboMove[] moveMoves, int i,
            ref int deleteIdx)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                MoveFloatValuesGUI(data, moveMoves, i, ref deleteIdx);

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var moveProp = prop.GetArrayElementAtIndex(i);
                    var animProp = moveProp.FindPropertyRelative(nameof(WeaponConfig.WeaponComboMove.ModelAnimationState));
                    EditorGUILayout.PropertyField(animProp, k_animStateW);

                    var fxListProp = moveProp.FindPropertyRelative(nameof(WeaponConfig.WeaponComboMove.WeaponFxList));
                    FXList(data, ref moveMoves, i, fxListProp);

                    if (!check.changed)
                        return;

                    data.TargetSerializedObject.ApplyModifiedProperties();
                    data.TargetSerializedObject.Update();
                }
            }
        }

        static void FXList(Data data, ref WeaponConfig.WeaponComboMove[] moveMoves, int i, SerializedProperty fxListProp)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new EditorGUILayout.VerticalScope())
            {
                var move = moveMoves[i];
                var deleteIdx = -1;

                for (var j = 0; j < move.WeaponFxList.Length; j++)
                {
                    var fx = move.WeaponFxList[j];
                    var fxProp = fxListProp.GetArrayElementAtIndex(j);
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("x", EditorStyles.miniButton))
                            deleteIdx = j;
                        fx.FXID = (WeaponFXID) EditorGUILayout.ObjectField(fx.FXID, typeof(WeaponFXID), false);

                        // fx.Prefab = EditorGUILayout.ObjectField(fx.Prefab, typeof(GameObject), false) as GameObject;
                        if (FXListPrefabData(data, fxProp) == ChangeCheck.Changed)
                            continue;
                    }

                    if (!check.changed)
                        continue;

                    move.WeaponFxList[j] = fx;
                    Apply(data, moveMoves, i, move);
                }

                if (deleteIdx != -1)
                {
                    RemoveAt(ref move.WeaponFxList, deleteIdx);
                    Apply(data, moveMoves, i, move);
                }

                if (!GUILayout.Button("+", EditorStyles.miniButton, GUILayout.Width(25f)))
                    return;

                Add(ref move.WeaponFxList, new WeaponConfig.SpawnableWeaponFX());
                Apply(data, moveMoves, i, move);
            }
        }

        static void Apply(Data data, IList<WeaponConfig.WeaponComboMove> moveMoves, int i, 
            WeaponConfig.WeaponComboMove move)
        {
            moveMoves[i] = move;
            data.TargetSerializedObject.Update();
        }

        static ChangeCheck FXListPrefabData(Data data, SerializedProperty fxProp)
        {
            using (var propCheck = new EditorGUI.ChangeCheckScope())
            {
                var variationProp = fxProp.FindPropertyRelative(nameof(WeaponConfig.SpawnableWeaponFX.Variation));
                EditorGUILayout.PropertyField(variationProp, GUIContent.none, true);

                if (!propCheck.changed)
                    return ChangeCheck.NotChanged;

                data.TargetSerializedObject.ApplyModifiedProperties();
                data.TargetSerializedObject.Update();
                return ChangeCheck.Changed;
            }
        }

        static void MoveFloatValuesGUI(Data data, IList<WeaponConfig.WeaponComboMove> moveMoves, int i, ref int deleteIdx)
        {
            var move = moveMoves[i];
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(20f)))
                    deleteIdx = i;

                move.DamageFactor = EditorGUILayout.FloatField(move.DamageFactor, k_gridW); //Next();
                move.KickbackForceFactor = EditorGUILayout.FloatField(move.KickbackForceFactor, k_gridW); //Next();

                move.AttackSpeedFactor = EditorGUILayout.FloatField(move.AttackSpeedFactor, k_gridW); //Next();
                move.RecoverSpeedFactor = EditorGUILayout.FloatField(move.RecoverSpeedFactor, k_gridW); //Next();
                move.Cooldown = EditorGUILayout.FloatField(move.Cooldown, k_gridW); //Next();
                move.MotionFactor = EditorGUILayout.FloatField(move.MotionFactor, k_gridW);
                move.EnergyCost = EditorGUILayout.FloatField(move.EnergyCost, k_gridW);

                move.MoveRange = EditorGUILayout.FloatField(move.MoveRange, k_gridW); //Next();
                move.AddBodyRange = EditorGUILayout.FloatField(move.AddBodyRange, k_gridW); //Next();
                move.WeaponRangeFactor = EditorGUILayout.FloatField(move.WeaponRangeFactor, k_gridW); //Next();

                move.AddRangeWeaponIndex = EditorGUILayout.IntField(move.AddRangeWeaponIndex, k_gridW); //Next();

                if (GUILayout.Button($"{move.AttackHitTime:F2}", k_gridW))
                    ExtractAttackTime(move.ModelAnimationState, ref move);
                var controllerOk = move.ModelAnimationState.Controller == data.PassThroughController;

                if (!check.changed && controllerOk)
                    return;

                if (!controllerOk)
                {
                    move.ModelAnimationState =
                        new AnimatorStateRef(data.PassThroughController, move.ModelAnimationState.StateName);
                }

                moveMoves[i] = move;
                data.TargetSerializedObject.Update();
            }
        }

        static void ExtractAttackTime(AnimatorStateRef state, ref WeaponConfig.WeaponComboMove move)
        {
            move.AttackHitTime = -1;

            var ctrl = state.Controller as AnimatorController;
            var clipName = ctrl.GetClipName(state.StateName);
            var clip = Array.Find(state.Controller.animationClips, (c) => string.Equals(c.name, clipName));
            if (clip == null)
                return;
            var clipEvent = Array.Find(clip.events, (ce) => string.Equals(ce.functionName, nameof(ActorAnimationEvents.OnHit)));
            if (clipEvent == null)
                return;
            move.AttackHitTime = clipEvent.time;
        }

        static void RemoveMove(Data data, ref WeaponConfig.WeaponComboMove[] moveMoves, int deleteIdx)
        {
            if (deleteIdx == -1) return;
            RemoveAt(ref moveMoves, deleteIdx);
            data.TargetSerializedObject.Update();
        }

        static void AddMove(Data data, ref WeaponConfig.WeaponComboMove[] moveMoves)
        {
            if (!GUILayout.Button("+")) return;
            var moveList = moveMoves.ToList();
            moveList.Add(new WeaponConfig.WeaponComboMove() { WeaponFxList = new WeaponConfig.SpawnableWeaponFX[0] });
            moveMoves = moveList.ToArray();
            data.TargetSerializedObject.Update();
        }

        static void WeaponComboMoveGridLabels()
        {
            //var splitterW = GUILayout.Width(15);
            //void Next() => GUILayout.Label(gridSeparator, splitterW);

            using (new EditorGUILayout.HorizontalScope())
            {
                // ReSharper disable StringLiteralTypo
                //Rect scale = GUILayoutUtility.GetLastRect();
                GUILayout.Space(22f); //Next();

                GUILayout.Label(k_dmg, k_gridW); //Next();
                GUILayout.Label(k_kickback, k_gridW); //Next();

                GUILayout.Label(k_attackSpeed, k_gridW); //Next();
                GUILayout.Label(k_recoverSpeed, k_gridW); //Next();
                GUILayout.Label(k_cooldown, k_gridW); //Next();
                GUILayout.Label(k_motionFactor, k_gridW); //Next();
                GUILayout.Label(k_energyCost, k_gridW); //Next();

                GUILayout.Label(k_range, k_gridW); //Next();
                GUILayout.Label(k_addBodyRange, k_gridW); //Next();
                GUILayout.Label(k_weaponRangeFac, k_gridW); //Next();
                GUILayout.Label(k_wIdx, k_gridW); //Next();

                GUILayout.Label(k_attackTime, k_gridW); //Next();

                GUILayout.Label(k_animState, k_animStateW); //Next();
                GUILayout.Label(k_fxHeader);
                // ReSharper restore StringLiteralTypo
            }
        }
    }
}
