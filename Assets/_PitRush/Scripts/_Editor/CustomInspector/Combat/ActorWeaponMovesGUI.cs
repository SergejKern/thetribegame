﻿using System;
using Game.Configs.Combat;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace _Editor.CustomInspector.Combat
{
    /// <summary>
    /// GUI for WeaponConfig.ActorWeaponMoves
    /// </summary>
    public static class ActorWeaponMovesGUI
    {
        public struct Data
        {
            public SerializedObject TargetSerializedObject;
            public Color BackgroundColor;
            public string[] WeaponMoveTypes;

            public RuntimeAnimatorController NewActorController;
            public int NewWeaponMovesTypeIdx;

            public WeaponComboMoveSetGUI.Data ChildGUIData;
        }

        public static void DrawList(ref Data data,
            ref WeaponConfig.ActorWeaponMoves[] actorWeaponMoves,
            SerializedProperty actorWeaponMovesListProp)
        {
            var deleteIdx = -1;

            var bg = GUI.backgroundColor;
            GUI.backgroundColor = data.BackgroundColor;

            for (var i = 0; i < actorWeaponMoves.Length; i++)
            {
                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    var childProp = actorWeaponMovesListProp.GetArrayElementAtIndex(i);

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(20f)))
                            deleteIdx = i;
                        GUILayout.Space(20f);
                        childProp.isExpanded = EditorGUILayout.Foldout(childProp.isExpanded, actorWeaponMoves[i].Name, true);
                    }

                    if (childProp.isExpanded)
                        Draw(data, ref actorWeaponMoves, i, childProp);
                }
            }

            if (deleteIdx != -1)
                RemoveAt(ref actorWeaponMoves, deleteIdx);
            

            AddGUI(ref data, ref actorWeaponMoves);

            GUI.backgroundColor = bg;
        }

        static void Draw(Data data, ref WeaponConfig.ActorWeaponMoves[] movesList, int elementIdx,
            SerializedProperty elementProp)
        {
            var moves = movesList[elementIdx];
            Draw(data, ref moves, ref movesList, elementProp);
            movesList[elementIdx] = moves;
        }

        //public static void Draw(Data data, ref WeaponConfig.ActorWeaponMoves moves, SerializedProperty elementProp)
        //{
        //    WeaponConfig.ActorWeaponMoves[] actorWeaponMoves = null;
        //    Draw(data, ref moves, ref actorWeaponMoves, elementProp);
        //}

        static void Draw(Data data,
            ref WeaponConfig.ActorWeaponMoves moves,
            ref WeaponConfig.ActorWeaponMoves[] actorWeaponMovesList,
            SerializedProperty elementProp)
        {
            GUILayout.Space(10f);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var actorController = EditorGUILayout.ObjectField("ActorController: ", moves.ActorController,
                    typeof(RuntimeAnimatorController), false) as RuntimeAnimatorController;

                var moveType = (WeaponMovesType)EditorGUILayout.EnumPopup(nameof(WeaponMovesType),
                    moves.WeaponMovesType);

                data.ChildGUIData.PassDownController = actorController;
                WeaponComboMoveSetGUI.DrawList(data.ChildGUIData, ref moves.MoveSets, 
                    elementProp.FindPropertyRelative(nameof(WeaponConfig.ActorWeaponMoves.MoveSets)));

                if (!check.changed)
                    return;

                if (actorWeaponMovesList != null)
                {
                    var existingIdx = Array.FindIndex(actorWeaponMovesList, a =>
                        a.ActorController == actorController && a.WeaponMovesType == moveType);

                    if (existingIdx != -1) // cannot apply change, todo 2: inform user
                        return;
                }

                moves.WeaponMovesType = moveType;
                moves.ActorController = actorController;
            }
        }

        static void AddGUI(ref Data data,
            ref WeaponConfig.ActorWeaponMoves[] actorWeaponMovesList)
        {
            using (new GUILayout.VerticalScope($"Add {nameof(WeaponConfig.ActorWeaponMoves)}", EditorStyles.helpBox))
            {
                GUILayout.Space(20f);

                data.NewActorController = EditorGUILayout.ObjectField("ActorController: ", data.NewActorController,
                    typeof(RuntimeAnimatorController), false) as RuntimeAnimatorController;

                data.NewWeaponMovesTypeIdx = EditorGUILayout.Popup(nameof(WeaponMovesType),
                    data.NewWeaponMovesTypeIdx,
                    data.WeaponMoveTypes);

                var newWeaponMovesType = (WeaponMovesType) data.NewWeaponMovesTypeIdx;

                var disabled = data.NewActorController == null || data.NewWeaponMovesTypeIdx == -1;
                var exists = false;
                if (!disabled)
                {
                    var controller = data.NewActorController;
                    var existingIdx = Array.FindIndex(actorWeaponMovesList, a =>
                        a.ActorController == controller && a.WeaponMovesType == newWeaponMovesType);
                    exists = existingIdx != -1;
                    disabled = exists;
                }

                if (exists)
                    EditorGUILayout.HelpBox("Element with these Values already exists!", MessageType.Info);

                using (new EditorGUI.DisabledScope(disabled))
                {
                    if (GUILayout.Button("Add"))
                        AddActorWeaponMove(data.TargetSerializedObject, ref actorWeaponMovesList,
                            data.NewActorController, data.NewWeaponMovesTypeIdx);
                }
            }
        }

        static void AddActorWeaponMove(SerializedObject targetSObject,
            ref WeaponConfig.ActorWeaponMoves[] actorWeaponMovesList,
            RuntimeAnimatorController newActorController,
            int newWeaponMovesTypeIdx)
        {
            var newWeaponMovesType = (WeaponMovesType) newWeaponMovesTypeIdx;

            Add(ref actorWeaponMovesList, new WeaponConfig.ActorWeaponMoves()
            {
                ActorController = newActorController,
                WeaponMovesType = newWeaponMovesType,
                MoveSets = new WeaponConfig.WeaponComboMoveSet[0]
            });

            targetSObject.Update();
        }
    }
}
