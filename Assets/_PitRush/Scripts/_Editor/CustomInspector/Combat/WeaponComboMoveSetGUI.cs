﻿using System;
using Game.Configs.Combat;
using GameUtility.Data.PhysicalConfrontation;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace _Editor.CustomInspector.Combat
{
    /// <summary>
    /// GUI for WeaponConfig.WeaponComboMoveSet
    /// </summary>
    public static class WeaponComboMoveSetGUI
    {
        public struct Data
        {
            public SerializedObject TargetSerializedObject;
            public Color BackgroundColor;

            public RuntimeAnimatorController PassDownController;
        }

        public static void DrawList(Data data, 
            ref WeaponConfig.WeaponComboMoveSet[] sets, 
            SerializedProperty setsProp)
        {
            var bg = GUI.backgroundColor;
            GUI.backgroundColor = data.BackgroundColor;

            var deleteIdx = -1;
            for (var i = 0; i < sets.Length; i++)
            {
                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    var childProp = setsProp.GetArrayElementAtIndex(i);

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(20f)))
                            deleteIdx = i;
                        GUILayout.Space(20f);
                        var name = sets[i].AttackTypeID != null 
                            ? sets[i].AttackTypeID.name 
                            : "";

                        childProp.isExpanded = EditorGUILayout.Foldout(childProp.isExpanded, name, true);
                    }

                    if (childProp.isExpanded)
                        Draw(data, ref sets, i, childProp);
                }
            }

            if (deleteIdx != -1)
                RemoveAt(ref sets, deleteIdx);
            
            AddMoveSetGUI(data, ref sets);
            GUI.backgroundColor = bg;
        }

        //public static void Draw(Data data, ref WeaponConfig.WeaponComboMoveSet moveSet, SerializedProperty elementProp)
        //{
        //    WeaponConfig.WeaponComboMoveSet[] comboMoveSets = null;
        //    Draw(data, ref moveSet, ref comboMoveSets, elementProp);
        //}

        static void Draw(Data data, ref WeaponConfig.WeaponComboMoveSet[] sets, int elementIdx, SerializedProperty elementProp)
        {
            var moveSet = sets[elementIdx];
            Draw(data, ref moveSet, ref sets, elementProp);
            sets[elementIdx] = moveSet;
        }

        static void Draw(Data data, ref WeaponConfig.WeaponComboMoveSet moveSet,
            ref WeaponConfig.WeaponComboMoveSet[] moveSetList,
            SerializedProperty elementProp)
        {
            GUILayout.Space(10f);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var attackType = (AttackTypeID) EditorGUILayout.ObjectField(moveSet.AttackTypeID, typeof(AttackTypeID), false);

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    const string moveName = nameof(WeaponConfig.WeaponComboMoveSet.Moves);
                    var moveProp = elementProp.FindPropertyRelative(moveName);

                    moveProp.isExpanded = EditorGUILayout.Foldout(moveProp.isExpanded, moveName, true);

                    if (moveProp.isExpanded)
                    {
                        WeaponComboMoveGUI.Draw(new WeaponComboMoveGUI.Data()
                        {
                            PassThroughController = data.PassDownController,
                            TargetSerializedObject = data.TargetSerializedObject
                        }, moveProp, ref moveSet.Moves);
                    }
                }

                if (!check.changed)
                    return;

                if (moveSetList != null)
                {
                    var existingIdx = Array.FindIndex(moveSetList, a => a.AttackTypeID == attackType);
                    if (existingIdx != -1) // cannot apply change, todo 2: inform user
                        return;
                }

                moveSet.AttackTypeID = attackType;
            }
        }

        static void AddMoveSetGUI(Data data,
            ref WeaponConfig.WeaponComboMoveSet[] sets)
        {
            using (new GUILayout.VerticalScope($"Add {nameof(WeaponConfig.WeaponComboMoveSet)}", EditorStyles.helpBox))
            {
                GUILayout.Space(20f);

                var attackTypeId = (AttackTypeID) EditorGUILayout.ObjectField(null, typeof(AttackTypeID), false);

                if (attackTypeId == null)
                    return;

                if (Array.FindIndex(sets, a => a.AttackTypeID == attackTypeId) != -1)
                    return; // todo 3: inform user why

                Add(ref sets, new WeaponConfig.WeaponComboMoveSet()
                {
                    AttackTypeID = attackTypeId,
                    Moves = new WeaponConfig.WeaponComboMove[0]
                });
                data.TargetSerializedObject.Update();
            }
        }
    }
}
