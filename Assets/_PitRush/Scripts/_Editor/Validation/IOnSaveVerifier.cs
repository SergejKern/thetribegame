﻿using System.Collections.Generic;
using Core.Unity.Interface;
using UnityEngine;

namespace _Editor.Validation
{
    public interface IOnSaveVerifier
    {
        void VerifyScene(ref VerificationResult res);
        void Verify(ref VerificationResult res, List<GameObject> prefabs);
        void Verify(ref VerificationResult res, List<ScriptableObject> configs);

        void Finish();
    }
}
