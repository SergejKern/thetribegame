﻿using System.Collections.Generic;
using System.Linq;
using Core.Unity.Interface;
using JetBrains.Annotations;
using UnityEngine;

namespace _Editor.Validation
{
    // referenced by reflection
    [UsedImplicitly]
    public class DefaultVerifier : IOnSaveVerifier
    {
        public void VerifyScene(ref VerificationResult result)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            var verifiers = Object.FindObjectsOfType<MonoBehaviour>().OfType<IVerify>();
            foreach (var v in verifiers)
                v.Verify(ref result);
        }

        public void Verify(ref VerificationResult result, List<GameObject> prefabs)
        {
            foreach (var p in prefabs)
            {
                var verifiers = p.GetComponentsInChildren<IVerify>();
                foreach (var v in verifiers)
                    v.Verify(ref result);
            }
        }

        public void Verify(ref VerificationResult result, List<ScriptableObject> configs)
        {
            foreach (var c in configs)
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                if(c is IVerify v)
                    v.Verify(ref result);
            }
        }

        public void Finish() { }
    }
}
