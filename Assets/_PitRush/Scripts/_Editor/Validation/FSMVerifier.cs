﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Unity.Interface;
using Core.Unity.Operations;
using Core.Extensions;
using Core.Types;
using Core;
using Core.Interface;
using GameUtility.Validation;
using JetBrains.Annotations;
using UnityEngine;

namespace _Editor.Validation
{
    //referenced by reflection
    [UsedImplicitly]
    public class FSMVerifier : IOnSaveVerifier
    {
        readonly Dictionary<object, FsmOperations.EventsAndStatesExtracted> m_fsmDict = new Dictionary<object, FsmOperations.EventsAndStatesExtracted>();
        public void VerifyScene(ref VerificationResult result)
        {
            m_fsmDict.Clear();

            var fsmValidations = Object.FindObjectsOfType<MonoBehaviour>().OfType<IFSMBehaviorValidation>();
            foreach (var validator in fsmValidations)
            {
                GetFSMEventAndStateRefs(validator, out var events, out var states, out var variableRefs);
                if (Validate(states, events, variableRefs, out var error) != OperationResult.Error)
                    continue;
                result.Error($"{nameof(FSMVerifier)}: Errors on {(validator as MonoBehaviour)?.gameObject}: \n{error}", 
                    (validator as MonoBehaviour)?.gameObject);
            }
        }

        public void Verify(ref VerificationResult result, List<GameObject> prefabs)
        {
            foreach (var prefab in prefabs)
            {
                var validator = prefab.GetComponentInChildren<IFSMBehaviorValidation>();
                if (validator == null)
                     continue;
                GetFSMEventAndStateRefs(validator, out var events, out var states, out var variableRefs);

                if (Validate(states, events, variableRefs, out var error) != OperationResult.Error)
                    continue;

                result.Error($"{nameof(FSMVerifier)}: Errors on {(validator as MonoBehaviour)?.gameObject}: \n{error}",
                    (validator as MonoBehaviour)?.gameObject);
            }
        }

        public void Verify(ref VerificationResult result, List<ScriptableObject> configs)
        {
            foreach (var c in configs)
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                if (!(c is IFSMBehaviorValidation validator))
                    continue;

                GetFSMEventAndStateRefs(validator, out var events, out var states, out var variableRefs);
                if (Validate(states, events, variableRefs, out var error) != OperationResult.Error)
                    continue;

                result.Error(error, c);
            }
        }

        OperationResult Validate(IEnumerable<IFSMStateRef> states, IEnumerable<IFSMEventRef> events, IEnumerable<IFSMVariableRef> variables, out string error)
        {
            error = "";

            ValidateStates(states, ref error);
            ValidateEvents(events, ref error);
            ValidateVariables(variables, ref error);

            return error.IsNullOrEmpty() ? OperationResult.OK : OperationResult.Error;
        }

        void ValidateStates(IEnumerable<IFSMStateRef> states, ref string error)
        {
            foreach (var s in states)
            {
                if (s.FSM == null)
                {
                    error += "FSM reference is missing!\n";
                    continue;
                }
                if (s.StateName.IsNullOrEmpty() || s.StateName.Equals(Name.Ignore))
                    continue;

                m_fsmDict.TryGetValue(s.FSM, out var extracted);
                if (extracted.StateNames == null)
                {
                    extracted = FsmOperations.Extract((s.FSM.FsmTemplate != null) ? s.FSM.FsmTemplate.fsm : s.FSM.Fsm);
                    m_fsmDict[s.FSM] = extracted;
                }

                if (!extracted.StateNames.Contains(s.StateName))
                    error += $"Unknown state: {s.StateName}!\n";
            }
        }

        void ValidateEvents(IEnumerable<IFSMEventRef> events, ref string error)
        {
            foreach (var e in events)
            {
                if (e.FSM == null)
                {
                    error += "FSM reference is missing!\n";
                    continue;
                }
                if (e.EventName.IsNullOrEmpty() || e.EventName.Equals(Name.Ignore))
                    continue;

                m_fsmDict.TryGetValue(e.FSM, out var extracted);
                if (extracted.EventNames == null)
                {
                    extracted = FsmOperations.Extract((e.FSM.FsmTemplate != null) ? e.FSM.FsmTemplate.fsm : e.FSM.Fsm);
                    m_fsmDict[e.FSM] = extracted;
                }

                if (!extracted.EventNames.Contains(e.EventName))
                    error += $"Unknown event: {e.EventName}!\n";
            }
        }

        // todo 1: variable verification not implemented
        // ReSharper disable MemberCanBeMadeStatic.Local, UnusedParameter.Local
        void ValidateVariables(IEnumerable<IFSMVariableRef> variables, ref string error)
        {
        }
        // ReSharper restore MemberCanBeMadeStatic.Local, UnusedParameter.Local

        public void Finish()
        {
        }

        static void GetFSMEventAndStateRefs(IFSMBehaviorValidation behaviour, 
            out IEnumerable<IFSMEventRef> events, 
            out IEnumerable<IFSMStateRef> states,
            out IEnumerable<IFSMVariableRef> variables)
        {
            const BindingFlags bindingFlags = BindingFlags.Instance |
                                              BindingFlags.NonPublic |
                                              BindingFlags.Public;

            events = behaviour.GetType()
                .GetFields(bindingFlags)
                .Select(field => field.GetValue(behaviour)).OfType<IFSMEventRef>();

            states = behaviour.GetType()
                .GetFields(bindingFlags)
                .Select(field => field.GetValue(behaviour)).OfType<IFSMStateRef>();

            variables = behaviour.GetType()
                .GetFields(bindingFlags)
                .Select(field => field.GetValue(behaviour)).OfType<IFSMVariableRef>();
        }
    }

    // todo 3: remove this or FSMVerifier
    public class IgnoreSuspiciousCast : MonoBehaviour, IFSMBehaviorValidation
    {
    }
}
