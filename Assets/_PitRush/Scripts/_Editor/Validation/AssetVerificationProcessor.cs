﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Editor.Extensions;
using Core.Extensions;
using Core.Unity.Interface;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Editor.Validation
{
    // ReSharper disable once HollowTypeName
    public class AssetVerificationProcessor : UnityEditor.AssetModificationProcessor
    {
        static List<IOnSaveVerifier> m_onSaveVerifier;

        [MenuItem("Tools/Verification/VerifyScene")]
        static void VerifyScene()
        {
            VerifyPrefabsDelayed(null);
        }

        static void CreateVerifier()
        {
            if (!m_onSaveVerifier.IsNullOrEmpty())
                return;
            m_onSaveVerifier = new List<IOnSaveVerifier>();
            var types = new List<Type>();

            EditorExtensions.ReflectionGetAllTypes(typeof(IOnSaveVerifier), ref types, out _, out _);
            foreach (var t in types)
            {
                var verifier = (IOnSaveVerifier) Activator.CreateInstance(t);
                m_onSaveVerifier.Add(verifier);
            }
        }

        public static string[] OnWillSaveAssets(string[] paths)
        {
            var isSavingScene = false;
            var isSavingCurrentPrefab = false;

            var modifiedPrefabs = new List<string>();
            var modifiedConfigs = new List<string>();

            GetCurrentPrefab(out var currentPrefab, out var prefabRoot);
            if (!CheckRelevantAssets(paths, currentPrefab, 
                modifiedPrefabs, modifiedConfigs,
                ref isSavingCurrentPrefab, ref isSavingScene))
                return paths;

            CreateVerifier();

            if (isSavingScene)
                DoVerifyScene();

            if (isSavingCurrentPrefab)
                DoVerifyPrefab(prefabRoot);
            if (!modifiedConfigs.IsNullOrEmpty())
                VerifyConfigs(modifiedConfigs);

            if (modifiedPrefabs.IsNullOrEmpty())
                FinishVerification();
            else
                VerifyPrefabsDelayed(modifiedPrefabs);

            return paths;
        }

        static void FinishVerification()
        {
            foreach (var verifier in m_onSaveVerifier)
                verifier.Finish();
        }

        static bool CheckRelevantAssets(IEnumerable<string> paths, string currentPrefab, 
            ICollection<string> modifiedPrefabs, ICollection<string> modifiedConfigs,
            ref bool isSavingCurrentPrefab,
            ref bool isSavingScene)
        {
            foreach (var path in paths)
            {
                if (currentPrefab != null && string.Equals(path, currentPrefab))
                    isSavingCurrentPrefab = true;
                else if (string.Equals(Path.GetExtension(path), ".prefab"))
                    modifiedPrefabs.Add(path);
                if (string.Equals(Path.GetExtension(path), ".asset"))
                    modifiedConfigs.Add(path);
                if (string.Equals(Path.GetExtension(path), ".unity"))
                    isSavingScene = true;
            }
            return isSavingScene || isSavingCurrentPrefab || !modifiedPrefabs.IsNullOrEmpty();
        }

        static void GetCurrentPrefab(out string currentPrefab, out GameObject prefabRoot)
        {
            currentPrefab = null;
            prefabRoot = null;

            var stage = PrefabStageUtility.GetCurrentPrefabStage();
            if (stage == null)
                return;

            currentPrefab = stage.assetPath;
            prefabRoot = stage.prefabContentsRoot;
        }

        static void VerifyPrefabsDelayed(IReadOnlyCollection<string> modifiedPrefabs)
        {
            //! wait until prefab is saved, because I don't know how to get the data otherwise
            if (!modifiedPrefabs.IsNullOrEmpty())
                EditorApplication.delayCall += () => { OnPrefabsSaved(modifiedPrefabs); };
        }

        static void DoVerifyScene()
        {
            Debug.Log($"{nameof(AssetVerificationProcessor)}: Verifying Scene '{SceneManager.GetActiveScene().name}' . . . ");

            var result = VerificationResult.Default;
            foreach (var verifier in m_onSaveVerifier)
                verifier.VerifyScene(ref result);

            Log(result);
            if (result.Errors > 0)
            {
                Debug.LogError($"{nameof(AssetVerificationProcessor)}: the Scene " +
                               $"'{SceneManager.GetActiveScene().name}' has {result.Errors} Errors! "
                               + "Please fix them");
            }
        }

        static void DoVerifyPrefab(GameObject prefabRoot)
        {
            var prefabList = new List<GameObject>(){ prefabRoot };
            Debug.Log($"{nameof(AssetVerificationProcessor)}: Verifying Prefab '{prefabRoot.name}' . . . ");

            var result = VerificationResult.Default;

            foreach (var verifier in m_onSaveVerifier)
                verifier.Verify(ref result, prefabList);

            Log(result);

            if (result.Errors > 0)
                Debug.LogError($"{nameof(AssetVerificationProcessor)}: the Prefab " +
                               $"'{prefabRoot.name}' has {result.Errors} Errors! "
                               + "Please fix them");
        }

        static void VerifyConfigs(IEnumerable<string> modifiedConfigs)
        {
            var scriptable = modifiedConfigs.Select(AssetDatabase.LoadAssetAtPath<ScriptableObject>).ToList();

            Debug.Log($"{nameof(AssetVerificationProcessor)}: Verifying Configs . . . ");

            var result = VerificationResult.Default;
            foreach (var verifier in m_onSaveVerifier)
                verifier.Verify(ref result, scriptable);

            Log(result);

            if (result.Errors > 0)
                Debug.LogError($"{nameof(AssetVerificationProcessor)}: A saved Config " +
                               $"' has {result.Errors } Errors! Please fix them");
        }

        static void OnPrefabsSaved(IEnumerable<string> prefabPaths)
        {
            var gameObjects = prefabPaths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToList();

            Debug.Log($"{nameof(AssetVerificationProcessor)}: Verifying Prefabs . . . ");

            var result = VerificationResult.Default;
            foreach (var verifier in m_onSaveVerifier)
                verifier.Verify(ref result, gameObjects);
            
            if (result.Errors > 0)
                Debug.LogError($"{nameof(AssetVerificationProcessor)}: A saved Prefab " +
                               $"' has {result.Errors} Errors! Please fix them");

            FinishVerification();
        }

        static void Log(VerificationResult result)
        {
            foreach (var i in result.InfoLogs)
                Debug.Log(i.LogString, i.Context);
            foreach (var w in result.WarningLogs)
                Debug.LogWarning(w.LogString, w.Context);
            foreach (var e in result.ErrorLogs)
                Debug.LogError(e.LogString, e.Context);
        }
    }
}
