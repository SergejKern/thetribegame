﻿//using System.Collections.Generic;
//using System.Linq;
//using Core.Interface;
//using UnityEngine;
//using UnityEditor;
//using GameUtility.Validation;
//namespace _Editor.Validation
//{
//    //_! referenced by reflection
//    // ReSharper disable once UnusedMember.Global
//    public class FMODVerifier : IOnSaveVerifier
//    {
//        public void VerifyScene(ref VerificationResult res)
//        {
//            if (CancelOnPlay(ref res))
//                return;
//            // ReSharper disable once SuspiciousTypeConversion.Global
//            var fmodValidations = Object.FindObjectsOfType<MonoBehaviour>().OfType<IAudioValidation>();
//            foreach (var validator in fmodValidations)
//            {
//                if (validator.IsAudioValid(out var error))
//                    continue;
//                res.ErrorLogs.Add(error);
//            }
//#if PLAYMAKER
//            var playMakerScripts = Object.FindObjectsOfType<PlayMakerFSM>();
//            foreach (var script in playMakerScripts)
//            {
//                foreach (var state in script.FsmStates)
//                {
//                    // todo 1: how to check if not initialized?
//                    if (!state.IsInitialized)
//                    {
//                        Debug.LogWarning($"Cannot verify state {state.Name}, because it is not initialized!");
//                        continue;
//                    }
//                    var fmodValidationsFSM = state.Actions.OfType<IAudioValidationFSM>();
//                    foreach (var fsmAction in fmodValidationsFSM)
//                    {
//                        if(fsmAction.IsAudioValid(script, state.Name, out var error))
//                            continue;
//                        res.ErrorLogs.Add(error);
//                    }
//                }
//            }
//#endif
//        }
//        static bool CancelOnPlay(ref VerificationResult res)
//        {
//            // this is needed or we won't have sound in game, because we release FMOD later
//            if (!EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
//                return false;
//            res.WasCanceled = true;
//            return true;
//        }
//        public void Verify(ref VerificationResult res, List<GameObject> prefabs)
//        {
//            if (CancelOnPlay(ref res))
//                return;
//            foreach (var prefab in prefabs)
//            {
//                var validator = prefab.GetComponentsInChildren<IAudioValidation>();
//                foreach (var val in validator)
//                {
//                    if (val.IsAudioValid(out var error))
//                        continue;
//                    res.ErrorLogs.Add(error);
//                }
//            }
//        }
//        public void Verify(ref VerificationResult res, List<ScriptableObject> configs)
//        {
//            if (CancelOnPlay(ref res))
//                return;
//            foreach (var c in configs)
//            {
//                // ReSharper disable once SuspiciousTypeConversion.Global
//                if (_!(c is IAudioValidation validator))
//                    continue;
//                if (validator.IsAudioValid(out var error))
//                    continue;
//                res.ErrorLogs.Add(error);
//            }
//        }
//        public void Finish()
//        {
//            EditorApplication.delayCall += () =>
//            {
//                //FMODUnity.RuntimeManager.StudioSystem.release();
//                //Object.DestroyImmediate(Resources.FindObjectsOfTypeAll<FMODUnity.RuntimeManager>()[0].gameObject);
//            };
//        }
//    }
//}
