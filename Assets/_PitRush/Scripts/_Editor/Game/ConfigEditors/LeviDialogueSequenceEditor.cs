﻿using System.Collections;
using Core.Unity.Extensions;
using Game.Configs.Elevator;
using UnityEditor;
using UnityEngine;

namespace _Editor.Game.ConfigEditors
{
    [CustomEditor(typeof(LeviDialogueSequence))]
    public class LeviDialogueSequenceEditor : Editor
    {
        // ReSharper disable once InconsistentNaming
        new LeviDialogueSequence target => base.target as LeviDialogueSequence;

        //bool m_dialogueRunning;
        GameObject m_audioPreviewObject;
        string m_dialogueText;
        float m_lastTime;
        const float k_timeBetweenCharacters = 0.1f;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Play"))
            {
                if (m_audioPreviewObject != null)
                    return;

                m_audioPreviewObject = new GameObject() { hideFlags = HideFlags.HideAndDontSave };
                EditorCoroutine.StartCoroutine(Dialogue());
            }

            if (m_audioPreviewObject != null)
            {
                Repaint();
            }
            GUILayout.TextArea(m_dialogueText);
        }

        IEnumerator Dialogue()
        {
            Debug.Log("Dialogue!");
            m_dialogueText = "";

            foreach (var element in target.DialogueElements)
            {
                float time;
                Debug.Log($"delay {element.Delay}");

                if (element.Delay > 0f)
                {
                    time = Time.realtimeSinceStartup;
                    while (Time.realtimeSinceStartup - time < element.Delay)
                        yield return null;
                }

                var textLength = element.Text.Length;
                m_dialogueText = "";

                string sound = null;
                AK.Wwise.Event @event = null;
                for (var i = 0; i < textLength; i++)
                {
                    var c = element.Text[i];
                    if (c.Equals('[')) 
                        sound = "";
                    else if (c.Equals(']'))
                    {
                        if (int.TryParse(sound, out var soundIdx))
                        {
                            @event?.Stop(m_audioPreviewObject);
                            @event = element.SoundSet[soundIdx];
                            Debug.Log($"Playing: {@event.Name}");
                            @event.Post(m_audioPreviewObject);
                        }   
                        sound = null;
                    }   
                    else if (sound != null)
                        sound += c;
                    else
                    {
                        m_dialogueText += c;
                        //m_characterAudio?.Result?.Play(gameObject);
                        time = Time.realtimeSinceStartup;
                        while (Time.realtimeSinceStartup - time < k_timeBetweenCharacters)
                            yield return null;
                    }
                }
                Debug.Log($"text element done!");

                time = Time.realtimeSinceStartup;
                while (Time.realtimeSinceStartup - time < element.ClearAfterSeconds)
                    yield return null;
                m_dialogueText = "";
            }
            m_audioPreviewObject.DestroyEx();
            m_audioPreviewObject = null;
        }
    }
}