using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Inspector;
using Core.Extensions;
using Core.Unity.Utility.GUITools;
using Game.Configs;
using GameUtility.Config;
using GameUtility.Data.PhysicalConfrontation;
using UnityEditor;
using UnityEngine;

using static Core.Extensions.CollectionExtensions;

namespace _Editor.Game.ConfigEditors
{
    [CustomEditor(typeof(CollisionsConfig))]
    public class CollisionsConfigEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new CollisionsConfig target => base.target as CollisionsConfig;

        CollisionMaterialID[] m_materialIds;

        Vector2 m_scrollPos;
        Rect m_lastGUIRect;
        int m_selectedIdx;

        const float k_elementSize = 27f;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Update Material IDs for Matrix") || m_materialIds.IsNullOrEmpty())
                UpdateAllMaterialIds();
            if (GUILayout.Button("Sort"))
                SortElements();
            if (GUILayout.Button("Remove Empties"))
                RemoveEmpties();

            var size = m_materialIds.Length * k_elementSize + 100f;

            GUILayout.Space(20f);
            using (var scope = new EditorGUILayout.ScrollViewScope(m_scrollPos,
                true, false, GUILayout.Width(size + 100f)))
            {
                m_scrollPos.x = scope.scrollPosition.x;
                GUILayout.Box("", GUIStyle.none, GUILayout.Width(size), GUILayout.Height(20f));
            }
            GUILayout.Box("", GUIStyle.none, GUILayout.Width(size), GUILayout.Height(size));

            m_lastGUIRect = GUILayoutUtility.GetLastRect();
            var newGuiPoint = m_lastGUIRect.position - m_scrollPos;
            Debug.Log(newGuiPoint);

            var pivotPoint = new Vector2(newGuiPoint.x, newGuiPoint.y + 100);
            DrawMatrix(pivotPoint, pivotPoint, new Vector2(-100f, -(m_materialIds.Length * k_elementSize + 100f)));

            DrawSelectedElement();
        }

        void RemoveEmpties()
        {
            var list = target.CollisionDataList.ToList();
            list.RemoveAll(a => a.Effect == null && a.Audio?.Object == null);
            target.CollisionDataList = list.ToArray();
        }

        void SortElements()
        {
            for (var i = 0; i < target.CollisionDataList.Length; i++)
            {
                var element = target.CollisionDataList[i];
                var compared = string.Compare(element.MatA.name, element.MatB.name, 
                    StringComparison.Ordinal);
                if (compared <= 0)
                    continue;
                
                var matB = element.MatB;
                element.MatB = element.MatA;
                element.MatA = matB;
                target.CollisionDataList[i] = element;
            }
            Array.Sort(target.CollisionDataList, (a,b) => 
                string.Compare($"{a.MatA.name}{a.MatB.name}", $"{b.MatA.name}{b.MatB.name}", 
                StringComparison.Ordinal));
        }
        void DrawSelectedElement()
        {
            if (m_selectedIdx == -1)
                return;
            if (!m_selectedIdx.IsInRange(target.CollisionDataList))
                return;
            GUILayout.Label($"Selected element {m_selectedIdx}: ");
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var selectedProp = serializedObject.FindProperty(nameof(CollisionsConfig.CollisionDataList));
                EditorGUILayout.PropertyField(selectedProp.GetArrayElementAtIndex(m_selectedIdx));
                if (check.changed)
                    serializedObject.ApplyModifiedProperties();
            }
        }

        void DrawMatrix(Vector2 mainPos, Vector2 pivot, Vector2 rotatedOffset)
        {
            for (var i = 0; i < m_materialIds.Length; i++)
            {
                var m = m_materialIds[i];
                var pos = new Vector2(mainPos.x, mainPos.y + (i * k_elementSize));
                GUI.Label(new Rect(pos.x, pos.y, 100f, k_elementSize), m.name);

                for (var j= 0; j < m_materialIds.Length-i; j++)
                    DrawElement(i, j, pos);
                
                GUIUtility.RotateAroundPivot(90f, pivot);
                GUI.Label(new Rect(pos.x + rotatedOffset.x, pos.y + rotatedOffset.y, 200f, k_elementSize), m.name);
                GUIUtility.RotateAroundPivot(-90f, pivot);
            }
        }

        void DrawElement(int i, int j, Vector2 pos)
        {
            var matA = m_materialIds[i];
            var matB = m_materialIds[m_materialIds.Length - 1 - j];
            var cData = GetCollisionData(matA, matB, out var idx);
            var str = " ";

            if (cData.Audio?.Object != null)
                str = "A";
            if (cData.Effect)
                str += "V";
            var selected = idx == m_selectedIdx;
            var warnColor = new Color(1f, 0.75f, 0f);
            var isUnitySound = cData.Audio?.Result is AudioClipAsset;
            using (new ColorScope(isUnitySound ? warnColor : GUI.color))
            {
                if (!GUI.Toggle(new Rect(pos.x + 100f + (j * k_elementSize), pos.y, k_elementSize, k_elementSize),
                    selected, str, "Button")) 
                    return;
            }

            if (idx != -1)
                m_selectedIdx = idx;
            else
            {
                Add(ref target.CollisionDataList, new CollisionsConfig.CollisionData() {MatA = matA, MatB = matB});
                m_selectedIdx = target.CollisionDataList.Length - 1;
            }
        }

        CollisionsConfig.CollisionData GetCollisionData(CollisionMaterialID a, CollisionMaterialID b, out int collisionDataIdx)
        {
            bool Find(CollisionsConfig.CollisionData dat) => 
                (dat.MatA == a && dat.MatB == b) || (dat.MatB == a && dat.MatA == b);

            collisionDataIdx = Array.FindIndex(target.CollisionDataList, Find);
            return collisionDataIdx == -1 ? default : target.CollisionDataList[collisionDataIdx];
        }

        void UpdateAllMaterialIds()
        {
            var allMaterialIdGuids = AssetDatabase.FindAssets($"t:{nameof(CollisionMaterialID)}").Distinct().ToArray();
            var materialIds = new List<CollisionMaterialID>();

            foreach (var matGuid in allMaterialIdGuids)
            {
                var objs= AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GUIDToAssetPath(matGuid));
                foreach (var o in objs)
                {
                    if (!(o is CollisionMaterialID matId))
                        continue;
                    if (materialIds.Contains(matId))
                        continue;
                    materialIds.Add(matId);
                }
            }
            materialIds.Sort((a,b)=> string.Compare(a.name, b.name, 
                StringComparison.Ordinal));
            m_materialIds = materialIds.ToArray();
        }
    }
}