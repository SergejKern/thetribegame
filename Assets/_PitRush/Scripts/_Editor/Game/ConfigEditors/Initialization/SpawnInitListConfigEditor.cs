﻿using System;
using System.IO;
using Core.Editor.Inspector;
using Core.Extensions;
using GameUtility.InitSystem.Config;
using UnityEditor;
using UnityEngine;

namespace _Editor.Game.ConfigEditors.Initialization
{
    [CustomEditor(typeof(SpawnInitListConfig))]
    public class SpawnInitListConfigEditor : BaseInspector
    {
        struct NestedEditor
        {
            public bool ShowEditor;
            public Editor Editor;
        }

        // ReSharper disable once InconsistentNaming
        new SpawnInitListConfig target => base.target as SpawnInitListConfig;

        NestedEditor[] m_cachedInitConfigEditors = new NestedEditor[0];
        ref NestedEditor GetEditorData(int idx) => ref m_cachedInitConfigEditors[idx];

        const string k_prefKeyEditorShown = "SpawnInitListConfigEditor_EditorShown_";

        public override void Init()
        {
            base.Init();
            AdjustCachedEditorArray();
        }

        void AdjustCachedEditorArray()
        {
            if (m_cachedInitConfigEditors.Length == target.InitConfigs.Count) return;
            Array.Resize(ref m_cachedInitConfigEditors, target.InitConfigs.Count);
            LoadVisibility();
        }
        void LoadVisibility()
        {
            for (var i = 0; i < m_cachedInitConfigEditors.Length; i++)
            {
                ref var edDat = ref GetEditorData(i);
                edDat.ShowEditor = EditorPrefs.GetBool(k_prefKeyEditorShown + i);
            }
        }

        void SaveVisibility()
        {
            for (var i = 0; i < m_cachedInitConfigEditors.Length; i++)
            {
                ref var edDat = ref GetEditorData(i);
                EditorPrefs.SetBool(k_prefKeyEditorShown + i, edDat.ShowEditor);
            }
        }

        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();
            AdjustCachedEditorArray();

            bool visibilityToggled = false;
            var deleteIdx = -1;
            for (var i = 0; i < target.InitConfigs.Count; i++)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("X", GUILayout.Width(20)))
                        deleteIdx = i;

                    target.InitConfigs[i] = (SpawnInitConfig) EditorGUILayout.ObjectField(target.InitConfigs[i], typeof(SpawnInitConfig),
                            false);
                }
                if (target.InitConfigs[i] == null)
                    continue;

                NestedEditorGUI(i, ref visibilityToggled);
            }
            if (visibilityToggled)
                SaveVisibility();
            AddSpawnInitConfigGUI();
            if (deleteIdx != -1)
                RemoveSpawnInitConfig(deleteIdx);

            VerificationGUI();
        }

        void NestedEditorGUI(int i, ref bool visibilityToggled)
        {
            ref var ed = ref GetEditorData(i);
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                CreateCachedEditor(target.InitConfigs[i], typeof(SpawnInitConfigEditor), ref ed.Editor);
                var prevShow = ed.ShowEditor;
                ShowNestedEditor(ref ed);
                visibilityToggled |= prevShow != ed.ShowEditor;
            }
        }

        static void ShowNestedEditor(ref NestedEditor ed)
        {
            if (ed.Editor == null) 
                return;
            ed.ShowEditor = EditorGUILayout.Foldout(ed.ShowEditor, ed.Editor.target.name, true);
            if (!ed.ShowEditor) 
                return;
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20);
                using (new EditorGUILayout.VerticalScope())
                    ed.Editor.OnInspectorGUI();
            }
        }

        void RemoveSpawnInitConfig(int deleteIdx) => target.InitConfigs.RemoveAt(deleteIdx);

        string m_newConfigName;
        void AddSpawnInitConfigGUI()
        {
            if (GUILayout.Button("+ Add Slot"))
            {
                target.InitConfigs.Add(null);
                OnChanged();
            }  
            
            m_newConfigName = EditorGUILayout.TextField("New Config Name:",m_newConfigName);
            // todo 3: core tools for path
            // bug ArgumentException: Invalid path f.e. when creating new SpawnInitListConfigEditor
            var path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(target))?.Replace('\\','/');
            GUILayout.Label($"Path: {path}/{m_newConfigName}.asset");

            using (new EditorGUI.DisabledScope(m_newConfigName.IsNullOrEmpty()))
            {
                if (!GUILayout.Button("+ Add File"))
                    return;
            }
            // path = EditorUtility.OpenFolderPanel("Create asset", path, "");
            var newInitConfig = (SpawnInitConfig)CreateInstance(typeof(SpawnInitConfig));
            AssetDatabase.CreateAsset(newInitConfig, $"{path}/{m_newConfigName}.asset");
            AssetDatabase.SaveAssets();
            target.InitConfigs.Add(newInitConfig);
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }
    }
}
