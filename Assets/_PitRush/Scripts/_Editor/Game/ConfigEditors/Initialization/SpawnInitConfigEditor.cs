﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using GameUtility.InitSystem.Config;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

using _Game = Game;
using _Configs = Game.Configs;
using _Init = Game.Configs.Initialization;

namespace _Editor.Game.ConfigEditors.Initialization
{
    [CustomEditor(typeof(SpawnInitConfig))]
    public class SpawnInitConfigEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new SpawnInitConfig target => base.target as SpawnInitConfig;
        readonly Dictionary<Object, Editor> m_cachedEditors = new Dictionary<Object, Editor>();
        List<Type> m_configTypes = new List<Type>();

        GenericMenu m_addMenu;
        static string RootNameSpace => $"{nameof(_Game)}.{nameof(_Configs)}.{nameof(_Init)}";

        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();
            //target.Prefab = (GameObject)EditorGUILayout.ObjectField(target.Prefab, typeof(GameObject), false);
            PrefabGUI();

            // todo 2 GAME_SETTINGS: buttons to apply all to prefab or specific (checkbox?) or revert from prefab

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20);
                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    ConfigsGUI();
                    AddConfigGUI();
                }
            }
            VerificationGUI();
        }

        void PrefabGUI()
        {
            var prefabDatProp = serializedObject.FindProperty(nameof(SpawnInitConfig.PrefabDat));
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(prefabDatProp, true);
                if (!check.changed)
                    return;

                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();
            }
        }

        void AddConfigGUI()
        {
            if (!GUILayout.Button("+"))
                return;
            m_addMenu.ShowAsContext();
        }

        void ConfigsGUI()
        {
            var deleteIdx = -1;
            var configListProperty = serializedObject.FindProperty(nameof(SpawnInitConfig.Configs));
            for (var i = 0; i < target.Configs.Count; i++)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("X", GUILayout.Width(20)))
                        deleteIdx = i;
                    var conf = target.Configs[i];
                    var confProp = configListProperty.GetArrayElementAtIndex(i);

                    if (conf.Object != null)
                        NestedConfigGUI(conf);
                    else // lost reference!
                    {
                        var obj = confProp.objectReferenceValue;
                        EditorGUILayout.PropertyField(confProp);
                        if (confProp.objectReferenceValue != obj)
                        {
                            serializedObject.ApplyModifiedProperties();
                            EditorUtility.SetDirty(target);
                            AssetDatabase.SaveAssets();
                        }
                    }

                    target.Configs[i] = conf;
                }
            }
            RemoveConfig(target, deleteIdx);
        }

        void RemoveConfig(SpawnInitConfig dat, int deleteIdx)
        {
            if (deleteIdx == -1) return;
            RemoveEditorForObj(dat.Configs[deleteIdx].Object);
            dat.Configs[deleteIdx].Object.DestroyEx();
            dat.Configs.RemoveAt(deleteIdx);
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }

        void NestedConfigGUI(RefIInitializationConfig conf)
        {
            var editor = GetEditorForObj(conf.Object);
            // ReSharper disable once ConvertToUsingDeclaration
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                using (new EditorGUILayout.VerticalScope())
                    editor.OnInspectorGUI();

                if (!check.changed)
                    return;

                EditorUtility.SetDirty(conf.Object);
                EditorUtility.SetDirty(target);
                if (!EditorApplication.isPlaying)
                    return;

                RuntimeUpdateChanges(conf);
            }
        }

        void RuntimeUpdateChanges(RefIInitializationConfig conf)
        {
            var prefabMarkers = FindObjectsOfType<MonoBehaviour>().OfType<IPrefabInstanceMarker>().Where(marker => marker.Prefab == target.Prefab);
            foreach (var marker in prefabMarkers)
            {
                var mono = marker as MonoBehaviour;
                if (mono==null)
                    continue;
                
                var comp = mono.GetComponentInChildren(conf.Result.ComponentType, true);
                if (comp == null)
                    continue;
                conf.Result.InitializeComponent(comp);
            }
        }

        Editor GetEditorForObj(Object obj)
        {
            if (m_cachedEditors.TryGetValue(obj, out var editor)) 
                return editor;
            editor = CreateEditor(obj);
            m_cachedEditors.Add(obj, editor);

            return editor;
        }

        void RemoveEditorForObj(Object obj)
        {
            if (!m_cachedEditors.TryGetValue(obj, out var editor))
                return;
            editor.DestroyEx();
            m_cachedEditors.Remove(obj);
        }

        public override void Init()
        {
            base.Init();
            m_configTypes.Clear();
            EditorExtensions.ReflectionGetAllTypes(typeof(IInitializationConfig), ref m_configTypes, out var names, out var namespaces);
            m_addMenu = EditorExtensions.CreateGenericMenuFromTypes(m_configTypes, names, namespaces, RootNameSpace, OnAddMenuTypeSelected);
        }

        void OnAddMenuTypeSelected(object data)
        {
            var type = (Type) data;

            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            var initConfig = (IInitializationConfig) obj;
            var refInitConfig = new RefIInitializationConfig { Result = initConfig };

            InitDefaultData(initConfig);

            AssetDatabase.AddObjectToAsset(obj, target);
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();

            target.Configs.Add(refInitConfig);
            serializedObject.Update();
        }

        void InitDefaultData(IInitializationConfig initConfig)
        {
            if (target.Prefab == null) return;
            var comp = target.Prefab.GetComponentInChildren(initConfig.ComponentType);
            if (comp != null)
                initConfig.InitDataFrom(comp);
        }

        void OnDisable()
        {
            foreach (var ed in m_cachedEditors.Values)
                ed.DestroyEx();
            m_cachedEditors.Clear();
        }
    }
}
