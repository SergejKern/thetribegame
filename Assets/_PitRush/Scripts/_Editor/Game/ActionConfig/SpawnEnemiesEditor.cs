﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Game.ActionConfig;
using Game.Arena;
using UnityEditor;
using UnityEngine;
using _Editor.CustomInspector.EnemySpawning;
using Core.Editor.Inspector;
using Game.Arena.EnemySpawn;
using Game.Arena.Level;
using Game.Configs;
using ScriptableUtility.Variables;
using ScriptableUtility.Variables.Reference;
using ScriptableUtility.Variables.Scriptable;
using static Core.Extensions.CollectionExtensions;
using Object = UnityEngine.Object;

using static _Editor.Game.Arena.LevelPlatformPreview;

namespace _Editor.Game.ActionConfig
{
    [CustomEditor(typeof(SpawnEnemies))]
    public class SpawnEnemiesEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new SpawnEnemies target => base.target as SpawnEnemies;

        static SpawnEnemies m_currentEditSpawnEnemies;
        static int m_currentSpawnerIdx = -1;

        bool m_regularClick;
        Vector2 m_scrollPos;
        int m_generatedSeed;

        static int m_previewSeed;
        static int m_previewPlayerCount = 1;
        static bool m_showPreview;

        static ScriptableInt m_defaultPreviewSeed;
        static ScriptableGameObject m_defaultSpawnerPrefab;

        List<EnemyWaveSpawnEntry> m_previewEntry;
        Vector2 m_previewScrollPos;

        static Object ParentEditObject => ArenaLevelFlowConfig.Current;

        #region Properties/ Quick Data Access
        bool IsPreviewMode => PreviewUpToAction == target && m_currentSpawnerIdx > -1;

        ref SpawnerConfig GetSpawnerConfig(int i)
            => ref target.Spawners[i];
        #endregion

        #region UnityMethods
        public override void Init()
        {
            base.Init();
            SceneView.duringSceneGui += DuringSceneGUI;

            AutoSelectVariables();
        }

        void AutoSelectVariables()
        {
            var changed = false;
            if (target.Seed.Scriptable != null)
                m_defaultPreviewSeed = (ScriptableInt) target.Seed.Scriptable;
            else if (target.Seed.IsNone() && m_defaultPreviewSeed != null)
            {
                target.Seed = new IntReference(m_defaultPreviewSeed);
                changed = true;
            }
            if (target.SpawnerPrefab.Scriptable != null)
                m_defaultSpawnerPrefab = (ScriptableGameObject) target.SpawnerPrefab.Scriptable;
            else if (target.SpawnerPrefab.IsNone() && m_defaultSpawnerPrefab != null)
            {
                target.SpawnerPrefab = new GameObjectReference(m_defaultSpawnerPrefab);
                changed = true;
            }

            if (changed)
                OnChanged();
        }

        void OnDisable() => SceneView.duringSceneGui -= DuringSceneGUI;
        public override void OnInspectorGUI()
        {
            var seed = serializedObject.FindProperty(nameof(SpawnEnemies.Seed));
            var spawner = serializedObject.FindProperty(nameof(SpawnEnemies.SpawnerPrefab));

            var spawnData = serializedObject.FindProperty(nameof(SpawnEnemies.NewSpawnData));
            var spawnOptions = serializedObject.FindProperty(nameof(SpawnEnemies.Options));

            var waitForVisible = serializedObject.FindProperty(nameof(SpawnEnemies.WaitForVisible));
            var waitForMoveOut = serializedObject.FindProperty(nameof(SpawnEnemies.WaitForMoveOut));
            var debugKey = serializedObject.FindProperty(nameof(SpawnEnemies.DebugKey));
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_previewSeed = EditorGUILayout.IntField("Preview-Seed: ", m_previewSeed);
                m_previewPlayerCount = EditorGUILayout.IntSlider("Preview-Players: ", m_previewPlayerCount, 1, 4);

                m_showPreview = GUILayout.Toggle(m_showPreview, "Show EnemySpawning Preview");

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {

                    EditorGUILayout.PropertyField(seed, true);
                    EditorGUILayout.PropertyField(spawner, true);

                    EditorGUILayout.PropertyField(spawnData, true);
                    EditorGUILayout.PropertyField(spawnOptions, true);

                    EditorGUILayout.PropertyField(waitForVisible);
                    EditorGUILayout.PropertyField(waitForMoveOut);
                    EditorGUILayout.PropertyField(debugKey);

                    SpawnerListGUI();
                    PreviewGUI();

                    if (check.changed)
                    {
                        serializedObject.ApplyModifiedProperties();
                        OnChanged();
                    }
                }
            }

            VerificationGUI();
        }
        #endregion

        #region GUI
        void SpawnerListGUI()
        {
            GUILayout.Label("Spawners: ");
            var deleteIdx = -1;
            for (var i = 0; i < target.Spawners.Length; i++)
                SpawnerGUI(i, ref deleteIdx);

            var delete = deleteIdx != -1;
            var add = GUILayout.Button("+ Add Spawner", GUILayout.Width(150f));
            if (delete)
                OnDeleteSpawner(deleteIdx);
            if (add)
                OnAddSpawner();
            if (!add && !delete) return;
            OnChanged();

            PlatformSelection.LeaveEditMode();
        }
        void PreviewGUI()
        {
            if (!m_showPreview)
            {
                m_previewEntry?.Clear();
                return;
            }            
            if (m_previewEntry.IsNullOrEmpty() || m_generatedSeed != m_previewSeed)
                GenerateEnemySpawningPreview();

            EnemySpawnsTimelineGUI.OnGUI(ref m_previewScrollPos, m_previewEntry);
        }
        void SpawnerGUI(int i, ref int deleteIdx)
        {
            ref var spawner = ref GetSpawnerConfig(i);

            //if (spawner.Layout!= null || spawner.ID.Layout!= null)
            //    UpgradeConfig();
            var isCurrentEdit = m_currentEditSpawnEnemies == target && m_currentSpawnerIdx == i;
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("x", GUILayout.Width(25)))
                    deleteIdx = i;
                if (GUILayout.Toggle(isCurrentEdit, "Edit", "Button"))
                    SetCurrentConfig(target, i);
                else if (isCurrentEdit)
                    SetCurrentConfig(null, -1);

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    spawner.Side = (ArenaPlatform.EnemySpawnerAttachmentSide)EditorGUILayout.EnumPopup(spawner.Side);
                    if (check.changed)
                        OnChanged();
                }
            }
        }

        //void UpgradeConfig()
        //{
        //    for (var i = 0; i < target.Spawners.Length; i++)
        //    {
        //        var layout = target.Spawners[i].Layout;
        //        if (layout == null)
        //            layout = target.Spawners[i].ID.Layout;
        //        var idx = Array.FindIndex(ArenaLevelFlowConfig.CurrentLevelFlow.ArenaLayoutDataList,
        //            l => target.Spawners[i].Layout == l.Config);
        //        target.Spawners[i].ID = new ArenaPlatform.PlatformID
        //        {
        //            UniqueLayoutIdx = idx,
        //            Index = target.Spawners[i].PlatformIndex
        //        };
        //        target.Spawners[i].PlatformIndex = default;
        //        target.Spawners[i].Layout = null;
        //    }
        //    Debug.Log("Upgraded Spawner");
        //    OnChanged();
        //}

        #endregion

        #region UI Actions
        void OnChanged()
        {
            m_previewEntry?.Clear();

            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }
        void OnDeleteSpawner(int deleteIdx) => RemoveAt(ref target.Spawners, deleteIdx);

        void OnAddSpawner() =>
            Array.Resize(ref target.Spawners, target.Spawners.Length + 1);

        void SetCurrentConfig(SpawnEnemies spawnEnemies, int spawnerIdx)
        {
            if (m_currentEditSpawnEnemies == spawnEnemies && m_currentSpawnerIdx == spawnerIdx)
                return;
            var validSelection = spawnEnemies != null && spawnerIdx != -1;
            var updatePreview = spawnEnemies != m_currentEditSpawnEnemies;

            ActiveEditorTracker.sharedTracker.isLocked = validSelection;

            m_currentEditSpawnEnemies = spawnEnemies;
            m_currentSpawnerIdx = spawnerIdx;

            if (updatePreview)
                UpdatePreview();
            if (!validSelection)
            {
                Selection.activeObject = ParentEditObject;
                return;
            }

            PlatformSelection.LeaveEditMode();
            UpdateSelectionFromSelectedSpawner();
        }

        void UpdatePreview()
        {
            var isPreview = m_currentEditSpawnEnemies != null;
            PlatformSelection.SetEditSelectionPlatformsActive(!isPreview);

            CleanupPreviewPlatforms();
            if (isPreview)
                PreviewPlatformsForAction((ArenaLevelFlowConfig) ParentEditObject, m_currentEditSpawnEnemies);
        }

        void GenerateEnemySpawningPreview()
        {
            //todo 3: non-robust code
            if (ConfigLinks.Instance == null)
            {
                var guid=AssetDatabase.FindAssets("t:ConfigLinks")[0];
                ConfigLinks.Instance = AssetDatabase.LoadAssetAtPath<ConfigLinks>(AssetDatabase.GUIDToAssetPath(guid));
            }

            var genDat = new EnemySpawnGeneratorData()
                { Seed = m_previewSeed, PlayerCount = m_previewPlayerCount,
                    SpawnerCount = target.Spawners.Length, SpawnData = target.NewSpawnData, Options = target.Options};
            var previewData = EnemyWaveGeneration.GenerateSpawnerEntries(ref genDat);
            m_generatedSeed = m_previewSeed;
            m_previewEntry = previewData;
        }
        #endregion

        #region SceneGUI and Selection
        void UpdateSelectionFromSelectedSpawner()
        {
            var spawner = GetSpawnerConfig(m_currentSpawnerIdx);
            var map = ArenaPlatformPreviewData.GetPlatformMap();
            if (map.IsNullOrEmpty())
                return;
            if (map.TryGetValue(spawner.ID, out var selectedPlatform) 
                && selectedPlatform.PlatformInstance != null)
                Selection.activeGameObject = selectedPlatform.PlatformInstance.gameObject;
        }

        void DuringSceneGUI(SceneView sceneView)
        {
            if (target == null)
            {
                DestroyImmediate(this);
                return;
            }
            if (!IsPreviewMode)
                return;

            var e = Event.current;
            if (e.alt || e.button >= 1)
            {
                m_regularClick = false;
                return;
            }
            if (Tools.current != Tool.View)
                Tools.current = Tool.None;

            // Block all clicks from propagating to the scene view.
            HandleUtility.AddDefaultControl(-1);
            if (e.button == 0)
                DuringSceneGUILeftMouse(e);

            DrawSpawnerGizmos();
        }

        void DrawSpawnerGizmos()
        {
            if (!IsPreviewMode)
                return;

            foreach (var spawner in target.Spawners)
            {
                var map = ArenaPlatformPreviewData.GetPlatformMap();
                if (map.IsNullOrEmpty())
                    continue;
                if (!map.TryGetValue(spawner.ID, out var selectedPlatform) 
                    || selectedPlatform.PlatformInstance == null)
                    continue;
                var platform = selectedPlatform.PlatformInstance;
                var data = Array.Find(platform.EnemySpawnerAttachments, a => a.Side == spawner.Side);
                Handles.ArrowHandleCap(-1, data.AttachmentPos.position, data.AttachmentPos.rotation, 5f, Event.current.type);
            }

            SceneView.RepaintAll();
        }

        void DuringSceneGUILeftMouse(Event e)
        {
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (e.type == EventType.MouseDown)
            {
                e.Use();
                m_regularClick = true;
            }
            else if (e.type == EventType.MouseDrag)
                m_regularClick = false;
            else if (e.type == EventType.MouseUp && m_regularClick)
                DuringSceneGUIClicked(e);
        }

        void DuringSceneGUIClicked(Event e)
        {
            // This returns a picked object as it normally does, but does not select it yet.
            var picked = HandleUtility.PickGameObject(e.mousePosition, false);
            if (picked == null)
                return;
            if (!GetArenaPlatformFromSelection(picked, out var platform))
                return;

            // We select it, if valid for our needs.
            //Selection.activeObject = Platform.gameObject;
            e.Use();

            ref var spawner = ref GetSpawnerConfig(m_currentSpawnerIdx);
            spawner.ID = platform.ID;

            EditorUtility.SetDirty(target);
            UpdateSelectionFromSelectedSpawner();
        }

        static bool GetArenaPlatformFromSelection(GameObject picked, out ArenaPlatform platform)
        {
            platform = picked.GetComponent<ArenaPlatform>();
            var parent = picked.transform.parent;
            if (platform == null && parent != null)
                platform = parent.GetComponent<ArenaPlatform>();
            return platform != null;
        }
        #endregion
    }
}
