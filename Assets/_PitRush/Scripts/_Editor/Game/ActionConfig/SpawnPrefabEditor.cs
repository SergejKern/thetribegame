﻿using System;
using _Editor.Game.Arena;
using Core.Editor.Inspector;
using Core.Unity.Extensions;
using Game.ActionConfig;
using Game.Arena.Level;
using GameUtility.Data.TransformAttachment;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

using static Core.Extensions.CollectionExtensions;

namespace _Editor.Game.ActionConfig
{
    [CustomEditor(typeof(SpawnPrefab))]
    public class SpawnPrefabEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new SpawnPrefab target => base.target as SpawnPrefab;

        GameObject[] m_previewObjects = new GameObject[1];
        GameObject PreviewObject => m_previewObjects.IsIndexInRange(m_previewIdx) ? m_previewObjects[m_previewIdx] : null;
        //set
        //{
        //    if (m_previewObjects.IsIndexInRange(m_previewIdx))
        //        m_previewObjects[m_previewIdx] = value;
        //}

        int m_previewIdx = -1;
        bool IsPreview => m_previewIdx != -1 && m_editorVisible;

        //TransformData m_data;
        Transform m_parent;

        static EditorWindow m_inspector;
        float m_yMin;
        float m_yMax;

        static bool m_showAllPreviewObjects;
        bool m_editorVisible;

        ref SpawnPrefab.LocalTransformData GetLocalData(int i) => ref target.LocalTransformDataList[i];

        static void FindInspector()
        {
            if (m_inspector != null)
                return;
            var windows = Resources.FindObjectsOfTypeAll<EditorWindow>();
            foreach (var ew in windows)
                if (string.Equals(ew.titleContent.text, "Inspector", StringComparison.Ordinal))
                    m_inspector = ew;
        }

        public override void Init()
        {
            base.Init();
            FindInspector();
            m_parent = SceneManager.GetActiveScene().GetRootGameObjects()[0].transform;
            SceneView.duringSceneGui += DuringSceneGUI;
            SyncPreviewObjects();
        }

        void OnDisable()
        {
            SceneView.duringSceneGui -= DuringSceneGUI;
            DestroyPreviewObjects();
        }

        public override void OnInspectorGUI()
        {
            PreviewSettingsGUI();
            var yMin = GUIUtility.GUIToScreenPoint(GUILayoutUtility.GetLastRect().min).y;
            HeaderPropsGUI();

            var deleteIdx = -1;
            var listProp = serializedObject.FindProperty(nameof(SpawnPrefab.LocalTransformDataList));
            TransformDataListGUI(listProp, ref deleteIdx);
            TransformDataListModification(deleteIdx);

            var yMax = GUIUtility.GUIToScreenPoint(GUILayoutUtility.GetLastRect().max).y;

            UpdateVisibility(yMin, yMax);

            VerificationGUI();
        }

        void TransformDataListModification(int deleteIdx)
        {
            var add = GUILayout.Button("+", GUILayout.Width(25));
            var deleted = deleteIdx != -1;
            if (deleted)
                RemoveAt(ref target.LocalTransformDataList, deleteIdx);
            if (add)
                Add(ref target.LocalTransformDataList, new SpawnPrefab.LocalTransformData());

            var modified = add || deleted;
            if (!modified) return;
            SyncPreviewObjects();
            OnChanged();
        }

        void HeaderPropsGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(SpawnPrefab.Prefab)));
                EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(SpawnPrefab.ParentReference)), true);
                EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(SpawnPrefab.AttachToPlatform)));

                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }
        }

        void UpdateVisibility(float yMin, float yMax)
        {
            var newVisible = IsThisEditorVisible(yMin, yMax);
            if (newVisible == m_editorVisible) return;
            m_editorVisible = newVisible;
            UpdateAllActivity();
        }

        void SetPreviewIdx(int idx)
        {
            HidePreviewObject();
            m_previewIdx = idx;
            ShowPreviewObject();
        }

        void HidePreviewObject()
        {
            if (m_showAllPreviewObjects)
                return;

            if (PreviewObject != null)
                PreviewObject.SetActive(false);
        }

        void ShowPreviewObject()
        {
            if (m_previewIdx != -1)
                LevelPlatformPreview.PreviewPlatformsForAction(ArenaLevelFlowConfig.Current, target);

            if (PreviewObject != null)
                PreviewObject.SetActive(true);
        }

        void SyncPreviewObjects()
        {
            if (target.Prefab == null)
                return;
            if (m_parent == null)
                return;

            var prevLength = m_previewObjects.Length;
            for (var i = target.LocalTransformDataList.Length; i < prevLength; i++)
            {
                if (m_previewObjects[i] != null)
                    m_previewObjects[i].DestroyEx();
            }

            if (m_previewObjects.Length != target.LocalTransformDataList.Length)
                Array.Resize(ref m_previewObjects, target.LocalTransformDataList.Length);

            for (var i = 0; i < m_previewObjects.Length; i++)
            {
                if (m_previewObjects[i] != null)
                    continue;

                m_previewObjects[i] = Instantiate(target.Prefab, m_parent, true);
                m_previewObjects[i].hideFlags = HideFlags.HideAndDontSave;
                UpdateActiveOn(i);
                UpdatePositionOn(i);
            }
        }

        void UpdateActiveOn(int i) => m_previewObjects[i].SetActive((i == m_previewIdx || m_showAllPreviewObjects) && m_editorVisible);

        void UpdateAllActivity()
        {
            for (var i = 0; i < m_previewObjects.Length; i++)
            {
                if(m_previewObjects[i] == null)
                    continue;
                UpdateActiveOn(i);
            }
        }

        void UpdateAllPositions()
        {
            for (var i = 0; i < m_previewObjects.Length; i++)
                UpdatePositionOn(i);
        }
        void UpdatePositionOn(int idx)
        {
            if (!m_previewObjects.IsIndexInRange(idx))
                return;
            if (m_previewObjects[idx]==null)
                return;

            var data = GetTransformData(idx);
            var tr = m_previewObjects[idx].transform;
            tr.position = data.Position;
            tr.rotation = data.Rotation;
        }

        void UpdatePositionOnPreviewObject() => UpdatePositionOn(m_previewIdx);

        void DestroyPreviewObjects()
        {
            foreach (var p in m_previewObjects)
            {
                if (p == null)
                    continue;
                p.DestroyEx();
            }
        }

        void TransformDataListGUI(SerializedProperty listProp, ref int deleteIdx)
        {
            for (var i = 0; i < target.LocalTransformDataList.Length; i++)
                TransformDataElementGUI(listProp, ref deleteIdx, i);
        }
        void TransformDataElementGUI(SerializedProperty listProp, ref int deleteIdx, int i)
        {
            var propEl = listProp.GetArrayElementAtIndex(i);
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("x", GUILayout.Width(20f)))
                    deleteIdx = i;
                PreviewButtonGUI(i);
                GUILayout.Space(10f);
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    using (new EditorGUILayout.VerticalScope())
                        EditorGUILayout.PropertyField(propEl, true);
                    if (!check.changed)
                        return;
                    serializedObject.ApplyModifiedProperties();
                    OnPositionGUIChanged();
                }
            }
        }

        void PreviewButtonGUI(int i)
        {
            using (new EditorGUI.DisabledScope(m_parent== null))
            {
                var isPreview = m_previewIdx == i;
                var newPreview = GUILayout.Toggle(isPreview, "Prev", "Button", GUILayout.Width(45));
                if (newPreview == isPreview)
                    return;
                SetPreviewIdx(newPreview ? i : -1);
            }
        }

        void PreviewSettingsGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_showAllPreviewObjects = GUILayout.Toggle(m_showAllPreviewObjects, "Show All Preview Objects");
                if (check.changed)
                    UpdateAllActivity();
            }
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_parent = (Transform) EditorGUILayout.ObjectField("PreviewParent", m_parent, 
                typeof(Transform), true);

                if (check.changed)
                    OnParentChanged();
            }
        }


        void OnParentChanged() => UpdateAllPositions();

        void OnPositionGUIChanged()
        {
            UpdateAllPositions();
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }

        void DuringSceneGUI(SceneView obj)
        {
            if (target.Prefab == null)
                return;
            if (m_parent == null)
                return;

            DuringSceneGUIButtons();
            PreviewObjectGUI();
        }

        void DuringSceneGUIButtons()
        {
            if (m_inspector == null)
                return;

            if (!m_editorVisible)
                return;

            for (var i = 0; i < target.LocalTransformDataList.Length; i++)
            {
                if (i == m_previewIdx)
                    continue;

                var data = GetTransformData(i);
                var pos = data.Position;
                var rot = data.Rotation;
                if (!Handles.Button(pos, rot, 0.5f, 1f, Handles.CubeHandleCap))
                    continue;

                SetPreviewIdx(i);
            }
        }

        static bool IsThisEditorVisible(float min, float max)
        {
            var yMinInspector = m_inspector.position.yMin;
            var yMaxInspector = m_inspector.position.yMax;

            var approxVisible = yMinInspector < max && yMaxInspector > min;
            return approxVisible;
        }

        void PreviewObjectGUI()
        {
            if (PreviewObject == null)
                return;
            if (!IsPreview)
                return;

            var data = GetTransformData(m_previewIdx);
            var pos = data.Position;
            var rot = data.Rotation;

            ref var previewData = ref GetLocalData(m_previewIdx);
            switch (Tools.current)
            {
                case Tool.Move:
                {
                    var newPos = Handles.PositionHandle(pos, rot);
                    if (pos == newPos)
                        return;

                    previewData.LocalPosition = data.Parent.InverseTransformPoint(newPos);
                    UpdatePositionOnPreviewObject();
                    OnChanged();
                    break;
                }

                case Tool.Rotate:
                {
                    var newRot = Handles.RotationHandle(rot, pos);
                    if (rot == newRot)
                        return;

                    previewData.LocalRotation = (Quaternion.Inverse(data.Parent.rotation) * newRot).eulerAngles;
                    UpdatePositionOnPreviewObject();
                    OnChanged();
                    break;
                }

                case Tool.View:
                case Tool.Scale:
                case Tool.Rect:
                case Tool.Transform:
                case Tool.Custom:
                case Tool.None: break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        TransformData GetTransformData(int idx)
        {
            var data = new TransformData()
            {
                Parent = m_parent,
                LocalRotation = target.LocalTransformDataList[idx].LocalRotation,
                LocalPosition = target.LocalTransformDataList[idx].LocalPosition
            };
            return data;
        }
    }
}
