﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Inspector;
using Core.Unity.Extensions;
using Game.ActionConfig;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Editor;
using UnityEditor;
using UnityEngine;
using static Core.Editor.Extensions.EditorExtensions;
using StrPair = System.Tuple<string, string>;

namespace _Editor.Game.ActionConfig
{
    [CustomEditor(typeof(PlayerCountActionContainer))]
    public class PlayerCountActionContainerEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new PlayerCountActionContainer target => base.target as PlayerCountActionContainer;

        readonly Editor[] m_cachedNestedEditor = new Editor[4];
        List<Type> m_platformBehaviourTypes = new List<Type>();
        CommonActionEditorGUI.ActionMenuData m_menu = CommonActionEditorGUI.ActionMenuData.Default;
        Vector2 m_scrollPos;


        public override void Init()
        {
            base.Init();

            var settings = new MenuFromNamespace()
            {
                ReplaceNamespaces = new[] {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
                SkipRoots = new[] { $"{nameof(ScriptableUtility)}" }
            };

            ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref m_platformBehaviourTypes, out var names,
                out var namespaces);
            m_menu.Menu = CreateTypeSelectorPopup(m_platformBehaviourTypes, names, namespaces, settings, TypeSelected);
        }

        void OnDisable()
        {
            foreach (var nestedEd in m_cachedNestedEditor)
                nestedEd.DestroyEx();
        }

        public override void OnInspectorGUI()
        {
            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;
                PlayerActionsGUI();

                if (check.changed)
                    OnChanged();
            }
            VerificationGUI();
        }

        void PlayerActionsGUI()
        {
            var mod = CommonActionEditorGUI.ActionListModifications.Default;
            //var moveIdxA = -1;
            //var moveIdxB = -1;
            var seqListProp = serializedObject.FindProperty("m_actions");
            for (var i = 0; i < 4; i++)
                PlayerActionsElementGUI(i, seqListProp, ref mod); //, ref moveIdxA, ref moveIdxB);

            //var move = moveIdxA!= -1&& moveIdxB!= -1;
            var delete = mod.DeleteIdx != -1;
            var cutPaste = mod.CutPasteIdx != -1;
            var setBehaviour = mod.SetIdx != -1;

            if (cutPaste)
                OnCutPaste(mod.CutPasteIdx);
            // else if (move)
            //    OnMove(moveIdxA, moveIdxB);

            if (delete || cutPaste) //|| move
                OnChanged();

            if (setBehaviour)
                PopupWindow.Show(m_menu.ButtonRect, m_menu.Menu);
        }

        void OnCutPaste(int cutIdx)
        {
            var prop = serializedObject.FindProperty("m_actions").GetArrayElementAtIndex(cutIdx);
            if (target.Editor_Actions[cutIdx] != null)
                CopyPasteOperation.Cut(prop);
            else
                CopyPasteOperation.Paste(prop);
        }


        //void OnMove(int moveIdxA, int moveIdxB)
        //{
        //    var seqA = target.Parallel[moveIdxA];
        //    var seqB = target.Parallel[moveIdxB];
        //    target.Parallel[moveIdxA] = seqB;
        //    target.Parallel[moveIdxB] = seqA;
        //}

        void PlayerActionsElementGUI(int i, SerializedProperty seqListProp, ref CommonActionEditorGUI.ActionListModifications mod) 
        {
            var it = new CommonActionEditorGUI.ActionListIterator
            {
                Idx = i,
                Action = target.Editor_Actions[i],
                ActionProperty = seqListProp.GetArrayElementAtIndex(i),
                Count = target.Editor_Actions.Length
            };

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                CommonActionEditorGUI.DefaultActionListElementHeader(serializedObject, it, ref mod, ref m_menu);
                NestedEditorGUI(i, it.ActionProperty, it.Action);
            }
        }

        void NestedEditorGUI(int i, SerializedProperty seqElProp, ScriptableBaseAction seq)
        {
            if (seq == null && m_cachedNestedEditor[i] != null)
                m_cachedNestedEditor[i].DestroyEx();

            if (!seqElProp.isExpanded || seq == null)
                return;

            CreateCachedEditor(seq, null, ref m_cachedNestedEditor[i]);
            if (m_cachedNestedEditor[i] == null)
                return;
            using (new EditorGUI.DisabledGroupScope(!seq.Enabled))
                m_cachedNestedEditor[i].OnInspectorGUI();
        }

        //void MoveElementGUI(int i, ref int moveIdxA, ref int moveIdxB)
        //{
        //    using (new EditorGUI.DisabledGroupScope(i == 0))
        //        if (GUILayout.Button("^", GUILayout.Width(25)))
        //        {
        //            moveIdxA = i - 1;
        //            moveIdxB = i;
        //        }
        //    using (new EditorGUI.DisabledGroupScope(i == target.Parallel.Count - 1))
        //        if (GUILayout.Button("v", GUILayout.Width(25)))
        //        {
        //            moveIdxA = i;
        //            moveIdxB = i + 1;
        //        }
        //}

        void TypeSelected(object data)
        {
            var type = (Type)data;
            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            var platformBehaviourBase = (ScriptableBaseAction)obj;

            var prevAction = target.Editor_Actions[m_menu.TypeSelectionIdx];
            if (prevAction != null)
                prevAction.DestroyEx();
            target.Editor_Actions[m_menu.TypeSelectionIdx] = platformBehaviourBase;

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }
    }
}
