﻿using Core.Editor.Inspector;
using Core.Extensions;
using Game.ActionConfig;
using Game.Arena;
using Game.Arena.Level;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

using static _Editor.Game.Arena.LevelPlatformPreview;

namespace _Editor.Game.ActionConfig
{
    [CustomEditor(typeof(SetSafePlatform))]
    public class SetSafePlatformEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new SetSafePlatform target => base.target as SetSafePlatform;

        static SetSafePlatform m_currentSetSafePlatform;
        bool m_regularClick;

        static Object ParentEditObject => ArenaLevelFlowConfig.Current;

        bool IsPreviewMode => PreviewUpToAction == target;


        #region UnityMethods
        public override void Init()
        {
            base.Init();

            SceneView.duringSceneGui += DuringSceneGUI;
        }

        void OnDisable() => SceneView.duringSceneGui -= DuringSceneGUI;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var isCurrentEdit = m_currentSetSafePlatform == target;
            if (GUILayout.Toggle(isCurrentEdit, "Edit", "Button"))
                SetCurrentConfig(target);
            else if (isCurrentEdit)
                SetCurrentConfig(null);

            VerificationGUI();
        }
        #endregion

        #region UI Actions
        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }

        void SetCurrentConfig(SetSafePlatform setPlatform)
        {
            if (m_currentSetSafePlatform == setPlatform)
                return;
            var validSelection = setPlatform != null;
            var updatePreview = setPlatform != m_currentSetSafePlatform;

            ActiveEditorTracker.sharedTracker.isLocked = validSelection;

            m_currentSetSafePlatform = setPlatform;

            if (updatePreview)
                UpdatePreview();
            if (!validSelection)
            {
                Selection.activeObject = ParentEditObject;
                return;
            }

            PlatformSelection.LeaveEditMode();
            UpdateSelectionFromSelected();
        }

        static void UpdatePreview()
        {
            var isPreview = m_currentSetSafePlatform != null;
            PlatformSelection.SetEditSelectionPlatformsActive(!isPreview);

            CleanupPreviewPlatforms();
            if (isPreview)
                PreviewPlatformsForAction((ArenaLevelFlowConfig) ParentEditObject, m_currentSetSafePlatform);
        }
        #endregion

        #region SceneGUI and Selection
        void UpdateSelectionFromSelected()
        {
            var map = ArenaPlatformPreviewData.GetPlatformMap();
            if (map.IsNullOrEmpty())
                return;
            if (map.TryGetValue(target.ID, out var selectedPlatform) 
                && selectedPlatform.PlatformInstance != null)
                Selection.activeGameObject = selectedPlatform.PlatformInstance.gameObject;
        }

        void DuringSceneGUI(SceneView sceneView)
        {
            if (target == null)
            {
                DestroyImmediate(this);
                return;
            }
            if (!IsPreviewMode)
                return;

            var e = Event.current;
            if (e.alt || e.button >= 1)
            {
                m_regularClick = false;
                return;
            }
            if (Tools.current != Tool.View)
                Tools.current = Tool.None;

            // Block all clicks from propagating to the scene view.
            HandleUtility.AddDefaultControl(-1);
            if (e.button == 0)
                DuringSceneGUILeftMouse(e);

            //SceneView.RepaintAll();
        }


        void DuringSceneGUILeftMouse(Event e)
        {
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (e.type == EventType.MouseDown)
            {
                e.Use();
                m_regularClick = true;
            }
            else if (e.type == EventType.MouseDrag)
                m_regularClick = false;
            else if (e.type == EventType.MouseUp && m_regularClick)
                DuringSceneGUIClicked(e);
        }

        void DuringSceneGUIClicked(Event e)
        {
            // This returns a picked object as it normally does, but does not select it yet.
            var picked = HandleUtility.PickGameObject(e.mousePosition, false);
            if (picked == null)
                return;
            if (!GetArenaPlatformFromSelection(picked, out var platform))
                return;

            // We select it, if valid for our needs.
            //Selection.activeObject = Platform.gameObject;
            e.Use();

            target.ID = platform.ID;

            OnChanged();
            UpdateSelectionFromSelected();
        }

        static bool GetArenaPlatformFromSelection(GameObject picked, out ArenaPlatform platform)
        {
            platform = picked.GetComponent<ArenaPlatform>();
            var parent = picked.transform.parent;
            if (platform == null && parent != null)
                platform = parent.GetComponent<ArenaPlatform>();
            return platform != null;
        }
        #endregion
    }
}
