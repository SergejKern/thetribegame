﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using Game.ActionConfig;
using Game.Arena;
using Game.Arena.Level;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using StrPair = System.Tuple<string, string>;

namespace _Editor.Game.ActionConfig
{
    [CustomEditor(typeof(PlatformSelector))]
    public class PlatformSelectorEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new PlatformSelector target => base.target as PlatformSelector;

        bool m_drag;
        bool m_regularClick;
        Vector2 m_clickedMousePos;
        Vector2 m_scrollPos;

        bool[] m_usedLayouts = new bool[1];

        static readonly Color k_dragRectSelectionColor = new Color(0f, 0.25f, 0.5f, 0.25f);
        static int LayoutCount =>
            ArenaLevelFlowConfig.Current != null
            && ArenaLevelFlowConfig.Current.ArenaLayoutDataList != null
                ? ArenaLevelFlowConfig.Current.ArenaLayoutDataList.Length
                : 0;

        #region Properties/ Quick Data Access
        bool IsEditingThis => ReferenceEquals(PlatformSelection.EditSelector, target);
        static Object ParentEditObject => ArenaLevelFlowConfig.Current;
        #endregion

        #region UnityMethods
        void OnDisable()
        {
            SceneView.duringSceneGui -= DuringSceneGUI;
            if (IsEditingThis)
                PlatformSelection.CleanupEditSelectionPlatforms();

            m_actionData.CachedEditor.DestroyEx();
        }
        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                PlatformSelectorGUI();
                if (check.changed)
                    OnChanged();
            }

            VerificationGUI();
        }
        #endregion

        #region Init

        public override void Init()
        {
            base.Init();

            SceneView.duringSceneGui += DuringSceneGUI;
            var platformBehaviourTypes = new List<Type>();
            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
                out var namespaces);

            var settings = new EditorExtensions.MenuFromNamespace()
            {
                ReplaceNamespaces = new[] {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
                SkipRoots = new []{$"{nameof(ScriptableUtility)}"}
            };
            m_menuData.Menu =
                EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, settings, OnTypeSelected);
            m_actionData.ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColorDefault;
            m_actionData.CutPasteEnabled = true;

            InitUsedLayouts();
        }

        void InitUsedLayouts()
        {

            if (target.Selection== null)
            {
                Debug.Log("skipping init used layouts, since selection = null");
                return;
            }
            Debug.Log("InitUsedLayouts");

            if (m_usedLayouts.Length != LayoutCount)
                m_usedLayouts = new bool[LayoutCount];
            for (var i = 0; i < LayoutCount; i++)
                m_usedLayouts[i] = Array.FindIndex(target.Selection.SelectionIds, s => s.UniqueLayoutIdx == i) != -1;
        }

        void EnsureSelectionAsset()
        {
            if (target.Selection != null)
                return;
            var path = AssetDatabase.GetAssetPath(target);
            if (path.IsNullOrEmpty())
                return;
            //Debug.Log(path);
            var newSelectionAsset = CreateInstance(typeof(ArenaSelectionConfig));
            newSelectionAsset.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(newSelectionAsset, target);
            target.Selection = (ArenaSelectionConfig) newSelectionAsset;
        }

        #endregion

        #region GUI

        Vector3Int m_move;
        static ArenaPlatform.PlatformID[] m_copyPasteSelection= new ArenaPlatform.PlatformID[0];
        CommonActionEditorGUI.ActionData m_actionData;
        CommonActionEditorGUI.ActionMenuData m_menuData;

        void PlatformSelectorGUI()
        {
            EnsureSelectionAsset();
            if (target.Selection == null)
                return;

            SelectionGUI();

            m_actionData.ActionProperty = serializedObject.FindProperty(nameof(PlatformSelector.ActionConfig));
            m_actionData.Action = target.ActionConfig;
            m_actionData.SObject = serializedObject;
            CommonActionEditorGUI.ActionGUI(ref m_actionData, ref m_menuData, 
                out var removeBehaviour, out var setBehaviour, out var cutPaste);

            //if (target.ActionConfig!= null)
            //    target.SetSelectionActionsRecursively();

            if (removeBehaviour)
                OnRemoveBehaviour();
            if (cutPaste)
                OnCutPaste();

            if (setBehaviour)
                PopupWindow.Show(m_menuData.ButtonRect, m_menuData.Menu);
        }

        void SelectionGUI()
        {
            //if (target.ActionConfig == null)
            //{
            //    if (IsEditingThis)
            //        SetConfigForEdit(null);
            //    return;
            //}

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Toggle(IsEditingThis, "Edit", "Button"))
                    SetConfigForEdit(target);
                else if (IsEditingThis)
                    SetConfigForEdit(null);

                LayoutSelectorGUI();

                CopyPasteSelectionGUI();
            }

            MoveSelectionGUI();
        }

        void CopyPasteSelectionGUI()
        {
            var copySelection = GUILayout.Button("C", GUILayout.Width(30f));
            var pasteSelection = GUILayout.Button("P", GUILayout.Width(30f));

            if (copySelection)
                CopyPasteSelection(ref target.Selection.m_selectionIds, ref m_copyPasteSelection);

            if (pasteSelection)
            {
                CopyPasteSelection(ref m_copyPasteSelection, ref target.Selection.m_selectionIds);
                InitUsedLayouts();
            }

            if (copySelection || pasteSelection)
                OnChanged();
        }

        static void CopyPasteSelection(ref ArenaPlatform.PlatformID[] from, ref ArenaPlatform.PlatformID[] to)
        {
            Array.Resize(ref to, from.Length);
            from.CopyTo(to, 0);
        }

        void OnCutPaste()
        {
            var actionProp = serializedObject.FindProperty(nameof(PlatformSelector.ActionConfig));
            if (target.ActionConfig != null)
                CopyPasteOperation.Cut(actionProp);
            else
                CopyPasteOperation.Paste(actionProp);
        }

        void LayoutSelectorGUI()
        {
            if (m_usedLayouts.Length!= LayoutCount)
                InitUsedLayouts();
            //using (new EditorGUILayout.HorizontalScope())
            //{
            for (var i = 0; i < m_usedLayouts.Length; i++)
            {
                var prevColor = GUI.color;
                GUI.color = m_usedLayouts[i] ? new Color(0.75f, 1f, 0.85f) : new Color(1f, 0.9f, 0.85f);
                var toggled = PlatformSelection.CurrentLayoutIdx == i;
                var newToggled = GUILayout.Toggle(PlatformSelection.CurrentLayoutIdx == i, $"#{i}", "Button");
                if (toggled != newToggled)
                    PlatformSelection.SelectLayoutConfig(i);
                GUI.color = prevColor;
            }
            //}
        }

        void MoveSelectionGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                var moveButton = GUILayout.Button("Move by:");
                m_move = EditorGUILayout.Vector3IntField("", m_move);

                bool clearButton;
                using (new ColorScope(Color.red))
                    clearButton = GUILayout.Button("Clear");

                if (moveButton)
                {
                    for (var i = 0; i < target.Selection.SelectionIds.Length; i++)
                        target.Selection.SelectionIds[i].Index += m_move;
                    OnChanged();
                }

                if (clearButton)
                {
                    var selList = target.Selection.SelectionIds.ToList();
                    selList.RemoveAll(s => s.UniqueLayoutIdx == PlatformSelection.CurrentLayoutIdx);
                    target.Selection.SelectionIds = selList.ToArray();
                    OnChanged();
                }
            }
        }

        #endregion

        #region UI Actions
        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }

        void OnTypeSelected(object data)
        {
            var type = (Type)data;
            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));

            var actionConfig = (ScriptableBaseAction) obj;

            target.ActionConfig = actionConfig;

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void OnRemoveBehaviour()
        {
            CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), target.ActionConfig);
            target.ActionConfig = null;
            AssetDatabase.SaveAssets();
            OnChanged();
        }
        #endregion


        #region SceneGUI
        void DuringSceneGUI(SceneView sceneView)
        {
            if (target == null)
            {
                DestroyImmediate(this);
                return;
            }
            if (!IsEditingThis)
                return;
            DuringSceneGUIEditSelection();
        }

        void DuringSceneGUIEditSelection()
        {
            var e = Event.current;
            if (m_drag)
            {
                DuringSceneGUIDrag(e);
                DrawRect(e);
            }

            if (e.alt || e.button >= 1)
            {
                m_drag = false;
                m_regularClick = false;
                return;
            }
            // Block all clicks from propagating to the scene view.
            HandleUtility.AddDefaultControl(-1);

            QuickSelectLayouts(e);
            if (Tools.current != Tool.View)
                return;

            if (e.button == 0)
                DuringSceneGUILeftMouse(e);
        }

        static void QuickSelectLayouts(Event e)
        {
            if (!e.isKey)
                return;
            if (e.keyCode < KeyCode.Alpha1 || e.keyCode > KeyCode.Alpha9)
                return;
            e.Use();

            var idx = (int) e.keyCode - (int) KeyCode.Alpha1;
            PlatformSelection.SelectLayoutConfig(idx);
        }

        void DuringSceneGUILeftMouse(Event e)
        {
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (e.type == EventType.MouseDown)
            {
                e.Use();
                m_clickedMousePos = e.mousePosition;
                m_drag = false;
                m_regularClick = true;
            }
            else if (e.type == EventType.MouseDrag)
            {
                // todo 3: fix drag
                //var dragDistanceOk = (e.mousePosition - m_clickedMousePos).sqrMagnitude > 32f;
                //if (!dragDistanceOk)
                //    return;
                // m_drag = true;
                // m_regularClick = false;
            }
            else if (e.type == EventType.MouseUp)
            {
                if (m_regularClick)
                    DuringSceneGUIClicked(e);
                else if (m_drag)
                    DuringSceneGUIDragEnd(e);
                m_drag = false;
            }
        }

        void DuringSceneGUIDragEnd(Event e)
        {
            DuringSceneGUIDrag(e);

            var sel = target.Selection;

            var selList = sel.m_selectionIds.ToList();
            selList.RemoveAll(s => s.UniqueLayoutIdx == PlatformSelection.CurrentLayoutIdx);
            selList.AddRange(Selection.gameObjects.Select(go => go.GetComponent<ArenaPlatform>().ID));

            sel.m_selectionIds = selList.ToArray();
            InitUsedLayouts();
            OnChanged();
        }

        void DuringSceneGUIClicked(Event e)
        {
            // This returns a picked object as it normally does, but does not select it yet.
            var picked = HandleUtility.PickGameObject(e.mousePosition, false);
            if (picked == null)
                return;
            if (!PlatformSelection.GetArenaPlatformFromSelection(picked, out var platform))
                return;

            // We select it, if valid for our needs.
            //Selection.activeObject = Platform.gameObject;
            e.Use();

            var sel = target.Selection;
            var idx = Array.FindIndex(sel.m_selectionIds, s => s == platform.ID);
            if (idx != -1)
                CollectionExtensions.RemoveAt(ref sel.m_selectionIds, idx);
            else
                CollectionExtensions.Add(ref sel.m_selectionIds, platform.ID);

            InitUsedLayouts();

            EditorUtility.SetDirty(target);
            PlatformSelection.UpdateSelectionFromCurrentEditConfig();
        }

        void DuringSceneGUIDrag(Event e)
        {
            var rect = GetSelectionRect(e);
            var picked = HandleUtility.PickRectObjects(rect, false);

            // ReSharper disable once ConvertToUsingDeclaration
            using (var configSelection = SimplePool<List<Object>>.I.GetScoped())
            using (var dragSelection = SimplePool<List<Object>>.I.GetScoped())
            {
                PlatformSelection.GetCurrentEditConfigPlatformSelection(configSelection.Obj);
                PlatformSelection.FilterSelectionForArenaPlatforms(picked, dragSelection.Obj);

                if (e.shift)
                    configSelection.Obj.RemoveAll(c => dragSelection.Obj.Contains(c));
                else
                    configSelection.Obj.AddRange(dragSelection.Obj.Where(s => !configSelection.Obj.Contains(s)));

                Selection.objects = configSelection.Obj.ToArray();
            }
        }

        void DrawRect(Event e)
        {
            var rect = GetSelectionRect(e);
            Handles.BeginGUI();
            Handles.DrawSolidRectangleWithOutline(rect, k_dragRectSelectionColor, Color.blue);
            Handles.EndGUI();
            SceneView.RepaintAll();
            //EditorGUI.DrawRect(new Rect(minX, minY, maxX - minX, maxY - minY), Color.blue);
        }

        Rect GetSelectionRect(Event e)
        {
            var minX = Mathf.Min(e.mousePosition.x, m_clickedMousePos.x);
            var maxX = Mathf.Max(e.mousePosition.x, m_clickedMousePos.x);
            var minY = Mathf.Min(e.mousePosition.y, m_clickedMousePos.y);
            var maxY = Mathf.Max(e.mousePosition.y, m_clickedMousePos.y);
            var rect = new Rect(minX, minY, maxX - minX, maxY - minY);
            return rect;
        }
        #endregion


        void SetConfigForEdit(IPlatformSelector selector) => PlatformSelection.SetConfigForEdit(selector, ParentEditObject);
    }
}
