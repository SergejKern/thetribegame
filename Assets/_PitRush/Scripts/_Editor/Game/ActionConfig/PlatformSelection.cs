﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Game.ActionConfig;
using Game.Arena;
using Game.Globals;
using GameUtility.Components.Collision;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;
using _Editor.Game.Arena;
using Game.Arena.Level;
using Object = UnityEngine.Object;
using static _Editor.Game.Arena.LevelPlatformPreview;

namespace _Editor.Game.ActionConfig
{
    public static class PlatformSelection
    {
        public static IPlatformSelector EditSelector { get; private set; }


        static readonly List<ArenaPlatform> k_editSelectionPlatforms = new List<ArenaPlatform>();

        static ArenaLevelFlowConfig.ArenaLayoutData CurrentLayout
        {
            get
            {
                var flow = ArenaLevelFlowConfig.Current;
                if (flow == null)
                    return default;
                return !flow.ArenaLayoutDataList.IsIndexInRange(CurrentLayoutIdx) 
                    ? default 
                    : flow.ArenaLayoutDataList[CurrentLayoutIdx];
            }
        }

        public static int CurrentLayoutIdx;

        static bool IsEditMode => EditSelector != null;
  
        #region UI Actions
        public static void SelectLayoutConfig(int idx)
        {
            CurrentLayoutIdx = idx;
            if (!IsEditMode)
                return;
            CleanupEditSelectionPlatforms();
            ShowEditSelectionPlatforms();
            UpdateSelectionFromCurrentEditConfig();
        }

        #endregion

        #region Selection

        public static void LeaveEditMode() => SetConfigForEdit(null, ArenaLevelFlowConfig.Current);
        //public static void SetConfigForEdit(IPlatformSelector selector) =>
        //    SetConfigForEdit(selector, ArenaLevelFlowConfig.Current);

        public static void SetConfigForEdit(IPlatformSelector selector, Object parentEditObject)
        {
            if (EditSelector == selector)
                return;
            var validSelection = selector != null;
            ActiveEditorTracker.sharedTracker.isLocked = validSelection;
            EditSelector = selector;

            //todo 3: dependency
            if (ArenaLevelFlowConfigEditor.CurrentEditor != null)
                ArenaLevelFlowConfigEditor.CurrentEditor.LeavePreviewMode();

            if (!validSelection)
            {
                Selection.activeObject = parentEditObject;
                CleanupEditSelectionPlatforms();
                return;
            }

            Tools.current = Tool.View;
            PreviewPlatformsForAction((ArenaLevelFlowConfig) parentEditObject, (ScriptableBaseAction) selector);
            ShowEditSelectionPlatforms();
            UpdateSelectionFromCurrentEditConfig();
        }
        public static void UpdateSelectionFromCurrentEditConfig()
        {
            // ReSharper disable once ConvertToUsingDeclaration
            using (var configSelection = SimplePool<List<Object>>.I.GetScoped())
            {
                GetCurrentEditConfigPlatformSelection(configSelection.Obj);
                Selection.objects = configSelection.Obj.ToArray();
            }
        }
        public static void GetCurrentEditConfigPlatformSelection(ICollection<Object> configSelection)
        {
            if (EditSelector == null)
                return;
            var sel = EditSelector.Selection.SelectionIds;
            foreach (var p in k_editSelectionPlatforms)
            {
                if (Array.FindIndex(sel, s => s == p.ID)!=-1)
                    configSelection.Add(p.gameObject);
            }
        }
        public static bool GetArenaPlatformFromSelection(GameObject picked, out ArenaPlatform platform)
        {
            picked = RelativeCollider.GetRoot(picked);
            platform = picked.GetComponent<ArenaPlatform>();
            while (platform == null)
            {
                var parent = picked.transform.parent;
                if (parent == null)
                    return false;

                picked = parent.gameObject;
                platform = picked.GetComponent<ArenaPlatform>();
            }

            if (platform.ID.UniqueLayoutIdx != CurrentLayoutIdx)
                return false;
            return platform != null;
        }
        public static void FilterSelectionForArenaPlatforms(IEnumerable<GameObject> picked, ICollection<Object> filteredSelection)
        {
            foreach (var p in picked)
            {
                if (GetArenaPlatformFromSelection(p, out var platform))
                    filteredSelection.Add(platform.gameObject);
            }
        }
        #endregion

        #region Preview Platforms
        public static void SetEditSelectionPlatformsActive(bool active)
        {
            foreach (var p in k_editSelectionPlatforms)
            {
                if(p.gameObject==null)
                    continue;
                p.gameObject.SetActive(active);
            }
        }

        static void ShowEditSelectionPlatforms()
        {
            CleanupEditSelectionPlatforms();
            var layoutConfig = CurrentLayout.Config;
            if (layoutConfig == null)
                return;
            foreach (Vector3Int idx in layoutConfig.GetIndexEnumerator())
            {
                if (TryTakePreviewPlatform(idx))
                    continue;

                GetPreferredVariant(idx, out var variant);
                var prefab = CurrentLayout.GetPrefab(idx, variant);
                if (prefab==null)
                    continue;
                CurrentLayout.GetDefaultTransformation(idx, prefab, out var pos, out var rot);
                AddEditPlatform(prefab, idx, variant, pos, rot);
            }
        }

        static bool TryTakePreviewPlatform(Vector3Int idx)
        {
            var id = new ArenaPlatform.PlatformID()
            {
                UniqueLayoutIdx = CurrentLayoutIdx,
                Index = idx
            };

            var platformExists = ArenaPlatformPreviewData.GetPlatform(id) != null;
            if (platformExists)
                k_editSelectionPlatforms.Add(ArenaPlatformPreviewData.GetPlatform(id));

            return platformExists;
        }

        static void GetPreferredVariant(Vector3Int idx, out ArenaPrefabVariant variant)
        {
            variant = ArenaPrefabVariant.Editor;
            var map = ArenaPlatformPreviewData.GetPlatformMap();

            var id = new ArenaPlatform.PlatformID()
            {
                UniqueLayoutIdx = CurrentLayoutIdx,
                Index = idx
            };

            if (!map.ContainsKey(id))
                return;
            variant = map[id].Variant;
        }

        static void AddEditPlatform(GameObject prefab, Vector3Int idx, ArenaPrefabVariant variant, Vector3 pos, Quaternion rot)
        {
            var editSelectionPlatform = Object.Instantiate(prefab, pos, rot);
            editSelectionPlatform.hideFlags =
                HideFlags.DontSave | HideFlags.HideInHierarchy | HideFlags.NotEditable;
            editSelectionPlatform.AddComponent<CleanupMarker>();

            var platform = editSelectionPlatform.GetComponent<ArenaPlatform>();
            platform.Init(new ArenaPlatform.PlatformID
            {
                UniqueLayoutIdx = CurrentLayoutIdx,
                Index = idx,
            }, variant); 

            k_editSelectionPlatforms.Add(platform);
        }

        public static void CleanupEditSelectionPlatforms()
        {
            if (k_editSelectionPlatforms.IsNullOrEmpty())
                return;

            foreach (var p in k_editSelectionPlatforms)
            {
                if (p == null)
                    continue;
                p.gameObject.DestroyEx();
            }
            k_editSelectionPlatforms.Clear();
        }
        #endregion
    }
}
