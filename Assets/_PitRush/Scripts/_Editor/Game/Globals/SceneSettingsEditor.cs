﻿using Core.Editor.Inspector;
using Game.Globals;
using UnityEditor;
using UnityEngine;

namespace _Editor.Game.Globals
{
    [CustomEditor(typeof(SceneSettings))]
    public class SceneSettingsEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new SceneSettings target => base.target as SceneSettings;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GameGlobals.SceneLights!= null)
                GameGlobals.SceneLights.Apply(target.LightSettings);

            CameraPositioning();
            VerificationGUI();
        }

        void CameraPositioning()
        {
            if (GameGlobals.GOGameCamera == null)
                return;

            if (GUILayout.Button("Camera to Setup-Pos"))
            {
                GameGlobals.GOGameCamera.transform.position = target.CameraSetupPos.position;
            }

            if (GUILayout.Button("Setup-Pos to Camera-Pos"))
            {
                target.CameraSetupPos.position = GameGlobals.GOGameCamera.transform.position;
            }
        }
    }
}
