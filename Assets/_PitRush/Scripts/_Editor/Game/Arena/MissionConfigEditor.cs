﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Unity.Extensions;
using Game.Arena.Level.Mission;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Editor;
using UnityEditor;
using UnityEngine;
using StrPair = System.Tuple<string, string>;

namespace _Editor.Game.Arena
{
    [CustomEditor(typeof(MissionConfig),true, isFallback = true)]
    public class MissionConfigEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new MissionConfig target => base.target as MissionConfig;

        CommonActionEditorGUI.ActionData m_actionData;
        CommonActionEditorGUI.ActionMenuData m_menuData;


        void OnDisable()
        {
            if (m_actionData.CachedEditor != null)
                m_actionData.CachedEditor.DestroyEx();
        }

        public override void Init()
        {
            base.Init();

            var platformBehaviourTypes = new List<Type>();
            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
                out var namespaces);

            var settings = new EditorExtensions.MenuFromNamespace()
            {
                ReplaceNamespaces = new[] {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
                SkipRoots = new[] { $"{nameof(ScriptableUtility)}" }
            };
            
            m_menuData.Menu =
                EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, settings, OnTypeSelected);
            m_actionData.ActionNotSetColor = new Color(0.85f,0.85f,0.85f);
            m_actionData.CutPasteEnabled = true;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                ActionGUI();

                //var missionFinProp = serializedObject.FindProperty(nameof(target.MissionFinished));
                //var waitForTriggerProp = serializedObject.FindProperty(nameof(target.WaitForFinishWaveActions));
                //EditorGUILayout.PropertyField(missionFinProp);
                //EditorGUILayout.PropertyField(waitForTriggerProp);

                if (check.changed)
                    OnChanged();
            }
            VerificationGUI();
        }

        void ActionGUI()
        {
            m_actionData.ActionProperty = serializedObject.FindProperty(nameof(target.OnFinish));

            m_actionData.Action = target.OnFinish;
            m_actionData.SObject = serializedObject;
            CommonActionEditorGUI.ActionGUI(ref m_actionData, ref m_menuData, 
                out var removeBehaviour, out var setBehaviour, out var cutPaste);

            if (removeBehaviour)
                OnRemoveBehaviour();
            if (cutPaste)
                OnCutPaste();
            if (setBehaviour)
                PopupWindow.Show(m_menuData.ButtonRect, m_menuData.Menu);
        }

        void OnTypeSelected(object data)
        {
            var type = (Type)data;
            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));

            var actionConfig = (ScriptableBaseAction) obj;
            target.OnFinish = actionConfig;

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }

        void OnRemoveBehaviour()
        {
            CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), target.OnFinish);
            target.OnFinish = null;

            AssetDatabase.SaveAssets();
            OnChanged();
        }


        void OnCutPaste()
        {
            bool isPaste;

            var prop = serializedObject.FindProperty(nameof(target.OnFinish));
            isPaste = target.OnFinish == null;
            
            if (isPaste)
                CopyPasteOperation.Paste(prop);
            else
                CopyPasteOperation.Cut(prop);
        }

    }
}
