﻿using System;
using Core.Editor.Inspector;
using Core.Extensions;
using Game.Arena;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace _Editor.Game.Arena
{
    [CustomEditor(typeof(ArenaLayoutConfigGrid))]
    public class ArenaLayoutConfigGridEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new ArenaLayoutConfigGrid target => base.target as ArenaLayoutConfigGrid;

        ArenaPrefabVariant m_addVariant;
        GameObject m_arenaSliceObject;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            InspectorGUIForGridLayout();
        }

        void InspectorGUIForGridLayout()
        {
            var mustAddDefault = target.VariantData.IsNullOrEmpty();
            using (new EditorGUI.DisabledScope(mustAddDefault))
            {
                m_addVariant = (ArenaPrefabVariant) EditorGUILayout.EnumPopup("Variant: ", m_addVariant);
            }
            if (mustAddDefault)
                m_addVariant = ArenaPrefabVariant.Default;

            var exists = target.VariantData != null &&
                         Array.FindIndex(target.VariantData, item => item.Variant == m_addVariant) != -1;

            using (new EditorGUI.DisabledScope(exists))
            {
                var add = GUILayout.Button("+");
                if (!add)
                    return;

                Add(ref target.VariantData, new ArenaLayoutConfigGrid.ArenaPrefabVariantData() { Variant = m_addVariant });
                EditorUtility.SetDirty(target);
                serializedObject.Update();
            }
        
        }
    }
}
