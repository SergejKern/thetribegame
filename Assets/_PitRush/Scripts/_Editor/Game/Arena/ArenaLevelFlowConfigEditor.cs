﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Extensions;
using Game.Arena;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;
using _Editor.Game.ActionConfig;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Types;
using Game.Arena.Level;
using Game.Arena.Level.Mission;
using ScriptableUtility.Editor;
using static Core.Editor.Extensions.EditorExtensions;
using static Core.Extensions.CollectionExtensions;
using static _Editor.Game.Arena.LevelPlatformPreview;
using MessageType = UnityEditor.MessageType;
using StrPair = System.Tuple<string, string>;

namespace _Editor.Game.Arena
{
    // todo 3: check all statics and make helper classes like with the preview stuff also for selecting stuff, edit etc. 
    [CustomEditor(typeof(ArenaLevelFlowConfig))]
    public class ArenaLevelFlowConfigEditor : BaseInspector
    {
        public struct TriggerGUIData
        {
            public CommonActionEditorGUI.ActionData MissionActionData;

            public Editor[] CachedNestedEditors;

            public static TriggerGUIData Default => new TriggerGUIData()
            {
                CachedNestedEditors = new Editor[3],
                MissionActionData = new CommonActionEditorGUI.ActionData(){
                    ActionNotSetColor = new Color(0.85f,0.85f,0.85f),
                    Label = new GUIContent("Mission"),
                    CutPasteEnabled = false
                }
            };
        }

        // ReSharper disable once InconsistentNaming
        new ArenaLevelFlowConfig target => base.target as ArenaLevelFlowConfig;

        public static ArenaLevelFlowConfigEditor CurrentEditor;

        Vector2 m_scrollPos;

        TriggerGUIData[] m_triggerGUIData = new TriggerGUIData[0];

        List<Type> m_platformBehaviourTypes = new List<Type>();
        CommonActionEditorGUI.ActionMenuData m_menu = CommonActionEditorGUI.ActionMenuData.Default;
        CommonActionEditorGUI.ActionMenuData m_missionMenu = CommonActionEditorGUI.ActionMenuData.Default;

        int m_addForTriggerIdx = -1;

        string[] m_layoutConfigOptions;

        #region Properties/ Quick Data Access
        static bool IsPreviewMode => CurrentPreviewIdx != -1;

        //ref ArenaLevelFlowConfig.ArenaLayoutData GetLayoutData(int i) => ref target.ArenaLayoutDataList[i];
        ref ArenaLevelFlowConfig.FlowData GetTriggerData(int i) => ref target.FlowDataList[i];
        ref TriggerGUIData GetTriggerGUIData(int i) => ref m_triggerGUIData[i];


        #endregion

        #region UnityMethods

        void OnDisable()
        {
            if (CurrentEditor != this)
                return;
            PlatformSelection.CleanupEditSelectionPlatforms();
            CleanupPreviewPlatforms();
            foreach (var cachedNestedEditors in m_triggerGUIData)
            foreach (var ed in cachedNestedEditors.CachedNestedEditors)
                ed.DestroyEx();
        }

        public override void OnInspectorGUI()
        {
            CurrentEditor = this;
            //base.OnInspectorGUI();

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var resultProp = serializedObject.FindProperty(nameof(ArenaLevelFlowConfig.Result));

                EditorGUILayout.PropertyField(resultProp);

                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;
                EditorGUILayout.HelpBox("Triggers are not necessarily tied to waves. But they should be setup to occur one after the other! " +
                                        "A wave-trigger should be called Wave0, Wave1, etc.", MessageType.Info);

                ArenaLayoutConfigGUI();
                TriggerDataListGUI();

                if (check.changed)
                    OnChanged();
            }
        }
        #endregion

        #region Init
        public override void Init()
        {
            base.Init();

            ArenaLevelFlowConfig.Current = target;

            m_menu.Menu = CreateMenu<ScriptableBaseAction>(OnActionTypeSelected);
            m_missionMenu.Menu = CreateMenu<MissionConfig>(OnMissionTypeSelected);

            //if (target.ArenaLayoutDataList.Length == 0)
            //    target.ExtractLayoutsFromActions();
            if (target.ArenaLayoutDataList==null)
                target.ArenaLayoutDataList = new ArenaLevelFlowConfig.ArenaLayoutData[0];
            if (target.ArenaLayoutDataList.Length > 0)
                PlatformSelection.SelectLayoutConfig(0);

            ArenaPlatformPreviewData.ClearMap();
            InitLayoutConfigOptions();
        }

        SelectTypesPopup CreateMenu<T>(GenericMenu.MenuFunction2 onTypeSelected)
        {
            //SceneView.duringSceneGui += DuringSceneGUI;
            ReflectionGetAllTypes(typeof(T), ref m_platformBehaviourTypes, out var names,
                out var namespaces);

            var settings = new MenuFromNamespace()
            {
                ReplaceNamespaces = new[]
                {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}", ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}", "Game")
                },
                SkipRoots = new[] {$"{nameof(ScriptableUtility)}"}
            };

            return CreateTypeSelectorPopup(m_platformBehaviourTypes, names, namespaces, settings, onTypeSelected);
        }

        void InitLayoutConfigOptions()
        {
            m_layoutConfigOptions = new string[target.ArenaLayoutDataList.Length];

            for (var i = 0; i < target.ArenaLayoutDataList.Length; i++)
            {
                var el = target.ArenaLayoutDataList[i];
                var config = el.Config;
                var configName = config == null ? "" : config.name;
                var debugName = el.DebugName.IsNullOrEmpty() ? "" : el.DebugName;
                m_layoutConfigOptions[i] = $"#{i} {configName} {debugName}";
            }
        }
        #endregion

        #region GUI

        bool m_showLayoutSettings;
        ArenaLevelFlowConfig.ArenaLayoutData m_addLayoutConfig;
        void ArenaLayoutConfigGUI()
        {
            LayoutSettingsGUI(out var changes, out var deleteIdx);

            if (deleteIdx != -1)
            {
                RemoveAt(ref target.ArenaLayoutDataList, deleteIdx);
                changes = true;
            }

            if (changes)
            {
                OnChanged();
                InitLayoutConfigOptions();
            }

            var selectedIdx = PlatformSelection.CurrentLayoutIdx;
            var newSelectedIdx = EditorGUILayout.Popup(selectedIdx, m_layoutConfigOptions);
            if (selectedIdx == newSelectedIdx)
                return;
            if (IsPreviewMode)
                LeavePreviewMode();

            var idxOk = ArenaLevelFlowConfig.Current.ArenaLayoutDataList.IsIndexInRange(newSelectedIdx);
            if (idxOk)
                PlatformSelection.SelectLayoutConfig(newSelectedIdx);
        }

        void LayoutSettingsGUI(out bool changes, out int deleteIdx)
        {
            changes = false;
            deleteIdx = -1;
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_showLayoutSettings = EditorGUILayout.Foldout(m_showLayoutSettings, "Show Layout Settings");
                if (!m_showLayoutSettings)
                    return;
                ArenaLayoutDataListGUI(ref changes, ref deleteIdx);
                AddLayoutGUI(ref changes);
            }
        }

        void ArenaLayoutDataListGUI(ref bool changes, ref int deleteIdx)
        {
            var layoutListProp = serializedObject.FindProperty(nameof(ArenaLevelFlowConfig.ArenaLayoutDataList));
            for (var i = 0; i < target.ArenaLayoutDataList.Length; i++)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("x", GUILayout.Width(20f)))
                        deleteIdx = i;
                    //ref var layoutData = ref GetLayoutData(i);
                    var elementProp = layoutListProp.GetArrayElementAtIndex(i);
                    using (new EditorGUILayout.VerticalScope())
                    using (var check = new EditorGUI.ChangeCheckScope())
                    {
                        EditorGUILayout.PropertyField(elementProp, true);
                        if (!check.changed)
                            continue;
                        changes = true;
                        serializedObject.ApplyModifiedProperties();
                    }
                }

                //if (!elementProp.isExpanded)
                //    continue;
            }
        }

        void AddLayoutGUI(ref bool changes)
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                EditorGUILayout.LabelField("Add Layout:");
                m_addLayoutConfig.Config = (ArenaLayoutConfig)
                    EditorGUILayout.ObjectField("", m_addLayoutConfig.Config, typeof(ArenaLayoutConfig), false);
                m_addLayoutConfig.DebugName = EditorGUILayout.TextField("DebugName: ", m_addLayoutConfig.DebugName);
                m_addLayoutConfig.Position = EditorGUILayout.Vector3Field("Position: ", m_addLayoutConfig.Position);

                using (new EditorGUI.DisabledGroupScope(m_addLayoutConfig.Config==null))
                {
                    if (!GUILayout.Button("+ Add Layout"))
                        return;
                }

                Add(ref target.ArenaLayoutDataList, m_addLayoutConfig);
                changes = true;
            }
        }

        void TriggerDataListGUI()
        {
            var mod = CommonActionEditorGUI.ActionListModifications.Default;
            SyncNestedEditorDataSize();

            var triggersProp = serializedObject.FindProperty(nameof(ArenaLevelFlowConfig.FlowDataList));
            for (var i = 0; i < target.FlowDataList.Length; i++)
                TriggerDataGUI(i, triggersProp, ref mod);

            EditorGUILayout.Space(15);
            var delete = mod.DeleteIdx != -1;
            var move = mod.MoveIdxA != -1 && mod.MoveIdxB != -1;
            var add = GUILayout.Button("+ Add Flow-Data", GUILayout.Width(175f));
            if (delete)
                OnDeleteTriggerData(mod.DeleteIdx);
            if (add)
                OnAddTriggerData();
            if (move)
                OnMoveTriggerData(mod.MoveIdxA, mod.MoveIdxB);
            if (!delete && !add && !move)
                return;
            OnChanged();
            PlatformSelection.LeaveEditMode();
        }

        void TriggerDataGUI(int i, SerializedProperty triggersProp, ref CommonActionEditorGUI.ActionListModifications mod)
        {
            ref var trigger = ref GetTriggerData(i);
            var isCurrentPreview = CurrentPreviewIdx == i;
            GUILayout.Space(5);

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                var triggerPropElement = triggersProp.GetArrayElementAtIndex(i);

                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("x", GUILayout.Width(25)))
                        mod.DeleteIdx = i;
                    GUILayout.Space(10);
                    triggerPropElement.isExpanded =
                        EditorGUILayout.Foldout(triggerPropElement.isExpanded, trigger.FlowKey.ToString(), true);

                    var actionName = CommonActionEditorGUI.LastSelection != null
                        ? CommonActionEditorGUI.LastSelection.Name
                        : $"[End of {trigger.FlowKey}]";

                    if (GUILayout.Toggle(isCurrentPreview, $"Preview {actionName}", "Button"))
                        PreviewActions(i);
                    else if (isCurrentPreview)
                        LeavePreviewMode();

                    if (GUILayout.Button("CL", GUILayout.Width(25f)))
                        CommonActionEditorGUI.LastSelection = null;

                    MoveTriggerElementGUI(i, ref mod.MoveIdxA, ref mod.MoveIdxB);
                }

                if (!triggerPropElement.isExpanded)
                    return;
                trigger.FlowKey = (LevelTriggerKey) EditorGUILayout.EnumPopup("TriggerKey: ", trigger.FlowKey);

                var musicStateProp = triggerPropElement.FindPropertyRelative(nameof(ArenaLevelFlowConfig.FlowData.MusicState));
                EditorGUILayout.PropertyField(musicStateProp, true);


                MissionGUI(i, trigger, triggerPropElement);
                ActionListGUI(ref trigger, i, triggerPropElement);
            }
        }

        void MissionGUI(int triggerIdx, ArenaLevelFlowConfig.FlowData trigger, SerializedProperty triggerPropElement)
        {
            ref var triggerGUIData = ref GetTriggerGUIData(triggerIdx);

            triggerGUIData.MissionActionData.SObject = serializedObject;
            triggerGUIData.MissionActionData.Action = trigger.Mission;
            triggerGUIData.MissionActionData.ActionProperty =
                triggerPropElement.FindPropertyRelative(nameof(ArenaLevelFlowConfig.FlowData.Mission));

            CommonActionEditorGUI.ActionGUI(ref triggerGUIData.MissionActionData, ref m_missionMenu, 
                out var remove, out var setBehaviour, out var cutPaste);

            if (remove)
                OnRemoveMission(triggerIdx);
            else if (cutPaste)
                OnCutPasteMission(triggerIdx);
            else if (setBehaviour)
            {
                m_addForTriggerIdx = triggerIdx;
                PopupWindow.Show(m_missionMenu.ButtonRect, m_missionMenu.Menu);
            }
        }
        
        void OnRemoveMission(int triggerIdx)
        {
            ref var trigger = ref GetTriggerData(triggerIdx);

            CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), trigger.Mission);
            trigger.Mission = null;
            AssetDatabase.SaveAssets();
            OnChanged();
        }
        void OnCutPasteMission(int triggerIdx)
        {
            ref var triggerGUIData = ref GetTriggerGUIData(triggerIdx);

            if (triggerGUIData.MissionActionData.Action != null)
                CopyPasteOperation.Cut(triggerGUIData.MissionActionData.ActionProperty);
            else
                CopyPasteOperation.Paste(triggerGUIData.MissionActionData.ActionProperty);
        }

        void MoveTriggerElementGUI(int i, ref int moveIdxA, ref int moveIdxB)
        {
            using (new EditorGUI.DisabledGroupScope(i == 0))
                if (GUILayout.Button("^", GUILayout.Width(25)))
                {
                    moveIdxA = i - 1;
                    moveIdxB = i;
                }

            using (new EditorGUI.DisabledGroupScope(i == target.FlowDataList.Length - 1))
                if (GUILayout.Button("v", GUILayout.Width(25)))
                {
                    moveIdxA = i;
                    moveIdxB = i + 1;
                }
        }

        void ActionListGUI(ref ArenaLevelFlowConfig.FlowData flow, int triggerIdx, SerializedProperty triggerProp)
        {
            var actionListProp = triggerProp.FindPropertyRelative(nameof(ArenaLevelFlowConfig.FlowData.Actions));
            var mod = CommonActionEditorGUI.ActionListModifications.Default;

            ref var nestedEdData = ref GetTriggerGUIData(triggerIdx);
            if (nestedEdData.CachedNestedEditors.Length < flow.Actions.Length)
                Array.Resize(ref nestedEdData.CachedNestedEditors, flow.Actions.Length);

            for (var i = 0; i < flow.Actions.Length; i++)
                ActionConfigGUI(ref flow, nestedEdData.CachedNestedEditors, i, actionListProp, ref mod);

            GUILayout.Space(5);
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label($"{flow.Actions.Length} elements");
                var add = GUILayout.Button($"+ Add to {flow.FlowKey}");
                GUILayout.FlexibleSpace();

                var delete = mod.DeleteIdx != -1;
                var cutPaste = mod.CutPasteIdx != -1;
                var setBehaviour = mod.SetIdx != -1;

                if (add)
                    OnAddAction(ref flow);
                else if (delete)
                    OnDeleteAction(ref flow, mod.DeleteIdx);
                else if (cutPaste)
                    OnCutPaste(ref flow, actionListProp, mod.CutPasteIdx);
                else if (setBehaviour)
                {
                    m_addForTriggerIdx = triggerIdx;
                    PopupWindow.Show(m_menu.ButtonRect, m_menu.Menu);
                }

                if (delete || add || cutPaste)
                {
                    OnChanged();
                    PlatformSelection.LeaveEditMode();
                }
            }

            if (flow.Actions.Any(a=> a == null))
                EditorGUILayout.HelpBox("There are empty action slots!",
                    MessageType.Warning);

            GUILayout.Space(10);
        }

        void ActionConfigGUI(ref ArenaLevelFlowConfig.FlowData flow, IList<Editor> nestedEditors, int i,
            SerializedProperty actionListProp, ref CommonActionEditorGUI.ActionListModifications mod)
        {
            var it = new CommonActionEditorGUI.ActionListIterator
            {
                Idx = i,
                Action = flow.Actions[i],
                ActionProperty = actionListProp.GetArrayElementAtIndex(i),
                Count = flow.Actions.Length
            };
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("•", GUILayout.Width(15f));

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    CommonActionEditorGUI.DefaultActionListElementHeader(serializedObject, it, ref mod, ref m_menu);

                    if (it.Action == null || !it.ActionProperty.isExpanded)
                        return;
                    
                    var ed = nestedEditors[i];
                    CreateCachedEditor(it.Action, null, ref ed);
                    nestedEditors[i] = ed;
                    using (new EditorGUI.DisabledGroupScope(!it.Action.Enabled))
                        ed.OnInspectorGUI();
                
                }
            }
        }


        #endregion

        #region UI Actions
        void OnAddTriggerData()
        {
            Array.Resize(ref target.FlowDataList, target.FlowDataList.Length + 1);
            target.FlowDataList[target.FlowDataList.Length - 1] = ArenaLevelFlowConfig.FlowData.Default;
        }

        void OnDeleteTriggerData(int deleteIdx)
        {
            foreach (var action in target.FlowDataList[deleteIdx].Actions)
                CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), action);

            var l = target.FlowDataList.ToList();
            l.RemoveAt(deleteIdx);
            target.FlowDataList = l.ToArray();
        }

        void OnMoveTriggerData(int moveIdxA, int moveIdxB)
        {
            var datA = target.FlowDataList[moveIdxA];
            var datB = target.FlowDataList[moveIdxB];
            target.FlowDataList[moveIdxB] = datA;
            target.FlowDataList[moveIdxA] = datB;
        }

        static void OnAddAction(ref ArenaLevelFlowConfig.FlowData flow) => Add(ref flow.Actions, null);

        void OnDeleteAction(ref ArenaLevelFlowConfig.FlowData flow, int deleteIdx)
        {
            CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), flow.Actions[deleteIdx]);
            RemoveAt(ref flow.Actions, deleteIdx);
        }

        static void OnCutPaste(ref ArenaLevelFlowConfig.FlowData flow, SerializedProperty prop, int cutIdx)
        {
            prop = prop.GetArrayElementAtIndex(cutIdx);
            if (flow.Actions[cutIdx] != null)
                CopyPasteOperation.Cut(prop);
            else
                CopyPasteOperation.Paste(prop);
        }

        void OnActionTypeSelected(object data)
        {
            if (CreateType(data, out var obj) == OperationResult.Error)
                return;

            var action = (ScriptableBaseAction) obj;

            ref var trigger = ref GetTriggerData(m_addForTriggerIdx);
            trigger.Actions[m_menu.TypeSelectionIdx] = action;

            TypeSelectDone();
        }

        void OnMissionTypeSelected(object data)
        {
            if (CreateType(data, out var obj) == OperationResult.Error)
                return;

            var action = (MissionConfig) obj;

            ref var trigger = ref GetTriggerData(m_addForTriggerIdx);
            trigger.Mission = action;

            TypeSelectDone();
        }

        void TypeSelectDone()
        {
            m_addForTriggerIdx = -1;
            m_menu.TypeSelectionIdx = -1;

            //AssetDatabase.SaveAssets();
            OnChanged();
        }

        OperationResult CreateType(object data, out ScriptableObject obj)
        {
            obj = null;
            if (!m_addForTriggerIdx.IsInRange(target.FlowDataList))
                return OperationResult.Error;

            var type = (Type) data;
            obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));
            return OperationResult.OK;
        }
        #endregion

        #region Utility
        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }

        void SyncNestedEditorDataSize()
        {
            if (m_triggerGUIData.Length >= target.FlowDataList.Length) return;
            var prevSize = m_triggerGUIData.Length;
            Array.Resize(ref m_triggerGUIData, target.FlowDataList.Length);
            for (var i = prevSize; i < m_triggerGUIData.Length; i++)
            {
                m_triggerGUIData[i] = TriggerGUIData.Default;
            }
        }
        #endregion
        #region Preview Platforms
        public void LeavePreviewMode()
        {
            PlatformSelection.SetEditSelectionPlatformsActive(true);
            CleanupPreviewPlatforms();
        }

        void PreviewActions(int triggerIdx)
        {
            if (triggerIdx == CurrentPreviewIdx)
                return;
            PlatformSelection.LeaveEditMode();
            PlatformSelection.SetEditSelectionPlatformsActive(false);

            if (CommonActionEditorGUI.LastSelection != null)
                PreviewPlatforms(target, triggerIdx, CommonActionEditorGUI.LastSelection);
            else 
                PreviewPlatformsForTrigger(target, triggerIdx);
        }
        #endregion
    }
}