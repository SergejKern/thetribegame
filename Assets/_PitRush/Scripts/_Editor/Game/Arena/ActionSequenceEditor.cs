﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Unity.Extensions;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.ActionConfigs.Logic;
using UnityEditor;
using UnityEngine;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Unity.Utility.GUITools;
using ScriptableUtility.Editor;
using static Core.Editor.Extensions.EditorExtensions;
using StrPair = System.Tuple<string, string>;

namespace _Editor.Game.Arena
{
    [CustomEditor(typeof(ActionSequence))]
    public class ActionSequenceEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new ActionSequence target => base.target as ActionSequence;

        Editor[] m_cachedNestedEditor = new Editor[3];
        CommonActionEditorGUI.ActionMenuData m_menuData = CommonActionEditorGUI.ActionMenuData.Default;
        Vector2 m_scrollPos;

        public override void Init()
        {
            base.Init();
            var platformBehaviourTypes = new List<Type>();

            var settings = new MenuFromNamespace()
            {
                ReplaceNamespaces = new[] {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
                SkipRoots = new[] { $"{nameof(ScriptableUtility)}" }
            };

            ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
                out var namespaces);
            m_menuData.Menu = CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, settings, TypeSelected);
        }

        void OnDisable()
        {
            foreach (var nestedEd in m_cachedNestedEditor)
                nestedEd.DestroyEx();
        }

        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var prop = serializedObject.FindProperty(ActionSequence.Editor_NameProperty);
                EditorGUILayout.PropertyField(prop);
            
                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;
                SequenceListGUI();

                if (check.changed)
                    OnChanged();
            }
        }

        void SequenceListGUI()
        {

            var mod = CommonActionEditorGUI.ActionListModifications.Default;

            if (target.HasColor)
            {
                EditorGUILayout.BeginHorizontal();
                CustomGUI.VSplitter(target.GUIColor, 2f);
            }
            using (new EditorGUILayout.VerticalScope())
            {
                if (m_cachedNestedEditor.Length < target.Sequence.Count)
                    Array.Resize(ref m_cachedNestedEditor, target.Sequence.Count);

                var seqListProp = serializedObject.FindProperty("m_actions");
                for (var i = 0; i < target.Sequence.Count; i++)
                    SequenceElementGUI(i, seqListProp, ref mod);
            }
            if (target.HasColor)
                EditorGUILayout.EndHorizontal();

            GUILayout.Space(5);
            using (new EditorGUILayout.HorizontalScope())
            {
                CommonActionEditorGUI.ColorGUI(target);
                GUILayout.Space(15f);

                GUILayout.Label($"{target.Sequence.Count} elements");
                var add = GUILayout.Button($"+ Add to {target.Name}");
                GUILayout.FlexibleSpace();

                var move = mod.MoveIdxA != -1 && mod.MoveIdxB != -1;
                var delete = mod.DeleteIdx != -1;
                var cutPaste = mod.CutPasteIdx != -1;
                var setBehaviour = mod.SetIdx != -1;

                if (add)
                    OnAdd();
                else if (move)
                    OnMove(mod.MoveIdxA, mod.MoveIdxB);
                else if (delete)
                    OnDelete(mod.DeleteIdx);
                else if (cutPaste)
                    OnCutPaste(mod.CutPasteIdx);
                else if (setBehaviour)
                    PopupWindow.Show(m_menuData.ButtonRect, m_menuData.Menu);

                if (add || move || delete || cutPaste)
                    OnChanged();
            }

            VerificationGUI();

            GUILayout.Space(10);
        }

        void OnCutPaste(int cutIdx)
        {
            var prop = serializedObject.FindProperty("m_actions").GetArrayElementAtIndex(cutIdx);
            if (target.Sequence[cutIdx] != null)
                CopyPasteOperation.Cut(prop);
            else
                CopyPasteOperation.Paste(prop);
        }

        void OnDelete(int deleteIdx)
        {
            CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), target.Sequence[deleteIdx]);
            target.Sequence.RemoveAt(deleteIdx);
        }

        void OnMove(int moveIdxA, int moveIdxB)
        {
            var seqA = target.Sequence[moveIdxA];
            var seqB = target.Sequence[moveIdxB];
            target.Sequence[moveIdxA] = seqB;
            target.Sequence[moveIdxB] = seqA;
        }

        void OnAdd() => target.Sequence.Add(null);

        void SequenceElementGUI(int i, SerializedProperty seqListProp, ref CommonActionEditorGUI.ActionListModifications mod)
        {
            var it = new CommonActionEditorGUI.ActionListIterator
            {
                Idx = i,
                Action = target.Sequence[i],
                ActionProperty = seqListProp.GetArrayElementAtIndex(i),
                Count = target.Sequence.Count
            };
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new ColorScope(target.GUIColor))
                    GUILayout.Label($"{i+1}.", GUILayout.Width(25f));

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    CommonActionEditorGUI.DefaultActionListElementHeader(serializedObject, it, ref mod, ref m_menuData);
                    NestedEditorGUI(i, it.ActionProperty, it.Action);
                }
            }
        }

        void NestedEditorGUI(int i, SerializedProperty seqElProp, ScriptableBaseAction seq)
        {
            if (seq == null && m_cachedNestedEditor[i] != null)
                m_cachedNestedEditor[i].DestroyEx();

            if (!seqElProp.isExpanded || seq == null)
                return;

            CreateCachedEditor(seq, null, ref m_cachedNestedEditor[i]);
            if (m_cachedNestedEditor[i] == null)
                return;
            using (new EditorGUI.DisabledGroupScope(!seq.Enabled))
                m_cachedNestedEditor[i].OnInspectorGUI();
        }

        void TypeSelected(object data)
        {
            var type = (Type)data;
            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            var platformBehaviourBase = (ScriptableBaseAction) obj;

            var seq = target.Sequence[m_menuData.TypeSelectionIdx];
            if (seq != null)
                seq.DestroyEx();
            target.Sequence[m_menuData.TypeSelectionIdx] = platformBehaviourBase;

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }
    }
}
