﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Unity.Extensions;

using ScriptableUtility.ActionConfigs;
using ScriptableUtility.ActionConfigs.Logic;
using ScriptableUtility.Editor;

using UnityEditor;
using UnityEngine;

using StrPair = System.Tuple<string, string>;

namespace _Editor.Game.Arena
{
    [CustomEditor(typeof(BoolCheck))]
    public class BoolCheckEditor : BaseInspector
    {
        enum ContextActionType
        {
            True,
            False
        }

        ContextActionType m_actionContext;
        // ReSharper disable once InconsistentNaming
        new BoolCheck target => base.target as BoolCheck;

        CommonActionEditorGUI.ActionData m_trueActionData;
        CommonActionEditorGUI.ActionData m_falseActionData;

        CommonActionEditorGUI.ActionMenuData m_menuData;

        void OnDisable()
        {
            if (m_trueActionData.CachedEditor != null)
                m_trueActionData.CachedEditor.DestroyEx();
            if (m_falseActionData.CachedEditor != null)
                m_falseActionData.CachedEditor.DestroyEx();
        }

        public override void Init()
        {
            base.Init();

            var platformBehaviourTypes = new List<Type>();
            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
                out var namespaces);

            var settings = new EditorExtensions.MenuFromNamespace()
            {
                ReplaceNamespaces = new[] {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
                SkipRoots = new[] { $"{nameof(ScriptableUtility)}" }
            };
            m_menuData.Menu = EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, settings, OnTypeSelected);
            m_trueActionData.ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColorDefault;
            m_trueActionData.CutPasteEnabled = true;
            m_trueActionData.Label = new GUIContent("True");
            m_falseActionData.ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColorDefault;
            m_falseActionData.CutPasteEnabled = true;
            m_falseActionData.Label = new GUIContent("False");
        }

        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var boolProp = serializedObject.FindProperty(nameof(target.BoolReference));

                var finishRule = serializedObject.FindProperty(nameof(target.FinishRule));
                var waitForBoolChange = serializedObject.FindProperty(nameof(target.WaitForBoolChange));
                var dontUpdateBoolCheck = serializedObject.FindProperty(nameof(target.DontUpdateBoolCheck));

                var breakVal = serializedObject.FindProperty(nameof(target.BreakValue));

                EditorGUILayout.PropertyField(boolProp);
                EditorGUILayout.PropertyField(finishRule);
                EditorGUILayout.PropertyField(breakVal);
                EditorGUILayout.PropertyField(waitForBoolChange);
                EditorGUILayout.PropertyField(dontUpdateBoolCheck);

                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                ActionGUI(ContextActionType.False, nameof(target.FalseAction),  ref target.FalseAction,
                    ref m_falseActionData);
                ActionGUI(ContextActionType.True, nameof(target.TrueAction), ref target.TrueAction,
                    ref m_trueActionData);
                if (check.changed)
                    OnChanged();
            }
            VerificationGUI();
        }

        void ActionGUI(ContextActionType type, string propertyName, ref ScriptableBaseAction action, ref CommonActionEditorGUI.ActionData data)
        {
            data.ActionProperty = serializedObject.FindProperty(propertyName);
            data.Action = action;
            data.SObject = serializedObject;
            CommonActionEditorGUI.ActionGUI(ref data, ref m_menuData, 
                out var removeBehaviour, out var setBehaviour, out var cutPaste);

            var anyAction = removeBehaviour || cutPaste || setBehaviour;
            if (anyAction)
                m_actionContext = type;

            if (removeBehaviour)
                OnRemoveBehaviour();
            if (cutPaste)
                OnCutPaste();
            if (setBehaviour)
                PopupWindow.Show(m_menuData.ButtonRect, m_menuData.Menu);
        }

        void OnTypeSelected(object data)
        {
            var type = (Type)data;
            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));

            var actionConfig = (ScriptableBaseAction) obj;

            if (m_actionContext == ContextActionType.True)
                target.TrueAction = actionConfig;
            else
                target.FalseAction = actionConfig;

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }

        void OnRemoveBehaviour()
        {
            if (m_actionContext == ContextActionType.True)
            {
                target.TrueAction.DestroyEx();
                target.TrueAction = null;
            }
            else
            {
                target.FalseAction.DestroyEx();
                target.FalseAction = null;
            }

            AssetDatabase.SaveAssets();
            OnChanged();
        }


        void OnCutPaste()
        {
            SerializedProperty prop;
            bool isPaste;
            if (m_actionContext == ContextActionType.True)
            {
                prop = serializedObject.FindProperty(nameof(BoolCheck.TrueAction));
                isPaste = target.TrueAction == null;
            }
            else
            {
                prop = serializedObject.FindProperty(nameof(BoolCheck.FalseAction));
                isPaste = target.FalseAction == null;
            }

            if (isPaste)
                CopyPasteOperation.Paste(prop);
            else
                CopyPasteOperation.Cut(prop);
        }

    }
}
