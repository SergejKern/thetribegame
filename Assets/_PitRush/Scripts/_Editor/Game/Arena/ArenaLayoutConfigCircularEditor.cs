﻿using System;
using Core.Editor.Inspector;
using Core.Extensions;
using Core.Types;
using Core.Unity.Types;
using Game.Arena;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace _Editor.Game.Arena
{
    [CustomEditor(typeof(ArenaLayoutConfigCircular))]
    public class ArenaLayoutConfigCircularEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new ArenaLayoutConfigCircular target => base.target as ArenaLayoutConfigCircular;

        ArenaPrefabVariant m_addVariant;
        GameObject m_arenaSliceObject;
        GameObject m_fenceObject;

        ref ArenaLayoutConfigCircular.ArenaPrefabVariantData GetVariantData(int idx) => ref target.VariantData[idx];

        static ref ArenaLayoutConfigCircular.RingPrefabData GetRingData(ref ArenaLayoutConfigCircular.RingPrefabData[] ringData, 
            int idx) => ref ringData[idx];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            InspectorGUIForCircularLayout();
        }

        void InspectorGUIForCircularLayout()
        {
            var exists = target.VariantData != null &&
                         Array.FindIndex(target.VariantData, 
                             item 
                             => item.Variant == m_addVariant) != -1;

            using (new EditorGUILayout.HorizontalScope())
            {
                var mustAddDefault = target.VariantData.IsNullOrEmpty();
                using (new EditorGUI.DisabledScope(mustAddDefault))
                    m_addVariant = (ArenaPrefabVariant) EditorGUILayout.EnumPopup("Variant: ", m_addVariant);
                if (mustAddDefault)
                    m_addVariant = ArenaPrefabVariant.Default;

                m_arenaSliceObject = (GameObject) EditorGUILayout.ObjectField(m_arenaSliceObject, 
                    typeof(GameObject), false);

                using (new EditorGUI.DisabledScope(exists || m_arenaSliceObject == null))
                {
                    var add = GUILayout.Button("+");
                    if (add && AddVariantDataForCircularLayout() == OperationResult.Error)
                        Debug.LogError("Arena-Object is not set up correctly");
                }
            }

            m_fenceObject = (GameObject)EditorGUILayout.ObjectField(m_fenceObject, 
                typeof(GameObject), false);

            if (exists && m_fenceObject != null)
            {
                var addFence = GUILayout.Button("add fence");
                if (!addFence)
                    return;
                if (AddFenceDataForCircularLayout() == OperationResult.Error)
                    Debug.LogError("Arena-Object is not set up correctly");
            }
        }

        OperationResult AddFenceDataForCircularLayout()
        {
            var sliceTr = m_fenceObject.transform;
            var isSegment = !sliceTr.GetComponentsInChildren<ArenaPlatformFences>().IsNullOrEmpty();
            if (!isSegment)
                return OperationResult.Error;
            Debug.Log("isSegment OK");
            var idx = Array.FindIndex(target.VariantData, 
                v=>v.Variant == m_addVariant);

            return ExtractFenceRingsInSegment(sliceTr, 
                ref GetVariantData(idx)) == OperationResult.Error 
                ? OperationResult.Error : OperationResult.OK;
        }

        
        static OperationResult ExtractFenceRingsInSegment(Transform segTr, 
            ref ArenaLayoutConfigCircular.ArenaPrefabVariantData variantData)
        {
            var fencesRingsCount = segTr.transform.childCount;

            using (var prefab = new EditPrefabAssetScope(AssetDatabase.GetAssetPath(variantData.Prefab)))
            {
                var prefabTr = prefab.PrefabRoot.transform;
                if (ExtractRingsInSegment(prefabTr, out var prefabRingData) == OperationResult.Error)
                    return OperationResult.Error;

                if (prefabRingData.Length != fencesRingsCount)
                    return OperationResult.Error;

                for (var i = 0; i < fencesRingsCount; i++)
                {
                    var fenceChild = segTr.GetChild(i);
                    var isFenceRing = !fenceChild.GetComponentsInChildren<ArenaPlatformFences>().IsNullOrEmpty();
                    if (!isFenceRing)
                        return OperationResult.Error;

                    if (ExtractPlatformFencesForRing(fenceChild, 
                        out var platformFencesInRing) == OperationResult.Error)
                        return OperationResult.Error;

                    ref var ring = ref GetRingData(ref prefabRingData, i);
                    for (var pIdx = 0; pIdx < ring.RingPlatforms.Length; pIdx++)
                    {
                        var pGo = ring.RingPlatforms[pIdx];
                        pGo.TryGetComponent(out ArenaPlatform p);
                        Debug.Log($"setting fence for {p} to {platformFencesInRing[pIdx]}");

                        p.FencesPrefabData = new PrefabData()
                        {
                            PrefabGO = platformFencesInRing[pIdx],
                            UseConfig = false
                        };
                        EditorUtility.SetDirty(pGo);
                    }
                }
            }
            return OperationResult.OK;
        }

        static OperationResult ExtractPlatformFencesForRing(Transform ring, out GameObject[] platformsInRing)
        {
            var platformCount = ring.childCount;
            platformsInRing = new GameObject[platformCount];

            for (var i = 0; i < platformCount; i++)
            {
                var platformTr = ring.GetChild(i);
                var platform = platformTr.GetComponent<ArenaPlatformFences>();
                if (platform == null)
                    return OperationResult.Error;

                platformsInRing[i] = platformTr.gameObject;
            }

            return OperationResult.OK;
        }


        OperationResult AddVariantDataForCircularLayout()
        {
            //for (var i = 0; i < m_arenaSliceObject.transform.childCount; ++i)
            //{
            var sliceTr = m_arenaSliceObject.transform;
            var isSegment = !sliceTr.GetComponentsInChildren<ArenaPlatform>().IsNullOrEmpty();
            if (!isSegment)
                return OperationResult.Error;
            if (ExtractRingsInSegment(sliceTr, out var ringData) == OperationResult.Error)
                return OperationResult.Error;

            Add(ref target.VariantData, new ArenaLayoutConfigCircular.ArenaPrefabVariantData()
            {
                Prefab = m_arenaSliceObject,
                RingData = ringData,
                Variant = m_addVariant
            });
            EditorUtility.SetDirty(target);
            serializedObject.Update();
            //}
            return OperationResult.OK;
        }

        // Layout-Circular
        static OperationResult ExtractRingsInSegment(Transform segTr, out ArenaLayoutConfigCircular.RingPrefabData[] ringData)
        {
            var platformCount = segTr.transform.childCount;
            ringData = new ArenaLayoutConfigCircular.RingPrefabData[platformCount];
            for (var i = 0; i < segTr.transform.childCount; i++)
            {
                var child = segTr.GetChild(i);
                var isRing = !child.GetComponentsInChildren<ArenaPlatform>().IsNullOrEmpty();
                if (!isRing)
                    return OperationResult.Error;

                if (ExtractPlatformsForRing(child, out var platformsInRing) == OperationResult.Error)
                    return OperationResult.Error;

                ringData[i].RingPlatforms = platformsInRing;
            }
            return OperationResult.OK;
        }
        static OperationResult ExtractPlatformsForRing(Transform ring, out GameObject[] platformsInRing)
        {
            var platformCount = ring.childCount;
            platformsInRing = new GameObject[platformCount];

            for (var i = 0; i < platformCount; i++)
            {
                var platformTr = ring.GetChild(i);
                var platform = platformTr.GetComponent<ArenaPlatform>();
                if (platform == null)
                    return OperationResult.Error;

                platformsInRing[i] = platformTr.gameObject;
            }

            return OperationResult.OK;
        }
        // ---


        class EditPrefabAssetScope : IDisposable {
            readonly string m_assetPath;
            public readonly GameObject PrefabRoot;
 
            public EditPrefabAssetScope(string assetPath) {
                m_assetPath = assetPath;
                PrefabRoot = PrefabUtility.LoadPrefabContents(assetPath);
            }
 
            public void Dispose() {
                PrefabUtility.SaveAsPrefabAsset(PrefabRoot, m_assetPath);
                PrefabUtility.UnloadPrefabContents(PrefabRoot);
            }
        }

    }

}
