﻿// duplicated code
//using System;
//using System.Collections.Generic;
//using Core.Editor.Extensions;
//using Core.Editor.Inspector;
//using Core.Unity.Extensions;
//using Core.Unity.Attribute;
//using ScriptableUtility.ActionConfigs;
//using ScriptableUtility.ActionConfigs.Logic;
//using ScriptableUtility.Editor;
//using UnityEditor;
//using UnityEngine;
//using StrPair = System.Tuple<string, string>;
//namespace _Editor.Game.Arena
//{
//    [CustomEditor(typeof(BoolCheck))]
//    public class BoolTestEditor : BaseInspector
//    {
//        enum ContextActionType
//        {
//            True,
//            False
//        }
//        ContextActionType m_actionContext;
//        // ReSharper disable once InconsistentNaming
//        new BoolCheck target => base.target as BoolCheck;
//        CommonActionEditorGUI.ActionData m_trueActionData;
//        CommonActionEditorGUI.ActionData m_falseActionData;
//        CommonActionEditorGUI.ActionMenuData m_menuData;
//        void OnDisable()
//        {
//            if (m_trueActionData.CachedEditor!= null)
//                m_trueActionData.CachedEditor.DestroyEx();
//            if (m_falseActionData.CachedEditor!= null)
//                m_falseActionData.CachedEditor.DestroyEx();
//        }
//        public override void Init()
//        {
//            base.Init();
//            var platformBehaviourTypes = new List<Type>();
//            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
//                out var namespaces);
//            var settings = new EditorExtensions.MenuFromNamespace()
//            {
//                ReplaceNamespaces = new[] {
//                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
//                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
//                SkipRoots = new[] { $"{nameof(ScriptableUtility)}" }
//            };
//            m_menuData.Menu = EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, settings, OnTypeSelected);
//            m_trueActionData.ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColor;
//            m_trueActionData.CutPasteEnabled = true;
//            m_falseActionData.ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColor;
//            m_falseActionData.CutPasteEnabled = true;
//        }
//        public override void OnInspectorGUI()
//        {
//            using (var check = new EditorGUI.ChangeCheckScope())
//            {
//                var boolProp = serializedObject.FindProperty(nameof(target.BoolReference));
//                EditorGUILayout.PropertyField(boolProp);
//                if (check.changed)
//                {
//                    serializedObject.ApplyModifiedProperties();
//                    OnChanged();
//                }
//            }
//            using (var check = new EditorGUI.ChangeCheckScope())
//            {
//                ActionGUI(ContextActionType.False, nameof(target.FalseAction), ref target.FalseAction,
//                    ref m_falseActionData);
//                ActionGUI(ContextActionType.True, nameof(target.TrueAction), ref target.TrueAction,
//                    ref m_trueActionData);
//                if (check.changed)
//                    OnChanged();
//            }
//            VerificationGUI();
//        }
//        void ActionGUI(ContextActionType type, string propertyName, ref ScriptableBaseAction action, ref CommonActionEditorGUI.ActionData data)
//        {
//            m_actionContext = type;
//            data.ActionProperty = serializedObject.FindProperty(propertyName);
//            data.Action = action;
//            data.SObject = serializedObject;
//            CommonActionEditorGUI.ActionGUI(ref data, ref m_menuData, 
//                out var removeBehaviour, out var cutPaste);
//            if (removeBehaviour)
//                OnRemoveBehaviour();
//            if (cutPaste)
//                OnCutPaste();
//        }
//        void OnTypeSelected(object data)
//        {
//            var type = (Type)data;
//            var obj = CreateInstance(type);
//            obj.hideFlags = HideFlags.HideInHierarchy;
//            AssetDatabase.AddObjectToAsset(obj, target);
//            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));
//            var actionConfig = (ScriptableBaseAction) obj;
//            if (m_actionContext == ContextActionType.True)
//                target.TrueAction = actionConfig;
//            else
//                target.FalseAction = actionConfig;
//            AssetDatabase.SaveAssets();
//            OnChanged();
//        }
//        void OnChanged()
//        {
//            EditorUtility.SetDirty(target);
//            serializedObject.Update();
//        }
//        void OnRemoveBehaviour()
//        {
//            if (m_actionContext == ContextActionType.True)
//            {
//                CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), target.TrueAction);
//                target.TrueAction = null;
//            }
//            else
//            {
//                CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), target.FalseAction);
//                target.FalseAction = null;
//            }
//            AssetDatabase.SaveAssets();
//            OnChanged();
//        }
//        void OnCutPaste()
//        {
//            SerializedProperty prop;
//            bool isPaste;
//            if (m_actionContext == ContextActionType.True)
//            {
//                prop = serializedObject.FindProperty(nameof(BoolCheck.TrueAction));
//                isPaste = target.TrueAction == null;
//            }
//            else
//            {
//                prop = serializedObject.FindProperty(nameof(BoolCheck.FalseAction));
//                isPaste = target.FalseAction == null;
//            }
//            if (isPaste)
//                CopyPasteOperation.Paste(prop);
//            else
//                CopyPasteOperation.Cut(prop);
//        }
//    }
//}
