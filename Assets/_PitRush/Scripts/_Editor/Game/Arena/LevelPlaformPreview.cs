using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Types;
using Game.Arena;
using Game.Arena.Level;
using JetBrains.Annotations;
using ScriptableUtility.ActionConfigs;
using static Core.Editor.Extensions.EditorExtensions;

namespace _Editor.Game.Arena
{
    public static class LevelPlatformPreview
    {
        public static readonly ArenaPlatformMapData ArenaPlatformPreviewData = new ArenaPlatformMapData();
        public static int CurrentPreviewIdx = -1;

        public static ScriptableBaseAction CurrentPreviewAction;
        public static ScriptableBaseAction PreviewUpToAction;

        //static ArenaLevelFlowConfig m_currentPreviewConfig;
        static Dictionary<Type, MethodInfo> m_previewer;
        static PreviewingState m_previewState;

        public static void PreviewPlatformsForAction(ArenaLevelFlowConfig config, ScriptableBaseAction upToAction)
            => PreviewPlatforms(config, -1, upToAction);

        public static void PreviewPlatformsForTrigger(ArenaLevelFlowConfig config, int triggerIdx)
            => PreviewPlatforms(config, triggerIdx, null);
        public static void PreviewPlatforms(ArenaLevelFlowConfig config, int triggerIdx, ScriptableBaseAction upToAction)
        {
            if (PreviewUpToAction == upToAction && CurrentPreviewIdx == triggerIdx)
                return;
            if (PreviewUpToAction != null || CurrentPreviewIdx != -1)
                CleanupPreviewPlatforms();

            ArenaPlatformMapData.PlatformData = ArenaPlatformPreviewData;

            //m_currentPreviewConfig = config;
            CurrentPreviewIdx = triggerIdx;

            using (var previewerTypes = SimplePool<List<Type>>.I.GetScoped())
            using (var previewerMap = SimplePool<Dictionary<Type, MethodInfo>>.I.GetScoped())
            {
                GetPreviewerMethods(previewerTypes.Obj, previewerMap.Obj);

                m_previewer = previewerMap.Obj;
                m_previewState = PreviewingState.Preview;
                PreviewUpToAction = upToAction;
                if (triggerIdx == -1)
                    triggerIdx = config.FlowDataList.Length - 1;

                for (var i = 0; i <= triggerIdx; i++)
                {
                    var trigger = config.FlowDataList[i];
                    foreach (var action in trigger.Actions)
                    {
                        if (action == null)
                            continue;
                        PreviewAction(action);
                    }

                    var m = trigger.Mission;
                    if (m != null && m.OnFinish!= null)
                        PreviewAction(m.OnFinish);
                }
            }
        }

        static void PreviewAction(ScriptableBaseAction action)
        {
            if (m_previewState != PreviewingState.Preview)
                return;
            if (action == PreviewUpToAction)
                m_previewState = PreviewingState.PreviewEnd;

            CurrentPreviewAction = action;
            if (m_previewer.TryGetValue(action.GetType(), out var method))
                method.Invoke(null, null);
            if (!(action is IActionConfigContainer container))
                return;

            var actions = container.GetContainedActions();
            foreach (var a in actions)
            {
                if (a != null)
                    PreviewAction(a);
            }
        }

        static void GetPreviewerMethods(List<Type> previewerTypes, IDictionary<Type, MethodInfo> previewerMap)
        {
            ReflectionGetAllTypes(typeof(ILevelPlatformPreview), ref previewerTypes, out _, out _);

            foreach (var previewType in previewerTypes)
            {
                var property = previewType.GetProperty(nameof(ILevelPlatformPreview.PreviewForAction),
                    BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                var method = previewType.GetMethod(nameof(ILevelPlatformPreview.EditorPreview),
                    BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

                if (property == null || method == null)
                    continue;
                var actionType = (Type) property.GetValue(null, null);
                previewerMap.Add(actionType, method);
            }
        }

        public static void CleanupPreviewPlatforms()
        {
            PreviewUpToAction = null;
            CurrentPreviewIdx = -1;
            ArenaPlatformPreviewData.DestroyArena();
        }
    }

    public enum PreviewingState
    {
        Preview, PreviewEnd, //Error
    }

    public interface ILevelPlatformPreview
    {
        // please implement also statically
        [UsedImplicitly]
        Type PreviewForAction { get; }
        void EditorPreview();
    }
}