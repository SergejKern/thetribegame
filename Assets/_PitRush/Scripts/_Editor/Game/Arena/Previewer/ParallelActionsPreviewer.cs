//using System;
//using ScriptableUtility.ActionConfigs.Logic;

//namespace _Editor.Game.Arena.Previewer
//{
//    public class ParallelActionsPreviewer : ILevelPlatformPreview
//    {
//        public static Type PreviewForAction => typeof(ParallelActions);

//        public static void EditorPreview()
//        {
//            if (LevelPlatformPreview.PreviewState != PreviewingState.Preview)
//                return;

//            var actionSeq = (ParallelActions) LevelPlatformPreview.CurrentPreviewAction;

//            foreach (var a in actionSeq.GetContainedActions())
//            {
//                if (a == null)
//                    continue;
//                if (!LevelPlatformPreview.Previewer.TryGetValue(a.GetType(), out var method))
//                    continue;
                
//                LevelPlatformPreview.CurrentPreviewAction = a;
//                method.Invoke(null, null);
//            }

//            if (actionSeq == LevelPlatformPreview.PreviewUpToAction)
//                LevelPlatformPreview.PreviewState = PreviewingState.PreviewEnd;
//        }

//        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
//        void ILevelPlatformPreview.EditorPreview()
//            => EditorPreview();
//    }

//}