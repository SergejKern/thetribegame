﻿using System;
using Game.ActionConfig;
using JetBrains.Annotations;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class PlatformRotationPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(PlatformRotation);

        static void EditorPreview()
        {
            var platformRotation = (PlatformRotation) LevelPlatformPreview.CurrentPreviewAction;
            PlatformRotationAction.InstantComplete(platformRotation.Selection, platformRotation.Segments);
        }

        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }
}
