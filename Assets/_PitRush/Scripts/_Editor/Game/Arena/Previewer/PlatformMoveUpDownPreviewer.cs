﻿using System;
using Game.ActionConfig;
using JetBrains.Annotations;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class PlatformMoveUpAndDownPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(PlatformMoveUpAndDown);

        static void EditorPreview()
        {
            var trans = (PlatformMoveUpAndDown) LevelPlatformPreview.CurrentPreviewAction;
            PlatformMoveUpAndDownAction.InstantComplete(trans.Selection.Platforms, trans.MoveToHeight);
        }
        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }
}
