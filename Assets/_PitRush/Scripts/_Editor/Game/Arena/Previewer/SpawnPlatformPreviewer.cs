﻿using System;
using Core.Types;
using Game.ActionConfig;
using Game.Arena;
using Game.Globals;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class SpawnPlatformPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(SpawnPlatform);

        static void EditorPreview()
        {
            var action = (SpawnPlatform)LevelPlatformPreview.CurrentPreviewAction;
            var selection = action.Selection;
            //var layout = selection.Layout;

            foreach (var id in selection.SelectionIds)
            {
                if (SpawnPlatformAction.GetSpawnPlatformData(id, 
                        selection, action.SpecifyVariant, action.Variant, 
                        out var data) == OperationResult.Error)
                    continue;

                AddPlatform(data.Prefab, id, selection, data.Variant, data.Pos, data.Rot);
            }
        }

        static void AddPlatform(GameObject platformPrefab, ArenaPlatform.PlatformID id, IArenaSelection selection, ArenaPrefabVariant variant, Vector3 pos, Quaternion rot)
        {
            var platformGo = Object.Instantiate(platformPrefab, pos, rot);
            platformGo.hideFlags = HideFlags.DontSave | HideFlags.HideInHierarchy | HideFlags.NotEditable;
            platformGo.AddComponent<CleanupMarker>();

            SpawnPlatformAction.AddToMap(platformGo, id, selection, variant);
        }

        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }
}
