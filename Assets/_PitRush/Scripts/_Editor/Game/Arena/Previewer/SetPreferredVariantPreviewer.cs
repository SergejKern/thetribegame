﻿using System;
using Game.ActionConfig;
using Game.Arena;
using JetBrains.Annotations;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class SetPreferredVariantPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(SetPreferredVariantType);

        static void EditorPreview()
        {
            var action = (SetPreferredVariantType) LevelPlatformPreview.CurrentPreviewAction;

            var selection = action.Selection;

            foreach (var id in selection.SelectionIds)
                selection.SetPreferredVariantType(id, action.Variant);
        }

        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }
}
