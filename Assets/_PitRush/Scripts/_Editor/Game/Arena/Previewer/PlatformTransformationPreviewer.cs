﻿using System;
using Game.ActionConfig;
using JetBrains.Annotations;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class PlatformTransformationPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(PlatformTransformation);

        static void EditorPreview()
        {
            var trans = (PlatformTransformation) LevelPlatformPreview.CurrentPreviewAction;
            PlatformTransformationAction.InstantComplete(trans.Selection.Platforms, trans.TransformationVector);
        }
        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }
}
