﻿using System;
using System.Collections.Generic;
using Core.Types;
using Game.ActionConfig;
using Game.Arena;
using JetBrains.Annotations;
using ScriptableUtility.ActionConfigs;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class PlatformSelectorPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(PlatformSelector);

        static void EditorPreview()
        {
            var selector = (PlatformSelector) LevelPlatformPreview.CurrentPreviewAction;
            var selection = selector.LinkData();
            SetSelectionActionsRecursively(selector, selection);
            selection.InitConfig(ArenaPlatformMapData.PlatformData);

            //if (LevelPlatformPreview.PreviewUpToAction == selector.ActionConfig)
            //    LevelPlatformPreview.PreviewState = PreviewingState.PreviewEnd;
            //if (LevelPlatformPreview.PreviewState!= PreviewingState.Preview)
            //    return;
            //if (selector.ActionConfig== null)
            //    return;
            //if (!LevelPlatformPreview.Previewer.TryGetValue(selector.ActionConfig.GetType(), out var method))
            //    return;
            //LevelPlatformPreview.CurrentPreviewAction = selector.ActionConfig;
            //method.Invoke(null, null);
        }

        static void SetSelectionActionsRecursively(PlatformSelector selector, IArenaSelection sel)
        {
            using (var actionsObj = SimplePool<List<IArenaSelectionAction>>.I.GetScoped())
            {
                GetSelectionActionsRecursively(selector.ActionConfig, actionsObj.Obj);
                foreach (var selectionAction in actionsObj.Obj)
                    selectionAction.Selection = sel;
            }
        }

        static void GetSelectionActionsRecursively(object action, ICollection<IArenaSelectionAction> actions)
        {
            if (action == null)
                return;
            switch (action)
            {
                case PlatformSelector _: return;
                case IArenaSelectionAction selAction: actions.Add(selAction); break;
                case IActionConfigContainer acCon:
                {
                    foreach (var a in acCon.GetContainedActions())
                        GetSelectionActionsRecursively(a, actions);
                    break;
                }
            }
        }

        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }

}