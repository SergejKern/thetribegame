﻿using System;
using Game.ActionConfig;
using Game.Arena;
using JetBrains.Annotations;

namespace _Editor.Game.Arena.Previewer
{
    [UsedImplicitly]
    public class DespawnPlatformPreviewer : ILevelPlatformPreview
    {
        static Type PreviewForAction => typeof(DespawnPlatform);

        static void EditorPreview()
        {
            var despawnAction = (DespawnPlatform) LevelPlatformPreview.CurrentPreviewAction;
            despawnAction.Selection.DespawnPlatforms();
        }

        Type ILevelPlatformPreview.PreviewForAction => PreviewForAction;
        void ILevelPlatformPreview.EditorPreview()
            => EditorPreview();
    }
}