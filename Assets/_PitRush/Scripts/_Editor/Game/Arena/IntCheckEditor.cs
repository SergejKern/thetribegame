﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.ActionConfigs.Logic;
using ScriptableUtility.Editor;

using UnityEditor;
using UnityEngine;

using StrPair = System.Tuple<string, string>;

using static Core.Extensions.CollectionExtensions;

namespace _Editor.Game.Arena
{
    [CustomEditor(typeof(IntCheck))]
    public class IntCheckEditor : BaseInspector
    {
        // ReSharper disable once InconsistentNaming
        new IntCheck target => base.target as IntCheck;

        Editor[] m_cachedNestedEditor = new Editor[3];

        CommonActionEditorGUI.ActionMenuData m_menuData;
        Vector2 m_scrollPos;

        void OnDisable()
        {
            foreach (var nestedEd in m_cachedNestedEditor)
                nestedEd.DestroyEx();
        }

        public override void Init()
        {
            base.Init();

            var platformBehaviourTypes = new List<Type>();
            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
                out var namespaces);

            var settings = new EditorExtensions.MenuFromNamespace()
            {
                ReplaceNamespaces = new[] {
                    new StrPair($"{nameof(ScriptableUtility)}.{nameof(ScriptableUtility.ActionConfigs)}"  , ""),
                    new StrPair($"{nameof(Game)}.{nameof(ActionConfig)}"                                  , "Game")},
                SkipRoots = new[] { $"{nameof(ScriptableUtility)}" }
            };
            m_menuData.Menu = EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, settings, OnTypeSelected);
        }

        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var intProp = serializedObject.FindProperty(nameof(target.IntRef));

                var finishRule = serializedObject.FindProperty(nameof(target.FinishRule));
                var waitForIntChange = serializedObject.FindProperty(nameof(target.WaitForIntChange));
                var dontUpdateIntCheck = serializedObject.FindProperty(nameof(target.DontUpdateIntCheck));

                var breakVal = serializedObject.FindProperty(nameof(target.BreakValue));

                EditorGUILayout.PropertyField(intProp);
                EditorGUILayout.PropertyField(finishRule);
                EditorGUILayout.PropertyField(breakVal);
                EditorGUILayout.PropertyField(waitForIntChange);
                EditorGUILayout.PropertyField(dontUpdateIntCheck);

                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            
            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;
                ListGUI();

                if (check.changed)
                    OnChanged();
            }
        }

        void ListGUI()
        {
            var mod = CommonActionEditorGUI.ActionListModifications.Default;

            if (target.HasColor)
            {
                EditorGUILayout.BeginHorizontal();
                CustomGUI.VSplitter(target.GUIColor, 2f);
            }
            using (new EditorGUILayout.VerticalScope())
            {
                if (m_cachedNestedEditor.Length < target.Actions.Length)
                    Array.Resize(ref m_cachedNestedEditor, target.Actions.Length);

                var seqListProp = serializedObject.FindProperty(nameof(IntCheck.Actions));
                for (var i = 0; i < target.Actions.Length; i++)
                    SequenceElementGUI(i, seqListProp, ref mod);
            }
            if (target.HasColor)
                EditorGUILayout.EndHorizontal();

            GUILayout.Space(5);
            using (new EditorGUILayout.HorizontalScope())
            {
                CommonActionEditorGUI.ColorGUI(target);
                GUILayout.Space(15f);

                GUILayout.Label($"{target.Actions.Length} elements");
                var add = GUILayout.Button($"+ Add to {target.Name}");
                GUILayout.FlexibleSpace();

                var move = mod.MoveIdxA != -1 && mod.MoveIdxB != -1;
                var delete = mod.DeleteIdx != -1;
                var cutPaste = mod.CutPasteIdx != -1;
                var setBehaviour = mod.SetIdx != -1;

                if (add)
                    OnAdd();
                else if (move)
                    OnMove(mod.MoveIdxA, mod.MoveIdxB);
                else if (delete)
                    OnDelete(mod.DeleteIdx);
                else if (cutPaste)
                    OnCutPaste(mod.CutPasteIdx);
                else if (setBehaviour)
                    PopupWindow.Show(m_menuData.ButtonRect, m_menuData.Menu);

                if (add || move || delete || cutPaste)
                    OnChanged();
            }

            VerificationGUI();

            GUILayout.Space(10);
        }

        void OnCutPaste(int cutIdx)
        {
            var prop = serializedObject.FindProperty("m_actions").GetArrayElementAtIndex(cutIdx);
            if (target.Actions[cutIdx] != null)
                CopyPasteOperation.Cut(prop);
            else
                CopyPasteOperation.Paste(prop);
        }

        void OnDelete(int deleteIdx)
        {
            CommonActionEditorGUI.SafeDestroy(AssetDatabase.GetAssetPath(target), target.Actions[deleteIdx]);
            CollectionExtensions.RemoveAt(ref target.Actions, deleteIdx);
        }

        void OnMove(int moveIdxA, int moveIdxB)
        {
            var seqA = target.Actions[moveIdxA];
            var seqB = target.Actions[moveIdxB];
            target.Actions[moveIdxA] = seqB;
            target.Actions[moveIdxB] = seqA;
        }

        void OnAdd() => Add(ref target.Actions, null);

        void SequenceElementGUI(int i, SerializedProperty seqListProp, ref CommonActionEditorGUI.ActionListModifications mod)
        {
            var it = new CommonActionEditorGUI.ActionListIterator
            {
                Idx = i,
                Action = target.Actions[i],
                ActionProperty = seqListProp.GetArrayElementAtIndex(i),
                Count = target.Actions.Length
            };
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new ColorScope(target.GUIColor))
                    GUILayout.Label($"{i}:", GUILayout.Width(25f));

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    CommonActionEditorGUI.DefaultActionListElementHeader(serializedObject, it, ref mod, ref m_menuData);
                    NestedEditorGUI(i, it.ActionProperty, it.Action);
                }
            }
        }

        void NestedEditorGUI(int i, SerializedProperty seqElProp, ScriptableBaseAction seq)
        {
            if (seq == null && m_cachedNestedEditor[i] != null)
                m_cachedNestedEditor[i].DestroyEx();

            if (!seqElProp.isExpanded || seq == null)
                return;

            CreateCachedEditor(seq, null, ref m_cachedNestedEditor[i]);
            if (m_cachedNestedEditor[i] == null)
                return;
            using (new EditorGUI.DisabledGroupScope(!seq.Enabled))
                m_cachedNestedEditor[i].OnInspectorGUI();
        }

        void OnTypeSelected(object data)
        {
            var type = (Type)data;
            var obj = CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, target);
            var platformBehaviourBase = (ScriptableBaseAction) obj;

            var seq = target.Actions[m_menuData.TypeSelectionIdx];
            if (seq != null)
                seq.DestroyEx();
            target.Actions[m_menuData.TypeSelectionIdx] = platformBehaviourBase;

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void OnChanged()
        {
            EditorUtility.SetDirty(target);
            serializedObject.Update();
        }
    }
}
